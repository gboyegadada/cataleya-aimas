<?php

/**
 *
 * [ Cataleya ]
 * ________________________________________________________________________
 *
 * (c) 2012 Fancy Paper Planes
 *
 *
*/



/**
 * General utility class in Cataleya, not to be instantiated.
 *
 * @package Cataleya
 *
 * @author Gboyega Dada
 */
abstract class Cataleya
{
    public static $initialized = false;
    public static $inits = array();


    /**
     * Registers an initializer callable that will be called the first time
     * a Cataleya class is autoloaded.
     *
     * This enables you to tweak the default configuration in a lazy way.
     *
     * @param mixed $callable A valid PHP callable that will be called when autoloading the first Cataleya class
     */
    public static function init($callable)
    {
        self::$inits[] = $callable;
    }

    /**
     * Internal autoloader for spl_autoload_register().
     *
     * @param string $class
     */
    public static function autoload($class)
    {
        //Don't interfere with other autoloaders
        if (0 !== strpos($class, 'Cataleya_')) {
            return;
        }
		
	$class = str_replace('Cataleya_', '', $class);
        $path = __DIR__.DIRECTORY_SEPARATOR.str_replace('_', DIRECTORY_SEPARATOR, $class).'.php';

        if (!file_exists($path)) {
            return;
        }

        require $path;

        if (self::$inits && !self::$initialized) {
            self::$initialized = true;
            foreach (self::$inits as $init) {
                call_user_func($init);
            }
        }
    }

    /**
     * Configure autoloading using Cataleya.
     *
     * This is designed to play nicely with other autoloaders.
     *
     * @param mixed $callable A valid PHP callable that will be called when autoloading the first Cataleya class
     */
    public static function registerAutoload($callable = null)
    {
        if (null !== $callable) {
            self::$inits[] = $callable;
        }
        spl_autoload_register(array('Cataleya', 'autoload'));
    }
}
