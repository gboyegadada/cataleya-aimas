<?php



namespace Cataleya\Front\Dashboard\RPC;
use Cataleya\Front\Dashboard\RPC;
use \Cataleya\Front\Dashboard\Controller as Main;



/*
 *
 * 	@Package: Cataleya
 * 	@Class: \Cataleya\Front\Dashboard\RPC\Controller
 *
 * 	(c) 2014 Fancy Paper Planes
 *
 *
 *
 */

class Controller 
{
    
    
    
    
    protected static $_User = null;
    protected static $_is_logged_in = false;



    public function __construct() {
       
        
    }
    
    
    


    public static function load () {



        // Authenticate user...
        self::doAuth();
        
        if (!self::isLoggedIn()) {
            header('HTTP/1.0 401 Unauthorized'); exit();
        }
        


        switch (self::getVersion()) 
        {
            
            case 'cataleya.1.0':
                return RPC\v1\Resource::load();

            case 'cataleya.1.1':
                return RPC\v1\Resource::load();

            default:
                header('HTTP/1.0 404 Resource not found'); exit();
        }
        
    }
    




    public static function getVersion () 
    {

        static $_version;
        if (empty($_version)) 
        {
            preg_match(
            '!^application\/(?<version>[a-z0-9\.\-_]+)\+json$!',
            strtolower ($_SERVER['HTTP_ACCEPT']),  
            $_m
            ); // 'Application/cataleya.1.0+json'

            $_version = isset($_m['version']) ? $_m['version'] : null;
        }
        
        return $_version;

    }

        

    

    
    /**
     * 
     * @return \Cataleya\Dashboard\User 
     * 
     */
    public function getUser () {
        return self::$_User;
    }


    
    
    /**
     * 
     * 
     * 
     * @return boolean
     * 
     */
    public static function isLoggedIn () 
    {
        return (self::$_is_logged_in === true);
    }
    
    
    
    
    private static function doAuth () {

        self::$_is_logged_in = false;


        // FETCH USER TRENDS INCLUDE (Checks if user is connecting from an unusual location)
        // require_once (INC_PATH.'user_trends.php');

        // Else do normal auth...
        if (isset($_SESSION[SESSION_PREFIX.'ADMIN_ID'], $_SESSION[SESSION_PREFIX.'LOGGED_IN']))
        {


                $_User = \Cataleya\Admin\User::authenticate(
                    $_SESSION[SESSION_PREFIX.'ADMIN_ID'], 
                    $_SESSION[SESSION_PREFIX.'LOGGED_IN']
                );
                
                if ($_User !== NULL) 
                {
                    // Auth complete !!

                    // a. Set new login token...
                    $_SESSION[SESSION_PREFIX.'LOGGED_IN'] = $_User->getLoginToken();
                    
                    // b. Set flag to true..
                    self::$_is_logged_in = true;
                    
                    
                    define('AUTH_TOKEN', $_SESSION[SESSION_PREFIX.'AUTH_TOKEN']);
                    
                }

        } 
        

        // Existing session expired
        else if (isset($_SESSION[SESSION_PREFIX.'ADMIN_ID']))
        {
            unset($_SESSION[SESSION_PREFIX.'ADMIN_ID']);
            unset($_SESSION[SESSION_PREFIX.'LOGGED_IN']);

        }
        
        
        
        define('IS_LOGGED_IN', self::isLoggedIn());

        if (self::isLoggedIn()) 
        {
            $_admin_role = $_User->getRole();
            define('IS_SUPER_USER', ($_admin_role->hasPrivilege('SUPER_USER')));
            define('CAN_DELETE_ADMIN_ACC', ($_admin_role->hasPrivilege('DELETE_ADMIN_PROFILES') || IS_SUPER_USER));
        } else {
            define('IS_SUPER_USER', FALSE);
            define('CAN_DELETE_ADMIN_ACC', FALSE);
        }

    
        
        
        
        return;
        
        
        
        
        
    }
    

        
 
}


