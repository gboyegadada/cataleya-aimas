<?php



namespace Cataleya\Front\Dashboard\RPC\v1\Resource;
use \Cataleya\Error;


/*
 *
 * 	@Package: Cataleya
 * 	@Class: \Cataleya\Front\Dashboard\RPC\v1\Resource\Catalog
 *
 * 	(c) 2014 Fancy Paper Planes
 *
 *
 *
 */

class Catalog
extends \Cataleya\Front\Dashboard\RPC\v1\Resource\Main
{




    public function __construct() {



    }



    /*
     *
     *
     * CALLBACKS
     *
     */








    /**
     *
     * [ importCSV ]
     * ______________________________________________________
     *
     * @param int $_POST['target_id']
     * @param int $_POST['data']
     *
     */
    protected function importCSV () {

        $json_reply = [];
        $_params = $this->digestParams(['store_id'=>'int', 'file_type'=>'int']);



        /* ------------------- 1: Handle Upload. --------------------------------------- */

        $_DateTime = new DateTime ();

        $src = $_FILES['attachment']['tmp_name'];

        if (empty($src) || !file_exists($src) || !is_uploaded_file($src)) _catch_error('Import failed.', __LINE__, true);


        $file_name = 'import_' . $_DateTime->format('_d-m-Y_h.i.s') . '.csv';
        define('CSV_PATH', ROOT_PATH . 'var/imports/' . $file_name);


        if(!move_uploaded_file($src, CSV_PATH))_catch_error('Import failed.', __LINE__, true);

        /* ---------------------------------------------------------------------------- */




        $_Store = \Cataleya\Store::load($_params['store_id']);
        if ($_Store === NULL ) _catch_error('Store could not be found.', __LINE__, true);

        $_Importer = new \Cataleya\Helper\Importer($_Store, $_params['file_type'], CSV_PATH);

        $_Importer->doImport();

        $json_reply['message'] = 'CSV has been imported.';

        return $json_reply;
    }










    protected function setTaxClass () {
        $json_reply = [];
        $_params = $this->digestParams(['target_id'=>'int', 'product_id' => 'integer_array']);

        static $TaxClass = NULL;
        if (empty($TaxClass) && (int)$_params['target_id'] > 0 )
        {
            $TaxClass = \Cataleya\Tax\TaxRule::load($_params['target_id']);
            if ($TaxClass === NULL ) _catch_error('The selected tax class could not be found.', __LINE__, true);
        }



        if ((int)$_params['target_id'] > 0 && $TaxClass !== NULL):

                foreach ($_params['product_id'] as $_id) {

                   // Load product
                   $Product = \Cataleya\Catalog\Product::load((int)$_id);
                   if ($Product === NULL) _catch_error('One or more products not found.', __LINE__, true);

                    $Product->setTaxClass($TaxClass);
                }

        else:

                foreach ($_params['product_id'] as $_id) {

                   // Load product
                   $Product = \Cataleya\Catalog\Product::load((int)$_id);
                   if ($Product === NULL) _catch_error('One or more products not found.', __LINE__, true);

                    $Product->unsetTaxClass();
                }


        endif;

        return $json_reply;
    }



    protected function showProduct () {

            $_params = $this->digestParams(['product_id' => 'integer_array']);
            $json_reply = [];

            $json_reply['affected'] = array ();

            foreach ($_params['product_id'] as $_id) {

               // Load product
               $Product = \Cataleya\Catalog\Product::load((int)$_id);
               if ($Product === NULL) _catch_error('One or more products not found.', __LINE__, true);

                $Product->show();
                $json_reply['affected'][] = $Product->getID();
            }

            return $json_reply;

    }


    protected function hideProduct () {

        $_params = $this->digestParams(['product_id' => 'integer_array']);
        $json_reply = [];

        $json_reply['affected'] = array ();

        foreach ($_params['product_id'] as $_id) {

           // Load product
           $Product = \Cataleya\Catalog\Product::load((int)$_id);
           if ($Product === NULL) _catch_error('One or more products not found.', __LINE__, true);

            $Product->hide();
            $json_reply['affected'][] = $Product->getID();
        }

        return $json_reply;

    }






    protected function feature () {

        $_params = $this->digestParams(['product_id' => 'integer_array']);
        $json_reply = [];

        $json_reply['affected'] = array ();

        foreach ($_params['product_id'] as $_id) {

           // Load product
           $Product = \Cataleya\Catalog\Product::load((int)$_id);
           if ($Product === NULL) _catch_error('One or more products not found.', __LINE__, true);

            $Product->feature();
            $json_reply['affected'][] = $Product->getID();
        }

        return $json_reply;

    }


    protected function unFeature () {

        $_params = $this->digestParams(['product_id' => 'integer_array']);
        $json_reply = [];

        $json_reply['affected'] = array ();

        foreach ($_params['product_id'] as $_id) {

           // Load product
           $Product = \Cataleya\Catalog\Product::load((int)$_id);
           if ($Product === NULL) _catch_error('One or more products not found.', __LINE__, true);

            $Product->unFeature();
            $json_reply['affected'][] = $Product->getID();
        }

        return $json_reply;

    }









    protected function addToCategory () {
        $json_reply = [];
        $_params = $this->digestParams(['target_id'=>'int', 'product_id' => 'integer_array']);

        static $Tag = NULL;
        if (empty($Tag) && (int)$_params['target_id'] > 0 ) $Tag = \Cataleya\Catalog\Tag::load($_params['target_id'], array(), array('show_hidden'=>TRUE));

        if ($Tag === NULL) _catch_error('The selected category could not be found.', __LINE__, true);

        // $Tags = \Cataleya\Catalog\Tags::load();


        foreach ($_params['product_id'] as $_id) {

           // Load product
           $Product = \Cataleya\Catalog\Product::load((int)$_id);


           if ($Product === NULL) _catch_error('One or more products not found.', __LINE__, true);

           // foreach ($Tags as $t) $t->removeProduct($Product);

           $Tag->addProduct($Product);
        }


        $json_reply['Category'] = \Cataleya\Front\Dashboard\Juicer::juice($Tag);
        $json_reply['ProductOptions'] = array ();

        if (count($_params['product_id']) === 1) {
            $Product = \Cataleya\Catalog\Product::load((int)$_params['product_id'][0]);

            foreach ($Product as $_Option) {
                $json_reply['ProductOptions'][] = \Cataleya\Front\Dashboard\Juicer::juice($_Option);
            }

        }


        return $json_reply;

    }


    protected function removeFromCategory () {
        $json_reply = [];
        $_params = $this->digestParams(['target_id'=>'int', 'product_id' => 'integer_array']);

        static $Tag = NULL;
        if (empty($Tag) && (int)$_params['target_id'] > 0 ) $Tag = \Cataleya\Catalog\Tag::load($_params['target_id'], array(), array('show_hidden'=>TRUE));

        if ($Tag === NULL) _catch_error('The selected category could not be found.', __LINE__, true);

        foreach ($_params['product_id'] as $_id) {

           // Load product
           $Product = \Cataleya\Catalog\Product::load((int)$_id);
           if ($Product === NULL) _catch_error('One or more products not found.', __LINE__, true);

           $Tag->removeProduct($Product);
        }


        $json_reply['Category'] = \Cataleya\Front\Dashboard\Juicer::juice($Tag);
        $json_reply['ProductOptions'] = array ();

        if (count($_params['product_id']) === 1) {
            $Product = \Cataleya\Catalog\Product::load((int)$_params['product_id'][0]);

            foreach ($Product as $_Option) {
                $json_reply['ProductOptions'][] = \Cataleya\Front\Dashboard\Juicer::juice($_Option);
            }

        }


        return $json_reply;
    }





    protected function addToStore () {
        $json_reply = [];
        $_params = $this->digestParams(['target_id'=>'int', 'product_id' => 'integer_array']);

        static $Store = NULL;
        if (empty($Store) && (int)$_params['target_id'] > 0 ) $Store = \Cataleya\Store::load($_params['target_id'], array(), array('show_hidden'=>TRUE));

        if ($Store === NULL) _catch_error('The selected store could not be found.', __LINE__, true);


        $Tag = $Store->getRootCategory();

        foreach ($_params['product_id'] as $_id) {

           // Load product
           $Product = \Cataleya\Catalog\Product::load((int)$_id);
           if ($Product === NULL) _catch_error('One or more products not found.', __LINE__, true);

           $Tag->addProduct($Product);
        }

        $json_reply['Store'] = \Cataleya\Front\Dashboard\Juicer::juiceDrop($Store);
        $json_reply['ProductOptions'] = array ();

        if (count($_params['product_id']) === 1) {
            $Product = \Cataleya\Catalog\Product::load((int)$_params['product_id'][0]);

            foreach ($Product as $_Option) {
                $json_reply['ProductOptions'][] = \Cataleya\Front\Dashboard\Juicer::juice($_Option);
            }

        }


        return $json_reply;
    }


    protected function removeFromStore () {
        $json_reply = [];
        $_params = $this->digestParams(['target_id'=>'int', 'product_id' => 'integer_array']);

        static $Store = NULL;
        if (empty($Store) && (int)$_params['target_id'] > 0 ) $Store = \Cataleya\Store::load($_params['target_id'], array(), array('show_hidden'=>TRUE));

        if ($Store === NULL) _catch_error('The selected store could not be found.', __LINE__, true);


        $_StoreCatalog = \Cataleya\Catalog::load(array('Store'=>$Store), array('show_hidden'=>TRUE));

        foreach ($_params['product_id'] as $_id) {

           // Load product
           $Product = \Cataleya\Catalog\Product::load((int)$_id);
           if ($Product === NULL) _catch_error('One or more products not found.', __LINE__, true);

           $_StoreCatalog->removeProduct($Product);
        }



        $json_reply['Store'] = \Cataleya\Front\Dashboard\Juicer::juiceDrop($Store);
        $json_reply['ProductOptions'] = array ();

        if (count($_params['product_id']) === 1) {
            $Product = \Cataleya\Catalog\Product::load((int)$_params['product_id'][0]);

            foreach ($Product as $_Option) {
                $json_reply['ProductOptions'][] = \Cataleya\Front\Dashboard\Juicer::juice($_Option);
            }

        }


        return $json_reply;
    }






    protected function addToSale () {
        $json_reply = [];
        $_params = $this->digestParams(['target_id'=>'int', 'product_id' => 'integer_array']);

        static $Sale = NULL;
        if (empty($Sale) && (int)$_params['target_id'] > 0 ) $Sale = \Cataleya\Sales\Sale::load($_params['target_id']);

        if ($Sale === NULL) _catch_error('The selected coupon could not be found.', __LINE__, true);

        foreach ($_params['product_id'] as $_id) {

           // Load product
           $Product = \Cataleya\Catalog\Product::load((int)$_id);
           if ($Product === NULL) _catch_error('One or more products not found.', __LINE__, true);

           $Sale->addProduct($Product);
        }


        $json_reply['Sale'] = \Cataleya\Front\Dashboard\Juicer::juice($Sale);


        return $json_reply;
    }


    protected function removeFromSale () {
        $json_reply = [];
        $_params = $this->digestParams(['target_id'=>'int', 'product_id' => 'integer_array']);

        static $Sale = NULL;
        if (empty($Sale) && (int)$_params['target_id'] > 0 ) $Sale = \Cataleya\Sales\Sale::load($_params['target_id']);

        if ($Sale === NULL) _catch_error('The selected coupon could not be found.', __LINE__, true);

        foreach ($_params['product_id'] as $_id) {

           // Load product
           $Product = \Cataleya\Catalog\Product::load((int)$_id);
           if ($Product === NULL) _catch_error('One or more products not found.', __LINE__, true);

           $Sale->removeProduct($Product);
        }


        $json_reply['Sale'] = \Cataleya\Front\Dashboard\Juicer::juice($Sale);

        return $json_reply;
    }






    protected function applyCoupon () {
        $json_reply = [];
        $_params = $this->digestParams(['target_id'=>'int', 'product_id' => 'integer_array']);

        static $Coupon = NULL;
        if (empty($Coupon) && (int)$_params['target_id'] > 0 ) $Coupon = \Cataleya\Sales\Coupon::load($_params['target_id']);

        if ($Coupon === NULL) _catch_error('The selected coupon could not be found.', __LINE__, true);

        foreach ($_params['product_id'] as $_id) {

           // Load product
           $Product = \Cataleya\Catalog\Product::load((int)$_id);
           if ($Product === NULL) _catch_error('One or more products not found.', __LINE__, true);

           $Coupon->addProduct($Product);
        }



        $json_reply['Coupon'] = \Cataleya\Front\Dashboard\Juicer::juice($Coupon);


        return $json_reply;
    }


    protected function removeCoupon () {
        $json_reply = [];
        $_params = $this->digestParams(['target_id'=>'int', 'product_id' => 'integer_array']);

        static $Coupon = NULL;
        if (empty($Coupon) && (int)$_params['target_id'] > 0 ) $Coupon = \Cataleya\Sales\Coupon::load($_params['target_id']);

        if ($Coupon === NULL) _catch_error('The selected coupon could not be found.', __LINE__, true);

        foreach ($_params['product_id'] as $_id) {

           // Load product
           $Product = \Cataleya\Catalog\Product::load((int)$_id);
           if ($Product === NULL) _catch_error('One or more products not found.', __LINE__, true);

           $Coupon->removeProduct($Product);
        }


        $json_reply['Coupon'] = \Cataleya\Front\Dashboard\Juicer::juice($Coupon);

        return $json_reply;
    }







    protected function deleteProduct () {
        $json_reply = [];
        $_params = $this->digestParams(['product_id' => 'integer_array']);


        if ($_params['auto_confirm'] !== TRUE)
        {
            $message = 'Are you sure you want to delete the selected items (' . count($_params['product_id']) . ')?';
            $this->confirm($message, FALSE);
        }

        \Cataleya\Helper\DBH::getInstance()->beginTransaction();
        foreach ($_params['product_id'] as $_id) {

           // Load product
           $Product = \Cataleya\Catalog\Product::load((int)$_id);
           if ($Product === NULL) _catch_error('One or more products not found.', __LINE__, true);

           $Product->delete();
        }
        \Cataleya\Helper\DBH::getInstance()->commit();
        $json_reply['Categories'] = $this->juiceCatalog();


        return $json_reply;
    }






    protected function newProduct () {


        $json_reply = [];
        $_params = $this->digestParams([
            'product_type_id'=>'int',
            'manufacturer_id'=>'int',
            'supplier_id'=>'int',
        ]);

        $_PT = \Cataleya\Catalog\Product\Type::load($_params['product_type_id']);
        $_M = \Cataleya\Agent::load($_params['manufacturer_id']);
        $_S = \Cataleya\Agent::load($_params['supplier_id']);

        $title = 'New ' . $_PT->getName();

        \Cataleya\Helper\DBH::getInstance()->beginTransaction();
        $Product = \Cataleya\Catalog\Product::create($_PT, $_M, $_S, $title, $title, 'EN');
        \Cataleya\Helper\DBH::getInstance()->commit();

        $json_reply['message'] = 'New ' . $_PT->getName() . ' added.';

        $json_reply['Product'] = array (
            'id' => $Product->getProductId(),
            'title' =>  $title,
            'description' =>  $title,
            'thumb' =>  $Product->getPrimaryImage()->getHref(\Cataleya\Asset\Image::LARGE),
            'stockLow'  =>  TRUE,
            'stockOut'  =>  TRUE,
            'views' => $Product->getViews(),
            'favourites'    =>  $Product->getFavourites(),
            'sold'  =>  $Product->getTotalSold(),
            'hidden'    =>  $Product->isHidden(),
            'viewsInEnglish'    =>  \Cataleya\Helper::countInEnglish($Product->getViews(), 'View', 'Views'),
            'index' =>  1
        );



        $json_reply['Categories'] = $this->juiceCatalog();

        return $json_reply;

    }













}
