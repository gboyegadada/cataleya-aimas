<?php



namespace Cataleya\Front\Dashboard\RPC\v1\Resource;
use \Cataleya\Error;


/*
 *
 * 	@Package: Cataleya
 * 	@Class: \Cataleya\Front\Dashboard\RPC\v1\Resource\Pages
 *
 * 	(c) 2014 Fancy Paper Planes
 *
 *
 *
 */

class Pages 
extends \Cataleya\Front\Dashboard\RPC\v1\Resource\Main 
{
    
    


    public function __construct() {

        
        
    }
    
    

    /*
     * 
     * 
     * CALLBACKS
     * 
     */





    /**
     * newPage
     *
     * @return void
     */
    protected function newPage () {

        $json_reply = [];
        $_params = $this->digestParams(['store_id'=>'int']);
        
        $_Store = \Cataleya\Store::load($_params['store_id']);
        if ($_Store === NULL ) throw new Error('Store could not be found.');
        
        $_Page = \Cataleya\Front\Shop\Page::create($_Store, $_params['title'], $_params['description'], 'EN');
        
        $json_reply['message'] = 'New page saved.';
        $json_reply['Page'] = \Cataleya\Front\Dashboard\Juicer::juice($_Page);


        return $json_reply;
    }










    /**
     * saveTitle
     *
     * @return void
     */
    protected function saveTitle () {

        $json_reply = [];
        
        $_params = $this->digestParams(['page_id'=>'int', 'text' => 'string']);
        $_Page = \Cataleya\Front\Shop\Page::load($_params['page_id']);
        if ($_Page === NULL ) throw new Error('Page could not be found.');
        
        // [1] Page Description
        $_Page
                
        ->setTitle($_params['text'], 'EN');
        
        
        // Update url_rewrite...
        //$_Page->setSlug($_params['text']);
        
        
        $json_reply['message'] = 'Title saved.';
        $json_reply['Page'] = \Cataleya\Front\Dashboard\Juicer::juiceDrop($_Page);


        return $json_reply;
    }






    /**
     * saveBody
     *
     * @return void
     */
    protected function saveBody () {

        $json_reply = [];
        
        $_params = $this->digestParams(['page_id'=>'int', 'page_content' => 'html']);
        $_Page = \Cataleya\Front\Shop\Page::load($_params['page_id']);
        if ($_Page === NULL ) throw new Error('Page could not be found.');
        
        // [1] Page Description
        $_Page->setText($_params['page_content'], 'EN');
        
        
        // Update url_rewrite...
        // $_Page->setSlug($_params['text']);
        
        
        $json_reply['message'] = 'Body saved.';
        $json_reply['Page'] = \Cataleya\Front\Dashboard\Juicer::juiceDrop($_Page);


        return $json_reply;
    }






    /**
     * save
     *
     * @return void
     */
    protected function save () {

        $json_reply = [];
        
        $_params = $this->digestParams(['page_id'=>'int', 'page_title' => 'string', 'page_content' => 'html']);
        $_Page = \Cataleya\Front\Shop\Page::load($_params['page_id']);
        if ($_Page === NULL ) throw new Error('Page could not be found.');
        
        // [1] Page Content
        $_Page->setTitle($_params['page_title'], 'EN');
        $_Page->setText($_params['page_content'], 'EN');
        
        $_Page->clearAutosave(\Cataleya\Front\Dashboard\User::getInstance());
        
        
        $json_reply['message'] = 'Content saved.';
        $json_reply['Page'] = \Cataleya\Front\Dashboard\Juicer::juiceDrop($_Page);


        return $json_reply;
    }








    /**
     * autosave
     *
     * @return void
     */
    protected function autosave () {

        $json_reply = [];
        
        $_params = $this->digestParams(['page_id'=>'int', 'page_title'=>'string', 'page_content'=>'html']);
        $_Page = \Cataleya\Front\Shop\Page::load($_params['page_id']);
        if ($_Page === NULL ) throw new Error('Page could not be found.');
        
        // [1] Page Description
        if (NULL === $_Page->autosave(\Cataleya\Front\Dashboard\User::getInstance(), $_params['page_title'], $_params['page_content'], 'EN')) throw new Error('Page could not be autosaved.');
        
        
        
        $json_reply['message'] = 'Page autosaved at ' . date('h:iP');
        $json_reply['Page'] = \Cataleya\Front\Dashboard\Juicer::juiceDrop($_Page);
        

        return $json_reply;

    }






    /**
     * clearAutosave
     *
     * @return void
     */
    protected function clearAutosave () {

        $json_reply = [];
        $_params = $this->digestParams(['page_id' => 'int']);
        
        $_Page = \Cataleya\Front\Shop\Page::load($_param['page_id']);
        if ($_Page === NULL ) throw new Error('Page could not be found.');
        
        $_Page->clearAutosave(\Cataleya\Front\Dashboard\User::getInstance());
        
        
        
        $json_reply['message'] = 'Auto save cleared at ' . date('h:iP');
        $json_reply['Page'] = \Cataleya\Front\Dashboard\Juicer::juiceDrop($_Page);
        

        return $json_reply;

    }








    /**
     * deletePage
     *
     * @return void
     */
    protected function deletePage () {

        $json_reply = [];
        $_params = $this->digestParams(['page_id' => 'int']);
     
        $_Page = \Cataleya\Front\Shop\Page::load($_params['page_id']);
        if ($_Page === NULL ) throw new Error('Page could not be found.');
        
        if ($_params['auto_confirm'] !== TRUE) 
        {
            $message = 'Are you sure you want to delete this page: "' . $_Page->getDescription()->getTitle('EN') . '"?';
            $this->confirm($message, FALSE);
        }
        

        $_Page->delete();
        
        $json_reply['message'] = 'Page deleted.';
        $json_reply['Page'] = ['id'=>$_params['page_id']];


        return $json_reply;
    }









    /**
     * getPageInfo
     *
     * @return void
     */
    protected function getPageInfo () {

        $json_reply = [];
        $_params = $this->digestParams(['page_id' => 'int']);
     
        $_Page = \Cataleya\Front\Shop\Page::load((int)$_params['page_id']);
        if ($_Page === NULL ) throw new Error('Page could not be found.');
        
        $_Store = $_Page->getStore();
        
        $json_reply['message'] = 'Page info.';
        $json_reply['Page'] = \Cataleya\Front\Dashboard\Juicer::juice($_Page);
        $json_reply['Theme'] = \Cataleya\Front\Dashboard\Juicer::juice($_Store->getTheme());
        
        $_temps = $_apps = [];
        
        foreach ($json_reply['Theme']['page_templates'] as $k=>$v) {
            
            $_temps[] = array(
                'id' => $v, 
                'name' => $v, 
                'selected' => ($_Page->getTemplateID() === $v)
            );
            
        }
        
        foreach (\Cataleya\Plugins\Package::getAppList('page') as $_handle) {
            $_apps[] = \Cataleya\Plugins\Package::getAppInfo($_handle);
        }


        $json_reply['Theme']['page_templates'] = $_temps;
        $json_reply['AppList'] = $_apps;


        return $json_reply;
    }







    /**
     * getAutosave
     *
     * @return void
     */
    protected function getAutosave () {

        $json_reply = [];
        $_params = $this->digestParams(['page_id' => 'int']);
     
        $_Page = \Cataleya\Front\Shop\Page::load((int)$_params['page_id']);
        if ($_Page === NULL ) throw new Error('Page could not be found.');
        
        $_Autosave = $_Page->getAutosave(\Cataleya\Front\Dashboard\User::getInstance());
        
        $json_reply['message'] = 'Autosave info.';
        $json_reply['Autosave'] = (NULL === $_Autosave) ? FALSE : $json_reply['Autosave'] = \Cataleya\Front\Dashboard\Juicer::juice($_Autosave);
        

        return $json_reply;
    }









    /**
     * saveTemplate
     *
     * @return void
     */
    protected function saveTemplate () {

        $json_reply = [];
        
        $_params = $this->digestParams(['page_id'=>'int', 'template_id'=>'filename']);
        $_Page = \Cataleya\Front\Shop\Page::load($_params['page_id']);
        if ($_Page === NULL ) throw new Error('Page could not be found.');
        
        // [1] Page Description
        $_Page->setTemplateID($_params['template_id']);
        
        
        
        $json_reply['message'] = 'Template saved.';
        $json_reply['Page'] = \Cataleya\Front\Dashboard\Juicer::juiceDrop($_Page);


        return $json_reply;
    }





    /**
     * addPlugins
     *
     * @access private
     * @return void
     */
    protected function addPlugins () {

        $json_reply = [];
        
        $_params = $this->digestParams(['page_id'=>'int', 'plugin_handles'=>'string_array']);
        $_Page = \Cataleya\Front\Shop\Page::load($_params['page_id']);
        if ($_Page === NULL ) throw new Error('Page could not be found.');

        try {
            $_Page->addPlugin($_params['plugin_handles']);
        } catch (Exception $e) {
            throw new Error('Plugin' . ((count($_params['plugin_handles'])>1) ? 's' : '') . ' could not be added: '.$e->getMessage());
        }
        
        $json_reply['message'] = 'Plugin added.';
        // $json_reply['Page'] = \Cataleya\Front\Dashboard\Juicer::juiceDrop($_Page);

        foreach ($_Page->getAppList() as $_handle) {
            $_apps[] = \Cataleya\Plugins\Package::getAppInfo($_handle);
        }

        $json_reply['AppList'] = $_apps;


        return $json_reply;
    }







    /**
     * removePlugins
     *
     * @access private
     * @return void
     */
    protected function removePlugins () {

        $json_reply = [];
        
        $_params = $this->digestParams(['page_id'=>'int', 'plugin_handles'=>'string_array']);
        $_Page = \Cataleya\Front\Shop\Page::load($_params['page_id']);
        if ($_Page === NULL ) throw new Error('Page could not be found.');

        try {
            $_Page->removePlugin($_params['plugin_handles']);
        } catch (Exception $e) {
            throw new Error('Plugin' . ((count($_params['plugin_handles'])>1) ? 's' : '') . ' could not be removed: '.$e->getMessage());
        }
        
        $json_reply['message'] = 'Plugin removed.';
        $json_reply['AppList'] = $_params['plugin_handles'];
        // $json_reply['Page'] = \Cataleya\Front\Dashboard\Juicer::juiceDrop($_Page);


        return $json_reply;
    }




    /**
     * savePageHandle
     *
     * @return void
     */
    protected function savePageHandle () {

        $json_reply = [];
        
        $_params = $this->digestParams(['page_id'=>'int', 'page_handle'=>'string']);
        $_Page = \Cataleya\Front\Shop\Page::load($_params['page_id']);
        if ($_Page === NULL ) throw new Error('Page could not be found.');
        
        // [1] Page Description
        $_Page->setHandle($_params['page_handle']);
        
        
        
        $json_reply['message'] = 'Page handle saved.';
        $json_reply['Page'] = \Cataleya\Front\Dashboard\Juicer::juiceDrop($_Page);

        return $json_reply;

    }










    
    

    
      
      
    

        

 
}


