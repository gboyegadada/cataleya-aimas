<?php



namespace Cataleya\Front\Dashboard\RPC\v1\Resource;
use \Cataleya\Error;


/*
 *
 * 	@Package: Cataleya
 * 	@Class: \Cataleya\Front\Dashboard\RPC\v1\Resource\Customer
 *
 * 	(c) 2014 Fancy Paper Planes
 *
 *
 *
 */

class Customer 
extends \Cataleya\Front\Dashboard\RPC\v1\Resource\Main 
{
    
    


    public function __construct() {

        
        
    }
    
    

    /*
     * 
     * 
     * CALLBACKS
     * 
     */








      
    

    /**
     * saveProfile
     *
     * @return array
     */
    protected function saveProfile () {


        $json_reply = [];
        $_params = $this->digestParams([
            'target_id' => 'int', 
            'fname' => 'name', 
            'lname' => 'name',
            'email' => 'email', 
            'phone' => 'phone', 
            'set_password' => 'bool' 
            // 'password' => 'password', 
            // 'confirm_password' => 'password'
        ]);


        if ($_params['set_password'] === TRUE && isset($_params['password'], $_params['confirm_password']))
        {
            $_params['password'] = \Cataleya\Helper\Validator::password($_params['password']);
            if ($_params['password'] === FALSE) throw new Error ('Invalid password!');

            $_params['confirm_password'] = \Cataleya\Helper\Validator::password($_params['confirm_password']);
            if ($_params['confirm_password'] === FALSE || $_params['confirm_password'] !== $_params['confirm_password']) 
                throw new Error ('New passwords do not match! After entering the new password, make sure you type it again the exact same way you did the first time.');
        }  


        // Confirm account update (and require password)
        $message = 'To continue please confirm your password...';
        $this->confirm($message, TRUE);


        $_Customer = \Cataleya\Customer::load($_params['target_id']);
        if ($_Customer === NULL) throw new Error ('Customer not found!');
                    
        // [1] Profile 
        $_Customer
        ->setFirstname($_params['fname'])
        ->setLastname($_params['lname'])
        
        ->setEmailAddress($_params['email'], FALSE)
        ->setTelephone($_params['phone']);
        
        // [2] Set password ??
        if ($_params['set_password'] === TRUE) $_Customer->setPassword($_params['password']);

        
        
        $json_reply['message'] = 'Customer info saved.'; 
        $json_reply['Profile'] = \Cataleya\Front\Dashboard\Juicer::juice($_Customer);


        return $json_reply;
    }










    /**
     * getInfo
     *
     * @return array
     */
    protected function getInfo () {

        $json_reply = [];
        $_params = $this->digestParams(['target_id' => 'int']);
        $_Customer = \Cataleya\Customer::load($_params['target_id']);
        if ($_Customer === NULL) throw new Error ('Customer not found!');
        
        $json_reply['message'] = 'Customer profile.';
        $json_reply['Profile'] = \Cataleya\Front\Dashboard\Juicer::juice($_Customer);


        return $json_reply;
    }




        

}


