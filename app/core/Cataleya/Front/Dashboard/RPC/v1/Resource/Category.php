<?php



namespace Cataleya\Front\Dashboard\RPC\v1\Resource;
use \Cataleya\Error;


/*
 *
 * 	@Package: Cataleya
 * 	@Class: \Cataleya\Front\Dashboard\RPC\v1\Resource\Category
 *
 * 	(c) 2014 Fancy Paper Planes
 *
 *
 *
 */

class Category
extends \Cataleya\Front\Dashboard\RPC\v1\Resource\Main
{




    public function __construct() {



    }



    /*
     *
     *
     * CALLBACKS
     *
     */











    /*
     *
     * [ newCategory ]
     * ______________________________________________________
     *
     * @param: int $_POST['target_id']
     * @param: int $_POST['data']
     *
     */


    protected function newCategory () {

        $json_reply = [];
        $_params = $this->digestParams([
            'target_id'=>'int',
            'title' => 'string',
            'description' => 'html',
            'store_id' => 'int'
         ]);


        $_Store = \Cataleya\Store::load($_params['store_id']);
        if ($_Store === NULL ) _catch_error('Store could not be found.', __LINE__, true);


        $_Category = \Cataleya\Catalog\Tag::create($_Store, $_params['title'], $_params['description'], 'EN');


        $json_reply['message'] = 'New category saved.';
        $json_reply['Category'] = \Cataleya\Front\Dashboard\Juicer::juiceDrop($_Category);

        $json_reply['Parents'] = [];

        $_id = (int)$_Category->getID();

        foreach (\Cataleya\Catalog\Tags::load() as $_Cat)
        {
            if ((int)$_Cat->getID() === $_id) continue;

            $_c = \Cataleya\Front\Dashboard\Juicer::juiceDrop($_Cat);
            $_c['is_parent'] = $_Category->isParent($_Cat);
            if ($_Cat->isRoot()) $_c['title'] .= ' (Shop Root)';
            $json_reply['Parents'][] = $_c;
        }

        return $json_reply;

    }




    /*
     *
     * [ saveCategory ]
     * ______________________________________________________
     *
     * @param: int $_POST['target_id']
     * @param: int $_POST['data']
     *
     */


    protected function saveCategory () {

        $json_reply = [];
        $_params = $this->digestParams([
            'target_id'=>'int',
            'title' => 'plain_text',
            'description' => 'html',
            'sort_order' => 'float',
            'parent_id' => 'int',
            'is_active' => 'boolean'
        ]);
        $_Category = \Cataleya\Catalog\Tag::load($_params['target_id']);
        if ($_Category === NULL ) _catch_error('Category could not be found.', __LINE__, true);

        $_Parent = \Cataleya\Catalog\Tag::load($_params['parent_id']);
        if ($_Parent === NULL ) _catch_error('Parent category could not be found.', __LINE__, true);

        // [1] Category Description
        $_Category->getDescription()->setTitle($_params['title'], 'EN');
        $_Category->getDescription()->setText($_params['description'], 'EN');

        $_Category->getURLRewrite()->setKeyword($_params['title']);

        // [2] Enable  / Disable
        if ($_params['is_active']) $_Category->enable ();
        else $_Category->disable ();

        // [3] Parent
        $_Category->setParent($_Parent);

        // [4] Sort Order
        $_Category->setSortOrder($_params['sort_order']);


        $json_reply['message'] = 'New category saved.';
        $json_reply['Category'] = \Cataleya\Front\Dashboard\Juicer::juiceDrop($_Category);

        return $json_reply;

    }






    /*
     *
     * [ saveBanner ]
     * ______________________________________________________
     *
     * @param: int $_POST['target_id']
     * @param: int $_POST['data']
     *
     */


    protected function saveBanner () {

        $json_reply = [];

        $_params = $this->digestParams([
            'target_id' => 'int',
            'image_id'=>'integer',
            'rotate'=>'float',
            'sw'=>'float',
            'sh'=>'float',
            'sx'=>'float',
            'sy'=>'float'
        ]);


        $new_image = \Cataleya\Asset\Image::load($_params['image_id']);
        if ((int)$new_image->getID() === 0) _catch_error('Image not found.' . $_params['image_id'], __LINE__, true);


        $_Category = \Cataleya\Catalog\Tag::load($_params['target_id']);
        if ($_Category === NULL )
        {
            $new_image->destroy();
            _catch_error('Category could not be found.', __LINE__, true);
        }



        $options = array(
                                        'rotate'=>$_params['rotate'],
                                        'sw'=>$_params['sw'],
                                        'sh'=>$_params['sh'],
                                        'sx'=>$_params['sx'],
                                        'sy'=>$_params['sy']
                                        );

        if (!$new_image->bake($options, FALSE)) _catch_error('Error saving image.', __LINE__, true);
        $new_image->makeLarge(1200);
        $new_image->makeThumb(200);
        $new_image->makeTiny(100);


       // attach image
        $_Category->setBannerImage($new_image);


        $json_reply['message'] = 'New banner saved.';
        $json_reply['Category'] = \Cataleya\Front\Dashboard\Juicer::juiceDrop($_Category);

        return $json_reply;

    }



    /*
     *
     * [ clearBanner ]
     * ______________________________________________________
     *
     * @param: int $_POST['target_id']
     * @param: int $_POST['data']
     *
     */



    protected function clearBanner () {

        $json_reply = [];

        $_params = $this->digestParams([ 'target_id' => 'int' ]);

        $_Category = \Cataleya\Catalog\Tag::load($_params['target_id']);
        if ($_Category === NULL )
        {
            _catch_error('Category could not be found.', __LINE__, true);
        }


       // clear image
        $_Category->unsetBannerImage($new_image);


        $json_reply['message'] = 'Banner removed.';
        $json_reply['Category'] = \Cataleya\Front\Dashboard\Juicer::juiceDrop($_Category);

        return $json_reply;

    }





    /*
     *
     * [ deleteCategory ]
     * ______________________________________________________
     *
     * @param: int $_POST['target_id']
     *
     *
     */


    protected function deleteCategory () {

        $json_reply = [];
        $_params = $this->digestParams(['target_id'=>'int']);

        $_Category = \Cataleya\Catalog\Tag::load($_params['target_id']);
        if ($_Category === NULL ) _catch_error('Category could not be found.', __LINE__, true);

        if ($_params['auto_confirm'] !== TRUE)
        {
            $message = 'Are you sure you want to delete this category: "' . $_Category->getDescription()->getTitle('EN') . '"?';
            $this->confirm($message, FALSE);
        }

        $_Category->delete();

        $json_reply['message'] = 'Category deleted.';
        $json_reply['Category'] = array ('id'=>$_params['target_id']);

        return $json_reply;

    }






    /*
     *
     * [ getInfo ]
     * ______________________________________________________
     *
     * @param: int $_POST['target_id']
     *
     *
     */


    protected function getInfo () {

        $json_reply = [];
        $_params = $this->digestParams(['target_id'=>'int']);

        $_Category = \Cataleya\Catalog\Tag::load($_params['target_id']);
        if ($_Category === NULL ) _catch_error('Category could not be found.', __LINE__, true);

        $json_reply['message'] = 'New category saved.';
        $json_reply['Category'] = \Cataleya\Front\Dashboard\Juicer::juiceDrop($_Category);

        $json_reply['Parents'] = [];

        $_id = (int)$_Category->getID();

        foreach (\Cataleya\Catalog\Tags::load() as $_Cat)
        {
            if ((int)$_Cat->getID() === $_id) continue;

            $_c = \Cataleya\Front\Dashboard\Juicer::juiceDrop($_Cat);
            $_c['is_parent'] = $_Category->isParent($_Cat);
            if ($_Cat->isRoot()) $_c['title'] .= ' (Shop Root)';
            $json_reply['Parents'][] = $_c;
        }
        return $json_reply;

    }






}
