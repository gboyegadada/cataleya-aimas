<?php



namespace Cataleya\Front\Dashboard\RPC\v1\Resource;
use \Cataleya\Error;


/*
 *
 * 	@Package: Cataleya
 * 	@Class: \Cataleya\Front\Dashboard\RPC\v1\Resource\Store
 *
 * 	(c) 2014 Fancy Paper Planes
 *
 *
 *
 */

class Store 
extends \Cataleya\Front\Dashboard\RPC\v1\Resource\Main 
{
    
    


    public function __construct() {

        
        
    }
    
    

    /*
     * 
     * 
     * CALLBACKS
     * 
     */










    /**
     * newStore
     *
     * @return void
     */
    protected function newStore () {

        global $json_reply;
        $_params = $this->digestParams([
            'target_id'=>'int', 
            'title' => 'text', 
            'description' => 'html'
        ]);
        

        $_title = (!empty($_params['title']) && is_string($_params['title'])) ? $_params['title'] : 'Untitled Store';
        $_description = (!empty($_params['description']) && is_string($_params['description'])) ? $_params['title'] : 'No Description';


        $_Country = \Cataleya\Geo\Country::load('NG');

        if ($_Country->hasProvinces()) 
        {
            foreach ($_Country as $_Province) 
            {
                $_Contact = \Cataleya\Asset\Contact::create($_Province);
                break;
            }
        } else {
            $_Contact = \Cataleya\Asset\Contact::create($_Country);
        }


        $_Store = \Cataleya\Store::create($_Contact, $_title, $_description, SHOPFRONT_LANGUAGE_CODE, 'NGN');


        

        // STORE PAGES
        \Cataleya\Front\Shop\Page::create($_Store, 'Contact', 'Contact...', $_Store->getLanguageCode(), TRUE);
        \Cataleya\Front\Shop\Page::create($_Store, 'About', 'About Us...', $_Store->getLanguageCode(), FALSE);
        \Cataleya\Front\Shop\Page::create($_Store, 'Terms', 'Term and Condidtions', $_Store->getLanguageCode(), TRUE);
        \Cataleya\Front\Shop\Page::create($_Store, 'Return Policies', 'Return Policies', $_Store->getLanguageCode(), TRUE);


        
        
        $json_reply['message'] = 'New Store saved.';
        $json_reply['Store'] = \Cataleya\Front\Dashboard\Juicer::juice($_Store);


        return $json_reply;
    }






    /**
     * deleteStore
     *
     * @return void
     */
    protected function deleteStore () {
        


        $json_reply = [];
        $_params = $this->digestParams(['target_id'=>'int']);
     

        
        
        /*
        * 
        * CHECK USER PRIVILEGES
        * 
        */

        $_admin_role = \Cataleya\Front\Dashboard\Controller::getUser()->getRole();
        if ((empty($_admin_role) || !$_admin_role->hasPrivilege('DELETE_STORES')) && !IS_SUPER_USER)  throw new Error ("You are not permitted to delete a store. Please contact your admin");

        
        
        $_Store = \Cataleya\Store::load($_params['target_id']);
        if ($_Store === NULL ) throw new Error ('Store could not be found.');
        

        // Confirm delete action (and require password)
        $message = 'To delete the \'' . $_Store->getTitleText() . '\' store please confirm your password...';
        $this->confirm($message, TRUE);
        

        \Cataleya\Helper\DBH::getInstance()->beginTransaction();
        $_Store->delete();
        \Cataleya\Helper\DBH::getInstance()->commit();
        
        $json_reply['message'] = 'Store deleted.';
        $json_reply['Store'] = array ('id'=>$_params['target_id']);


        return $json_reply;
    }






    /**
     * openStore
     *
     * @return void
     */
    protected function openStore () {

        $json_reply = [];
        $_params = $this->digestParams(['target_id'=>'int']);

        $_Store = \Cataleya\Store::load($_params['target_id']);
        if ($_Store === NULL ) throw new Error ('Store could not be found.');
        

        // Confirm delete action (and require password)
        $message = 'To open (enable) the \'' . $_Store->getTitleText() . '\' store please confirm your password...';
        $this->confirm($message, TRUE);
        

        $_Store->enable();
        
        $json_reply['message'] = $_Store->getTitleText() . ' is now ' . (($_Store->isActive()) ? 'open.' : 'closed.');
        $json_reply['Store'] = \Cataleya\Front\Dashboard\Juicer::juiceDrop($_Store);


        return $json_reply;
    }






    /**
     * closeStore
     *
     * @return void
     */
    protected function closeStore () {

        $json_reply = [];
        $_params = $this->digestParams(['target_id'=>'int']);
     
        $_Store = \Cataleya\Store::load($_params['target_id']);
        if ($_Store === NULL ) throw new Error ('Store could not be found.');
        

        // Confirm delete action (and require password)
        $message = 'To close (disable) the \'' . $_Store->getTitleText() . '\' store please confirm your password...';
        $this->confirm($message, TRUE);
        
        $_Store->disable();
        
        $json_reply['message'] = $_Store->getTitleText() . ' is now ' . (($_Store->isActive()) ? 'open.' : 'closed.');
        $json_reply['Store'] = \Cataleya\Front\Dashboard\Juicer::juiceDrop($_Store);


        return $json_reply;
    }










    /**
     * getStoreInfo
     *
     * @return void
     */
    protected function getStoreInfo () {

        $json_reply = [];
        $_params = $this->digestParams(['target_id'=>'int']);
     
        $_Store = \Cataleya\Front\Shop\Store::load((int)['target_id']);
        if ($_Store === NULL ) throw new Error ('Store could not be found.');
        
        
        $json_reply['message'] = 'Store info.';
        $json_reply['Store'] = \Cataleya\Front\Dashboard\Juicer::juice($_Store);
        


        return $json_reply;
    }












    /**
     * saveTitle
     *
     * @return void
     */
    protected function saveTitle () {

        $json_reply = [];
        $_params = $this->digestParams([
            'target_id'=>'int', 
            'text' => 'text'
        ]);
        
        $_params = $this->digestParams(['target_id'=>'int']);
        $_Store = \Cataleya\Store::load($_params['target_id']);
        if ($_Store === NULL ) throw new Error ('Store could not be found.');
        
        // [1] Store Description
        $_Store
                
        ->setTitle($_params['text']);
        
        
        // Update url_rewrite...
        //$_Store->setHandle($_params['text']);
        
        
        $json_reply['message'] = 'Title saved.';
        $json_reply['Store'] = \Cataleya\Front\Dashboard\Juicer::juiceDrop($_Store);


        return $json_reply;
    }






    /**
     * saveDescription
     *
     * @return void
     */
    protected function saveDescription () {


        $json_reply = [];
        $_params = $this->digestParams([
            'target_id'=>'int', 
            'text' => 'html'
        ]);

        $_Store = \Cataleya\Store::load($_params['target_id']);
        if ($_Store === NULL ) throw new Error ('Store could not be found.');
        
        // [1] Store Description
        $_Store
                
        ->setDescription($_params['text']);
        
        
        
        $json_reply['message'] = 'Description saved.';
        $json_reply['Store'] = \Cataleya\Front\Dashboard\Juicer::juiceDrop($_Store);


        return $json_reply;
    }









    /**
     * saveLanguage
     *
     * @return void
     */
    protected function saveLanguage () {


        $json_reply = [];
        $_params = $this->digestParams([
            'target_id'=>'int', 
            'text' => 'iso2'
        ]);

        $_Store = \Cataleya\Store::load($_params['target_id']);
        if ($_Store === NULL ) throw new Error ('Store could not be found.');
        
        
        $_Store->setLanguageCode($_params['text']);
        
        
        
        $json_reply['message'] = 'Store Language saved.';
        $json_reply['Store'] = \Cataleya\Front\Dashboard\Juicer::juiceDrop($_Store);


        return $json_reply;
    }










    /**
     * saveCurrency
     *
     * @return void
     */
    protected function saveCurrency () {


        $json_reply = [];        
        $_params = $this->digestParams([
            'target_id'=>'int', 
            'text' => 'iso3'
        ]);

        $_Store = \Cataleya\Store::load($_params['target_id']);
        if ($_Store === NULL ) throw new Error ('Store could not be found.');
        
        
        $_Store->setCurrencyCode($_params['text']);
        
        
        
        $json_reply['message'] = 'Store Currency saved.';
        $json_reply['Store'] = \Cataleya\Front\Dashboard\Juicer::juiceDrop($_Store);


        return $json_reply;
    }






    /**
     * addStaffMember
     *
     * @return void
     */
    protected function addStaffMember () {

        $json_reply = [];
        $_params = $this->digestParams([
            'target_id' => 'int', 
            'admin_id' => 'int'
        ]);
        
        
        $_Store = \Cataleya\Store::load($_params['target_id']);
        if ($_Store === NULL ) throw new Error ('Store could not be found.');
        
        $_AdminUser = \Cataleya\Admin\User::load($_params['admin_id']);
        if ($_AdminUser === NULL ) throw new Error ('Admin profile could not be found.');
        
        $_Staff = $_Store->getStaffMembers();
        $_Staff->addStaffMember($_AdminUser);
        
        $json_reply['message'] = 'Staff Members';
        $json_reply['payload'] = \Cataleya\Front\Dashboard\Juicer::juice($_Staff);
        $json_reply['StaffMember'] = \Cataleya\Front\Dashboard\Juicer::juice($_AdminUser);


        return $json_reply;
    }





    /**
     * removeStaffMember
     *
     * @return void
     */
    protected function removeStaffMember () {

        $json_reply = [];
        $_params = $this->digestParams([
            'target_id' => 'int', 
            'admin_id' => 'int'
        ]);
        
        $_Store = \Cataleya\Store::load($_CLEAN['target_id']);
        if ($_Store === NULL ) throw new Error ('Store could not be found.');
        
        $_AdminUser = \Cataleya\Admin\User::load($_params['admin_id']);
        if ($_AdminUser === NULL ) throw new Error ('Admin profile could not be found.');
        
        
        $message = 'Are you sure you want to remove this person from the list: "' . $_AdminUser->getName()  . ' (' . $_AdminUser->getRole()->getDescription()->getTitle('EN') . ')"?';
        $this->confirm($message, FALSE);
        
        
        $_Staff = $_Store->getStaffMembers();
        $_Staff->removeStaffMember($_AdminUser);
        
        $json_reply['message'] = 'Staff Members';
        $json_reply['payload'] = \Cataleya\Front\Dashboard\Juicer::juice($_Staff);
        $json_reply['StaffMember'] = array('id' => $_AdminUser->getID() );


        return $json_reply;
    }







    /**
     * getStaff
     *
     * @return void
     */
    protected function getStaff () {

        $json_reply = [];
        $_params = $this->digestParams([
            'target_id' => 'int' 
        ]);
        $_Store = \Cataleya\Store::load($_CLEAN['target_id']);
        if ($_Store === NULL ) throw new Error ('Store could not be found.');
        
        $json_reply['message'] = 'Staff Members';
        $json_reply['Staff'] = \Cataleya\Front\Dashboard\Juicer::juice($_Store->getStaffMembers());


        return $json_reply;
    }








    /**
     * disablePaymentProcessor
     *
     * @return void
     */
    protected function disablePaymentProcessor () {

        $json_reply = [];
        $_params = $this->digestParams([
            'store_id'=>'int',
            'payment_processor_id' => 'string' 
        ]);
     
        $_Store = \Cataleya\Store::load($_params['store_id']);
        if ($_Store === NULL ) throw new Error ('Store could not be found.');
        
        $_Store->removePaymentProcessor($_params['payment_processor_id']);
        

        
        $json_reply['message'] = 'Payment Processor removed.';
        $json_reply['PaymentProcessor'] = \Cataleya\Plugins\Package::getAppInfo($_params['payment_processor_id']);


        return $json_reply;
    }







    /**
     * enablePaymentProcessor
     *
     * @return void
     */
    protected function enablePaymentProcessor () {

        $json_reply = [];
        $_params = $this->digestParams([
            'store_id'=>'int', 
            'payment_processor_id' => 'string'
        ]);
     
        $_Store = \Cataleya\Store::load($_params['store_id']);
        if ($_Store === NULL ) throw new Error ('Store could not be found.');
        
        $_Store->addPaymentProcessor($_params['payment_processor_id']);


        
        $json_reply['message'] = 'Payment Type added.';
        $json_reply['PaymentProcessor'] = \Cataleya\Plugins\Package::getAppInfo($_params['payment_processor_id']);


        return $json_reply;
    }








    /**
     * saveTheme
     *
     * @return void
     */
    protected function saveTheme () {

        $json_reply = [];
        
        $_params = $this->digestParams([
            'target_id'=>'int', 
            'theme_id' => 'string'
        ]);
        $_Store = \Cataleya\Store::load($_params['target_id']);
        if ($_Store === NULL ) throw new Error ('Store could not be found.');
        
        $_Theme = \Cataleya\Front\Shop\Theme::load($_params['theme_id']);
        if ($_Theme === NULL ) throw new Error ('Theme could not be found.');
        
        
        // [1] Store Description
        $_Store->setTheme($_Theme);
        
        
        
        $json_reply['message'] = 'Template saved.';
        $json_reply['Store'] = \Cataleya\Front\Dashboard\Juicer::juiceDrop($_Store);


        return $json_reply;
    }








    /**
     * saveLabelColor
     *
     * @return void
     */
    protected function saveLabelColor () {

        $json_reply = [];
        
        $_params = $this->digestParams([
            'target_id'=>'int', 
            'label_color' => 'alpha'
        ]);
        $_Store = \Cataleya\Store::load($_params['target_id']);
        if ($_Store === NULL ) throw new Error ('Store could not be found.');
        

        // [1] Store Description
        $_Store->setLabelColor($_params['label_color']);
        
        
        
        $json_reply['message'] = 'Label color saved.';
        $json_reply['Store'] = \Cataleya\Front\Dashboard\Juicer::juiceDrop($_Store);


        return $json_reply;
    }






    /**
     * saveHandle
     *
     * @return void
     */
    protected function saveHandle () {

        $json_reply = [];        
        $_params = $this->digestParams([
            'target_id'=>'int', 
            'shop_handle' => 'string'
        ]);

        $_Store = \Cataleya\Store::load($_params['target_id']);
        if ($_Store === NULL ) throw new Error ('Store could not be found.');
        

        // [1] Store Description
        $_Store->setHandle($_params['shop_handle']);
        
        
        
        $json_reply['message'] = 'Shop handle saved.';
        $json_reply['Store'] = \Cataleya\Front\Dashboard\Juicer::juiceDrop($_Store);


        return $json_reply;
    }







    /**
     * saveDisplayImage
     *
     * @return void
     */
    protected function saveDisplayImage () {

        $json_reply = [];        
        $_params = $this->digestParams([
            'target_id'=>'int', 
            'image_id' => 'int', 
            'rotate' => 'angle', 
            'sw' => 'float', 
            'sh' => 'float', 
            'sx' => 'float',
            'sy' => 'float'
        ]);
        

        $new_image = \Cataleya\Asset\Image::load($_params['image_id']);
        if ((int)$new_image->getID() === 0) throw new Error ('Image not found.' . $_params['image_id']);
        
        
        

        $options = array(
                        'rotate'=>$_params['rotate'], 
                        'sw'=>$_params['sw'], 
                        'sh'=>$_params['sh'], 
                        'sx'=>$_params['sx'], 
                        'sy'=>$_params['sy']
                        ); 
        
        if (!$new_image->bake($options, TRUE)) 
        {
            $new_image->destroy();
            throw new Error ('Error saving image.');
        }
        

        $new_image->makeLarge(300);
        $new_image->makeThumb(160);
        $new_image->makeTiny(80);
        

        $_Store = \Cataleya\Store::load($_params['target_id']);
        if ($_Store === NULL ) { 
                $new_image->destroy();
                throw new Error ('Error saving image: Store could not be found.');
        }
        else { 
            $_Store->setDisplayPicture($new_image);
                
            
        }
        
        
        
        
        $json_reply['message'] = 'Image saved.';
        $json_reply['Image'] = array (
                                        'id' => $new_image->getID(), 
                                        'hrefs' => $new_image->getHrefs()
                                       );


        return $json_reply;
    }







    /**
     * saveStoreHours
     *
     * @return void
     */
    protected function saveStoreHours () {


        $json_reply = [];
        $_params = $this->digestParams([
            'target_id'=>'int', 
            'text' => 'string'
        ]);

        $_Store = \Cataleya\Store::load($_params['target_id']);
        if ($_Store === NULL ) throw new Error ('Store could not be found.');
        
        
        $_Store->setHours($_params['text']);
        
        
        
        $json_reply['message'] = 'Store Hours saved.';
        $json_reply['Store'] = \Cataleya\Front\Dashboard\Juicer::juiceDrop($_Store);


        return $json_reply;
    }







    /**
     * saveTimeZone
     *
     * @return void
     */
    protected function saveTimeZone () {


        $json_reply = [];
        $_params = $this->digestParams([
            'target_id'=>'int', 
            'text' => 'timezone'
        ]);

        $_Store = \Cataleya\Store::load($_params['target_id']);
        if ($_Store === NULL ) throw new Error ('Store could not be found.');
        
        $_Store->setTimezone(new \DateTimeZone($_params['text']));
        
        
        
        $json_reply['message'] = 'Store Language saved.';
        $json_reply['Store'] = \Cataleya\Front\Dashboard\Juicer::juiceDrop($_Store);


        return $json_reply;
    }






    /**
     * saveContactName
     *
     * @return void
     */
    protected function saveContactName () {


        $json_reply = [];
        $_params = $this->digestParams([
            'target_id'=>'int', 
            'text' => 'name'
        ]);

        $_Store = \Cataleya\Store::load($_params['target_id']);
        if ($_Store === NULL ) throw new Error ('Store could not be found.');
        
        
        $_Store->setContactName($_params['text']);
        
        
        $json_reply['message'] = 'Contact Name saved.';
        $json_reply['Store'] = \Cataleya\Front\Dashboard\Juicer::juiceDrop($_Store);


        return $json_reply;
    }






    /**
     * saveContactPhone
     *
     * @return void
     */
    protected function saveContactPhone () {


        $json_reply = [];
        
        $_params = $this->digestParams([
            'target_id'=>'int', 
            'text' => 'phone'
        ]);
        $_Store = \Cataleya\Store::load($_params['target_id']);
        if ($_Store === NULL ) throw new Error ('Store could not be found.');
        
        
        $_Store->setTelephone($_params['text']);
        
        
        
        $json_reply['message'] = 'Contact Phone Number saved.';
        $json_reply['Store'] = \Cataleya\Front\Dashboard\Juicer::juiceDrop($_Store);


        return $json_reply;
    }






    /**
     * saveContactEmail
     *
     * @return void
     */
    protected function saveContactEmail () {


        $json_reply = [];
        $_params = $this->digestParams([
            'target_id'=>'int', 
            'text' => 'email'
        ]);

        $_Store = \Cataleya\Store::load($_params['target_id']);
        if ($_Store === NULL ) throw new Error ('Store could not be found.');
        
        
        $_Store->setEmail($_params['text']);
        
        
        
        $json_reply['message'] = 'Contact Email saved.';
        $json_reply['Store'] = \Cataleya\Front\Dashboard\Juicer::juiceDrop($_Store);


        return $json_reply;
    }







    /**
     * saveContactStreetAddress
     *
     * @return void
     */
    protected function saveContactStreetAddress () {


        $json_reply = [];
        $_params = $this->digestParams([
            'target_id'=>'int',
            'text' => 'address'
        ]);

        $_Store = \Cataleya\Store::load($_params['target_id']);
        if ($_Store === NULL ) throw new Error ('Store could not be found.');
        
        
        $_Store->getDefaultContact()->setEntryStreetAddress($_params['text']);
        
        
        
        $json_reply['message'] = 'Contact Address saved.';
        $json_reply['Store'] = \Cataleya\Front\Dashboard\Juicer::juiceDrop($_Store);


        return $json_reply;
    }






    /**
     * saveContactStreetAddress2
     *
     * @return void
     */
    protected function saveContactStreetAddress2 () {


        $json_reply = [];
        
        $_params = $this->digestParams([
            'target_id'=>'int',
            'text' => 'name'
        ]);

        $_Store = \Cataleya\Store::load($_params['target_id']);
        if ($_Store === NULL ) throw new Error ('Store could not be found.');
        
        
        $_Store->getDefaultContact()->setEntryStreetAddress2($_params['text']);
        
        
        
        $json_reply['message'] = 'Contact Address (2) saved.';
        $json_reply['Store'] = \Cataleya\Front\Dashboard\Juicer::juiceDrop($_Store);


        return $json_reply;
    }







    /**
     * saveContactCity
     *
     * @return void
     */
    protected function saveContactCity () {


        $json_reply = [];
        
        $_params = $this->digestParams([
            'target_id'=>'int',
            'text' => 'name'
        ]);

        $_Store = \Cataleya\Store::load($_params['target_id']);
        if ($_Store === NULL ) throw new Error ('Store could not be found.');
        
        
        $_Store->getDefaultContact()->setEntryCity($_params['text']);
        
        
        
        $json_reply['message'] = 'Contact Address City saved.';
        $json_reply['Store'] = \Cataleya\Front\Dashboard\Juicer::juiceDrop($_Store);


        return $json_reply;
    }








    /**
     * saveContactState
     *
     * @return void
     */
    protected function saveContactState () {


        $json_reply = [];
        $_params = $this->digestParams([
            'target_id'=>'int',
            'text' => 'name'
        ]);

        $_Store = \Cataleya\Store::load($_params['target_id']);
        if ($_Store === NULL ) throw new Error ('Store could not be found.');
        
        
        $_Store->getDefaultContact()->setEntryState($_params['text']);
        
        
        
        $json_reply['message'] = 'Contact Address State saved.';
        $json_reply['Store'] = \Cataleya\Front\Dashboard\Juicer::juiceDrop($_Store);


        return $json_reply;
    }








    /**
     * saveContactCountry
     *
     * @return void
     */
    protected function saveContactCountry () {


        $json_reply = [];
        
        $_params = $this->digestParams([
            'target_id'=>'int',
            'text' => 'iso2'
        ]);

        $_Store = \Cataleya\Store::load($_params['target_id']);
        if ($_Store === NULL ) throw new Error ('Store could not be found.');
        
        
        $_Store->getDefaultContact()->setEntryCountryCode($_params['text']);
        
        
        
        $json_reply['message'] = 'Contact Address Country saved.';
        $json_reply['Store'] = \Cataleya\Front\Dashboard\Juicer::juiceDrop($_Store);


        return $json_reply;
    }







    /**
     * saveContactPostCode
     *
     * @return void
     */
    protected function saveContactPostCode () {


        $json_reply = [];
        
        $_params = $this->digestParams([
            'target_id'=>'int',
            'text' => 'alphanum'
        ]);

        $_Store = \Cataleya\Store::load($_params['target_id']);
        if ($_Store === NULL ) throw new Error ('Store could not be found.');
        
        
        $_Store->getDefaultContact()->setEntryPostcode($_params['text']);
        
        
        
        $json_reply['message'] = 'Contact Address Post Code saved.';
        $json_reply['Store'] = \Cataleya\Front\Dashboard\Juicer::juiceDrop($_Store);


        return $json_reply;
    }






      
    

        

}

