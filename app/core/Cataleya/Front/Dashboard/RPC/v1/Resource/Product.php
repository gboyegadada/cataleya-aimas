<?php



namespace Cataleya\Front\Dashboard\RPC\v1\Resource;
use \Cataleya\Error;


/*
 *
 * 	@Package: Cataleya
 * 	@Class: \Cataleya\Front\Dashboard\RPC\v1\Resource\Product
 *
 * 	(c) 2014 Fancy Paper Planes
 *
 *
 *
 */

class Product 
extends \Cataleya\Front\Dashboard\RPC\v1\Resource\Main 
{
    
    


    public function __construct() {

        
        
    }
    
    

    /*
     * 
     * 
     * CALLBACKS
     * 
     */





    /**
     * newProduct
     *
     * @return void
     */
    protected function newProduct () {

        $json_reply = [];
        
        
        $_ProductType = \Cataleya\Catalog\Product\Type::load($_params['product_type_id']);
        if ($_ProductType === NULL ) throw new Error ('Product type could not be found.');
        
        \Cataleya\Helper\DBH::getInstance()->beginTransaction();
        $_Product = \Cataleya\Catalog\Product::create($_ProductType, $_params['title'], $_params['description'], 'EN');
        \Cataleya\Helper\DBH::getInstance()->commit();
        
        $json_reply['message'] = 'New product saved.';
        $json_reply['Product'] = \Cataleya\Front\Dashboard\Juicer::juice($_Product);


        return $json_reply;
    }





    /**
     * newProductOption
     *
     * @return void
     */
    protected function newProductOption () {

        $json_reply = [];
     
        $_params = $this->digestParams([
             'product_id' => 'int'
        ]);
        $_Product = \Cataleya\Catalog\Product::load($_params['product_id']);
        if ($_Product === NULL ) throw new Error ('Product could not be found.');
        
        $_ProductOption = \Cataleya\Catalog\Product\Option::create ($_Product);
        
        $json_reply['message'] = 'Product option added.';
        $json_reply['ProductOption'] = \Cataleya\Front\Dashboard\Juicer::juice($_ProductOption);
        $json_reply['ProductOption']['isNew'] = true;


        return $json_reply;
    }







    /**
     * saveTitle
     *
     * @return void
     */
    protected function saveTitle () {

        $json_reply = [];
        
        $_params = $this->digestParams([
             'target_id' => 'int', 
             'text' => 'text'
        ]);
        $_Product = \Cataleya\Catalog\Product::load($_params['target_id']);
        if ($_Product === NULL ) throw new Error ('Product could not be found.');
        
        // [1] Product Description
        $_Product->getDescription()->setTitle($_params['text'], 'EN');
        
        // Update meta_keywords
        $_Product->updateMetaKeywords()

        // Update full_text_index
        ->updateFullTextIndex()

        // Update url_rewrite...
        ->setHandle($_params['text']);
        
        
        $json_reply['message'] = 'Title saved.';
        $json_reply['Product'] = \Cataleya\Front\Dashboard\Juicer::juiceDrop($_Product);


        return $json_reply;
    }






    /**
     * saveProductDetails
     *
     * @return void
     */
    protected function saveProductDetails () {

        $json_reply = [];
        
        $_params = $this->digestParams([
             'target_id' => 'int',                
             'text' => 'text'
        ]);
        $_Product = \Cataleya\Catalog\Product::load($_params['target_id']);
        if ($_Product === NULL ) throw new Error ('Product could not be found.');
        
        // [1] Product Description
        $_Product->getDescription()->setText($_params['text'], 'EN');
        
        
        $json_reply['message'] = 'Details saved.';
        $json_reply['Product'] = \Cataleya\Front\Dashboard\Juicer::juiceDrop($_Product);


        return $json_reply;
    }








    /**
     * saveDescription
     *
     * @return void
     */
    protected function saveDescription () {

        $json_reply = [];
        
        
        $_params = $this->digestParams([
             'target_id' => 'int', 
             'title' => 'text', 
             'description' => 'html'
        ]);
        $_Product = \Cataleya\Catalog\Product::load($_params['target_id']);
        if ($_Product === NULL ) throw new Error ('Product could not be found.');
        
        // [1] Product Description
        $_Description = $_Product->getDescription(); 
        
        if (is_string($_params['title'])) { $_Description->setTitle($_params['title'], 'EN'); }
        if (is_string($_params['title'])) { $_Description->setText($_params['description'], 'EN'); }
        
        
        // Update meta_keywords
        $_Product->updateMetaKeywords()

        // Update full_text_index
        ->updateFullTextIndex()

        // Update url_rewrite...
        ->setHandle($_params['title']);
        
        $json_reply['message'] = 'Details saved.';
        $json_reply['Product'] = \Cataleya\Front\Dashboard\Juicer::juiceDrop($_Product);


        return $json_reply;
    }





    /**
     * saveProductReferenceCode
     *
     * @return void
     */
    protected function saveProductReferenceCode () {

        $json_reply = [];
        
        
        $_params = $this->digestParams([
             'target_id' => 'int', 
             'text' => 'string'
        ]);
        $_Product = \Cataleya\Catalog\Product::load($_params['target_id']);
        if ($_Product === NULL ) throw new Error ('Product could not be found.');
        
        // [1] Product Description
        $_Product->setReferenceCode($_params['text']);
        
        
        $json_reply['message'] = 'Reference code saved.';
        // $json_reply['Product'] = \Cataleya\Front\Dashboard\Juicer::juiceDrop($_Product);


        return $json_reply;
    }







    /**
     * saveOptionSKU
     *
     * @return void
     */
    protected function saveOptionSKU () {

        $json_reply = [];
        
        
        $_params = $this->digestParams([
             'target_id' => 'int', 
             'text' => 'string'
        ]);
        $_Option = \Cataleya\Catalog\Product\Option::load($_params['target_id']);
        if ($_Option === NULL ) throw new Error ('Product option could not be found.');
        
        // [1] Product Option
        $_Option->setSKU($_params['text']);
        
        
        $json_reply['message'] = 'Option reference code saved.';
        // $json_reply['Product'] = \Cataleya\Front\Dashboard\Juicer::juiceDrop($_Product);


        return $json_reply;
    }





    /**
     * saveOptionUPC
     *
     * @return void
     */
    protected function saveOptionUPC () {

        $json_reply = [];
        
        
        $_params = $this->digestParams([
             'target_id' => 'int', 
             'text' => 'string'
        ]);
        
        $_Option = \Cataleya\Catalog\Product\Option::load($_params['target_id']);
        if ($_Option === NULL ) throw new Error ('Product option could not be found.');
        
        // [1] Product Option
        $_Option->setUPC($_params['text']);
        
        
        $json_reply['message'] = 'Option UPC code saved.';
        // $json_reply['Product'] = \Cataleya\Front\Dashboard\Juicer::juiceDrop($_Product);


        return $json_reply;
    }





    /**
     * saveOptionEAN
     *
     * @return void
     */
    protected function saveOptionEAN () {

        $json_reply = [];
        
        
        $_params = $this->digestParams([
             'target_id' => 'int', 
             'text' => 'string'
        ]);
        
        $_Option = \Cataleya\Catalog\Product\Option::load($_params['target_id']);
        if ($_Option === NULL ) throw new Error ('Product option could not be found.');
        
        // [1] Product Option
        $_Option->setEAN($_params['text']);
        
        
        $json_reply['message'] = 'Option EAN code saved.';
        // $json_reply['Product'] = \Cataleya\Front\Dashboard\Juicer::juiceDrop($_Product);


        return $json_reply;
    }





    /**
     * saveOptionISBN
     *
     * @return void
     */
    protected function saveOptionISBN () {

        $json_reply = [];
        
        $_params = $this->digestParams([
             'target_id' => 'int', 
             'text' => 'string'
        ]);
        $_Option = \Cataleya\Catalog\Product\Option::load($_params['target_id']);
        if ($_Option === NULL ) throw new Error ('Product option could not be found.');
        
        // [1] Product Option
        $_Option->setISBN($_params['text']);
        
        
        $json_reply['message'] = 'Option ISBN code saved.';
        // $json_reply['Product'] = \Cataleya\Front\Dashboard\Juicer::juiceDrop($_Product);


        return $json_reply;
    }







    /**
     * savePrice
     *
     * @return void
     */
    protected function savePrice () {

        $json_reply = [];
        
        $_params = $this->digestParams([
             'target_id' => 'int', 
             'store_id' => 'int', 
             'text' => 'float'
        ]);
        
        $_Price = \Cataleya\Catalog\Price::load($_params['target_id']);
        if ($_Price === NULL ) throw new Error ('Price could not be saved.');
        
        $_Store = \Cataleya\Store::load($_params['store_id']);
        if ($_Store === NULL ) throw new Error ('Store not found.');
        
        
        if (!$_Price->setValue($_Store, $_params['text'])) throw new Error ('Could not set price.');
        
        
        $json_reply['message'] = 'Product option price saved.';
        $json_reply['Price'] = array (
            'id' => $_Price->getID(), 
            'value' => $_Price->getValue($_Store), 
            'formatted_value' => number_format($_Price->getValue($_Store), 2), 
            'debug_text' => $_params['text']
        ); 


        return $json_reply;
    }






    /**
     * saveStock
     *
     * @return void
     */
    protected function saveStock () {

        $json_reply = [];
        
        $_params = $this->digestParams([
             'target_id' => 'int', 
             'store_id' => 'int', 
             'text' => 'float'
        ]);
        
        
        $_Stock = \Cataleya\Catalog\Product\Stock::load($_params['target_id']);
        if ($_Stock === NULL ) throw new Error ('Stock could not be saved.');
        
        $_Store = \Cataleya\Store::load($_params['store_id']);
        if ($_Store === NULL ) throw new Error ('Store not found.');
        
        
        $_Stock->setValue($_Store, $_params['text']);
        
        
        $json_reply['message'] = 'Product option stock saved.';
        $json_reply['Stock'] = array (
            'id' => $_Stock->getID(), 
            'value' => $_Stock->getValue($_Store)
        ); 


        return $json_reply;
    }





    /**
     * saveNewAttribute
     *
     * @return void
     */
    protected function saveNewAttribute () {

        $json_reply = [];
        
        $_params = $this->digestParams([
             'target_id' => 'int', 
             'option_id' => 'int', 
             'text' => 'text'
        ]);
        
        $_AttributeType = \Cataleya\Catalog\Product\Attribute\Type::load($_params['target_id']);
        if ($_AttributeType === NULL ) throw new Error ('Attribute Type could not be found.');
        if ( empty($_params['text']) ) throw new Error ('Please specify a ' . $_AttributeType->getName() . '.');
        
        // Create Attribute...
        $_Attribute = \Cataleya\Catalog\Product\Attribute::create($_AttributeType, $_params['text']);
        
        
        // ADD new Attribute to product option ?
        $_Option = (isset($_params['option_id']) && !empty($_params['option_id'])) 
            ? \Cataleya\Catalog\Product\Option::load (
                is_array($_params['option_id']) 
                ? $_params['option_id'][0] 
                : $_params['option_id']
            ) 
            : null;
        
        if ($_Option instanceof \Cataleya\Catalog\Product\Option) { 
            $_Option->addAttribute ($_Attribute);
            $json_reply['AddedToOption'] = true;
            $json_reply['ProductOption'] = \Cataleya\Front\Dashboard\Juicer::juice($_Option);
        } else {
            $json_reply['AddedToOption'] = false;
        }
        
        
        
        
        $json_reply['message'] = 'Option reference code saved.';
        $json_reply['Attribute'] = \Cataleya\Front\Dashboard\Juicer::juice($_Attribute);


        return $json_reply;
    }






    /**
     * addOptionAttribute
     *
     * @return void
     */
    protected function addOptionAttribute () {

        $json_reply = [];
        
        $_params = $this->digestParams([
             'target_id' => 'int', 
             'option_id' => 'int', 
             'text' => 'float'
        ]);
        
        $_Attribute = \Cataleya\Catalog\Product\Attribute::load($_params['target_id']);
        if ($_Attribute === NULL ) throw new Error ('Attribute could not be found.');
        
        
        // ADD new Attribute to product option ?
        $_Option = (isset($_params['option_id']) && !empty($_params['option_id'])) 
            ? \Cataleya\Catalog\Product\Option::load (
                is_array($_params['option_id']) 
                ? $_params['option_id'][0] 
                : $_params['option_id']
            ) 
            : null;
        
        if ($_Option instanceof \Cataleya\Catalog\Product\Option) { 
            $_Option->addAttribute ($_Attribute);
            $json_reply['ProductOption'] = \Cataleya\Front\Dashboard\Juicer::juice($_Option);
        } else {
            throw new Error ('Product option could not be found.');
        }
        
        
        $json_reply['message'] = $_Attribute->getAttributeType()->getName() . ' has been added.';


        return $json_reply;
    }










    /**
     * saveBaseImage
     *
     * @return void
     */
    protected function saveBaseImage () {

        $json_reply = [];
        
        $_params = $this->digestParams([
            'target_id' => 'int', 
            'image_id' => 'int', 
            'set_primary' => 'bool', 
            'rotate' => 'angle', 
            'sw' => 'float', 
            'sh' => 'float', 
            'sx' => 'float', 
            'sy' => 'float'
        ]);
        
        

        $new_image = \Cataleya\Asset\Image::load($_params['image_id']);
        if ((int)$new_image->getID() === 0) throw new Error ('Image not found.' . $_params['image_id']);
        
        
        $_Product = \Cataleya\Catalog\Product::load($_params['target_id']);
        if ($_Product === NULL ) 
        {
            $new_image->destroy();
            throw new Error ('Your photo could not be saved. Product not be found.');
        }
        
        $_images = $_Product->getBaseImages();
        if (count($_images) > 8) {
            $new_image->destroy();
            throw new Error ('Your photo could not be saved. You cannot upload more than 8 pictures for each product.');
        }
        


        $options = array(
                                        'rotate'=>$_params['rotate'], 
                                        'sw'=>$_params['sw'], 
                                        'sh'=>$_params['sh'], 
                                        'sx'=>$_params['sx'], 
                                        'sy'=>$_params['sy']
                                        ); 
        
        if (!$new_image->bake($options, TRUE)) 
        {
            $new_image->destroy();
            throw new Error ('Error saving image.');
        }
        
        
       // attach image
        $_Product->addBaseImage($new_image)->saveData();
        if ($_params['set_primary']) $_Product->setPrimaryImage($new_image);
        
        
        $json_reply['message'] = 'Image saved.';
        $json_reply['Product'] = \Cataleya\Front\Dashboard\Juicer::juiceDrop($_Product);
        $json_reply['Image'] = array (
                                        'id' => $new_image->getID(), 
                                        'hrefs' => $new_image->getHrefs(), 
                                        'is_primary' => ($_params['set_primary']) ? true : false
                                       );


        return $json_reply;
    }






    /**
     * setPrimaryBaseImage
     *
     * @return void
     */
    protected function setPrimaryBaseImage () {

        $json_reply = [];
        
        $_params = $this->digestParams([
             'target_id' => 'int', 
             'image_id' => 'int'
        ]);
        
        

        $image = \Cataleya\Asset\Image::load($_params['image_id']);
        if ((int)$image->getID() === 0) throw new Error ('Image not found.' . $_params['image_id']);
        
        
        $_Product = \Cataleya\Catalog\Product::load($_params['target_id']);
        
        // Set primary image...

        if ($_Product != NULL) $_Product->setPrimaryImage($image);
        else throw new Error ('Product not found.');



        $json_reply['message'] = 'Photo has been set as the primary product image!';
        $json_reply['product_id'] = $_params['target_id'];
        $json_reply['Image'] = array (
                                        'id' => $image->getID(), 
                                        'hrefs' => $image->getHrefs()
                                       );


        return $json_reply;
    }






    /**
     * removePrimaryBaseImage
     *
     * @return void
     */
    protected function removePrimaryBaseImage () {

        $json_reply = [];
        
        $_params = $this->digestParams([
             'target_id' => 'int'
        ]);
        
        

        
        $_Product = \Cataleya\Catalog\Product::load($_params['target_id']);
        
        // Delete image...

        if ($_Product != NULL) {
            $_PrimaryImage = $_Product->getPrimaryImage();
            $_Product->unsetPrimaryImage();
        }
        else throw new Error ('Product not found.');


        

        $json_reply['message'] = 'Photo has been removed!';
        $json_reply['product_id'] = $_params['target_id'];
        $json_reply['Image'] = array (
                                        'id' => $_params['image_id'],
                                       );
        $json_reply['PrimaryImage'] = array (
                                        'id' => $_PrimaryImage->getID(), 
                                        'hrefs' => $_PrimaryImage->getHrefs()
                                       );


        return $json_reply;
    }







    /**
     * deleteBaseImage
     *
     * @return void
     */
    protected function deleteBaseImage () {

        $json_reply = [];
        
        $_params = $this->digestParams([
            'target_id' => 'int', 
            'image_id' => 'int'
        ]);
        
        

        $image = \Cataleya\Asset\Image::load($_params['image_id']);
        if ((int)$image->getID() === 0) throw new Error ('Image not found.');
        
        
        $_Product = \Cataleya\Catalog\Product::load($_params['target_id']);
        
        // Delete image...

        if ($_Product != NULL) $_Product->removeBaseImage($image);
        else throw new Error ('Product not found.');


        // Now, it is quite possible that we have just deleted the product's default photo.
        // The system should automatically set a new one. To figure out what the new one is (and inform remote client), 
        // I will reload the Product instance...
        
        $_Product = NULL;
        $_Product = \Cataleya\Catalog\Product::load($_params['target_id']);
        
        $_PrimaryImage = $_Product->getPrimaryImage();

        $json_reply['message'] = 'Photo has been deleted!';
        $json_reply['product_id'] = $_params['target_id'];
        $json_reply['Image'] = array (
                                        'id' => $_params['image_id'],
                                       );
        $json_reply['PrimaryImage'] = array (
                                        'id' => $_PrimaryImage->getID(), 
                                        'hrefs' => $_PrimaryImage->getHrefs()
                                       );


        return $json_reply;
    }















    /**
     * deleteProduct
     *
     * @return void
     */
    protected function deleteProduct () {

        $json_reply = [];
        $_params = $this->digestParams([
             'target_id' => 'int'
        ]);
        
     
        $_Product = \Cataleya\Catalog\Product::load($_params['target_id']);
        if ($_Product === NULL ) throw new Error ('Product could not be found.');
        
        if ($_params['auto_confirm'] !== TRUE) 
        {
            $message = 'Are you sure you want to delete this product: "' . $_Product->getDescription()->getTitle('EN') . '"?';
            $this->confirm($message, FALSE);
        }
        

        \Cataleya\Helper\DBH::getInstance()->beginTransaction();
        $_Product->delete();
        \Cataleya\Helper\DBH::getInstance()->commit();
        
        $json_reply['message'] = 'Product deleted.';
        $json_reply['Product'] = array ('id'=>$_params['target_id']);


        return $json_reply;
    }




    /**
     * deleteProductOption
     *
     * @return void
     */
    protected function deleteProductOption () {

        $json_reply = [];
        
        $_params = $this->digestParams([
             'option_id' => 'int'
        ]);
        
        $json_reply['ProductOptions'] = array ();
        
        

        foreach ($_params['option_id'] as $_option_id) {

            $_Option = \Cataleya\Catalog\Product\Option::load($_option_id);
            if ($_Option === NULL ) throw new Error ('One or more Product Options could not be found.');
        
        }   
        

        if ($_params['auto_confirm'] !== TRUE) 
        {
            $message = 'Are you sure you want to delete ' . ((count($_params['option_id']) > 1) ? 'these items' : 'this item') . ' ( ' . count($_params['option_id']) . ') ?';
            $this->confirm($message, FALSE);
        }
        
        

        foreach ($_params['option_id'] as $_option_id) {
            

            $_Option = \Cataleya\Catalog\Product\Option::load($_option_id);
            if ($_Option !== NULL ) { 
                $json_reply['ProductOptions'][] = array (
                    'id' => $_Option->getID(), 
                    'product_id' => $_Option->getProductId()
                        );
                $_Option->delete(); 
            }
            else throw new Error ('One or more Product Options could not be found.');
        
        }   
        
        
        
        $json_reply['message'] = 'Product options deleted.';


        return $json_reply;
    }







    /**
     * saveProductOptionImage
     *
     * @return void
     */
    protected function saveProductOptionImage () {

        $json_reply = [];
        
        $_params = $this->digestParams([
            'option_id' => 'int', 
            'image_id' => 'int', 
            'rotate' => 'angle', 
            'sw' => 'float', 
            'sh' => 'float', 
            'sx' => 'float', 
            'sy' => 'float'
        ]);
        
        

        $new_image = \Cataleya\Asset\Image::load($_params['image_id']);
        if ((int)$new_image->getID() === 0) throw new Error ('Image not found.' . $_params['image_id']);
        
        
        

        $options = array(
                                        'rotate'=>$_params['rotate'], 
                                        'sw'=>$_params['sw'], 
                                        'sh'=>$_params['sh'], 
                                        'sx'=>$_params['sx'], 
                                        'sy'=>$_params['sy']
                                        ); 
        
        if (!$new_image->bake($options, TRUE)) 
        {
            $new_image->destroy();
            throw new Error ('Error saving image.');
        }
        
        

        $_ProductOptions = array ();
        
        foreach ($_params['option_id'] as $_option_id) {
            

            $_Option = \Cataleya\Catalog\Product\Option::load($_option_id);
            
            if ($_Option === NULL ) { 
                $new_image->destroy();
                throw new Error ('One or more Product Options could not be found.');
            }
            else { 
                $_ProductOptions[] = $_Option;
            }
                
            
        }
        
        
        
        
        // attach image
        
        $_image_one_saved = false;
        
        foreach ($_ProductOptions as $_Option) {

                if (!$_image_one_saved) { 
                    $_Option->setPreviewImage($new_image);
                    $_image_one_saved = true;
                } else {
                    $_Option->setPreviewImage($new_image->duplicate());
                }
                
            
                $json_reply['ProductOptions'][] = array (
                    'id' => $_Option->getID(), 
                    'product_id' => $_Option->getProductId(), 
                    'Image' => $_Option->getPreviewImage()->getHrefs()
                        );
        
        }   
        
        
        
        
        $json_reply['message'] = 'Image saved.';
        $json_reply['Image'] = array (
                                        'id' => $new_image->getID(), 
                                        'hrefs' => $new_image->getHrefs()
                                       );


        return $json_reply;
    }










    /**
     * removeProductOptionImage
     *
     * @return void
     */
    protected function removeProductOptionImage () {

        $json_reply = [];
        
        $_params = $this->digestParams([
             'option_id' => 'int'
        ]);
        
        


        
        foreach ($_params['option_id'] as $_option_id) {
            

            $_Option = \Cataleya\Catalog\Product\Option::load($_option_id);
            
            if ($_Option === NULL ) { 
                
                throw new Error ('One or more Product Options could not be found.');
            }
            else { 

                $_Option->unsetPreviewImage();
                
            
                $json_reply['ProductOptions'][] = array (
                    'id' => $_Option->getID(), 
                    'product_id' => $_Option->getProductId()
                        );
            }
                
            
        }
        
        
        
        
        
        $json_reply['message'] = 'Images removed.';
        $json_reply['Image'] = array (
                                        'id' => 0, 
                                        'hrefs' => \Cataleya\Asset\Image::load()->getHrefs()
                                       );


        return $json_reply;
    }












    /**
     * savePricePlural
     *
     * @return void
     */
    protected function savePricePlural () {

        $json_reply = [];
        
        $_params = $this->digestParams([
            'option_id' => 'int', 
            'store_id' => 'int', 
            'text' => 'float'
        ]);
        
        $_ProductOptions = array ();
        
        foreach ($_params['option_id'] as $_option_id) {
            

            $_Option = \Cataleya\Catalog\Product\Option::load($_option_id);
            
            if ($_Option === NULL ) { 
                throw new Error ('One or more Product Options could not be found.');
            }
            else { 
                $_ProductOptions[] = $_Option;
            }
                
            
        }
        
        
        $_Store = \Cataleya\Store::load($_params['store_id']);
        if ($_Store === NULL ) throw new Error ('Store not found.');
        
        
        foreach ($_ProductOptions as $_Option) {

                $_Price = $_Option->getPrice();
                if (!$_Price->setValue($_Store, $_params['text'])) throw new Error ('Could not set price for one or more items.');
                
            
                $json_reply['Prices'][] = array (
                    'id' => $_Price->getID(), 
                    'store_id' => $_Store->getID(), 
                    'value' => $_Price->getValue($_Store), 
                    'formatted_value' => number_format($_Price->getValue($_Store), 2), 
                    'debug_text' => $_params['text']
                ); 
        
        }   
        
        $json_reply['message'] = 'Product option price saved.';


        return $json_reply;
    }






    /**
     * saveStockPlural
     *
     * @return void
     */
    protected function saveStockPlural () {

        $json_reply = [];
        
        $_params = $this->digestParams([
            'option_id' => 'int', 
            'store_id' => 'int', 
            'text' => 'float'
        ]);
        
        $_ProductOptions = array ();
        
        foreach ($_params['option_id'] as $_option_id) {
            

            $_Option = \Cataleya\Catalog\Product\Option::load($_option_id);
            
            if ($_Option === NULL ) { 
                throw new Error ('One or more Product Options could not be found.');
            }
            else { 
                $_ProductOptions[] = $_Option;
            }
                
            
        }
        
        
        $_Store = \Cataleya\Store::load($_params['store_id']);
        if ($_Store === NULL ) throw new Error ('Store not found.');

        foreach ($_ProductOptions as $_Option) {

                $_Stock = $_Option->getStock();
                if (!$_Stock->setValue($_Store, $_params['text'])) throw new Error ('Could not update stock for one or more items.');
                
            
                $json_reply['Stock'][] = array (
                    'id' => $_Stock->getID(), 
                    'store_id' => $_Store->getID(), 
                    'value' => $_Stock->getValue($_Store)
                ); 
        
        }   
        
        
        
        $json_reply['message'] = 'Product option stock saved.';


        return $json_reply;
    }













    /**
     * addToProductGroup
     *
     * @return void
     */
    protected function addToProductGroup () {

        $json_reply = [];
        
     
        $_params = $this->digestParams([
            'target_id' => 'int', 
            'product_id' => 'int'
        ]);
        $_Product = \Cataleya\Catalog\Product::load($_params['target_id']);
        if ($_Product === NULL ) throw new Error ('Product could not be found.');
        
        
        $_MemberProduct = \Cataleya\Catalog\Product::load($_params['product_id']);
        if ($_MemberProduct === NULL ) throw new Error ('Product could not be found.');
        
        if ($_Product->inProductGroup($_MemberProduct)) $json_reply['Duplicate'] = TRUE;
        else {
            $json_reply['Duplicate'] = FALSE;
            $_Product->addToProductGroup($_MemberProduct);
        }
        
        $json_reply['message'] = 'Item added.';
        $json_reply['Product'] = \Cataleya\Front\Dashboard\Juicer::juiceDrop($_Product);
        $json_reply['MemberProduct'] = \Cataleya\Front\Dashboard\Juicer::juiceDrop($_MemberProduct);


        return $json_reply;
    }








    /**
     * removeFromProductGroup
     *
     * @return void
     */
    protected function removeFromProductGroup () {

        $json_reply = [];
        
     
        $_params = $this->digestParams([
            'target_id' => 'int', 
            'product_id' => 'int'
        ]);
        $_Product = \Cataleya\Catalog\Product::load($_params['target_id']);
        if ($_Product === NULL ) throw new Error ('Product could not be found.');
        
        $_MemberProduct = \Cataleya\Catalog\Product::load($_params['product_id']);
        if ($_MemberProduct === NULL ) throw new Error ('Product could not be found.');
        
        $_Product->removeFromProductGroup($_MemberProduct);
        
        $json_reply['message'] = 'Item added.';
        $json_reply['Product'] = \Cataleya\Front\Dashboard\Juicer::juiceDrop($_Product);
        $json_reply['MemberProduct'] = \Cataleya\Front\Dashboard\Juicer::juiceDrop($_MemberProduct);


        return $json_reply;
    }











    /**
     * getProductInfo
     *
     * @return void
     */
    protected function getProductInfo () {

        $json_reply = [];
     
        $_params = $this->digestParams([
             'target_id' => 'int'
        ]);

        $_Product = \Cataleya\Catalog\Product::load($_params['target_id']);
        if ($_Product === NULL ) throw new Error ('Product could not be found.');
        
        $json_reply['message'] = 'Product info.';
        $json_reply['Product'] = \Cataleya\Front\Dashboard\Juicer::juice($_Product);


        return $json_reply;
    }





    /**
     * getProductOptionInfo
     *
     * @return void
     */
    protected function getProductOptionInfo () {

        $json_reply = [];
     
        $_params = $this->digestParams([
             'target_id' => 'int'
        ]);

        $_ProductOption = \Cataleya\Catalog\Product\Option::load($_params['target_id']);
        if ($_ProductOption === NULL ) throw new Error ('Product variant could not be found.');
        
        $json_reply['message'] = 'Product option info.';
        $json_reply['ProductOption'] = \Cataleya\Front\Dashboard\Juicer::juice($_ProductOption);
        $json_reply['ProductOption']['isNew'] = false;


        return $json_reply;
    }







    /**
     * search
     *
     * @return void
     */
    protected function search () {

        $json_reply = [];
     
        $_params = $this->digestParams([
             'text' => 'string'
        ]);
        
        
        // First, take out funny looking characters...
        $_params['text'] = preg_replace('/[^-\s\.A-Za-z0-9@*#+]+/', '', $_params['text']);


        // Load search results as a collection of objects...
        $search_results = \Cataleya\Catalog::search($_params['text'], array('show_hidden'=>TRUE));


        // $_item_num = $search_results->getPageStart();
        $json_reply['Products'] = array ();

        foreach ($search_results as $_Product) $json_reply['Products'][] = \Cataleya\Front\Dashboard\Juicer::juiceDrop($_Product);
        
        $json_reply['message'] = \Cataleya\Helper::countInEnglish($search_results->getPopulation(), 'Product', 'Products') . ' found.';
        $json_reply['count'] = $search_results->getPopulation();


        return $json_reply;
    }




}
