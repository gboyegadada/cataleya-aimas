<?php



namespace Cataleya\Front\Dashboard\RPC\v1\Resource;
use \Cataleya\Error;


/*
 *
 * 	@Package: Cataleya
 * 	@Class: \Cataleya\Front\Dashboard\RPC\v1\Resource\Store
 *
 * 	(c) 2014 Fancy Paper Planes
 *
 *
 *
 */

class Stats 
extends \Cataleya\Front\Dashboard\RPC\v1\Resource\Main 
{
    
    


    public function __construct() {

        
        
    }
    
    

    /*
     * 
     * 
     * CALLBACKS
     * 
     */










    /**
     * newStore
     *
     * @return void
     */
    protected function newStore () {

        global $json_reply;
        $_params = $this->digestParams([
            'target_id'=>'int', 
            'title' => 'text', 
            'description' => 'html'
        ]);
        

        $_title = (!empty($_params['title']) && is_string($_params['title'])) ? $_params['title'] : 'Untitled Store';
        $_description = (!empty($_params['description']) && is_string($_params['description'])) ? $_params['title'] : 'No Description';


        $_Country = \Cataleya\Geo\Country::load('NG');

        if ($_Country->hasProvinces()) 
        {
            foreach ($_Country as $_Province) 
            {
                $_Contact = \Cataleya\Asset\Contact::create($_Province);
                break;
            }
        } else {
            $_Contact = \Cataleya\Asset\Contact::create($_Country);
        }


        $_Store = \Cataleya\Store::create($_Contact, $_title, $_description, SHOPFRONT_LANGUAGE_CODE, 'NGN');


        

        // STORE PAGES
        \Cataleya\Front\Shop\Page::create($_Store, 'Contact', 'Contact...', $_Store->getLanguageCode(), TRUE);
        \Cataleya\Front\Shop\Page::create($_Store, 'About', 'About Us...', $_Store->getLanguageCode(), FALSE);
        \Cataleya\Front\Shop\Page::create($_Store, 'Terms', 'Term and Condidtions', $_Store->getLanguageCode(), TRUE);
        \Cataleya\Front\Shop\Page::create($_Store, 'Return Policies', 'Return Policies', $_Store->getLanguageCode(), TRUE);


        
        
        $json_reply['message'] = 'New Store saved.';
        $json_reply['Store'] = \Cataleya\Front\Dashboard\Juicer::juice($_Store);


        return $json_reply;
    }






    /**
     * deleteStore
     *
     * @return void
     */
    protected function deleteStore () {
        


        $json_reply = [];
        $_params = $this->digestParams(['target_id'=>'int']);
     

        
        
        /*
        * 
        * CHECK USER PRIVILEGES
        * 
        */

        $_admin_role = \Cataleya\Front\Dashboard\Controller::getUser()->getRole();
        if ((empty($_admin_role) || !$_admin_role->hasPrivilege('DELETE_STORES')) && !IS_SUPER_USER)  throw new Error ("You are not permitted to delete a store. Please contact your admin");

        
        
        $_Store = \Cataleya\Store::load($_params['target_id']);
        if ($_Store === NULL ) throw new Error ('Store could not be found.');
        

        // Confirm delete action (and require password)
        $message = 'To delete the \'' . $_Store->getTitleText() . '\' store please confirm your password...';
        $this->confirm($message, TRUE);
        

        \Cataleya\Helper\DBH::getInstance()->beginTransaction();
        $_Store->delete();
        \Cataleya\Helper\DBH::getInstance()->commit();
        
        $json_reply['message'] = 'Store deleted.';
        $json_reply['Store'] = array ('id'=>$_params['target_id']);


        return $json_reply;
    }









    private function digestGraphData (array $_chart_data) 
    {
        $_baked = array ('data'=>array(), 'labels'=>array());
        $_key = -1;
        
        
        if (isset($_chart_data['tally_views'])):
        
        $_key++;
        $_color = $this->pickColor ();

        $_baked['data'][$_key] = array (
            'key'   =>  $_key, 
            'color' => $_color, 
            'data' => $_chart_data['tally_views']
        );

        $_baked['labels'][$_key] = array (
            'key'   =>  $_key, 
            'label' => 'Views', 
            'color' => $_color, 
            'icon'  =>  'icon-star-4'
        );
        
        endif;
        
        
        
        
        if (isset($_chart_data['tally_wishlists'])):
            
        $_key++;
        $_color = $this->pickColor ();
        
        $_baked['data'][$_key] = array (
            'key'   =>  $_key, 
            'color' => $_color, 
            'data' => $_chart_data['tally_wishlists']
        );

        $_baked['labels'][$_key] = array (
            'key'   =>  $_key, 
            'label' => 'Wishlists', 
            'color' => $_color, 
            'icon'  =>  'icon-heart'
        );
            
        endif;
        
        
        
        if (isset($_chart_data['tally_orders'])):
            
        $_key++;
        $_color = $this->pickColor ();
        
        $_baked['data'][$_key] = array (
            'key'   =>  $_key, 
            'color' => $_color, 
            'data' => $_chart_data['tally_orders']
        );

        $_baked['labels'][$_key] = array (
            'key'   =>  $_key, 
            'label' => 'Orders', 
            'color' => $_color, 
            'icon'  =>  'icon-basket'
        );
        
        endif;
        
        
        
        if (isset($_chart_data['order_subtotal'])):
            
    //    $_key++;
    //    $_color = $this->pickColor ();
    //    
    //    $_baked['data'][$_key] = array (
    //        'key'   =>  $_key, 
    //        'color' => $_color, 
    //        'data' => $_chart_data['order_subtotal']
    //    );
    //
    //    $_baked['labels'][$_key] = array (
    //        'key'   =>  $_key, 
    //        'label' => 'Subtotals', 
    //        'color' => $_color, 
    //        'icon'  =>  'icon-basket'
    //    );
        
        endif;

        
        
        if (isset($_chart_data['tally_admin'])):
     
        $_key++;
        $_color = $this->pickColor ();
        
        $_baked['data'][$_key] = array (
            'key'   =>  $_key, 
            'color' => $_color, 
            'data' => $_chart_data['tally_admin']
        );

        $_baked['labels'][$_key] = array (
            'key'   =>  $_key, 
            'label' => 'Admin', 
            'color' => $_color, 
            'icon'  =>  'icon-user-4'
        );
        
        endif;

        
        
        if (isset($_chart_data['tally_customers'])):
            
        $_key++;
        $_color = $this->pickColor ();
        
        $_baked['data'][$_key] = array (
            'key'   =>  $_key, 
            'color' => $_color, 
            'data' => $_chart_data['tally_customers']
        );

        $_baked['labels'][$_key] = array (
            'key'   =>  $_key, 
            'label' => 'Customer', 
            'color' => $_color, 
            'icon'  =>  'icon-user-4'
        );
        
        endif;

        
        return $_baked;
    }





    private function pickColor () 
    {
        static $_COLORS = [
            '#660', '#046', '#281'
        ];
        static $_index = 0;
        
        $_color = $_COLORS[$_index];
        
        $_index = ($_index < (count($_COLORS)-1)) ? $_index+1 : 0;
        return $_color;
    }


    private function getInterval ($_val) 
    {

        static $_INTERVALS = array (
            'day' => Cataleya\Helper\Stats::INTERVAL_DAY, 
            'week' => Cataleya\Helper\Stats::INTERVAL_WEEK, 
            'month' => Cataleya\Helper\Stats::INTERVAL_MONTH, 
            'month-3d' => Cataleya\Helper\Stats::INTERVAL_MONTH_3D, 
            'year' => Cataleya\Helper\Stats::INTERVAL_YEAR
        );

        return isset($_INTERVAL[$_val]) ? $_INTERVAL[$_val] : null;


    }


    protected function getCatalogStats () {
        
        global $_CLEAN, $_payload, $_meta;
        $_data = digestParams();
        
        
        $_Store = (empty($_CLEAN['store_id'])) ? NULL : \Cataleya\Store::load((int)$_CLEAN['store_id'][0]);
        
        $_payload = $this->digestGraphData(\Cataleya\Helper\Stats::getCatalogStats($_Store, NULL, $_data['offset'], $_data['interval'], $_data['from_now']));
        $_meta = array (
           'title'  => (empty($_Store) ? 'All Stores' : $_Store->getDescription()->getTitle('EN')) 
        );
        

    }






    protected function getProductStats () {
        
        $_data = digestParams();
        
        $json_reply = [];
        $_params = $this->digestParams([
            'store_id'=>'int', 
            'product_id'=>'int',  
            'offset'=>'int', 
            'interval'=>'alpha',  
            'from_now'=>'boolean' 
        ]);
     

        
        $_Store = (empty($_CLEAN['store_id'])) ? NULL : \Cataleya\Store::load((int)$_CLEAN['store_id'][0]);
        $_Product = (empty($_CLEAN['product_id'])) ? NULL : \Cataleya\Catalog\Product::load((int)$_CLEAN['product_id'][0]);
        
        $json_reply['payload'] = $this->digestGraphData(\Cataleya\Helper\Stats::getCatalogStats($_Store, $_Product, $_data['offset'], $_data['interval'], $_data['from_now']));
        
        $_title = empty($_Product) ? 'All Products' : $_Product->getDescription()->getTitle('EN');
        $_title .= ' (' . (empty($_Store) ? 'All Stores' : $_Store->getDescription()->getTitle('EN')) . ')';
        
        $json_reply['meta'] = array (
           'title'  => $_title 
        );
        
        $json_reply['message'] = 'Graph Data.';

        return $json_reply;
    }






    protected function getShopStats () {
        
        $json_reply = [];
        $_params = $this->digestParams([
            'store_id'=>'int', 
            'offset'=>'int', 
            'interval'=>'alpha',  
            'from_now'=>'boolean' 
        ]);
     

        
        $_Store = \Cataleya\Store::load((int)$_params['store_id']);
        
        $json_reply['payload'] = $this->digestGraphData(
            \Cataleya\Helper\Stats::getCatalogStats(
                $_Store, null, $_params['offset'], 
                $_params['interval'], 
                $_params['from_now']
                )
            );
        
        $_title = 'All Products (' . $_Store->getDescription()->getTitle('EN') . ')';
        
        $json_reply['meta'] = array (
           'title'  => $_title 
        );
        
        $json_reply['message'] = 'Graph Data.';

        return $json_reply;
    }






    protected function getFailedLoginStats () {
        
        global $_payload, $_meta;
        $_data = digestParams();
        
        $_payload = $this->digestGraphData(\Cataleya\Helper\Stats::getFailedLoginStats($_data['offset'], $_data['interval'], $_data['from_now']));
        $_meta = array (
           'title'  => 'Failed Logins' 
        );
        

    }









      
    

        

}

