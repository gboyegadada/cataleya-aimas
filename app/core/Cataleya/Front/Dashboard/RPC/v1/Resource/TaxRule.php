<?php



namespace Cataleya\Front\Dashboard\RPC\v1\Resource;
use \Cataleya\Error;


/*
 *
 * 	@Package: Cataleya
 * 	@Class: \Cataleya\Front\Dashboard\RPC\v1\Resource\TaxRule
 *
 * 	(c) 2014 Fancy Paper Planes
 *
 *
 *
 */

class TaxRule 
extends \Cataleya\Front\Dashboard\RPC\v1\Resource\Main 
{
    
    


    public function __construct() {

        
        
    }
    
    

    /*
     * 
     * 
     * CALLBACKS
     * 
     */





    /**
     * newTaxRule
     *
     * @return array
     */
    protected function newTaxRule () {

        global $json_reply;
        $_params = $this->digestParams(['target_id'=>'int']);
        
        // Load Zone
        $_Zone = \Cataleya\Geo\Zone::load($_params['zone_id']);
        if ($_Zone === NULL) throw new Error('Zone could not be loaded.');
        
        $_TaxRule = \Cataleya\Tax\TaxRule::create($_Zone, $_params['type'], $_params['rate'], $_params['name']);
        
        
        $json_reply['message'] = 'New tax rule saved.';
        $json_reply['TaxRule'] =\Cataleya\Front\Dashboard\Juicer::juice($_TaxRule);


        return $json_reply;
    }



    /**
     * saveTaxRule
     *
     * @return array
     */
    protected function saveTaxRule () {

        $json_reply = [];
        $_params = $this->digestParams([
            'target_id'=>'int',
            'name' => 'name', 
            'rate' => 'percent', 
            'priority' => 'int',  
            'zone_id'=>'int', 
        ]);
        
        // Load Zone
        $_Zone = \Cataleya\Geo\Zone::load($_params['zone_id']);
        if ($_Zone === NULL) throw new Error('Zone could not be loaded.');
        
        $_TaxRule = \Cataleya\Tax\TaxRule::load($_params['target_id']);
        if ($_TaxRule === NULL ) throw new Error('TaxRule could not be found.');
        
        // [1] save
        $_TaxRule
        
        ->setRate($_params['rate'])
        ->setTaxPriority($_params['priority'])
        ->setTaxZone($_Zone)
        ->getDescription()
        ->setTitle($_params['name'], 'EN');
        

        
        
        $json_reply['message'] = 'New tax rule saved.';
        $json_reply['TaxRule'] =\Cataleya\Front\Dashboard\Juicer::juice($_TaxRule);


        return $json_reply;
    }








    /**
     * deleteTaxRule
     *
     * @return array
     */
    protected function deleteTaxRule () {

        $json_reply = [];
        $_params = $this->digestParams(['target_id'=>'int']);

        $_TaxRule = \Cataleya\Tax\TaxRule::load($_params['target_id']);
        if ($_TaxRule === NULL ) throw new Error('TaxRule could not be found.');

        $message = 'Are you sure you want to delete this tax rule: "' . $_TaxRule->getDescription()->getTitle('EN') . '"?';
        $this->confirm($message, false);
        
        $_TaxRule->delete();
        
        $json_reply['message'] = 'TaxRule deleted.';
        $json_reply['TaxRule'] = array ('id'=>$_params['target_id']);


        return $json_reply;
    }






    /**
     * getTaxRule
     *
     * @return array
     */
    protected function getTaxRule () {

        $json_reply = [];
        $_params = $this->digestParams(['target_id'=>'int']);
     
        $_TaxRule = \Cataleya\Tax\TaxRule::load($_params['target_id']);
        if ($_TaxRule === NULL ) throw new Error('TaxRule could not be found.');
        
        $json_reply['message'] = 'Tax rule.';
        $json_reply['TaxRule'] =\Cataleya\Front\Dashboard\Juicer::juice($_TaxRule);


        return $json_reply;
    }



      
    

        

}


