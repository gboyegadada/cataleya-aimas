<?php



namespace Cataleya\Front\Dashboard\RPC\v1\Resource;
use \Cataleya\Error;


/*
 *
 * 	@Package: Cataleya
 * 	@Class: \\Cataleya\Front\Dashboard\RPC\v1\Resource\App
 *
 * 	(c) 2014 Fancy Paper Planes
 *
 *
 * 	NOTE:   Difference between [ Dashboard\Controller\App ] and [ Dashboard\Controller\API\App ]
 *
 *          The --former-- either 
 *          1: renders specified app page (wrapped in Cataleya skin of cause) if 
 *          a GET request is made to it like this:
 *
 *              "http://shop.com/dashboard/app/myapp/welcome"
 *
 *          --OR-- 
 *
 *          2: executes API calls to our app and echos 
 *          a JSON response.
 *
 *          The --latter-- is part of the System's API conduit! And is used to get information about an app 
 *          or manipulate that app eg. delete it.
 *
 */

class App 
extends \Cataleya\Front\Dashboard\RPC\v1\Resource\Main 
{
    
    


    public function __construct() {

        
        
    }
    
    



    
    





    /*
     * 
     * 
     * CALLBACKS
     * 
     */



    /**
     * getAppInfo 
     *
     * @return void
     */
    protected function getAppInfo () {

        $json_reply = [];
        $_params = $this->digestParams(['app_handle'=>'string']);
        
        if (!\Cataleya\Plugins\Package::exists($_params['app_handle'])) throw new Error('App could not be found.');
        
        
        $json_reply['message'] = 'App info.';
        $json_reply['App'] = \Cataleya\Plugins\Package::getAppInfo($_params['app_handle']);


        return $json_reply;
    }







    /**
     * getPageHTML 
     *
     * @return void
     */
    protected function getPageHTML () {

        $_params = $this->digestParams(['app_handle'=>'string', 'page_id'=>'string']);
        if (!\Cataleya\Plugins\Package::exists($_params['app_handle'])) throw new Error('App could not be found.');
        
        
        return [
            'message' => 'App page render.', 
            'page' => [
                        'id' => $_params['page_id'], 
                        'html' => \Cataleya\Plugins\Action::trigger(
                            'dashboard-app.render', [ 'params' => $_params ], [ $_params['app_handle'] ]
                                    )
                        ]

                ];


    }









    
      
      
    

        

 
}


