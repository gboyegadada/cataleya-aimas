<?php



namespace Cataleya\Front\Dashboard\RPC\v1\Resource;
use \Cataleya\Error;


/*
 *
 * 	@Package: Cataleya
 * 	@Class: \Cataleya\Front\Dashboard\RPC\v1\Resource\Coupon
 *
 * 	(c) 2014 Fancy Paper Planes
 *
 *
 *
 */

class Coupon 
extends \Cataleya\Front\Dashboard\RPC\v1\Resource\Main 
{
    
    


    public function __construct() {

        
        
    }
    
    

    /*
     * 
     * 
     * CALLBACKS
     * 
     */






    /**
     * newCoupon
     *
     * @return array
     */
    protected function newCoupon () {

        $json_reply = [];
        $_params = $this->digestParams([
            'store_id' => 'int', 
            'code' => 'alphanum'
        ]);
        
        $_Store = \Cataleya\Store::load($_params['store_id']);
        if ($_Store === NULL ) throw new Error ('Store could not be found.');
        
        $_Coupon = \Cataleya\Sales\Coupon::create($_Store, $_params['code'], $_params);
        
        $json_reply['message'] = 'New coupon saved.';
        $json_reply['Coupon'] = \Cataleya\Front\Dashboard\Juicer::juice($_Coupon);


        return $json_reply;
    }




    /**
     * saveCoupon
     *
     * @return array
     */
    protected function saveCoupon () {

        $json_reply = [];        
        $_params = $this->digestParams([
            'target_id' => 'int',
            'store_id' => 'int',  
            'code' => 'alphanum', 

            'discount_type' => 'int', 
            'discount_amount' => 'float', 

            'start_day' => 'int', 
            'start_month' => 'int', 
            'start_year' => 'int', 

            'end_day' => 'int', 
            'end_month' => 'int', 
            'end_year' => 'int', 

            'valid_from_today' => 'boolean',
            'valid_until_never' => 'boolean', 
            'expires_after_uses' => 'int',
            'is_active' => 'boolean' 
        ]);


        /* ---------------  Additional validation --------------- */

        // [1] Discount Amount
        $_max = ($_params['discount_type'] === \Cataleya\Catalog\Price::TYPE_REDUCTION_PERCENT) ? 100 : NULL;
        $_params['discount_amount'] = \Cataleya\Helper\Validator::float($_params['discount_amount'], 0, $_max);
        if ($_params['discount_amount'] === FALSE) throw new Error ("Invalid discount amount."); // $_bad_params[] = 'discount_amount';

        // [2] Start date
        $_params['start_date'] = \Cataleya\Helper\Validator::date(
            $_params['start_year'].'-'.
            $_params['start_month'].'-'.
            $_params['start_day']
        );
        if ($_params['start_date'] === FALSE) throw new Error ("Invalid start date.");

        // [3] End date
        $_params['end_date'] = \Cataleya\Helper\Validator::date(
            $_params['end_year'].'-'.
            $_params['end_month'].'-'.
            $_params['end_day']
        );
        if ($_params['end_date'] === FALSE) throw new Error ("Invalid end date.");



        $_Coupon = \Cataleya\Sales\Coupon::load($_params['target_id']);
        if ($_Coupon === NULL ) throw new Error ('Coupon could not be found.');
        
        $_Store = \Cataleya\Store::load($_params['store_id']);
        if ($_Store === NULL ) throw new Error ('Store could not be found.');
        

        /* ----------------- Do your laundry --------------- */

        // [1] Coupon code
        $_Coupon->setCode($_params['code'])
                
        // [2] Store
        ->setStore($_Store)
        
        // [3] Discount Type
        ->setDiscountType($_params['discount_type'])
                
        // [4] Discount Amount
        ->setDiscountAmount($_params['discount_amount'])
        
        // [5] Start Date
        ->setStartDate(($_params['valid_from_today']) ? date('Y-m-d') : $_params['start_date'])
        
        // [6] End Date
        ->setEndDate(($_params['valid_until_never']) ? '0000-00-00' : $_params['end_date'])
        
        // [7] Expires After Uses
        ->setUsesPerCoupon($_params['expires_after_uses'], 1);
        
        // [8] Enable  / Disable
        if ($_params['is_active']) $_Coupon->enable ();
        else $_Coupon->disable ();
        
        
        
        $json_reply['message'] = 'Coupon saved.';
        $json_reply['Coupon'] = \Cataleya\Front\Dashboard\Juicer::juice($_Coupon);


        return $json_reply;
    }





    /**
     * deleteCoupon
     *
     * @return array
     */
    protected function deleteCoupon () {

        $json_reply = [];
        $_params = $this->digestParams(['target_id'=>'int']);
     
        $_Coupon = \Cataleya\Sales\Coupon::load($_params['target_id']);
        if ($_Coupon === NULL ) throw new Error ('Coupon could not be found.');
        
        $message = 'Are you sure you want to delete this coupon: "' . $_Coupon->getCode() . '" (' . $_Coupon->getStore()->getDescription()->getTitle('EN') .  ')?';
        $this->confirm($message, FALSE);
        
        $_Coupon->delete();
        
        
        $json_reply['message'] = 'Coupon deleted.';
        $json_reply['Coupon'] = array ('id'=>$_params['target_id']);


        return $json_reply;
    }






    /**
     * getInfo
     *
     * @return array
     */
    protected function getInfo () {

        $json_reply = [];
        $_params = $this->digestParams(['target_id'=>'int']);
     
        $_Coupon = \Cataleya\Sales\Coupon::load($_params['target_id']);
        if ($_Coupon === NULL ) throw new Error ('Coupon could not be found.');
        
        $json_reply['message'] = 'Coupon info.';
        $json_reply['Coupon'] = \Cataleya\Front\Dashboard\Juicer::juice($_Coupon);
        

        return $json_reply;
    }


      
    

        

}


