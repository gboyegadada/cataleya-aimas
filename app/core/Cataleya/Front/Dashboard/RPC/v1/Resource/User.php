<?php



namespace Cataleya\Front\Dashboard\RPC\v1\Resource;
use \Cataleya\Error;


/*
 *
 * 	@Package: Cataleya
 * 	@Class: \Cataleya\Front\Dashboard\RPC\v1\Resource\User
 *
 * 	(c) 2014 Fancy Paper Planes
 *
 *
 *
 */

class User 
extends \Cataleya\Front\Dashboard\RPC\v1\Resource\Main 
{
    
    


    public function __construct() {

        
        
    }
    
    

    /*
     * 
     * 
     * CALLBACKS
     * 
     */








      
    

    /**
     * saveProfile
     *
     * @return array
     */
    protected function saveProfile () {


        $json_reply = [];
        $_params = $this->digestParams([
            'target_id' => 'int', 
            'fname' => 'name', 
            'lname' => 'name',
            'email' => 'email', 
            'phone' => 'phone', 
            'set_password' => 'bool' 
            // 'password' => 'password', 
            // 'confirm_password' => 'password'
        ]);


        if ($_params['set_password'] === TRUE && isset($_params['password'], $_params['confirm_password']))
        {
            $_params['password'] = \Cataleya\Helper\Validator::password($_params['password']);
            if ($_params['password'] === FALSE) throw new Error ('Invalid password!');

            $_params['confirm_password'] = \Cataleya\Helper\Validator::password($_params['confirm_password']);
            if ($_params['confirm_password'] === FALSE || $_params['confirm_password'] !== $_params['confirm_password']) 
                throw new Error ('New passwords do not match! After entering the new password, make sure you type it again the exact same way you did the first time.');
        }  


        // Confirm account update (and require password)
        $message = 'To continue please confirm your password...';
        $this->confirm($message, TRUE);


        $_User = \Cataleya\Admin\User::load($_params['target_id']);
        if ($_User === NULL) throw new Error ('Profile not found!');
                    
        // [1] Profile 
        $_User
        ->setFirstname($_params['fname'])
        ->setLastname($_params['lname'])
        
        ->setEmailAddress($_params['email'], FALSE)
        ->setTelephone($_params['phone']);
        
        // [2] Set password ??
        if ($_params['set_password'] === TRUE) $_User->setPassword($_params['password']);

        
        
        $json_reply['message'] = 'User info saved.'; 
        $json_reply['Profile'] = \Cataleya\Front\Dashboard\Juicer::juice($_User);


        return $json_reply;
    }










    /**
     * getInfo
     *
     * @return array
     */
    protected function getInfo () {

        $json_reply = [];
        $_params = $this->digestParams(['target_id' => 'int']);
        $_User = \Cataleya\Admin\User::load($_params['target_id']);
        if ($_User === NULL) throw new Error ('Profile not found!');
        
        $json_reply['message'] = 'User profile.';
        $json_reply['Profile'] = \Cataleya\Front\Dashboard\Juicer::juice($_User);


        return $json_reply;
    }





    /**
     * saveDisplayPicture
     *
     * @return void
     */
    protected function saveDisplayPicture () {

        $json_reply = [];
        $_params = $this->digestParams([
            'image_id' => 'int', 
            'rotate' => 'angle', 
            'sw' => 'float', 
            'sh' => 'float', 
            'sx' => 'float',
            'sy' => 'float'
        ]);
        

        $new_image = Cataleya\Asset\Image::load($_params['image_id']);
        if ((int)$new_image->getID() === 0) throw new Error ('Image not found.' . $_params['image_id']);
        


        $options = array(
                                        'rotate'=>$_params['rotate'], 
                                        'sw'=>$_params['sw'], 
                                        'sh'=>$_params['sh'], 
                                        'sx'=>$_params['sx'], 
                                        'sy'=>$_params['sy']
                                        ); 
        
        if (!$new_image->bake($options, FALSE)) throw new Error ('Error saving image.');
        $new_image->makeLarge(200);
        $new_image->makeThumb(120);
        $new_image->makeTiny(60);   
        
        
       // attach image
        $_MyAccount->setDisplayPicture($new_image);
        
        
        $json_reply['message'] = 'Profile saved.';
        $json_reply['Profile'] = \Cataleya\Front\Dashboard\Juicer::juice($_MyAccount);


        return $json_reply;
    }








        

}

