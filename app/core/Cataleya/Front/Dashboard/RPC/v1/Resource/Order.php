<?php



namespace Cataleya\Front\Dashboard\RPC\v1\Resource;
use \Cataleya\Error;


/*
 *
 * 	@Package: Cataleya
 * 	@Class: \Cataleya\Front\Dashboard\RPC\v1\Resource\Order
 *
 * 	(c) 2014 Fancy Paper Planes
 *
 *
 *
 */

class Order 
extends \Cataleya\Front\Dashboard\RPC\v1\Resource\Main 
{
    
    


    public function __construct() {

        
        
    }
    
    

    /*
     * 
     * 
     * CALLBACKS
     * 
     */







    /**
     * getOrder
     *
     * @return array
     */
    protected function getOrder () {

        $json_reply = [];
        $_params = $this->digestParams(['target_id' => 'int']);

        
        $_Order = \Cataleya\Store\Order::load($_params['target_id']);
        if ($_Order === NULL ) throw new Error ('Order could not be loaded.');
        
        
        $json_reply['message'] = 'Order info';
        $json_reply['Order'] =\Cataleya\Front\Dashboard\Juicer::juice($_Order);


        return $json_reply;
    }





    /**
     * getReciepts
     *
     * @return array
     */
    protected function getReciepts () {

        $json_reply = [];
        $_params = $this->digestParams(['order_id' => 'int']);

        
        $_Order = \Cataleya\Store\Order::load($_params['order_id']);
        if ($_Order === NULL ) throw new Error ('Order could not be loaded.');

        $json_reply['reciepts'] = \Cataleya\Plugins\Action::trigger(
            'payment.get-transactions', 
            [ 'params' => ['order_id' => $_Order->getID()] ]
        );

        
        
        $json_reply['message'] = 'Payment reciepts.';


        return $json_reply;
    }





    /**
     * sendEmail
     *
     * @return array
     */
    protected function sendEmail () {

        $json_reply = [];
        $_params = $this->digestParams([
            'order_id' => 'int_array',
            'to' => 'email', 
            'subject' => 'text', 
            'body' => 'html'
        ]);
        
        $_User = \Cataleya\Front\Dashboard\Controller::getUser();
        $twig_vars = array (
            'header_title' => $_params['subject'], 
            'inline_title' => 'New Message', 
            'title_icon' => ((SSL_REQUIRED ? 'https://' : 'http://'). HOST . '/' . ROOT_URI) . 'ui/images/ui/notification-icons/email-50x50.png', 
            'body' => (preg_replace('![\n]!', '<br />', $_params['body']))
        );


        // Load template...
        $template = \Cataleya\Front\Twiggy::loadTemplate('admin-message.email.html.twig');
        $_Email = \Cataleya\Helper\Emailer::getInstance('order-details.mailer');
        
        
        $_Email
        ->setTo(is_array($_params['to']) ? [$_params['to'][0]] : [ $_params['to'] ]) // array('user@email.com', 'user2@email.com'=>'Bola Kalejaye');
        ->setFrom(array('no-reply@'.EMAIL_HOST => $_User->getName())) // array('no-reply@aimas.com.ng'=>'Aimas Notification')
        ->setSender('no-reply@'.EMAIL_HOST)
        ->setReplyTo($_User->getEmailAddress())
        ->setSubject ($_params['subject'])
        ->setHTMLBody($template->render($twig_vars))
        ->setTextBody($_params['body']);

        foreach ($_params['order_id'] as $_id) 
        { 
            $_Order = \Cataleya\Store\Order::load($_id);
            if ($_Order === NULL ) throw new Error ('One order could not be found.');
            
            $_Email->attach($_Order->makePDF());
        }
        
        if (is_array($_params['to']) && count($_params['to']) > 1) 
        {
            $_Email->setBcc(array_slice($_params['to'], 1));
        }
        
        
        if ($_Email->send() === 0) throw new Error ('Email could not be sent!');

        
        $json_reply['message'] = 'Email sent!';
        $json_reply['Orders'] = $_params['order_id'];


        return $json_reply;
    }









    /**
     * cancelOrder
     *
     * @return void
     */
    protected function cancelOrder () {
        $json_reply = [ 
            'Orders' => []
        ];
        $_params = $this->digestParams(['order_id' => 'int']);

        $_ids = is_array($_params['order_id']) ? $_params['order_id'] : [ $_params['order_id'] ];

        foreach ($_ids as $_id) {
            $_Order = \Cataleya\Store\Order::load($_id);

            if ($_Order === NULL ) throw new Error ('One or more orders could not be found.');
            else $_Order->setOrderStatus('cancelled');

            $json_reply['Orders'][] = [ 
                'order_id'=>$_id, 
                'status' => $_Order->getOrderStatus()
            ];
        }

        return $json_reply;
    }




    /**
     * setOrderStatus
     *
     * @return void
     */
    protected function setOrderStatus () {

        $json_reply = [ 
            'Orders' => []
        ];
        $_params = $this->digestParams(['order_id' => 'int']);

        $_ids = is_array($_params['order_id']) ? $_params['order_id'] : [ $_params['order_id'] ];

        foreach ($_ids as $_id) {
            $_Order = \Cataleya\Store\Order::load($_id);

            if ($_Order === NULL ) throw new Error ('One or more orders could not be found.');
            else $_Order->setOrderStatus($_params['order_status'])->saveData();

            $json_reply['Orders'][] = [ 
                'order_id'=>$_id, 
                'status' => $_Order->getOrderStatus()
            ];
        }
        $_FilterStore = ((int)$_params['active_store'] !== 0) ? \Cataleya\Store::load($_params['active_store']) : NULL;
        
        $json_reply['Population'] = array (
            'pending' => \Cataleya\Store\Orders::load($_FilterStore, NULL, array('status' => 'pending'))->getPopulation(), 
            'shipped' => \Cataleya\Store\Orders::load($_FilterStore, NULL, array('status' => 'shipped'))->getPopulation(), 
            'delivered' => \Cataleya\Store\Orders::load($_FilterStore, NULL, array('status' => 'delivered'))->getPopulation(), 
            'cancelled' => \Cataleya\Store\Orders::load($_FilterStore, NULL, array('status' => 'cancelled'))->getPopulation()
        );
        
        
        return $json_reply;
    }







    /**
     * deleteOrder
     *
     * @return void
     */
    protected function deleteOrder () {

        $json_reply = [ 
            'Orders' => []
        ];
        $_params = $this->digestParams(['order_id' => 'int']);

        $_ids = is_array($_params['order_id']) ? $_params['order_id'] : [ $_params['order_id'] ];

        \Cataleya\Helper\DBH::getInstance()->beginTransaction();
        foreach ($_ids as $_id) {
            $_Order = \Cataleya\Store\Order::load($_id);

            if ($_Order === NULL ) throw new Error ('One or more orders could not be found.');
            else $_Order->delete('cancelled');

            $json_reply['Orders'][] = ['order_id'=>$_id];

        }
        \Cataleya\Helper\DBH::getInstance()->commit();    
        
        
        $_FilterStore = ((int)$_params['active_store'] !== 0) ? \Cataleya\Store::load($_params['active_store']) : NULL;
        
        $json_reply['Population'] = array (
            'all' => \Cataleya\Store\Orders::load($_FilterStore, NULL, array('status' => 'all'))->getPopulation(), 
            'pending' => \Cataleya\Store\Orders::load($_FilterStore, NULL, array('status' => 'pending'))->getPopulation(), 
            'shipped' => \Cataleya\Store\Orders::load($_FilterStore, NULL, array('status' => 'shipped'))->getPopulation(), 
            'delivered' => \Cataleya\Store\Orders::load($_FilterStore, NULL, array('status' => 'delivered'))->getPopulation(), 
            'cancelled' => \Cataleya\Store\Orders::load($_FilterStore, NULL, array('status' => 'cancelled'))->getPopulation()
        );


        return $json_reply;
    }

        

    


        

}

