<?php



namespace Cataleya\Front\Dashboard\RPC\v1\Resource;
use \Cataleya\Error;


/*
 *
 * 	@Package: Cataleya
 * 	@Class: \Cataleya\Front\Dashboard\RPC\v1\Resource\Notifications
 *
 * 	(c) 2014 Fancy Paper Planes
 *
 *
 *
 */

class Notifications 
extends \Cataleya\Front\Dashboard\RPC\v1\Resource\Main 
{
    
    


    public function __construct() {

        
        
    }
    
    

    /*
     * 
     * 
     * CALLBACKS
     * 
     */





    /**
     * getNotification
     *
     * @return array
     */
    protected function getNotification () {

        $json_reply = [];
        $_params = $this->digestParams(['notification_id' => 'int']);
        
        $_Notification = \Cataleya\Admin\Notification::load($_params['notification_id']);
        if ($_Notification === NULL ) throw new Error ('Notification could not be loaded.');
        
        $json_reply['message'] = 'Notification.';
        $json_reply['Notification'] = \Cataleya\Front\Dashboard\Juicer::juice ($_Notification, $adminUser);


        return $json_reply;
    }




    /**
     * getNotifications
     *
     * @return array
     */
    protected function getNotifications () {


        $json_reply = [];

        $_User = \Cataleya\Front\Dashboard\Controller::getUser();
        $_Notifications = \Cataleya\Admin\Notifications::load();
        $json_reply['Notifications'] = [];
        
        foreach ($_Notifications as $_Notification) $json_reply['Notifications'][] = \Cataleya\Front\Dashboard\Juicer::juice ($_Notification, $_User);
        
        
        $json_reply['message'] = 'Notifications.';


        return $json_reply;
    }






    /**
     * markDisplayed
     *
     * @return array
     */
    protected function markDisplayed () {

        $json_reply = [];
        $_params = $this->digestParams(['notification_id' => 'int']);

        $_User = \Cataleya\Front\Dashboard\Controller::getUser();
        
        $_Notification = \Cataleya\Admin\Notification::load($_params['notification_id']);
        if ($_Notification === NULL ) throw new Error ('Notification could not be loaded.');
        
        $_Notification->getAlert($_User)->markDisplayed();
        
        
        $json_reply['message'] = 'Marked as displayed.';
        $json_reply['Notification'] = \Cataleya\Front\Dashboard\Juicer::juice ($_Notification, $_User);


        return $json_reply;
    }



    /**
     * markNotDisplayed
     *
     * @return array
     */
    protected function markNotDisplayed () {

        $json_reply = [];
        $_params = $this->digestParams(['notification_id' => 'int']);

        $_User = \Cataleya\Front\Dashboard\Controller::getUser();
        
        $_Notification = \Cataleya\Admin\Notification::load($_params['notification_id']);
        if ($_Notification === NULL ) throw new Error ('Notification could not be loaded.');
        
        $_Notification->getAlert($_User)->markAsNotDisplayed();
        
        
        $json_reply['message'] = 'Marked as "not displayed".';
        $json_reply['Notification'] = \Cataleya\Front\Dashboard\Juicer::juice ($_Notification, $_User);


        return $json_reply;
    }





    /**
     * markRead
     *
     * @return array
     */
    protected function markRead () {

        $json_reply = [];
        $_params = $this->digestParams(['notification_id' => 'int']);

        $_User = \Cataleya\Front\Dashboard\Controller::getUser();
        
        $_Notification = \Cataleya\Admin\Notification::load($_params['notification_id']);
        if ($_Notification === NULL ) throw new Error ('Notification could not be loaded.');
        
        $_Notification->getAlert($_User)->markRead();
        
        
        $json_reply['message'] = 'Marked as displayed.';
        $json_reply['Notification'] = \Cataleya\Front\Dashboard\Juicer::juice ($_Notification, $_User);


        return $json_reply;
    }






    /**
     * markUnread
     *
     * @return array
     */
    protected function markUnread () {

        $json_reply = [];
        $_params = $this->digestParams(['notification_id' => 'int']);

        $_User = \Cataleya\Front\Dashboard\Controller::getUser();
        
        $_Notification = \Cataleya\Admin\Notification::load($_params['notification_id']);
        if ($_Notification === NULL ) throw new Error ('Notification could not be loaded.');
        
        $_Notification->getAlert($_User)->markUnread();
        
        
        $json_reply['message'] = 'Marked as displayed.';
        $json_reply['Notification'] = \Cataleya\Front\Dashboard\Juicer::juice ($_Notification, $_User);


        return $json_reply;
    }




        

}

