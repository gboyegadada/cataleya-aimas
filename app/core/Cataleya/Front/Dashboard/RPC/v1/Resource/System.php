<?php



namespace Cataleya\Front\Dashboard\RPC\v1\Resource;
use \Cataleya\Error;


/*
 *
 * 	@Package: Cataleya
 * 	@Class: \Cataleya\Front\Dashboard\RPC\v1\Resource\Pages
 *
 * 	(c) 2014 Fancy Paper Planes
 *
 *
 *
 */

class System 
extends \Cataleya\Front\Dashboard\RPC\v1\Resource\Main 
{
    
    


    public function __construct() {

        
        
    }
    
    

    /*
     * 
     * 
     * CALLBACKS
     * 
     */








    /**
     * toggleAppStatus
     * 
     * @return array 
     */
    protected function toggleAppStatus () {

        $json_reply = [];
        
        $_params = $this->digestParams(['active'=>'bool']);
        

        $message = 'To ' . (($_params['active'] === false) ? 'turn off' : 'turn on') . ' Cataleya please confirm your password...';
        $this->confirm($message, true);
        
        $App = \Cataleya\System::load();
        
        // Disable or enable Cataleya (if we make it past the 'confirm' function)...
        if ((bool)$_params['active'] === true) $App->activate();
        else if ((bool)$_params['active'] === false) $App->shutdown();


        
        $json_reply['message'] = 'Cataleya is now ' . ($App->isActive() ? 'on.' : 'off.');
        $json_reply['App'] = [ "isActive"=>$App->isActive() ];


        return $json_reply;
    }










    
    

    
      
      
    

        

 
}


