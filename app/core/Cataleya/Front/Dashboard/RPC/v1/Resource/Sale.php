<?php



namespace Cataleya\Front\Dashboard\RPC\v1\Resource;
use \Cataleya\Error;


/*
 *
 * 	@Package: Cataleya
 * 	@Class: \Cataleya\Front\Dashboard\RPC\v1\Resource\Sale
 *
 * 	(c) 2014 Fancy Paper Planes
 *
 *
 *
 */

class Sale 
extends \Cataleya\Front\Dashboard\RPC\v1\Resource\Main 
{
    
    


    public function __construct() {

        
        
    }
    
    

    /*
     * 
     * 
     * CALLBACKS
     * 
     */






    /**
     * newSale
     *
     * @return array
     */
    protected function newSale () {

        $json_reply = [];
        $_params = $this->digestParams([
            'store_id' => 'int',  
            'title' => 'text', 

            'discount_type' => 'int', 
            'discount_amount' => 'float', 

            /*
            'start_day' => 'int', 
            'start_month' => 'int', 
            'start_year' => 'int', 

            'end_day' => 'int', 
            'end_month' => 'int', 
            'end_year' => 'int', 
            */

            'is_active' => 'boolean' 
        ]);


        $_params['discount_type'] = ($_params['discount_type'] === 0) 
                ? \Cataleya\Catalog\Price::TYPE_REDUCTION_PERCENT 
                : \Cataleya\Catalog\Price::TYPE_REDUCTION;


        $_Store = \Cataleya\Store::load($_params['store_id']);
        if ($_Store === NULL ) throw new Error ('Store could not be found.');
        
        $_Sale = \Cataleya\Sales\Sale::create($_Store, $_params);
        
        $json_reply['message'] = 'New Sale saved.';
        $json_reply['Sale'] = \Cataleya\Front\Dashboard\Juicer::juice($_Sale);


        return $json_reply;
    }




    /**
     * saveSale
     *
     * @return array
     */
    protected function saveSale () {

        $json_reply = [];        
        $_params = $this->digestParams([
            'store_id' => 'int',  
            'title' => 'text', 

            'discount_type' => 'int', 
            'discount_amount' => 'float', 

            /*
            'start_day' => 'int', 
            'start_month' => 'int', 
            'start_year' => 'int', 

            'end_day' => 'int', 
            'end_month' => 'int', 
            'end_year' => 'int', 
            */
            'is_active' => 'boolean' 
        ]);


        /* ---------------  Additional validation --------------- */

        // [1] Discount Amount
        $_max = ($_params['discount_type'] === \Cataleya\Catalog\Price::TYPE_REDUCTION_PERCENT) ? 100 : NULL;
        $_params['discount_amount'] = \Cataleya\Helper\Validator::float($_params['discount_amount'], 0, $_max);
        if ($_params['discount_amount'] === FALSE) throw new Error ("Invalid discount amount."); // $_bad_params[] = 'discount_amount';

        /*
        // [2] Start date
        $_params['start_date'] = \Cataleya\Helper\Validator::date(
            $_params['start_year'].'-'.
            $_params['start_month'].'-'.
            $_params['start_day']
        );
        if ($_params['start_date'] === FALSE) throw new Error ("Invalid start date.");

        // [3] End date
        $_params['end_date'] = \Cataleya\Helper\Validator::date(
            $_params['end_year'].'-'.
            $_params['end_month'].'-'.
            $_params['end_day']
        );
        if ($_params['end_date'] === FALSE) throw new Error ("Invalid end date.");
        */


        $_Sale = \Cataleya\Sales\Sale::load($_params['target_id']);
        if ($_Sale === NULL ) throw new Error ('Sale could not be found.');
        
        $_Store = \Cataleya\Store::load($_params['store_id']);
        if ($_Store === NULL ) throw new Error ('Store could not be found.');
        

        /* ----------------- Do your laundry --------------- */

        // [1] Sale Description
        $_Sale->getDescription()->setTitle($_params['title'], 'EN');
                
        // [2] Discount Type
        $_Sale->setDiscountType($_params['discount_type'])
                
        // [3] Discount Amount
        ->setDiscountAmount($_params['discount_amount']);
        
        // [4] Enable  / Disable
        $_params['is_active'] ? $_Sale->enable () : $_Sale->disable ();
        
        
        
        $json_reply['message'] = 'Sale saved.';
        $json_reply['Sale'] = \Cataleya\Front\Dashboard\Juicer::juice($_Sale);


        return $json_reply;
    }





    /**
     * deleteSale
     *
     * @return array
     */
    protected function deleteSale () {

        $json_reply = [];
        $_params = $this->digestParams(['target_id'=>'int']);
     
        $_Sale = \Cataleya\Sales\Sale::load($_params['target_id']);
        if ($_Sale === NULL ) throw new Error ('Sale could not be found.');
        
        $message = 'Are you sure you want to delete this SALE: "' . 
            $_Sale->getDescription()->getTitle('EN') . '" (' . 
            $_Sale->getStore()->getDescription()->getTitle('EN') .  
            ')?';

        $this->confirm($message, FALSE);
        
        $_Sale->delete();
        
        
        $json_reply['message'] = 'Sale deleted.';
        $json_reply['Sale'] = array ('id'=>$_params['target_id']);


        return $json_reply;
    }






    /**
     * getInfo
     *
     * @return array
     */
    protected function getInfo () {

        $json_reply = [];
        $_params = $this->digestParams(['target_id'=>'int']);
     
        $_Sale = \Cataleya\Sales\Sale::load($_params['target_id']);
        if ($_Sale === NULL ) throw new Error ('Sale could not be found.');
        
        $json_reply['message'] = 'Sale info.';
        $json_reply['Sale'] = \Cataleya\Front\Dashboard\Juicer::juice($_Sale);
        

        return $json_reply;
    }


      
    

        

}


