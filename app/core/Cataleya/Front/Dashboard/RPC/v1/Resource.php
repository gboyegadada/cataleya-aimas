<?php


namespace Cataleya\Front\Dashboard\RPC\v1;
use Cataleya\Front\Dashboard\RPC\v1\Resource;
use \Cataleya\Front\Dashboard\RPC;
use \Cataleya\Front\Dashboard\Controller as MainController;


/*
 *
 * 	@Package: Cataleya
 * 	@Class: \Cataleya\Front\Dashboard\RPC\v1\Resource
 *
 * 	(c) 2014 Fancy Paper Planes
 *
 *
 *
 */

class Resource 
{
    
    
    

    public function __construct() {


    }


    public static function load () {


        if (!RPC\Controller::isLoggedIn()) {
            header('HTTP/1.0 401 Unauthorized'); exit();
        }

        switch (MainController::getAction()) 
        {
            
            case 'pages':
                return Resource\Pages::load();

            case 'catalog':
                return Resource\Catalog::load();

            case 'category':
                return Resource\Category::load();

            case 'sale':
                return Resource\Sale::load();

            case 'coupon':
                return Resource\Coupon::load();

            case 'store':
                return Resource\Store::load();

            case 'customer':
                return Resource\Customer::load();

            case 'my-account':
                return Resource\MyAccount::load();

            case 'user':
                return Resource\User::load();

            case 'my-notifications':
                return Resource\Notifications::load();

            case 'order':
                return Resource\Order::load();

            case 'product':
                return Resource\Product::load();

            case 'tax-rule':
                return Resource\TaxRule::load();

            case 'app':
                return Resource\App::load();
            
            case 'system':
                return Resource\System::load();
            
            default:
                header('HTTP/1.0 404 Resource Not Found'); exit();
        }
        
    }
    

    

        

 
}


