<?php



namespace Cataleya\Front\Dashboard;







        
        
        

/*
 *
 * 	@Package: Cataleya
 * 	@Class: \Cataleya\Front\Dashboard\Themes
 *
 * 	(c) 2014 Fancy Paper Planes
 *
 *
 *
 */


        
class Themes extends \Cataleya\Collection 
{
    
    


    protected 
            $_themes_root, 
            $_config;



    
    // To be used by [\Cataleya\Collection]
    protected $_collection = array ();
    protected $_position = 0;



    public function __construct() {


            $this->_themes_root = ROOT_PATH.'skin/Dashboard/';
        

            parent::__construct();
            
            
    }
    
    
    
    
    



    /*
     *
     *  [ load ]
     * ________________________________________________________________
     * 
     * @return Singleton Instance
     *
     *
     *
     */

    static public function load ($page_size = 100, $page_num = 1) 
    {

        static $_instance = NULL;
        
        
        if (NULL === $_instance) {

            $_instance = new static();
        
        
        
            $folders = scandir($_instance->_themes_root);

            foreach ($folders as $folder) {
                if (
                        preg_match('/^([a-zA-Z\-_]+)$/i', $folder) == 0
                        || !file_exists($_instance->_themes_root . $folder) 
                        || !is_dir($_instance->_themes_root . $folder) 
                        || !file_exists($_instance->_themes_root . $folder . '/meta.json') 
                        ) continue;

                $_instance->_collection[] = \Cataleya\Front\Dashboard\Theme::load($folder);

            }
        
        
        
        }
        
        
        $_instance->pageSetup($page_size, $page_num);
        
        return $_instance;
        
        
    }

    
    
    /**
     * Private clone method to prevent cloning of the instance of the
     * *Singleton* instance.
     *
     * @return void
     */
    private function __clone()
    {
    }

    /**
     * Private unserialize method to prevent unserializing of the *Singleton*
     * instance.
     *
     * @return void
     */
    private function __wakeup()
    {
    }
        



    public function current()
    {

        return $this->_collection[$this->_position];
        
    }
    
    
    
    
    




    
      
    

        

 
}


