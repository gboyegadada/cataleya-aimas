<?php



namespace Cataleya\Front\Dashboard\HTML;
use \Cataleya\Front\Dashboard\Controller as MainController;







        
        
        

/*
 *
 * 	@Package: Cataleya
 * 	@Class: \Cataleya\Front\Dashboard\View
 *
 * 	(c) 2014 Fancy Paper Planes
 *
 *
 *
 */


        
class View
{
    
    


    protected 
            $_response = '', 
            $_errors = array (), 
            $_auto_fill = array (), 
            $_Controller;







    public function __construct($_Controller=NULL) {
        
        $this->_Controller = $_Controller;
        
        $_f = (MainController::isReady()) ? MainController::getContentType() : 'HTML';
        
        
        switch ($_f) 
        {

            case 'JSON':
                define('OUTPUT', 'JSON');
                header('Content-type: application/x-json');
            break;

            case 'HTML':
                define('OUTPUT', 'HTML');
                header('Content-type: text/html');
            break;

            case 'HTM':
                define('OUTPUT', 'HTML');
                header('Content-type: text/html');
            break;

            case 'PHP':
                define('OUTPUT', 'HTML');
                header('Content-type: text/html');
            break;

            case 'PDF':
                define('OUTPUT', 'PDF');
                header('Content-type: application/pdf');
            break;


            case 'TEXT':
                define('OUTPUT', 'TEXT');
                header('Content-Type: text/plain; charset="UTF-8"');
            break;

            case 'JS':
                define('OUTPUT', 'JS');
                ob_start("ob_gzhandler");
                header('Content-Type: application/javascript');
            break;

            case 'MUSTACHE':
                define('OUTPUT', 'MUSTACHE');
                ob_start("ob_gzhandler");
                header('Content-Type: text/plain');
            break;

            case 'CSS':
                define('OUTPUT', 'CSS');
                ob_start("ob_gzhandler");
                header('Content-Type: text/css');
            break;

            default:
                define('OUTPUT', 'HTML');
                header('Content-type: text/html');
            break;


        }
        
        

        \Cataleya\Front\Twiggy::getInstance(\__path('dash.skin') . '/layouts');


        
        
    }
    
    
    /**
     * 
     * @return \Cataleya\Front\Dashboard\HTML\Controller
     * 
     */
    public function getController () {
        return $this->_Controller;
    }








    /*
     * 
     * [ dispatch ]
     * ______________________________________________________________
     * 
     * 
     * 
     * 
     */


    public function dispatch () 
    {

    }
    



    /*
     * 
     * [ setResponse ]
     * ______________________________________________________________
     * 
     * 
     * 
     * 
     */


    public function setResponse ($_response_text = '') 
    {

        if (is_string($_response_text)) $this->_response = $_response_text;
        
        return $this;
    }
    
    
    
    

    /*
     * 
     * [ getResponse ]
     * ______________________________________________________________
     * 
     * 
     * 
     * 
     */


    public function getResponse ($_response_text = '') 
    {

        return $this->_response;
    }
    
    
    
    


    /*
     * 
     * [ setErrors ]
     * ______________________________________________________________
     * 
     * 
     * 
     * 
     */


    public function setErrors ($_errors = array()) 
    {
        if (!empty($_errors) && is_array($_errors)) {
            $this->_errors = array ();
            
            foreach ($_errors as $error_text) {
                if (is_string($error_text)) $this->_errors[] = $error_text;
            }
        }
        
        return $this;
    }
    
    



    /*
     * 
     * [ addError ]
     * ______________________________________________________________
     * 
     * 
     * 
     * 
     */


    public function addError ($_error_text = '') 
    {
        if (!empty($_error_text) && is_string($_error_text)) {
            $this->_errors[] = $_error_text;
        }
        
        return $this;
    }
    
    
    


    /*
     * 
     * [ resetErrors ]
     * ______________________________________________________________
     * 
     * 
     * 
     * 
     */


    public function resetErrors () 
    {
        $this->_errors = array ();
        
        return $this;
    }
    
    
    
    
    


    /*
     * 
     * [ getErrors ]
     * ______________________________________________________________
     * 
     * 
     * 
     * 
     */


    public function getErrors () 
    {
        
        return $this->_errors;
    }
    
    
    
    
    
    
    
    
    
    
    
    
    
    


    /*
     * 
     * [ setAutoFills ]
     * ______________________________________________________________
     * 
     * 
     * 
     * 
     */


    public function setAutoFills ($_auto_fills = array()) 
    {
        if (!empty($_auto_fills) && is_array($_auto_fills)) {
            $this->_auto_fills = array ();
            
            foreach ($_auto_fills as $_field => $_text) {
                if (is_string($_field, $_text)) $this->_auto_fills[$_field] = $_text;
            }
        }
        
        return $this;
    }
    
    



    /*
     * 
     * [ addAutoFill ]
     * ______________________________________________________________
     * 
     * 
     * 
     * 
     */


    public function addAutoFill ($_field = '', $_text = '') 
    {
        if (is_string($_field) && trim($_field) !== '' && is_scalar($_text)) {
            $this->_auto_fills[$_field] = $_text;
        }
        
        
        return $this;
    }
    
    
    



    /*
     * 
     * [ removeAutoFill ]
     * ______________________________________________________________
     * 
     * 
     * 
     * 
     */


    public function removeAutoFill ($_field = '') 
    {
        if (is_scalar($_field) && isset($this->_auto_fills[$_field]) ) {
            unset ($this->_auto_fills[$_field]);
        }
        
        
        return $this;
    }
    
    


    /*
     * 
     * [ resetAutoFills ]
     * ______________________________________________________________
     * 
     * 
     * 
     * 
     */


    public function resetAutoFills () 
    {
        $this->_auto_fills = array ();
        
        return $this;
    }
    
    
    
    
    


    /*
     * 
     * [ getAutoFills ]
     * ______________________________________________________________
     * 
     * 
     * 
     * 
     */


    public function getAutoFills () 
    {
        
        return $this->_auto_fills;
    }
    
    
    
    
    
    
    
    
    
    


    /*
     * 
     * [ notFound ]
     * ______________________________________________________________
     * 
     * 
     * 
     */


    public function notFound () 
    {


        header('HTTP/1.0 404 Not Found', TRUE, '404');
        
        // Fast CGI
        // header('Status: 404 Not Found', TRUE, '404');
        
        $twig_vars = MainController::getBaseTwigVars();




        // JSON anyone?
        if (in_array(OUTPUT, ['JSON', 'AJAX_JSON'])) {
            echo json_encode(array(
                'status' => '404 Not Found', 
                'message' => 'The resource you are looking for could not be found.'
            ), JSON_PRETTY_PRINT);
            exit();
        }
        

        // LOAD TEMPLATE
        $template = (OUTPUT === 'HTML' && IS_AJAX) 
        ? $template = \Cataleya\Front\Twiggy::loadTemplate('404.html.twig')
        : $template = \Cataleya\Front\Twiggy::loadTemplate('404.html.twig');

        echo $template->render($twig_vars);
        exit();  


    }
    
    
    
    
    
    
    



    
    
      
    

        

 
}


