<?php



namespace Cataleya\Front\Dashboard\HTML\View;
use \Cataleya\Front\Dashboard\HTML\Controller as MainController;



/*
 *
 * 	@Package: Cataleya
 * 	@Class: \Cataleya\Front\Dashboard\HTML\View\Login
 *
 * 	(c) 2014 Fancy Paper Planes
 *
 *
 *
 */

class Login extends \Cataleya\Front\Dashboard\HTML\View 
{
    
    


    protected 
            $_username = '';




    

    


    /*
     * 
     * [ dispatch ]
     * ______________________________________________________________
     * 
     * 
     * 
     * 
     * 
     */


    public function dispatch () 
    {


        // If user is already logged in

        if (defined('IS_LOGGED_IN') && IS_LOGGED_IN === TRUE) 
        {

            // Redirect browser...
            $referrer = (isset($_SESSION[SESSION_PREFIX.'REFERRER'])) 
                ? $_SESSION[SESSION_PREFIX.'REFERRER'] 
                : '/' . ADMIN_ROOT_URI;
            redirect($referrer);
            exit('<center><p>Please wait...</p></center>');
        }

        else if (!defined('IS_LOGGED_IN')) {
            define('IS_LOGGED_IN', FALSE);
        }



        if (!defined ('NEW_FORM_TOKEN')) define ('NEW_FORM_TOKEN', \Cataleya\Helper::generateToken(''));
        $_SESSION[SESSION_PREFIX.'FORM_TOKEN'] = NEW_FORM_TOKEN; // SECURITY TOKEN...

        

        $twig_vars = $this->getController()->getBaseTwigVars();
        $twig_vars['login'] = array (
            'response'  =>  $this->getController()->getResponse(), 
            'errors'  =>  $this->getController()->getErrors(), 
            'user'  =>  $this->getController()->getUsername(), 
            'token' =>  NEW_FORM_TOKEN
        );


        // LOAD TEMPLATE
        $login_template = \Cataleya\Front\Twiggy::loadTemplate('login.html.twig');
        echo $login_template->render($twig_vars);
        exit(); 


        
    }
    
    
    
    
    

    
      
    

        

 
}


