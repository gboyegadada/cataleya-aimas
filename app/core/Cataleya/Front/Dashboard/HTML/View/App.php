<?php



namespace Cataleya\Front\Dashboard\HTML\View;
use \Cataleya\Front\Dashboard\Controller as MainController;



/*
 *
 * 	@Package: Cataleya
 * 	@Class: \Cataleya\Front\Dashboard\HTML\View\Category
 *
 * 	(c) 2014 Fancy Paper Planes
 *
 *
 *
 */

class App extends \Cataleya\Front\Dashboard\HTML\View 
{
    




    public function __construct() {

        parent::__construct();
            
 
        
    }
    
    
    
    

    
    
    

    
    
    
    
    
    
    


    /*
     * 
     * [ dispatch ]
     * ______________________________________________________________
     * 
     * 
     * 
     */


    public function dispatch () 
    {
        
        
        

        $_app_handle = MainController::getAction();



        // 1. POST requests: are interpreted as an API Call to our App...

        if ($_SERVER['REQUEST_METHOD'] === 'POST') {


            $_data = filter_input_array(INPUT_POST, [
                    'auth_token'		=>	FILTER_SANITIZE_STRIPPED,
                    'target_id'	=>	FILTER_VALIDATE_INT, 
                    'data'	=>	[ 'flags' => FILTER_REQUIRE_ARRAY ], 
                    'do'	=>	[   'filter'	=>	FILTER_VALIDATE_REGEXP, 
                                    'options'	=>	array('regexp' => '/^[A-Za-z ]{1,50}$/') ]
                                    
                    ]
        
                );


            $app_response['app'] = \Cataleya\Plugins\Package::getAppInfo($_app_handle);
            $app_response['app']['response'] = \Cataleya\Plugins\Action::trigger(
                'dashboard-app.api-call', [ 'params' => $_data ], [ $_app_handle ]
            );


            echo json_encode($app_response);
            exit();


        }



        // 2. GET requests: we assume user is navigating to [ http://shop.com/dashboard/app/myapp ]
        //                  so we call the "render" method and specify page as "index"...

        $_params = MainController::getParams();
        $_page = isset($_params['app_pg']) ? $_params['app_pg'] : 'index';


        // INCLUDE BASE TWIG VARIABLES (ARRAYS)
        $twig_vars = $this->getController()->getBaseTwigVars();
        $twig_vars['app'] = \Cataleya\Plugins\Package::getAppInfo($_app_handle);
        $twig_vars['app']['html'] = \Cataleya\Plugins\Action::trigger(
            'dashboard-app.render', 
            [ 'params' => ['page'=>$_page] ], 
            [ $_app_handle ]
        );

        // LOAD THEME
        $template = \Cataleya\Front\Twiggy::loadTemplate('app.html.twig');
        echo $template->render($twig_vars);
        exit();

        
        
        
    }
    
    

    
    
    
    

    

        

 
}


