<?php

namespace Cataleya\Front\Dashboard\HTML;



/**
 *
 * 	@Package: Cataleya
 * 	@Class: \Cataleya\Front\Dashboard\HTML\Resource 
 *
 * 	(c) 2015 Fancy Paper Planes
 *
 *
 *
 */
abstract class Resource 
{
    
    protected $_errors = array();
    protected $_notices = array();
    protected $_failed_items = array();
    

    public function __construct() {
       
        
    }
    
    


    /**
     * load
     *
     * @return void
     */
    public static function load () 
    {
        
    }
    
    
    
    
     
    
    /**
     * 
     * @return array
     * 
     */
    public function getErrors() 
    {
        return $this->_errors;
    }

    /**
     * 
     * @return array
     * 
     */
    public function getNotices() 
    {
        return $this->_notices;
    }

    
    
    /**
     * 
     * @return array
     * 
     */
    public function getFailedItems() 
    {
        return $this->_failed_items;
    }
    
    
    


    /*
     * 
     * [ getTwigVars ]
     * ______________________________________________________________
     * 
     * @return array (JSON)
     * 
     * 
     */


    public function getBaseTwigVars () 
    {


        
        
        
        // TWIG VARIABLES ARRAY
        $twig_vars = [
            'dash' => [
                'base_url'=>BASE_URL,
                'landing' => \Cataleya\Front\Dashboard\Controller::getDashLanding(),  
                'domain'=> DOMAIN, 
                'assets_path' => \Cataleya\System::load()->getDashboardTheme()->getAssetsPath(),
                'error' =>  FALSE, 
                'errorText' =>  array (), 
                'language' => \Cataleya\System::load()->getDashboardLanguageCode()
            ], 

            'session' => [

                'auth_token' => defined('AUTH_TOKEN') ? AUTH_TOKEN : \Cataleya\Helper::generateToken('')

            ],

            'user' => [
                'is' => [],
                'can' => []
            ],

            'meta' => [
                'description' => ''
            ]


        ];


        

        $_privileges = \Cataleya\Admin\Privileges::load();

        $_User = __('dash.user');
        if (!empty($_User)) 
        {
            
            $_admin_role = $_User->getRole();
		    $twig_vars['user']['is'][$_admin_role->getHandle()] = true; 
            if (!$_admin_role->is('ROOT')) $twig_vars['user']['is']['ROOT'] = false;

            foreach ($_privileges as $_privilege) 
                 $twig_vars['user']['can'][$_privilege->getID()] = $_admin_role->hasPrivilege($_privilege->getID());




            // User info

            $twig_vars['user']['is']['LOGGED_IN'] = true;
            $twig_vars['user']['firstname'] = $_User->getFirstName();
            $twig_vars['user']['lastname'] = $_User->getLastName();
            $twig_vars['user']['email'] = $_User->getEmailAddress();
            $twig_vars['user']['id'] = $_User->getAdminID();
            $twig_vars['user']['authToken'] = (isset($_SESSION[SESSION_PREFIX.'AUTH_TOKEN'])) 
                ? $_SESSION[SESSION_PREFIX.'AUTH_TOKEN'] 
                : '';



        } else {
            $twig_vars['user']['is']['SUPER_USER'] = false;
            foreach ($_privileges as $_privilege) $twig_vars['user']['can'][$_privilege->getID()] = false;

            $twig_vars['user']['is']['LOGGED_IN'] = true;
        }



        // Meta
        $twig_vars['meta'] = array (
            'keywords'  =>  '', 
            'description'   =>  ''
        );
        
        
        return $twig_vars;

        
        
    }
    
    
    
    

 
}


