<?php



namespace Cataleya\Front\Dashboard\HTML\Resource\Asset;
use \Cataleya\Front\Dashboard\Controller as Main;



/*
 *
 * 	@Package: Cataleya
 * 	@Class: \Cataleya\Front\Dashboard\HTML\Controller\Cart
 *
 * 	(c) 2014 Fancy Paper Planes
 *
 *
 *
 */

class JS  
extends \Cataleya\Front\Dashboard\HTML\Resource 
{
    
    


    protected $_BLOB;







    public function __construct() {

    }


    public static function load () {
       

        return new static ();
 
        
    }
    


    
    
    
    

    /*
     * 
     * [ getBlob ]
     * ______________________________________________________________
     * 
     * 
     * 
     * 
     */
    
    public function getBlob() 
    {
        return $this->_BLOB;
    }


    

    /*
     * 
     * [ getErrors ]
     * ______________________________________________________________
     * 
     * 
     * 
     * 
     */
    
    public function getErrors() 
    {
        return $this->_errors;
    }






    /*
     * 
     * [ execute ]
     * ______________________________________________________________
     * 
     * 
     * 
     * 
     */


    public function execute () 
    {
        
        


        switch (Main::getAction()) 
        {
            
            
            case 'classes':
                $this->getJSClasses();
                break;

            case 'libs':
                $this->getJSLib();
                break;

            case 'lib':
                $this->getJSLib();
                break;
            
            default:
                $this->fetchJSFile();
                break;
        }
    }
    
    
    
    
    
    

    
    
    




    public function getJSClasses () 
    {
             

        $this->_BLOB = '';


        $_path_to_js_classes = \Cataleya\System::load()->getDashboardTheme()->getFolderPath() . '/assets/js/classes/';
        $_dir  = scandir($_path_to_js_classes);

        foreach ($_dir as $filename) {
            if (preg_match('/^(([A-Za-z0-9\-_]+\.?){1,10})\.js$/', $filename) > 0) { 
                $this->_BLOB .= file_get_contents ($_path_to_js_classes.$filename, FILE_SKIP_EMPTY_LINES);
            }
        }


        $_View = new \Cataleya\Front\Dashboard\HTML\View\Asset($this);
        $_View->dispatch();
        
    }
    
    
      
    
    
    
    



    public function getJSLib () 
    {
             
        
        $_assets_path = \Cataleya\System::load()->getDashboardTheme()->getAssetsPath();
        

        $this->_BLOB = '';

        
        $_resource_path = file_exists($_assets_path.'js/lib') ? $_assets_path.'js/lib' :  ADMIN_ROOT_PATH.'ui/jscript/libs/';
        $_dir  = scandir($_resource_path);

        foreach ($_dir as $filename) {
            if (preg_match('/^(([A-Za-z0-9\-_]+\.?){1,10})\.js$/', $filename) > 0) { 
                $this->_BLOB .= "\n\n/* ------------------------------------------- */\n\n" . file_get_contents ($_resource_path.$filename, FILE_SKIP_EMPTY_LINES);
            }
        }

        $_View = new \Cataleya\Front\Dashboard\HTML\View\Asset($this);
        $_View->dispatch();
        
    }
    
    


    public function fetchJSFile () 
    {
        
        
        $_assets_path = \Cataleya\System::load()->getDashboardTheme()->getAssetsPath();
        

        $_resource_path = file_exists($_assets_path.'jscript/') ? $_assets_path.'jscript/' :  ADMIN_ROOT_PATH.'ui/jscript/';
        $filename = $_resource_path . Main::getAction() . '.js';
        
        $this->_BLOB = file_exists($filename) ? file_get_contents ($filename, FILE_SKIP_EMPTY_LINES) : '/* Script Not Found */';
        

        $_View = new \Cataleya\Front\Dashboard\HTML\View\Asset($this);
        $_View->dispatch();
        
    }

        

 
}


