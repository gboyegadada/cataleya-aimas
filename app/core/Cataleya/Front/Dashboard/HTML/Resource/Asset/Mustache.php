<?php



namespace Cataleya\Front\Dashboard\HTML\Resource\Asset;
use \Cataleya\Front\Dashboard\Controller as Main;



/*
 *
 * 	@Package: Cataleya
 * 	@Class: \Cataleya\Front\Dashboard\HTML\Controller\Cart
 *
 * 	(c) 2014 Fancy Paper Planes
 *
 *
 *
 */

class Mustache 
extends \Cataleya\Front\Dashboard\HTML\Resource 
{
    
    


    protected $_BLOB;





    public function __construct() {

         // \Cataleya\Front\Dashboard\HTML\Controller::requireLogin ();

    }


    public static function load () {
       

        return new static ();
 
        
    }
    
    
    
    

    /*
     * 
     * [ getBlob ]
     * ______________________________________________________________
     * 
     * 
     * 
     * 
     */
    
    public function getBlob() 
    {
        return $this->_BLOB;
    }


    

    /*
     * 
     * [ getErrors ]
     * ______________________________________________________________
     * 
     * 
     * 
     * 
     */
    
    public function getErrors() 
    {
        return $this->_errors;
    }






    /*
     * 
     * [ execute ]
     * ______________________________________________________________
     * 
     * 
     * 
     * 
     */


    public function execute () 
    {
        
        


        
        
        $_theme_path = \Cataleya\System::load()->getDashboardTheme()->getFolderPath();
        

        $filename = $_theme_path.'/mustache/' . Main::getAction() . '.mustache';
        
        
        $this->_BLOB = file_exists($filename) ? file_get_contents ($filename, FILE_SKIP_EMPTY_LINES) : '{# Template Not Found #}';
        

        $_View = new \Cataleya\Front\Dashboard\HTML\View\Asset($this);
        $_View->dispatch();
        
    }
    
    
    
    
    

        

 
}


