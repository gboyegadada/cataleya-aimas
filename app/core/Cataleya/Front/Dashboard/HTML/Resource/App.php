<?php



namespace Cataleya\Front\Dashboard\HTML\Resource;
use \Cataleya\Front\Dashboard\Controller as Main;



/*
 *
 * 	@Package: Cataleya
 * 	@Class: \Cataleya\Front\Dashboard\HTML\Controller\App
 *
 * 	(c) 2014 Fancy Paper Planes
 *
 *
 * 	NOTE:   Difference between [ Dashboard\Controller\App ] and [ Dashboard\Controller\API\App ]
 *
 *          The --former-- either 
 *          1: renders specified app page (wrapped in Cataleya skin of cause) if 
 *          a GET request is made to it like this:
 *
 *              "http://shop.com/dashboard/app/myapp/welcome"
 *
 *          --OR-- 
 *
 *          2: executes API calls to our app and echos 
 *          a JSON response.
 *
 *          The --latter-- is part of the System's API conduit! And is used to get information about an app 
 *          or manipulate that app eg. delete it.
 *
 *
 *
 */

class App 
extends \Cataleya\Front\Dashboard\HTML\Resource 
{
    
    
    protected $_app_handle;
    

    public function __construct() {

        \Cataleya\Front\Dashboard\HTML\Controller::requireLogin ();
 
        
    }
    
    
    public static function load () {
       

        return new static ();
 
        
    }
    
    



    /*
     * 
     * [ execute ]
     * ______________________________________________________________
     * 
     * 
     * 
     * 
     */


    public function execute () 
    {

        $_app_handle = Main::getAction();
        if (\Cataleya\Plugins\Package::exists($_app_handle)) {
            $this->_app_handle = $_app_handle;
            $this->view();
        } else {
            // app not found...
            $_View = new \Cataleya\Front\Dashboard\HTML\View\NotFound($this);
            $_View->dispatch();

        } 


    }
    
    
    
    
    
    

    
    
    


    /*
     * 
     * [ view ]
     * ______________________________________________________________
     * 
     * 
     * 
     * 
     * 
     */


    public function view () 
    {
        

        $_View = new \Cataleya\Front\Dashboard\HTML\View\App($this);
        $_View->dispatch();
        
        
    }
    
    
    
    
    

    
      
      
    

        

 
}


