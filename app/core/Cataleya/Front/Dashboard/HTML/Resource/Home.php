<?php



namespace Cataleya\Front\Dashboard\HTML\Resource;



/*
 *
 * 	@Package: Cataleya
 * 	@Class: \Cataleya\Front\Dashboard\HTML\Controller\Home
 *
 * 	(c) 2014 Fancy Paper Planes
 *
 *
 *
 */

class Home 
extends \Cataleya\Front\Dashboard\HTML\Resource 
{
    
    

    

    public function __construct() {

        \Cataleya\Front\Dashboard\HTML\Controller::requireLogin ();

    }


    public static function load () {
       

        return new static ();
 
        
    }
    
    





    /*
     * 
     * [ execute ]
     * ______________________________________________________________
     * 
     * 
     * 
     * 
     */


    public function execute () 
    {

        $_View = new \Cataleya\Front\Dashboard\HTML\View\Home($this);
        $_View->dispatch();
    }
    
    
    
    
    
    

    
    
    
    
    
      
    

        

 
}


