<?php



namespace Cataleya\Front\Dashboard\HTML\Resource;



/*
 *
 * 	@Package: Cataleya
 * 	@Class: \Cataleya\Front\Dashboard\HTML\Controller\Login
 *
 * 	(c) 2014 Fancy Paper Planes
 *
 *
 *
 */

class Login 
extends \Cataleya\Front\Dashboard\HTML\Resource 
{
    
    


    protected 
            $_username = '', 
            $_finger_print = '';







    public function __construct() {
        
 
        
    }
    
    
    
    public static function load () {
       

        return new static ();
 
        
    }
    
    
    




    /*
     * 
     * [ execute ]
     * ______________________________________________________________
     * 
     * 
     * 
     * 
     */


    public function execute () 
    {

        if (\Cataleya\Front\Dashboard\HTML\Controller::isLoggedIn()) 
        {
            redirect (__url('dash'));
            exit('Redirecting...');
        }
        
        switch ($_SERVER['REQUEST_METHOD']) 
        {
            
            
            case 'POST':
                $this->doLogin();
                break;

            case 'GET':
                $this->showLogin();
                break;

            default:
                $this->showLogin();
                break;
        }
    }
    
    
    
    
    
    

    public function getUsername () 
    {
        return $this->_username;

    }

    
    

    public function getResponse () 
    {

        return $this->_response;
    }
    
    

    public function getFingerPrint () 
    {

        return $this->_finger_print;
    }
    


    /*
     * 
     * [ doLogin ]
     * ______________________________________________________________
     * 
     * 
     * 
     */


    public function doLogin () 
    {
        
        
        
        
        // If user is already logged in

        if (defined('IS_LOGGED_IN') && IS_LOGGED_IN === TRUE) 
        {
            // Redirect browser...
            $referrer = (isset($_SESSION[SESSION_PREFIX.'REFERRER'])) 
                ? $_SESSION[SESSION_PREFIX.'REFERRER'] 
                : '/' . ADMIN_ROOT_URI;

            redirect($referrer);
            exit('<center><p>Please wait...</p></center>');
        }

        else if (!defined('IS_LOGGED_IN')) {
            define('IS_LOGGED_IN', FALSE);
        }
        
        
                
        ///////////////////  IF INPUT IS NOT VIA POST METHOD //////////////////
        if (!isset($_SESSION[SESSION_PREFIX.'FORM_TOKEN'])) $this->showLogin ();
        else define ('FORM_TOKEN', $_SESSION[SESSION_PREFIX.'FORM_TOKEN']);



        // -----> validate //
        
        
        $_FORM = \Cataleya\Front\Form::load(INPUT_POST, \Cataleya\Front\Form::FROM_INPUT, [
            'username*' => 'email', 
            'password*' => 'password', 
            'fp*' => 'token'
        ]);
        
        if (!$_FORM->isHealthy()) $this->showLogin();
        
        $_Param_Username = $_FORM->getParam('username');
        $_Param_Password = $_FORM->getParam('password');
        $_Param_FingerPrint = $_FORM->getParam('fp');
         
        $_User = \Cataleya\Admin\User::login($_Param_Username->getValue(), $_Param_Password->getValue());
        if ($_User === NULL)
        {
                
            (defined('DEBUG_MODE') && DEBUG_MODE == TRUE) || sleep(4);
            
            $this->_username = ($_Param_Username->isHealthy()) ? $_Param_Username->getValue() : '';
            $this->_errors[] = $this->_response = 'Username or Password incorrect. -2';

            $this->showLogin ();
            
            
        } else {
            $this->_finger_print = $_Param_FingerPrint->getValue();
        }

        



        // PCI-DSS / PA-DSS requirements for lockouts and intervals:
        define('ADMIN_LOGIN_LOCKOUT_TIMER', (30 * 60));
        define('ADMIN_LOGIN_LOCKOUT_AFTER', 10);
        define('ADMIN_PASSWORD_EXPIRES_INTERVAL', strtotime('- 90 day'));




        ////////////////////// SETUP LOGIN SESSION DATA //////////
        session_regenerate_id(true);

        // First name // Last Name 
        $_SESSION[SESSION_PREFIX.'NAME'] = $_User->getName();
        $_SESSION[SESSION_PREFIX.'FNAME'] = $_User->getFirstname();
        $_SESSION[SESSION_PREFIX.'LNAME'] = $_User->getLastname();

        // IS ADMIN ?
        $_SESSION[SESSION_PREFIX.'IS_ADMIN'] = TRUE;


        // ADMIN ID
        $_SESSION[SESSION_PREFIX.'ADMIN_ID'] = $_User->getAdminID();


        // AUTH TOKEN // LOGGED IN

        unset($_SESSION[SESSION_PREFIX.'FORM_TOKEN']);
        $_SESSION[SESSION_PREFIX.'AUTH_TOKEN'] = md5( uniqid(rand(), TRUE) ) . '-' . CURRENT_DT;
        $_SESSION[SESSION_PREFIX.'LOGGED_IN'] = $_User->getLoginToken();
        $_SESSION[SESSION_PREFIX.'FINGER_PRINT'] = $_Param_FingerPrint->getValue();
        
        $_View = new \Cataleya\Front\Dashboard\HTML\View\Login\Session($this);
        
        $_View->dispatch(); 
        
        
        
    }
    
    

    
    
    
    

    


    /*
     * 
     * [ showLogin ]
     * ______________________________________________________________
     * 
     * 
     * 
     * 
     * 
     */


    public function showLogin ($_response="", $_user="") 
    {


         $_View = new \Cataleya\Front\Dashboard\HTML\View\Login($this);
        
         $_View->dispatch();         
        



        
    }
    
    
    
    
    

    


    
    

    
      
    

        

 
}


