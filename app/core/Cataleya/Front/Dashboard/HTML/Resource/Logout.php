<?php



namespace Cataleya\Front\Dashboard\HTML\Resource;
use \Cataleya\Front\Dashboard\Controller as Main;



/*
 *
 * 	@Package: Cataleya
 * 	@Class: \Cataleya\Front\Dashboard\HTML\Controller\Logout
 *
 * 	(c) 2014 Fancy Paper Planes
 *
 *
 *
 */

class Logout 
extends \Cataleya\Front\Dashboard\HTML\Resource 
{
    
    




    public function __construct() {



    }


    public static function load () {
       

        return new static ();
 
        
    }
    
    
    




    /*
     * 
     * [ execute ]
     * ______________________________________________________________
     * 
     * 
     * 
     * 
     */


    public function execute () 
    {

        $this->doLogout();
    }
    
    
    
    
    
    

    
    
    
    
    
    


    
    


    /*
     * 
     * [ doLogout ]
     * ______________________________________________________________
     * 
     * 
     * 
     */


    public function doLogout () 
    {
        $_User = __('dash.user');
        if (!empty($_User)) $_User->logout();
        
        
        \Cataleya\Session::kill_session(SESSION_NAME);


        $_View = new \Cataleya\Front\Dashboard\HTML\View\Logout($this);
        $_View->dispatch();         
        
        

    }
    
    
      
    

        

 
}


