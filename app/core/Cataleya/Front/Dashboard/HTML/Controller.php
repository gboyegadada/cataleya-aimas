<?php

namespace Cataleya\Front\Dashboard\HTML;
use \Cataleya\Front\Dashboard\HTML\Resource;
use \Cataleya\Front\Dashboard\Controller as Main;


//if (!defined ('IS_ADMIN_FLAG') || IS_ADMIN_FLAG !== TRUE) die("Illegal Access");



/**
 *
 * 	@Package: Cataleya
 * 	@Class: \Cataleya\Front\Dashboard\HTML\Resource 
 *
 * 	(c) 2015 Fancy Paper Planes
 *
 *
 *
 */
class Controller 
{
    
    
    protected static $_RW = [];
   
    protected static $_is_logged_in = false;



    public function __construct() {
       
        
    }
    
    
    
    



    /**
     * load
     *
     * @return void
     */
    public static function load () 
    {
        
        self::$_RW = Main::getParams();
        
        // Verify that requested APP exists...
        if (self::$_RW['c'] === 'app' && \Cataleya\Plugins\Package::exists(self::$_RW['a'])) {
                // App not found...
                return Resource\NotFound::load();
        }




        // Authenticate user...
        self::doAuth();
        
        
        
        switch (self::$_RW['c']) 
        {
            
            
            case 'index': return Resource\Home::load();
            
            case 'home': return Resource\Home::load();
            
            case 'js': return Resource\Asset\JS::load();
            
            case 'mustache': return Resource\Asset\Mustache::load();
            
            case 'app': return Resource\App::load();
            
            case 'login': return Resource\Login::load();
            
            case 'logout': return Resource\Logout::load();
            
            case 'forgot-password': return Resource\PasswordRecovery::load();
            
            default: return Resource\NotFound::load();
        }
    }
    
    
    
    
    

    
    

    
    /**
     * 
     * 
     * 
     * @return boolean
     * 
     */
    public static function isLoggedIn () 
    {
        return (self::$_is_logged_in === true);
    }
    
    
    
    


    
    
      


    /*
     * 
     * ______________________________________________________________
     * 
     * @return array (JSON)
     * 
     * 
     */


    public static function requireLogin () 
    {

        if (self::isLoggedIn()) return;
            
        
        

        if (!in_array(Main::getControllerHandle(), ['login', 'logout', 'forgot-password']) && 'HTML' === Main::getContentType())
        {
            $_SESSION[SESSION_PREFIX.'REFERRER'] = (SSL_REQUIRED ? 'https://' : 'http://').HOST.Main::getURI();
            if (!empty($_SERVER['QUERY_STRING'])) $_SESSION[SESSION_PREFIX.'REFERRER'] .=  '?' . $_SERVER['QUERY_STRING'];
                
        }
        
            
        $twig_vars = array (
            'status' => 'error', 
            'message' => 'Login required.'
        );
        
            
        // JSON anyone?
        if (in_array(OUTPUT, ['JSON', 'AJAX_JSON'])) {
            echo json_encode($twig_vars, JSON_PRETTY_PRINT);
            exit();
        }
        
        // - OR - //
        
        redirect (Main::getDashLanding().'login');
        exit('Redirecting...');
    
    
    }
    
    
    
    
    
    
    
    private static function doAuth () {

        self::$_is_logged_in = false;


        // FETCH USER TRENDS INCLUDE (Checks if user is connecting from an unusual location)
        // require_once (INC_PATH.'user_trends.php');

        // Else do normal auth...
        if (isset($_SESSION[SESSION_PREFIX.'ADMIN_ID'], $_SESSION[SESSION_PREFIX.'LOGGED_IN']))
        {


                $_User = \Cataleya\Admin\User::authenticate(
                    $_SESSION[SESSION_PREFIX.'ADMIN_ID'], 
                    $_SESSION[SESSION_PREFIX.'LOGGED_IN']
                );
                
                if ($_User !== NULL) 
                {
                    // Auth complete !!

                    // a. Set new login token...
                    $_SESSION[SESSION_PREFIX.'LOGGED_IN'] = $_User->getLoginToken();
                    
                    // b. Set flag to true..
                    self::$_is_logged_in = true;
                    
                    
                    define('AUTH_TOKEN', $_SESSION[SESSION_PREFIX.'AUTH_TOKEN']);
                    
                }

        } 
        

        // Existing session expired
        else if (isset($_SESSION[SESSION_PREFIX.'ADMIN_ID']))
        {
            unset($_SESSION[SESSION_PREFIX.'ADMIN_ID']);
            unset($_SESSION[SESSION_PREFIX.'LOGGED_IN']);

        }
        
        
        
        define('IS_LOGGED_IN', self::isLoggedIn());

        if (self::isLoggedIn()) 
        {
            $_admin_role = $_User->getRole();
            define('IS_SUPER_USER', ($_admin_role->hasPrivilege('SUPER_USER')));
            define('CAN_DELETE_ADMIN_ACC', ($_admin_role->hasPrivilege('DELETE_ADMIN_PROFILES') || IS_SUPER_USER));
        } else {
            define('IS_SUPER_USER', FALSE);
            define('CAN_DELETE_ADMIN_ACC', FALSE);
        }

    
        
        
        
        return;
        
        
        
        
        
        
        
        


        }
        
        
    

        

 
}


