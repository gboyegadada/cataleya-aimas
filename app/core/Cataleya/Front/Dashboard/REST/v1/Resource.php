<?php



namespace Cataleya\Front\Dashboard\REST\v1;
use \Cataleya\Front\Dashboard\REST\v1\Resource;



/*
 *
 * 	@Package: Cataleya
 * 	@Class: \Cataleya\Front\Dashboard\REST\v1\Resource
 *
 * 	(c) 2014 Fancy Paper Planes
 *
 *
 *
 */

class Resource 
{
    
    
    
    private static $_instance;
    private static $_post_data = [];
    private static $_jwt_token = null;
    private static $_jwt_secret = null;
    private static $_jwt_issuer = null;



    public function __construct() {


    }


    public static function load () {


        
        if (!in_array(self::getResourceHandle(), ['oauth', 'refresh'])) self::doAuth(); // Authenticate user...
        
        switch (self::getResourceHandle()) 
        {
            
            case 'oauth':
                return Resource\OAuth::load();

            case 'refresh':
                return Resource\Refresh::load();

            case 'page':
                return Resource\Pages::load();

            case 'catalog':
                return Resource\Catalog::load();

            case 'category':
                return Resource\Category::load();

            case 'store':
                return Resource\Store::load();

            case 'customer':
                return Resource\Customer::load();

            case 'my-account':
                return Resource\MyAccount::load();

            case 'user':
                return Resource\User::load();

            case 'notification':
                return Resource\Notifications::load();

            case 'order':
                return Resource\Order::load();

            case 'product':
                return Resource\Product::load();

            case 'app':
                return Resource\App::load();
            
            default:
                header('HTTP/1.0 404 Resource not found'); exit();

        }
        
    }
    

    


    public static function init () 
    {
        if (empty(self::$_jwt_secret)) {
            $_Config = __('config', 'store.core');
            self::$_jwt_secret = $_Config->getParam('JWT_SECRET');
            self::$_jwt_issuer = $_Config->getParam('domain');
        }

    }


    /**
     * getJWTSecret
     *
     * @return string
     */
    public static function getJWTSecret () 
    {
        self::init();
        return self::$_jwt_secret;
    }


    public static function getJWTIssuer () 
    {
        self::init();
        return self::$_jwt_issuer;
    }
    
    
    /**
     * parseJWT
     *
     * @return void
     */
    private static function parseJWT () {


        if (!empty(self::$_jwt_token)) return self::$_jwt_token;


        // 1. Match 'Bearer' at beginning of string...
        $_auth_header = (isset($_SERVER['HTTP_AUTHORIZATION'])) 
            ? $_SERVER['HTTP_AUTHORIZATION'] 
            : ''; 

        $_m = sscanf($_auth_header, 'Bearer %s');  

        // 2. Check that there is an access token...
        if (empty($_m)) {

            header('HTTP/1.0 400 Bad Request'); exit();

        }



        $_Config = __('config', 'store.core');

        // 3. Decode JWT token and verify...
        $token = (new \Lcobucci\JWT\Parser())->parse((string)$_m[0]); // Parses from a string
        $_is_valid = $token->verify(
            (new \Lcobucci\JWT\Signer\Hmac\Sha512()), 
            self::getJWTSecret()
        );


        if ($_is_valid !== true) {

            header('HTTP/1.0 400 Bad Request'); exit();

        }


        // 5. Validate with JWT library (valid, untampered, and not expired)..
        $data = new \Lcobucci\JWT\ValidationData(); // It will use the current time to validate (iat, nbf and exp)

        $data->setIssuer(self::getJWTIssuer());
        $data->setAudience(self::getJWTIssuer());
        // $data->setId(self::$_User->getLoginToken());

        if ($token->validate($data) !== true)  { 
            header('HTTP/1.0 401 Unauthorized'); exit();
        }



        // 5. If all is well..
        self::$_jwt_token = $token;


    }






    /**
     * doAuth
     *
     * @return void
     */
    private static function doAuth () {


        $token = self::parseJWT();


        /*

        // 5. Validate with JWT library (valid, untampered, and not expired)..
        $data = new \Lcobucci\JWT\ValidationData(); // It will use the current time to validate (iat, nbf and exp)

        $data->setIssuer($_Config->getParam('domain'));
        $data->setAudience($_Config->getParam('domain'));
        $data->setId(self::$_User->getLoginToken());

        if ($token->validate($data) !== true)  { 
            header('HTTP/1.0 401 Unauthorized'); exit();
        }

         */



    }
    
    
        

 
}


