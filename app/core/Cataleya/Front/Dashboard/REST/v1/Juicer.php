<?php



namespace Cataleya\Front\Dashboard\REST\v1;



/*
 *
 * 	@Package: Cataleya
 * 	@Class: Juicer
 *
 * 	(c) 2012 Fancy Paper Planes
 *
 *
 *
 */

class Juicer {
    //put your code here
    
    
    /*
     * @const integer
     * 
     */
    
    const CUSTOMER = 0;
    
    
    
    /*
     * @const integer
     * 
     */
    
    const STOREFRONT = 0;
    
    
    /*
     * @const integer
     * 
     */
    
    const ADMIN = 1;
    





    /*
     * 
     * [ juice ]
     * ______________________________________________________________
     * 
     * @param object (Product / Tag / Store / ... )
     * @param boolean (self::CUSTOMER / self::ADMIN)
     * 
     * @return array (JSON)
     * 
     * 
     */


    public static function juice ($_Object) 
    {
        
        
        $_m = 'juice_'.str_replace('\\', '_', get_class($_Object));
        
        
        if (is_object($_Object) && method_exists(__CLASS__, $_m)) {
            
            return forward_static_call_array([__CLASS__, $_m], func_get_args());
            
        } 
        
        return NULL;
        
        
    }
    
    
    
    
    
    



    /*
     * 
     * [ juiceDrop ]
     * ______________________________________________________________
     * 
     * @param object (Product / Tag / Store / ... )
     * @param \Cataleya\Store $_Store
     * 
     * @return array (JSON)
     * 
     * 
     */


    public static function juiceDrop ($_Object) 
    {
        
        
        $_m = 'juiceDrop_'.str_replace('\\', '_', get_class($_Object));
        
        
        if (is_object($_Object) && method_exists(__CLASS__, $_m)) {
            
            return forward_static_call_array([__CLASS__, $_m], func_get_args());
            
        }
        
        
        return NULL;
        
    }
    
    
      
    
    
    
    
    
    /**
     * juice_Cataleya_Customer
     *
     * @param \Cataleya\Customer $_Customer
     * @return array
     */
    private static function juice_Cataleya_Customer (\Cataleya\Customer $_Customer) 
    {
        
        $_info = array ( 
            'id'    =>  $_Customer->getCustomerId(), 
            'name'  =>  $_Customer->getName(), 
            'firstname'  =>  $_Customer->getFirstname(), 
            'lastname'  =>  $_Customer->getLastname(), 
            'email'  =>  $_Customer->getEmailAddress(), 
            'telephone'  =>  $_Customer->getTelephone(), 
            'isOnline'   =>  $_Customer->isOnline()
            
        );
        
        return $_info;
        
        
    }
    
    
    

    /**
     * juice_Cataleya_Admin_User
     *
     * @param \Cataleya\Admin\User $AdminAccount
     * @return array
     */
    private static function juice_Cataleya_Admin_User (\Cataleya\Admin\User $AdminAccount) 
    {
        
        $_info = array ( 
            'id'    =>  $AdminAccount->getAdminId(), 
            'name'  =>  $AdminAccount->getName(), 
            'fname'  =>  $AdminAccount->getFirstname(), 
            'lname'  =>  $AdminAccount->getLastname(), 
            'email'  =>  $AdminAccount->getEmail(), 
            'telephone'  =>  $AdminAccount->getPhone(), 
            'DisplayPicture'    =>  $AdminAccount->getDisplayPicture()->getHrefs(),
            'isActive'   =>  $AdminAccount->isActive(), 
            'role' => self::juice($AdminAccount->getRole())
            
        );
        
        return $_info;
        
        
    }
    
    
    
    /**
     * juice_Cataleya_Admin_Role
     *
     * @param \Cataleya\Admin\Role $_Role
     * @return void
     */
    private static function juice_Cataleya_Admin_Role (\Cataleya\Admin\Role $_Role) 
    {
        
        $_info = array (
            'id'    =>   $_Role->getRoleId(), 
            'handle' => $_Role->getHandle(), 
            'name'  =>   $_Role->getDescription()->getTitle('EN'), 
            'description' => $_Role->getDescription()->getText('EN'), 
            'isAdmin'   =>  $_Role->is('ROOT'),
            'is' => [],  
            'can' => []
        );
        
        $_privileges = \Cataleya\Admin\Privileges::load();
        $_info['is'][$_Role->getHandle()] = true; 
        if (!$_Role->is('ROOT')) $_info['is']['ROOT'] = false;

        foreach ($_privileges as $_privilege) $_info['can'][$_privilege->getID()] = $_Role->hasPrivilege($_privilege->getID());

        return $_info;
        
        
    }
    



    /**
     * juice_Cataleya_Admin_Notification
     *
     * @param \Cataleya\Admin\Notification $_Notification
     * @param \Cataleya\Admin\User $_User
     * @return array
     */
    private static function juice_Cataleya_Admin_Notification (\Cataleya\Admin\Notification $_Notification, \Cataleya\Admin\User $_User) 
    {

        $_Alert = $_Notification->getAlert($_User);
        
        $_info = array (
            'id'    =>  $_Notification->getID(), 
            'keyword'    =>  $_Notification->getKeyword(), 
            'title'  =>  $_Notification->getDescription()->getTitle('EN'), 
            'description'  =>  $_Notification->getDescription()->getText('EN'), 
            'entityName'  =>  $_Notification->getEntityName(), 
            'entityNamePlural'  =>  $_Notification->getEntityNamePlural(), 
            'notificationText'  =>  $_Notification->getNotificationText(), 
            'sendAfterPeriod'  =>  $_Notification->getSendAfterPeriod(), 
            'sendAfterInterval'  =>  $_Notification->getSendAfterInterval(), 
            'sendAfterIncrements'  =>  $_Notification->getSendAfterIncrements(),  
            'incrementsSinceLastSent'  =>  $_Notification->getIncrementsSinceLastSent(), 
            'url'  =>  $_Notification->getRequestUri(),
            'icon'  =>  $_Notification->getIconHref(), 
            
            'count' => $_Alert->getCount(), 
            'isDisplayed' => $_Alert->isDisplayed(), 
            'isRead' => $_Alert->isRead(), 
            'dashboardEnabled' => $_Alert->dashboardEnabled(), 
            'emailEnabled' => $_Alert->emailEnabled()
        );
        
        
        return $_info;
        
        
    }






    /*
     * 
     * [ juiceDrop_Product ]
     * ______________________________________________________________
     * 
     * @param \Cataleya\Catalog\Product $_Product
     * 
     * @return array (template ready JSON)
     * 
     * 
     */
    private static function juiceDrop_Cataleya_Catalog_Product (\Cataleya\Catalog\Product $_Product) 
    {


        // [1] PRIMARY INFO

        $json = array (
            'id' => $_Product->getProductId(), 
            'ean' => $_Product->getEAN(), 
            'upc' => $_Product->getUPC(), 
            'reference_code' => $_Product->getReferenceCode(), 
            'title' =>  $_Product->getDescription()->getTitle('EN'), 
            'description' =>  $_Product->getDescription()->getText('EN'), 
            'hidden' => $_Product->isHidden(), 
            'is_featured'  =>  $_Product->isFeatured()
        );



        // [2] PRIMARY IMAGE

        $json['primary_image'] = array (
            'id'    =>  $_Product->getPrimaryImage()->getID(), 
            'hrefs' =>  $_Product->getPrimaryImage()->getHrefs()
        );



        return $json;

    }


    



    /*
     * 
     * [ juice_Product ]
     * ______________________________________________________________
     * 
     * @param \Cataleya\Catalog\Product $_Product
     * 
     * @return array (template ready JSON)
     * 
     * 
     */


    private static function juice_Cataleya_Catalog_Product (\Cataleya\Catalog\Product $_Product) 
    {


         
        // [2] PRIMARY INFO
        
        $json = array (
            
            'id' => $_Product->getProductId(), 
            'ean' => $_Product->getEAN(), 
            'upc' => $_Product->getUPC(), 
            'reference_code' => $_Product->getReferenceCode(), 
            'title' =>  $_Product->getDescription()->getTitle('EN'), 
            'short_title' => $_Product->getDescription()->getTitle('EN', 40), 
            'description' =>  $_Product->getDescription()->getText('EN'), 
            'base_url'   =>  BASE_URL,  
            'hide_drop_label'  =>  ($_Product->getPrimaryImage()->getID() !== 0), 
            'hidden' => $_Product->isHidden(), 
            'is_featured'  =>  $_Product->isFeatured()
            
        );



        // [2] PRIMARY IMAGE

        $json['primary_image'] = array (
            'id'    =>  $_Product->getPrimaryImage()->getID(), 
            'hrefs' =>  $_Product->getPrimaryImage()->getHrefs()
        );



        // [3] BASE IMAGES

        $json['images'] = array ();
        $_item_num = 0;

        foreach ($_Product->getBaseImages() as $image) 
        {
            $json['images'][] = array (
                'index' => ++$_item_num, 
                'id'    =>  $image->getID(), 
                'hrefs' =>  $image->getHrefs(), 
                'hide'  =>  (($json['primary_image']['id'] === $image->getID()) ? 'none' : 'block')
            );

        }
        
        $json['thumbs_preview'] = array_slice($json['images'], 0, 4);



        // [4] TAGS

        $json['tags'] = array ();
        $Tags = \Cataleya\Catalog\Tags::load();

        foreach ($Tags as $_Tag) 
        {
            $json['tags'][] = self::juice($_Tag, $_Product);
        }



        // [4B] STORES

        $json['stores'] = array ();
        $Stores = \Cataleya\Stores::load();

        foreach ($Stores as $Store) 
        {
            $_store_info = self::juiceDrop($Store, $_Product);
            // $_store_info['in_store'] = ($Store->hasProduct($_Product)) ? true : false;
            $json['stores'][] = $_store_info;
                
        }







        // [5] ATTRIBUTES


        $_ProductType = $_Product->getProductType();
        $json['product_type'] = self::juice($_ProductType);
        
        /*
        $json['attributes'] = array ();

        foreach ($_ProductType as $_AttributeType) 
        {


            $json['attributes'][] = self::juice($_AttributeType);

        }
        


         */
        
         

        // [6] BASE PRICES

        $json['base_prices'] = array ();
        $_Price = $_Product->getBasePrice();
        if ($_Price !== null) {

            foreach ($json['stores'] as $_store_info) 
            {

                $price_value = $_Price->getValue($_store_info['id']);
                
                $json['base_prices'][] = array (
                    'id' => $_Price->getID(), 
                    'value' => $price_value, 
                    'formatted_value' => number_format($price_value, 2), 
                    'store' =>  $_store_info
                    );
            }
        }

        





        // [7] Product Option Pricing and Stock
        $json['options'] = array ();

        foreach ($_Product as $_Option) 
        {

            $option_data = self::juice($_Option);

            // $option_data['pid'] contains a concatination of all attribute IDs.
            // We will use this to help javascript select a [product option] based on the combination
            // of attributes our shopper selects.
            $json['attributes_to_options'][$option_data['pid']] = $option_data['id']; 

            $json['options'][] = $option_data;



        }



        // [9] Product Group

        $json['product_group'] = array ();
        foreach ($_Product->getProductGroup() as $_Item) $json['product_group'][] = juiceDrop ($_Item);


        return $json;


    }
    
    
    
    
    
    
    
    /**
     * 
     * @param \Cataleya\Catalog\Tag $_Tag
     * @param \Cataleya\Catalog\Product $_Product
     * @return array
     * 
     */
    private static function juice_Cataleya_Catalog_Tag (\Cataleya\Catalog\Tag $_Tag, \Cataleya\Catalog\Product $_Product = NULL) 
    {

        $json = array (
                'id'    =>  $_Tag->getID(), 
                'title' =>  $_Tag->getDescription()->getTitle('EN'), 
                'description'   =>  $_Tag->getDescription()->getText('EN'), 
                'in_category'   =>  (!empty($_Product) && $_Product->hasTag($_Tag)) ? true : false, 
                'active'   =>  $_Tag->isActive(), 
                'is_root' => $_Tag->isRoot(), 
                'population' =>$_Tag->getPopulation(), 
                'Store' => self::juiceDrop($_Tag->getStore())
            );
        
        return $json;
    }
    
    
    
    
    
    
    
    /**
     * 
     * @param \Cataleya\Catalog\Tag $_Tag
     * @param \Cataleya\Catalog\Product $_Product
     * @return array
     * 
     */
    private static function juiceDrop_Cataleya_Catalog_Tag (\Cataleya\Catalog\Tag $_Tag, \Cataleya\Catalog\Product $_Product = NULL) 
    {

        $json = array (
                'id'    =>  $_Tag->getID(), 
                'title' =>  $_Tag->getDescription()->getTitle('EN'), 
                'description'   =>  $_Tag->getDescription()->getText('EN'), 
                'in_category'   =>  (!empty($_Product) && $_Product->hasTag($_Tag)) ? true : false, 
                'active'   =>  $_Tag->isActive(), 
                'is_root' => $_Tag->isRoot(), 
                'population' =>$_Tag->getPopulation(), 
                'Store' => self::juiceDrop($_Tag->getStore())
            );
        
        return $json;
    }
    
    
    
   
    


    /*
     * 
     * [ juice_ProductOption ]
     * ______________________________________________________________
     * 
     * @param \Cataleya\Catalog\Product\Option $_ProductOption
     * 
     * @return array (template ready JSON)
     * 
     * 
     */



    private static function juice_Cataleya_Catalog_Option (\Cataleya\Catalog\Product\Option $_Option) 
    {


        
        $stores = array ();
        $_Stores = \Cataleya\Stores::load();
        $_Product = $_Option->getProduct();
        $_ProductType = $_Product->getProductType();

        foreach ($_Stores as $_Store) 
        {
            if ($_Store->hasProduct($_Product)) {
                $stores[] = self::juiceDrop($_Store);
            }
        }

        $prices = $stock = array();
        $pseudo_id = "";

        foreach ($stores as $_store_info) 
        {
            $_Price = $_Option->getPrice();
            $_Stock = $_Option->getStock();
            $price = $_Price->getValue($_store_info['id']);
            
            $prices[] = array (
                'id' => $_Price->getID(), 
                'value' => $price, 
                'formatted_value' => number_format($price, 2), 
                'store' =>  $_store_info, 
                'option_id'  =>  $_Option->getID()
                );

            $stock[] = array (
                'id' => $_Stock->getID(), 
                'value' => $_Stock->getValue($_store_info['id']), 
                'store' =>  $_store_info, 
                'option_id'  =>  $_Option->getID()
                 );
        }



        $_PreviewImage = $_Option->getPreviewImage();
        $_APP = \Cataleya\System::load();
                
        $option_data = array (
                'id' => $_Option->getID(), 
                'product_id' => $_Option->getProductId(), 
                'description' => array (), 
                'product_type' => self::juice($_ProductType, $_Option), 
                'attributes' => array (),             
                'prices' =>  $prices, 
                'stock' =>  $stock, 
                'ean' => $_Option->getEAN(), 
                'has_EAN' => ($_ProductType->hasEAN() && $_APP->eanEnabled()), 
                'upc' => $_Option->getUPC(),  
                'has_UPC' => ($_ProductType->hasUPC() && $_APP->upcEnabled()), 
                'sku' => $_Option->getSKU(),   
                'has_SKU' => ($_ProductType->hasSKU() && $_APP->skuEnabled()), 
                'isbn' => $_Option->getISBN(),  
                'has_ISBN' => ($_ProductType->hasISBN() && $_APP->isbnEnabled()),  
                'hide_drop_label'  =>  ($_PreviewImage->getID() !== 0),
                'image' => array (
                            'id'    =>  $_PreviewImage->getID(), 
                            'hrefs' =>  $_PreviewImage->getHrefs()
                        )
            );

        
        
        // ATTRIBUTES..
        
        foreach ($_Option as $_Attribute) 
        {
            $pseudo_id .= $_Attribute->getID();
            $_AttributeType = $_Attribute->getAttributeType();

            $option_data['attributes'][] = array (
                'id' => $_Attribute->getID(), 
                'label' => $_AttributeType->getName(), 
                'label_plural' => $_AttributeType->getNamePlural(), 
                'value' => $_Attribute->getValue() 
                // 'AttributeType' => self::juice($_AttributeType, $_Option)
            );
            
            $option_data['description'][] = $_Attribute->getValue();
            

        }
        
        


        
        
        $option_data['description'] = (!empty($option_data['description'])) ? implode(', ', $option_data['description']) : 'No attributes yet!'; 
        $option_data['pid'] = $pseudo_id;

        

        return $option_data;


    }

    
    
    
    
    
    
    
    
    
    

    /*
     * 
     * [ juice_ProductType ]
     * ______________________________________________________________
     * 
     * @param \Cataleya\Catalog\Product\Type $_ProductType
     * 
     * @return array (template ready JSON)
     * 
     * 
     */



    private static function juice_Cataleya_Catalog_Product_Type (\Cataleya\Catalog\Product\Type $_ProductType, \Cataleya\Catalog\Product\Option $_Option = NULL) 
    {


        $_info = array (

            'id'    =>  $_ProductType->getID(), 
            'name' =>  $_ProductType->getName(), 
            'name_plural' =>  $_ProductType->getNamePlural(),
            'code' => $_ProductType->getCode(), 
            'icon_href' =>  $_ProductType->getIconHref(), 
            'has_EAN' => $_ProductType->hasEAN(), 
            'has_UPC' => $_ProductType->hasUPC(), 
            'has_SKU' => $_ProductType->hasSKU(),  
            'has_ISBN' => $_ProductType->hasISBN(),  
            'attribute_types' => array ()
        );

        foreach ($_ProductType as $_AttributeType) {
            
            $_info['attribute_types'][] = self::juice($_AttributeType, $_Option);
        }
        return $_info;


    }

    
    
    
    

    /*
     * 
     * [ juice_AttributeType ]
     * ______________________________________________________________
     * 
     * @param \Cataleya\Catalog\Product\Attribute\Type $_AttributeType
     * 
     * @return array (template ready JSON)
     * 
     * 
     */



    private static function juice_Cataleya_Catalog_Product_Attribute_Type (\Cataleya\Catalog\Product\Attribute\Type $_AttributeType, \Cataleya\Catalog\Product\Option $_Option = NULL) 
    {


        $_info = array(
                'id' => $_AttributeType->getID(),
                'label' => $_AttributeType->getName(), 
                'label_plural' => $_AttributeType->getNamePlural(), 
                'code' => $_AttributeType->getCode(), 
                'values' => array (), 
                'has_attribute_type' => (!empty($_Option)) ? $_Option->hasAttributeType($_AttributeType) : false, 
                'no_attribute' => true
            );
        
        foreach ($_AttributeType as $_Attribute) 
        {
            $_selected = (!empty($_Option)) ? $_Option->hasAttribute($_Attribute) : false;
            if ($_info['no_attribute']) $_info['no_attribute'] = !$_selected;
            
            $_info['values'][] = array (
                'id' => $_Attribute->getID(), 
                'label' => $_AttributeType->getName(), 
                'label_plural' => $_AttributeType->getNamePlural(),
                'code' => $_AttributeType->getCode(), 
                'value' => $_Attribute->getValue(), 
                'selected' => $_selected
            );
        }

        return $_info;


    }

    
    
    
    

    /*
     * 
     * [ juice_Attribute ]
     * ______________________________________________________________
     * 
     * @param \Cataleya\Catalog\Product\Attribute $_Attribute
     * 
     * @return array (template ready JSON)
     * 
     * 
     */



    private static function juice_Cataleya_Catalog_Product_Attribute (\Cataleya\Catalog\Product\Attribute $_Attribute) 
    {

        $_AttributeType = $_Attribute->getAttributeType();
        
        $_info = array (
                    'id' => $_Attribute->getID(), 
                    'label' => $_AttributeType->getName(), 
                    'label_plural' => $_AttributeType->getNamePlural(),
                    'value' => $_Attribute->getValue()
                );

        return $_info;


    }

    
    
    
    
    
    

    /*
     * 
     * [ juice_Store ]
     * ______________________________________________________________
     * 
     * @param \Cataleya\Store $_Store
     * @param \Cataleya\Store $_Store
     * 
     * @return array (template ready JSON)
     * 
     * 
     */


    
    private static function juice_Cataleya_Store (\Cataleya\Store $_Store, \Cataleya\Catalog\Product $_Product=NULL) 
    {

        


        $StoreLocation = $_Store->getDefaultContact();
        $_Country = $StoreLocation->getCountry();

        $_Currency = $_Store->getCurrency();
        
        
        // Store info
        $json = array (
            'title' =>  $_Store->getDescription()->getTitle($_Store->getLanguageCode()), 
            'short_title' => $_Store->getDescription()->getTitle($_Store->getLanguageCode(), 10), 
            'description'   =>  $_Store->getDescription()->getText($_Store->getLanguageCode()), 
            'language'  =>  $_Store->getLanguageCode(), 
            'country'   =>  self::juice($_Country),
            'landing'   =>  $_Store->getShopLanding(), 
            'population' => $_Store->getPopulation(), 
            'in_store' => ($_Product !== NULL ) ? $_Store->hasProduct($_Product) : FALSE, 
            'keyword' => $_Store->getHandle(),
            'handle' => $_Store->getHandle(),
            'label_color'=>$_Store->getLabelColor(), 
            'payment_options' => $_Store->getPaymentOptions(), 
            'currency'  =>  array (
                                    'code' =>  $_Store->getCurrencyCode(), 
                                    'symbol'    =>  '&#'.$_Currency->getUtfCode().';', 
                                    'name'  =>  $_Currency->getCurrencyName()
                              ), 

            // SHOP contact

            'contact' => self::juice($_Store->getDefaultContact())
        );
        
        
        
        

        return $json;

    }

    
    
    
    
    
    

    /*
     * 
     * [ juiceDrop_Store ]
     * ______________________________________________________________
     * 
     * @param \Cataleya\Store $_Store
     * @param \Cataleya\Store $_Store
     * 
     * @return array (template ready JSON)
     * 
     * 
     */


    
    private static function juiceDrop_Cataleya_Store (\Cataleya\Store $_Store, \Cataleya\Catalog\Product $_Product=NULL) 
    {

        


        // $StoreLocation = $_Store->getDefaultContact();
        // $_Country = $StoreLocation->getCountry();

        $_Currency = $_Store->getCurrency();
        
        // Store info
        $json = array (
            'id'    =>  $_Store->getID(), 
            'title' =>  $_Store->getDescription()->getTitle($_Store->getLanguageCode()), 
            'short_title' => $_Store->getDescription()->getTitle($_Store->getLanguageCode(), 8), 
            'description'   =>  $_Store->getDescription()->getText($_Store->getLanguageCode()), 
            'active'   =>  ($_Store->isActive()) ? true : false, 
            'population' => $_Store->getPopulation(), 
            'language'  =>  $_Store->getLanguageCode(), 
            'label_color'=>$_Store->getLabelColor(), 
            'in_store' => ($_Product !== NULL ) ? $_Store->hasProduct($_Product) : FALSE, 
            // 'country'   =>  self::juice($_Country),
            'landing'   =>  $_Store->getShopLanding(), 
            'keyword' => $_Store->getHandle(),
            'handle' => $_Store->getHandle(),
            'payment_options' => $_Store->getPaymentOptions(), 
            'currency'  =>  array (
                                    'code' =>  $_Store->getCurrencyCode(), 
                                    'symbol'    =>  '&#'.$_Currency->getUtfCode().';', 
                                    'name'  =>  $_Currency->getCurrencyName()
                              ), 

            // SHOP contact

            // 'contact' => self::juice($_Store->getDefaultContact())
        );
        
        

        return $json;

    }

    
    


    /**
     * juice_Cataleya_Store_Staff
     *
     * @param \Cataleya\Store\Staff $_Staff
     * @return void
     */
    private static function juice_Cataleya_Store_Staff (\Cataleya\Store\Staff $_Staff) 
    {
        
        $_info = array ();
        $_AdminUsers = \Cataleya\Admin\Users::load();
        
        foreach ($_AdminUsers as $_AdminUser) 
        {
            if ($_Staff->hasStaffMember($_AdminUser)) continue;
            
            $_info = self::juice($_AdminUser) +
            [
                'label'  => ($_AdminUser->getName() . ' (' . 
                            $_AdminUser->getRole()->getDescription()->getTitle('EN') . 
                            ')' )
            ];

            
        }

        
        return $_info;
        
        
    }
    
    
    
 

    /**
     * 
     * 
     * @param \Cataleya\Payment\PaymentType $_PaymentType
     * 
     * @return array (template ready JSON)
     * 
     * 
     */
    private static function juice_Cataleya_Payment_PaymentType (\Cataleya\Payment\PaymentType $_PaymentType, \Cataleya\Store $_Store) 
    {

        $_Currency = $_Store->getCurrency();
        
        $json = array (
                'id' => $_PaymentType->getID(), 
                'Title' => $_PaymentType->getDisplayName(), 
                'Logo' => $_PaymentType->getLogo(), 
                'MaxAmount' => ($_Currency->getCurrencyCode() . number_format ((float)$_PaymentType->getMaxAmount($_Currency), 2)), 
                'MinAmount' => ($_Currency->getCurrencyCode() . number_format ((float)$_PaymentType->getMinAmount($_Currency), 2)), 
                'active' => $_Store->acceptsPaymentType($_PaymentType->getID())
            );

        return $json;

    }

    
    
    
    
    

    /**
     * 
     * @param \Cataleya\Sales\Coupon $_Coupon
     * @return array (template ready JSON)
     * 
     */
    private static function juice_Cataleya_Sales_Coupon (\Cataleya\Sales\Coupon $_Coupon) 
    {
        $_Store = $_Coupon->getStore();
        $_Currency = $_Store->getCurrency();

        $start_date = $_Coupon->getStartDate(TRUE);
        $end_date = $_Coupon->getEndDate(TRUE);

        $_info = array (
            'code' =>  $_Coupon->getCode(), 
            'id'    =>  $_Coupon->getID(), 
            'discountType'  =>  (($_Coupon->getDiscountType() === \Cataleya\Catalog\Price::TYPE_REDUCTION_PERCENT) ? FALSE : TRUE), 
            'discountAmount'    =>  $_Coupon->getDiscountAmount(), 
            'Currency'  =>  array (
                'name'  =>  $_Currency->getCurrencyName(), 
                'code'  =>  $_Currency->getCurrencyCode(), 
                'utf'  =>  $_Currency->getUtfCode()
            ), 
            'Store' =>  self::juiceDrop($_Store), 
            'startDateDay' => $start_date['day'], 
            'startDateMonth' => $start_date['month'], 
            'startDateYear' => $start_date['year'], 
            'startDate' =>  $_Coupon->getStartDate(), 

            'endDateDay' => $end_date['day'], 
            'endDateMonth' => $end_date['month'], 
            'endDateYear' => $end_date['year'], 
            'endDate'   =>  $_Coupon->getEndDate(), 

            'isIndefinite'    =>  $_Coupon->isIndefinite(), 

            'usesPerCoupon' =>  $_Coupon->getUsesPerCoupon(), 
            'isActive'  =>  $_Coupon->isActive(), 
            'population' => $_Coupon->getPopulation()

        );

        return $_info;


    }


    
    
    


    /**
     * 
     * @param \Cataleya\Sales\Sale $_Sale
     * @return array (template ready JSON)
     * 
     */
    function juice_Cataleya_Sales_Sale (\Cataleya\Sales\Sale $_Sale) 
    {
        $_Store = $_Sale->getStore();
        $_Currency = $_Store->getCurrency();

        $_info = array (
            'title' =>  $_Sale->getDescription()->getTitle('EN'), 
            'id'    =>  $_Sale->getID(), 
            'discountType'  =>  (($_Sale->getDiscountType() === \Cataleya\Catalog\Price::TYPE_REDUCTION_PERCENT) ? FALSE : TRUE), 
            'discountAmount'    =>  $_Sale->getDiscountAmount(), 
            'Currency'  =>  array (
                'name'  =>  $_Currency->getCurrencyName(), 
                'code'  =>  $_Currency->getCurrencyCode(), 
                'utf'  =>  $_Currency->getUtfCode()
            ), 
            'Store' => self::juiceDrop($_Store),
            
            'start_date' => $_Sale->getStartDate(TRUE), 
            'end_date' => $_Sale->getEndDate(TRUE), 

            'isActive'  =>  $_Sale->isActive(), 
            'population'=> $_Sale->getPopulation()

        );

        return $_info;


    }

    
    
    
    
    
    
    
    /**
     * 
     * 
     * 
     * @param \Cataleya\Front\Shop\Page $_Page
     * 
     * @return array (template ready JSON)
     * 
     * 
     */
    private static function juice_Cataleya_Front_Shop_Page (\Cataleya\Front\Shop\Page $_Page) 
    {

        


        
        $_Store = $_Page->getStore();
        $_Autosave = $_Page->getAutosave(\Cataleya\Front\Dashboard\User::getInstance());
        
               
        
        // Store info
        $json = array (
            'id'    =>  $_Page->getID(), 
            'title' =>  $_Page->getTitle($_Store->getLanguageCode()), 
            'body'   =>  $_Page->getText($_Store->getLanguageCode()), 
            'active'   =>  ($_Page->isActive()) ? true : false, 
            'language'  =>  $_Store->getLanguageCode(),
            'keyword' => $_Page->getSlug(), 
            'handle' => $_Page->getSlug(), 
            'Store' => self::juiceDrop($_Store),  
            'Content' => $_Page->getContent($_Store->getLanguageCode()), 
            'Autosave' => (NULL === $_Autosave) ? FALSE : self::juice($_Autosave), 

            'AppList' => []
        );
        
        
        foreach ($_Page->getAppList() as $_handle) {
            $json['AppList'][] = \Cataleya\Plugins\Package::getAppInfo($_handle);
        }
        

        return $json;

    }

    
    
    
    
    
    
    /*
     * 
     * [ juice_Page ]
     * ______________________________________________________________
     * 
     * @param \Cataleya\Front\Shop\Page $_Page
     * 
     * @return array (template ready JSON)
     * 
     * 
     */


    
    private static function juiceDrop_Cataleya_Front_Shop_Page (\Cataleya\Front\Shop\Page $_Page) 
    {

        
        $_Store = $_Page->getStore();
        //$_Autosave = $_Page->getAutosave(\Cataleya\Front\Dashboard\User::getInstance());
        
        // Store info
        $json = array (
            'id'    =>  $_Page->getID(), 
            'title' =>  $_Page->getTitle($_Store->getLanguageCode()), 
            'body'   =>  $_Page->getText($_Store->getLanguageCode()), 
            'active'   =>  ($_Page->isActive()) ? true : false, 
            'language'  =>  $_Store->getLanguageCode(),
            'keyword' => $_Page->getSlug(), 
            'handle' => $_Page->getSlug(), 
            'Store' => self::juiceDrop($_Store),  
            'Content' => $_Page->getContent($_Store->getLanguageCode()) 
            //'Autosave' => (NULL === $_Autosave) ? FALSE : self::juice($_Autosave)
        );
        
        
        
        
        
        

        return $json;

    }

    
    
    
    
    
    
    
    /*
     * 
     * [ juiceDrop_Autosave ]
     * ______________________________________________________________
     * 
     * @param \Cataleya\Front\Shop\Page\Autosave $_Autosave
     * 
     * @return array (template ready JSON)
     * 
     * 
     */


    
    private static function juiceDrop_Cataleya_Front_Shop_Page_Autosave (\Cataleya\Front\Shop\Page\Autosave $_Autosave) 
    {

        
        return self::juice_Cataleya_Front_Shop_Page_Autosave($_Autosave);

    }

    
    
    
    
    
    
    
    
    /*
     * 
     * [ juice_Autosave ]
     * ______________________________________________________________
     * 
     * @param \Cataleya\Front\Shop\Page\Autosave $_Autosave
     * 
     * @return array (template ready JSON)
     * 
     * 
     */


    
    private static function juice_Cataleya_Front_Shop_Page_Autosave (\Cataleya\Front\Shop\Page\Autosave $_Autosave) 
    {

        
        $_Store = $_Autosave->getStore();
        
        // Store info
        $json = array (
            'id'    =>  $_Autosave->getID(), 
            'page_id' => $_Autosave->getPageID(), 
            'title' =>  $_Autosave->getTitle($_Store->getLanguageCode()), 
            'body'   =>  $_Autosave->getText($_Store->getLanguageCode()), 
            'active'   =>  ($_Autosave->isActive()) ? true : false, 
            'language'  =>  $_Store->getLanguageCode(),
            //'Store' => self::juiceDrop($_Store)
        );
        
        
        

        return $json;

    }

    
    
    
    
    
    
    /*
     * 
     * [ juice_Theme ]
     * ______________________________________________________________
     * 
     * @param \Cataleya\Front\Shop\Theme $_Theme
     * 
     * @return array (template ready JSON)
     * 
     * 
     */


    
    private static function juice_Cataleya_Front_Shop_Theme (\Cataleya\Front\Shop\Theme $_Theme) 
    {

        
        
        // template info
        $json = array (
            'id'    =>  $_Theme->getID(), 
            'name' =>  $_Theme->getName(), 
            'folder_path'   =>  $_Theme->getFolderPath(), 
            'assets_path'   =>  $_Theme->getAssetsPath(), 
            'version'  =>  $_Theme->getVersion(),
            'page_templates' => $_Theme->getPageTemplates()
        );
        
        

        return $json;

    }

    
    
    
    
    
    
    
    
    
    
    /*
     * 
     * [ juiceDrop_Theme ]
     * ______________________________________________________________
     * 
     * @param \Cataleya\Front\Shop\Theme $_Theme
     * 
     * @return array (template ready JSON)
     * 
     * 
     */


    
    private static function juiceDrop_Cataleya_Front_Shop_Theme (\Cataleya\Front\Shop\Theme $_Theme) 
    {

        
        
        // theme info
        $json = array (
            'id'    =>  $_Theme->getID(), 
            'name' =>  $_Theme->getName(), 
            'folder_path'   =>  $_Theme->getFolderPath(), 
            'assets_path'   =>  $_Theme->getAssetsPath(), 
            'version'  =>  $_Theme->getVersion(),
            'page_templates' => $_Theme->getPageTemplates()
        );
        
        

        return $json;

    }

    
    
    
    
    
    
    
    /*
     * 
     * [ juice_DashboardTheme ]
     * ______________________________________________________________
     * 
     * @param \Cataleya\Front\Shop\Theme $_Theme
     * 
     * @return array (template ready JSON)
     * 
     * 
     */


    
    private static function juice_Cataleya_Front_Dashboard_Theme (\Cataleya\Front\Dashboard\Theme $_Theme) 
    {

        
        
        // template info
        $json = array (
            'id'    =>  $_Theme->getID(), 
            'name' =>  $_Theme->getName(), 
            'folder_path'   =>  $_Theme->getFolderPath(), 
            'assets_path'   =>  $_Theme->getAssetsPath(), 
            'version'  =>  $_Theme->getVersion(),
            'page_templates' => $_Theme->getPageTemplates()
        );
        
        

        return $json;

    }

    
    
    
    
    
    
    
    
    
    
    /*
     * 
     * [ juiceDrop_DashboardTheme ]
     * ______________________________________________________________
     * 
     * @param \Cataleya\Front\Shop\Theme $_Theme
     * 
     * @return array (template ready JSON)
     * 
     * 
     */


    
    private static function juiceDrop_Cataleya_Front_Dashboard_Theme (\Cataleya\Front\Dashboard\Theme $_Theme) 
    {

        
        
        // theme info
        $json = array (
            'id'    =>  $_Theme->getID(), 
            'name' =>  $_Theme->getName(), 
            'folder_path'   =>  $_Theme->getFolderPath(), 
            'assets_path'   =>  $_Theme->getAssetsPath(), 
            'version'  =>  $_Theme->getVersion(),
            'page_templates' => $_Theme->getPageTemplates()
        );
        
        

        return $json;

    }

    
    
    
    
    
    
    
    
    
    
    
    
    


    /*
     * 
     * [ juice_Image ]
     * ______________________________________________________________
     * 
     * @param \Cataleya\Asset\Image $_Image
     * 
     * @return array (template ready JSON)
     * 
     * 
     */



    private static function juice_Cataleya_Asset_Image (\Cataleya\Asset\Image $_Image) 
    {


        $_info = array (

            'id'    =>  $_Image->getID(), 
            'hrefs' =>  $_Image->getHrefs(), 
        );

        return $_info;


    }

    
    
    
    
    
    



    /*
     * 
     * [ juice_Cart ]
     * ______________________________________________________________
     * 
     * @param \Cataleya\Customer\Cart $_Cart
     * @param \Cataleya\Store $_Store
     * 
     * @return array (template ready JSON)
     * 
     * 
     */


    private static function juice_Cataleya_Customer_Cart (\Cataleya\Customer\Cart $_Cart, \Cataleya\Store $_Store) 
    {
        
        // CART info
        $json = array (

            'cart_id'   =>  $_Cart->getCartId(), 
            'key'   =>  $_Cart->getSecureKey(), 
            'totalQuantity'  =>  0, 
            'totalAmount'  =>  0, 
            'items' =>  array (), 
            'newCartItem' => array (), 
            'anyOutOfStock' =>  FALSE

            );
        
        foreach ($_Cart as $_CartItem) {
            
            $json['items'][] = self::juice($_CartItem, $_Store);
            $stock = (int)$_CartItem->getProductOption()->getStock()->getValue($_Store);
        
            $json['totalQuantity'] += $_CartItem->getQuantity();
            $json['totalAmount'] += $_CartItem->getSubtotal();
            if ((float)$stock < (float)$_CartItem->getQuantity()) $json['cart']['anyOutOfStock'] = TRUE;
        
        }
        
        return $json;
        
        
    }
    
    
    
    
    
    /*
     * 
     * [ juice_CartItem ]
     * ______________________________________________________________
     * 
     * @param \Cataleya\Customer\Cart $_Cart
     * @param \Cataleya\Store $_Store
     * 
     * @return array (template ready JSON)
     * 
     * 
     */


    private static function juice_Cataleya_Customer_Cart_Item (\Cataleya\Customer\Cart\Item $_CartItem, \Cataleya\Store $_Store) 
    {
        
        
        $_Product = $_CartItem->getProduct();
        $tag_ids = $_Product->getProductTagIDs();
        
        // Load any category this product is associated with...
        foreach ($tag_ids as $tag_id) 
        {
            $tag = \Cataleya\Catalog\Tag::load($tag_id);
            if ($tag !== NULL) break;
        }
        
        if (empty($tag)) return [];
        
        $item_opt = $_CartItem->getProductOption();
        $opt_image = $item_opt->getPreviewImage();
        $stock = (int)$_CartItem->getProductOption()->getStock()->getValue($_Store);
        // $subtotal = $_CartItem->getSubtotal();
        
        

        // ATTRIBUTES
        
        $attributes = array ();

        foreach ($item_opt as $_Attribute) 
        {
            $_AttributeType = $_Attribute->getAttributeType();

            $attributes[] = array (
                'id' => $_Attribute->getID(), 
                'label' => $_AttributeType->getName(), 
                'value' => $_Attribute->getValue()
            );

        }



        $json = array (
            
            'id'    =>  $_CartItem->getID(),
            'title' =>   $_Product->getDescription()->getTitle('EN'), 
            'description' =>   $_Product->getDescription()->getText('EN'), 
            'product_id'    =>  $_Product->getProductId(), 
            'option_id'    =>  $item_opt->getID(), 
            'attributes'  =>  $attributes, 
            'quantity'  =>  $_CartItem->getQuantity(), 
            'price' => $_CartItem->getCost(), 
            'stock' =>  $stock, 
            'subtotal'  => $_CartItem->getSubtotal(), 
            'image' => ($opt_image instanceof \Cataleya\Asset\Image) ? $opt_image->getHrefs() : $_Product->getPrimaryImage()->getHrefs(), 
            'href'   =>  $_Store->getShopLanding() . $tag->getURLRewrite()->getKeyword() . '/' . $_Product->getURLRewrite()->getKeyword() . '/' . $_Product->getProductId()

            );
        
        
        
        return $json;
        
    }
    







    /*
     * 
     * [ juice_Order]
     * ______________________________________________________________
     * 
     * @param \Cataleya\Store\Order $_Order
     * 
     * @return array (template ready JSON)
     * 
     * 
     */



    private static function juice_Cataleya_Store_Order (\Cataleya\Store\Order $_Order) 
    {

        $_info = array (
            'id'    =>   $_Order->getID(), 
            'orderNum'    =>   str_pad(strval($_Order->getID()), 5, '0', STR_PAD_LEFT), 
            'status' => $_Order->getOrderStatus(),
            'payment_status' => $_Order->getPaymentStatus(),
            'total' =>  number_format($_Order->getTotalCost(), 2), 
            'grandTotal'    =>  number_format($_Order->getGrandTotal(), 2), 
            'transactions' => $_Order->getReceipts(),
            'total_paid' =>  number_format($_Order->getTotalPaid(), 2), 
            'shippingCharges'   =>  number_format($_Order->getShippingCharges(), 2), 
            'quantity'  =>  $_Order->getQuantity(), 
            'prettyQuantity'  => \Cataleya\Helper::countInEnglish($_Order->getQuantity()), 
            'population'    =>  $_Order->getPopulation(), 
            'orderDate' =>  $_Order->getOrderDate(FALSE)->format('h:ia, d M, Y'), 
            'Items' =>  array (), 
            'Discounts' =>  $_Order->getDiscounts(), 
            'Taxes' =>  $_Order->getTaxes(), 

            'Customer' => self::juice($_Order->getCustomer()), 
            'Currency'  =>  self::juice($_Order->getCurrency()), 
            'Store' =>  self::juice($_Order->getStore()),
            
            'Shipping'    =>  array (
                'amount'  =>  $_Order->getShippingCharges(), 
                'amountBeforeTax'  =>  $_Order->getShippingCharges(FALSE, TRUE), 
                'description'  =>  $_Order->getShippingDescription(), 
                'Taxes' =>  $_Order->getShippingTaxes()
                    ), 
                
            'ShippingAddress' => self::juice($_Order->getShippingAddress())
        
        );

        // Inject pretty_amount into discount vars
        if (!empty($_info['Discounts'])) {
            foreach ($_info['Discounts'] as $k=>$d) {
                $_info['Discounts'][$k]['pretty_amount'] = number_format($d['less'], 2);
            }
        }



        // Order Details
        foreach ($_Order as $_OrderDetail) $_info['Items'][] = self::juice($_OrderDetail);


        return $_info;


    }




    
    

    /*
     * 
     * [ juice_OrderDetail]
     * ______________________________________________________________
     * 
     * @param \Cataleya\Store\OrderDetail $_Order
     * 
     * @return array (template ready JSON)
     * 
     * 
     */



    private static function juice_Cataleya_Store_OrderDetail (\Cataleya\Store\OrderDetail $_OrderDetail) 
    {


        $_info = array (
            'id'    =>   $_OrderDetail->getID(), 
            'itemName'  =>  $_OrderDetail->getItemName(), 
            'itemDescription'   =>  $_OrderDetail->getItemDescription(), 
            'price' =>  number_format($_OrderDetail->getPrice(), 2), 
            'originalPrice' =>  number_format($_OrderDetail->getOriginalPrice(), 2), 
            'subtotal'  =>  number_format($_OrderDetail->getSubtotal(), 2), 
            'originalSubtotal'  =>  number_format($_OrderDetail->getSubtotal(FALSE), 2), 
            'quantity'  =>  $_OrderDetail->getQuantity() 

        );

        return $_info;


    }








        

 
}


