<?php



namespace Cataleya\Front\Dashboard\REST\v1\Resource;



/*
 *
 * 	@Package: Cataleya
 * 	@Class: \Cataleya\Front\Dashboard\REST\v1\Resource\OAuth
 *
 * 	(c) 2014 Fancy Paper Planes
 *
 *
 *
 */

class OAuth 
extends \Cataleya\Front\Dashboard\REST\v1\Resource
{
    
    



    public function __construct() {
        
 
        
    }
    
    
    
    public static function load () {
       

        return new static ();
 
        
    }
    
    
    




    /*
     * 
     * [ execute ]
     * ______________________________________________________________
     * 
     * 
     * 
     * 
     */


    public function execute () 
    {

        if ($_SERVER['REQUEST_METHOD'] !== 'POST') 
        {
            header('HTTP/1.0 405 Method Not Allowed'); exit();
        }



        
        // SANITIZE INPUT DATA...
        $_params = filter_input_array(INPUT_POST, [
                                        'u'	=>	FILTER_VALIDATE_EMAIL, 
                                        'p'	=>	[
                                                    'filter' =>	FILTER_VALIDATE_REGEXP, 
                                                    'options'	=>	['regexp' => '/^[-_\W\s\.A-Za-z0-9]{1,50}$/']
                                                ] 
                                    ]);
        
        
        if ($_params['u'] === false || $_params['p'] === false) {

            header('HTTP/1.0 400 Bad Request'); exit();
        }
        
         
        self::$_User = \Cataleya\Admin\User::login($_params['u'], $_params['p']);
        if (self::$_User === NULL)
        {
                
            (defined('DEBUG_MODE') && DEBUG_MODE == TRUE) || sleep(4);
            
            header('HTTP/1.0 401 Unauthorized'); exit();
            
        } else {

            // If all is well: generate JWT token with lib and echo JSON response..
        
        
            // Create JWT s
            $_secret = self::getJWTSecret();
            $_signer = new \Lcobucci\JWT\Signer\Hmac\Sha512();

            // Create token as array...
            $issuer = self::getJWTIssuer();


            // 1. Regular JWT: Used for api calls, expires in 20 mins...
            $_new_jti_0 = base64_encode(mcrypt_create_iv(32));
            $_api_token = (new \Lcobucci\JWT\Builder())->setIssuer($issuer) // (iss claim)
                                    ->setAudience($issuer) // (aud claim)
                                    ->setId($_new_jti_0, true) // (jti claim), replicating as a header item
                                    ->setIssuedAt(time()) // (iat claim)
                                    ->setNotBefore(time() + 60) // (nbf claim)
                                    ->setExpiration(time() + 15*60) // (exp claim)
                                    ->set('uid', $_User->getID()) // Configures a new claim, called "uid"
                                    ->sign($_signer, $_secret) // creates a signature using $_secret as key
                                    ->getToken(); // Retrieves the generated token

            // 2. Refresh JWT: Used to get new JWT (1) each time it expires, shelf life is 7 days...
            $_new_jti_1 = base64_encode(mcrypt_create_iv(32));
            $_User->setJTI($_new_jti_1);
            $_refresh_token = (new \Lcobucci\JWT\Builder())->setIssuer($issuer) // (iss claim)
                                    ->setAudience($issuer) // (aud claim)
                                    ->setId($_new_jti_1, true) // (jti claim), replicating as a header item
                                    ->setIssuedAt(time()) // (iat claim)
                                    ->setNotBefore(time() + 60) // (nbf claim)
                                    ->setExpiration(time() + (7*24*60*60)) // (exp claim)
                                    ->set('uid', $_User->getID()) // Configures a new claim, called "uid"
                                    ->sign($_signer, $_secret) // creates a signature using $_secret as key
                                    ->getToken(); // Retrieves the generated token

            echo json_encode([ 
                'jwt' => $_api_token, 
                'refresh' => $_refresh_token
            ]);
            exit(1);

        }
        



        /*
        // PCI-DSS / PA-DSS requirements for lockouts and intervals:
        define('ADMIN_LOGIN_LOCKOUT_TIMER', (30 * 60));
        define('ADMIN_LOGIN_LOCKOUT_AFTER', 10);
        define('ADMIN_PASSWORD_EXPIRES_INTERVAL', strtotime('- 90 day'));
         */



    }
    
    
    
    
    
    
    
    
    

        

 
}


