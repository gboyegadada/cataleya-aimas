<?php



namespace Cataleya\Front\Dashboard\REST\v1\Resource;



/*
 *
 * 	@Package: Cataleya
 * 	@Class: \Cataleya\Front\Dashboard\REST\v1\Resource\Refresh
 *
 * 	(c) 2014 Fancy Paper Planes
 *
 *
 *
 */

class Refresh  
extends \Cataleya\Front\Dashboard\REST\v1\Resource
{
    
    



    public function __construct() {
        
 
        
    }
    
    
    
    public static function load () {
       

        return new static ();
 
        
    }
    
    
    




    /*
     * 
     * [ execute ]
     * ______________________________________________________________
     * 
     * 
     * 
     * 
     */


    public function execute () 
    {

        self::verifyToken();

    
        // Create JWT s
        $_secret = self::getJWTSecret();
        $_signer = new \Lcobucci\JWT\Signer\Hmac\Sha512();

        // Create token as array...
        $issuer = self::getJWTIssuer();


        // 1. Regular JWT: Used for api calls, expires in 20 mins...
        $_new_jti_0 = base64_encode(mcrypt_create_iv(32));
        $_api_token = (new \Lcobucci\JWT\Builder())->setIssuer($issuer) // (iss claim)
                                ->setAudience($issuer) // (aud claim)
                                ->setId($_new_jti_0, true) // (jti claim), replicating as a header item
                                ->setIssuedAt(time()) // (iat claim)
                                ->setNotBefore(time() + 60) // (nbf claim)
                                ->setExpiration(time() + 15*60) // (exp claim)
                                ->set('uid', $_User->getID()) // Configures a new claim, called "uid"
                                ->sign($_signer, $_secret) // creates a signature using $_secret as key
                                ->getToken(); // Retrieves the generated token

        // 2. Refresh JWT: Used to get new JWT (1) each time it expires, shelf life is 7 days...
        $_new_jti_1 = base64_encode(mcrypt_create_iv(32));
        $_User->setJTI($_new_jti_1);
        $_refresh_token = (new \Lcobucci\JWT\Builder())->setIssuer($issuer) // (iss claim)
                                ->setAudience($issuer) // (aud claim)
                                ->setId($_new_jti_1, true) // (jti claim), replicating as a header item
                                ->setIssuedAt(time()) // (iat claim)
                                ->setNotBefore(time() + 60) // (nbf claim)
                                ->setExpiration(time() + (7*24*60*60)) // (exp claim)
                                ->set('uid', $_User->getID()) // Configures a new claim, called "uid"
                                ->sign($_signer, $_secret) // creates a signature using $_secret as key
                                ->getToken(); // Retrieves the generated token

        echo json_encode([ 
            'jwt' => $_api_token, 
            'refresh' => $_refresh_token
        ]);

        exit();

    }
    
    

    
    
    /**
     * verifyToken
     *
     * @return void
     */
    private static function verifyToken () {

        $token = self::parseJWT();


        $_User = \Cataleya\Admin\User::load((int)$token->getClaim('uid'));

        if (empty($_User)) { 
            header('HTTP/1.0 401 Unauthorized'); exit();
        }


        // 5. Validate with JWT library (valid, untampered, and not expired)..
        $data = new \Lcobucci\JWT\ValidationData(); // It will use the current time to validate (iat, nbf and exp)

        $data->setIssuer(self::getJWTIssuer());
        $data->setAudience(self::getJWTIssuer());
        $data->setId($_User->getJTI());

        if ($token->validate($data) !== true)  { 
            $_User->setJTI('');
            header('HTTP/1.0 401 Unauthorized'); exit();
        }




    }
    
    
    
    
    
    

        

 
}


