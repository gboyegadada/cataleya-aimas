<?php



namespace Cataleya\Front\Dashboard\REST;
use \Cataleya\Front\Dashboard\REST;


/*
 *
 * 	@Package: Cataleya
 * 	@Class: \Cataleya\Front\Dashboard\REST\Controller
 *
 * 	(c) 2014 Fancy Paper Planes
 *
 *
 *
 */

class Controller 
{
    
    
    
    private static $_instance;
    private static $_post_data = [];



    public function __construct() {


    }


    public static function load () {




        switch (self::getVersion()) 
        {
            
            case 'cataleya.1.0':
                return REST\v1\Resource::load();

            case 'cataleya.1.1':
                return REST\v1\Resource::load();

            default:
                header('HTTP/1.0 404 Resource not found'); exit();
        }
        
    }
    



    public static function getVersion () 
    {

        static $_version;
        if (empty($_version)) 
        {
            preg_match(
            '!^application\/(?<version>[a-z0-9\.\-_]+)\+json$!',
            strtolower ($_SERVER['HTTP_ACCEPT']),  
            $_m
            ); // 'Application/cataleya.1.0+json'

            $_version = isset($_m['version']) ? $_m['version'] : null;
        }
        
        return $_version;

    }
    

        

 
}


