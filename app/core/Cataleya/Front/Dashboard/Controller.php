<?php

namespace Cataleya\Front\Dashboard;


// if (!defined ('IS_ADMIN_FLAG') || IS_ADMIN_FLAG !== TRUE) die("Illegal Access");



/**
 *
 * 	@Package: Cataleya
 * 	@Class: \Cataleya\Front\Dashboard\Controller 
 *
 * 	(c) 2015 Fancy Paper Planes
 *
 *
 *
 */
class Controller 
{
    
    
    protected static $_RW = array (
        'c' => '404', 
        'a' => 'void', 
        'f' => 'HTML', 
        'ref' => ''
    );

    protected static $_base_url;
    
    private static $_query_ready = false;
    



    public function __construct() {
       
        
    }
    
    
    
    



    /**
     * load
     *
     * @return void
     */
    public static function load () 
    {

        self::$_base_url = __url('dash');
        
        $m = array ();
        $_type = '404';
        $_RW = array (
            'ref' => $_SERVER['REQUEST_URI']
        );
        
        
        if (preg_match('!^/' . ROOT_URI . 'dashboard/(?<uri>([A-Za-z0-9_\-]+[/]?)+)?(?<ext>\.[a-zA-Z]{2,10})?(\?.*)?$!', $_SERVER['REQUEST_URI'], $_matches) === 0) { 
            // Assume ROOT_URI!
            $_matches = array ('uri'=>'');
        }

        
        // File extension...
        $_RW['f'] = (!empty($_matches['ext'])) ? $_matches['ext'] : '.html';
        
        if (!empty($_matches['uri'])) {


            $_patterns = array (
                'root' => '~^/?$~i', 
                'app_page' => '~^app/([^/.]+)/([^/.]+)(\.[a-zA-Z]{2,4})?/?$~i', 
                'others_default' => '~^([^/.]+)(\.[a-zA-Z]{2,4})?/?$~i', 
                'others_with_action' => '~^([^/.]+)/([^/.]+)(\.[a-zA-Z]{2,10})?/?$~i'
            );


            foreach ($_patterns as $k=>$p) {

               $m = array ();
               if (preg_match($p, $_matches['uri'], $m) > 0) {

                   $_type = $k;

                   break;
               }

            }
        
        } else {
            $_type = 'root';
        }
        
           
        
        $m[1] = filter_var($m[1], FILTER_VALIDATE_REGEXP, array('options'=>array('regexp' => '/^[A-Za-z0-9\-]{1,200}$/')));
        
        
        switch ($_type) 
        {
            
            
            case 'root':
                
                
                $_RW['s'] = filter_var(
                    $_GET['s'], 
                    FILTER_VALIDATE_REGEXP, 
                    array('options'=>array('regexp' => '/^[A-Za-z0-9\-]{1,200}$/'))
                    );
             
                
                
                // Vanity URL...
                if ($_RW['s'] !== FALSE) {

                    $url = SHOP_ROOT . $_RW['s'];
                    Header("Location: $url");
                    exit();
                } else {
                    $_RW['c'] = 'index';
                }

                break;
                        
            case 'home':
                $_RW['c'] = 'home';
                $_RW['s'] = $m[1];
                
                break;
            
            case 'app_page':
                $_RW['c'] = 'app';
                $_RW['a'] = $m[1];
                $_RW['app_pg'] = $m[2];
                
                break;
            
            
            case 'others_default':
                $_RW['c'] = $m[1];
                $_RW['a'] = 'void';
                break;
            
            case 'others_with_action':
                $_RW['c'] = $m[1];
                $_RW['a'] = $m[2];
                break;
            
            default:
                $_RW = array (
                    'c' => '404', 
                    'a' => 'void', 
                    'f' => '.html', 
                    'ref' => ''
                );
                break;
            
        }
        
        
        
        
        
        
        
        $_RW['f'] = (!empty($_RW['f'])) ? strtoupper(substr($_RW['f'], 1)) : 'HTML';
        
        
        self::$_RW = $_RW;
        self::$_query_ready = TRUE;
        
        

        
        
        switch (self::$_RW['c']) 
        {
            
            case 'rpc': return \Cataleya\Front\Dashboard\RPC\Controller::load();

            case 'api': return \Cataleya\Front\Dashboard\REST\Controller::load();

            default: return \Cataleya\Front\Dashboard\HTML\Controller::load();
                
        }
    }
    
    
    
    
    
    
    
    
    /**
     * 
     * Check is request query has been processed
     * 
     * @return bool Check is request query has been processed...
     * 
     */
    public static function isReady () 
    {
        return self::$_query_ready;
    }

    
    
    /**
     * 
     * RETURN URL Rewrite params...
     * 
     * @return Array Contains URL Rewrite params like shop, controller, and requested file format.
     * 
     */
    public static function getParams () 
    {
        return self::$_RW;
    }

    
    
    /**
     * 
     * 
     * @return string Controller action.
     * 
     */
    public static function getAction () 
    {
        return self::$_RW['a'];
    }
    


    /**
     * 
     * 
     * @return string Controller.
     * 
     */
    public static function getControllerHandle () 
    {
        return self::$_RW['c'];
    }
    
    
    

    /**
     * 
     * RETURN URI...
     * 
     * @return string URI.
     * 
     */
    public static function getURI () 
    {
        return self::$_RW['ref'];
    }
    
    


    /**
     * 
     * RETURN BASE URL...
     * 
     * @return string URL.
     * 
     */
    public static function getDashLanding () 
    {
        return self::$_base_url;
    }
    

    /**
     * 
     * RETURN URL Rewrite params...
     * 
     * @return string expected ContentType (HTML, JSON e.t.c.)
     * 
     */
    public static function getContentType () 
    {
        return self::$_RW['f'];
    }
    
    
    
    


    

 
}


