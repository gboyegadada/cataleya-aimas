<?php


namespace Cataleya\Front\Form;




/*
 *
 * 	@Package: Cataleya
 * 	@Class: \Cataleya\Front\Form\Param
 *
 * 	(c) 2014 Fancy Paper Planes
 *
 *
 *
 */

class Param
{
    
    
    protected $_data = array();


    public function __construct($_data = array ()) {
       
        $this->_data = $_data;
        
    }
    
    
    public function __get($name) {
        return (isset ($this->_data[$name])) ? $this->_data[$name] : NULL;
    }
    
    
    public function __set($name, $value) {
        if (isset ($this->_data[$name])) $this->_data[$name] = $value;
        
        return $this;
    }

    



    /*
     * 
     * [ getValue ]
     * ______________________________________________________________
     * 
     * 
     * 
     * 
     */

    public function getValue () 
    {
        return $this->_data['value'];
    }

    

    public function getRawValue () 
    {
        return $this->_data['value_raw'];
    }



    /*
     * 
     * [ getName ]
     * ______________________________________________________________
     * 
     * 
     * 
     * 
     */

    public function getName () 
    {
        return $this->_data['name'];
    }




    /**
     * 
     * @return boolean
     * 
     */
    public function isHealthy () 
    {
        return ($this->_data['healthy']) ? TRUE : FALSE;
    }
    
    
    

    /**
     * 
     * @return boolean
     * 
     */
    public function isConfirm () 
    {
        return ($this->_data['is_confirm']) ? TRUE : FALSE;
    }

    

    /*
     * 
     * [ isRequired ]
     * ______________________________________________________________
     * 
     * 
     * 
     * 
     */

    public function isRequired () 
    {
        return ($this->_data['required']) ? TRUE : FALSE;
    }

    
    
    

    /*
     * 
     * [ getAutofill ]
     * ______________________________________________________________
     * 
     * 
     * 
     * 
     */

    public function getAutofill () 
    {
        return $this->_data['autofill'];
    }

    

    
    /*
     * 
     * [ getErrorText ]
     * ______________________________________________________________
     * 
     * 
     * 
     * 
     */

    public function getErrorText () 
    {
        return $this->_data['error_text'];
    }

    



    /**
     * 
     * @return string
     * 
     */
    public function getReferencedField () 
    {
        return $this->_data['ref_field'];
    }


    
 
        

 
}


