<?php


namespace Cataleya\Front;




/*
 *
 * 	@Package: Cataleya
 * 	@Class: \Cataleya\Front\Controller 
 *
 * 	(c) 2014 Fancy Paper Planes
 *
 *
 *
 */

class Controller 
{
    
    

    public function __construct() {
       
        
    }
    
    
    
    


    /*
     * 
     * [ load ]
     * ______________________________________________________________
     * 
     * 
     * 
     * 
     */


    public static function load () 
    {
        
        
        if (preg_match('!^/' . ROOT_URI . 'dashboard(/.*)?$!', $_SERVER['REQUEST_URI'], $_matches) > 0)
        {  

            define ('IS_ADMIN_FLAG', true);

            define('TOKEN_EXP', CURRENT_DT-(60*60)); // After 60 min...
            define('SESSION_EXP', (30*60)); // After 30 min...
            define('SESSION_REGEN_AFTER', (30 * 60));
            define('SESSION_PREFIX', 'DASH_');
            define('SESSION_NAME', 'dash');

            define('LOGIN_EXPIRES_AFTER', (30*60)); // After 30 min...

        } else {

            define ('IS_ADMIN_FLAG', false);

            define('TOKEN_EXP', CURRENT_DT-(60*60)); // After 60 min...
            define('SESSION_EXP', (60*60)); // After 60 min...
            define('SESSION_PREFIX', 'FRONT_');
            define('SESSION_NAME', 'front');
            
            define('LOGIN_EXPIRES_AFTER', (60*60)); // After 60 min...

        }




        ///////////////////  REGISTER SESSION HANDLER //////////////

        if (IS_ADMIN_FLAG === true) {
                if (!$SESS_LIFE = (SESSION_EXP > 3600 ? 900 : SESSION_EXP)) $SESS_LIFE = (SESSION_EXP > 3600 ? 900 : SESSION_EXP);
        } else {
                if (!$SESS_LIFE = get_cfg_var('session.gc_maxlifetime')) $SESS_LIFE = SESSION_EXP; //1440;
        }

        if (defined('DISABLE_SECURE_SESSION') && DISABLE_SECURE_SESSION) { /* don't load session class */ }
        else {
            \Cataleya\Session::setSessionLifeTime($SESS_LIFE);
            \Cataleya\Session::load();
        }


        // INIT SESSION
        session_name(SESSION_NAME);
        \Cataleya\Session::init_session_cookie(array('path'=>'/'));
        session_start();




        // Return Dashboard | Shop Front Controller
        return (IS_ADMIN_FLAG) 
            ? \Cataleya\Front\Dashboard\Controller::load()
            : \Cataleya\Front\Shop\Controller::load();

 

        
    }
    


    
} 
