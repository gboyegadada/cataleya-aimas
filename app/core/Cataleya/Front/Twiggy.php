<?php


namespace Cataleya\Front;



/*

CLASS Twiggy 

*/



class Twiggy 
{
	
	private static $_twig_instance;



    /**
     * init
     *
     * @param string $_PATH
     * @param bool $_FORCE_INIT
     * @return void
     */
    public static function init ($_PATH = NULL, $_FORCE_INIT = FALSE) 
    {


		if (!isset(self::$_twig_instance) || $_FORCE_INIT === TRUE)
		{
                    if (!file_exists($_PATH)) $_PATH = __path('skin');
                    
                    $loader = new \Twig_Loader_Filesystem($_PATH);
                    self::$_twig_instance = new \Twig_Environment($loader, array('cache'=>FALSE, 'autoescape'=>FALSE));
                    
                    self::$_twig_instance->addExtension(new \Twig_Extensions_Extension_Text());
                    
                    
                    // cache_dir: SHARED_INC_PATH.'templates/twig/cache'
		
		}

    } 

        
	
	/**
	 * getInstance
	 *
	 * @param string $_PATH
	 * @param bool $_FORCE_INIT
	 * @return void
	 */
	public static function getInstance ($_PATH = NULL, $_FORCE_INIT = FALSE) 
	{
                
        self::init($_PATH, $_FORCE_INIT);
		
		return self::$_twig_instance;
		
	}	
        
        
        
	/**
	 * loadTemplate
	 *
	 * @param string $_template
	 * @return void
	 */
	public static function loadTemplate ($_template = NULL) 
	{
        if ($_template === NULL) return NULL;
		
		return self::getInstance()->loadTemplate($_template);
		
	}	
	
	
	
	
	
}


