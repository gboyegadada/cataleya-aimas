<?php


namespace Cataleya\Front;




/*
 *
 * 	@Package: Cataleya
 * 	@Class: \Cataleya\Front\Form
 *
 * 	(c) 2014 Fancy Paper Planes
 *
 *
 *
 */

class Form extends \Cataleya\Collection 
{
    
    
    
    protected static $_instance, $_cache_instance;

    protected static $_skip_autofill = array (
        'password', 
        'password_confirm'
    );
    
    protected static $_error_text = array (
        
        'int' => 'Please enter a number.', 
        'float' => 'Please enter a number.',
        'alphanum' => 'Please enter alphanumerical text [a-Z or 0-9].',
        'alpha' => 'Please enter alphabets only [a-z].',
        'num' => 'Please enter numbers only [a-z].',
        'address' => 'Please enter an address.',
        
        'name' => 'Please type in a name (Make sure to only use the characters: A-Z, a-z, 0-9, dashes and dots).', 
        'firstname' => 'Please type in your first name (Make sure to only use the characters: A-Z, a-z, 0-9, dashes and dots).', 
        'lastname' => 'Please type in your last name (Make sure to only use the characters: A-Z, a-z, 0-9, dashes and dots).', 
        'email' => 'Please provide a valid email address', 
        'myemail' => 'Please enter your email address', 
        'phone' => 'Please enter a valid phone number.', 
        'date' => 'Please choose a date.', 
        'password' => 'Please enter a password that is at least 8 characters long, and contains 1 uppercase letter and a number. <br/>- OR -<br/> Use a pass-phrase instead. For example: "I love bvlgari".', 
        'mypassword' => 'Please enter your password.', 
        'confirm' => 'Remember to re-type your {{name}} (must be the same as the first).', 
        'postalcode' => 'Please enter your postal code.', 
        'iso2' => 'Please choose a country.'
    );
    
    protected static $_expected_params = array ();


    protected $_mode = 0; // PARAM_OBJECT || PARAM_ARRAY
    protected $_params_arrays = array();
    protected $_params_objects = array();
    
    protected $_validation_failed = false;

    const PARAM_OBJECT = 0;
    const PARAM_ARRAY = 1;
    const FROM_INPUT = 0;
    const FROM_CACHE = 1;
    
    
    // To be used by [\Cataleya\Collection]
    protected $_collection = array();
    protected $_position = 0;
    protected $_population = 0;
    


    public function __construct($_INPUT_TYPE = INPUT_POST, $_LOAD_FROM = self::FROM_INPUT, array $_expected_params=null) {
        
        if ($_LOAD_FROM === self::FROM_CACHE) {
              $this->_params_objects = $this->_params_arrays = [];
        
              if (isset($_SESSION[SESSION_PREFIX.'FORM_CACHE'])) {
                  $this->_params_arrays = json_decode($_SESSION[SESSION_PREFIX.'FORM_CACHE'], true);
                  
                  foreach ($this->_params_arrays as $_label=>$_param_data) {
                      $this->_params_objects[$_label] = new \Cataleya\Front\Form\Param($_param_data);
                      $this->_collection[] = $_label;
                  }
                  
                  $this->_population = count($this->_params_arrays);
              }
              
          
        } else { 

            $_raw = $_types = $_required = [];
            $_INPUT = ($_INPUT_TYPE === INPUT_POST) ? $_POST : $_GET;
            
            foreach ($_INPUT as $k=>$v) {
                list ($_n, $_t) = explode('-', $k);
                if (!empty($_n) && !empty($_t) && $_t === $_SESSION[SESSION_PREFIX.'FORM_TOKEN']) {
                    $_raw[$_n] = $v;
                }
            }

            foreach ($_expected_params as $k=>$v) {
                $_ = ["!","*"];
                if (in_array(substr($k,-1), $_)) {
                    $_name = substr($k,0,-1);
                    $_required[$_name] = true;
                } else {
                    $_name = $k;
                    $_required[$_name] = false;
                }

                $_types[$_name] = $v;
            }
            


            $_params = \Cataleya\Helper\Validator::INPUT($_INPUT_TYPE, $_types);
            $this->_population = count($_params);

            
            foreach ($_params as $k=>$v) {
                $_result = [ 
            
                    'name' => $k, 
                    'value' => $v, 
                    'value_raw' => (isset($_raw[$k])) ? $_raw[$k] : null, 
                    'healthy' => (($_params[$k] !== null) ? true : false), 
                    'autofill' => ($v !== null && !in_array($_types[$k], self::$_skip_autofill)) ? $v : '', 
                    'error_text' => ($v === null && isset(self::$_error_text[$_types[$k]])) ? str_replace('{{name}}', $k, self::$_error_text[$_types[$k]]) : '', 
                    'required' => $_required[$k], 
                    
                    'is_confirm' => (substr($k,0,1) === '_'), 
                    'ref_field' => (substr($k,0,1) === '_') ? substr($k,1) : null
                ];
                

                $this->_params_arrays[$k] = $_result;
                $this->_collection[] = $k;
            }
                    

            foreach ($this->_params_arrays as $k=>$p) {
                if ($p['is_confirm'] && $this->_params_arrays[$p['ref_field']]['value'] !== $p['value']) {
                    $this->_params_arrays[$k]['healthy'] = false;
                    $this->_params_arrays[$k]['error_text'] = str_replace('{{name}}', $p['ref_field'], self::$_error_text['confirm']);
                }


                $this->_params_objects[$k] = new \Cataleya\Front\Form\Param($this->_params_arrays[$k]);

                if ($_required[$k] && !$this->_params_arrays[$k]['healthy']) $this->_validation_failed = true;
            }
            

        }
            
            
        $this->pageSetup();

            
        
    }
    
    
    

    /**
     * 
     * @return \Cataleya\Front\Form\Param
     * 
     */
    public function current()
    {
        $_label = $this->_collection[$this->_position];
        
        return ($this->_mode === self::PARAM_ARRAY) 
                ? $this->_params_arrays[$_label] 
                : $this->_params_objects[$_label];
        
    }
    


    /**
     * 
     * @param int $_INPUT_TYPE
     * @param int $_LOAD_FROM
     * @return \Cataleya\Front\Form
     * 
     */
    static public function load ($_INPUT_TYPE = INPUT_POST, $_LOAD_FROM = self::FROM_INPUT, array $_expected_params = array ()) 
    {
            /* Note: Trying to implement similar validation approach I used in some of the admin api scripts. No time. Will continue in next iteration of Cataleya.  */
            (!empty($_expected_params)) ? self::$_expected_params = $_expected_params : null;
            

            if ($_LOAD_FROM === self::FROM_CACHE && empty(self::$_cache_instance)) {
                
                $_Class = __CLASS__;
                self::$_cache_instance = new $_Class ($_INPUT_TYPE, $_LOAD_FROM);
                
            }
            
            
            if ($_LOAD_FROM !== self::FROM_CACHE && empty(self::$_instance)) {
                
                $_LOAD_FROM = self::FROM_INPUT;
                
                $_Class = __CLASS__;
                self::$_instance = new $_Class ($_INPUT_TYPE, $_LOAD_FROM, $_expected_params);
                
            }
            
            


            ////////////////// RETURN INSTANCE ////////////////////

            return ($_LOAD_FROM === self::FROM_CACHE) 
                    ? self::$_cache_instance 
                    : self::$_instance;

     }




    
    
    
    
    
    /*
     * 
     * [ setMode ]
     * ______________________________________________________________
     * 
     * 
     * 
     * 
     */


    
    
    public function setMode ($_PARAM_TYPE = self::PARAM_OBJECT) 
    {

            if (in_array($_PARAM_TYPE, [self::PARAM_OBJECT, self::PARAM_ARRAY])) 
                            $this->_mode =  $_PARAM_TYPE;
            
            return $this;
            
    }

    
    
    
    /*
     * 
     * [ getMode ]
     * ______________________________________________________________
     * 
     * 
     * 
     * 
     */


    
    
    public function getMode () 
    {

            return $this->_mode;
            
    }

    
    

    /*
     * 
     * [ isHealthy ]
     * ______________________________________________________________
     * 
     * 
     * 
     * 
     */

    public function isHealthy () 
    {
        return ($this->_validation_failed === true) ? false : true;
    }

    
    
    
    

    /*
     * 
     * [ validationFailed ]
     * ______________________________________________________________
     * 
     * 
     * 
     * 
     */

    public function validationFailed () 
    {
        return $this->_validation_failed;
    }
    
    
    
    

    /*
     * 
     * [ hasParams ]
     * ______________________________________________________________
     * 
     * 
     * 
     * 
     */


    
    
    public function hasParams () 
    {
        $_params = func_get_args();

         $_has_params = true;
         
         foreach ($_params as $_name) 
             if (
                 !in_array($_name, $this->_collection)
                 ) $_has_params = false;
             
        return $_has_params;
            
    }
    
    
    
    
    
    
    
    
    

    

    /*
     * 
     * [ getParams ]
     * ______________________________________________________________
     * 
     * 
     * 
     * 
     */


    
    
    public function getParams ($_PARAM_TYPE = self::PARAM_OBJECT) 
    {

            return ($_PARAM_TYPE === self::PARAM_OBJECT) 
                    ? $this->_params_objects 
                    : $this->_params_arrays;
            
    }







    /*
     * 
     * [ getParam ]
     * ______________________________________________________________
     * 
     * 
     * 
     * 
     */


    
    
    public function getParam ($_name = '', $_PARAM_TYPE = self::PARAM_OBJECT) 
    {
            
        if (is_scalar($_name) && isset($this->_params_arrays[$_name])):
            
            return ($_PARAM_TYPE === self::PARAM_OBJECT) 
                    ? $this->_params_objects[$_name] 
                    : $this->_params_arrays[$_name];
        
        else:
            
            return null;
        
        endif;
            
    }



    
    


    /*
     * 
     * [ cache ]
     * ______________________________________________________________
     * 
     * 
     * 
     * 
     */


    
    
    public function cache () 
    {
        $_SESSION[SESSION_PREFIX.'FORM_CACHE'] = json_encode($this->getParams(self::PARAM_ARRAY));
        
        return true;
    }
    
    
    

    
 
        

 
}


