<?php


namespace Cataleya\Front;



/*

CLASS WEB HOOK

*/



class WebHook implements \Cataleya\System\Event\Listener 
{
	
	
	private $_data = array();
	private $_modified = array();
        
	private $dbh, $e;
	




	/*
	 *
	 * [ __construct ]
	 * ________________________________________________________________
	 *
	 *
	 *
	 */
	 
	 
	protected function __construct ()
	{
		
            
                parent::__construct();
                
		// Get database handle...
                $this->dbh = \Cataleya\Helper\DBH::getInstance();
		
		// Get error handler
		$this->e = \Cataleya\Helper\ErrorHandler::getInstance();
		
	}
	




	/*
	 *
	 * [ __destruct ]
	 * ________________________________________________________________
	 *
	 *
	 *
	 */

        public function __destruct()
        {
                    $this->saveData();
        }
	




	/**
         * 
         * @param integer $id
         * @return \Cataleya\Front\WebHook 
         * 
         */
	public static function load ($id = 0)
	{
		
                if ($id == 0 || filter_var($id, FILTER_VALIDATE_INT) == FALSE) return NULL;
                
		
                // Create instance...
                $_objct = __CLASS__;
                $instance = new $_objct;
		
		// LOAD DATA
		static $instance_select, $param_instance_id;
		
			
		if (!isset($instance_select)) {
			// PREPARE SELECT STATEMENT...
			$instance_select = $instance->dbh->prepare('SELECT * FROM web_hooks WHERE hook_id = :instance_id LIMIT 1');
			$instance_select->bindParam(':instance_id', $param_instance_id, \PDO::PARAM_INT);
		}
		
		
		$param_instance_id = $id;
		
		if (!$instance_select->execute()) $instance->e->triggerException('
												Error in class (' . __CLASS__ . '): [ ' . 
												implode(', ', $instance_select->errorInfo()) . 
												' ] on line ' . __LINE__);
		
		$instance->_data = $instance_select->fetch(\PDO::FETCH_ASSOC);
                if (empty($instance->_data)) return NULL;
		


		return $instance;	
		
		
	}



	/**
         * 
         * @param array $_config e.g. array (
         *       'name' => 'string', 
         *       'description' => 'string', 
         *       'scope' => 'string',  
         *       'headers' => 'array', 
         *       'fields' => 'string', 
         *       'is_active' => 'boolean', 
         *       'format' => 'string', 
         *       'client_id' => 'string', 
         *       'url' => 'string'
         *   );
         * @return type
         * 
         */
	static public function create ($_config = array ()) 
	{
		 
            $_expected_keys = array (
                'name' => 'string', 
                'description' => 'string', 
                'scope' => 'string',  
                'headers' => 'array', 
                'fields' => 'string', 
                'is_active' => 'boolean', 
                'format' => 'string', 
                'client_id' => 'string', 
                'url' => 'string'
            );
            
            
            $_default_params = array ( 
                'name' => 'Web Hook ', 
                'description' => 'No Description', 
                'is_active' => FALSE, 
                'format' => 'json'
                        );
            $_config = array_merge($_default_params, $_config);
            $_params = \Cataleya\Helper\DBH::buildParamsArray($_expected_keys, $_config);
            
            //$_params['params'] = array_merge($_default_params, $_params['params']);
            
            
            
            


            // Get database handle...
            $dbh = \Cataleya\Helper\DBH::getInstance();

            // Get error handler
            $e = \Cataleya\Helper\ErrorHandler::getInstance();

            // construct
            static $insert_handle;

            if (empty($insert_handle))
            {
                    $insert_handle = $dbh->prepare('
                                                    INSERT INTO web_hooks  
                                                    (' . implode(', ', $_params['columns']) . ') 
                                                    VALUES (' . implode(', ', $_params['placeholders']) . ')
                                                    ');
            }

            \Cataleya\Helper\DBH::bindParamsArray($insert_handle, $_params['params']);
            
            
            if (!$insert_handle->execute()) $e->triggerException('
                                                                            Error in class (' . __CLASS__ . '): [ ' . 
                                                                            implode(', ', $insert_handle->errorInfo()) . 
                                                                            ' ] on line ' . __LINE__);


            // AUTOLOAD NEW PRODUCT AND RETURN IT
            $instance = self::load($dbh->lastInsertId());
            
            \Cataleya\System\Event::notify('webhook/created', $instance, array('scope'=>'webhook/created'));

            return $instance;
		
		
	}

        
        


        
        
        

	 
	/**
         * 
         * @return boolean
         * 
         */
	public function delete () 
	{
            
                        
                        $data = array (
                            'scope'=>'webhook/deleted', 
                            'id' => $this->getID(), 
                            'title' => $this->getDescription()->getTitle(), 
                            'description' => $this->getDescription()->getDescription()
                        );
                

                        // Delete description
                        $this->getDescription()->delete();
		
			// DELETE 
			$instance_delete = $this->dbh->prepare('DELETE FROM web_hooks WHERE hook_id = :instance_id');
			$instance_delete->bindParam(':instance_id', $this->_data['hook_id'], \PDO::PARAM_INT);
			
	
			if (!$instance_delete->execute()) $this->e->triggerException('
											Error in class (' . __CLASS__ . '): [ ' . 
											implode(', ', $instance_delete->errorInfo()) . 
											' ] on line ' . __LINE__);
                        

                        \Cataleya\System\Event::notify('webhook/deleted', NULL, $data);
                        
		return TRUE;
		
		
	}

        
        
        
        
        
        




        /**
         * 
         * @return int
         * 
         */
        public function getID()
        {
                return $this->_data['hook_id'];
        }



        
        



        


        /**
         * 
         * @return boolean
         * 
         */
        public function isActive()
        {
                return ((int)$this->_data['is_active'] === 1);
        }




        /**
         * 
         * @param boolean $value
         * @return \Cataleya\Front\WebHook|boolean
         * 
         */
        public function setActive($value)
        {

                if (filter_var($value, FILTER_VALIDATE_BOOLEAN, FILTER_NULL_ON_FAILURE) === NULL) return FALSE;




                $this->_data['is_active'] = ($value) ? 1 : 0;
                $this->_modified[] = 'is_active';

                return $this;
        }
        
        
        
        
        
        
        


        /**
         * 
         * @return \Cataleya\Front\WebHook
         * 
         */
        public function enable () 
        {

            if ((int)$this->_data['is_active'] === 0) 
            {
                $this->_data['is_active'] = 1;
                $this->_modified[] = 'is_active';
            }

            return $this;
        }





        /**
         * 
         * @return \Cataleya\Front\WebHook
         * 
         */
        public function disable () 
        {
            if ((int)$this->_data['is_active'] === 1) 
            {
                $this->_data['is_active'] = 0;
                $this->_modified[] = 'is_active';
            }

            return $this;
        }
        
        
        
        
        
        
        

        /**
         *
         * Singular name of product type e.g. Dress, Tee, DVD
         * 
         * @return string
         */
        public function getName()
        {
                return $this->_data['name'];
        }
        
        
        
        

        /**
         * 
         * @return string
         */
        public function getScope()
        {
                return $this->_data['scope'];
        }
        
        
        

        

        
        public function notify ($_event, array $data = array()) {
            
        }
        
        



        /*
         *
         * [ setName ] 
         *_____________________________________________________
         *
         *
         */

        public function setName($value = NULL)
        {
                if ($value===NULL || !is_string($value)) return FALSE;

                $this->_data['name'] = $value;
                $this->_modified[] = 'name';

                return $this;
        }



        
        
        
        
        

        /**
         * 
         * @param string $value
         * @return \Cataleya\Front\WebHook
         */
        public function setScope($value = NULL)
        {
            
                $value = \Cataleya\Helper\Validator::scope($value);
                
                if ($value === FALSE) $this->e->triggerException('Error in class (' . __CLASS__ . '): [ invalid argument for method: ' . __FUNCTION__ . ' ] on line ' . __LINE__);


                $this->_data['scope'] = $value;
                $this->_modified[] = 'scope';

                return $this;
        }

        
        
        
        
        
        
       
        
        




        
	
	/**
         * 
         * @return \Cataleya\Front\WebHook
         * 
         */
	public function saveData () 
	{
		

		if (empty($this->_modified)) return;
		$_update_params = array();
		$key_val_pairs = array();
		
		foreach ($this->_modified as $key) $key_val_pairs[$key] = $key . ' = :' . $key;
		
	
		// Do prepared statement for 'INSERT'...
		$update_handle = $this->dbh->prepare('
                                                        UPDATE web_hooks 
                                                        SET ' . implode (', ', $key_val_pairs) . '    
                                                        WHERE hook_id = :instance_id 
                                                        ');
                
		$update_handle->bindParam(':instance_id', $_update_params['hook_id'], \PDO::PARAM_INT);
                $_update_params['hook_id'] = $this->_data['hook_id'];
                
		foreach ($this->_modified as $key) {
                    
                        $_update_params[$key] = $this->_data[$key];                        
			$update_handle->bindParam(':'.$key, $_update_params[$key], \Cataleya\Helper\DBH::getTypeConst($_update_params[$key]));

		}

		
		if (!$update_handle->execute()) $this->e->triggerException('Error in class (' . __CLASS__ . '): [ ' . implode(', ', $update_handle->errorInfo()) . ' ] on line ' . __LINE__);
                
                
                \Cataleya\System\Event::notify('webhook/updated', $this, array('scope'=>'webhook/updated'));
                
                return $this;
		
	}
	
	
       
 
 
        
        
        
        
        
        
        

        
        
        



        





}
	
	
	
	
