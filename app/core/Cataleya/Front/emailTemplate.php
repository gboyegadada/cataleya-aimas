<?php


namespace Cataleya\Front;



/*

CLASS EMAIL_TEMPLATE



Usage
______________________________________________

$emails = array(
    'bob@bobsite.com',
    'you@yoursite.com'
);

$Emailer = new Emailer($emails);
 //More code here

$Template = new EmailTemplate('path/to/my/email/template');
    $Template->Firstname = 'Robert';
    $Template->Lastname = 'Pitt';
    $Template->LoginUrl= 'http://stackoverflow.com/questions/3706855/send-email-with-a-template-using-php';
    //...

$Emailer->SetTemplate($Template); //Email runs the compile
Emailer->send();



*/




class EmailTemplate
{
    protected $variables = array();
    protected $path_to_file= array();
	private $e;
	static $_instance;

	const DEFAULT_TEMPLATE = 'includes/templates/email/default.tmp.php';
	
    public function __construct($path_to_file)
    {	
		// Get error handler
		$this->e = \Cataleya\Helper\ErrorHandler::getInstance();
		
        if(!file_exists($path_to_file))
        {
             $this->e->triggerException('Error in class (' . __CLASS__ . '): [ Email template not found ] on line ' . __LINE__);
             return;
        }
         $this->path_to_file = $path_to_file;
    }
	
	


	public function load($template = self::DEFAULT_TEMPLATE)
	{
		
		if (!isset(self::$_instance))
		{
			
			// Create instance...
			$object =  __CLASS__;
			self::$_instance = new $object ($template);
		
		
		}
		
		return self::$_instance;
		
	}
	
	
	

    public function __set($key,$val)
    {
        $this->variables[$key] = $val;
	}


    public function compile()
    {
        ob_start();

        extract($this->variables);
        include $this->path_to_file;


        $content = ob_get_contents();
        ob_end_clean();

        return $content;
    }
}