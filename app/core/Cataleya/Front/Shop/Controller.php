<?php


namespace Cataleya\Front\Shop;



//if (!defined ('IS_ADMIN_FLAG') || IS_ADMIN_FLAG === TRUE) die("Illegal Access");

/*
 *
 * 	@Package: Cataleya
 * 	@Class: \Cataleya\Front\Shop\Controller 
 *
 * 	(c) 2014 Fancy Paper Planes
 *
 *
 *
 */

class Controller 
{
    
    
    protected static $_RW = array (
        's' => false, 
        'pg' => 'index', 
        'c' => '404', 
        'a' => 'view', 
        'f' => 'HTML', 
        'id' => false, 
        'cat' => false, 
        'ref' => ''
    );
    
    private static $_query_ready = false;
    protected $_errors = array();
    protected $_notices = array();
    protected $_failed_items = array();
    
    protected $_Customer;
    protected static $_is_logged_in = FALSE;
    protected $_Transaction;



    public function __construct() {
       
        
    }
    
    
    
    


    /*
     * 
     * [ load ]
     * ______________________________________________________________
     * 
     * 
     * 
     * 
     */


    public static function load () 
    {
        
        
        $m = array ();
        $_type = '404';
        $_RW = array (
            'ref' => $_SERVER['REQUEST_URI']
        );
        
        //$_root_uri = str_replace('/', '\/', ROOT_URI);
        // /store/aimas-ng/js/classes.js
        
        //die ('!^/' . ROOT_URI . '(?<uri>([A-Za-z0-9_\-]+[/])+)?(\?.*)?$!');
        
        if (preg_match('!^/' . ROOT_URI . '(?<uri>([A-Za-z0-9_\-]+[/]?)+)?(?<ext>\.[a-zA-Z]{2,10})?(\?.*)?$!', $_SERVER['REQUEST_URI'], $_matches) === 0) { 
            // Assume ROOT_URI!
            $_matches = array ('uri'=>'');
        }

        //die(print_r($_matches, true).$_SERVER['REQUEST_URI'].__FILE__);
        
        // File extension...
        $_RW['f'] = (!empty($_matches['ext'])) ? $_matches['ext'] : '.html';
        
        if (!empty($_matches['uri'])) {


            $_patterns = array (
                'root' => '~^/?$~i', 
                'home' => '~^([^/]+)/?$~i', 
                'page' => '~^([^/]+)/page/([^/]+)/?$~i', 
                'category' => '~^([^/]+)/c/([^/]+)/?$~i', 
                'product' => '~^([^/]+)/p/([^/]+)/[^/]+/([0-9]+)/?$~i', 
                'others_default' => '~^([^/.]+)/([^/.]+)(\.[a-zA-Z]{2,4})?/?$~i', 
                'others_with_action' => '~^([^/.]+)/([^/.]+)/([^/.]+)(\.[a-zA-Z]{2,10})?/?$~i'
            );


            foreach ($_patterns as $k=>$p) {

               $m = array ();
               if (preg_match($p, $_matches['uri'], $m) > 0) {

                   $_type = $k;

                   break;
               }

            }
        
        } else {
            $_type = 'root';
        }
        
           
        // die ('<br/>' . $_type . ' : ' . ROOT_URI . ' : ' . $_matches['uri']);

        
        
        // if (empty($_GET['c'])) return new \Cataleya\Front\Shop\Controller\Home();
        
        $m[1] = isset($m[1]) 
            ? filter_var($m[1], FILTER_VALIDATE_REGEXP, array('options'=>array('regexp' => '/^[A-Za-z0-9\-]{1,200}$/'))) 
            : false;

        
        
        switch ($_type) 
        {
            
            
            case 'root':
                
                
                $_RW['s'] = isset($_GET['s']) 
                    ? filter_var(
                        $_GET['s'], 
                        FILTER_VALIDATE_REGEXP, 
                        array('options'=>array('regexp' => '/^[A-Za-z0-9\-]{1,200}$/'))
                    ) : false;
             
                
                
                // Vanity URL...
                if ($_RW['s'] !== FALSE) {

                    $url = SHOP_ROOT . $_RW['s'];
                    Header("Location: $url");
                    exit();
                } else {
                    $_RW['c'] = 'index';
                }

                
                break;
                        
            case 'home':
                $_RW['c'] = 'home';
                $_RW['s'] = $m[1];
                
                break;
            
            case 'page':
                $_RW['c'] = 'page';
                $_RW['s'] = $m[1];
                $_RW['pg'] = $m[2];
                
                break;
            
            case 'category':
                $_RW['c'] = 'c';
                $_RW['s'] = $m[1];
                $_RW['a'] = $m[2];
                $_RW['cat'] = filter_var(
                        $m[2], 
                        FILTER_VALIDATE_REGEXP, 
                        array('options'=>array('regexp' => '/^[A-Za-z0-9\-]{1,200}$/'))
                        );
                $_RW['id'] = filter_var(
                        $m[2], 
                        FILTER_VALIDATE_REGEXP, 
                        array('options'=>array('regexp' => '/^[A-Za-z0-9\-]{1,200}$/'))
                        );
                break;
            
            case 'product':
                $_RW['c'] = 'p';
                $_RW['s'] = $m[1];
                $_RW['a'] = $m[2];
                $_RW['cat'] = filter_var(
                        $m[2], 
                        FILTER_VALIDATE_REGEXP, 
                        array('options'=>array('regexp' => '/^[A-Za-z0-9\-]{1,200}$/'))
                        );
                $_RW['id'] = filter_var(
                        $m[3], 
                        FILTER_VALIDATE_INT
                        );
                break;
            
            case 'others_default':
                $_RW['s'] = $m[1];
                $_RW['c'] = $m[2];
                $_RW['a'] = 'view';
                // $_RW['f'] = (!empty($m[3])) ? $m[3] : '.html';
                break;
            
            case 'others_with_action':
                $_RW['s'] = $m[1];
                $_RW['c'] = $m[2];
                $_RW['a'] = $m[3];
                //$_RW['f'] = (!empty($m[4])) ? $m[4] : '.html';
                break;
            
            default:
                $_RW = array (
                    's' => false, 
                    'c' => '404', 
                    'a' => 'view', 
                    'f' => '.html', 
                    'id' => false, 
                    'ref' => ''
                );
                break;
            
        }
        
        
        
        
        
        
        
        $_RW['f'] = (!empty($_RW['f'])) ? strtoupper(substr($_RW['f'], 1)) : 'HTML';
        
        
        self::$_RW = $_RW;
        self::$_query_ready = TRUE;
        
        
        // Check if STORE is active
        
        $_Store = \Cataleya\Front\Shop::getInstance();
        
        
       
        // Check that we have a valid Store instance...
        if (self::$_RW['c'] !== 'index' && empty($_Store)) {
            
                Header("Location: " . SHOP_ROOT);
                exit();
            
        }
        
        // Verify that Shop Handle is valid...
        else if (!empty($_Store) && self::$_RW['s'] !== $_Store->getHandle()) {
            
                Header("Location: " . $_Store->getShopLanding());
                exit();
                
        }
        
        
        
        \Cataleya\Front\Twiggy::getInstance(\__path('shop.skin') . '/layouts');


        

        if (!AUTHORIZED_SHOPPER && !empty($_Store) && !$_Store->isActive()) {
            // Store is closed and user is NOT authorized view

            $_View = new \Cataleya\Front\Shop\View();
            $_View->storeClosed();

        } 
        
        

        // Check if APP is active

        if (!AUTHORIZED_SHOPPER && !\Cataleya\System::load()->isActive()) {
            // Store is closed and user is NOT authorized view

            \Cataleya\Session::kill_session(SESSION_NAME);
            $_View = new \Cataleya\Front\Shop\View();
            $_View->storeClosed();

        }

        
        // Verify that requested PAGE exists...
        
        if ($_RW['c'] === 'page') {
            $_Page = $_Store->getCMSPage($_RW['pg']);
            
            if (empty($_Page)) {
                // Page not found...
                return new \Cataleya\Front\Shop\Controller\NotFound();
            }
        }

        

        

        // Authenticate user...
        (self::$_RW['c'] !== 'index') ? self::doAuth() : TRUE;

        
        
        switch (self::$_RW['c']) 
        {
            
            
            case 'index':
                return new \Cataleya\Front\Shop\Controller\StoreSelector();
                break;
            
            case 'home':
                return new \Cataleya\Front\Shop\Controller\Home();
                break;
            
            case 'rpc':
                return \Cataleya\Front\Shop\RPC\Controller::load();
                break;
            
            case 'js':
                return new \Cataleya\Front\Shop\Controller\Asset\JS();
                break;
            
            case 'mustache':
                return new \Cataleya\Front\Shop\Controller\Asset\Mustache();
                break;
            
            case 'page':
                return new \Cataleya\Front\Shop\Controller\Page();
                break;
            
            case 'cart':
                return new \Cataleya\Front\Shop\Controller\Cart();
                break;
            
            case 'checkout':
                return new \Cataleya\Front\Shop\Controller\Checkout();
                break;
            
            case 'wishlist':
                return new \Cataleya\Front\Shop\Controller\Wishlist();
                break;
            
            case 'p':
                return new \Cataleya\Front\Shop\Controller\Product();
                break;
            
            case 'c':
                return new \Cataleya\Front\Shop\Controller\Category();
                break;
            
            case 'search':
                return new \Cataleya\Front\Shop\Controller\ProductSearch();
                break;
            
            case 'myaccount':
                return new \Cataleya\Front\Shop\Controller\MyAccount();
                break;
            
            case 'register':
                return new \Cataleya\Front\Shop\Controller\Register();
                break;
            
            case 'login':
                return new \Cataleya\Front\Shop\Controller\Login();
                break;
            
            case 'logout':
                return new \Cataleya\Front\Shop\Controller\Logout();
                break;
            
            case 'confirm':
                return new \Cataleya\Front\Shop\Controller\Confirm();
                break;
            
            case 'forgot-password':
                return new \Cataleya\Front\Shop\Controller\PasswordRecovery();
                break;
            
            default:
                return new \Cataleya\Front\Shop\Controller\NotFound();
                break;
        }
    }
    
    
    
    
    

    
    /**
     * 
     * @return \Cataleya\Customer 
     * 
     */
    public function getCustomer () {
        return $this->_Customer;
    }


    
    /**
     * 
     * @return \Cataleya\Payment\Transaction
     * 
     */
    public function getTransaction () {
        return $this->_Transaction;
    }


    
    
    /**
     * 
     * @return array
     * 
     */
    public function getErrors() 
    {
        return $this->_errors;
    }

    /**
     * 
     * @return array
     * 
     */
    public function getNotices() 
    {
        return $this->_notices;
    }

    
    
    /**
     * 
     * @return array
     * 
     */
    public function getFailedItems() 
    {
        return $this->_failed_items;
    }
    
    
    
    
    
    
    /**
     * 
     * Check is request query has been processed
     * 
     * @return bool Check is request query has been processed...
     * 
     */
    public static function isReady () 
    {
        return self::$_query_ready;
    }

    
    
    /**
     * 
     * RETURN URL Rewrite params...
     * 
     * @return Array Contains URL Rewrite params like shop, controller, and requested file format.
     * 
     */
    public static function getParams () 
    {
        return self::$_RW;
    }

    
    
    /**
     * 
     * RETURN URL Rewrite params...
     * 
     * @return string Controller action.
     * 
     */
    public static function getAction () 
    {
        return isset(self::$_RW['a']) ? self::$_RW['a'] : null;
    }
    


    /**
     * 
     * RETURN URL Rewrite params...
     * 
     * @return string Controller.
     * 
     */
    public static function getControllerHandle () 
    {
        return isset(self::$_RW['c']) ? self::$_RW['c'] : null;
    }
    
    
    

    /**
     * 
     * RETURN URI...
     * 
     * @return string URI.
     * 
     */
    public static function getURI () 
    {
        return isset(self::$_RW['ref']) ? self::$_RW['ref'] : null;
    }
    
    
    

    /**
     * 
     * RETURN URL Rewrite params...
     * 
     * @return string expected ContentType (HTML, JSON e.t.c.)
     * 
     */
    public static function getContentType () 
    {
        return isset(self::$_RW['f']) ? self::$_RW['f'] : null;
    }
    
    
    
    


    /**
     * 
     * RETURN URL Rewrite params...
     * 
     * @return string Store slug/keyword
     * 
     */
    public static function getStoreKeyword () 
    {
        return isset(self::$_RW['s']) ? self::$_RW['s'] : null;
    }
    
    
    
    
    /**
     * 
     * RETURN URL Rewrite params...
     * 
     * @return string Store slug/keyword
     * 
     */
    public static function getShopHandle () 
    {
        return isset(self::$_RW['s']) ? self::$_RW['s'] : null;
    }
    
 


    /**
     * 
     * RETURN URL Rewrite params...
     * 
     * @return string Page slug/keyword
     * 
     */
    public static function getPageKeyword () 
    {
        return isset(self::$_RW['pg']) ? self::$_RW['pg'] : null;
    }
    
    

    /**
     * 
     * RETURN URL Rewrite params...
     * 
     * @return string Category slug/keyword
     * 
     */
    public static function getCategoryKeyword () 
    {
        return isset(self::$_RW['cat']) ? self::$_RW['cat'] : null;
    }
    


    /**
     * 
     * RETURN URL Rewrite params...
     * 
     * @return string Category / Product ID
     * 
     */
    public static function getItemID () 
    {
        return isset(self::$_RW['id']) ? self::$_RW['id'] : null;
    }
    
    
    
    
    /**
     * 
     * 
     * 
     * @return boolean
     * 
     */
    public static function isLoggedIn () 
    {
        return self::$_is_logged_in;
    }
    
    
    
    



    /*
     * 
     * [ getTwigVars ]
     * ______________________________________________________________
     * 
     * @return array (JSON)
     * 
     * 
     */


    public static function getBaseTwigVars () 
    {


        $_Store = (self::isReady()) ? \Cataleya\Front\Shop::getInstance() : NULL;
        
        
        if (!isset($_SESSION[SESSION_PREFIX.'AUTH_TOKEN'])) {
            $_SESSION[SESSION_PREFIX.'AUTH_TOKEN'] = md5( uniqid(rand(), TRUE) ) . '-' . CURRENT_DT;
        }

        
        
        // TWIG VARIABLES ARRAY
        $twig_vars = array (
            'base_url'=>BASE_URL, 
            'domain'=> DOMAIN, 
            'assets_path' => (!empty($_Store)) 
                                ? $_Store->getTheme()->getAssetsPath()
                                : \Cataleya\System::load()->getDefaultShopFrontTheme()->getAssetsPath(),
            'error' =>  FALSE, 
            'errorText' =>  array (), 
            'session' => [ 'auth_token' => $_SESSION[SESSION_PREFIX.'AUTH_TOKEN'] ]
        );



        // App info
        $twig_vars['app'] = array (

            );
        


        // Store info
        
        if (!empty($_Store)) {
            
            
            $twig_vars['store'] = \Cataleya\Front\Shop\Juicer::juice($_Store);
        
        
            // PAGES...

            $twig_vars['pages'] = array ();
            $_Pages = $_Store->getCMSPages();

            foreach ($_Pages as $_Page) {
                $twig_vars['pages'][$_Page->getHandle()] = \Cataleya\Front\Shop\Juicer::juiceDrop($_Page);
            }
            
            
            // FEATURED ...
            
            $twig_vars['catalog'] = array (
                'featured' => array ()
            );
            
        
            $_Featured = \Cataleya\Catalog::load(array('featured'=>true));
            foreach ($_Featured as $_Item) {
                $twig_vars['catalog']['featured'][] = \Cataleya\Front\Shop\Juicer::juice($_Item);
            }

            // Categories ...

            /* @var $categories \Cataleya\Catalog\Tag */
            $_root_cat = \Cataleya\Front\Shop\Juicer::juiceDrop($_Store->getRootCategory(), false, true);


            $twig_vars['categories'] = $_root_cat['children'];

        }



        // Customer info
        if (!isset($twig_vars['customer'])) $twig_vars['customer'] = array (); 

        $twig_vars['customer']['loggedIn'] = (isset($_SESSION[SESSION_PREFIX.'LOGGED_IN']) && $_SESSION[SESSION_PREFIX.'LOGGED_IN'] !== FALSE) ? TRUE : FALSE;
        $twig_vars['customer']['firstname'] = (isset($_SESSION[SESSION_PREFIX.'FNAME'])) ? $_SESSION[SESSION_PREFIX.'FNAME'] : '';
        $twig_vars['customer']['lastname'] = (isset($_SESSION[SESSION_PREFIX.'LNAME'])) ? $_SESSION[SESSION_PREFIX.'LNAME'] : '';
        $twig_vars['customer']['email'] = (isset($_SESSION[SESSION_PREFIX.'CUSTOMER_EMAIL'])) ? $_SESSION[SESSION_PREFIX.'CUSTOMER_EMAIL'] : '';
        $twig_vars['customer']['id'] = (isset($_SESSION[SESSION_PREFIX.'CUSTOMER_ID'])) ? $_SESSION[SESSION_PREFIX.'CUSTOMER_ID'] : '';
        $twig_vars['customer']['authToken'] = (isset($_SESSION[SESSION_PREFIX.'AUTH_TOKEN'])) ? $_SESSION[SESSION_PREFIX.'AUTH_TOKEN'] : '';

        $twig_vars['customer']['wishlistItemCount'] = self::isLoggedIn()  
                                                        ? \Cataleya\Customer::load($_SESSION[SESSION_PREFIX.'CUSTOMER_ID'])->getWishlist()->getPopulation() 
                                                        : 0;





        // Meta

        $twig_vars['meta'] = array (
            'keywords'  =>  '', 
            'description'   =>  ''
        );
        
        
        return $twig_vars;

        
        
    }
    
    
    
    
    
      


    /*
     * 
     * [ showLogin ]
     * ______________________________________________________________
     * 
     * @return array (JSON)
     * 
     * 
     */


    public static function requireLogin () 
    {
        
        if (self::isLoggedIn()) return;
            
        
        if (!in_array(self::getControllerHandle(), array('login', 'logout', 'resend', 'forgot-password', 'confirm', 'register')) && 'HTML' === self::getContentType())
        {
            $_SESSION[SESSION_PREFIX.'REFERRER'] = (SSL_REQUIRED ? 'https://' : 'http://').HOST.self::getURI();
            if (!empty($_SERVER['QUERY_STRING'])) $_SESSION[SESSION_PREFIX.'REFERRER'] .=  '?' . $_SERVER['QUERY_STRING'];
                
        }
            
        $twig_vars = array (
            'response' => 'Login required.'
        );
        
            
        // JSON anyone?
        if (in_array(OUTPUT, ['JSON', 'AJAX_JSON'])) {
            echo json_encode($twig_vars, JSON_PRETTY_PRINT);
            exit();
        }
        
        // - OR - //
        
        redirect (\Cataleya\Front\Shop::getInstance()->getShopLanding() . 'login');
        exit('Redirecting...');
    
    
    }
    
    
    
    
    
    
    
    private static function doAuth () {


        $_Store = \Cataleya\Front\Shop::getInstance();
        
        // 1. Is there an active session?
        if (!empty($_Store) && isset($_SESSION[SESSION_PREFIX.'CUSTOMER_ID'], $_SESSION[SESSION_PREFIX.'LOGGED_IN'])) 
        {
            $customer = \Cataleya\Customer::authenticate($_SESSION[SESSION_PREFIX.'CUSTOMER_ID'], $_SESSION[SESSION_PREFIX.'LOGGED_IN']);

            if ($customer !== NULL) 
            {
                // Auth complete !!
                
                // a. Set new login token...
                $_SESSION[SESSION_PREFIX.'LOGGED_IN'] = $customer->getLoginToken();
                
                // b. Set flag to true..
                self::$_is_logged_in = true;
            } 
        } 
        
        else if (isset($_SESSION[SESSION_PREFIX.'CUSTOMER_ID']))
        {
            unset($_SESSION[SESSION_PREFIX.'CUSTOMER_ID']);

        }
        
       
        define('IS_LOGGED_IN', self::$_is_logged_in);
        
        return;
        
        
        


    }
    

        

 
}


