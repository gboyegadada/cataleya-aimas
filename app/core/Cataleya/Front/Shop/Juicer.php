<?php



namespace Cataleya\Front\Shop;



/*
 *
 * 	@Package: Cataleya
 * 	@Class: Juicer
 *
 * 	(c) 2012 Fancy Paper Planes
 *
 *
 *
 */

class Juicer {
    //put your code here
    
    
    /*
     * @const integer
     * 
     */
    
    const CUSTOMER = 0;
    
    
    
    /*
     * @const integer
     * 
     */
    
    const STOREFRONT = 0;
    
    
    /*
     * @const integer
     * 
     */
    
    const ADMIN = 0;
    





    /*
     * 
     * [ juice ]
     * ______________________________________________________________
     * 
     * @param object (Product / Tag / Store / ... )
     * @param \Cataleya\Store $_Store
     * 
     * @return array (JSON)
     * 
     * 
     */


    public static function juice ($_Object) 
    {
        
        $_m = 'juice_'.str_replace('\\', '_', get_class($_Object));
        
        
        if (is_object($_Object) && method_exists(__CLASS__, $_m)) {
            
            return forward_static_call_array([__CLASS__, $_m], func_get_args());
            
        }
        
        
        return NULL;
        
        
        
        
        
    }
    
    
    
    
    
    



    /*
     * 
     * [ juiceDrop ]
     * ______________________________________________________________
     * 
     * @param object (Product / Tag / Store / ... )
     * @param \Cataleya\Store $_Store
     * 
     * @return array (JSON)
     * 
     * 
     */


    public static function juiceDrop ($_Object) 
    {
        
        
        
        $_m = 'juiceDrop_'.str_replace('\\', '_', get_class($_Object));
        
        
        if (is_object($_Object) && method_exists(__CLASS__, $_m)) {
            
            return forward_static_call_array([__CLASS__, $_m], func_get_args());
            
        }
        
        
        return NULL;
        
        
    }
    
    
      
    
    
    
    
    
    
    
    
    
    
    
    ///////////////////////////// STOREFRONT METHODS //////////////////////////


    
    

    


    /*
     * 
     * [ juice_Layout ]
     * ______________________________________________________________
     * 
     * @param \Cataleya\Front\Home\Layout $_Layout
     * 
     * @return array (template ready JSON)
     * 
     * 
     */



    private static function juice_Layout (\Cataleya\Front\Home\Layout $_Layout) 
    {

        $_Store = $_Layout->getStore();

        $_info = array (
            'id'    =>   $_Layout->getID(), 
            'Tiles'   =>  array (), 
            'Store' =>  array (
                'id' => $_Store->getStoreId(), 
                'title' => $_Store->getDescription()->getTitle('EN')
            )
        );


        foreach ($_Layout as $_Tile) $_info['Tiles'][] = self::juice($_Tile);

        return $_info;


    }





    


    /*
     * 
     * [ juice_Tile]
     * ______________________________________________________________
     * 
     * @param \Cataleya\Front\Home\Tile $_Tile
     * 
     * @return array (template ready JSON)
     * 
     * 
     */



    private static function juice_Cataleya_Front_Home_Tile (\Cataleya\Front\Home\Tile $_Tile) 
    {
        $_max_width = \Cataleya\Front\Home\Layout::PIXEL_WIDTH_MULTIPLE*6;
        $_pixel_width = $_Tile->getPixelWidth();
        $_adjusted_pixel_width = ($_pixel_width < $_max_width) ? $_pixel_width-5 : $_pixel_width;

        $_info = array (
            'id'    =>   $_Tile->getID(), 
            'population'  =>  $_Tile->getPopulation(), 
            'noSlides'  =>  (($_Tile->getPopulation() < 1) ? true : false), 
            'pixel_height' => $_Tile->getPixelHeight(),  
            'pixel_width' => $_pixel_width, 
            'adjusted_pixel_width' => $_adjusted_pixel_width, // compensate for margin 
            'slide_strip_pixel_width' => ($_adjusted_pixel_width*$_Tile->getPopulation()), 
            'sort_order'  =>   $_Tile->getSortOrder(), 
            'Slides'   =>  array ()
        );


        foreach ($_Tile as $_Slide) $_info['Slides'][] = self::juice($_Slide);

        return $_info;


    }







    


    /*
     * 
     * [ juice_Slide ]
     * ______________________________________________________________
     * 
     * @param \Cataleya\Front\Home\Slide $_Slide
     * 
     * @return array (template ready JSON)
     * 
     * 
     */



    private static function juice_Cataleya_Front_Home_Slide (\Cataleya\Front\Home\Slide $_Slide) 
    {

        $_Tile = $_Slide->getTile();
        $_max_width = \Cataleya\Front\Home\Layout::PIXEL_WIDTH_MULTIPLE*6;
        $_pixel_width = $_Tile->getPixelWidth();
        $_adjusted_pixel_width = ($_pixel_width < $_max_width) ? $_pixel_width-5 : $_pixel_width;

        $_info = array (
            'id'    =>   $_Slide->getID(), 
            'type'   =>  $_Slide->getType(), 
            'is_text'   =>  ($_Slide->getType() === 'text') ? TRUE : FALSE, 
            'is_image'   =>  ($_Slide->getType() === 'image') ? TRUE : FALSE, 
            'text'  =>  $_Slide->getText(), 
            'image' =>  $_Slide->getImage()->getHrefs(), 
            'pixel_height' => $_Tile->getPixelHeight(),  
            'pixel_width' => $_pixel_width, 
            'adjusted_pixel_width' => $_adjusted_pixel_width, // compensate for margin 
            'href'  =>  $_Slide->getHref(), 
            'target'    =>  $_Slide->getTarget(), 
            'open_in_new_window'    =>  (($_Slide->getTarget() === '_blank') ? true : false), 
            'sort_order'    =>  $_Slide->getSortOrder()
        );

        return $_info;


    }

    
    
    

    


    /*
     * 
     * [ juice_Store ]
     * ______________________________________________________________
     * 
     * @param \Cataleya\Store $_Store
     * @param \Cataleya\Store $_Store
     * 
     * @return array (template ready JSON)
     * 
     * 
     */


    
    private static function juice_Cataleya_Store (\Cataleya\Store $_Store = NULL) 
    {

        


        if (NULL === $_Store) {
            $_Store = \Cataleya\Front\Shop::getInstance();
        }
        
        $StoreLocation = $_Store->getDefaultContact();
        $_Country = $StoreLocation->getCountry();

        $_Currency = $_Store->getCurrency();
        
        
        
        // Store info
        $json = array (
            'id'    =>  $_Store->getID(), 
            'title' =>  $_Store->getDescription()->getTitle($_Store->getLanguageCode()), 
            'description'   =>  $_Store->getDescription()->getText($_Store->getLanguageCode()), 
            'active'   =>  ($_Store->isActive()) ? true : false, 
            'language'  =>  $_Store->getLanguageCode(), 
            'country'   =>  self::juice($_Country),
            'landing'   =>  $_Store->getShopLanding(),
            'keyword' => $_Store->getHandle(),
            'handle' => $_Store->getHandle(),
            'label_color'=>$_Store->getLabelColor(), 
            'payment_options' => $_Store->getPaymentOptions(), 
            'currency'  =>  array (
                                    'code' =>  $_Store->getCurrencyCode(), 
                                    'symbol'    =>  '&#'.$_Currency->getUtfCode().';', 
                                    'name'  =>  $_Currency->getCurrencyName()
                              ), 

            // SHOP contact

            'contact' => self::juice($_Store->getDefaultContact())
        );
        
        

        return $json;

    }

    
    
    
    
    
    


    /*
     * 
     * [ juiceDrop_Store ]
     * ______________________________________________________________
     * 
     * @param \Cataleya\Store $_Store
     * @param \Cataleya\Store $_Store
     * 
     * @return array (template ready JSON)
     * 
     * 
     */


    
    private static function juiceDrop_Cataleya_Store (\Cataleya\Store $_Store = NULL) 
    {

        

        if (NULL === $_Store) {
            $_Store = \Cataleya\Front\Shop::getInstance();
        }

        // $StoreLocation = $_Store->getDefaultContact();
        // $_Country = $StoreLocation->getCountry();

        $_Currency = $_Store->getCurrency();

        

        // Store info
        $json = array (
            'id'    =>  $_Store->getID(), 
            'title' =>  $_Store->getDescription()->getTitle($_Store->getLanguageCode()), 
            'description'   =>  $_Store->getDescription()->getText($_Store->getLanguageCode()), 
            'active'   =>  ($_Store->isActive()) ? true : false, 
            'language'  =>  $_Store->getLanguageCode(), 
            // 'country'   =>  self::juice($_Country),
            'landing'   =>  $_Store->getShopLanding(), 
            'keyword' => $_Store->getHandle(),
            'handle' => $_Store->getHandle(),
            'label_color'=>$_Store->getLabelColor(), 
            'payment_options' => $_Store->getPaymentOptions(), 
            'currency'  =>  array (
                                    'code' =>  $_Store->getCurrencyCode(), 
                                    'symbol'    =>  '&#'.$_Currency->getUtfCode().';', 
                                    'name'  =>  $_Currency->getCurrencyName()
                              ), 

            // SHOP contact

            // 'contact' => self::juice($_Store->getDefaultContact())
        );
        
        
        

        return $json;

    }

    
    
    
    

    /**
     * 
     * @param \Cataleya\Sales\Coupon $_Coupon
     * @return array (template ready JSON)
     * 
     */
    private static function juice_Cataleya_Sales_Coupon (\Cataleya\Sales\Coupon $_Coupon) 
    {
        $_Store = $_Coupon->getStore();
        $_Currency = $_Store->getCurrency();

        $start_date = $_Coupon->getStartDate(TRUE);
        $end_date = $_Coupon->getEndDate(TRUE);

        $_info = array (
            'code' =>  $_Coupon->getCode(), 
            'id'    =>  $_Coupon->getID(), 
            'discountType'  =>  (($_Coupon->getDiscountType() === \Cataleya\Catalog\Price::TYPE_REDUCTION_PERCENT) ? FALSE : TRUE), 
            'discountAmount'    =>  $_Coupon->getDiscountAmount(), 
            'Currency'  =>  array (
                'name'  =>  $_Currency->getCurrencyName(), 
                'code'  =>  $_Currency->getCurrencyCode(), 
                'utf'  =>  $_Currency->getUtfCode()
            ), 
            'Store' =>  self::juiceDrop($_Store), 
            'startDateDay' => $start_date['day'], 
            'startDateMonth' => $start_date['month'], 
            'startDateYear' => $start_date['year'], 
            'startDate' =>  $_Coupon->getStartDate(), 

            'endDateDay' => $end_date['day'], 
            'endDateMonth' => $end_date['month'], 
            'endDateYear' => $end_date['year'], 
            'endDate'   =>  $_Coupon->getEndDate(), 

            'isIndefinite'    =>  $_Coupon->isIndefinite(), 

            'usesPerCoupon' =>  $_Coupon->getUsesPerCoupon(), 
            'isActive'  =>  $_Coupon->isActive(), 
            'population' => $_Coupon->getPopulation()

        );

        return $_info;


    }


    
    
    


    /**
     * 
     * @param \Cataleya\Sales\Sale $_Sale
     * @return array (template ready JSON)
     * 
     */
    function juice_Cataleya_Sales_Sale (\Cataleya\Sales\Sale $_Sale) 
    {
        $_Store = $_Sale->getStore();
        $_Currency = $_Store->getCurrency();

        $_info = array (
            'title' =>  $_Sale->getDescription()->getTitle('EN'), 
            'id'    =>  $_Sale->getID(), 
            'discountType'  =>  (($_Sale->getDiscountType() === \Cataleya\Catalog\Price::TYPE_REDUCTION_PERCENT) ? FALSE : TRUE), 
            'discountAmount'    =>  $_Sale->getDiscountAmount(), 
            'Currency'  =>  array (
                'name'  =>  $_Currency->getCurrencyName(), 
                'code'  =>  $_Currency->getCurrencyCode(), 
                'utf'  =>  $_Currency->getUtfCode()
            ), 
            'Store' => self::juiceDrop($_Store),
            
            'start_date' => $_Sale->getStartDate(TRUE), 
            'end_date' => $_Sale->getEndDate(TRUE), 

            'isActive'  =>  $_Sale->isActive(), 
            'population'=> $_Sale->getPopulation()

        );

        return $_info;


    }

    
    
    
    
    
    
    /*
     * 
     * [ juice_Page ]
     * ______________________________________________________________
     * 
     * @param \Cataleya\Front\Shop\Page $_Page
     * 
     * @return array (template ready JSON)
     * 
     * 
     */


    
    private static function juice_Cataleya_Front_Shop_Page (\Cataleya\Front\Shop\Page $_Page) 
    {

        


        
        $_Store = $_Page->getStore();
        
               
        
        // Store info
        $json = array (
            'id'    =>  $_Page->getID(), 
            'title' =>  $_Page->getTitle($_Store->getLanguageCode()), 
            'body'   =>  $_Page->getText($_Store->getLanguageCode()), 
            'active'   =>  ($_Page->isActive()) ? true : false, 
            'language'  =>  $_Store->getLanguageCode(),
            'keyword' => $_Page->getSlug(), 
            'handle' => $_Page->getSlug(), 
            'url' => $_Page->getURL(), 
            'Store' => self::juiceDrop($_Store),  
            'Content' => $_Page->getContent($_Store->getLanguageCode()),
            'AppList' => []
        );
        
        
        foreach ($_Page->getAppList() as $_handle) {
            $json['AppList'][] = \Cataleya\Plugins\Package::getAppInfo($_handle);
        }

        return $json;

    }

    
    
    
    
    
    
    /*
     * 
     * [ juice_Page ]
     * ______________________________________________________________
     * 
     * @param \Cataleya\Front\Shop\Page $_Page
     * 
     * @return array (template ready JSON)
     * 
     * 
     */


    
    private static function juiceDrop_Cataleya_Front_Shop_Page (\Cataleya\Front\Shop\Page $_Page) 
    {

        
        $_Store = $_Page->getStore();
        
        // Store info
        $json = array (
            'id'    =>  $_Page->getID(), 
            'title' =>  $_Page->getTitle($_Store->getLanguageCode()), 
            'body'   =>  $_Page->getText($_Store->getLanguageCode()), 
            'active'   =>  ($_Page->isActive()) ? true : false, 
            'language'  =>  $_Store->getLanguageCode(),
            'keyword' => $_Page->getSlug(), 
            'handle' => $_Page->getSlug(), 
            'url' => $_Page->getURL(), 
            'Store' => self::juiceDrop($_Store),  
            'Content' => $_Page->getContent($_Store->getLanguageCode())
        );
        
        
        
        
        
        

        return $json;

    }

    
    
    
    
    
    
    
    
    /*
     * 
     * [ juice_Theme ]
     * ______________________________________________________________
     * 
     * @param \Cataleya\Front\Shop\Theme $_Theme
     * 
     * @return array (template ready JSON)
     * 
     * 
     */


    
    private static function juice_Cataleya_Front_Shop_Theme (\Cataleya\Front\Shop\Theme $_Theme) 
    {

        
        
        // template info
        $json = array (
            'id'    =>  $_Theme->getID(), 
            'name' =>  $_Theme->getName(), 
            'folder_path'   =>  $_Theme->getFolderPath(), 
            'assets_path'   =>  $_Theme->getAssetsPath(), 
            'version'  =>  $_Theme->getVersion(),
            'page_templates' => $_Theme->getPageTemplates()
        );
        
        

        return $json;

    }

    
    
    
    
    
    
    
    
    
    
    /*
     * 
     * [ juiceDrop_Theme ]
     * ______________________________________________________________
     * 
     * @param \Cataleya\Front\Shop\Theme $_Theme
     * 
     * @return array (template ready JSON)
     * 
     * 
     */


    
    private static function juiceDrop_Cataleya_Front_Shop_Theme (\Cataleya\Front\Shop\Theme $_Theme) 
    {

        
        
        // template info
        $json = array (
            'id'    =>  $_Theme->getID(), 
            'name' =>  $_Theme->getName(), 
            'folder_path'   =>  $_Theme->getFolderPath(), 
            'assets_path'   =>  $_Theme->getAssetsPath(), 
            'version'  =>  $_Theme->getVersion(),
            'page_templates' => $_Theme->getPageTemplates()
        );
        
        

        return $json;

    }

    
    
    
    
    
    
    
    

    


    /*
     * 
     * [ juice_Order]
     * ______________________________________________________________
     * 
     * @param \Cataleya\Store\Order $_Order
     * 
     * @return array (template ready JSON)
     * 
     * 
     */



    private static function juice_Cataleya_Store_Order (\Cataleya\Store\Order $_Order) 
    {

        $_info = array (
            'id'    =>   $_Order->getID(), 
            'order_num'    =>   str_pad(strval($_Order->getID()), 5, '0', STR_PAD_LEFT), 
            'status' => $_Order->getOrderStatus(),
            'payment_status' => $_Order->getPaymentStatus(),
            'total' =>  number_format($_Order->getTotalCost(), 2), 
            'grand_total'    =>  number_format($_Order->getGrandTotal(), 2), 
            'receipts' => $_Order->getReceipts(),
            'total_paid' =>  number_format($_Order->getTotalPaid(), 2), 
            'shipping_charges'   =>  number_format($_Order->getShippingCharges(), 2), 
            'quantity'  =>  $_Order->getQuantity(), 
            'pretty_quantity'  => \Cataleya\Helper::countInEnglish($_Order->getQuantity()), 
            'population'    =>  $_Order->getPopulation(), 
            'order_date' =>  $_Order->getOrderDate(FALSE)->format('h:ia, d M, Y'), 
            'items' =>  array (), 
            'discounts' =>  $_Order->getDiscounts(), 
            'taxes' =>  $_Order->getTaxes(), 

            'customer' => self::juice($_Order->getCustomer()), 
            'currency'  =>  self::juice($_Order->getCurrency()), 
            'store' =>  self::juice($_Order->getStore()),
            
            'shipping'    =>  array (
                'amount'  =>  $_Order->getShippingCharges(), 
                'amount_before_tax'  =>  $_Order->getShippingCharges(FALSE, TRUE), 
                'description'  =>  $_Order->getShippingDescription(), 
                'taxes' =>  $_Order->getShippingTaxes()
                    ), 
                
            'shipping_address' => self::juice($_Order->getShippingAddress())
        
        );

        // Inject pretty_amount into discount vars
        if (!empty($_info['discounts'])) {
            foreach ($_info['discounts'] as $k=>$d) {
                $_info['discounts'][$k]['pretty_amount'] = number_format($d['less'], 2);
            }
        }


        // Order Details
        foreach ($_Order as $_OrderDetail) $_info['items'][] = self::juice($_OrderDetail);

        return $_info;


    }




    
    

    /*
     * 
     * [ juice_OrderDetail]
     * ______________________________________________________________
     * 
     * @param \Cataleya\Store\OrderDetail $_Order
     * 
     * @return array (template ready JSON)
     * 
     * 
     */



    private static function juice_Cataleya_Store_OrderDetail (\Cataleya\Store\OrderDetail $_OrderDetail) 
    {


        $_info = array (
            'id'    =>   $_OrderDetail->getID(), 
            'item_name'  =>  $_OrderDetail->getItemName(), 
            'item_description'   =>  $_OrderDetail->getItemDescription(), 
            'price' =>  number_format($_OrderDetail->getPrice(), 2), 
            'original_price' =>  number_format($_OrderDetail->getOriginalPrice(), 2), 
            'subtotal'  =>  number_format($_OrderDetail->getSubtotal(), 2), 
            'original_subtotal'  =>  number_format($_OrderDetail->getSubtotal(FALSE), 2), 
            'quantity'  =>  $_OrderDetail->getQuantity() 

        );

        return $_info;


    }


    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    



    /*
     * 
     * [ juice_Contact ]
     * ______________________________________________________________
     * 
     * @param \Cataleya\Asset\Contact $_Contact
     * 
     * @return array (template ready JSON)
     * 
     * 
     */


    
    private static function juice_Cataleya_Asset_Contact (\Cataleya\Asset\Contact $_Contact) 
    {

        
        $json = array (
                'name' => $_Contact->getEntryName(), 
                'first_name' => $_Contact->getEntryFirstname(), 
                'last_name' => $_Contact->getEntryLastname(), 
                'phone' => $_Contact->getEntryWorkPhone(), 
                'email' => $_Contact->getEntryEmail(), 
                'phone' => $_Contact->getEntryWorkPhone(), 
                'work_phone' => $_Contact->getEntryWorkPhone(), 
                'home_phone' => $_Contact->getEntryHomePhone(), 
                'fax' => $_Contact->getEntryFax(), 

            
                'street' => $_Contact->getEntryStreetAddress(), 
                'street_2' => $_Contact->getEntryStreetAddress2(), 
            
                'street_address' => $_Contact->getEntryStreetAddress(), 
                'street_address_2' => $_Contact->getEntryStreetAddress2(), 
                'suburb' => $_Contact->getEntrySuburb(), 
                'post_code' => $_Contact->getEntryPostcode(), 
                'city' => $_Contact->getEntryCity(), 
                'province' => $_Contact->getEntryProvince(), 
                'province_iso2' => $_Contact->getEntryProvince(), 
                'country' => $_Contact->getEntryCountry(), 
                'country_iso2' => $_Contact->getEntryCountryCode()
        );

        return $json;

    }

    
    
    
    
    



    /*
     * 
     * [ juiceDrop_Contact ]
     * ______________________________________________________________
     * 
     * @param \Cataleya\Asset\Contact $_Contact
     * 
     * @return array (template ready JSON)
     * 
     * 
     */


    
    private static function juiceDrop_Cataleya_Asset_Contact (\Cataleya\Asset\Contact $_Contact) 
    {

        
        
        return self::juice_Cataleya_Asset_Contact($_Contact);

    }

    
    
    
    
    
    




    /*
     * 
     * [ juice_Country ]
     * ______________________________________________________________
     * 
     * @param \Cataleya\Asset\Contact $_Contact
     * 
     * @return array (template ready JSON)
     * 
     * 
     */


    
    private static function juice_Cataleya_Geo_Country (\Cataleya\Geo\Country $_Country) 
    {

        
        $json = array (
                                    'iso2'  =>  $_Country->getIso2(), 
                                    'iso3'  =>  $_Country->getIso3(), 
                                    'printableName' =>  $_Country->getPrintableName(),
                                    'hasProvinces' =>  $_Country->hasProvinces(), 
                                    'provinces' =>  array ()
                                );



        if ($_Country->hasProvinces()):
        foreach ($_Country as $_Province) 
        {
            $json['provinces'][] = array (
                'id'    =>  $_Province->getID(), 
                'iso'   =>  $_Province->getIsoCode(), 
                'name'  =>  $_Province->getName(), 
                'printableName' => $_Province->getPrintableName()
            );
        }

        endif;

        return $json;

    }

    
    
    
    
    



    /**
     * 
     *
     * @param \Cataleya\Payment\PaymentType $_PaymentType
     * 
     * @return array (template ready JSON)
     * 
     */
    private static function juice_Cataleya_Payment_PaymentType (\Cataleya\Payment\PaymentType $_PaymentType, \Cataleya\Store $_Store = NULL) 
    {

        if (empty($_Store)) { 
            $_Store = \Cataleya\Front\Shop::getInstance();
        }
        
        // If Store is still empty, no context, return empty array...
        if (empty($_Store)) {
            return array ();
        }
        
        
        $_Currency = $_Store->getCurrency();
        
        $json = array (
                'id' => $_PaymentType->getID(), 
                'Title' => $_PaymentType->getDisplayName(), 
                'Logo' => $_PaymentType->getLogo(), 
                'RequiresBillingAddress' => $_PaymentType->requiresBillingAddress(), 
                'MaxAmount' => ($_Currency->getCurrencyCode() . number_format ((float)$_PaymentType->getMaxAmount($_Currency), 2)), 
                'MinAmount' => ($_Currency->getCurrencyCode() . number_format ((float)$_PaymentType->getMinAmount($_Currency), 2)), 
                'active' => $_Store->hasPaymentType($_PaymentType)
            );

        return $json;

    }

    
    



    /**
     * 
     *
     * @param \Cataleya\Payment\PaymentType $_PaymentType
     * 
     * @return array (template ready JSON)
     * 
     */
    private static function juiceDrop_Cataleya_Payment_PaymentType (\Cataleya\Payment\PaymentType $_PaymentType) 
    {
        return self::juice_Cataleya_Payment_PaymentType($_PaymentType);
        
    }
    
    
    



    /*
     * 
     * [ juice_Transaction ]
     * ______________________________________________________________
     * 
     * @param \Cataleya\Payment\Transaction $_Transaction
     * 
     * @return array (template ready JSON)
     * 
     * 
     */


    
    private static function juice_Cataleya_Payment_Transaction (\Cataleya\Payment\Transaction $_Transaction) 
    {

        
        $json = array (
                'id' => $_Transaction->getID(),
                'TranxID' => '#'.str_pad(strval($_Transaction->getID()), 5, '0', STR_PAD_LEFT),
                'PaymentGateway' => $_Transaction->getPaymentType()->getName(), 
                'AmountPaid' => number_format($_Transaction->getAmount(), 2) . ' ' . $_Transaction->getCurrencyCode(), 
                'Currency' => self::juice($_Transaction->getCurrency()), 
                'PaymentStatus' => $_Transaction->getStatus(), 
                'PaymentStatusClass' => str_replace(' ', '-', $_Transaction->getStatus())
            );

        return $json;

    }

    
    
    /**
     * 
     * @param \Cataleya\Locale\Currency $_Currency
     * 
     * @return array (template ready JSON)
     * 
     */
    private static function juice_Cataleya_Locale_Currency (\Cataleya\Locale\Currency $_Currency) {
        
        $json = array (
                    'id' => $_Currency->getID(), 
                    'code' =>  $_Currency->getCurrencyCode(), 
                    'symbol'    =>  '&#'.$_Currency->getUtfCode().';', 
                    'name'  =>  $_Currency->getCurrencyName(), 
                    'active' => $_Currency->isActive(), 
                    'numeric_code' => $_Currency->getNumericCode(), 
                    'rate' => $_Currency->getRate(), 
                    'minor_unit' => $_Currency->getMinorUnit(), 
                    'symbol_left' => $_Currency->getSymbolLeft(), 
                    'synbol_right' => $_Currency->getSymbolRight(),
                    'value' => $_Currency->getValue()
              );
        
        return $json;
        
    }







    /*
     * 
     * [ juiceDrop_Category ]
     * ______________________________________________________________
     * 
     * @param \Cataleya\Catalog\Tag $_Category
     * @param \Cataleya\Store $_Store
     * 
     * @return array (template ready JSON)
     * 
     * 
     */


    
    private static function juiceDrop_Cataleya_Catalog_Tag (\Cataleya\Catalog\Tag $_Category, $_juice_products = FALSE, $_load_children = FALSE) 
    {

        $_Store = \Cataleya\Front\Shop::getInstance();
        
        $json = array (
            'title'  => $_Category->getDescription()->getTitle(STORE_LANG), 
            'description'   =>  $_Category->getDescription()->getText(STORE_LANG), 
            'sort_order' => $_Category->getSortOrder(), 
            'href'   =>  (!empty($_Store) ? $_Store->getShopLanding() : '') . 'c/' . $_Category->getHandle() . '/' , 
            'size'   =>  $_Category->getPopulation(), 
            'keyword'    => $_Category->getHandle(), 
            'banner' =>  self::juice($_Category->getBannerImage()), 
            'population' => $_Category->getPopulation(),
            'active'   =>  $_Category->isActive(), 
            'is_root' => $_Category->isRoot(), 
            'products' => [], 
            'has_parent' => $_Category->hasParent(), 
            'parent' => $_Category->hasParent() ? self::juiceDrop($_Category->getParent()) : null, 
            'children' => []
         );
        
        if ($_juice_products) foreach ($_Category as $_Product) $json['products'][] = self::juiceDrop($_Product);
        
        
        if ($_load_children) 
        {
            foreach ($_Category->getSubCategories() as $_c) $json['children'][$_c->getHandle()] = self::juiceDrop($_c, $_juice_products, $_load_children);
        
        
            if (!empty($json['children'])) 
            {

                $_sort_array = [];

                foreach  ($json['children'] as $_h=>$_c) $_sort_array[$_h] = $_c['sort_order'];

                array_multisort($_sort_array, SORT_ASC, SORT_STRING, $json['children']);

            }
        }

        return $json;

    }

    
    
    
    
    /*
     * 
     * [ juice_Category ]
     * ______________________________________________________________
     * 
     * @param \Cataleya\Catalog\Tag $_Category
     * @param \Cataleya\Store $_Store
     * 
     * @return array (template ready JSON)
     * 
     * 
     */


    
    private static function juice_Cataleya_Catalog_Tag (\Cataleya\Catalog\Tag $_Category, $_juice_products = FALSE, $_load_children = FALSE) 
    {

        $_Store = \Cataleya\Front\Shop::getInstance();
        
        $json = array (
            'title'  => $_Category->getDescription()->getTitle(STORE_LANG), 
            'description'   =>  $_Category->getDescription()->getText(STORE_LANG), 
            'sort_order' => $_Category->getSortOrder(), 
            'href'   =>  (!empty($_Store) ? $_Store->getShopLanding() : '') . 'c/' . $_Category->getHandle() . '/' , 
            'size'   =>  $_Category->getPopulation(), 
            'keyword'    => $_Category->getHandle(), 
            'banner' =>  self::juice($_Category->getBannerImage()), 
            'products' => array(),
            'active'   =>  $_Category->isActive(), 
            'is_root' => $_Category->isRoot(), 
            'products' => [], 
            'has_parent' => $_Category->hasParent(), 
            'parent' => $_Category->hasParent() ? self::juiceDrop($_Category->getParent()) : null, 
            'children' => [] 
         );
        
        
        if ($_juice_products) foreach ($_Category as $_Product) $json['products'][] = self::juice($_Product);

        if ($_load_children) foreach ($_Category->getSubCategories() as $_c) $json['children'][$_c->getHandle()] = self::juice($_c, $_juice_products, $_load_children);
        

        
        if (!empty($json['children'])) 
        {

            $_sort_array = [];

            for ($_i=0,$_l=count($json['children']); $_i<$_l; $_i++) $_sort_array[$_i] = $json['children'][$_i];

            array_multisort($_sort_array, SORT_ASC, SORT_STRING, $json['children']);

        }
        return $json;

    }

    
    
    
    
    
    
    


    /*
     * 
     * [ juiceDrop_Product ]
     * ______________________________________________________________
     * 
     * @param \Cataleya\Catalog\Product $_Product
     * @param \Cataleya\Store $_Store
     * 
     * @return array (template ready JSON)
     * 
     * 
     */


    
    private static function juiceDrop_Cataleya_Catalog_Product (\Cataleya\Catalog\Product $_Product) 
    {

        $_Store = \Cataleya\Front\Shop::getInstance();
        $price = $_Product->getBasePrice(); 
        $price = ($price !== null) 
            ? $price->getValue($_Store->getStoreId()) 
            : 0; 

        $discount_price = \Cataleya\Plugins\Action::trigger(
                'discount.get-sale-price', [ 
                    'params' => [
                        'store' => $_Store, 
                        'product_id'=>$_Product->getID() 
                    ] , 'data' => $price 
                ] 
            );

        $discount_price = ( is_numeric($discount_price) && floatval($discount_price) > 0 ) ? (float)$discount_price : $price;


        // FETCH TAG (CATEGORY)
        $_Category = (defined('TAG_KEYWORD')) 
                    ? \Cataleya\Catalog\Tag::load(TAG_KEYWORD) 
                    : NULL;
        
        
        
        // If product is not is any category, it shouldn't be visible
        // in the storefront.
        if (empty($_Category)) {
            $_Category = \Cataleya\Front\Shop::getInstance()->getRootCategory();
            define('TAG_KEYWORD', 'all');
        }
        
        
        
        $json = array (

            'id' => $_Product->getProductId(), 
            'ean' => $_Product->getEAN(), 
            'upc' => $_Product->getUPC(), 
            'reference_code' => $_Product->getReferenceCode(), 
            'title' =>  $_Product->getDescription()->getTitle(STORE_LANG), 
            'description' =>  $_Product->getDescription()->getText(STORE_LANG), 
            'base_price'    =>  $price, 
            'is_on_sale'  =>  ($discount_price < $price), 
            'sale_price' => $discount_price,  
            'href'   =>  $_Store->getShopLanding() . 'p/' . $_Category->getHandle() . '/' . $_Product->getHandle() . '/' . $_Product->getProductId()
        );
        
        
        $json['primary_image'] = self::juice($_Product->getPrimaryImage());
        



        return $json;

    }


    
    
    
    
    
    
 


    /*
     * 
     * [ juice_Product ]
     * ______________________________________________________________
     * 
     * @param \Cataleya\Catalog\Product $_Product
     * @param \Cataleya\Store $_Store
     * 
     * @return array (template ready JSON)
     * 
     * 
     */


    private static function juice_Cataleya_Catalog_Product (\Cataleya\Catalog\Product $_Product) 
    {
        
        
        
        
        $_Store = \Cataleya\Front\Shop::getInstance();
        
        // FETCH TAG (CATEGORY)
        $_Category = (defined('TAG_KEYWORD')) 
                    ? \Cataleya\Catalog\Tag::load(TAG_KEYWORD) 
                    : NULL;
        
        
        
        // If product is not is any category, it shouldn't be visible
        // in the storefront.
        if (empty($_Category)) {
            $_Category = \Cataleya\Front\Shop::getInstance()->getRootCategory();
            define('TAG_KEYWORD', 'all');
        }
        
        
        
        $price = $_Product->getBasePrice(); 
        $price = ($price !== null) 
            ? $price->getValue($_Store->getStoreId()) 
            : 0; 

        $discount_price = \Cataleya\Plugins\Action::trigger(
                'discount.get-sale-price', [ 
                    'params' => [
                        'store' => $_Store, 
                        'product_id'=>$_Product->getID() 
                    ] , 'data' => $price 
                ] 
            );
        
        $discount_price = ( is_numeric($discount_price) && floatval($discount_price) > 0 ) ? (float)$discount_price : $price;

        $json = array (
            'id'    =>  $_Product->getProductId(), 
            'title' =>  $_Product->getDescription()->getTitle(STORE_LANG), 
            'description'   => str_replace("\n", '<br />', $_Product->getDescription()->getText(STORE_LANG)), 
            'base_price'    => $price, 
            'is_on_sale'  =>  ($discount_price < $price), 
            'sale_price' => $discount_price,  
            'out_of_stock'  =>  true, 
            'href'   =>  $_Store->getShopLanding() . 'p/' . $_Category->getHandle() . '/' . $_Product->getHandle() . '/' . $_Product->getProductId()

        );



        // Product Group
        $_ProductGroup = $_Product->getProductGroup();
        $json['product_group'] = array ();

        foreach ($_ProductGroup as $_GrpMember) 
        {
            $tag = NULL;
            $tag_ids = $_GrpMember->getProductTagIDs();

            // Load any category this product is associated with...
            foreach ($tag_ids as $tag_id) 
            {
                $tag = \Cataleya\Catalog\Tag::load($tag_id);
                if ($tag !== NULL) break;
            }

            if (empty($tag)) continue;

            $json['product_group'][] = array (
            'id'    =>  $_GrpMember->getProductId(), 
            'title' =>  $_GrpMember->getDescription()->getTitle(STORE_LANG), 
            'description'   => $_GrpMember->getDescription()->getText(STORE_LANG), 
            'image' => self::juice($_GrpMember->getPrimaryImage()), 
            'href'   =>  $_Store->getShopLanding() . 'p/' . $tag->getHandle() . '/' . $_GrpMember->getHandle() . '/' . $_GrpMember->getProductId()
            );

        }


        // 2. Primary Product Image
        $json['primary_image'] = self::juice($_Product->getPrimaryImage());
        
        

        


        // 3. Base Product Images
        $product_images = $_Product->getBaseImages();

        $json['images'] = array ();


        foreach ($product_images as $image) 
        {
            $_img_id = $image->getID();
            $json['images'][$_img_id] = self::juice($image);
            $json['images'][$_img_id]['is_primary'] = ((int)$_img_id === (int)$json['primary_image']['id']) ? true : false;
        }





        // 4. ATTRIBUTES


        $_ProductType = $_Product->getProductType();
        $json['attributes'] = array ();
        
        foreach ($_ProductType as $_AttributeType) {
            $json['attributes'][] = self::juice($_AttributeType, $_Product);
        }



        // 5. Product Option Pricing and Stock

        
        
        $json['options'] = array ();
        $json['attributes_to_options'] = array ();
        
        

        foreach ($_Product as $_Option) 
        {

            $option_data = self::juice($_Option, \Cataleya\Front\Shop::getInstance());

            // $option_data['pid'] contains a concatination of all attribute IDs.
            // We will use this to help javascript select a [product option] based on the combination
            // of attributes our shopper selects.
            $json['attributes_to_options'][$option_data['pid']] = $option_data['id'];
            $json['options'][$_Option->getID()] = $option_data;
            
            
            if ((int)$option_data['stock'] > 0) { $json['out_of_stock'] = FALSE; }

        }





        // 6. Tag (category) info


        $json['category'] = array (
           'title'  => $_Category->getDescription()->getTitle(STORE_LANG), 
           'description'   =>  $_Category->getDescription()->getText(STORE_LANG), 
           'href'   =>  $_Store->getShopLanding() . 'c/' . $_Category->getHandle() . '/', 
           'banner' =>  $_Category->getBannerImage()->getHrefs()
        );


        // 7. META
        if (!isset($json['meta'])) $json['meta'] = [];
        if (!isset($json['meta']['keywords'])) $json['meta']['keywords'] = '';

        $json['meta']['keywords'] .= $_Product->getMetaKeywords()->getText(STORE_LANG);
        $json['meta']['description'] = $_Product->getMetaDescription()->getText(STORE_LANG);




        // 8. SHARE BUTTONS

        $json['Social'] = array (

            // Pinterest
            'Pinterest' => array (
                'url' => urlencode(($_Store->getShopLanding() . 'p/' . $_Category->getHandle() . '/' . $_Product->getHandle() . '/' . $_Product->getProductId())), 
                'media' => urlencode($_Product->getPrimaryImage()->getHref(\Cataleya\Asset\Image::LARGE)), 
                'description' => urlencode($_Product->getDescription()->getTitle(STORE_LANG))
            )
        );
        
        
        return $json;




    }
    
    
    
    
    
    /**
     * 
     * @param \Cataleya\Asset\Image $_Image
     * @return array
     * 
     */
    private static function juice_Cataleya_Asset_Image (\Cataleya\Asset\Image $_Image) 
    {
            return array (
                        'id'    =>  $_Image->getID(), 
                        'src' =>  $_Image->getHrefs(), 
                        'hrefs' =>  $_Image->getHrefs()
                    );
    }
    
    
    
    


    /*
     * 
     * [ juice_ProductOption ]
     * ______________________________________________________________
     * 
     * @param \Cataleya\Catalog\Product\Option $_ProductOption
     * 
     * @return array (template ready JSON)
     * 
     * 
     */
    private static function juice_Cataleya_Catalog_Product_Option (\Cataleya\Catalog\Product\Option $_Option) 
    {



        $_Store = \Cataleya\Front\Shop::getInstance();
        
        $pseudo_id = "";

        $price = $_Option->getPrice()->getValue($_Store->getStoreID());
        
        
        
        
        $stock = $_Option->getStock()->getValue($_Store->getStoreID());
        $image = $_Option->getPreviewImage();
        $_Product = $_Option->getProduct();
        $_ProductType = $_Product->getProductType();
        
        
        $discount_price = \Cataleya\Plugins\Action::trigger(
                'discount.get-sale-price', [ 
                    'params' => [
                        'store' => $_Store, 
                        'product_id'=>$_Product->getID() 
                    ] , 'data' => $price 
                ] 
            );
        
        $discount_price = ( is_numeric($discount_price) && floatval($discount_price) > 0 ) ? (float)$discount_price : $price;
        $salePrice = ($discount_price < $price) ? $discount_price : 0;
                
        $_APP = \Cataleya\System::load();
        
        list($price_left, $price_right) = array_pad(explode('.', number_format($price).""), 2, '00');
        
        $option_data = array (
                'id' => $_Option->getID(), 
                'description' => array (), 
                'product_id' => $_Option->getProductId(), 
                // 'ProductType' => self::juice($_ProductType, $_Option),
                'attributes' => array (), 
                'price' =>  ($price === NULL) ? '0' : $price, 
                'formatted_price' => number_format($price), 
                'price_left' => $price_left, 
                'price_right'=> $price_right, 
                'sale_price' =>  $salePrice, 
                'formatted_sale_price' => number_format($salePrice), 
                'stock' =>  ($stock === NULL) ? '0' : $stock, 
                'image' => ($image === NULL || $image->getID() === 0) ? false : self::juice($image), 
                'ean' => $_Option->getEAN(), 
                'has_EAN' => ($_ProductType->hasEAN() && $_APP->eanEnabled()), 
                'upc' => $_Option->getUPC(),  
                'has_UPC' => ($_ProductType->hasUPC() && $_APP->upcEnabled()), 
                'sku' => $_Option->getSKU(),   
                'has_SKU' => ($_ProductType->hasSKU() && $_APP->skuEnabled()), 
                'isbn' => $_Option->getISBN(),  
                'has_ISBN' => ($_ProductType->hasISBN() && $_APP->isbnEnabled()), 
            );

        foreach ($_Option as $_Attribute) 
        {
            $pseudo_id .= $_Attribute->getID();
            $_AttributeType = $_Attribute->getAttributeType();

            $option_data['attributes'][$_AttributeType->getName()] = array (
                'id' => $_Attribute->getID(), 
                'label' => $_AttributeType->getName(), 
                'value' => $_Attribute->getValue()
            );

            $option_data['description'][] = $_Attribute->getValue();

        }

        
        
        
        $option_data['description'] = implode(', ', $option_data['description']); 
        $option_data['pid'] = $pseudo_id;

        
        
        

        return $option_data;


    }

    
    
    
    
    
    
    
    /**
     * 
     * @param \Cataleya\Catalog\Product\Option $_Option
     * @return array
     * 
     */
    private static function juiceDrop_Cataleya_Catalog_Product_Option (\Cataleya\Catalog\Product\Option $_Option) 
    {

        return self::juice_Cataleya_Catalog_Option($_Option);
        
    }
    
    

    
    
    

    /**
     * 
     * 
     * @param \Cataleya\Catalog\Product\Type $_ProductType
     * 
     * @return array (template ready JSON)
     * 
     * 
     */
    private static function juice_Cataleya_Catalog_Product_Type (\Cataleya\Catalog\Product\Type $_ProductType, \Cataleya\Catalog\Product $_Product = NULL) 
    {


        $_info = array (

            'id'    =>  $_ProductType->getID(), 
            'name' =>  $_ProductType->getName(), 
            'name_plural' =>  $_ProductType->getNamePlural(),
            'code' => $_ProductType->getCode(), 
            'icon_href' =>  $_ProductType->getIconHref(),
            'has_EAN' => $_ProductType->hasEAN(), 
            'has_UPC' => $_ProductType->hasUPC(), 
            'has_SKU' => $_ProductType->hasSKU(),  
            'has_ISBN' => $_ProductType->hasISBN(),  
            'attribute_types' => array ()
        );

        foreach ($_ProductType as $_AttributeType) {
            
            $_info['attribute_types'][] = self::juice($_AttributeType, $_Product);
        }
        return $_info;


    }

    
    
    
    

    /*
     * 
     * [ juice_AttributeType ]
     * ______________________________________________________________
     * 
     * @param \Cataleya\Catalog\Product\Attribute\Type $_AttributeType
     * 
     * @return array (template ready JSON)
     * 
     * 
     */



    private static function juice_Cataleya_Catalog_Product_Attribute_Type (\Cataleya\Catalog\Product\Attribute\Type $_AttributeType, \Cataleya\Catalog\Product $_Product = NULL) 
    {


        $_info = array(
                'id' => $_AttributeType->getID(),
                'label' => $_AttributeType->getName(), 
                'label_plural' => $_AttributeType->getNamePlural(), 
                'code' => $_AttributeType->getCode(), 
                'values' => array (),  
                // 'on' => (!empty($_Product)) ? $_Product->getProductType()->hasAttributeType($_AttributeType) : false, 
                'no_attribute' => true
            );
       
        
        
        foreach ($_AttributeType as $_Attribute) 
        {
            if (!empty($_Product) && !$_Product->hasAttribute($_Attribute)) continue;
                    
            if ($_info['no_attribute']) $_info['no_attribute'] = FALSE;
            
            $_info['values'][] = array (
                'id' => $_Attribute->getID(), 
                'label' => $_AttributeType->getName(), 
                'label_plural' => $_AttributeType->getNamePlural(),
                'code' => $_AttributeType->getCode(), 
                'value' => $_Attribute->getValue()
            );
        }

        return $_info;


    }

    
    
    
    

    /*
     * 
     * [ juice_Attribute ]
     * ______________________________________________________________
     * 
     * @param \Cataleya\Catalog\Product\Attribute $_Attribute
     * 
     * @return array (template ready JSON)
     * 
     * 
     */



    private static function juice_Cataleya_Catalog_Product_Attribute (\Cataleya\Catalog\Product\Attribute $_Attribute) 
    {

        $_AttributeType = $_Attribute->getAttributeType();
        
        $_info = array (
                    'id' => $_Attribute->getID(), 
                    'label' => $_AttributeType->getName(), 
                    'label_plural' => $_AttributeType->getNamePlural(),
                    'value' => $_Attribute->getValue()
                );

        return $_info;


    }

    
    
    
    
    
    
    
    
    
    
    
    
    
    

    
    
    
    
    
    /////////////////////// CUSTOMER //////////////////////////////
    
    
    
    
    


    /*
     * 
     * [ juice_Customer ]
     * ______________________________________________________________
     * 
     * @param \Cataleya\Customer $_Customer
     * 
     * @return array (template ready JSON)
     * 
     * 
     */


    
    private static function juice_Cataleya_Customer (\Cataleya\Customer $_Customer) 
    {

        $_Store = \Cataleya\Front\Shop::getInstance();
        
        $json = array (
        'id' => $_Customer->getCustomerId(), 
        'name' => $_Customer->getName(), 
        'firstname' => $_Customer->getFirstname(), 
        'lastname'  =>  $_Customer->getLastname(), 
        'fullname'  =>  $_Customer->getName(), 
        'phone' => $_Customer->getTelephone(), 
        'email' =>  $_Customer->getEmailAddress(), 
        'confirm'   => array (
            'href'  =>  $_Store->getShopLanding().'confirm?id=' . 
                        $_Customer->getConfirmDigest() . 
                        ':' . $_Customer->getConfirmCode(), 
            'numCode'   =>  $_Customer->getConfirmCode(), 
            'digest' =>  $_Customer->getConfirmDigest(),
            'confirmToken' =>  (isset($_SESSION[SESSION_PREFIX.'CONFIRM_TOKEN'])) ? $_SESSION[SESSION_PREFIX.'CONFIRM_TOKEN'] : ''
            )
        );
        


        return $json;

    }

    
    
    
    
    
    
    
    
    
    
    
    



    /*
     * 
     * [ juice_Cart ]
     * ______________________________________________________________
     * 
     * @param \Cataleya\Customer\Cart $_Cart
     * @param \Cataleya\Store $_Store
     * 
     * @return array (template ready JSON)
     * 
     * 
     */


    private static function juice_Cataleya_Customer_Cart (\Cataleya\Customer\Cart $_Cart) 
    {
        
        $_Store = \Cataleya\Front\Shop::getInstance();
        
        // CART info
        $json = array (

            'cart_id'   =>  $_Cart->getCartId(), 
            'key'   =>  $_Cart->getSecureKey(), 
            'totalQuantity'  =>  0, 
            'totalAmount'  =>  0, 
            'subTotal' => $_Cart->getTotal(TRUE, TRUE),
            'grandTotal' => $_Cart->getTotal(), 
            'Discounts' => $_Cart->getDiscounts(), 
            'items' =>  array (), 
            'newCartItem' => array (), 
            'anyOutOfStock' =>  FALSE, 
            'isEmpty' =>  $_Cart->isEmpty()

            );
        
        foreach ($_Cart as $_CartItem) {
            
            $json['items'][] = self::juice($_CartItem, $_Store);
        
            $json['totalQuantity'] += $_CartItem->getQuantity();
            $json['totalAmount'] += $_CartItem->getSubtotal();
            if (!$_CartItem->inStock($_Store)) $json['cart']['anyOutOfStock'] = TRUE;
        
        }
        
        
        
        return $json;
        
        
    }
    
    
    
    
    
    /*
     * 
     * [ juice_CartItem ]
     * ______________________________________________________________
     * 
     * @param \Cataleya\Customer\Cart $_Cart
     * @param \Cataleya\Store $_Store
     * 
     * @return array (template ready JSON)
     * 
     * 
     */


    private static function juice_Cataleya_Customer_Cart_Item (\Cataleya\Customer\Cart\Item $_CartItem) 
    {
        
        
        $_Store = \Cataleya\Front\Shop::getInstance();
        $_Product = $_CartItem->getProduct();
        $tag_ids = $_Product->getProductTagIDs();
        
        // Load any category this product is associated with...
        foreach ($tag_ids as $tag_id) 
        {
            $tag = \Cataleya\Catalog\Tag::load($tag_id);
            if ($tag !== NULL) break;
        }
        
        if (empty($tag)) return [];
        
        $item_opt = $_CartItem->getProductOption();
        $opt_image = $item_opt->getPreviewImage();
        // $subtotal = $_CartItem->getSubtotal();
        
        

        // ATTRIBUTES
        
        $attributes = array ();

        foreach ($item_opt as $_Attribute) 
        {
            $_AttributeType = $_Attribute->getAttributeType();

            $attributes[] = array (
                'id' => $_Attribute->getID(), 
                'label' => $_AttributeType->getName(), 
                'value' => $_Attribute->getValue()
            );

        }



        $json = array (
            
            'id'    =>  $_CartItem->getID(),
            'title' =>   $_Product->getDescription()->getTitle(STORE_LANG), 
            'description' =>   $_Product->getDescription()->getText(STORE_LANG), 
            'product_id'    =>  $_Product->getProductId(), 
            'option_id'    =>  $item_opt->getID(), 
            'attributes'  =>  $attributes, 
            'quantity'  =>  $_CartItem->getQuantity(), 
            'price' => $_CartItem->getCost(), 
            'stock' =>  (int)$_CartItem->getStock($_Store), 
            'subtotal'  => $_CartItem->getSubtotal(), 
            'image' => ($opt_image instanceof \Cataleya\Asset\Image && $opt_image->getID() !== 0) ? $opt_image->getHrefs() : $_Product->getPrimaryImage()->getHrefs(), 
            'href'   =>  $_Store->getShopLanding() . 'p/' . $tag->getHandle() . '/' . $_Product->getHandle() . '/' . $_Product->getProductId()

            );
        
        
        
        return $json;
        
    }
    
    
    
    
    
    
    




    /*
     * 
     * [ juice_Wishlist ]
     * ______________________________________________________________
     * 
     * @param \Cataleya\Customer\Wishlist $_Wishlist
     * @param \Cataleya\Store $_Store
     * 
     * @return array (template ready JSON)
     * 
     * 
     */


    private static function juice_Cataleya_Customer_Wishlist (\Cataleya\Customer\Wishlist $_Wishlist) 
    {
        
        $_Store = \Cataleya\Front\Shop::getInstance();
        
        $_Customer = (IS_LOGGED_IN) 
            ? \Cataleya\Customer::load($_SESSION[SESSION_PREFIX.'CUSTOMER_ID']) 
            : NULL;
        
        $_WishlistOwner = $_Wishlist->getCustomer();
        
        // CART info
        $json = array (

            'cart_id'   =>  $_Wishlist->getWishlistId(), 
            'key'   =>  $_Wishlist->getSecureKey(), 
            'totalQuantity'  =>  0, 
            'totalAmount'  =>  0, 
            'items' =>  array (), 
            'newWishlistItem' => array (), 
            'anyOutOfStock' =>  FALSE, 
            'isEmpty' =>  $_Wishlist->isEmpty(), 
            'ownerFirstName'    =>  $_WishlistOwner->getFirstname(), 
            'ownerLastName'    =>  $_WishlistOwner->getLastname(), 
            'ownerDisplayName'  =>  ((IS_LOGGED_IN && $_Customer->getCustomerId() === $_WishlistOwner->getCustomerId()) ? 'Your' : $_WishlistOwner->getFirstname() ."'s")


            );
        
        foreach ($_Wishlist as $_WishlistItem) {
            
            $json['items'][] = self::juice($_WishlistItem, $_Store);
            $stock = (int)$_WishlistItem->getProductOption()->getStock()->getValue($_Store);
        
            $json['totalQuantity'] += $_WishlistItem->getQuantity();
            $json['totalAmount'] += $_WishlistItem->getSubtotal($_Store);
            if ((float)$stock < (float)$_WishlistItem->getQuantity()) $json['wishlist']['anyOutOfStock'] = TRUE;
        
        }
        
        return $json;
        
        
    }
    
    
    
    
    
    /*
     * 
     * [ juice_WishlistItem ]
     * ______________________________________________________________
     * 
     * @param \Cataleya\Customer\Cart $_Cart
     * @param \Cataleya\Store $_Store
     * 
     * @return array (template ready JSON)
     * 
     * 
     */


    private static function juice_Cataleya_Customer_Wishlist_Item (\Cataleya\Customer\Wishlist\Item $_WishlistItem) 
    {
        
        
        $_Store = \Cataleya\Front\Shop::getInstance();
        $_Product = $_WishlistItem->getProduct();
        $tag_ids = $_Product->getProductTagIDs();
        
        // Load any category this product is associated with...
        foreach ($tag_ids as $tag_id) 
        {
            $tag = \Cataleya\Catalog\Tag::load($tag_id);
            if ($tag !== NULL) break;
        }
        
        if (empty($tag)) return [];
        
        $item_opt = $_WishlistItem->getProductOption();
        $opt_image = $item_opt->getPreviewImage();
        $stock = (int)$_WishlistItem->getProductOption()->getStock()->getValue($_Store);
        // $subtotal = $_CartItem->getSubtotal();
        
        

        // ATTRIBUTES
        
        $attributes = array ();

        foreach ($item_opt as $_Attribute) 
        {
            $_AttributeType = $_Attribute->getAttributeType();

            $attributes[] = array (
                'id' => $_Attribute->getID(), 
                'label' => $_AttributeType->getName(), 
                'value' => $_Attribute->getValue()
            );

        }



        $json = array (
            
            'id'    =>  $_WishlistItem->getID(),
            'title' =>   $_Product->getDescription()->getTitle(STORE_LANG), 
            'description' =>   $_Product->getDescription()->getText(STORE_LANG), 
            'product_id'    =>  $_Product->getProductId(), 
            'option_id'    =>  $item_opt->getID(), 
            'attributes'  =>  $attributes, 
            'quantity'  =>  $_WishlistItem->getQuantity(), 
            'price' => $_WishlistItem->getCost($_Store), 
            'stock' =>  $stock, 
            'subtotal'  => $_WishlistItem->getSubtotal($_Store), 
            'image' => ($opt_image instanceof \Cataleya\Asset\Image) ? $opt_image->getHrefs() : $_Product->getPrimaryImage()->getHrefs(), 
            'href'   =>  $_Store->getShopLanding() . 'p/' . $tag->getHandle() . '/' . $_Product->getHandle() . '/' . $_Product->getProductId()

            );
        
        
        
        return $json;
        
    }
    

    



        

 
}


