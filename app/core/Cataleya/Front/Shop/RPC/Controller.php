<?php



namespace Cataleya\Front\Shop\RPC;
use Cataleya\Front\Shop\RPC;
use \Cataleya\Front\Shop\Controller as Main;



/*
 *
 * 	@Package: Cataleya
 * 	@Class: \Cataleya\Front\Shop\RPC\Controller
 *
 * 	(c) 2014 Fancy Paper Planes
 *
 *
 *
 */

class Controller 
{
    
    
    
    public function __construct() {
       
        
    }
    
    
    


    public static function load () {



        switch (self::getVersion()) 
        {
            
            case 'cataleya.1.0':
                return RPC\v1\Resource::load();

            case 'cataleya.1.1':
                return RPC\v1\Resource::load();

            default:
                header('HTTP/1.0 404 Resource not found'); exit();
        }
        
    }
    




    public static function getVersion () 
    {

        static $_version;
        if (empty($_version)) 
        {
            preg_match(
            '!^application\/(?<version>[a-z0-9\.\-_]+)\+json$!',
            strtolower ($_SERVER['HTTP_ACCEPT']),  
            $_m
            ); // 'Application/cataleya.1.0+json'

            $_version = isset($_m['version']) ? $_m['version'] : null;
        }
        
        return $_version;

    }

        

    


        
 
}


