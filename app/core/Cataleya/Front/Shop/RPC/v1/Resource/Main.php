<?php



namespace Cataleya\Front\Shop\RPC\v1\Resource;
use \Cataleya\Error;


/*
 *
 * 	@Package: Cataleya
 * 	@Class: \Cataleya\Front\Shop\RPC\v1\Resource\Main
 *
 * 	(c) 2014 Fancy Paper Planes
 *
 *
 *
 */

abstract class Main 
{
    
    

    private static $_post_data = [];





    public function __construct() {

        
        
    }
    
    
    

    public static function load () {

        return new static ();

    }
    
    




    /*
     * 
     * [ execute ]
     * ______________________________________________________________
     * 
     * 
     * 
     * 
     */


    public function execute ()  
    {

        $_response = [
            'result'=> null, 
            'id'=> null, 
            'error'=> null
        ];

        // 1. digest POST data...
        try {
            $this->digestPostData();
        } catch (Error $e) {
            $_response['error'] = [
                    'code'=> $e->getCode(), 
                    'message'=> $e->getMessage()
            ];

            $this->respond($_response);
        }
        


        // 2. Check if method exists...
        $_m = $this->getAPIMethod();
        if (!method_exists($this, $_m)) {

            $_response['error'] = [
                    'code'=> 0, 
                    'message'=> "Method not found."
            ];

            $this->respond($_response);
        }



        // 3. Attempt to execute method...
        try {

            $_response['id'] = $this->getRequestID();
            $_response['result'] = $this->$_m();

        } catch (Error $e) {
            $_response['id'] = $this->getRequestID();
            $_response['error'] = [
                    'code'=> $e->getCode(), 
                    'message'=> $e->getMessage()
            ];
        }


        $this->respond($_response);

    }



    protected function respond ($_response) 
    {

        header('Content-type: application/x-json');
        echo json_encode($_response);
        
        exit();
    }


    
    public function getPostData() 
    {

        return self::$_post_data;

    }


    public function getRequestID() 
    {
        return self::$_post_data['id'];
    }


    public function getAPIMethod() 
    {
        return self::$_post_data['method'];
    }
    

    
    private function digestPostData () 
    {

        $WHITE_LIST = array(
                        'id', // rpc: id
                        'method', // rpc: method
                        'params',// rpc: // params
                        'attachment' 
                    );


        // DIE IF INPUT IS NOT VIA POST METHOD...
        if ($_SERVER['REQUEST_METHOD'] != 'POST') throw new Error ('Method Not Allowed.');

        // SANITIZE INPUT DATA...
        $_CLEAN = filter_input_array(INPUT_POST, array(
                                                    'id'	=>	FILTER_VALIDATE_INT, 
                                                    'params'	=>	array( 'flags' => FILTER_REQUIRE_ARRAY ), 
                                                    'method'	=>	array(
                                                                'filter'		=>	FILTER_VALIDATE_REGEXP, 
                                                                'options'	=>	array('regexp' => '/^[A-Za-z ]{1,50}$/')
                                                                                                                ), 

                                                            )
                                                
                            );


        // Check if INPUT keys are ALL expected AND MADE IT THROUGH...
        // $_suspect = anySuspects();
        // if ($_suspect !== FALSE) throw new Error ('Error processing white_list!' . implode('+', $_suspect));

        // VALIDATE AUTH TOKEN...
        
        //$_token = \Cataleya\Helper::getAuthToken();
        //if(!isset($_SESSION[SESSION_PREFIX.'AUTH_TOKEN']) || $_token !== $_SESSION[SESSION_PREFIX.'AUTH_TOKEN']) {
        //    throw new Error ('Access Denied: '.$_token.'|'.$_SESSION[SESSION_PREFIX.'AUTH_TOKEN']);
        //} else 
        
        self::$_post_data = $_CLEAN;


    }







    /**
     * digestParams
     *
     * @param array $_expected
     * @param array $_data
     * @return array
     */
    protected function digestParams (array $_expected, $_abort_on_failure=true) 
    {

        $_params = \Cataleya\Helper\Validator::params($_expected, self::$_post_data['params']);
        if (!isset($_params['auto_confirm']) || !is_bool($_params['auto_confirm'])) $_result['auto_confirm'] = false;

        $_bad_params = array ();
        

        if (!isset($_params['language_code'])) 
        {
            $_params['language_code'] = __('shop')->getLanguageCode();
        }
        
        
        
        // Check if every one made it through
        foreach ($_params as $k=>$v) {
            if ($v === NULL) $_bad_params[] = $k;
        }
        

        if (!empty($_bad_params) && $_abort_on_failure) throw new Error ('One or more invalid parameters: ' . implode(', ', $_bad_params));
        
        return $_params;
    }




    protected function juiceCatalog () 
    {
        
        $_info = array (
            array (
                'id'    =>  'all', 
                'population'    => \Cataleya\System::load()->getProductCount(), 
                'isActive'  =>  TRUE // $_Category->isActive()
            )
        );
        
        
        foreach (\Cataleya\Catalog\Tags::load(array('show_hidden'=>TRUE)) as $_Category) $_info[] = \Cataleya\Front\Shop\Juicer::juiceDrop($_Category);
        
        return $_info;
        
        
    }




    

    /*
     * 
     * 
     * CALLBACKS
     * 
     */


    protected function void () {
        
        return [];
    }





        

 
}


