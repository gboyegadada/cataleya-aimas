<?php



namespace Cataleya\Front\Shop\RPC\v1\Resource;
use \Cataleya\Error;


/*
 *
 * 	@Package: Cataleya
 * 	@Class: \Cataleya\Front\Shop\RPC\v1\Resource\Product
 *
 * 	(c) 2014 Fancy Paper Planes
 *
 *
 *
 */

class Product 
extends \Cataleya\Front\Shop\RPC\v1\Resource\Main 
{
    
    


    public function __construct() {

        
        
    }
    
    

    /*
     * 
     * 
     * CALLBACKS
     * 
     */









    /**
     * getProductInfo
     *
     * @return void
     */
    protected function getProductInfo () {

        $json_reply = [];
     
        $_params = $this->digestParams([
             'target_id' => 'int'
        ]);

        $_Product = \Cataleya\Catalog\Product::load($_params['target_id']);
        if ($_Product === NULL ) throw new Error ('Product could not be found.');
        
        $json_reply['message'] = 'Product info.';
        $json_reply['Product'] = \Cataleya\Front\Shop\Juicer::juice($_Product);


        return $json_reply;
    }





    /**
     * getProductOptionInfo
     *
     * @return void
     */
    protected function getProductOptionInfo () {

        $json_reply = [];
     
        $_params = $this->digestParams([
             'target_id' => 'int'
        ]);

        $_ProductOption = \Cataleya\Catalog\Product\Option::load($_params['target_id']);
        if ($_ProductOption === NULL ) throw new Error ('Product variant could not be found.');
        
        $json_reply['message'] = 'Product option info.';
        $json_reply['ProductOption'] = \Cataleya\Front\Shop\Juicer::juice($_ProductOption);
        $json_reply['ProductOption']['isNew'] = false;


        return $json_reply;
    }







    /**
     * search
     *
     * @return void
     */
    protected function search () {

        $json_reply = [];
     
        $_params = $this->digestParams([
             'text' => 'string'
        ]);
        
        
        // First, take out funny looking characters...
        $_params['text'] = preg_replace('/[^-\s\.A-Za-z0-9@*#+]+/', '', $_params['text']);


        // Load search results as a collection of objects...
        $search_results = \Cataleya\Catalog::search($_params['text'], array('show_hidden'=>TRUE));


        // $_item_num = $search_results->getPageStart();
        $json_reply['Products'] = array ();

        foreach ($search_results as $_Product) $json_reply['Products'][] = \Cataleya\Front\Shop\Juicer::juiceDrop($_Product);
        
        $json_reply['message'] = \Cataleya\Helper::countInEnglish($search_results->getPopulation(), 'Product', 'Products') . ' found.';
        $json_reply['count'] = $search_results->getPopulation();


        return $json_reply;
    }




}
