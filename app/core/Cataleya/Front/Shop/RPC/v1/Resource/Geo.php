<?php



namespace Cataleya\Front\Shop\RPC\v1\Resource;
use \Cataleya\Error;


/*
 *
 * 	@Package: Cataleya
 * 	@Class: \Cataleya\Front\Shop\RPC\v1\Resource\Pages
 *
 * 	(c) 2014 Fancy Paper Planes
 *
 *
 *
 */

class Geo 
extends \Cataleya\Front\Shop\RPC\v1\Resource\Main 
{
    
    


    public function __construct() {

        
        
    }
    
    

    /*
     * 
     * 
     * CALLBACKS
     * 
     */






    /**
     * getProvinces
     *
     * @return void
     */
    protected function getProvinces () {

        $json_reply = [];
        
        $_params = $this->digestParams(['country_code' => 'iso']);

        $_Country = \Cataleya\Geo\Country::load($_params['country_code']);

        if (empty($_Country)) throw new Error('Country could not be loaded.');


        $provinces = array ();

        foreach ($_Country as $province) 
        {
            $provinces[] = array (
                'id'    =>  $province->getID(), 
                'iso'   =>  $province->getIsoCode(), 
                'name'  =>  $province->getName(), 
                'printableName' => $province->getPrintableName()
            );
        }

        // OUTPUT...
        $json_reply = array ( 
                "message"=>(($_Country->getPopulation() < 1) ? 'No': $_Country->getPopulation()) . ' provinces found.', 
                "provinces"=>$provinces, 
                "count"=>$_Country->getPopulation()
                );


        return $json_reply;
    }






    /**
     * getCharges
     *
     * @return void
     */
    protected function getCharges () {

        $json_reply = [];
        
        $_params = $this->digestParams([
            'province_id'=>'int', 
            'country_code' => 'iso', 
            'shipping_option_id' => 'int'
            ]);

        $_Country = \Cataleya\Geo\Country::load($_params['country_code']);
        if ($_Country === NULL) throw new Error('Country could not be loaded.');

        $_Province = ($_Country->hasProvinces() && (int)$_params['province_id'] !== 0) ? \Cataleya\Geo\Province::load($_params['province_id']) : NULL;
        if ($_Country->hasProvinces() && empty($_Province)) throw new Error('Province could not be loaded.');


        $_ShippingOption = \Cataleya\Shipping\ShippingOption::load($_params['shipping_option_id']);
        if (empty($_ShippingOption)) throw new Error('Delivery option could not be loaded.');

        $cart = __('shop.cart');
        $_charges = $cart->calcShippingCharges($_ShippingOption, (!empty($_Province) ? $_Province : $_Country));
        $_Currency = $cart->getStore()->getCurrency();
        $_charges["FancyAmount"] = ('&#'.$_Currency->getUtfCode().'; ') . number_format($_charges['amount'], 2);
        $_charges["symbol"] = ('&#'.$_Currency->getUtfCode().'; ');

        //$_charges['subTotal'] = $cart->getTotal(TRUE, TRUE);
        $_charges['grandTotal'] = $_charges['amount_tax_included'] + $cart->getTotal();

        // add taxes
        $_ProductTaxes = $cart->getTaxes((!empty($_Province) ? $_Province : $_Country));
        if (!empty($_ProductTaxes)) foreach ($_ProductTaxes as $_Tax) $_charges['grandTotal'] += $_Tax['amount'];

        $_charges['grandTotal'] = number_format($_charges['grandTotal'], 2);


        // OUTPUT...
        $json_reply = [
                "message"=> 'Shipping charges.', 
                "ShippingCharges" => $_charges

                ];

        return $json_reply;
    }






    
    

    
      
      
    

        

 
}


