<?php



namespace Cataleya\Front\Shop\RPC\v1\Resource;
use \Cataleya\Error;


/*
 *
 * 	@Package: Cataleya
 * 	@Class: \Cataleya\Front\Shop\RPC\v1\Resource\Pages
 *
 * 	(c) 2014 Fancy Paper Planes
 *
 *
 *
 */

class Shipping 
extends \Cataleya\Front\Shop\RPC\v1\Resource\Main 
{
    
    


    public function __construct() {

        
        
    }
    
    

    /*
     * 
     * 
     * CALLBACKS
     * 
     */






    /**
     * getOptions
     *
     * @return void
     */
    protected function getOptions () {

        $json_reply = [];
        
        $_params = $this->digestParams(['province_id'=>'int', 'country_code' => 'iso']);

        $_Country = $_Province = NULL;

        $_Province = \Cataleya\Geo\Province::load($_params['province_id']);

        if (empty($_Province)) 
        {
            $_Country = \Cataleya\Geo\Country::load($_params['country_code']);
        } else {
            $_Country = $_Province->getCountry();
        }


        if (empty($_Country)) throw new Error('Country could not be loaded.');

        if (empty($_Province) && $_Country->hasProvinces()) throw new Error('Province must be specified.');


        // Load Store
        $Store = __('shop');
        if ($Store === NULL) throw new Error('Store could not be loaded.');

        // Load shipping options
        $ShippingOptions = \Cataleya\Shipping\ShippingOptions::load($Store);


        // retrieve info about tax rates
        $shipping_options_info = array ();

        foreach ($ShippingOptions as $ShippingOption) 
        {

                $zone = $ShippingOption->getShippingZone();
                if (!empty($_Province) && !$zone->hasLocation($_Province)) continue;

                if (empty($_Province) && !$zone->hasLocation($_Country)) continue;


                // retrieve info about shipping carrier

                $Carrier = $ShippingOption->getCarrier();

                $carrier_info = array (
                        'id'    =>  $Carrier->getID(), 
                        'name'  =>  $Carrier->getCompanyName()
                    );



                // retrieve info about zone


                $zone_info = array (
                        'id'    =>  $zone->getID(), 
                        'name'  =>  $zone->getZoneName(), 
                        'listType'  =>  $zone->getListType(),
                        'countryPopulation' => $zone->getCountryPopulation(), 
                        'provincePopulation' => $zone->getProvincePopulation()
                    );


                // retrieve info about shipping option

                $shipping_options_info[] = array (
                    'id'   =>  $ShippingOption->getID(), 
                    'name'  =>  $ShippingOption->getDescription()->getTitle('EN'), 
                    'description'  =>  $ShippingOption->getDescription()->getText('EN'),  
                    'deliveryDays'  =>  $ShippingOption->getMaxDeliveryDays(),  
                    'rateType'  =>  $ShippingOption->getRateType(), 
                    'rate'  =>  $ShippingOption->getRate(), 
                    'Zone'  =>  $zone_info, 
                    'Carrier'   =>  $carrier_info, 
                    'isTaxable'  =>  $ShippingOption->isTaxable()
                );
        }







        // OUTPUT...
        $json_reply = [
                "message"=> 'Shipping options.', 
                "ShippingOptions"=>$shipping_options_info
                ];

        

        return $json_reply;
    }






    /**
     * getCharges
     *
     * @return void
     */
    protected function getCharges () {

        $json_reply = [];
        
        $_params = $this->digestParams([
            'province_id'=>'int', 
            'country_code' => 'iso', 
            'shipping_option_id' => 'int'
            ]);

        $_Country = \Cataleya\Geo\Country::load($_params['country_code']);
        if ($_Country === NULL) throw new Error('Country could not be loaded.');

        $_Province = ($_Country->hasProvinces() && (int)$_params['province_id'] !== 0) ? \Cataleya\Geo\Province::load($_params['province_id']) : NULL;
        if ($_Country->hasProvinces() && empty($_Province)) throw new Error('Province could not be loaded.');


        $_ShippingOption = \Cataleya\Shipping\ShippingOption::load($_params['shipping_option_id']);
        if (empty($_ShippingOption)) throw new Error('Delivery option could not be loaded.');

        $cart = __('shop.cart');
        $_charges = $cart->calcShippingCharges($_ShippingOption, (!empty($_Province) ? $_Province : $_Country));
        $_Currency = $cart->getStore()->getCurrency();
        $_charges["FancyAmount"] = ('&#'.$_Currency->getUtfCode().'; ') . number_format($_charges['amount'], 2);
        $_charges["symbol"] = ('&#'.$_Currency->getUtfCode().'; ');

        //$_charges['subTotal'] = $cart->getTotal(TRUE, TRUE);
        $_charges['grandTotal'] = $_charges['amount_tax_included'] + $cart->getTotal();

        // add taxes
        $_ProductTaxes = $cart->getTaxes((!empty($_Province) ? $_Province : $_Country));
        if (!empty($_ProductTaxes)) foreach ($_ProductTaxes as $_Tax) $_charges['grandTotal'] += $_Tax['amount'];

        $_charges['grandTotal'] = number_format($_charges['grandTotal'], 2);


        // OUTPUT...
        $json_reply = [
                "message"=> 'Shipping charges.', 
                "ShippingCharges" => $_charges

                ];

        return $json_reply;
    }






    
    

    
      
      
    

        

 
}


