<?php



namespace Cataleya\Front\Shop\RPC\v1\Resource;
use \Cataleya\Error;


/*
 *
 * 	@Package: Cataleya
 * 	@Class: \Cataleya\Front\Shop\RPC\v1\Resource\Pages
 *
 * 	(c) 2014 Fancy Paper Planes
 *
 *
 *
 */

class Contact 
extends \Cataleya\Front\Shop\RPC\v1\Resource\Main 
{
    
    


    public function __construct() {

        
        
    }
    
    

    /*
     * 
     * 
     * CALLBACKS
     * 
     */






    /**
     * sendMessage
     *
     * @return void
     */
    protected function sendMessage () {

        
        $_params = $this->digestParams([
            'g_recaptcha_response'=>'longtext', 
            'fullname' => 'name', 
            'email'	=>	'email', 
            'subject'	=>	'text', 
            'body'	=>	'text'
            ], false);


        
        $json_reply = [
                 "status" => 'ok', 
                 "message" => '<p>Your request has been sent. Thank you for contacting us.</p>', 
                 "failedValidation" => false
        ];
        
        // FAILED ITEMS ARRAY 
        $_FailedItemLabels = array(	
                                'fullname'	=>	'Your Full Name', 
                                'email'	=>	'Your Email', 
                                'subject'	=>	'Subject', 
                                'captcha'	=>	'string', 
                                'body'	=>	'Enquiry and Phone Number'

                                );
        
        require_once(__path('lib').'/recaptcha/recaptchalib.php');


        if ($_params["g_recaptcha_response"]) {
                $resp = \recaptcha_check_answer (

                                    RECAPTCHA_PRIVATE_KEY,
                                    $_SERVER["REMOTE_ADDR"],
                                    $_params["g_recaptcha_response"]
                        );

        }
        
        if (in_array(false, $_params) || !$resp->success) {

            $json_reply['status'] = 'ok';
            $json_reply['message'] = '<p>Please fill out the following correctly:<br /><br />';
            $json_reply['failedValidation'] = true;

        } 


        $_i = 1;


        foreach ($_params as $k => $v) if ($v === false) $json_reply['message'] .= (($_i++) . '. ' . $_FailedItemLabels[$k] . '<br />');


        if (!$resp->success) $json_reply['message'] .= (($_i++) . '. Recaptcha<br />');



        if ($json_reply['failedValidation']) {

            $json_reply['message'] .= '</p>';
            return $json_reply;

        }

        
        

        $twig_vars = array ();


        $twig_vars['email'] = array (
            'subject'  =>  ('Inquiry by ' . $_params['firstname']), 
            'message' =>  '', 
            'Customer' => array (
                'fullname'  =>  $_params['fullname'], 
                'email' =>  $_params['email'], 
                'subject' =>  $_params['subject'], 
                'body' =>  $_params['body']
                )
        );



        $twig_vars['header_title'] = $twig_vars['inline_title'] = 'Inquiry';
        $twig_vars['title_icon'] = BASE_URL . 'ui/images/ui/notification-icons/email-50x50.png';


        $twig_vars['contact_email'] = EMAIL_INQUIRIES;



        // New email
        $message = \Cataleya\Helper\Emailer::getInstance('enquiry.email');
        $_Shop = \__('shop');


        // ****************** Send a copy of INVOICE to shop managers ************************ ///////

        $_OrderNotification = $_Shop->getNewOrderNotification();
        $_StaffEmails = [];

        foreach ($_OrderNotification as $_Alert) 
        {
            if (!$_Alert->emailEnabled()) continue;

            $_Recipient = $_Alert->getRecipient();
            $_StaffEmails[$_Recipient->getEmailAddress()] = $_Recipient->getName();
        }

        $message->Subject = 'Inquiry by ' . $_params['fullname'];
        $message->From = array('no-reply@'. EMAIL_HOST=> $_Shop->getName() );
        $message->Sender = ('no-reply@'.EMAIL_HOST);
        $message->To = [ 'info@' . EMAIL_HOST => 'Customer Care' ];
        $message->Bcc = $_StaffEmails;
        $message->ReplyTo = $_params['email'];




        // New email


        $textBody = "Inquiry by: " . $_params['fullname'] . "! \n\n";
        $textBody .= "Email: " . $_params['email'] . "\n\n";
        $textBody .= "Subject: " . $_params['subject'] . "\n\n";
        $textBody .= "Question/Comment: \n\n";
        $textBody .= "------------------------------- \n\n" . $_params['body'] . "\n\n";

        $message->TextBody = $textBody;
        $message->setHTMLBody(\__('dash.skin')->render('inquiry.email.html.twig', $twig_vars), 'text/html');
        // $message->setPriority(3);


        $error_text = "";               

        try {
            $result = $message->send();

        } catch (\Swift_SwiftException $e) {
            $result = 0;
            $error_text = $e->getCode() . ": " . $e->getMessage();  
        }






        if ($result === 0) {
             $json_reply['status'] = 'error';
             $json_reply['message'] = '<p>Your inquiry could not be sent.</p>'.$error_text;
        } 



        return $json_reply;
    }






    

    
      
      
    

        

 
}


