<?php


namespace Cataleya\Front\Shop\RPC\v1;
use Cataleya\Front\Shop\RPC\v1\Resource;
use \Cataleya\Front\Shop\Controller as MainController;


/*
 *
 * 	@Package: Cataleya
 * 	@Class: \Cataleya\Front\Shop\RPC\v1\Resource
 *
 * 	(c) 2014 Fancy Paper Planes
 *
 *
 *
 */

class Resource 
{
    
    
    

    public function __construct() {


    }


    public static function load () {


        switch (MainController::getAction()) 
        {
            
            case 'shipping':
                return Resource\Shipping::load();

            case 'geo':
                return Resource\Geo::load();
            
            case 'cart':
                return Resource\Cart::load();
                
            case 'product':
                return Resource\Product::load();
            
            case 'contact':
                return Resource\Contact::load();
            default:
                header('HTTP/1.0 404 Resource Not Found'); exit();
        }
        
    }
    

    

        

 
}


