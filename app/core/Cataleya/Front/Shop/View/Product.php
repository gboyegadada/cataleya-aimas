<?php



namespace Cataleya\Front\Shop\View;



/*
 *
 * 	@Package: Cataleya
 * 	@Class: \Cataleya\Front\Shop\View\Product
 *
 * 	(c) 2014 Fancy Paper Planes
 *
 *
 *
 */

class Product extends \Cataleya\Front\Shop\View 
{
    



    
    


    /*
     * 
     * [ dispatch ]
     * ______________________________________________________________
     * 
     * 
     * 
     */


    public function dispatch () 
    {
        
        
        $_CLEAN = array (
            'id' => \Cataleya\Front\Shop\Controller::getItemID(), 
            'cat' => \Cataleya\Front\Shop\Controller::getCategoryKeyword()
        );


        // Quick check if product ID makes any sense (in case request was made directly to product.php)...
        if ($_CLEAN['id'] === FALSE) $this->notFound();
        if ($_CLEAN['cat'] === FALSE) $this->notFound();


        if (!defined('PROD_ID')) define('PROD_ID', $_CLEAN['id']);
        if (!defined('TAG_KEYWORD')) define('TAG_KEYWORD', $_CLEAN['cat']);


        // 1. fetch product...
        $_Product = \Cataleya\Catalog\Product::load(PROD_ID);
        if ($_Product === NULL) $this->notFound();


        // FETCH TAG (CATEGORY)
        $category = \Cataleya\Catalog\Tag::load(TAG_KEYWORD);


        
        // If product is not is any category, it shouldn't be visible
        // in the storefront.
        if (empty($category)) {
            $category = \Cataleya\Front\Shop::getInstance()->getRootCategory();
            define('TAG_KEYWORD', 'all');
        }
        
        
        
        

        if (empty($category)) $this->notFound();

        
        
        // Update views
        $_Product->updateViews(\Cataleya\Front\Shop::getInstance());
        
        
        
        
        
        

        
        $twig_vars = \Cataleya\Front\Shop\Controller::getBaseTwigVars();
        $twig_vars['product'] = \Cataleya\Front\Shop\Juicer::juice(
                                $_Product, 
                                \Cataleya\Front\Shop::getInstance()
                                );
        
        $twig_vars['category'] = \Cataleya\Front\Shop\Juicer::juiceDrop($category);
        
        
        $twig_vars['cart'] = \Cataleya\Front\Shop\Juicer::juice(
                \Cataleya\Front\Shop\MyCart::getInstance(), 
                \Cataleya\Front\Shop::getInstance()
                );



        // Set redirect
        $host_pattern = '!^' . preg_replace('/[.]/', '\.', HOST) . '!';
        $twig_vars['cart']['continueHref'] = (isset($_SERVER['HTTP_REFERER']) && preg_match($host_pattern, $_SERVER['HTTP_REFERER']) === 1 && preg_match('/\/shop\/(cart|login|register|logout|confirm|resend-confirmation)/', $_SERVER['HTTP_REFERER']) === 0) ? $_SERVER['HTTP_REFERER'] : \Cataleya\Front\Shop::getInstance()->getShopLanding();




        // JSON anyone?
        if (in_array(OUTPUT, ['JSON', 'AJAX_JSON'])) {
            echo json_encode($twig_vars['product'], JSON_PRETTY_PRINT);
            exit();
        }
        
        echo __('shop.skin')->render('product.html.twig', $twig_vars);
        exit();           
        
        
        
        
        
    }
    
    

    
    
    
    

    

        

 
}


