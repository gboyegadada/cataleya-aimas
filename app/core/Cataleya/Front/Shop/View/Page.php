<?php



namespace Cataleya\Front\Shop\View;



/*
 *
 * 	@Package: Cataleya
 * 	@Class: \Cataleya\Front\Shop\View\Category
 *
 * 	(c) 2014 Fancy Paper Planes
 *
 *
 *
 */

class Page extends \Cataleya\Front\Shop\View 
{
    




    public function __construct() {

        parent::__construct();
            
 
        
    }
    
    
    
    

    
    
    

    
    
    
    
    
    
    


    /*
     * 
     * [ dispatch ]
     * ______________________________________________________________
     * 
     * 
     * 
     */


    public function dispatch () 
    {
        
        
        

        // SANITIZE GET DATA...
        $_CLEAN = array (
            'pg' => \Cataleya\Front\Shop\Controller::getPageKeyword()
        );



        if ($_CLEAN['pg'] === FALSE) { 
            
            $this->notFound();
        }
        
        $_Store = \Cataleya\Front\Shop::getInstance();
        $_Page = $_Store->getCMSPage($_CLEAN['pg']);
        
        

        // INCLUDE BASE TWIG VARIABLES (ARRAYS)

        $twig_vars = \Cataleya\Front\Shop\Controller::getBaseTwigVars();
        
        $twig_vars['page'] = \Cataleya\Front\Shop\Juicer::juice($_Page);
        
        
        $twig_vars['cart'] = \Cataleya\Front\Shop\Juicer::juice(
                \Cataleya\Front\Shop\MyCart::getInstance(), 
                \Cataleya\Front\Shop::getInstance()
                );


        // LOAD PLUGINS
        $_plugin_list = $_Page->getPluginList();
        if (!empty($_plugin_list)) {
            $twig_vars['apps'] = \Cataleya\Plugins\Action::trigger('page.get-partials', [], $_plugin_list);
        }


        echo __('shop.skin')->render('/pages/'.$_Page->getTemplateID(), $twig_vars);
        exit();

        
        
        
    }
    
    

    
    
    
    

    

        

 
}


