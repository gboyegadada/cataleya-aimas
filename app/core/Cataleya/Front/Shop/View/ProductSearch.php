<?php



namespace Cataleya\Front\Shop\View;



/*
 *
 * 	@Package: Cataleya
 * 	@Class: \Cataleya\Front\Shop\View\Category
 *
 * 	(c) 2014 Fancy Paper Planes
 *
 *
 *
 */

class ProductSearch extends \Cataleya\Front\Shop\View 
{
    



    
    
    
    
    


    /*
     * 
     * [ dispatch ]
     * ______________________________________________________________
     * 
     * 
     * 
     */


    public function dispatch () 
    {
        
        

        
        $_SearchResults = $this->getController()->getSearchResults();
        
        


        if (OUTPUT === "JSON"):



                // Output...
                $json_data = array (
                                'status' => 'Ok', 
                                'message' => '', 
                                'Products' => array (), 
                                'Store' => \Cataleya\Front\Shop\Juicer::juiceDrop(\Cataleya\Front\Shop::getInstance()), 
                                'count' => 0
                                );
        

                foreach ($_SearchResults as $Product) 
                {

                    if (!$Product->isDisplayed()) continue; 

                    $json_data['count']++;

                    $json_data['Products'][] = \Cataleya\Front\Shop\Juicer::juiceDrop($Product);


                }

                $json_data['message'] = \Cataleya\Helper::countInEnglish($json_data['count'], 'Product', 'Products') . ' found.';
                
                echo json_encode($json_data);
                exit();


        else:

                // INCLUDE BASE TWIG VARIABLES (ARRAYS)
                $twig_vars = \Cataleya\Front\Shop\Controller::getBaseTwigVars();
        
                $twig_vars['cart'] = \Cataleya\Front\Shop\Juicer::juice(
                        \Cataleya\Front\Shop\MyCart::getInstance(), 
                        \Cataleya\Front\Shop::getInstance()
                        );




                // PRODUCTS
                $twig_vars['products'] = array ();

                foreach ($_SearchResults as $_Product) 
                {
                    if (!$_Product->isDisplayed()) continue;

                    $twig_vars['products'][$_Product->getID()] = \Cataleya\Front\Shop\Juicer::juiceDrop($_Product);
                }


                echo __('shop.skin')->render('search-results.html.twig', $twig_vars);
                exit();


        endif;

        
        
        
    }
    
    

    
    
    
    

    

        

 
}


