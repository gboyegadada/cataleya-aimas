<?php



namespace Cataleya\Front\Shop\View;



/*
 *
 * 	@Package: Cataleya
 * 	@Class: \Cataleya\Front\Shop\View\Register
 *
 * 	(c) 2014 Fancy Paper Planes
 *
 *
 *
 */

class Register extends \Cataleya\Front\Shop\View 
{
    
    


    protected 
            $_username = '';




    
    
    

    


    /*
     * 
     * [ dispatch ]
     * ______________________________________________________________
     * 
     * 
     * 
     * 
     * 
     */


    public function dispatch () 
    {




        $twig_vars = \Cataleya\Front\Shop\Controller::getBaseTwigVars();
        $twig_vars['cart'] = \Cataleya\Front\Shop\Juicer::juice(
                \Cataleya\Front\Shop\MyCart::getInstance(), 
                \Cataleya\Front\Shop::getInstance()
                );

        $_FORM = \Cataleya\Front\Form::load(INPUT_POST, \Cataleya\Front\Form::FROM_CACHE);
        $_autofill = $_errors = array();
        
        foreach ($_FORM as $_Param) {
            $_autofill[$_Param->getName()] = $_Param->getAutofill();
            $_errors[$_Param->getName()] = (!$_Param->isHealthy() && $_Param->isRequired()) ? $_Param->getErrorText() : '';

        }
        
        

        if (!defined ('NEW_REG_TOKEN')) define ('NEW_REG_TOKEN', \Cataleya\Helper::generateToken(''));
        $_SESSION[SESSION_PREFIX.'FORM_TOKEN'] = NEW_REG_TOKEN; // SECURITY TOKEN...

        
        $twig_vars['register'] = array (
            'autofill'  =>  $_autofill,
            'token' =>  NEW_REG_TOKEN
        );
        
        $twig_vars['error'] = $_errors;
        

        echo __('shop.skin')->render('register.html.twig', $twig_vars);
        exit(); 


        
    }
    
    
    
    
    

    
      
    

        

 
}


