<?php



namespace Cataleya\Front\Shop\View;



/*
 *
 * 	@Package: Cataleya
 * 	@Class: \Cataleya\Front\Shop\View\Login
 *
 * 	(c) 2014 Fancy Paper Planes
 *
 *
 *
 */

class Login extends \Cataleya\Front\Shop\View 
{
    
    


    protected 
            $_username = '';




    

    


    /*
     * 
     * [ dispatch ]
     * ______________________________________________________________
     * 
     * 
     * 
     * 
     * 
     */


    public function dispatch () 
    {


        // If user is already logged in

        if (defined('IS_LOGGED_IN') && IS_LOGGED_IN === TRUE) 
        {
            $host_pattern = '!^' . preg_replace('/[.]/', '\.', HOST) . '!';

            // Redirect browser...
            $referrer = (isset($_SESSION[SESSION_PREFIX.'REFERRER'])) ? $_SESSION[SESSION_PREFIX.'REFERRER'] : \Cataleya\Front\Shop::getInstance()->getShopLanding();
            redirect($referrer);
            exit('<center><p>Please wait...</p></center>');
        }

        else if (!defined('IS_LOGGED_IN')) {
            define('IS_LOGGED_IN', FALSE);
        }





        if (!defined ('NEW_FORM_TOKEN')) define ('NEW_FORM_TOKEN', \Cataleya\Helper::generateToken(''));
        $_SESSION[SESSION_PREFIX.'FORM_TOKEN'] = NEW_FORM_TOKEN; // SECURITY TOKEN...


        $twig_vars = \Cataleya\Front\Shop\Controller::getBaseTwigVars();
        $twig_vars['cart'] = \Cataleya\Front\Shop\Juicer::juice(
                \Cataleya\Front\Shop\MyCart::getInstance(), 
                \Cataleya\Front\Shop::getInstance()
                );
        $_FORM = \Cataleya\Front\Form::load(INPUT_POST, \Cataleya\Front\Form::FROM_INPUT, [
            'username*' => 'email' 
        ]);
        $_Param_Username = $_FORM->getParam('username');

        $twig_vars['login'] = array (
            'response'  =>  $this->getResponse(), 
            'errors'  =>  $this->getErrors(), 
            'user'  =>  $_Param_Username->isHealthy() ? $_Param_Username->getValue() : "", 
            'token' =>  NEW_FORM_TOKEN
        );

        echo __('shop.skin')->render('login.html.twig', $twig_vars);
        exit(); 


        
    }
    
    
    
    
    

    
      
    

        

 
}


