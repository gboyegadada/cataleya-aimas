<?php



namespace Cataleya\Front\Shop\View\Checkout;



/*
 *
 * 	@Package: Cataleya
 * 	@Class: \Cataleya\Front\Shop\View\Checkout\Status
 *
 * 	(c) 2014 Fancy Paper Planes
 *
 *
 *
 */

class Status extends \Cataleya\Front\Shop\View 
{
    


    
    


    /*
     * 
     * [ dispatch ]
     * ______________________________________________________________
     * 
     * 
     * 
     */


    public function dispatch () 
    {
        
        $_response = $this->getController()->getTransactionResponse();
        
        $_transaction_successful = FALSE;
        
        if (in_array($_response['status'], array('paid', 'pending', 'cash on delivery', 'bank transfer'))) 
        {
            
            $_transaction_successful = TRUE;

            // CLEAR CART
            \Cataleya\Front\Shop\MyCart::delete();

        } else {

            
            $_transaction_successful = FALSE;


        }
        
        
        
        
        
        $twig_vars = \Cataleya\Front\Shop\Controller::getBaseTwigVars();
        $twig_vars['cart'] = \Cataleya\Front\Shop\Juicer::juice(
                                \Cataleya\Front\Shop\MyCart::getInstance(), 
                                \Cataleya\Front\Shop::getInstance()
                                );




        $twig_vars['cart']['error'] = (!empty($this->_errors)) ? TRUE: FALSE;
        $twig_vars['cart']['errorMessages'] = $this->_errors;
        
        
        $twig_vars['tranx_successful'] = $_transaction_successful;
        $twig_vars['payment_response'] = $_response;
        
        
        


        // JSON anyone?
        if (OUTPUT === 'JSON') {
            echo json_encode($twig_vars['cart'], JSON_PRETTY_PRINT);
            exit();
        }
        

        echo __('shop.skin')->render('status.tranx.html.twig', $twig_vars);
        exit();           
        
        
        
    }
    
    

    
    
    
    

    

        

 
}


