<?php



namespace Cataleya\Front\Shop\View\Checkout;



/*
 *
 * 	@Package: Cataleya
 * 	@Class: \Cataleya\Front\Shop\View\Confirm\Email
 *
 * 	(c) 2014 Fancy Paper Planes
 *
 *
 *
 */

class Email extends \Cataleya\Front\Shop\View 
{
    
    


    protected 
            $_username = '';






    
    


    
    
    

    


    /*
     * 
     * [ dispatch ]
     * ______________________________________________________________
     * 
     * 
     * 
     * 
     * 
     */


    public function dispatch () 
    {





        // SEND CONFIRM EMAIL
        $twig_vars = \Cataleya\Front\Shop\Controller::getBaseTwigVars();


        $_Customer = \Cataleya\Front\Shop\Customer::getInstance();
        $_response = $this->getController()->getTransactionResponse();
        
        $_Order = $this->getController()->getOrder();
        $twig_vars['Order'] = \Cataleya\Front\Shop\Juicer::juice($_Order);


        $twig_vars['email'] = array (
            'subject'  =>  '', 
            'message' =>  '', 
            'firstname' => $_Customer->getFirstname(), 
            'lastname'  =>  $_Customer->getLastname(), 
            'fullname'  =>  $_Customer->getName(), 
            'email' =>  $_Customer->getEmailAddress()
        );
        
        $twig_vars['payment_instructions'] = \Cataleya\Plugins\Action::trigger(
                    'payment.get-instructions', [ ],  
                    [ $_Order->getPaymentTypeID() ]
                );




        $twig_vars['header_title'] = 'Order Invoice: #' . $_Order->getOrderNumber();
        $twig_vars['inline_title'] = 'Order Invoice';
        $twig_vars['title_icon'] = BASE_URL . 'ui/images/ui/notification-icons/basket-50x50.png';



        
        $textBody = "Order Invoice (#" . $_Order->getOrderNumber() . "): if you are reading this, your email client is unable to display HTML content. Please check using any modern desktop web browser.";




        // ****************** Send a copy of INVOICE to shop managers ************************ ///////

        $_OrderNotification = \Cataleya\Front\Shop::getInstance()->getNewOrderNotification();
        $_StaffEmails = array ();

        foreach ($_OrderNotification as $_Alert) 
        {
            if (!$_Alert->emailEnabled()) continue;

            $_Recipient = $_Alert->getRecipient();
            $_StaffEmails[$_Recipient->getEmailAddress()] = $_Recipient->getName();
        }


        // New email
        $email = \Cataleya\Helper\Emailer::getInstance('order.invoice.email');

        // Email setup
        $email->To = array( $_Customer->getEmailAddress() => $_Customer->getName() );
        $email->Bcc = $_StaffEmails;
        $email->From = array('no-reply@'. EMAIL_HOST=> $twig_vars['store']['title'] );
        $email->Sender = 'no-reply@'. EMAIL_HOST;
        $email->ReplyTo = 'no-reply@' . EMAIL_HOST;
        $email->Subject = 'Order Invoice: #' . $_Order->getOrderNumber();
        $email->TextBody = $textBody;
        $email->Priority = 3;

        $email->setHTMLBody(__('shop.skin')->render('order-invoice.email.html.twig', $twig_vars));


        // Send email
        $email->send();



        return;
        
        
    }
    
    
    
    
    

      
    

        

 
}


