<?php



namespace Cataleya\Front\Shop\View;



/*
 *
 * 	@Package: Cataleya
 * 	@Class: \Cataleya\Front\Shop\View\Category
 *
 * 	(c) 2014 Fancy Paper Planes
 *
 *
 *
 */

class Asset extends \Cataleya\Front\Shop\View 
{
    




    public function __construct($_Controller) {

       parent::__construct($_Controller);
            
 
        
       //if (strtolower(OUTPUT) !== $_Controller->getControllerHandle()) {
       //    $this->notFound();
       //}
        
    }
    
    
    
    

    
    
    

    
    
    
    
    
    
    


    /*
     * 
     * [ dispatch ]
     * ______________________________________________________________
     * 
     * 
     * 
     */


    public function dispatch () 
    {
        
        
        
        
        // JSON anyone?
        if (OUTPUT === 'JSON') {
            
            $twig_vars = array (
                'key' => $this->getController()->getKey(), 
                'blob' => $this->getController()->getBlob()
            );
            
            echo json_encode($twig_vars, JSON_PRETTY_PRINT);
            exit();
        }
        


        echo $this->getController()->getBlob();
        exit();

        
        
        
    }
    
    

    
    
    
    

    

        

 
}


