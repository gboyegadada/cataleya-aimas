<?php



namespace Cataleya\Front\Shop\View;



/*
 *
 * 	@Package: Cataleya
 * 	@Class: \Cataleya\Front\Shop\View\MyAccount
 *
 * 	(c) 2014 Fancy Paper Planes
 *
 *
 *
 */




class MyAccount extends \Cataleya\Front\Shop\View 
{
    

    
    
    
    


    /*
     * 
     * [ dispatch ]
     * ______________________________________________________________
     * 
     * 
     * 
     */


    public function dispatch () 
    {
        
        
        




        // INCLUDE BASE TWIG VARIABLES (ARRAYS)

        $twig_vars = \Cataleya\Front\Shop\Controller::getBaseTwigVars();
        
        
        $twig_vars['cart'] = \Cataleya\Front\Shop\Juicer::juice(
                \Cataleya\Front\Shop\MyCart::getInstance(), 
                \Cataleya\Front\Shop::getInstance()
                );




        // Load order history

        $_Orders = \Cataleya\Store\Orders::load(NULL, $this->getController()->getCustomer(), array('order_by'=>\Cataleya\Collection::ORDER_BY_CREATED, 'order'=>\Cataleya\Collection::ORDER_DESC, 'index'=>'all', 'status'=>'all'), 20, 1);

        $twig_vars['Orders'] = array ();
        foreach ($_Orders as $_Order) $twig_vars['Orders'][] = \Cataleya\Front\Shop\Juicer::juice($_Order);


        echo __('shop.skin')->render('view.myaccount.html.twig', $twig_vars);
        exit();

        
        
        
    }
    
    

    
    
    
    

    

        

 
}


