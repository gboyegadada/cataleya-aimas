<?php



namespace Cataleya\Front\Shop\View;



/*
 *
 * 	@Package: Cataleya
 * 	@Class: \Cataleya\Front\Shop\View\Category
 *
 * 	(c) 2014 Fancy Paper Planes
 *
 *
 *
 */

class Category extends \Cataleya\Front\Shop\View 
{
    



    
    
    
    
    


    /*
     * 
     * [ dispatch ]
     * ______________________________________________________________
     * 
     * 
     * 
     */


    public function dispatch () 
    {
        
        
        



        // SANITIZE GET DATA...
        $_CLEAN = filter_input_array(INPUT_GET, 
                        array(	
                                         // Page number...
                                        'pg' => array('filter'=>FILTER_VALIDATE_INT, 
                                                    'options'	=>	array('min_range' => -1, 'max_range' => 1000)
                                                    ), 
                                        // Page size...
                                        'pgsize' =>array('filter' =>	FILTER_VALIDATE_INT, 
                                                                    'options'	=>	array('min_range' => -1, 'max_range' => 1000)
                                                                    ),  
                                        // Page size...
                                        'pg_num' =>array('filter' =>	FILTER_VALIDATE_INT, 
                                                                    'options'	=>	array('min_range' => 1, 'max_range' => 1000)
                                                                    ),    
                                        // Offset (product ID)...
                                       'os'	=>	array('filter'		=>	FILTER_VALIDATE_INT, 
                                                                                       ),                                                                   


                                         // order 						
                                        'o' =>	array('filter'		=>	FILTER_VALIDATE_INT, 
                                                                                        'options'	=>	array('min_range' => 0, 'max_range' => 20)
                                                                                        ), 
                                        // order by 						
                                        'ob' =>	array('filter'		=>	FILTER_VALIDATE_INT, 
                                                                                        'options'	=>	array('min_range' => 0, 'max_range' => 20)
                                                                                        )                                                                    

                                        )
                        );
        
        
        $_CLEAN['cat'] = \Cataleya\Front\Shop\Controller::getCategoryKeyword();


        // Deafault page size...
        define ('DEFAULT_PAGE_SIZE', 15);


        // Check if INPUT keys are ALL expected AND MADE IT THROUGH...

        $_CLEAN['pg'] = ($_CLEAN['pg'] === FALSE || $_CLEAN['pg'] === NULL) ? 1 : (int)$_CLEAN['pg'];
        $_CLEAN['pg_num'] = ($_CLEAN['pg_num'] === FALSE || $_CLEAN['pg_num'] === NULL) ? 0 : (int)$_CLEAN['pg_num'];
        $_CLEAN['pgsize'] = ($_CLEAN['pgsize'] === FALSE || $_CLEAN['pgsize'] === NULL) ? DEFAULT_PAGE_SIZE : (int)$_CLEAN['pgsize'];
        $_CLEAN['cat'] = ($_CLEAN['cat'] === FALSE || $_CLEAN['cat'] === NULL) ? 'all' : $_CLEAN['cat'];
        $_CLEAN['os'] = empty($_CLEAN['os']) ? 0 : (int)$_CLEAN['os'];
        $_CLEAN['o'] = ($_CLEAN['o'] === FALSE || $_CLEAN['o'] === NULL) ? \Cataleya\Catalog\Tag::ORDER_ASC : $_CLEAN['o'];
        $_CLEAN['ob'] = ($_CLEAN['ob'] === FALSE || $_CLEAN['ob'] === NULL) ? \Cataleya\Catalog\Tag::ORDER_BY_CREATED : $_CLEAN['ob'];

        define('TAG_KEYWORD', strtolower($_CLEAN['cat']));

        define ('NO_OF_PAGE_INDEX_ANCHORS', 3);

        define('ORDER_BY', $_CLEAN['ob']);
        define('ORDER', $_CLEAN['o']);
        define('PAGE_SIZE', $_CLEAN['pgsize']);
        define('PAGE_NUM', (($_CLEAN['pg']<0) ? $_CLEAN['pg_num'] : $_CLEAN['pg_num']+$_CLEAN['pg']));


        /////////////////////// GET TAG IF IT IS SPECIFIED /////////////////

        if (TAG_KEYWORD != 'all') {

            $category = \Cataleya\Catalog\Tag::load(TAG_KEYWORD, array(\Cataleya\Front\Shop::getInstance()), array('order'=>ORDER, 'order_by'=>ORDER_BY), $_CLEAN['pgsize'], $_CLEAN['os'], $_CLEAN['pg']);
            if ($category === NULL) 
            {
                    $this->notFound();	
            }




        } 

        else if (TAG_KEYWORD === 'all') {


        // SELECT PRODUCTS...

        $this->notFound();			






        } 

        /////////////////////////  IF NO VALID TAG KEYWORD WAS PROVIDED (i.e. it didn't even get past the INPUT_TAG) //////
        else {
                                // Redirect to store landing...
                                $this->notFound();	
                        }



        // INCLUDE BASE TWIG VARIABLES (ARRAYS)

        $twig_vars = \Cataleya\Front\Shop\Controller::getBaseTwigVars();
        
        
        $twig_vars['cart'] = \Cataleya\Front\Shop\Juicer::juice(
                \Cataleya\Front\Shop\MyCart::getInstance(), 
                \Cataleya\Front\Shop::getInstance()
                );



        // Tag (category) info
        $twig_vars['category'] = \Cataleya\Front\Shop\Juicer::juice($category, \Cataleya\Front\Shop::getInstance());


        // JSON anyone?
        if (OUTPUT === 'JSON') {
            echo json_encode($twig_vars['category'], JSON_PRETTY_PRINT);
            exit();
        }
        
        
        // META
        $twig_vars['meta']['keywords'] .= ', ' . $twig_vars['category']['title'];
        $twig_vars['meta']['description'] = $twig_vars['category']['description'];




        // PAGE INFO
        $twig_vars['page'] = array ();

        // SET PAGE SIZE 
        $twig_vars['page']['size'] = (PAGE_SIZE > 0) ? PAGE_SIZE : $category->getPopulation();



        // GET PAGE COUNT (also redirect to shop 'home' if specified page num is invalid)
        $twig_vars['page']['count'] = ceil($category->getPopulation() / PAGE_SIZE);
        $twig_vars['page']['number'] = (PAGE_NUM > $twig_vars['page']['count']) ? $twig_vars['page']['count'] : PAGE_NUM; 


        // OTHER META DATA...
        $twig_vars['page']['start'] = $category->getPageStart();
        $twig_vars['page']['stop'] = $category->getPageStop();



        define ('NEXT_OFFSET', (int)$category->getPageOffset());
        define ('OFFSET', $_CLEAN['os']);

        // PREVIOUS ANCHOR + INFO
        
        $_prev_page_num = $_next_page_num = 0;
        $_next_page_num = ((PAGE_NUM+1) > $twig_vars['page']['count']) ? PAGE_NUM : PAGE_NUM+1; 
        
        if ($_CLEAN['pg'] < 0) {
            $_prev_page_num = ((PAGE_NUM+$_CLEAN['pg']) < 1) ? PAGE_NUM : PAGE_NUM+$_CLEAN['pg'];  
        } else {
            $_prev_page_num = $_CLEAN['pg']; 
            
        }
        

        // Href
        $href = array (
            'o' =>  ORDER, 
            'ob'    =>  ORDER_BY, 
            'pg'    =>  -1, //$twig_vars['page']['previous']['number'], 
            'os'    => OFFSET, 
            'pgsize'    =>  PAGE_SIZE, 
            'pg_num'    =>  $_prev_page_num
        );

        $twig_vars['page']['previous']['href'] = $twig_vars['category']['href'] . '?' . http_build_query($href);
        $twig_vars['page']['previous']['number'] = $_prev_page_num; 




        // NEXT ANCHOR + INFO
        
        // Href
        $href['pg'] = 1; //$twig_vars['page']['next']['number'];
        $href['os'] = NEXT_OFFSET;
        $href['pg_num'] = $twig_vars['page']['next']['number'];

        $twig_vars['page']['next']['href'] = $twig_vars['category']['href'] . '?' . http_build_query($href);
        $twig_vars['page']['next']['number'] = $_next_page_num; 



        echo __('shop.skin')->render('category.html.twig', $twig_vars);
        exit();

        
        
        
    }
    
    

    
    
    
    

    

        

 
}


