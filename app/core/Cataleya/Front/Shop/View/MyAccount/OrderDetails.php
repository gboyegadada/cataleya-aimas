<?php



namespace Cataleya\Front\Shop\View\MyAccount;



/*
 *
 * 	@Package: Cataleya
 * 	@Class: \Cataleya\Front\Shop\View\MyAccount
 *
 * 	(c) 2014 Fancy Paper Planes
 *
 *
 *
 */




class OrderDetails extends \Cataleya\Front\Shop\View 
{
    

    
    
    
    


    /*
     * 
     * [ dispatch ]
     * ______________________________________________________________
     * 
     * 
     * 
     */


    public function dispatch () 
    {
        
        

    // SANITIZE POST DATA...
    $_CLEAN = filter_input_array(INPUT_GET, 
                            array(

                                             // id...
                                            'id'	=>	array('filter'		=>	FILTER_VALIDATE_INT, 
                                                                                            'options'	=>	array('min_range' => 1, 'max_range' => 999999)
                                                                                            ), 
                                             // [f] for file?...
                                            'f'	=>	array('filter'		=>	FILTER_VALIDATE_INT, 
                                                                                            'options'	=>	array('min_range' => 0, 'max_range' => 2)
                                                                                            )
                                            )
                            );


        // INCLUDE BASE TWIG VARIABLES (ARRAYS)

        $twig_vars = \Cataleya\Front\Shop\Controller::getBaseTwigVars();
        
        
        $twig_vars['cart'] = \Cataleya\Front\Shop\Juicer::juice(
                \Cataleya\Front\Shop\MyCart::getInstance(), 
                \Cataleya\Front\Shop::getInstance()
                );



        $_id = $_CLEAN['id'];
        $_as_download = $_CLEAN['f'];

        $_Order = ($_id !== FALSE) ? \Cataleya\Store\Order::load ($_id) : NULL;

        if (OUTPUT === 'PDF'):

            // OUTPUT PDF 
            if (!empty($_Order)) { 
                \Cataleya\Helper::echoPDF($_Order->makePDF(), ($_as_download ? TRUE : FALSE)); 
                exit(); 

                }
            else {
                $twig_vars['Order'] = array (
                    'Error' => 'Order not found.'
                );
            }
        else:

            (!empty($_Order)) 
            ? $twig_vars['Order'] = \Cataleya\Front\Shop\Juicer::juice($_Order) 
            : $twig_vars['Order'] = array (
                'Error' => 'Order not found.'
            );

        endif;
        
        echo __('shop.skin')->render('order-details.html.twig', $twig_vars);
        exit();

        
        
        
    }
    
    

    
    
    
    

    

        

 
}


