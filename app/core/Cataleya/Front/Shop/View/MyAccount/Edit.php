<?php



namespace Cataleya\Front\Shop\View\MyAccount;



/*
 *
 * 	@Package: Cataleya
 * 	@Class: \Cataleya\Front\Shop\View\MyAccount\Edit
 *
 * 	(c) 2014 Fancy Paper Planes
 *
 *
 *
 */




class Edit extends \Cataleya\Front\Shop\View 
{
    

    



    /*
     * 
     * [ dispatch ]
     * ______________________________________________________________
     * 
     * 
     * 
     */


    public function dispatch () 
    {
        
        
        

	define ('NEW_REG_TOKEN', \Cataleya\Helper::generateToken(''));
	$_SESSION[SESSION_PREFIX.'FORM_TOKEN'] = NEW_REG_TOKEN; // SECURITY TOKEN...

        $_FORM = \Cataleya\Front\Form::load(INPUT_POST, \Cataleya\Front\Form::FROM_CACHE);
        $_autofill = $_errors = array();
        
        /*
        foreach ($_FORM as $_Param) {
            $_autofill[$_Param->getName()] = $_Param->getAutofill();
            $_errors[$_Param->getName()] = '<span class="validator-message">' . $_Param->getErrorText() . '</span>'; // ($_Param->isRequired()) ? $_Param->getErrorText() : '';

        }
        */
        

        // INCLUDE BASE TWIG VARIABLES (ARRAYS)

        $twig_vars = \Cataleya\Front\Shop\Controller::getBaseTwigVars();
        
        
        $twig_vars['cart'] = \Cataleya\Front\Shop\Juicer::juice(
                \Cataleya\Front\Shop\MyCart::getInstance(), 
                \Cataleya\Front\Shop::getInstance()
                );


        
        
        
        $twig_vars['update'] = array (
            'autofill'  =>  $_autofill, 
            'token' =>  NEW_REG_TOKEN
        );
        
        $twig_vars['errors'] = [];
        foreach ($this->getController()->getFailedItems() as $k=>$v) {
            $twig_vars['errors'][$k] = '<span class="validator-message">' . $v . '</span>';
        }
        

        // LOAD TEMPLATE
        $template = \Cataleya\Front\Twiggy::loadTemplate('edit.myaccount.html.twig');
        echo __('shop.skin')->render('edit.myaccount.html.twig', $twig_vars);
        exit();

        
        
        
    }
    
    

    
    
    
    

    

        

 
}


