<?php



namespace Cataleya\Front\Shop\View\MyAccount;



/*
 *
 * 	@Package: Cataleya
 * 	@Class: \Cataleya\Front\Shop\View\MyAccount\Saved
 *
 * 	(c) 2014 Fancy Paper Planes
 *
 *
 *
 */




class Saved extends \Cataleya\Front\Shop\View 
{
    

    

    
    
    
    
    
    
    


    /*
     * 
     * [ dispatch ]
     * ______________________________________________________________
     * 
     * 
     * 
     */


    public function dispatch () 
    {
        
        
        


        $_Customer = $this->getController()->getCustomer();


        // INCLUDE BASE TWIG VARIABLES (ARRAYS)

        $twig_vars = \Cataleya\Front\Shop\Controller::getBaseTwigVars();
        
        
        $twig_vars['cart'] = \Cataleya\Front\Shop\Juicer::juice(
                \Cataleya\Front\Shop\MyCart::getInstance(), 
                \Cataleya\Front\Shop::getInstance()
                );




        // Load order history

        $_Orders = \Cataleya\Store\Orders::load(NULL, $_Customer, array('order_by'=>\Cataleya\Collection::ORDER_BY_CREATED, 'order'=>\Cataleya\Collection::ORDER_DESC, 'index'=>'all', 'status'=>'all'), 20, 1);

        $twig_vars['Orders'] = array ();
        foreach ($_Orders as $_Order) $twig_vars['Orders'][] = juiceOrder($_Order);


        echo __('shop.skin')->render('show.myaccount.html.twig', $twig_vars);
        exit();

        
        
        
    }
    
    

    
    
    
    

    

        

 
}


