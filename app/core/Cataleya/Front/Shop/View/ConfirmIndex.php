<?php



namespace Cataleya\Front\Shop\View;



/*
 *
 * 	@Package: Cataleya
 * 	@Class: \Cataleya\Front\Shop\View\ConfirmIndex
 *
 * 	(c) 2014 Fancy Paper Planes
 *
 *
 *
 */

class ConfirmIndex extends \Cataleya\Front\Shop\View 
{
    
    


    protected 
            $_username = '';





    

    


    /*
     * 
     * [ dispatch ]
     * ______________________________________________________________
     * 
     * 
     * 
     * 
     * 
     */


    public function dispatch () 
    {


        /*
         * 
         * CONFIRM TOKEN REQUIRED IF REQUEST VIA POST
         * 
         */

        if (isset($_SESSION[SESSION_PREFIX.'FORM_TOKEN'])) define ('CONFIRM_TOKEN', $_SESSION[SESSION_PREFIX.'FORM_TOKEN']);
        else if ($_SERVER['REQUEST_METHOD'] == 'GET') define ('CONFIRM_TOKEN', '');
        else \Cataleya\Front\Shop\Controller::requireLogin();


        
        if (isset($_SESSION[SESSION_PREFIX.'CONFIRM_DIGEST'])) $_Customer = \Cataleya\Customer::load($_SESSION[SESSION_PREFIX.'CONFIRM_DIGEST']);
        else \Cataleya\Front\Shop\Controller::requireLogin();

        // Check if customer var is set
        if (!isset($_Customer)) \Cataleya\Helper\ErrorHandler::getInstance()->triggerException('Error in script (' . __FILE__ . '): [ Customer not found ] on line ' . __LINE__);

        // STORE CONFIRM DIGEST IN SESSION FOR DIRECT CODE ENTRY
        $_SESSION[SESSION_PREFIX.'CONFIRM_DIGEST'] = $_Customer->getConfirmDigest();

        // SET A CONFIRM TOKEN (for confirm form)
        $_SESSION[SESSION_PREFIX.'FORM_TOKEN'] = \Cataleya\Helper::generateToken('');


        $twig_vars = \Cataleya\Front\Shop\Controller::getBaseTwigVars();
        $twig_vars['cart'] = \Cataleya\Front\Shop\Juicer::juice(
                \Cataleya\Front\Shop\MyCart::getInstance(), 
                \Cataleya\Front\Shop::getInstance()
                );
        
        $twig_vars['customer'] = \Cataleya\Front\Shop\Juicer::juice($_Customer);
        $twig_vars['form_token'] = $_SESSION[SESSION_PREFIX.'FORM_TOKEN'];
        


        // Log customer out
        $_Customer->logout();
        

        echo __('shop.skin')->render('confirm.html.twig', $twig_vars);
        exit();    

        
    }
    
    
    
    
    

      
    

        

 
}


