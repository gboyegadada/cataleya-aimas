<?php



namespace Cataleya\Front\Shop\View;



/*
 *
 * 	@Package: Cataleya
 * 	@Class: \Cataleya\Front\Shop\View\StoreSelector
 *
 * 	(c) 2014 Fancy Paper Planes
 *
 *
 *
 */

class StoreSelector extends \Cataleya\Front\Shop\View 
{
    



    
    
    
    
    
    
    


    /*
     * 
     * [ dispatch ]
     * ______________________________________________________________
     * 
     * 
     * 
     */


    public function dispatch () 
    {
        
        
        




        // TWIG VARIABLES ARRAY
        $twig_vars = \Cataleya\Front\Shop\Controller::getBaseTwigVars();
                
        /*        
                array (
            'title' =>  'Aimas | Accessories for living.', 
            'base_url'=>BASE_URL, 
            'assets_path' => TEMPLATES_WEB_PATH.'/assets/',
            'shop_landing'=>SHOP_ROOT, 
            'error' =>  FALSE, 
            'errorText' =>  array (), 
            'authToken' => defined('AUTH_TOKEN') ? AUTH_TOKEN : \Cataleya\Helper::generateToken()
        );
        */


        // LOAD STORES
        

        $Stores = \Cataleya\Stores::load();
        $twig_vars['stores'] = array();

        foreach ($Stores as $Store) 
        {

        if (!$Store->isActive()) continue;

        // Store info
        $twig_vars['stores'][] = \Cataleya\Front\Shop\Juicer::juice($Store);


        }



        // Meta

        $twig_vars['meta'] = array (
            'keywords'  =>  '', //'aimas, aimas room, aimas online store', 
            'description'   =>  '', // 'Accessories for living.', 
            'copyright' =>  '' // 'Aimas'
        );



        echo __('shop.skin')->render('store-selector.html.twig', $twig_vars);
        exit();


        
        
        
    }
    
    

    
    
    
    

    

        

 
}


