<?php



namespace Cataleya\Front\Shop\View;



/*
 *
 * 	@Package: Cataleya
 * 	@Class: \Cataleya\Front\Shop\View\Checkout
 *
 * 	(c) 2014 Fancy Paper Planes
 *
 *
 *
 */

class Checkout extends \Cataleya\Front\Shop\View 
{
    



    
   

    
    


    /*
     * 
     * [ dispatch ]
     * ______________________________________________________________
     * 
     * 
     * 
     */


    public function dispatch () 
    {
        
        
        
        //$_Customer = \Cataleya\Customer::load($_SESSION[SESSION_PREFIX.'CUSTOMER_ID']);
        // if (empty($_Customer)) \Cataleya\Front\Shop\Controller::requireLogin ();

        
        $_Cart = \Cataleya\Front\Shop\MyCart::getInstance();
        $_Store = \Cataleya\Front\Shop::getInstance();
        $_Currency = $_Store->getCurrency();
        


        if (!defined ('NEW_FORM_TOKEN')) define ('NEW_FORM_TOKEN', \Cataleya\Helper::generateToken(''));
        $_SESSION[SESSION_PREFIX.'FORM_TOKEN'] = NEW_FORM_TOKEN; // SECURITY TOKEN...

        
        
        
        
        $twig_vars = \Cataleya\Front\Shop\Controller::getBaseTwigVars();
        $twig_vars['cart'] = \Cataleya\Front\Shop\Juicer::juice($_Cart, $_Store);

        $twig_vars['checkout'] = array (
            'token' => NEW_FORM_TOKEN
        );
        
        $twig_vars['token'] = NEW_FORM_TOKEN;

        $_errors = $this->getController()->getErrors();
        
        $twig_vars['cart']['error'] = (!empty($_errors)) ? TRUE: FALSE;
        $twig_vars['cart']['errorMessages'] = $_errors;
        $twig_vars['CheckoutNotices'] = $this->getController()->getNotices();
        $twig_vars['FailedItems'] = $this->getController()->getFailedItems();
        
        
        


        // Load countries
        $twig_vars['countries'] = array ();

        $_Countries = \Cataleya\Geo\Countries::load();

        foreach ($_Countries as $_Country) 
        {
            $twig_vars['countries'][] = \Cataleya\Front\Shop\Juicer::juice($_Country);
        }
        
        
        


        // Load form cache
        // $_FORM = \Cataleya\Front\Form::load(INPUT_POST, \Cataleya\Front\Form::FROM_CACHE);
        // $twig_vars['FormCache'] = $_FORM->getParams(\Cataleya\Front\Form::PARAM_ARRAY);
        
        
        


        // 1. SHIPPING ADDRESS
        
        
        
        $_ShippingAddress = $_Cart->getShippingAddress();
        $_Customer = \Cataleya\Front\Shop\Customer::load();
        
        $_fname = $_ShippingAddress->getEntryFirstname();
        $_lname = $_ShippingAddress->getEntryLastname();
        
        $twig_vars['FormCache'] = array(
            'shipping_firstname' => (!empty($_fname)) ? $_fname : $_Customer->getFirstname(), 
            'shipping_lastname' => (!empty($_lname)) ? $_lname : $_Customer->getLastname(),
            'shipping_address_1' => $_ShippingAddress->getEntryStreetAddress(), 
            'shipping_address_2' => $_ShippingAddress->getEntryStreetAddress2(), 
            'shipping_city' => $_ShippingAddress->getEntryCity(), 
            'shipping_postal_code' => $_ShippingAddress->getEntryPostcode()
        );

        
        
        $_ShippingCountry = $_ShippingAddress->getCountry();
        $_ShippingOption = $_Cart->getShippingOption();
        $twig_vars['FormCache']['shipping_option_id'] = (!empty($_ShippingOption)) ? $_ShippingOption->getID() : 0;

        // SELECTED SHIPPING COUNTRY

        $twig_vars['FormCache']['shipping_country'] = array (
                                    'iso2'  =>  $_ShippingCountry->getIso2(), 
                                    'iso3'  =>  $_ShippingCountry->getIso3(), 
                                    'printableName' =>  $_ShippingCountry->getPrintableName(),
                                    'hasProvinces' =>  $_ShippingCountry->hasProvinces(), 
                                    'provinces' =>  array ()
                                );



        if ($_ShippingCountry->hasProvinces()):
            
            $_ShippingProvince = $_ShippingAddress->getProvince();
            foreach ($_ShippingCountry as $_Province) 
            {
                $twig_vars['FormCache']['shipping_country']['provinces'][] = array (
                    'id'    =>  $_Province->getID(), 
                    'iso'   =>  $_Province->getIsoCode(), 
                    'name'  =>  $_Province->getName(), 
                    'printableName' => $_Province->getPrintableName()
                );
            }
        endif;


        
        
        // SELECTED SHIPPING PROVINCE
        
        if (!empty($_ShippingProvince)) $twig_vars['FormCache']['shipping_province_id'] = $_ShippingProvince->getID(); 




        // AVAILABLE SHIPPING OPTIONS

        $twig_vars['FormCache']['shipping_options'] = array ();

        foreach (\Cataleya\Shipping\ShippingOptions::load($_Store) as $ShippingOption) 
        {

                $zone = $ShippingOption->getShippingZone();
                if (!empty($_ShippingProvince) && !$zone->hasLocation($_ShippingProvince)) continue;

                if (empty($_ShippingProvince) && !$zone->hasLocation($_ShippingCountry)) continue;


                // retrieve info about shipping carrier

                $Carrier = $ShippingOption->getCarrier();

                $carrier_info = array (
                        'id'    =>  $Carrier->getID(), 
                        'name'  =>  $Carrier->getCompanyName()
                    );



                // retrieve info about zone


                $zone_info = array (
                        'id'    =>  $zone->getID(), 
                        'name'  =>  $zone->getZoneName(), 
                        'listType'  =>  $zone->getListType(),
                        'countryPopulation' => $zone->getCountryPopulation(), 
                        'provincePopulation' => $zone->getProvincePopulation()
                    );


                // retrieve info about shipping option

                $_shipping_options_info = array (
                    'id'   =>  $ShippingOption->getID(), 
                    'name'  =>  $ShippingOption->getDescription()->getTitle('EN'), 
                    'description'  =>  $ShippingOption->getDescription()->getText('EN'),  
                    'deliveryDays'  =>  $ShippingOption->getMaxDeliveryDays(),  
                    'rateType'  =>  $ShippingOption->getRateType(), 
                    'rate'  =>  $ShippingOption->getRate(), 
                    'Zone'  =>  $zone_info, 
                    'Carrier'   =>  $carrier_info, 
                    'isTaxable'  =>  $ShippingOption->isTaxable()
                );

                $twig_vars['FormCache']['shipping_options'][] = $_shipping_options_info;

                if ($_shipping_options_info['id'] == $twig_vars['FormCache']['shipping_option_id']) $twig_vars['FormCache']['selected_shipping_option'] = $_shipping_options_info;


        }



        // CALCULATE SHIPPING COST
        if (!empty($_ShippingOption) && !empty($_ShippingCountry)): 
            $twig_vars['cart']['ShippingCharges'] = $_Cart->calcShippingCharges($_ShippingOption, (!empty($_ShippingProvince) ? $_ShippingProvince : $_ShippingCountry));
        endif;


        // CALCULATE TAXES
        if (!empty($_ShippingCountry)):
            $twig_vars['cart']['Taxes'] = $_Cart->getTaxes((!empty($_ShippingProvince) ? $_ShippingProvince : $_ShippingCountry));
        endif;






        // SELECTED PAYMENT TYPE
        $_payment_option = $_Cart->getPaymentType();
        
        if (!empty($_payment_option)) 
        {
            $twig_vars['FormCache']['payment_type'] = array_merge(
                $_payment_option, [

                    'form_controls' => \Cataleya\Plugins\Action::trigger(
                        'payment.get-form-controls', [],  
                        [ $_payment_option['id'] ]
                    )
                ]);

        }




        // ------------------- DISCOUNTS ------------------------------ //

        $twig_vars['cart']['Discounts'] = $_Cart->getDiscounts();

        // Discount Form Controls (coupon code fields, gift card codes etc)
        $twig_vars['discount_forms'] = \Cataleya\Plugins\Action::trigger('discount.get-form-controls');


        // -------------------  GRAND TOTAL --------------------------- //


        $twig_vars['cart']['subTotal'] = $_Cart->getTotal(TRUE, TRUE);
        $twig_vars['cart']['grandTotal'] = $_Cart->getTotal();

        // add taxes
        if (!empty($twig_vars['cart']['Taxes'])) foreach ($twig_vars['cart']['Taxes'] as $_Tax) $twig_vars['cart']['grandTotal'] += $_Tax['amount'];

        // add shipping charges (taxes included)
        if (!empty($twig_vars['cart']['ShippingCharges'])) $twig_vars['cart']['grandTotal'] += $twig_vars['cart']['ShippingCharges']['amount_tax_included'];



        
        
        
        
        


        // JSON anyone?
        if (OUTPUT === 'JSON') {
            echo json_encode($twig_vars['cart'], JSON_PRETTY_PRINT);
            exit();
        }
        

        echo __('shop.skin')->render('checkout.html.twig', $twig_vars);
        exit();           
        
        
        
    }
    
    

    
    
    
    

    

        

 
}


