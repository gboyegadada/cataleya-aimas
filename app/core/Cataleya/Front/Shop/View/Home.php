<?php



namespace Cataleya\Front\Shop\View;



/*
 *
 * 	@Package: Cataleya
 * 	@Class: \Cataleya\Front\Shop\View\Category
 *
 * 	(c) 2014 Fancy Paper Planes
 *
 *
 *
 */

class Home extends \Cataleya\Front\Shop\View 
{
    



    
    
    
    
    
    


    /*
     * 
     * [ dispatch ]
     * ______________________________________________________________
     * 
     * 
     * 
     */


    public function dispatch () 
    {
        
        
        

        $_Store = \Cataleya\Front\Shop::getInstance();
        
        $_Page = $_Store->getDefaultPage();
        
        

        // INCLUDE BASE TWIG VARIABLES (ARRAYS)

        $twig_vars = \Cataleya\Front\Shop\Controller::getBaseTwigVars();
        
        
        $twig_vars['cart'] = \Cataleya\Front\Shop\Juicer::juice(
                \Cataleya\Front\Shop\MyCart::getInstance(), 
                $_Store
                );


        // LOAD PLUGINS
        $_plugin_list = $_Page->getPluginList();
        if (!empty($_plugin_list)) {
            $twig_vars['apps'] = \Cataleya\Plugins\Action::trigger('page.get-partials', [], $_plugin_list);
        }

        echo __('shop.skin')->render('/pages/'.$_Page->getTemplateID(), $twig_vars);
        exit();

        
        
        
    }
    
    

    
    
    
    

    

        

 
}


