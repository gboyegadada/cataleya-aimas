<?php



namespace Cataleya\Front\Shop\View\Confirm;



/*
 *
 * 	@Package: Cataleya
 * 	@Class: \Cataleya\Front\Shop\View\Confirm\WelcomeEmail
 *
 * 	(c) 2014 Fancy Paper Planes
 *
 *
 *
 */

class WelcomeEmail extends \Cataleya\Front\Shop\View 
{
    
    


    protected 
            $_username = '';






    
    
    

    


    /*
     * 
     * [ dispatch ]
     * ______________________________________________________________
     * 
     * 
     * 
     * 
     * 
     */


    public function dispatch () 
    {



        
        // COUPON INFO //////////////////////////
        $COUPON = array (
            'code' => 'WELCOME10', 
            'discount' => '10%'
        );




        // SEND CONFIRM EMAIL
        $twig_vars = \Cataleya\Front\Shop\Controller::getBaseTwigVars();
        
        $_Customer = $this->getController()->getCustomer();

        $twig_vars['email'] = array (
            'subject'  =>  '', 
            'message' =>  '', 
            'Customer' => array (
                'firstname' => $_Customer->getFirstname(), 
                'lastname'  =>  $_Customer->getLastname(), 
                'fullname'  =>  $_Customer->getName(), 
                'email' =>  $_Customer->getEmailAddress()
                ), 
            'Coupon' => $COUPON
        );




        $twig_vars['header_title'] = $twig_vars['inline_title'] = 'Welcome to ' . $twig_vars['store']['title'];
        $twig_vars['title_icon'] = BASE_URL . 'ui/images/ui/notification-icons/user-5-50x50.png';



        // New email
        $email = \Cataleya\Helper\Emailer::getInstance('customer.welcome.email');

        $textBody = "Hello " . $_Customer->getFirstname() . "! \n\n";
        $textBody .= "As a thank-you for signing up as a new customer, please use the code: " . $COUPON['code'] . "\n\n";
        $textBody .= "at the checkout for " . $COUPON['discount'] . " off your first order!\n\n";
        $textBody .= 'For help with any of our online services, please email us at ' . $twig_vars['store']['contact']['ContactEmail'];
        $textBody .= "\n\nHappy Shopping, \n\n" . $twig_vars['store']['title'];


        // Email setup
        $email->To = array( $_Customer->getEmailAddress() => $_Customer->getName() );
        $email->From = array('no-reply@'. EMAIL_HOST=> $twig_vars['store']['title'] );
        $email->Sender = 'no-reply@'. EMAIL_HOST;
        $email->ReplyTo = 'no-reply@' . EMAIL_HOST;
        $email->Subject = 'Welcome to ' . $twig_vars['store']['title'];
        $email->TextBody = $textBody;
        $email->Priority = 3;

        $email->setHTMLBody(__('shop.skin')->render('welcome.email.html.twig', $twig_vars));


        // Send confirm email
        return $email->send();
        
        
        
        
    }
    
    
    
    
    

      
    

        

 
}


