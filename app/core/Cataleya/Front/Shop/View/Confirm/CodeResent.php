<?php



namespace Cataleya\Front\Shop\View\Confirm;



/*
 *
 * 	@Package: Cataleya
 * 	@Class: \Cataleya\Front\Shop\View\Confirm\CodeResent
 *
 * 	(c) 2014 Fancy Paper Planes
 *
 *
 *
 */

class CodeResent extends \Cataleya\Front\Shop\View 
{
    
    


    protected 
            $_username = '';





    
    
    

    


    /*
     * 
     * [ dispatch ]
     * ______________________________________________________________
     * 
     * 
     * 
     * 
     * 
     */


    public function dispatch () 
    {



        if (OUTPUT === 'JSON'):
        
            // OUTPUT JSON
            $json_reply = array (
                                    "status"=>'Ok', 
                                    "message"=>"Your verification code has been resent. Please follow the instructions above."
                                    );

            echo json_encode($json_reply);
            exit();
        

        else:

            // OUTPUT HTML
            // GET BASE TWIG VARIABLES (ARRAYS)
            $twig_vars = \Cataleya\Front\Shop\Controller::getBaseTwigVars();


            //Get cart content as twig variables
            $twig_vars['cart'] = \Cataleya\Front\Shop\Juicer::juice(
                    \Cataleya\Front\Shop\MyCart::getInstance(), 
                    \Cataleya\Front\Shop::getInstance()
                    );
            

            
            echo __('shop.skin')->render('confirm_resent.html.twig', $twig_vars);
            exit();

        endif;
        
    }
    
    
    
    
    

      
    

        

 
}


