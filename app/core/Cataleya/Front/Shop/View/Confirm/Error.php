<?php



namespace Cataleya\Front\Shop\View\Confirm;



/*
 *
 * 	@Package: Cataleya
 * 	@Class: \Cataleya\Front\Shop\View\Confirm\Error
 *
 * 	(c) 2014 Fancy Paper Planes
 *
 *
 *
 */

class Error extends \Cataleya\Front\Shop\View 
{
    
    


    protected 
            $_username = '';







    

    


    /*
     * 
     * [ dispatch ]
     * ______________________________________________________________
     * 
     * 
     * 
     * 
     * 
     */


    public function dispatch () 
    {


        // ERROR

        // GET BASE TWIG VARIABLES (ARRAYS)
        $twig_vars = \Cataleya\Front\Shop\Controller::getBaseTwigVars();
        
        
        //Get cart content as twig variables
        $twig_vars['cart'] = \Cataleya\Front\Shop\Juicer::juice(
                \Cataleya\Front\Shop\MyCart::getInstance(), 
                \Cataleya\Front\Shop::getInstance()
                );
        
        // JSON anyone?
        if (in_array(OUTPUT, ['JSON', 'AJAX_JSON'])) {
            echo json_encode(array(
                'status' => 'Error'
            ), JSON_PRETTY_PRINT);
            exit();
        }
        
        
        echo __('shop.skin')->render('error.confirm.html.twig', $twig_vars);
        exit();



        
    }
    
    
    
    
    

      
    

        

 
}


