<?php



namespace Cataleya\Front\Shop\View\Confirm;



/*
 *
 * 	@Package: Cataleya
 * 	@Class: \Cataleya\Front\Shop\View\Confirm\Email
 *
 * 	(c) 2014 Fancy Paper Planes
 *
 *
 *
 */

class Email extends \Cataleya\Front\Shop\View 
{
    
    


    protected 
            $_username = '';






    
    


    
    
    

    


    /*
     * 
     * [ dispatch ]
     * ______________________________________________________________
     * 
     * 
     * 
     * 
     * 
     */


    public function dispatch () 
    {



        

        // Generate new RESEND_TOKEN

        // SEND CONFIRM EMAIL
        $twig_vars = \Cataleya\Front\Shop\Controller::getBaseTwigVars();
        
        $_Customer = $this->getController()->getCustomer();

        $twig_vars['email'] = array (
            'subject'  =>  '', 
            'message' =>  '', 
            'customer' => array (
                'firstname' => $_Customer->getFirstname(), 
                'lastname'  =>  $_Customer->getLastname(), 
                'fullname'  =>  $_Customer->getName(), 
                'email' =>  $_Customer->getEmailAddress()
                ), 
            'confirm'   => array (
                'href'  =>  \Cataleya\Front\Shop::getInstance()->getShopLanding().'confirm/verify?id=' . $_Customer->getConfirmDigest() . ':' . $_Customer->getConfirmCode(), 
                'numCode'   =>  $_Customer->getConfirmCode(), 
                'digest' =>  $_Customer->getConfirmDigest()
            )
        );





        // New email
        $email = \Cataleya\Helper\Emailer::getInstance('customer.confirm.email');

        $textBody = "Hello " . $_Customer->getFirstname() . ", \n\n";
        $textBody .= "Your verification code is: " . $_Customer->getConfirmCode() . "\n\n";
        $textBody .= "Please enter this on the confirmation screen or click the following link to complete your user account registration: \n\n";
        $textBody .= $twig_vars['store']['landing']."confirm/?id=" . $_Customer->getConfirmDigest() . ':' . $_Customer->getConfirmCode();
        $textBody .= "\n\nHappy Shopping, \n\n" . $twig_vars['store']['title'];


        // Email setup


        $email->To = array( $_Customer->getEmailAddress() => $_Customer->getName() );
        $email->From = array(('no-reply@'.EMAIL_HOST) => $twig_vars['store']['title'] );
        $email->Sender = ('no-reply@'.EMAIL_HOST);
        $email->ReplyTo = ('no-reply@'.EMAIL_HOST);
        $email->Subject = $twig_vars['store']['title'] . ': Please verify your email address';
        $email->TextBody = $textBody;
        $email->Priority = 3;

        $email->setHTMLBody(__('shop.skin')->render('confirm-account.email.html.twig', $twig_vars));


        // Send confirm email
        return $email->send();

        
        
        
    }
    
    
    
    
    

      
    

        

 
}


