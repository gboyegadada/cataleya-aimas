<?php



namespace Cataleya\Front\Shop\View\Confirm;



/*
 *
 * 	@Package: Cataleya
 * 	@Class: \Cataleya\Front\Shop\View\Confirm\IncorrectCode
 *
 * 	(c) 2014 Fancy Paper Planes
 *
 *
 *
 */

class IncorrectCode extends \Cataleya\Front\Shop\View 
{
    
    


    protected 
            $_Customer, 
            $_username = '';







    public function __construct() {
        
        parent::__construct();
        
    }
    
    
    
    


    
    
    

    


    /*
     * 
     * [ dispatch ]
     * ______________________________________________________________
     * 
     * 
     * 
     * 
     * 
     */


    public function dispatch () 
    {


        // SET A CONFIRM TOKEN (for confirm form)
        $_SESSION[SESSION_PREFIX.'FORM_TOKEN'] = \Cataleya\Helper::generateToken('');

        
        
        if (IS_LOGGED_IN) $_Customer = \Cataleya\Customer::load($_SESSION[SESSION_PREFIX.'CUSTOMER_ID']);
        else \Cataleya\Front\Shop\Controller::requireLogin();
        
         // GET BASE TWIG VARIABLES (ARRAYS)
        $twig_vars = \Cataleya\Front\Shop\Controller::getBaseTwigVars();
        
        
        //Get cart content as twig variables
        $twig_vars['cart'] = \Cataleya\Front\Shop\Juicer::juice(
                \Cataleya\Front\Shop\MyCart::getInstance(), 
                \Cataleya\Front\Shop::getInstance()
                );

        $twig_vars['customer'] = array (
            'id' => $_Customer->getCustomerId(), 
            'firstname' => $_Customer->getFirstname(), 
            'lastname'  =>  $_Customer->getLastname(), 
            'fullname'  =>  $_Customer->getName(), 
            'email' =>  $_Customer->getEmailAddress(), 
            'confirm'   => array (
                                'href'  =>  \Cataleya\Front\Shop::getInstance()->getShopLanding().'confirm?id=' . 
                                $_Customer->getConfirmDigest() . ':' . 
                                $_Customer->getConfirmCode(), 
                'numCode'   =>  $_Customer->getConfirmCode(), 
                'digest' =>  $_Customer->getConfirmDigest(),
                'confirmToken' =>  $_SESSION[SESSION_PREFIX.'FORM_TOKEN'],
                'resendToken' =>  $_SESSION[SESSION_PREFIX.'RESEND_TOKEN']
            )
        );


        echo __('shop.skin')->render('incorrect_code.confirm.html.twig', $twig_vars);
        exit();

        
    }
    
    
    
    
    

      
    

        

 
}


