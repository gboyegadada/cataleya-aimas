<?php



namespace Cataleya\Front\Shop\View\Register;



/*
 *
 * 	@Package: Cataleya
 * 	@Class: \Cataleya\Front\Shop\View\Register\Success
 *
 * 	(c) 2014 Fancy Paper Planes
 *
 *
 *
 */

class Success extends \Cataleya\Front\Shop\View 
{
    
    


    protected 
            $_username = '';






    

    


    /*
     * 
     * [ dispatch ]
     * ______________________________________________________________
     * 
     * 
     * 
     * 
     * 
     */


    public function dispatch () 
    {


        $twig_vars = \Cataleya\Front\Shop\Controller::getBaseTwigVars();
        $twig_vars['cart'] = \Cataleya\Front\Shop\Juicer::juice(
                \Cataleya\Front\Shop\MyCart::getInstance(), 
                \Cataleya\Front\Shop::getInstance()
                );
        
 
        
        $twig_vars['customer'] = \Cataleya\Front\Shop\Juicer::juice($this->getController()->getCustomer(), \Cataleya\Front\Shop::getInstance());
        
        $_SESSION[SESSION_PREFIX.'FORM_TOKEN'] = \Cataleya\Helper::generateToken('');
        $twig_vars['form_token'] = $_SESSION[SESSION_PREFIX.'FORM_TOKEN'];
        

        echo __('shop.skin')->render('success.register.html.twig', $twig_vars);
        exit();


        
        
        
    }
    
    
    
    
    

      
    

        

 
}


