<?php



namespace Cataleya\Front\Shop\View;



/*
 *
 * 	@Package: Cataleya
 * 	@Class: \Cataleya\Front\Shop\View\Wishlist
 *
 * 	(c) 2014 Fancy Paper Planes
 *
 *
 *
 */

class Wishlist extends \Cataleya\Front\Shop\View 
{
    

    
    

    
    
    
    
    
    
    


    /*
     * 
     * [ dispatch ]
     * ______________________________________________________________
     * 
     * 
     * 
     */


    public function dispatch () 
    {
        
        
        

        $twig_vars = \Cataleya\Front\Shop\Controller::getBaseTwigVars();
        

        $twig_vars['wishlist'] = \Cataleya\Front\Shop\Juicer::juice(
                $this->getController()->getWishlist(), 
                \Cataleya\Front\Shop::getInstance()
                );
        
        $twig_vars['cart'] = \Cataleya\Front\Shop\Juicer::juice(
                \Cataleya\Front\Shop\MyCart::getInstance(), 
                \Cataleya\Front\Shop::getInstance()
                );



        // Set redirect
        $host_pattern = '!^' . preg_replace('/[.]/', '\.', HOST) . '!';
        $twig_vars['cart']['continueHref'] = (isset($_SERVER['HTTP_REFERER']) && preg_match($host_pattern, $_SERVER['HTTP_REFERER']) === 1 && preg_match('/\/shop\/(cart|login|register|logout|confirm|resend-confirmation)/', $_SERVER['HTTP_REFERER']) === 0) ? $_SERVER['HTTP_REFERER'] : \Cataleya\Front\Shop::getInstance()->getShopLanding();



        $twig_vars['cart']['error'] = (!empty($this->_errors)) ? TRUE: FALSE;
        $twig_vars['cart']['errorMessages'] = $this->_errors;
        $twig_vars['cart']['ajaxError'] = $this->_errors;

        // JSON anyone?
        if (in_array(OUTPUT, ['JSON', 'AJAX_JSON'])) {
            echo json_encode($twig_vars, JSON_PRETTY_PRINT);
            exit();
        }
        
        // LOAD TEMPLATE
        $template = (OUTPUT === 'HTML' && IS_AJAX) 
        ? 'wishlist.html.twig'
        : 'wishlist.html.twig';

        echo __('shop.skin')->render($template, $twig_vars);
        exit();         
        
        
        
    }
    
    

    
    
    
    

    

        

 
}


