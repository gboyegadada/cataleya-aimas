<?php



namespace Cataleya\Front\Shop\View;



/*
 *
 * 	@Package: Cataleya
 * 	@Class: \Cataleya\Front\Shop\View\PasswordRecovery
 *
 * 	(c) 2014 Fancy Paper Planes
 *
 *
 *
 */

class PasswordRecovery extends \Cataleya\Front\Shop\View 
{
    
    


    protected 
            $_username = '';







    
    
    

    


    /*
     * 
     * [ dispatch ]
     * ______________________________________________________________
     * 
     * 
     * 
     * 
     * 
     */


    public function dispatch () 
    {




        if (!defined ('NEW_REQ_TOKEN')) define ('NEW_REQ_TOKEN', \Cataleya\Helper::generateToken(''));
        $_SESSION[SESSION_PREFIX.'FORM_TOKEN'] = NEW_REQ_TOKEN; // SECURITY TOKEN...


        $twig_vars = \Cataleya\Front\Shop\Controller::getBaseTwigVars();
        $twig_vars['cart'] = \Cataleya\Front\Shop\Juicer::juice(
                \Cataleya\Front\Shop\MyCart::getInstance(), 
                \Cataleya\Front\Shop::getInstance()
                );

        
        $twig_vars['form_token'] = NEW_REQ_TOKEN;
        $twig_vars['response'] = $this->getResponse();

        echo __('shop.skin')->render('forgot-password.html.twig', $twig_vars);
        exit(); 


        
    }
    
    
    
    
    

    
      
    

        

 
}


