<?php



namespace Cataleya\Front\Shop\View\Login;



/*
 *
 * 	@Package: Cataleya
 * 	@Class: \Cataleya\Front\Shop\View\Login\Session
 *
 * 	(c) 2014 Fancy Paper Planes
 *
 *
 *
 */

class Session extends \Cataleya\Front\Shop\View 
{
    
    


    protected  
            $_username = '';





    
    


    
    
    

    


    /*
     * 
     * [ dispatch ]
     * ______________________________________________________________
     * 
     * 
     * 
     * 
     * 
     */


    public function dispatch ($_RETURN_TO = 'referrer') 
    {



        ////////////////////// SETUP LOGIN SESSION DATA //////////
        //session_name(SESSION_NAME);
        //session_start();

        $_old_session = $_SESSION;
        $_Customer = $this->getController()->getCustomer();


        // IS ADMIN ?
        if (isset($_old_session['ADMIN_IS_ADMIN']) && $_old_session['ADMIN_IS_ADMIN'] === TRUE ) 
        {
                $_SESSION[SESSION_PREFIX.'IS_ADMIN'] = TRUE;

                $_SESSION['ADMIN_IS_ADMIN'] = $_old_session['ADMIN_IS_ADMIN'];
                $_SESSION['ADMIN_ADMIN_ID'] = $_old_session['ADMIN_ADMIN_ID'];
                $_SESSION['ADMIN_LOGGED_IN'] = $_old_session['ADMIN_LOGGED_IN'];

        }

        else {
            $_SESSION[SESSION_PREFIX.'IS_ADMIN'] = FALSE;
            session_regenerate_id(TRUE);
        }




        // First name // Last Name 
        $_SESSION[SESSION_PREFIX.'FNAME'] = $_Customer->getFirstname();
        $_SESSION[SESSION_PREFIX.'LNAME'] = $_Customer->getLastname();


        // CUSTOMER ID
        $_SESSION[SESSION_PREFIX.'CUSTOMER_ID'] = $_Customer->getCustomerId();
        $_SESSION[SESSION_PREFIX.'CUSTOMER_EMAIL'] = $_Customer->getEmailAddress();


        // AUTH TOKEN // LOGGED IN

        unset($_SESSION[SESSION_PREFIX.'LOGIN_TOKEN']);
        $_SESSION[SESSION_PREFIX.'AUTH_TOKEN'] = md5( uniqid(rand(), TRUE) ) . '-' . CURRENT_DT;
        $_SESSION[SESSION_PREFIX.'LOGGED_IN'] = $_Customer->getLoginToken();


        // SET REFERRER
        if (isset($_old_session[SESSION_PREFIX.'REFERRER'])) $_SESSION[SESSION_PREFIX.'REFERRER'] = $_old_session[SESSION_PREFIX.'REFERRER'];

        
        if (empty($_RETURN_TO)) return;
        

        if (
                is_string($_RETURN_TO) 
                && preg_match('/^(referrer)$/i', $_RETURN_TO) > 0) 
                
           {
            
                // Redirect browser...
                $referrer = (
                        isset($_SESSION[SESSION_PREFIX.'REFERRER'])) 
                        ? $_SESSION[SESSION_PREFIX.'REFERRER'] 
                        : \Cataleya\Front\Shop::getInstance()->getShopLanding();

                // If checkout is in progress..
                if (
                        isset($_SESSION[SESSION_PREFIX.'CHECKOUT_IN_PROGRESS']) 
                        && $_SESSION[SESSION_PREFIX.'CHECKOUT_IN_PROGRESS'] === TRUE
                        ) $referrer = \Cataleya\Front\Shop::getInstance()->getShopLanding().'checkout';

           } else {
                $referrer = (is_string($_RETURN_TO)) ? $_RETURN_TO : \Cataleya\Front\Shop::getInstance()->getShopLanding();
           }
            
            
            
            redirect($referrer);
            exit('<center><p>Please wait...</p></center>');
            


        
    }
    
    
    
    
    

    
      
    

        

 
}


