<?php



namespace Cataleya\Front\Shop\View\PasswordRecovery;



/*
 *
 * 	@Package: Cataleya
 * 	@Class: \Cataleya\Front\Shop\View\PasswordRecovery\Email
 *
 * 	(c) 2014 Fancy Paper Planes
 *
 *
 *
 */

class Email extends \Cataleya\Front\Shop\View 
{
    
    

    


    /*
     * 
     * [ dispatch ]
     * ______________________________________________________________
     * 
     * 
     * 
     * 
     * 
     */


    public function dispatch () 
    {



            $_Customer = $this->getController()->getCustomer();
            $_NewPassword = $this->getController()->getNewPassword();
        
            // SEND CONFIRM EMAIL
            $twig_vars = \Cataleya\Front\Shop\Controller::getBaseTwigVars();

            // [2] add password
            $twig_vars['new_password'] = $_NewPassword;

            $twig_vars['email'] = array (
                'subject'  =>  '', 
                'message' =>  '', 
                'customer' => array (
                    'firstname' => $_Customer->getFirstname(), 
                    'lastname'  =>  $_Customer->getLastname(), 
                    'fullname'  =>  $_Customer->getName(), 
                    'email' =>  $_Customer->getEmailAddress()
                    )
            );




            $twig_vars['header_title'] = $twig_vars['inline_title'] = 'New Password';
            $twig_vars['title_icon'] = BASE_URL . 'ui/images/ui/notification-icons/key-50x50.png';



            // New email
            $email = \Cataleya\Helper\Emailer::getInstance('customer.password.email');

            $textBody = "Hello " . $_Customer->getFirstname() . "! \n\n";
            $textBody .= "As you requested, your password has now been reset. Your new password to '" . $twig_vars['store']['title'] . "' is: " . $twig_vars['new_password'] . "\n\n";
            $textBody .= 'For help with any of our online services, please email us at ' . $twig_vars['store']['contact']['ContactEmail'];
            $textBody .= "\n\nHappy Shopping, \n\n" . $twig_vars['store']['title'];


            // Email setup
            $email->To = array( $_Customer->getEmailAddress() => $_Customer->getName() );
            $email->From = array('no-reply@'.EMAIL_HOST=>$twig_vars['store']['title']);
            $email->Sender = 'no-reply@'.EMAIL_HOST;
            $email->ReplyTo = 'no-reply@'.EMAIL_HOST;
            $email->Subject = 'New Password';
            $email->TextBody = $textBody;
            $email->Priority = 3;


            $email->setHTMLBody(__('shop.skin')->render('new-password.email.html.twig', $twig_vars));


            // Send confirm email
            return $email->send();


        
    }
    
    
    
    
    

    
      
    

        

 
}


