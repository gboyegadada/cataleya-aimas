<?php



namespace Cataleya\Front\Shop\View;



/*
 *
 * 	@Package: Cataleya
 * 	@Class: \Cataleya\Front\Shop\View\Logout
 *
 * 	(c) 2014 Fancy Paper Planes
 *
 *
 *
 */

class Logout extends \Cataleya\Front\Shop\View 
{
    
    



    
    

    


    /*
     * 
     * [ dispatch ]
     * ______________________________________________________________
     * 
     * 
     * 
     * 
     * 
     */


    public function dispatch () 
    {



        $host_pattern = '!^' . preg_replace('/[.]/', '\.', HOST) . '!';

        // Redirect browser...
        $referrer = (isset($_SERVER['HTTP_REFERER']) && preg_match($host_pattern, $_SERVER['HTTP_REFERER']) === 1) ? $_SERVER['HTTP_REFERER'] : \Cataleya\Front\Shop::getInstance()->getShopLanding();
        redirect($referrer);
        exit('<center><p>Please wait...</p></center>');



        
    }
    
    
    
    
    

    

        

 
}


