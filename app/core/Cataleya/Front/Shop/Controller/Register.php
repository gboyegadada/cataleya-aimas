<?php



namespace Cataleya\Front\Shop\Controller;



/*
 *
 * 	@Package: Cataleya
 * 	@Class: \Cataleya\Front\Shop\Controller\Register
 *
 * 	(c) 2014 Fancy Paper Planes
 *
 *
 *
 */

class Register 
extends \Cataleya\Front\Shop\Controller 
{
    
    


    protected 
            $_username = '';








    /*
     * 
     * [ execute ]
     * ______________________________________________________________
     * 
     * 
     * 
     * 
     */


    public function execute () 
    {

        switch ($_SERVER['REQUEST_METHOD']) 
        {
            
            
            case 'POST':
                $this->doRegisteration();
                break;

            case 'GET':
                $this->showRegisterationForm();
                break;

            default:
                $this->showRegisterationForm();
                break;
        }
    }
    
    
    
    
    
    

    
    
    
    
    
    
    


    /*
     * 
     * [ doRegister ]
     * ______________________________________________________________
     * 
     * 
     * 
     */


    public function doRegisteration () 
    {
        
        
        

        ///////////////////  IF INPUT IS NOT VIA POST METHOD //////////////////
        if (
                $_SERVER['REQUEST_METHOD'] == 'GET' 
                || !isset($_SESSION[SESSION_PREFIX.'FORM_TOKEN'])) 
        {
            $this->showRegisterationForm();

        }
        else define ('FORM_TOKEN', $_SESSION[SESSION_PREFIX.'FORM_TOKEN']);


        
        
        
        // -----> validate //
        
        $_FORM = \Cataleya\Front\Form::load(INPUT_POST, \Cataleya\Front\Form::FROM_INPUT, [
            'firstname*' => 'name', 
            'lastname*' => 'name', 
            'email*' => 'email', 
            '_email*' => 'email', 
            'password*' => 'password', 
            '_password*' => 'password'
        ]);
        
        if (!$_FORM->isHealthy()) $this->showRegisterationForm();



        // ATTEMPT TO CREATE NEW USER...

        $new_customer_info = array (
            'firstname' =>  $_FORM->getParam('firstname')->getValue(), 
            'lastname' =>  $_FORM->getParam('lastname')->getValue()    
        );

        $this->_Customer = \Cataleya\Customer::create($_FORM->getParam('email')->getValue(), $_FORM->getParam('password')->getValue(), $new_customer_info);
        if ($this->_Customer === NULL) $this->showRegisterationForm();




        // SUBSCRIBE TO NEWSLETTER ??
        $_NewsletterParam = $_FORM->getParam('newsletter');

        if (!empty($_NewsletterParam) && (int)$_NewsletterParam->getValue() === 1 ) 
        {
            // Yes
            $this->_Customer->setNewsletter(TRUE);
        }

        else {
            // No
            $this->_Customer->setNewsletter(FALSE);
        }

        
        

        // STORE CONFIRM DIGEST IN SESSION FOR DIRECT CODE ENTRY
        $_SESSION[SESSION_PREFIX.'CONFIRM_DIGEST'] = $this->_Customer->getConfirmDigest();
        $_SESSION[SESSION_PREFIX.'CONFIRM_CODE'] = $this->_Customer->getConfirmCode();


        // Send Confirm Email...
        $_EmailView = new \Cataleya\Front\Shop\View\Confirm\Email($this);
        $_EmailView->dispatch();
        
        
        
        
        
        
        ////////////////////// SHOW SUCCESS VIEW //////////
        
        $_View = new \Cataleya\Front\Shop\View\Register\Success($this);
        $_View->dispatch(); 
        
        
        
    }
    
    

    
    
    
    

    


    /*
     * 
     * [ showRegisterationForm ]
     * ______________________________________________________________
     * 
     * 
     * 
     * 
     * 
     */


    public function showRegisterationForm () 
    {


        $_View = new \Cataleya\Front\Shop\View\Register();
        $_View->dispatch();         
        
         


        
    }
    
    
    
    
    

    


    
    

    
      
    

        

 
}


