<?php



namespace Cataleya\Front\Shop\Controller\Asset;



/*
 *
 * 	@Package: Cataleya
 * 	@Class: \Cataleya\Front\Shop\Controller\Cart
 *
 * 	(c) 2014 Fancy Paper Planes
 *
 *
 *
 */

class JS 
extends \Cataleya\Front\Shop\Controller 
{
    
    


    protected $_BLOB;







    public function __construct() {
        
        
    }
    
    
    
    

    /*
     * 
     * [ getBlob ]
     * ______________________________________________________________
     * 
     * 
     * 
     * 
     */
    
    public function getBlob() 
    {
        return $this->_BLOB;
    }



    /*
     * 
     * [ getKey ]
     * ______________________________________________________________
     * 
     * 
     * 
     * 
     */
    
    public function getKey() 
    {
        return self::getAction();
    }

    

    /*
     * 
     * [ getErrors ]
     * ______________________________________________________________
     * 
     * 
     * 
     * 
     */
    
    public function getErrors() 
    {
        return $this->_errors;
    }






    /*
     * 
     * [ execute ]
     * ______________________________________________________________
     * 
     * 
     * 
     * 
     */


    public function execute () 
    {
        
        


        switch (self::getAction()) 
        {
            
            
            case 'classes':
                $this->getJSClasses();
                break;

            case 'libs':
                $this->getJSLib();
                break;

            case 'lib':
                $this->getJSLib();
                break;
            
            default:
                $this->fetchJSFile();
                break;
        }
    }
    
    
    
    
    
    

    
    
    




    public function getJSClasses () 
    {
             

        $this->_BLOB = '';


        $_path_to_js_classes = ROOT_PATH . 'ui/jscript/classes/';
        $_dir  = scandir($_path_to_js_classes);

        foreach ($_dir as $filename) {
            if (preg_match('/^(([A-Za-z0-9\-_]+\.?){1,10})\.js$/', $filename) > 0) { 
                $this->_BLOB .= file_get_contents ($_path_to_js_classes.$filename, FILE_SKIP_EMPTY_LINES);
            }
        }


        $_View = new \Cataleya\Front\Shop\View\Asset($this);
        $_View->dispatch();
        
    }
    
    
      
    
    
    
    



    public function getJSLib () 
    {
             
        $_Store = \Cataleya\Front\Shop::getInstance();
        
        $_assets_path = (!empty($_Store)) 
                ? $_Store->getTheme()->getAssetsPath()
                : \Cataleya\System::load()->getDefaultShopFrontTheme()->getAssetsPath();
        

        $this->_BLOB = '';

        
        $_resource_path = file_exists($_assets_path.'js/lib') ? $_assets_path.'js/lib' :  ROOT_PATH.'ui/jscript/libs/';
        $_dir  = scandir($_resource_path);

        foreach ($_dir as $filename) {
            if (preg_match('/^(([A-Za-z0-9\-_]+\.?){1,10})\.js$/', $filename) > 0) { 
                $this->_BLOB .= "\n\n/* ------------------------------------------- */\n\n" . file_get_contents ($_resource_path.$filename, FILE_SKIP_EMPTY_LINES);
            }
        }

        $_View = new \Cataleya\Front\Shop\View\Asset($this);
        $_View->dispatch();
        
    }
    
    


    public function fetchJSFile () 
    {
             
        $_Store = \Cataleya\Front\Shop::getInstance();
        
        $_assets_path = (!empty($_Store)) 
                ? $_Store->getTheme()->getAssetsPath()
                : \Cataleya\System::load()->getDefaultShopFrontTheme()->getAssetsPath();
        

        $_resource_path = file_exists($_assets_path.'jscript/') ? $_assets_path.'jscript/' :  ROOT_PATH.'ui/jscript/';
        $filename = $_resource_path . self::getAction() . '.js';
        
        $this->_BLOB = file_exists($filename) ? file_get_contents ($filename, FILE_SKIP_EMPTY_LINES) : '/* Script Not Found */';
        

        $_View = new \Cataleya\Front\Shop\View\Asset($this);
        $_View->dispatch();
        
    }

        

 
}


