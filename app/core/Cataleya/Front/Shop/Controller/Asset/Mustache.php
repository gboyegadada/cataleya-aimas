<?php



namespace Cataleya\Front\Shop\Controller\Asset;



/*
 *
 * 	@Package: Cataleya
 * 	@Class: \Cataleya\Front\Shop\Controller\Cart
 *
 * 	(c) 2014 Fancy Paper Planes
 *
 *
 *
 */

class Mustache
extends \Cataleya\Front\Shop\Controller 
{
    
    


    protected $_BLOB;







    public function __construct() {
        
        
    }
    
    
    
    

    /*
     * 
     * [ getBlob ]
     * ______________________________________________________________
     * 
     * 
     * 
     * 
     */
    
    public function getBlob() 
    {
        return $this->_BLOB;
    }
    
    
    

    /*
     * 
     * [ getKey ]
     * ______________________________________________________________
     * 
     * 
     * 
     * 
     */
    
    public function getKey() 
    {
        return self::getAction();
    }


    

    /*
     * 
     * [ getErrors ]
     * ______________________________________________________________
     * 
     * 
     * 
     * 
     */
    
    public function getErrors() 
    {
        return $this->_errors;
    }






    /*
     * 
     * [ execute ]
     * ______________________________________________________________
     * 
     * 
     * 
     * 
     */


    public function execute () 
    {
        
        


        
        
        $_Store = \Cataleya\Front\Shop::getInstance();
        
        $_theme_path = (!empty($_Store)) 
                ? $_Store->getTheme()->getFolderPath()
                : \Cataleya\System::load()->getDefaultShopFrontTheme()->getFolderPath();
        

        $filename = $_theme_path.'/mustache/' . self::getAction() . '.mustache';
        
        
        $this->_BLOB = file_exists($filename) ? file_get_contents ($filename, FILE_SKIP_EMPTY_LINES) : '{# Template Not Found #}';
        

        $_View = new \Cataleya\Front\Shop\View\Asset($this);
        $_View->dispatch();
        
    }
    
    
    
    
    

        

 
}


