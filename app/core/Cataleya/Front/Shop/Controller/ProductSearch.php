<?php



namespace Cataleya\Front\Shop\Controller;



/*
 *
 * 	@Package: Cataleya
 * 	@Class: \Cataleya\Front\Shop\Controller\ProductSearch 
 *
 * 	(c) 2014 Fancy Paper Planes
 *
 *
 *
 */

class ProductSearch 
extends \Cataleya\Front\Shop\Controller
{
    
    
    protected $_SearchResults;



    public function __construct() {

 
        
    }
    
    




    /*
     * 
     * [ execute ]
     * ______________________________________________________________
     * 
     * 
     * 
     * 
     */


    public function execute () 
    {

        switch (self::getAction()) 
        {
            
            
            
            case 'view':
                $this->view();
                break;
            
            default:
                $this->view();
                break;
        }
    }
    
    
    
    
    public function getSearchResults() {
        return $this->_SearchResults;
    }
    

    
    
    


    /*
     * 
     * [ view ]
     * ______________________________________________________________
     * 
     * 
     * 
     * 
     * 
     */


    public function view () 
    {
        
        // SANITIZE INPUT DATA...
        $_CLEAN = filter_input_array(INPUT_GET, 
                                                array(
                                                                'term'		=>	FILTER_SANITIZE_STRING

                                                                )
                                                );
        
        


        // Check if INPUT keys are ALL expected AND MADE IT THROUGH...
        if ($_CLEAN['term'] === FALSE) 
        {


            // Output...
            $json_data = array (
                            'status' => 'Ok', 
                            'message' => 'No products found.', 
                            'Products' => array(), 
                            'count' => 0
                            );
            echo json_encode($json_data);
            exit();


        }


        // Words to ignore in search ...

        $_IGNORE = array (
            'the'
        );

        $_CLEAN['term'] = preg_replace('/^(' . implode('|', $_IGNORE) . ')$/', '', $_CLEAN['term']);

        foreach ($_IGNORE as $k=>$word) $_IGNORE[$k] .= '\s';
        $_CLEAN['term'] = preg_replace('/(' . implode('|', $_IGNORE) . ')/', '', $_CLEAN['term']);





        // First, take out funny looking characters...
        $_CLEAN['term'] = preg_replace('/[^-\s\.A-Za-z0-9@*#+]+/', '', $_CLEAN['term']);




        // Load search results as a collection of objects...
        $this->_SearchResults = \Cataleya\Catalog::search($_CLEAN['term']);
        
        $_View = new \Cataleya\Front\Shop\View\ProductSearch($this);
        $_View->dispatch();
        
        
        
        
    }
    
    
    
    
    

    
      
      
    

        

 
}


