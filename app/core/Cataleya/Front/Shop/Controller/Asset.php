<?php



namespace Cataleya\Front\Shop\Controller;



/*
 *
 * 	@Package: Cataleya
 * 	@Class: \Cataleya\Front\Shop\Controller\Cart
 *
 * 	(c) 2014 Fancy Paper Planes
 *
 *
 *
 */

class Asset 
extends \Cataleya\Front\Shop\Controller 
{
    
    


    protected $_Cart;







    public function __construct() {
       
        $this->_Cart = \Cataleya\Front\Shop\MyCart::getInstance();
        
    }
    
    
    
    

    /*
     * 
     * [ getCart ]
     * ______________________________________________________________
     * 
     * 
     * 
     * 
     */
    
    public function getCart() 
    {
        return $this->_Cart;
    }


    

    /*
     * 
     * [ getErrors ]
     * ______________________________________________________________
     * 
     * 
     * 
     * 
     */
    
    public function getErrors() 
    {
        return $this->_errors;
    }






    /*
     * 
     * [ execute ]
     * ______________________________________________________________
     * 
     * 
     * 
     * 
     */


    public function execute () 
    {

        switch (self::getAction()) 
        {
            
            
            case 'classes':
                $this->getJSClasses();
                break;

            case 'lib':
                $this->getJSLib();
                break;
            
            case 'fetch':
                $this->fetchJSFile;
                break;
            
            default:
                $this->view();
                break;
        }
    }
    
    
    
    
    
    

    
    
    




    public function getJSClasses () 
    {
             
        
        
    }
    
    
      
    
    
    
    


    /*
     * 
     * [ remove ]
     * ______________________________________________________________
     * 
     * 
     * 
     */


    public function remove () 
    {
        

        // PROCESS ITEMS TO REMOVE

        if (isset($_POST['items']) && is_array($_POST['items'])) 
        {

            foreach ($_POST['items'] as $key=>$val) {
                $del_id = filter_var($val, FILTER_VALIDATE_INT);

                if ($del_id === FALSE) break; // bail if any invalid param.

                 // Remove cart item
                 $this->_Cart->removeItem((int)$del_id);
            }

        } 

        else if (isset($_GET['id']) && is_scalar($_GET['id'])) 
        {


            $del_id = filter_var($_GET['id'], FILTER_VALIDATE_INT);

            if ($del_id !== FALSE) $this->_Cart->removeItem((int)$del_id);

        }


        
        $this->view();
        
        
        
    }
    
    

    


    /*
     * 
     * [ update ]
     * ______________________________________________________________
     * 
     * 
     * 
     */


    public function update () 
    {
        



        if (!isset($_POST['items']) || !is_array($_POST['items'])) $this->view();
        
        foreach ($_POST['items'] as $key=>$qty): 

            $updt_id  = filter_var($key, FILTER_VALIDATE_INT);
            if ($updt_id === FALSE) continue;

            if (filter_var(
                    $qty, 
                    FILTER_VALIDATE_INT, 
                    array('min_range' => 0, 'max_range' => 50)
                    ) === FALSE) continue; // bail if any invalid param.

            $new_qty = (int)$qty;



            // if qiantity is ZERO: delete
            if ($new_qty === 0) 
            {
                $this->_Cart->removeItem ($updt_id);
                continue;
            }


            // get Cart_item
            $cart_item = $this->_Cart->getItem($updt_id); 

            // Invalid cart_item
            if ($cart_item === NULL) continue;


            // skip if value hasn't changed...
            if ((int)$cart_item->getQuantity() === $new_qty) continue;

            $av_stock = (int)$cart_item->getProductOption()->getStock()->getValue(\Cataleya\Front\Shop::getInstance());
            $qty_order_max = (int)$cart_item->getProduct()->getProductQuantityOrderMax();


            // Check stock and order_max...
            if (($new_qty <= $qty_order_max || $qty_order_max === 0) && ($new_qty <= $av_stock))
            {
                    // Update cart item
                    $cart_item->setQuantity($new_qty);

                    $cart_item->saveData();
            } else {

                // NOTICE:  add a notice here to let the shopper know QTY is not being updated... //
                $this->_errors[] = "We were unable to update some of the items in your bag. This can happen if the quantity you wish to order is more than what we have in stock.";
            }


        endforeach;
        


        
        $this->view();
        
        
        
    }
    
    
    
    
    

    


    /*
     * 
     * [ view ]
     * ______________________________________________________________
     * 
     * 
     * 
     * 
     * 
     */


    public function view () 
    {
        


        
        $_View = new \Cataleya\Front\Shop\View\Cart($this);
        
        $_View->setErrors($this->_errors)->dispatch();         
        
        
        
    }
    
    
    
      
    

        

 
}


