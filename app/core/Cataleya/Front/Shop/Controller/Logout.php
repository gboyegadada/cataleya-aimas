<?php



namespace Cataleya\Front\Shop\Controller;



/*
 *
 * 	@Package: Cataleya
 * 	@Class: \Cataleya\Front\Shop\Controller\Logout
 *
 * 	(c) 2014 Fancy Paper Planes
 *
 *
 *
 */

class Logout 
extends \Cataleya\Front\Shop\Controller 
{
    
    




    public function __construct() {



        
 
        
    }
    
    
    
    




    /*
     * 
     * [ execute ]
     * ______________________________________________________________
     * 
     * 
     * 
     * 
     */


    public function execute () 
    {

        $this->doLogout();
    }
    
    
    
    
    
    

    
    
    
    
    
    


    
    


    /*
     * 
     * [ doLogout ]
     * ______________________________________________________________
     * 
     * 
     * 
     */


    public function doLogout () 
    {

        if (isset($_SESSION[SESSION_PREFIX.'LOGGED_IN']) && $_SESSION[SESSION_PREFIX.'LOGGED_IN'] !== FALSE)
        {
                $_Customer = \Cataleya\Customer::load($_SESSION[SESSION_PREFIX.'CUSTOMER_ID']);
                if ($_Customer !== NULL)
                {

                    // Logout
                    $_Customer->logout();
                    \Cataleya\Session::kill_session(SESSION_NAME);

                    $this->_Customer = $_Customer;      

                }

        }
        

        $_View = new \Cataleya\Front\Shop\View\Logout($this);
        $_View->dispatch();         
        
        

    }
    
    
      
    

        

 
}


