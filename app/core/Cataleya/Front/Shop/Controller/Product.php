<?php



namespace Cataleya\Front\Shop\Controller;



/*
 *
 * 	@Package: Cataleya
 * 	@Class: \Cataleya\Front\Shop\Controller\Cart
 *
 * 	(c) 2014 Fancy Paper Planes
 *
 *
 *
 */

class Product 
extends \Cataleya\Front\Shop\Controller 
{
    
    
    /*
     * 
     * [ execute ]
     * ______________________________________________________________
     * 
     * 
     * 
     * 
     */


    public function execute () 
    {

        switch (self::getAction()) 
        {
            
            
            
            case 'view':
                $this->view();
                break;
            
            default:
                $this->view();
                break;
        }
    }
    
    
    
    
    
    

    
    
    


    /*
     * 
     * [ view ]
     * ______________________________________________________________
     * 
     * 
     * 
     * 
     * 
     */


    public function view () 
    {
        

        $_View = new \Cataleya\Front\Shop\View\Product();
        $_View->dispatch();
        
        
    }
    
    
    
    
    

    
      
      
    

        

 
}


