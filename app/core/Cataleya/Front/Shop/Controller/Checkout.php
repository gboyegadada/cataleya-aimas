<?php



namespace Cataleya\Front\Shop\Controller;



/*
 *
 * 	@Package: Cataleya
 * 	@Class: \Cataleya\Front\Shop\Controller\Checkout
 *
 * 	(c) 2014 Fancy Paper Planes
 *
 *
 *
 */

class Checkout 
extends \Cataleya\Front\Shop\Controller 
{
    
    

    protected $_Form = null, $_tranx_response = [];

    public function __construct() {

 
        
    }
    
    




    /**
     * execute
     *
     * @return void
     */
    public function execute () 
    {
        
        $_Cart = \Cataleya\Front\Shop\MyCart::getInstance();
        
        if (empty($_Cart) || $_Cart->isEmpty()) {

            redirect (\Cataleya\Front\Shop::getInstance()->getShopLanding().'cart/');
            exit('Redirecting...');
            
        }
        
        
        $_SESSION[SESSION_PREFIX.'CHECKOUT_IN_PROGRESS'] = true;
        
        
        \Cataleya\Front\Shop\Controller::requireLogin ();
        $this->_Customer = \Cataleya\Front\Shop\Customer::getInstance();
        

        switch (self::getAction()) 
        {

        case 'view':
            $this->view();
            break;

        case 'shipping':
            $this->saveShipping();
            break;

        case 'discount':
            $this->applyDicount();
            break;

        case 'payment':
            $this->startTransaction();
            break;

        case 'complete':
            $this->completeTransaction();
            break;

        default:
            $this->view();
            break;
        }

    }
    
    
    
    
    
    
    public function getCheckoutForm () 
    {

        return $this->_Form;

    }

    

    
    public function getTransactionResponse () 
    {

        return $this->_tranx_response;

    }



    public function getOrder () 
    {
        if (empty($this->_order)) $this->_order = \Cataleya\Store\Order::load($_SESSION[SESSION_PREFIX.'CHECKOUT_ORDER_ID']);
        return $this->_order;
    }
    


    /**
     * view
     *
     * @return void
     */
    public function view () 
    {
        

        $_View = new \Cataleya\Front\Shop\View\Checkout($this);
        $_View->dispatch();
        
        
    }
    
    
    
    
    
    /**
     * applyDiscount
     *
     * @return void
     */
    public function applyDiscount () 
    {
        

        
        // $this->digestCheckoutForm();
        
        $this->_Form = \Cataleya\Front\Form::load(INPUT_POST, \Cataleya\Front\Form::FROM_INPUT, [
            'discount_app_handle*' => 'handle', 
            'discount_code*' => 'alphanum' 
        ]);

        $_Cart = \Cataleya\Front\Shop\MyCart::getInstance();

        // if cart is not successfully loaded i.e. no existing cart, create.
        $_Cart->setCookie (); // Make sure cookie is fresh

        
        if (!$this->_Form->isHealthy()) $this->show();
        
        $_Param_Handle = $this->_Form->getParam('discount_app_handle');
        $_Param_DiscountCode = $this->_Form->getParam('discount_code');

        $_data = [
            'discount_code' => $_Param_DiscountCode->getValue(), 
            'shop' => \Cataleya\Front\Shop::getInstance(),
            'customer' => $this->getCustomer(), 
            'cart' => $_Cart
        ];

        $_result = \Cataleya\Plugins\Action::trigger(
                    'discount.apply-code', 
                    [ 'params' => $_data ],  
                    [ $_Param_Handle->getValue() ]
                );
        $_result = (!empty($_result)) 
            ? $_result[0] 
            : [ 
                'discount' => null, 
                'error' => [ 'code' => 0, 'message' => 'App not found.' ]
            ];


        // Coupon valid ??
        if (!empty($_result['error'])) {

            $this->_failed_items['discount_code'] = $_result['error']['message'];

        }


        $this->view();
        
    }
    
    
    
    


    /**
     * applyCoupon
     *
     * @return void
     */
    public function applyCoupon () 
    {
        

        
        $this->digestCheckoutForm();
        
        $_FORM = \Cataleya\Front\Form::load();
        $_Store = \Cataleya\Front\Shop::getInstance();
        $_Cart = \Cataleya\Front\Shop\MyCart::getInstance();

        // Validate coupon code...
        $_CouponCode =  $_FORM->getParam('coupon_code');


        // Load coupon...
        if (empty($_CouponCode) || !$_CouponCode->isHealthy()) { 
            $this->_failed_items[$_CouponCode->getName()] = 'Please enter a valid coupon code.';
        } else {
             $_Coupon = \Cataleya\Sales\Coupon::loadByCode ($_Store, $_CouponCode->getValue());
             if (empty($_Coupon)) $this->_failed_items['coupon_code'] = 'Please enter a valid coupon code.';
        }


        // Coupon valid ??
        if (!empty($_Coupon)):
            

            // if cart is not successfully loaded i.e. no existing cart, create.
            $_Cart->setCookie (); // Make sure cookie is fresh


            // Redeemed ??
            if ($_Coupon->isRedeemed($this->_Customer)) $this->_failed_items['coupon_code'] = 'Coupon has already been redeemed.';
            else {

                // Apply Coupon
                foreach ($_Cart as $_CartItem) 
                {
                        $_CartItem->applyCoupon($_Coupon);
                        $_CartItem->saveData();

                    /*
                    foreach (\Cataleya\Sales\Coupons::load() as $_Coupon) 
                    {
                        $_CartItem->applyCoupon($_Coupon);
                        $_CartItem->saveData();
                    }
                     */
                }
            }

        endif;


        $this->view();
        // return;
        
    }
    
    
    
    
    
    
    public function digestCheckoutForm() {

        $_Cart = \Cataleya\Front\Shop\MyCart::getInstance();
        $_Store = \Cataleya\Front\Shop::getInstance();
        $_Currency = $_Store->getCurrency();
        
        $_expected_params = [
            'payment_type*' => 'string', 
            'shipping_option*' => 'int',    
            'shipping_firstname*' => 'name', 
            'shipping_lastname*' => 'name', 
            'shipping_address_1*' => 'address', 
            'shipping_address_2' => 'address', 
            'shipping_city*' => 'name', 
            'shipping_province*' => 'name', 
            'shipping_province_id' => 'int', 
            'shipping_country*' => 'iso', 
            'shipping_postal_code' => 'postcode'
        ];



        // If the checkout form has been displayed before, our cutomer may have selected a payment type. 
        // This option will be stored in the CART until (s)he chooses a new payment type. Here we will try 
        // to retrieve and use it to figure out which form controls to expect in our checkout form..
        $_cart_payment_type = $_Cart->getPaymentType();
        if ( !empty($_cart_payment_type) ) {

            $_ = \Cataleya\Plugins\Action::trigger(
                'payment.get-form-controls', [], [ $_cart_payment_type['id'] ]
            );

            $_payment_form_controls = !empty($_) ? $_ : [];
            foreach ($_payment_form_controls as $_field) {
                $_expected_params[$_field['name']] =  $_field['type'];
            }
         }



        $_FORM = \Cataleya\Front\Form::load(INPUT_POST, \Cataleya\Front\Form::FROM_INPUT, $_expected_params);
        $_FORM->cache();
        
        
        
        foreach ($_FORM as $_Param) {
            
            if (!$_Param->isHealthy() && $_Param->isRequired()) { 
                $this->_failed_items[$_Param->getName()] = $_Param->getErrorText();
            }
            
        }
        
        
        
        // 2. DELIVERY OPTION
        $_Param_ShippingOption = $_FORM->getParam('shipping_option');
        
        if (!empty($_Param_ShippingOption) && $_Param_ShippingOption->isHealthy()) {
            
            $_ShippingOption = \Cataleya\Shipping\ShippingOption::load ($_Param_ShippingOption->getValue());
            
        }
        
        (!empty($_ShippingOption)) 
                ? $_Cart->setShippingOption($_ShippingOption) 
                : $this->_failed_items['shipping_option'] = "Please choose a delivery option.";
        
        
        
        
        // 3. PAYMENT TYPE
        $_Param_PaymentType = $_FORM->getParam('payment_type');
        $_payment_plugin_handle = (!empty($_Param_PaymentType) && $_Param_PaymentType->isHealthy()) 
                ? $_Param_PaymentType->getValue() : null;
 
        // MAX Payable amount..
        if ($_payment_plugin_handle !== null && \Cataleya\Plugins\Package::exists($_payment_plugin_handle))
        {
            $_max_amount = (float)\Cataleya\Plugins\Action::trigger(
                'payment.get-max-amount', 
                [ 
                    'params' => [ 'currency_code' => $_Currency->getCurrencyCode() ] 
                ], 
                [ $_payment_plugin_handle ] 
            );


            $_Cart->setPaymentType($_payment_plugin_handle);

            if (($_max_amount < (float)$_Cart->getTotal())) 
            {

                $this->_notices[] =     'Your order total has exceeded the maximum payable amount (' . 
                                        $_Currency->getCurrencyCode()  . ' ' . 
                                        number_format ($_max_amount, 2) . 
                                        '). Please remove one or more items from your <a href="' . 
                                        \Cataleya\Front\Shop::getInstance()->getShopLanding() . 
                                        'cart">shopping bag</a> to fix this.';

            }

        } else {
            $this->_failed_items['payment_type'] = "Please choose a payment option.";
        }
        
        
       $_Params = $_FORM->getParams(\Cataleya\Front\Form::PARAM_OBJECT);
        

        // 4. SHIPPING ADDRESS
       
        $_ShippingAddress = $_Cart->getShippingAddress();
       
        $_ShippingCountry = \Cataleya\Geo\Country::load ($_Params['shipping_country']->getValue());
        
        empty($_ShippingCountry) 
                ? $_ShippingCountry = $_ShippingAddress->getCountry() 
                : $_ShippingAddress->setCountry($_ShippingCountry);
            
        if ($_ShippingCountry->hasProvinces()) { 
            
                if (
                isset($_Params['shipping_province_id']) && 
                $_Params['shipping_province_id']->isHealthy() 
                )  {
            
                    $_ShippingProvince = \Cataleya\Geo\Province::load ($_Params['shipping_province_id']->getValue());
        
                    if (!empty($_ShippingProvince)) {
                        $_ShippingAddress->setProvince($_ShippingProvince);
                        unset($this->_failed_items['shipping_province']);
                    }
                    else $this->_failed_items['shipping_province_id'] = "Please choose a province/state.";
                           
                } else {
                    $this->_failed_items['shipping_province_id'] = "Please choose a province/state.";
                }
                
        } else if (isset($_Params['shipping_province']) && $_Params['shipping_province']->isHealthy())  {
            
            $_ShippingAddress->setEntryProvince($_Params['shipping_province']->getValue());

        } else {
            $this->_failed_items['shipping_province'] = "Please specify a province/state.";
        }
        
        
        
        
        $_Params['shipping_firstname']->isHealthy() ? $_ShippingAddress->setEntryFirstname($_Params['shipping_firstname']->getValue()) : FALSE;
        $_Params['shipping_lastname']->isHealthy() ? $_ShippingAddress->setEntryLastname($_Params['shipping_lastname']->getValue()) : FALSE;
        $_Params['shipping_address_1']->isHealthy() ? $_ShippingAddress->setEntryStreetAddress($_Params['shipping_address_1']->getValue()) : FALSE;
        $_Params['shipping_address_2']->isHealthy() ? $_ShippingAddress->setEntryStreetAddress2($_Params['shipping_address_2']->getValue()) : FALSE;
        $_Params['shipping_city']->isHealthy() ? $_ShippingAddress->setEntryCity($_Params['shipping_city']->getValue()) : FALSE;
        $_Params['shipping_postal_code']->isHealthy() ? $_ShippingAddress->setEntryPostcode($_Params['shipping_postal_code']->getValue()) : FALSE;
        $_ShippingAddress->saveData();
        
               
        
        $_Cart->saveData();

        
    }
    
    
    
    
    
    


    /**
     * startTransaction
     *
     * @return void
     */
    public function startTransaction () 
    {
        
        $this->digestCheckoutForm();
        
        $_Cart = \Cataleya\Front\Shop\MyCart::getInstance();
        
        /// If ALL is well, start transaction.

        if (empty($this->_failed_items) && empty($this->_notices) && $_Cart->isReady() === true): 

            
            
            $_ShippingAddress = $_Cart->getShippingAddress();
            $_ShippingAddress->setEntryEmail($this->_Customer->getEmailAddress());
            
            
            // Lets remember Shipping Address next time customer checks out...
            $_DefaultContact = $this->_Customer->getDefaultContact();
            
            // Init default contact...
            if (empty($_DefaultContact)) {
                $_DefaultContact = \Cataleya\Asset\Contact::create(
                        $_ShippingAddress->getCountry()->hasProvinces() ? $_ShippingAddress->getProvince() : $_ShippingAddress->getCountry()
                        );
                $this->_Customer->setDefaultContact($_DefaultContact);
            }
            
            // Copy shipping address...
            $_DefaultContact->copy($_ShippingAddress);
            
            
            $_Cart->setCustomer($this->_Customer);
            
        
            // Checkout
            $_payment_app_handle = $_Cart->getPaymentTypeID();
            $_Order = \Cataleya\Store\Order::create($_Cart);
            $_tranx_params = [

                'order_id' => $_Order->getID(), 
                'form_controls' => []

            ];




            $_payment_form_controls = \Cataleya\Plugins\Action::trigger(
                'payment.get-form-controls', [], [ $_payment_app_handle ]
            );

            if (!empty($_payment_form_controls))
            {
                $_FORM = \Cataleya\Front\Form::load(INPUT_POST, \Cataleya\Front\Form::FROM_CACHE);

                foreach ($_payment_form_controls as $_field) {
                    $_Param = $_FORM->getParam($_field['name']); 
                    $_tranx_params['form_controls'][$_field['name']] = $_Param->getValue();
                }
            }





            // We're going to cache some Checkout information in the Session here.
            $_SESSION[SESSION_PREFIX.'CHECKOUT_ORDER_ID'] = $_Order->getID();
            $_SESSION[SESSION_PREFIX.'CHECKOUT_PAYMENT_HANDLE'] = $_payment_app_handle;
            $_SESSION[SESSION_PREFIX.'CHECKOUT_TRANSACTION_REF'] = \Cataleya\Plugins\Action::trigger(
                'payment.get-transaction-reference', 
                ['params' => $_tranx_params], 
                [ $_payment_app_handle ]
            );
            
            $_tranx_params['transaction_ref'] = $_SESSION[SESSION_PREFIX.'CHECKOUT_TRANSACTION_REF'];

            \Cataleya\Plugins\Action::trigger(
                'payment.start-transaction', 
                ['params' => $_tranx_params], 
                [ $_payment_app_handle ]
            );


            /* --------------------- IF NO REDIRECTION HAS OCCURED HERE: 
             * [1] Tranx error OR 
             * [2] Credit card payment OR 
             * [3] Cash on delivery OR 
             * [4] bank transfer ------------------- //
             * 
             * 
             * DO: Complete Transaction
             * 
             * 
             */
            
            $_SESSION[SESSION_PREFIX.'CHECKOUT_IN_PROGRESS'] = FALSE;
        
            $this->completeTransaction();


        else:
               
            // All is not yet well...
            // print_r($this->_failed_items); die();
        
            $this->view();
            
            
            
        endif;

        
        
    }
    
    
    
    
    


    /*
     * 
     * [ completeTransaction ]
     * ______________________________________________________________
     * 
     * 
     * 
     * 
     * 
     */


    public function completeTransaction () 
    {


        /*
         * 
         * Check payment status and echo appropriate response: [ payment successful ] OR [ not successful ]
         * 
         * 
         */


        // [1] Load Tranx
        $_params = [
            'order_id' => $_SESSION[SESSION_PREFIX.'CHECKOUT_ORDER_ID'], 
            'app_handle' => $_SESSION[SESSION_PREFIX.'CHECKOUT_PAYMENT_HANDLE'], 
            'transaction_ref' => $_SESSION[SESSION_PREFIX.'CHECKOUT_TRANSACTION_REF']

            ];

        // [2] Complete tranx
        $this->_tranx_response = \Cataleya\Plugins\Action::trigger(
            'payment.complete-transaction', 
            [ 'params' => $_params ], 
            [ $_SESSION[SESSION_PREFIX.'CHECKOUT_PAYMENT_HANDLE'] ]
        );



        if (isset($this->_tranx_response['status']) && !in_array($this->_tranx_response['status'], ['paid', 'pending'])) 
        {
            // Revert stock quantity...
            $_Cart = \Cataleya\Front\Shop\MyCart::getInstance();
            $_Store = \Cataleya\Front\Shop::getInstance();
            
            foreach ($_Cart as $_Item) 
            {
                $_Stock = $_Item->getProductOption()->getStock();
                $_Stock->addQuantity($_Store, (float)$_Item->getQuantity());
            }
        }


        $_EmailView = new \Cataleya\Front\Shop\View\Checkout\Email($this);
        $_EmailView->dispatch();
        
        $_View = new \Cataleya\Front\Shop\View\Checkout\Status($this);
        $_View->dispatch();

        
    }
    
    
    
    
    
    
    
    
    
      
    

    

        

 
}


