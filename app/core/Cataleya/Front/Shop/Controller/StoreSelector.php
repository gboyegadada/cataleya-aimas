<?php



namespace Cataleya\Front\Shop\Controller;



/*
 *
 * 	@Package: Cataleya
 * 	@Class: \Cataleya\Front\Shop\Controller\StoreSelector
 *
 * 	(c) 2014 Fancy Paper Planes
 *
 *
 *
 */

class StoreSelector 
extends \Cataleya\Front\Shop\Controller 
{
    
    




    /*
     * 
     * [ execute ]
     * ______________________________________________________________
     * 
     * 
     * 
     * 
     */


    public function execute () 
    {

        
        switch (self::getAction()) 
        {
            
            
            
            case 'view':
                $this->view();
                break;
            
            default:
                $this->view();
                break;
        }
    }
    
    
    
    
    
    

    
    
    


    /*
     * 
     * [ view ]
     * ______________________________________________________________
     * 
     * 
     * 
     * 
     * 
     */


    public function view () 
    {
        

        $_View = new \Cataleya\Front\Shop\View\StoreSelector();
        $_View->dispatch();
        
        
    }
    
    
    
    
    

    
      
      
    

        

 
}


