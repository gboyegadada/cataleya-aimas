<?php



namespace Cataleya\Front\Shop\Controller;



/*
 *
 * 	@Package: Cataleya
 * 	@Class: \Cataleya\Front\Shop\Controller\Cart
 *
 * 	(c) 2014 Fancy Paper Planes
 *
 *
 *
 */

class Cart 
extends \Cataleya\Front\Shop\Controller 
{
    
    


    protected $_Cart;







    public function __construct() {
       
        $this->_Cart = \Cataleya\Front\Shop\MyCart::getInstance();
        
    }
    
    
    
    

    /*
     * 
     * [ getCart ]
     * ______________________________________________________________
     * 
     * 
     * 
     * 
     */
    
    public function getCart() 
    {
        return $this->_Cart;
    }


    

    /*
     * 
     * [ getErrors ]
     * ______________________________________________________________
     * 
     * 
     * 
     * 
     */
    
    public function getErrors() 
    {
        return $this->_errors;
    }






    /*
     * 
     * [ execute ]
     * ______________________________________________________________
     * 
     * 
     * 
     * 
     */


    public function execute () 
    {

        switch (self::getAction()) 
        {
            
            
            case 'add':
                $this->add();
                break;

            case 'update':
                $this->update();
                break;
            
            case 'remove':
                $this->remove();
                break;
            
            case 'view':
                $this->view();
                break;
            
            default:
                $this->view();
                break;
        }
    }
    
    
    
    
    
    

    
    
    



    /*
     * 
     * [ add ]
     * ______________________________________________________________
     * 
     * @param object (Product / Tag / Store / ... )
     * @param \Cataleya\Store $_Store
     * 
     * @return array (JSON)
     * 
     * 
     */


    public function add () 
    {
        

        $_CLEAN = filter_input_array(INPUT_POST, 
        array(
                'qty'	=>	array (
                                        'filter' => FILTER_VALIDATE_INT, 
                                        'option' => array(
                                            'min_range' => 0, 
                                            'max_range' => 50
                                            )
                                        ), 
                'product_id'	=> FILTER_VALIDATE_INT, 
                'option_id'	=>	FILTER_VALIDATE_INT
                )
        );





        $_item_added = FALSE; 



        // 2. fetch product option...
        $product_option = \Cataleya\Catalog\Product\Option::load($_CLEAN['option_id']);
        if ($product_option === NULL) {
            $this->_errors[] = "To add an item to your shopping bag, 
                                you must choose something 
                                (e.g. color and size).";

            } else {

                define(
                        'AVAILABLE_STOCK', 
                        $product_option->getStock()->getValue(\Cataleya\Front\Shop::getInstance())
                        );
                
                $_Product = $product_option->getProduct();
                define('QTY_ORDER_MAX', (int)$_Product->getProductQuantityOrderMax());
            }





        if (empty($this->_errors) && $_CLEAN['qty'] !== FALSE) 
        {


                $new_cart_item = $this->_Cart->getItemByAttrib($product_option);

                if ($new_cart_item !== NULL) $item_qty = (int)$new_cart_item->getQuantity();
                else $item_qty = 0;


                // 3. Add to cart...
                $_qty_subtotal = $item_qty +(int)$_CLEAN['qty'];
                
                if (($_qty_subtotal <= QTY_ORDER_MAX || QTY_ORDER_MAX === 0) && ($_qty_subtotal <= AVAILABLE_STOCK))
                {

                        if ($new_cart_item !== NULL) $new_cart_item->updateQuantity((int)$_CLEAN['qty']);
                        else $new_cart_item = $this->_Cart->addItem($product_option);

                        $new_cart_item->saveData();
                        // $_response = "Selected item";
                        $_item_added = TRUE;
                } else {
                    $_item_added = FALSE;
                    $this->_errors[] = "We were unable to add the selected item to your cart. This can happen if the quantity you wish to order is more than what we have in stock.";
                }

        }
        
        
        $this->view();          
        
        
        
    }
    
    
      
    
    
    
    


    /*
     * 
     * [ remove ]
     * ______________________________________________________________
     * 
     * 
     * 
     */


    public function remove () 
    {
        

        // PROCESS ITEMS TO REMOVE

        if (isset($_POST['items']) && is_array($_POST['items'])) 
        {

            foreach ($_POST['items'] as $key=>$val) {
                $del_id = filter_var($val, FILTER_VALIDATE_INT);

                if ($del_id === FALSE) break; // bail if any invalid param.

                 // Remove cart item
                 $this->_Cart->removeItem((int)$del_id);
            }

        } 

        else if (isset($_GET['id']) && is_scalar($_GET['id'])) 
        {


            $del_id = filter_var($_GET['id'], FILTER_VALIDATE_INT);

            if ($del_id !== FALSE) $this->_Cart->removeItem((int)$del_id);

        }


        
        $this->view();
        
        
        
    }
    
    

    


    /*
     * 
     * [ update ]
     * ______________________________________________________________
     * 
     * 
     * 
     */


    public function update () 
    {
        



        if (!isset($_POST['items']) || !is_array($_POST['items'])) $this->view();
        
        foreach ($_POST['items'] as $key=>$qty): 

            $updt_id  = filter_var($key, FILTER_VALIDATE_INT);
            if ($updt_id === FALSE) continue;

            if (filter_var(
                    $qty, 
                    FILTER_VALIDATE_INT, 
                    array('min_range' => 0, 'max_range' => 50)
                    ) === FALSE) continue; // bail if any invalid param.

            $new_qty = (int)$qty;



            // if qiantity is ZERO: delete
            if ($new_qty === 0) 
            {
                $this->_Cart->removeItem ($updt_id);
                continue;
            }


            // get Cart_item
            $cart_item = $this->_Cart->getItem($updt_id); 

            // Invalid cart_item
            if ($cart_item === NULL) continue;


            // skip if value hasn't changed...
            if ((int)$cart_item->getQuantity() === $new_qty) continue;

            $av_stock = (int)$cart_item->getProductOption()->getStock()->getValue(\Cataleya\Front\Shop::getInstance());
            $qty_order_max = (int)$cart_item->getProduct()->getProductQuantityOrderMax();


            // Check stock and order_max...
            if (($new_qty <= $qty_order_max || $qty_order_max === 0) && ($new_qty <= $av_stock))
            {
                    // Update cart item
                    $cart_item->setQuantity($new_qty);

                    $cart_item->saveData();
            } else {

                // NOTICE:  add a notice here to let the shopper know QTY is not being updated... //
                $this->_errors[] = "We were unable to update some of the items in your bag. This can happen if the quantity you wish to order is more than what we have in stock.";
            }


        endforeach;
        


        
        $this->view();
        
        
        
    }
    
    
    
    
    

    


    /*
     * 
     * [ view ]
     * ______________________________________________________________
     * 
     * 
     * 
     * 
     * 
     */


    public function view () 
    {
        


        
        $_View = new \Cataleya\Front\Shop\View\Cart($this);
        
        $_View->setErrors($this->_errors)->dispatch();         
        
        
        
    }
    
    
    
      
    

        

 
}


