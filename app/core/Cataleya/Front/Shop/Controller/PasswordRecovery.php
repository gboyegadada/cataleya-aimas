<?php



namespace Cataleya\Front\Shop\Controller;



/*
 *
 * 	@Package: Cataleya
 * 	@Class: \Cataleya\Front\Shop\Controller\PasswordRecovery
 *
 * 	(c) 2014 Fancy Paper Planes
 *
 *
 *
 */

class PasswordRecovery 
extends \Cataleya\Front\Shop\Controller 
{
    
    


    protected 
            $_username = '', 
            $_NewPassword = '';







    public function __construct() {
        
 
        
    }
    
    
    
    




    /*
     * 
     * [ execute ]
     * ______________________________________________________________
     * 
     * 
     * 
     * 
     */


    public function execute () 
    {

        switch ($_SERVER['REQUEST_METHOD']) 
        {
            
            
            case 'POST':
                $this->doPasswordRecovery();
                break;

            case 'GET':
                $this->showPasswordRecovery();
                break;

            default:
                $this->showPasswordRecovery();
                break;
        }
    }
    
    
    
    
    
    

    
    
    
    
    
    
    


    /*
     * 
     * [ doPasswordRecovery ]
     * ______________________________________________________________
     * 
     * 
     * 
     */


    public function doPasswordRecovery () 
    {
        
        


        // -----> validate //
        
        $_FORM = \Cataleya\Front\Form::load(INPUT_POST, \Cataleya\Front\Form::FROM_INPUT, [
            'username*' => 'email' 
        ]);
        
        if (!$_FORM->isHealthy()) $this->showPasswordRecovery('Please enter a valid email address');
        
        $_Param_Username = $_FORM->getParam('username');
        

        $this->_Customer = \Cataleya\Customer::load($_Param_Username->getValue());
        if ($this->_Customer === NULL)
        {

                (defined('DEBUG_MODE') && DEBUG_MODE == TRUE) || sleep(4);

                $this->showPasswordRecovery('Your e-mail address is not registered.');

        }
        
        
        ////////// Reset Password //////////////////
        
        $this->_NewPassword = $_NewPassword = \Cataleya\Helper::generatePassword(6);
        $this->_Customer->setPassword($_NewPassword)->saveData();


        
        /////////// Send Recovery Email /////////////
        $_Email = new \Cataleya\Front\Shop\View\PasswordRecovery\Email($this);
        $_Email->dispatch(); 
        
        
        
        
        ///////// Show Login Page ///////////////////
        $_LoginView = new \Cataleya\Front\Shop\View\Login($this);
        $_LoginView->setResponse('Hey ' . $this->_Customer->getFirstname() . '! A new password has been sent to your email address.');
        $_LoginView->dispatch(); 
        
        
        
    }
    
    
    /**
     * 
     * @return string
     * 
     */
    public function getNewPassword() {
        return $this->_NewPassword;
    }













    /*
     * 
     * [ showPasswordRecovery ]
     * ______________________________________________________________
     * 
     * 
     * 
     * 
     * 
     */


    public function showPasswordRecovery ($_response_text = '') 
    {


        $_View = new \Cataleya\Front\Shop\View\PasswordRecovery();
        $_View->setResponse($_response_text);
        
        $_View->setErrors($this->_errors)->dispatch();         
        
         


        
    }
    
    
    
    
    
    


    /*
     * 
     * [ sendRecoveryEmail ]
     * ______________________________________________________________
     * 
     * 
     * 
     * 
     * 
     */


    public function sendRecoveryEmail () 
    {


        $_View = new \Cataleya\Front\Shop\View\PasswordRecovery\Email($this);
        
        $_View->dispatch();         
        
         


        
    }
    
    
    


    
    

    
      
    

        

 
}


