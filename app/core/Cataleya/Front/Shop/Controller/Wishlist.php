<?php



namespace Cataleya\Front\Shop\Controller;



/*
 *
 * 	@Package: Cataleya
 * 	@Class: \Cataleya\Front\Shop\Controller\Wishlist
 *
 * 	(c) 2014 Fancy Paper Planes
 *
 *
 *
 */

class Wishlist 
extends \Cataleya\Front\Shop\Controller 
{
    
    


    protected $_Wishlist, $_Store;







    public function __construct() {

        $this->_Store = \Cataleya\Front\Shop::getInstance();
        
        // ATTEMPT TO LOAD WISHLIST
        $_wishlist_key = (isset($_GET['k'])) ? \Cataleya\Helper\Validator::token(substr($_GET['k'], 0), 20, 160) : FALSE;

        if ($_wishlist_key !== FALSE) $this->_Wishlist = \Cataleya\Customer\Wishlist::load($_wishlist_key);
        else if (IS_LOGGED_IN) 
        {
            $_Customer = \Cataleya\Customer::load($_SESSION[SESSION_PREFIX.'CUSTOMER_ID']);
            $this->_Wishlist = $_Customer->getWishlist();
        }

        else if (isset($_GET['k']) && \Cataleya\Helper\Validator::int($_GET['k'], 0, 0) === 0) 
        {
            \Cataleya\Front\Shop\Controller::requireLogin();
        }

        else {
            $this->_Wishlist = NULL;
            _catch_error("Your wishlist could not be found :'''( " . $_GET['k'], __LINE__, true);
        }

 
        
    }
    
    
    
    

    /*
     * 
     * [ getWishlist ]
     * ______________________________________________________________
     * 
     * 
     * 
     * 
     */
    
    public function getWishlist() 
    {
        return $this->_Wishlist;
    }


    

    /*
     * 
     * [ getErrors ]
     * ______________________________________________________________
     * 
     * 
     * 
     * 
     */
    
    public function getErrors() 
    {
        return $this->_errors;
    }






    /*
     * 
     * [ execute ]
     * ______________________________________________________________
     * 
     * 
     * 
     * 
     */


    public function execute () 
    {
        $_action = self::getAction();
        
        if (isset($_POST['a'])) $_action = $_POST['a'];
        
        
            
        switch ($_action) 
        {
            
            
            case 'add':
                $this->add();
                break;

            case 'update':
                $this->update();
                break;
            
            case 'remove':
                $this->remove();
                break;
            
            case 'view':
                $this->view();
                break;
            
            case 'add-to-cart':
                $this->addToCart();
                break;
            
            case 'move-to-cart':
                $this->addToCart(TRUE);
                break;
            
            case 'add-all-to-cart':
                $this->addAllToCart();
                break;
            
            case 'move-all-to-cart':
                $this->addAllToCart(TRUE);
                break;
            
            
            default:
                $this->view();
                break;
        }
    }
    
    
    
    
    
    

    
    
    



    /*
     * 
     * [ add ]
     * ______________________________________________________________
     * 
     * @param object (Product / Tag / Store / ... )
     * @param \Cataleya\Store $_Store
     * 
     * @return array (JSON)
     * 
     * 
     */


    public function add () 
    {
        

        $_CLEAN = filter_input_array(INPUT_POST, 
        array(
                'qty'	=>	array (
                                        'filter' => FILTER_VALIDATE_INT, 
                                        'option' => array(
                                            'min_range' => 0, 
                                            'max_range' => 50
                                            )
                                        ), 
                'product_id'	=> FILTER_VALIDATE_INT, 
                'option_id'	=>	FILTER_VALIDATE_INT
                )
        );





        $_item_added = FALSE; 




        // 1. fetch product...
        $_Product = \Cataleya\Catalog\Product::load($_CLEAN['product_id']);




        if ($_Product !== NULL) 
        {
                define('QTY_ORDER_MAX', (int)$_Product->getProductQuantityOrderMax());

                // 2. fetch product option...
                $product_option = \Cataleya\Catalog\Product\Option::load($_CLEAN['option_id']);

                if ($product_option === NULL) {
                    $this->_errors[] = "To add an item to your wishlist, 
                                        you must choose something 
                                        (e.g. a color and size).";

                    } else {

                        define(
                                'AVAILABLE_STOCK', 
                                $product_option->getStock()->getValue($this->_Store)
                                );
                    }


        } else {
            $this->_errors[] = "The selected item could not be added to your wishlist. This can happen if it is no longer available.";
            }





        if (empty($this->_errors) && $_CLEAN['qty'] !== FALSE) 
        {


            $new_wishlist_item = $this->_Wishlist->getItemByAttrib($_Product->getProductId(), $product_option->getID());

            $item_qty = ($new_wishlist_item !== NULL) 
                        ? (int)$new_wishlist_item->getQuantity() 
                        : 0;


            // 3. Add to wishlist...
            if ((($item_qty +(int)$_CLEAN['qty']) <= QTY_ORDER_MAX || QTY_ORDER_MAX === 0) && (($item_qty +(int)$_CLEAN['qty']) <= AVAILABLE_STOCK))
            {

                    if ($new_wishlist_item !== NULL) $new_wishlist_item->updateQuantity((int)$_CLEAN['qty']);
                    else $new_wishlist_item = $this->_Wishlist->addItem($this->_Store, $product_option);

                    $new_wishlist_item->saveData();
                    // $_response = "Selected item";
                    $_item_added = TRUE;
            } else {
                $_item_added = FALSE;
                $this->_errors[] = "We were unable to add the selected item to your wishlist. This can happen if the quantity you wish to order is more than what we have in stock.";
            }

        }
        
        
        $this->view();          
        
        
        
    }
    
    
      
    
    
    
    


    /*
     * 
     * [ remove ]
     * ______________________________________________________________
     * 
     * 
     * 
     */


    public function remove () 
    {
        

        // PROCESS ITEMS TO REMOVE

        if (isset($_POST['items']) && is_array($_POST['items'])) 
        {

            foreach ($_POST['items'] as $key=>$val) {
                $del_id = filter_var($val, FILTER_VALIDATE_INT);

                if ($del_id === FALSE) break; // bail if any invalid param.

                 // Remove wishlist item
                 $this->_Wishlist->removeItem((int)$del_id);
            }

        } 

        else if (isset($_GET['id']) && is_scalar($_GET['id'])) 
        {


            $del_id = filter_var($_GET['id'], FILTER_VALIDATE_INT);

            if ($del_id !== FALSE) $this->_Wishlist->removeItem((int)$del_id);

        }


        
        $this->view();
        
        
        
    }
    
    

    


    /*
     * 
     * [ update ]
     * ______________________________________________________________
     * 
     * 
     * 
     */


    public function update () 
    {
        



        if (!isset($_POST['items']) || !is_array($_POST['items'])) $this->view();
        
        foreach ($_POST['items'] as $key=>$qty):
            
            $updt_id  = filter_var($key, FILTER_VALIDATE_INT);
            if ($updt_id === FALSE) continue;

            $new_qty = filter_var(
                    $qty, 
                    FILTER_VALIDATE_INT, 
                    array('min_range' => 0, 
                        'max_range' => 50)
                    );

            if ($new_qty === FALSE ) continue; // bail if any invalid param.
            $new_qty = (int)$new_qty;

            // get Wishlist_item
            $_WishlistItem = $this->_Wishlist->getItem($updt_id); 

            // Invalid wishlist_item
            if ($_WishlistItem === NULL) continue;

            // skip if value hasn't changed...
            if ((int)$_WishlistItem->getQuantity() === $new_qty) continue;

            $av_stock = (int)$_WishlistItem->getProductOption()->getStock()->getValue($this->_Store);
            $qty_order_max = (int)$_WishlistItem->getProduct()->getProductQuantityOrderMax();


            // Check stock and order_max...
            if (($new_qty <= $qty_order_max || $qty_order_max === 0) && ($new_qty <= $av_stock))
            {
                    // Update wishlist item
                    $_WishlistItem->setQuantity($new_qty);

                    $_WishlistItem->saveData();
            } else {

                // NOTICE:  add a notice here to let the shopper know QTY is not being updated... //
                $this->_errors[] = "We were unable to update some of the items in your bag. This can happen if the quantity you wish to order is more than what we have in stock.";
            }


        endforeach;
        


        
        $this->view();
        
        
        
    }
    
    
    
    
    



    /*
     * 
     * [ addToCart ]
     * ______________________________________________________________
     * 
     * @param object (Product / Tag / Store / ... )
     * @param \Cataleya\Store $_Store
     * 
     * @return array (JSON)
     * 
     * 
     */


    public function addToCart ($_move = FALSE) 
    {
        
        $_move = (is_bool($_move)) ? $_move : FALSE;
        
        // IF CART HAS NOT BEEN PREVIOUSLY LOADED...
        $_Cart = \Cataleya\Front\Shop\MyCart::getInstance();

        
        // PROCESS ITEMS TO REMOVE

        if (isset($_POST['items']) && is_array($_POST['items'])) 
        {

            foreach ($_POST['items'] as $key=>$val) {
                $_id = filter_var($val, FILTER_VALIDATE_INT);

                if ($_id === FALSE) break; // bail if any invalid param.
                
                $_Item = \Cataleya\Customer\Wishlist\Item::load($_id);
                $_Cart->addItem($_Item->getProductOption(), $_Item->getQuantity());
                
                 // Remove wishlist item
                 if ($_move) $this->_Wishlist->removeItem((int)$_id);
            }

        } 

        else if (isset($_GET['id']) && is_scalar($_GET['id'])) 
        {


            $_id = filter_var($_GET['id'], FILTER_VALIDATE_INT);

            if ($_id !== FALSE) {
                $_Item = \Cataleya\Customer\Wishlist\Item::load($_id);
                $_Cart->addItem($_Item->getProductOption(), $_Item->getQuantity());
                
                if ($_move) $this->_Wishlist->removeItem($_Item);
            }

        }
        
        
        
        if (IS_AJAX) {
        
            $_MiniCart_View = new \Cataleya\Front\Shop\View\Cart($this);
            $_MiniCart_View->dispatch();
        
        } else {
             $this->view();
        }
        
        
    }
    
    
    
    
    
    
    



    /*
     * 
     * [ addAllToCart ]
     * ______________________________________________________________
     * 
     * @param object (Product / Tag / Store / ... )
     * @param \Cataleya\Store $_Store
     * 
     * @return array (JSON)
     * 
     * 
     */


    public function addAllToCart ($_move = FALSE) 
    {
        
        $_move = (is_bool($_move)) ? $_move : FALSE;
        
        // IF CART HAS NOT BEEN PREVIOUSLY LOADED...
        $_Cart = \Cataleya\Front\Shop\MyCart::getInstance();

        // Add items to cart
        foreach ($this->_Wishlist as $_Item) $_Cart->addItem($_Item->getProductOption());


        if ($_move) { 
            $this->_Wishlist->removeAllItems();
            
            // redirect to cart
            header('Location: ' . \Cataleya\Front\Shop::getInstance()->getShopLanding() . 'cart/');
            exit(1);
        }
        
        $this->view();
        
        
    }
    
    
    
    
    
    
    
    
    
    
    
    
    
    

    


    /*
     * 
     * [ view ]
     * ______________________________________________________________
     * 
     * 
     * 
     * 
     * 
     */


    public function view () 
    {

        
            $_View = new \Cataleya\Front\Shop\View\Wishlist($this);
            $_View->dispatch();          
        
        
        
    }
    
    
    
      
    

        

 
}


