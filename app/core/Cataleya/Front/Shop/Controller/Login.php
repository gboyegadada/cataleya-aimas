<?php



namespace Cataleya\Front\Shop\Controller;



/*
 *
 * 	@Package: Cataleya
 * 	@Class: \Cataleya\Front\Shop\Controller\Login
 *
 * 	(c) 2014 Fancy Paper Planes
 *
 *
 *
 */

class Login 
extends \Cataleya\Front\Shop\Controller
{
    
    


    protected 
            $_response = '', 
            $_username = '';







    public function __construct() {
        
 
        
    }
    
    
    
    




    /*
     * 
     * [ execute ]
     * ______________________________________________________________
     * 
     * 
     * 
     * 
     */


    public function execute () 
    {
        
        if (\Cataleya\Front\Shop\Controller::isLoggedIn()) 
        {
            redirect (__url('shop'));
            exit('Redirecting...');
        }

        switch ($_SERVER['REQUEST_METHOD']) 
        {
            
            
            case 'POST':
                $this->doLogin();
                break;

            case 'GET':
                $this->showLogin();
                break;

            default:
                $this->showLogin();
                break;
        }
    }
    
    
    
    
    
    
    

    public function getUsername () 
    {
        return $this->_username;

    }

    
    

    public function getResponse () 
    {

        return $this->_response;
    }
    

    
    
    
    
    


    /*
     * 
     * [ doLogin ]
     * ______________________________________________________________
     * 
     * 
     * 
     */


    public function doLogin () 
    {
        
        
        
        
        // If user is already logged in

        if (defined('IS_LOGGED_IN') && IS_LOGGED_IN === TRUE) 
        {

            // Redirect browser...
            $referrer = (isset($_SESSION[SESSION_PREFIX.'REFERRER'])) ? $_SESSION[SESSION_PREFIX.'REFERRER'] : \Cataleya\Front\Shop::getInstance()->getShopLanding();
            redirect($referrer);
            exit('<center><p>Please wait...</p></center>');
        }

        else if (!defined('IS_LOGGED_IN')) {
            define('IS_LOGGED_IN', FALSE);
        }
        
        
        
        


        ///////////////////  IF INPUT IS NOT VIA POST METHOD //////////////////
        if ($_SERVER['REQUEST_METHOD'] == 'GET' || !isset($_SESSION[SESSION_PREFIX.'FORM_TOKEN'])) $this->showLogin();
        else define ('LOGIN_TOKEN', $_SESSION[SESSION_PREFIX.'FORM_TOKEN']);


        
        
        

        // PROCESS ITEMS TO REMOVE

        define ('LOGIN_TOKEN', $_SESSION[SESSION_PREFIX.'LOGIN_TOKEN']);

        // -----> validate //
        
        $_FORM = \Cataleya\Front\Form::load(INPUT_POST, \Cataleya\Front\Form::FROM_INPUT, [
            'username*' => 'email', 
            'password*' => 'password' 
        ]);
        
        if (!$_FORM->isHealthy()) $this->showLogin();
        
        $_Param_Username = $_FORM->getParam('username');
        $_Param_Password = $_FORM->getParam('password');
        

        $this->_Customer = \Cataleya\Customer::login($_Param_Username->getValue(), $_Param_Password->getValue());
        if ($this->_Customer === NULL)
        {

                (defined('DEBUG_MODE') && DEBUG_MODE == TRUE) || sleep(4);

                //$this->_username = ($_Param_Username->isHealthy()) ? $_Param_Username->getValue() : '';
                $this->_errors[] = $this->_response = 'Username or Password incorrect.';

                $this->showLogin();


        }

        // Check if email is confirmed...
        else if (!$this->_Customer->isConfirmed()) {
            
            // $this->showConfirm();
            $_SESSION[SESSION_PREFIX.'CONFIRM_DIGEST'] = $this->_Customer->getConfirmDigest();
            
            redirect (\Cataleya\Front\Shop::getInstance()->getShopLanding() . 'confirm');
            exit('Redirecting...');
            
        }
        
        
        
        $_ShippingAddress = \Cataleya\Front\Shop\MyCart::getInstance()->getShippingAddress();
        
        $_DefaultContact = $this->_Customer->getDefaultContact();
        if (!empty($_DefaultContact)) { 
            $_ShippingAddress->copy($_DefaultContact);
        }
        
        
        
        ////////////////////// SETUP LOGIN SESSION DATA //////////
        
        $_View = new \Cataleya\Front\Shop\View\Login\Session($this);
        
        $_View->dispatch(); 
        
        
        
    }
    
    

    
    
    
    

    


    /*
     * 
     * [ showLogin ]
     * ______________________________________________________________
     * 
     * 
     * 
     * 
     * 
     */


    public function showLogin () 
    {
        


        $_View = new \Cataleya\Front\Shop\View\Login($this);
        
        $_View
        ->setErrors($this->_errors)
        ->setResponse($this->_response)
        ->dispatch();    
        
         


        
    }
    
    
    
    
    

    


    
    

    
      
    

        

 
}


