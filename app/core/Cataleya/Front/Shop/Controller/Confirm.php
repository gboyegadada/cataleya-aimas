<?php



namespace Cataleya\Front\Shop\Controller;



/*
 *
 * 	@Package: Cataleya
 * 	@Class: \Cataleya\Front\Shop\Controller\Confirm
 *
 * 	(c) 2014 Fancy Paper Planes
 *
 *
 *
 */

class Confirm 
extends \Cataleya\Front\Shop\Controller 
{
    
    


    protected 
            $_username = '';







    public function __construct() {
        
        
        
 
        
    }
    
    
    
    




    /*
     * 
     * [ execute ]
     * ______________________________________________________________
     * 
     * 
     * 
     * 
     */


    public function execute () 
    {

        switch (self::getAction()) 
        {
            
            case 'view':
                $this->showConfirm();
                break;
            
            case 'verify':
                $this->doConfirm();
                break;
 
            case 'resend':
                $this->resendConfirmCode();
                break;

            default:
                $this->showConfirm();
                break;
        }
    }
    
    
    
    
    
    




    /*
     * 
     * [ doConfirm ]
     * ______________________________________________________________
     * 
     * 
     * 
     */


    public function doConfirm () 
    {
        
        
        /*
        * 
        * CONFIRM TOKEN REQUIRED IF REQUEST VIA POST
        * 
        */

       if (isset($_SESSION[SESSION_PREFIX.'FORM_TOKEN'])) define ('CONFIRM_TOKEN', $_SESSION[SESSION_PREFIX.'FORM_TOKEN']);
       else if ($_SERVER['REQUEST_METHOD'] == 'GET') define ('CONFIRM_TOKEN', '');
       else \Cataleya\Front\Shop\Controller::requireLogin();




        $_response = '';
        $confirm_digest = $num_code = NULL;

        ///////////////////  IF INPUT IS VIA GET METHOD //////////////////
        if ($_SERVER['REQUEST_METHOD'] == 'GET' && isset($_GET['id'])) 
        {
            list($confirm_digest, $num_code) = explode(':', $_GET['id']);

            $confirm_digest = filter_var($confirm_digest, FILTER_SANITIZE_STRIPPED);
            $num_code = filter_var($num_code, FILTER_VALIDATE_INT);

            if ($confirm_digest === FALSE) $this->showConfirmError(); 


        }
        
        
        
        
        



        ///////////////////  IF INPUT IS VIA POST METHOD //////////////////
        else if ($_SERVER['REQUEST_METHOD'] === 'POST')
        {
            
            
            
            // -----> validate //

            $_FORM = \Cataleya\Front\Form::load(INPUT_POST, \Cataleya\Front\Form::FROM_INPUT, [
                'digest*' => 'alphanum', 
                'num_code*' => 'alphanum' 
            ]);

            if (!$_FORM->isHealthy()) $this->showConfirmError();
            
            $confirm_digest = $_FORM->getParam('digest')->getValue();
            $num_code = $_FORM->getParam('num_code')->getValue();


        }
        
        
        

        // ATTEMPT TO RETRIEVE USER INFO...

        $this->_Customer = \Cataleya\Customer::load($confirm_digest);
        if ($this->_Customer === NULL) $this->showConfirmError(); // Error


        
        


        // VERIFY NUMBER CODE

        if (!$this->_Customer->confirm($confirm_digest, $num_code))
        {
            // show confirm if there is an active session
            if (
                    isset($_SESSION[SESSION_PREFIX.'CONFIRM_DIGEST']) && 
                    $_SESSION[SESSION_PREFIX.'CONFIRM_DIGEST'] === $confirm_digest
                    ) $this->showIncorrectCode();

            // Else display error
            else $this->showConfirmError();
        }




        // ADD TO MAILING LIST

        if ($this->_Customer->isSubscribed()) 
        {
            $MailChimp = new MailChimp(MAILCHIMP_API_KEY);
            $result = $MailChimp->call('lists/subscribe', array(
                                            'id'                => MAILCHIMP_LIST_ID,
                                            'email'             => array('email'=>$this->_Customer->getEmailAddress()),
                                            'merge_vars'        => array('FNAME'=>$this->_Customer->getFirstname(), 'LNAME'=>$this->_Customer->getLastname()),
                                            'double_optin'      => false,
                                            'update_existing'   => true,
                                            'replace_interests' => false,
                                            'send_welcome'      => false,
                                    ));
        }


        
        

        // Confirmation SUCCESSFUL. Send welcome email.
        $this->sendWelcomeEmail();

        // AUTO LOGIN
        $this->_Customer->quickLogin();

        $_View = new \Cataleya\Front\Shop\View\Confirm\Success($this);
        $_View->dispatch();    
        
    }
    
    

    
    
    
    

    


    /*
     * 
     * [ showConfirmError ]
     * ______________________________________________________________
     * 
     * 
     * 
     * 
     * 
     */


    public function showConfirmError () 
    {


        
        $_View = new \Cataleya\Front\Shop\View\Confirm\Error($this);
        
        $_View->dispatch(); 

        
    }
    
    
    
    
    
    


    /*
     * 
     * [ showIncorrectCode ]
     * ______________________________________________________________
     * 
     * 
     * 
     * 
     * 
     */


    public function showIncorrectCode () 
    {

        
        $_View = new \Cataleya\Front\Shop\View\Confirm\IncorrectCode($this);
        
        $_View->dispatch(); 

        
    }
    
    
    
    
    
    
    
    
    

    


    /*
     * 
     * [ showConfirm ]
     * ______________________________________________________________
     * 
     * 
     * 
     * 
     * 
     */


    public function showConfirm () 
    {

        
        
        
        $_View = new \Cataleya\Front\Shop\View\ConfirmIndex($this);
        $_View->dispatch();      
        
        
        
    }
    
    
    
    
    


    /*
     * 
     * [ resendConfirmCode ]
     * ______________________________________________________________
     * 
     * 
     * 
     * 
     * 
     */


    public function resendConfirmCode () 
    {
        // Force request via POST.
        if ($_SERVER['REQUEST_METHOD'] !== 'POST') $this->showConfirmError ();
        
        

        if (isset($_SESSION[SESSION_PREFIX.'CONFIRM_DIGEST'])) $this->_Customer = \Cataleya\Customer::load($_SESSION[SESSION_PREFIX.'CONFIRM_DIGEST']);
        

        // Check if customer var is set
        if (!isset($this->_Customer)) $this->showConfirmError ();

        
        
        $_EmailView = new \Cataleya\Front\Shop\View\Confirm\Email($this);
        $_EmailView->dispatch();


            
        $_View = new \Cataleya\Front\Shop\View\Confirm\CodeResent($this);
        $_View->dispatch();   
    }
    
    
    
    


    /*
     * 
     * [ sendConfirmCode ]
     * ______________________________________________________________
     * 
     * 
     * 
     * 
     * 
     */


    public function sendConfirmCode () 
    {

        
        // Force request via POST.
        if ($_SERVER['REQUEST_METHOD'] !== 'POST') $this->showConfirmError ();
        
        

        if (isset($_SESSION[SESSION_PREFIX.'CONFIRM_DIGEST'])) $this->_Customer = \Cataleya\Customer::load($_SESSION[SESSION_PREFIX.'CONFIRM_DIGEST']);
        

        // Check if customer var is set
        if (!isset($this->_Customer)) $this->showConfirmError ();

        
        
        $_EmailView = new \Cataleya\Front\Shop\View\Confirm\Email($this);
        $_EmailView->dispatch();


        return $this;    
        
        
        
    }
    
    
    
      
    
    
    

    
    


    /*
     * 
     * [ sendWelcomeEmail ]
     * ______________________________________________________________
     * 
     * 
     * 
     */


    protected function sendWelcomeEmail () 
    {


        $_EmailView = new \Cataleya\Front\Shop\View\Confirm\WelcomeEmail($this);
        $_EmailView->dispatch();




    }
    
    
      
    

        

 
}


