<?php



namespace Cataleya\Front\Shop\Controller;



/*
 *
 * 	@Package: Cataleya
 * 	@Class: \Cataleya\Front\Shop\Controller\MyAccount
 *
 * 	(c) 2014 Fancy Paper Planes
 *
 *
 *
 */

class MyAccount 
extends \Cataleya\Front\Shop\Controller 
{
    
    

    

    public function __construct() {

 
        
    }
    
    




    /*
     * 
     * [ execute ]
     * ______________________________________________________________
     * 
     * 
     * 
     * 
     */


    public function execute () 
    {
        

        \Cataleya\Front\Shop\Controller::requireLogin ();
        
        $this->_Customer = \Cataleya\Customer::load($_SESSION[SESSION_PREFIX.'CUSTOMER_ID']);

        switch (self::getAction()) 
        {
            
            
            
            case 'view':
                $this->view();
                break;
                        
            case 'order-details':
                $this->showOrderDetails();
                break;
            
            case 'edit':
                $this->edit();
                break;
            
            case 'save':
                $this->save();
                break;
            
            default:
                $this->view();
                break;
        }
    }
    
    
    
    
    
    

    
    
    


    /*
     * 
     * [ view ]
     * ______________________________________________________________
     * 
     * 
     * 
     * 
     * 
     */


    public function view () 
    {
        
        
        $_View = new \Cataleya\Front\Shop\View\MyAccount($this);
        $_View->dispatch();
        
        
    }
    
    
    
    
    
    
    /**
     * Order Details
     */
    public function showOrderDetails() 
    {
        
        
        $_View = new \Cataleya\Front\Shop\View\MyAccount\OrderDetails($this);
        $_View->dispatch();
        
        
    }
    
    
    
    
    
    
    


    /*
     * 
     * [ edit ]
     * ______________________________________________________________
     * 
     * 
     * 
     * 
     * 
     */


    public function edit () 
    {
        
        

        $_View = new \Cataleya\Front\Shop\View\MyAccount\Edit($this);
        $_View->dispatch();
        
        
    }
    
    
    
      
    


    /*
     * 
     * [ save ]
     * ______________________________________________________________
     * 
     * 
     * 
     * 
     * 
     */


    public function save () 
    {
        
        
        
        // -----> validate //
        
        $_FORM = \Cataleya\Front\Form::load(INPUT_POST, \Cataleya\Front\Form::FROM_INPUT, [
            'firstname*' => 'name', 
            'lastname*' => 'name', 
            'email*' => 'email', 
            '_email' => 'email', 
            'current_password' => 'mypassword', 
            'new_password' => 'password', 
            '_new_password' => 'password'
        ]);
        $_FORM->cache();
        
        if (!$_FORM->isHealthy()) { 
            foreach ($_FORM as $p) {
                if (!$p->isHealthy() && $p->isRequired() ) {
                    
                    $this->_failed_items[$p->getName()] = $p->getErrorText();
                }
            }
            
            
            $this->edit();
        }



        // ATTEMPT TO UPDATE USER INFO...
        $_Params = $_FORM->getParams();
        
        if ($_Params['firstname']->isHealthy()) $this->_Customer->setFirstname($_Params['firstname']->getValue());
        else $this->_failed_items['firstname'] = $_Params['firstname']->getErrorText();
        
        if ($_Params['lastname']->isHealthy()) $this->_Customer->setLastname($_Params['lastname']->getValue());
        else $this->_failed_items['lastname'] = $_Params['lastname']->getErrorText();

        // Change Password
        if (
                $_Params['new_password']->isHealthy() 
                && $_Params['_new_password']->isHealthy() 
                && $_Params['current_password']->isHealthy() 
                && $this->_Customer->confirmPassword($_Params['current_password']->getValue()) 
                
                ) {
            
                    $this->_Customer->setPassword($_Params['new_password']->getValue());

                } else  {
                    
                               
                    $_raw_value = $_Params['new_password']->getRawValue();
                    if (!empty($_raw_value) && is_string($_raw_value)) {
                        
                        if (!$_Params['current_password']->isHealthy()) $this->_failed_items['current_password'] = $_Params['current_password']->getErrorText();
                        else if (!$this->_Customer->confirmPassword($_Params['current_password']->getValue())) $this->_failed_items['current_password'] = "Incorrect password.";
                        
                        
                        if (!$_Params['new_password']->isHealthy()) $this->_failed_items['new_password'] = $_Params['new_password']->getErrorText();
                        if (!$_Params['_new_password']->isHealthy()) $this->_failed_items['new_password'] = $_Params['new_password']->getErrorText();
                        
                        $this->_failed_items['_new_password'] = $_Params['_new_password']->getErrorText();
                        $this->edit();
                    }
                }
                

        // Change Email
        if (
                $_Params['email']->isHealthy() 
                && $this->_Customer->getEmailAddress() !== $_Params['email']->getValue()
                ) {
                    
                    // Set new email address...
                    $this->_Customer->setEmailAddress($_Params['email']->getValue(), FALSE);

                    // Send Confirm Email...
                    //$_EmailView = new \Cataleya\Front\Shop\View\Confirm\Email($this);
                    //$_EmailView->dispatch();

                }
                
        // Setup session variables
        $_SessionView = new \Cataleya\Front\Shop\View\Login\Session($this);
        $_SessionView->dispatch(\Cataleya\Front\Shop::getInstance()->getShopLanding().'myaccount'); 
        
        
        
    }
    
    
    
    

        

 
}


