<?php


namespace Cataleya\Front\Shop;




/*
 *
 *	@Package: Cataleya
 *	@Class: \Cataleya\Front\Shop\Content
 *
 *	(c) 2012 Fancy Paper Planes
 *
 *
 *
 */


class Page extends \Cataleya\Asset\Content 

{
	
	protected 
                
         $_data, 
	 $_content_languages = array(), 
	 $_modified = array(),  
	 $_content_modified = array(), 
         $_johny_jus_come = false, 
                
         $_content_type, 
         $_url_rewrite;


	protected 
			$dbh, 
			$e;
			
			
			






	/**
         * 
         * @param integer $instance_id
         * @param \Cataleya\Store $_Store
         * @return \Cataleya\Front\Shop\Page
         * 
         */
	public static function load ($instance_id = 0, \Cataleya\Store $_Store = NULL) 
	{
		
		// construct
                $_objct = __CLASS__;
                
                $instance = new $_objct ();
		
                
		$load_with_slug = false;
                
                // arg is tag_id
		if (filter_var($instance_id, FILTER_VALIDATE_INT) !== FALSE && (int)$instance_id > 0)  {} // Do nothing
                
                // arg is a keyword
                else if (is_string($instance_id) && filter_var($instance_id, FILTER_VALIDATE_REGEXP, array('options'=>array('regexp' => '/^(([A-Za-z0-9]+\-?){1,50})$/'))) !== FALSE) { $load_with_slug = true; }
                
                // Fire burn this ID !!
                else \Cataleya\Helper\ErrorHandler::getInstance()->triggerException('Error in class (' . __CLASS__ . '): [ invalid argument (invalid id or keyword) for method: ' . __FUNCTION__ . ' ] on line ' . __LINE__);
                
		
										
		$row = 	($load_with_slug) 
                        ? $instance->loadDataWithKeyword($instance_id, $_Store) 
                        : $instance->loadDataWithID($instance_id);	
                
                
		if (empty($row))
		{ 
                    
                    
                    return NULL;
		} else {
                    $instance->_data = $row;
                }
                
                
                
		
		// LOAD: URL_REWRITE
                //if (is_numeric($instance->_data['url_rewrite_id'])) {
                 //   $instance->_url_rewrite = \Cataleya\Asset\URLRewrite::load($instance->_data['url_rewrite_id']);
                //}


                $instance->_loadEntries();
                
                return $instance;
		
	}
        
        
        
        
        protected function loadDataWithKeyword ($instance_id, \Cataleya\Store $_Store) {

		///// CHECK IF DESCRIPTION ID IS VALID /////////////////////////
		static 
					$select_handle, 
					$select_handle_param_instance_id, 
                                        $select_handle_param_store_id;

			
		if (!isset($select_handle)) {
			// PREPARE SELECT STATEMENT...
			$select_handle = $this->dbh->prepare('
                            SELECT * FROM content a
                            INNER JOIN url_rewrite b 
                            ON a.url_rewrite_id = b.url_rewrite_id 
                            WHERE b.keyword = :instance_id  
                            AND b.store_id = :store_id  
                            LIMIT 1
                            ');
			$select_handle->bindParam(':instance_id', $select_handle_param_instance_id, \PDO::PARAM_STR);
			$select_handle->bindParam(':store_id', $select_handle_param_store_id, \PDO::PARAM_INT);
		}
                
		
		$select_handle_param_instance_id = $instance_id;
                $select_handle_param_store_id = $_Store->getID();
		
		if (!$select_handle->execute()) $this->e->triggerException('
										Error in class (' . __CLASS__ . '): [ ' . 
										implode(', ', $select_handle->errorInfo()) . 
										' ] on line ' . __LINE__);
										
		$row = 	$select_handle->fetch(\PDO::FETCH_ASSOC);
                
                return $row;
                
        }

        
        
        

        
        protected function loadDataWithID ($instance_id) {

		///// CHECK IF DESCRIPTION ID IS VALID /////////////////////////
		static 
					$select2_handle, 
					$select2_handle_param_instance_id;

			
		
		if (empty($select2_handle))
		{
			$select2_handle = $this->dbh->prepare('SELECT * FROM content WHERE content_id = :instance_id LIMIT 1');
			$select2_handle->bindParam(':instance_id', $select2_handle_param_instance_id, \PDO::PARAM_INT);
		}
                
                
		
		$select2_handle_param_instance_id = $instance_id;
		
		if (!$select2_handle->execute()) $this->e->triggerException('
										Error in class (' . __CLASS__ . '): [ ' . 
										implode(', ', $select2_handle->errorInfo()) . 
										' ] on line ' . __LINE__);
										
		$row = 	$select2_handle->fetch(\PDO::FETCH_ASSOC);
                
                
                if (empty($row)) \Cataleya\Helper\ErrorHandler::getInstance()->triggerException('data could not be read...' . $instance_id . ':' . __LINE__);
                
                return $row;
                
        }





        /**
         * 
         * @param \Cataleya\Store $_Store
         * @param string $title
         * @param string $text
         * @param string $language_code
         * @param boolean $_is_system
         * @return \Cataleya\Front\Shop\Page
         * 
         */
	public static function create (\Cataleya\Store $_Store, $title = 'Untitled', $text = 'No description', $language_code = 'EN', $_is_system = FALSE)  // defaults to english (EN)
	{


                
                /*
                 * Handle url_rewrite Attribute 
                 *
                 */

                 $url_rewrite = \Cataleya\Asset\URLRewrite::create($_Store, $title, 'index.php', 0);

                 $options = array (
                     $_Store, 
                     $url_rewrite
                 );

             
		
                
                $_optional_params = self::buildOptionsQuery($options);
                
                $_params = array ( 
                'content_type' => self::TYPE_PAGE, 
                'is_system' => ($_is_system === TRUE) ? 1 : 0
                        );
                
                $_params = array_merge_recursive($_params, $_optional_params);
                
                $_cols_str = $_vals_str = [];

                foreach ($_params as $key=>$val) {

                     If (!is_numeric($val) && !is_string($val) && !is_null($val)) { $_params[$key] = $val= ''; }
                     
                     $_cols_str[] = $key;
                     $_vals_str[] = ':'.$key;
                }
                

                // Get database handle...
                $dbh = \Cataleya\Helper\DBH::getInstance();

                // Get error handler
                $e = \Cataleya\Helper\ErrorHandler::getInstance();
                
                
		
		static $insert_handle;
		
		if (empty($insert_handle))
		{
			$insert_handle = $dbh->prepare('INSERT INTO content (date_added, last_modified, ' . implode(', ', $_cols_str) . ') VALUES (NOW(), NOW(), ' . implode(', ', $_vals_str) . ')');
		

                        
                }
                
   
                foreach ($_params as $key=>$val) {


                        $insert_handle->bindParam(
                                ':'.$key, 
                                $_params[$key], 
                                \Cataleya\Helper\DBH::getTypeConst($val)
                                );

                }
                

		if (!$insert_handle->execute()) $e->triggerException('
										Error in class (' . __CLASS__ . '): [ ' . 
										implode(', ', $insert_handle->errorInfo()) . 
										' ] on line ' . __LINE__);
										
		$_instance_id = $dbh->lastInsertId();
		$instance = self::load($_instance_id);
		
		if ($title !== NULL && $text !== NULL)
		{
		if(!is_string($language_code) || strlen($language_code) != 2) $language_code = 'EN';
		$language_code = strtoupper($language_code);
		
		$instance->saveEntry($title, $text, $language_code);
		}
		
                $instance->setTemplateID('index.html.twig');
                $instance->_johny_jus_come = TRUE;
                
                $instance->setSlug($title);
                
		return $instance;
		
	}
        
        
        
        
        


        protected static function buildOptionsQuery($objects = array()) 
        {

            $_statements = array();


            $_classes = array(

                'Cataleya\Store' => array(
                                        'col' => 'store_id'
                                    ), 

                'Cataleya\Asset\URLRewrite' => array(
                                        'col' => 'url_rewrite_id'
                                    ), 

                'Cataleya\Front\Shop\Page' => array(
                                        'col' => 'parent_id'
                                    )

            );



            foreach ($objects as $obj) {
                if (!is_object($obj)) continue;
                $_classname = get_class($obj);
                
                if (!array_key_exists($_classname, $_classes)) continue;

                $_statements[$_classes[$_classname]['col']] = (int)$obj->getID();

            }






            return $_statements;
        }
        
        
        
        
        

        
        
        
        
        
        
        
        

	/*
	 *
	 *  getTemplateID
	 * 
	 *
	 *
	 *
	 */
	 
	public function getTemplateID () 
	{
		
		
		return $this->_data['template'];
			
		
		
	}
        
        
        
        
        
        

	/*
	 *
	 *  setTemplateID
	 * 
	 *
	 *
	 *
	 */
	 
	public function setTemplateID ($_template_id) 
	{
		
		$_list = $this->getStore()->getTheme()->getPageTemplates();
                
		if (in_array($_template_id, $_list)) $this->_data['template'] = $_template_id;
        else return FALSE;
			
		$this->_modified[] = 'template';
                
        return TRUE;
    
	}





    /**
     * getPluginList
     *
     * @return array    Array of plugin handles.
     */
    public function getPluginList () {

        return (!empty($this->_data['plugin_list'])) 
            ? explode(' ', $this->_data['plugin_list']) 
            : [];

    }



    /**
     * getAppList
     *
     * @return array    Array of plugin handles.
     */
    public function getAppList () {

        return $this->getPluginList();

    }



    /**
     * addPlugin
     *
     * @param string|array $_plugin_handle
     * @return \Cataleya\Front\Shop\Page
     */
    public function addPlugin ($_plugin_handle) {

        if (is_string($_plugin_handle)) $_plugin_handle = [ $_plugin_handle ];
        if (!is_array($_plugin_handle)) throw new \Cataleya\Error ('Argument must be a string or an array of strings.');


        $_list = $this->getPluginList ();
    
        foreach ($_plugin_handle as $_handle) {
           if ( 
            is_string($_handle) && 
            \Cataleya\Plugins\Package::exists($_handle)
            ) $_list[] = $_handle;
        }

        foreach ($_list as $k=>$v) {
            $v = trim($v);
            if (empty($v) || !\Cataleya\Plugins\Package::exists($v)) unset($_list[$k]);
            else $_list[$k] = $v;
        }

        $this->_data['plugin_list'] = (!empty($_list)) ?  implode(' ', $_list) : '';
        $this->_modified[] = 'plugin_list';
        
        return $this;

    }





    /**
     * removePlugin
     *
     * @param string|array $_plugin_handle
     * @return \Cataleya\Front\Shop\Page
     */
    public function removePlugin ($_plugin_handle) {


        if (is_string($_plugin_handle)) $_plugin_handle = [ $_plugin_handle ];
        if (!is_array($_plugin_handle)) throw new \Cataleya\Error ('Argument must be a string or an array of strings.');

        $_list = array_diff($this->getPluginList (), $_plugin_handle);

        foreach ($_list as $k=>$v) {
            $v = trim($v);
            if (empty($v) || !\Cataleya\Plugins\Package::exists($v)) unset($_list[$k]);
            else $_list[$k] = $v;
        }

        $this->_data['plugin_list'] = (!empty($_list)) ?  implode(' ', $_list) : '';
        $this->_modified[] = 'plugin_list';

        return $this;

    }





	/*
	 *
	 *  [ SETTERS ]
	 * ________________________________________________________________
	 * 
	 * 
	 *
	 *
	 *
	 */
	 






        
        
        
        
        
        
        

        
        

 
 
        /**
         *
         *  
         * Will return product URL Rewrite (object)
         * 
         * @return \Cataleya\Asset\URLRewrite
         *
         */
	public function getURLRewrite () 
	{
		if (empty($this->_url_rewrite)) $this->_url_rewrite = \Cataleya\Asset\URLRewrite::load ($this->_data['url_rewrite_id']);
		return $this->_url_rewrite;
		
	}
        


	/**
	 *
	 * @return string
	 *
	 *
	 *
	 */
	 
	public function getSlug () 
	{
		
                return $this->getURLRewrite()->getKeyword();
		
	}
        
        
        
        
        

	/**
	 *
	 * @param string $keyword
	 *
	 *
	 *
	 */
	public function setSlug ($keyword = '') 
	{

		return $this->getURLRewrite()->setKeyword($keyword);
                
		
	}
        
        
        
        
        
        

	/**
	 *
	 * @return string
	 *
	 *
	 *
	 */
	 
	public function getHandle () 
	{
		
                return $this->getURLRewrite()->getKeyword();
		
	}
        
        
        
        
        

	/**
	 *
	 * @param string $keyword
	 *
	 *
	 *
	 */
	public function setHandle ($keyword = '') 
	{

		return $this->getURLRewrite()->setKeyword($keyword);
                
		
	}
        
        




        /**
         *
         *  
         * Will return url (full url)
         * 
         * @return string
         *
         */
        public function getURL()
        {

                return $this->getStore()->getHandle() . '/page/' . $this->getSlug();
        }




        /*
         *
         *  @return int Store ID
         *
         *
         */
        public function getStoreId()
        {
                return $this->_data['store_id'];
        }



        /**
         * 
         * @return \Cataleya\Store
         * 
         */
        public function getStore()
        {
            
            
                if (empty($this->_store)) $this->_store = \Cataleya\Store::load((int)$this->_data['store_id']);
                
                
                return $this->_store;
        }


        
        /**
         * 
         * @param \Cataleya\Admin\User $_AdminUser
         * @param string $title
         * @param string $content
         * @param string $language_code
         * @return \Cataleya\Front\Shop\Page\Autosave
         * 
         */
        public function autosave (\Cataleya\Admin\User $_AdminUser, $title = '', $content = '', $language_code = 'EN') {
            
            $_Autosave = $this->getAutosave($_AdminUser, TRUE);
            
            $_Autosave->saveEntry($title, $content, $language_code);
            
            return $_Autosave;
            
        }
        
        
        /**
         * 
         * @param \Cataleya\Admin\User $_AdminUser
         * @param boolean $_AUTO_CREATE
         * @return \Cataleya\Front\Shop\Page\Autosave
         * 
         */
        public function getAutosave (\Cataleya\Admin\User $_AdminUser, $_AUTO_CREATE = FALSE) {
            
            return \Cataleya\Front\Shop\Page\Autosave::load($this, $_AdminUser, $_AUTO_CREATE);
            
        }
        
        
        
        
        /**
         * 
         * @param \Cataleya\Admin\User $_AdminUser
         * @return \Cataleya\Front\Shop\Page
         * 
         */
        public function clearAutosave (\Cataleya\Admin\User $_AdminUser) {
            
            $_Autosave = $this->getAutosave($_AdminUser);
            
            if (NULL !== $_Autosave) {
                
                $_Autosave->delete();
                
            }
            
            return $this;
            
        }
        
        
        






}

