<?php



namespace Cataleya\Front\Shop;







        
        
        

/*
 *
 * 	@Package: Cataleya
 * 	@Class: \Cataleya\Front\Shop\Customer
 *
 * 	(c) 2014 Fancy Paper Planes
 *
 *
 *
 */


        
class Customer 
{
    
    

    private static $_instance;

    public function __construct() {



            
            
    }
    
    
    
    
    



    /**
     * 
     * @return \Cataleya\Customer
     * 
     */
    static public function load () 
    {

        
        if (NULL === self::$_instance && IS_LOGGED_IN === TRUE):

            self::$_instance = \Cataleya\Customer::load($_SESSION[SESSION_PREFIX.'CUSTOMER_ID']);
        
        
        endif;
        

        
        
        return self::$_instance;
        
        
    }
    
    
    
    
    

    
    
    /**
     * Private clone method to prevent cloning of the instance of the
     * *Singleton* instance.
     *
     * @return \Cataleya\Customer
     * 
     */
    static public function getInstance()
    {
        return self::load();
    } 
    
    
    
    

    
    
    /**
     * Private clone method to prevent cloning of the instance of the
     * *Singleton* instance.
     *
     * @return void
     */
    private function __clone()
    {
    }

    /**
     * Private unserialize method to prevent unserializing of the *Singleton*
     * instance.
     *
     * @return void
     */
    private function __wakeup()
    {
    }
        


    
    
    




    
      
    

        

 
}


