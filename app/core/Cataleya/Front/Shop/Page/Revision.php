<?php


namespace Cataleya\Front\Shop\Page;




/*
 *
 *	@Package: Cataleya
 *	@Class: \Cataleya\Front\Shop\Page\Revision
 *
 *	(c) 2012 Fancy Paper Planes
 *
 *
 *
 */


class Revision extends \Cataleya\Asset\Content 

{
	
	protected 
                
         $_data, 
	 $_content_languages = array(), 
	 $_modified = array(),  
	 $_content_modified = array(), 
         $_johny_jus_come = false, 
                
         $_content_type, 
         $_parent;


	protected 
			$dbh, 
			$e;
			
			
			


        const MAX_REVISIONS = 5;

        
	/**
         * 
         * @param integer $instance_id
         * @param \Cataleya\Store $_Store
         * @return \Cataleya\Front\Shop\Page\Revision
         * 
         */
	public static function load ($instance_id = 0) 
	{
		
		// construct
                $_objct = __CLASS__;
                
                $instance = new $_objct ();
		
                
                
                // arg is tag_id
		if (filter_var($instance_id, FILTER_VALIDATE_INT) !== FALSE && (int)$instance_id > 0)  {} // Do nothing
                
                // Fire burn this ID !!
                else \Cataleya\Helper\ErrorHandler::getInstance()->triggerException('Error in class (' . __CLASS__ . '): [ invalid argument (invalid id or keyword) for method: ' . __FUNCTION__ . ' ] on line ' . __LINE__);
                
		
										
		$row = 	$instance->loadDataWithID($instance_id);	
                
                
		if (empty($row))
		{ 
                    
                    
                    return NULL;
		} else {
                    $instance->_data = $row;
                }
                
                

                $instance->_loadEntries();
                
                return $instance;
		
	}
        
        
        

        
        protected function loadDataWithID ($instance_id) {

		///// CHECK IF DESCRIPTION ID IS VALID /////////////////////////
		static 
					$select2_handle, 
					$select2_handle_param_instance_id;

			
		
		if (empty($select2_handle))
		{
			$select2_handle = $this->dbh->prepare('SELECT * FROM content WHERE content_id = :instance_id LIMIT 1');
			$select2_handle->bindParam(':instance_id', $select2_handle_param_instance_id, \PDO::PARAM_INT);
		}
                
                
		
		$select2_handle_param_instance_id = $instance_id;
		
		if (!$select2_handle->execute()) $this->e->triggerException('
										Error in class (' . __CLASS__ . '): [ ' . 
										implode(', ', $select2_handle->errorInfo()) . 
										' ] on line ' . __LINE__);
										
		$row = 	$select2_handle->fetch(\PDO::FETCH_ASSOC);
                
                
                if (empty($row)) \Cataleya\Helper\ErrorHandler::getInstance()->triggerException('data could not be read...' . $instance_id . ':' . __LINE__);
                
                return $row;
                
        }





        /**
         * 
         * @param \Cataleya\Front\Shop\Page $_Page
         * @param string $title
         * @param string $text
         * @param string $language_code
         * @param boolean $_is_system
         * @return \Cataleya\Front\Shop\Page\Revision
         * 
         */
	public static function create (\Cataleya\Front\Shop\Page $_Page, $title = 'Untitled', $text = 'No description', $language_code = 'EN')  // defaults to english (EN)
	{


                
                $_params = array ( 
                    'content_type' => self::TYPE_AUTOSAVE, 
                    'parent_id' => $_Page->getID(), 
                    'store_id' => $_Page->getStoreId(), 
                    'is_system' => 0
                        );
                
                
                $_cols_str = $_vals_str = [];

                foreach ($_params as $key=>$val) {

                     If (!is_numeric($val) && !is_string($val) && !is_null($val)) { $_params[$key] = $val= ''; }
                     
                     $_cols_str[] = $key;
                     $_vals_str[] = ':'.$key;
                }
                

                // Get database handle...
                $dbh = \Cataleya\Helper\DBH::getInstance();

                // Get error handler
                $e = \Cataleya\Helper\ErrorHandler::getInstance();
                
                
		
		static $insert_handle;
		
		if (empty($insert_handle))
		{
			$insert_handle = $dbh->prepare('INSERT INTO content (date_added, last_modified, ' . implode(', ', $_cols_str) . ') VALUES (NOW(), NOW(), ' . implode(', ', $_vals_str) . ')');
		

                        
                }
                
   
                foreach ($_params as $key=>$val) {


                        $insert_handle->bindParam(
                                ':'.$key, 
                                $_params[$key], 
                                \Cataleya\Helper\DBH::getTypeConst($val)
                                );

                }
                

		if (!$insert_handle->execute()) $e->triggerException('
										Error in class (' . __CLASS__ . '): [ ' . 
										implode(', ', $insert_handle->errorInfo()) . 
										' ] on line ' . __LINE__);
										
		$_instance_id = $dbh->lastInsertId();
		$instance = self::load($_instance_id);
		
		if ($title !== NULL && $text !== NULL)
		{
		if(!is_string($language_code) || strlen($language_code) != 2) $language_code = 'EN';
		$language_code = strtoupper($language_code);
		
		$instance->saveEntry($title, $text, $language_code);
		}
		
                // $instance->setTemplateID('index.html.twig');
                $instance->_johny_jus_come = TRUE;
                
                $instance->garbageCollect();
                
		return $instance;
		
	}
        
        
        
        
        
        
        
        
        
        
        


        /**
         *
         *  @return int Page ID
         *
         *
         */
        public function getParentID()
        {
                return $this->_data['parent_id'];
        }



        /**
         * 
         * @return \Cataleya\Front\Shop\Page
         * 
         */
        public function getParent($_AUTO_LOAD = TRUE)
        {
            
            
                if (empty($this->_parent)) $this->_parent = \Cataleya\Front\Shop\Page::load((int)$this->_data['parent_id']);
                
                
                return $this->_parent;
        }



        /**
         * 
         * @return \Cataleya\Front\Shop\Page
         * 
         */
        public function getPage()
        {
            
                return $this->getParent();
        }
        
        
        
        /**
         *  Alias for getParentID()
         * 
         *  @return int Page ID
         *
         *
         */
        public function getPageID()
        {
                return $this->getParentID();
        }




        /*
         *
         *  @return int Store ID
         *
         *
         */
        public function getStoreID()
        {
                return $this->_data['store_id'];
        }



        /**
         * 
         * @return \Cataleya\Store
         * 
         */
        public function getStore()
        {
            
            
                if (empty($this->_store)) $this->_store = \Cataleya\Store::load((int)$this->_data['store_id']);
                
                
                return $this->_store;
        }



        
        
        
        


	/**
         * 
         * Deletes old autosaves...
         * 
         * @return boolean
         * 
         */
	public function garbageCollect () 
	{
            
            
            
            
            
            
            static 
            
            $select_handle, 
            $select_handle_param_parent_id, 
            $select_handle_param_content_type, 
            $select_handle_param_limit;

            if (empty($select_handle)):
                    // PREPARE SELECT STATEMENT...
                    $select_handle = $this->dbh->prepare(''
                            . 'SELECT content_id FROM content '
                            . 'WHERE content_type = :content_type '
                            . 'AND parent_id = :parent_id '
                            . 'ORDER BY date_added DESC '
                            . 'LIMIT :limit');
                    $select_handle->bindParam(':parent_id', $select_handle_param_parent_id, \PDO::PARAM_INT);
                    $select_handle->bindParam(':content_type', $select_handle_param_content_type, \PDO::PARAM_INT);
                    $select_handle->bindParam(':limit', $select_handle_param_limit, \PDO::PARAM_INT);
            endif;

            $select_handle_param_parent_id = $this->getParentID();
            $select_handle_param_content_type = self::TYPE_REVISION;
            $select_handle_param_limit = self::MAX_REVISIONS;
            



            if (!$select_handle->execute()) $this->e->triggerException('
                                    Error in class (' . __CLASS__ . '): [ ' . 
                                    implode(', ', $select_handle->errorInfo()) . 
                                    ' ] on line ' . __LINE__);
            
            
            $_siblings = array ();
            while ($row = $select_handle->fetch(\PDO::FETCH_ASSOC))
            {
                $_siblings[] = (int)$row['content_id'];
            }   
               
               
               

            static 
            
            $delete_handle, 
            $delete_handle_param_parent_id, 
            $delete_handle_param_content_type;

            if (empty($delete_handle)):
            // DELETE DESCRIPTION ....
            $delete_handle = $this->dbh->prepare(''
                                                . 'DELETE FROM content '
                                                . 'WHERE content_type = :content_type '
                                                . 'AND parent_id = :parent_id '
                                                . 'AND content_id NOT IN ('
                                                . implode(', ', $_siblings)
                                                . ')'
                                                . '');
            $delete_handle->bindParam(':parent_id', $delete_handle_param_parent_id, \PDO::PARAM_INT);
            $delete_handle->bindParam(':content_type', $delete_handle_param_content_type, \PDO::PARAM_INT);
            endif;

            $delete_handle_param_parent_id = $this->getParentID();
            $delete_handle_param_content_type = self::TYPE_REVISION;

            if (!$delete_handle->execute()) $this->e->triggerException('
                                    Error in class (' . __CLASS__ . '): [ ' . 
                                    implode(', ', $delete_handle->errorInfo()) . 
                                    ' ] on line ' . __LINE__);



            // $this = NULL;
            return TRUE;
		
		
	}
        
        
        
        
        






}

