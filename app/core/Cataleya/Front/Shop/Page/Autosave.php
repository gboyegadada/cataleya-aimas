<?php


namespace Cataleya\Front\Shop\Page;




/*
 *
 *	@Package: Cataleya
 *	@Class: \Cataleya\Front\Shop\Page\Autosave
 *
 *	(c) 2012 Fancy Paper Planes
 *
 *
 *
 */


class Autosave extends \Cataleya\Asset\Content 

{
	
	protected 
                
         $_data, 
	 $_content_languages = array(), 
	 $_modified = array(),  
	 $_content_modified = array(), 
         $_johny_jus_come = false, 
                
         $_content_type, 
         $_parent;


	protected 
			$dbh, 
			$e;
			
			
			


        const MAX_ENTRIES = 1;

        
	/**
         * 
         * @param integer $instance_id
         * @param \Cataleya\Store $_Store
         * @return \Cataleya\Front\Shop\Page\Autosave
         * 
         */
	public static function load (\Cataleya\Front\Shop\Page $_Page, \Cataleya\Admin\User $_AdminUser, $_AUTO_CREATE = FALSE) 
	{
		
		// construct
                $_objct = __CLASS__;
                
                $instance = new $_objct ();
		
              
                								
		///// CHECK IF DESCRIPTION ID IS VALID /////////////////////////
		static 
					$select2_handle, 
					$select2_handle_param_parent_id,
                                        $select2_handle_param_author_id, 
                                        $select2_handle_param_content_type;

			
		
		if (empty($select2_handle))
		{
			$select2_handle = \Cataleya\Helper\DBH::getInstance()->prepare(''
                                . 'SELECT * FROM content '
                                . 'WHERE content_type = :content_type '
                                . 'AND parent_id = :parent_id '
                                . 'AND author_id = :author_id '
                                . 'LIMIT 1');
			$select2_handle->bindParam(':parent_id', $select2_handle_param_parent_id, \PDO::PARAM_INT);
			$select2_handle->bindParam(':author_id', $select2_handle_param_author_id, \PDO::PARAM_INT);
			$select2_handle->bindParam(':content_type', $select2_handle_param_content_type, \PDO::PARAM_INT);
		}
                
                
		
		$select2_handle_param_parent_id = $_Page->getID();
                $select2_handle_param_author_id = $_AdminUser->getID();
                $select2_handle_param_content_type = self::TYPE_AUTOSAVE;
		
		if (!$select2_handle->execute()) \Cataleya\Helper\ErrorHandler::getInstance()->triggerException('
										Error in class (' . __CLASS__ . '): [ ' . 
										implode(', ', $select2_handle->errorInfo()) . 
										' ] on line ' . __LINE__);
										
		$row = 	$select2_handle->fetch(\PDO::FETCH_ASSOC);
                
                
                
		if (empty($row))
		{ 
                    
                    
                    return ($_AUTO_CREATE) ? self::create($_Page, $_AdminUser) : NULL;
                    
                    
		} else {
                    $instance->_data = $row;
                }
                
                

                $instance->_loadEntries();
                
                return $instance;
		
	}
        
        
        




        /**
         * 
         * @param \Cataleya\Front\Shop\Page $_Page
         * @param string $title
         * @param string $text
         * @param string $language_code
         * @param boolean $_is_system
         * @return \Cataleya\Front\Shop\Page\Autosave
         * 
         */
	public static function create (\Cataleya\Front\Shop\Page $_Page, \Cataleya\Admin\User $_AdminUser, $title = 'Untitled', $text = 'No description', $language_code = 'EN')  // defaults to english (EN)
	{


                
                $_params = array ( 
                    'content_type' => self::TYPE_AUTOSAVE, 
                    'parent_id' => $_Page->getID(), 
                    'author_id' => $_AdminUser->getID(), 
                    'store_id' => $_Page->getStoreId(), 
                    'is_system' => 0
                        );
                
                
                $_cols_str = $_vals_str = [];

                foreach ($_params as $key=>$val) {

                     If (!is_numeric($val) && !is_string($val) && !is_null($val)) { $_params[$key] = $val= ''; }
                     
                     $_cols_str[] = $key;
                     $_vals_str[] = ':'.$key;
                }
                

                // Get database handle...
                $dbh = \Cataleya\Helper\DBH::getInstance();

                // Get error handler
                $e = \Cataleya\Helper\ErrorHandler::getInstance();
                
                
		
		static $insert_handle;
		
		if (empty($insert_handle))
		{
			$insert_handle = $dbh->prepare('INSERT INTO content (date_added, last_modified, ' . implode(', ', $_cols_str) . ') VALUES (NOW(), NOW(), ' . implode(', ', $_vals_str) . ')');
		

                        
                }
                
   
                foreach ($_params as $key=>$val) {


                        $insert_handle->bindParam(
                                ':'.$key, 
                                $_params[$key], 
                                \Cataleya\Helper\DBH::getTypeConst($val)
                                );

                }
                

		if (!$insert_handle->execute()) $e->triggerException('
										Error in class (' . __CLASS__ . '): [ ' . 
										implode(', ', $insert_handle->errorInfo()) . 
										' ] on line ' . __LINE__);
										
		$_instance_id = $dbh->lastInsertId();
		$instance = self::load($_Page, $_AdminUser);
		
		if ($title !== NULL && $text !== NULL)
		{
		if(!is_string($language_code) || strlen($language_code) != 2) $language_code = 'EN';
		$language_code = strtoupper($language_code);
		
		$instance->saveEntry($title, $text, $language_code);
		}
		
                // $instance->setTemplateID('index.html.twig');
                $instance->_johny_jus_come = TRUE;
                
                
		return $instance;
		
	}
        
        
        
        
        
        
        
        
        
        
        


        /**
         *
         *  @return int Page ID
         *
         *
         */
        public function getParentID()
        {
                return $this->_data['parent_id'];
        }



        /**
         * 
         * @return \Cataleya\Front\Shop\Page
         * 
         */
        public function getParent($_AUTO_LOAD = TRUE)
        {
            
            
                if (empty($this->_parent)) $this->_parent = \Cataleya\Front\Shop\Page::load((int)$this->_data['parent_id']);
                
                
                return $this->_parent;
        }



        /**
         * 
         * @return \Cataleya\Front\Shop\Page
         * 
         */
        public function getPage()
        {
            
                return $this->getParent();
        }
        
        
        
        /**
         *  Alias for getParentID()
         * 
         *  @return int Page ID
         *
         *
         */
        public function getPageID()
        {
                return $this->getParentID();
        }




        /*
         *
         *  @return int Store ID
         *
         *
         */
        public function getStoreID()
        {
                return $this->_data['store_id'];
        }



        /**
         * 
         * @return \Cataleya\Store
         * 
         */
        public function getStore()
        {
            
            
                if (empty($this->_store)) $this->_store = \Cataleya\Store::load((int)$this->_data['store_id']);
                
                
                return $this->_store;
        }



        
        






}

