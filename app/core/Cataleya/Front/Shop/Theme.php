<?php



namespace Cataleya\Front\Shop;







        
        
        

/*
 *
 * 	@Package: Cataleya
 * 	@Class: \Cataleya\Front\Shop\Theme
 *
 * 	(c) 2014 Fancy Paper Planes
 *
 *
 *
 */


        
class Theme
{
    
    


    protected 
            $_themes_root, 
            $_theme_path, 
            $_assets_web_path, 
            $_theme_id, 
            $_config;







    public function __construct() {

        $this->_themes_root = ROOT_PATH.'skin/ShopFront/';
        
    }
    
    



    /**
     *
     * Store Contructor. Always use instead of 'new' !!
     *
     * @params string $id Theme ID
     * 
     * @return \Cataleya\Front\Shop\Theme (an instance of store)
     *
     *
     */
    static public function load ($_id = '') 
    {
        
        $_themes_root = ROOT_PATH.'skin/ShopFront/';
        $_theme_path = $_themes_root . $_id;

        if (!file_exists($_theme_path)) return NULL;


        if (
                !is_dir($_theme_path) 
                || !file_exists($_theme_path . '/meta.json') 
                ) return NULL;


        $_objct = __CLASS__;
        $instance = new $_objct;

        // Initialize properties
        $instance->_config = json_decode(file_get_contents($_theme_path . '/meta.json'), TRUE);
        $instance->_themes_root = $_themes_root;
        $instance->_theme_path = $_theme_path;
        $instance->_assets_web_path = BASE_URL . 'skin/ShopFront/' . $_id . '/assets/';
        $instance->_theme_id = $_id;

        
        return $instance;
    }

    
    
    public function __get($name) {
        return (isset ($this->_config[$name])) ? $this->_config[$name] : NULL;
    }
    
    
    public function __set($name, $value) {
        if (isset ($this->_config[$name])) $this->_config[$name] = $value;
        
        return $this;
    }

    
    
    
    

    /*
     * 
     * [ getID ]
     * ______________________________________________________________
     * 
     * 
     * 
     * 
     */


    public function getID () 
    {
        return $this->_theme_id;
    }
    
    

    /*
     * 
     * [ getRootPath ]
     * ______________________________________________________________
     * 
     * 
     * 
     * 
     */


    public function getFolderPath () 
    {
        return $this->_theme_path;
    }
    
    
    
    
    

    /*
     * 
     * [ getAssetsPath ]
     * ______________________________________________________________
     * 
     * 
     * 
     * 
     */


    public function getAssetsPath () 
    {
        return $this->_assets_web_path;
    }
    




    /*
     * 
     * [ getName ]
     * ______________________________________________________________
     * 
     * 
     * 
     * 
     */


    public function getName () 
    {
        return $this->__get('name');
    }
    



    /*
     * 
     * [ getVersion ]
     * ______________________________________________________________
     * 
     * 
     * 
     * 
     */


    public function getVersion () 
    {
        return $this->__get('version');
    }
      
    
    
    


    /*
     * 
     * [ getPreview ]
     * ______________________________________________________________
     * 
     * 
     * 
     * 
     */


    public function getPreview () 
    {
        $_path = $this->__get('preview');
        
        if (
                !empty($_path) 
                && is_string($_path) 
                && file_exists($this->_theme_path . '/' . $_path)
                ) return $this->_theme_path . '/' . $_path;
        else return NULL;
    }
    
    
    
    
    
    /*
     * 
     * @return array $_list 
     * 
     * 
     * 
     */


    public function getPageTemplates () 
    {
        $_list = array ();
        
        if (!file_exists($this->_theme_path . '/layouts/pages')) return $_list;
        
        foreach (new \DirectoryIterator($this->_theme_path . '/layouts/pages') as $fileInfo) {
            if($fileInfo->isDot() || $fileInfo->isDir() || !$fileInfo->isReadable()) continue;
            
            $filename = $fileInfo->getFilename();

            if (preg_match('/(.html.twig)$/i', $filename) === 0) continue; 
                    
            $_list[] = $filename;
        }
        
        return $_list;
    }

        

 
}


