<?php



namespace Cataleya\Front\Shop;







        
        
        

/*
 *
 * 	@Package: Cataleya
 * 	@Class: \Cataleya\Front\Shop\MyCart
 *
 * 	(c) 2014 Fancy Paper Planes
 *
 *
 *
 */


        
class MyCart 
{
    
    

    private static $_instance;

    public function __construct() {



            
            
    }
    
    
    
    
    



    /**
     * 
     * @return \Cataleya\Customer\Cart
     * 
     */
    static public function load () 
    {

        
        if (NULL === self::$_instance):

            self::$_instance = \Cataleya\Customer\Cart::load(\Cataleya\Front\Shop::getInstance());
        
        
        endif;
        

        
        
        return self::$_instance;
        
        
    }
    
    
    

    /*
     *
     *  [ delete ]
     * ________________________________________________________________
     * 
     * @return boolean
     *
     *
     *
     */

    static public function delete () 
    {

        
        
        if (!empty(self::$_instance)):

            self::$_instance->delete();
            self::$_instance = NULL;
        
        
        endif;
        

        
        
        return TRUE;
        
        
    }
    
    

    
    
    /**
     * Private clone method to prevent cloning of the instance of the
     * *Singleton* instance.
     *
     * @return *Singleton* instance of \Cataleya\Customer\Cart
     */
    static public function getInstance()
    {
        return self::load();
    } 
    
    
    
    

    
    
    /**
     * Private clone method to prevent cloning of the instance of the
     * *Singleton* instance.
     *
     * @return void
     */
    private function __clone()
    {
    }

    /**
     * Private unserialize method to prevent unserializing of the *Singleton*
     * instance.
     *
     * @return void
     */
    private function __wakeup()
    {
    }
        


    
    
    




    
      
    

        

 
}


