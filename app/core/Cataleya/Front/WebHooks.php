<?php



namespace Cataleya\Front;




/*
 *
 * 	@Package: Cataleya
 * 	@Class: \Cataleya\Front\WebHooks
 *
 * 	(c) 2012 Fancy Paper Planes
 *
 *
 *
 */

class WebHooks extends \Cataleya\Collection {
   
    // To be used by [\Cataleya\Collection]
    protected $_collection = array ();
    protected $_position = 0;


    public function __construct() {
       
        // Get database handle...
        $this->dbh = \Cataleya\Helper\DBH::getInstance();
		
        // Get error handler
        $this->e = \Cataleya\Helper\ErrorHandler::getInstance();
        

        
        parent::__construct();
 
        
    }
    
    
    
    


	/*
	 *
	 * [ load ]
	 * ________________________________________________________________
	 *
	 *
	 *
	 */
	 
	 
	public static function load ($sort = array('order_by'=>'', 'order'=>'', 'index'=>''), $page_size = 100, $page_num = 1)
	{
            
            
                $_objct = __CLASS__;
                $instance = new $_objct;
                
                

                // This is used to determine which column is to be used in the index...
                // $_index_with = 'title';

                if (is_array($sort) && !empty($sort['order_by']) && !empty($sort['order']) && is_int($sort['order_by']) && is_int($sort['order']) )
                {


                    // ASC | DESC
                    switch ($sort['order']) {

                        case self::ORDER_ASC:
                            $param_order = 'ASC';
                            break;

                        case self::ORDER_DESC:
                            $param_order = 'DESC';
                            break;

                        default :
                            $param_order = 'ASC';
                            break;
                    }  


                    // ORDER BY
                    switch ($sort['order_by']) {

                        case self::ORDER_BY_NAME:
                            $param_order_by = "title $param_order";
                            $_index_with = 'title';
                            break;

                        case self::ORDER_BY_ID:
                            $param_order_by = "hook_id $param_order";
                            $_index_with = 'hook_id';
                            break;
                        
                        case self::ORDER_BY_CREATED:
                            $param_order_by = "date_added $param_order";
                            $_index_with = 'date_added';
                            break;
                        
                        case self::ORDER_BY_MODIFIED:
                            $param_order_by = "last_modified $param_order";
                            $_index_with = 'last_modified';
                            break;

                        default :
                            $param_order_by = "title $param_order";
                            $_index_with = 'title';
                            break;
                    }





                }

                // Defaults                
                else {

                        $param_order = 'ASC';
                        $param_order_by = "title $param_order";
                        $_index_with = 'title';

                }    



                // Check if index is specified
                if (is_array($sort) && isset($sort['index']) && is_string($sort['index']))
                {
                    $param_regex = (preg_match('/^[a-zA-Z]{1}$/', $sort['index']) > 0) ? '^[' . strtolower($sort['index']) . '].*' : '.*';
                }

                else {
                    $param_regex = '.*';
                }




                $param_offset = 0;
                $param_limit = 99999999;
        
        


                // PREPARE SELECT STATEMENT...
                $select_handle = $instance->dbh->prepare('
                                                SELECT hook_id   
                                                FROM web_hooks a 
                                                INNER JOIN content_languages b 
                                                ON a.description_id = b.content_id 
                                                WHERE ' . $_index_with . ' REGEXP :regex 
                                                ORDER BY '. $param_order_by . '   
                                                LIMIT :offset, :limit
                                                ');

                $select_handle->bindParam(':offset', $param_offset, \PDO::PARAM_INT);
                $select_handle->bindParam(':limit', $param_limit, \PDO::PARAM_INT);
                $select_handle->bindParam(':regex', $param_regex, \PDO::PARAM_STR);
                

                if (!$select_handle->execute()) $instance->e->triggerException('
                                                                                                Error in class (' . __CLASS__ . '): [ ' . 
                                                                                                implode(', ', $select_handle->errorInfo()) . 
                                                                                                ' ] on line ' . __LINE__);
                while ($row = $select_handle->fetch(\PDO::FETCH_ASSOC))
                {
                    $instance->_collection[] = $row['hook_id'];
                }

                
                $instance->pageSetup($page_size, $page_num);
                
                return $instance;

            
        }
    
    
    
    
    
    public function current()
    {
        return \Cataleya\Locale\Currency::load($this->_collection[$this->_position]);
        
    }



        
        
        
    
}


