<?php



namespace Cataleya\Front;







        
        
        

/*
 *
 * 	@Package: Cataleya
 * 	@Class: \Cataleya\Front\Shop
 *
 * 	(c) 2014 Fancy Paper Planes
 *
 *
 *
 */


        
class Shop 
{
    
    



    public function __construct() {



            
            
    }
    
    
    
    
    





    /**
     *
     *  
     * Will return singuleton instance of \Cataleya\Store
     * 
     * @return \Cataleya\Store
     *
     */
    static public function load () 
    {

        static $_instance = NULL;
        
        
        if (NULL === $_instance):

        
            $store_key = \Cataleya\Front\Shop\Controller::getStoreKeyword();
        

            if ($store_key !== FALSE) $_instance = \Cataleya\Stores::load()->getStoreByKeyword($store_key);



            // If we fail to load from keyword (in url), we'll check the cookie
            if ($_instance === NULL && isset($_COOKIE['_aimas03'])) {
                $store_pref_id = filter_var($_COOKIE['_aimas03'], FILTER_VALIDATE_INT);

                
                // ------------- Uncomment next line to auto-navigate to previously selected shop front. ------- //
                
                // if ($store_pref_id !== FALSE) $_instance = \Cataleya\Store::load((int)$store_pref_id);
                
                

                if (!empty($_instance) && OUTPUT === 'HTML') {

                    $url = SHOP_ROOT . $_instance->getURLRewrite()->getKeyword();
                    Header("Location: $url");
                    exit();
                }

        

            } 
            
        
        endif;
        
        
        
        


        if (NULL !== $_instance) {
            // Save prefered store in cookie...      
            setcookie(
                '_aimas03', // Name
                $_instance->getStoreId(), // Value
                COOKIE_EXPIRES, // Expires
                COOKIE_PATH, // Path
                COOKIE_DOMAIN, // Domain
                COOKIE_SECURE, // Is secure
                TRUE // HTTP Only
                );

                // DEFINE SHOP_LANDING ONLY AFTER A STORE IS SELECTED
                // define('SHOP_LANDING', (SHOP_ROOT . $_instance->getURLRewrite()->getKeyword() . '/'));


                // Define a few useful constants
                if (!defined('STORE_LANG')) { define ('STORE_LANG', $_instance->getLanguageCode()); }
                
        } else {
            
                if (!defined('STORE_LANG')) { define ('STORE_LANG', \Cataleya\System::load()->getShopFrontLanguageCode()); }

        }

        

        
        
        return $_instance;
        
        
    }
    
    
    
    

    
    
    /**
     * Private clone method to prevent cloning of the instance of the
     * *Singleton* instance.
     *
     * @return *Singleton* instance of \Cataleya\Store
     */
    static public function getInstance()
    {
        return self::load();
    } 
    
    
    
    

    
    
    /**
     * Private clone method to prevent cloning of the instance of the
     * *Singleton* instance.
     *
     * @return void
     */
    private function __clone()
    {
    }

    /**
     * Private unserialize method to prevent unserializing of the *Singleton*
     * instance.
     *
     * @return void
     */
    private function __wakeup()
    {
    }
        


    
    
    




    
      
    

        

 
}


