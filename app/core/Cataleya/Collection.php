<?php


namespace Cataleya;



/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Collection
 *
 * @author Gboyega Dada for Fancy Paper Planes.
 */
abstract class Collection implements \Iterator {
    
    // CONST VALUES 1 - 15 RESERVED FOR [ _Abstract_Collection ]
    
    const ORDER_ASC = 1;
    const ORDER_DESC = 2; 
 
    const ORDER_BY_SORT = 3;
    const ORDER_BY_CREATED = 4;
    const ORDER_BY_MODIFIED = 5;
    
   
    protected $_page_start = 0;
    protected $_page_stop = 0;
    
    protected $_page_count = 1;
    protected $_page_num = 1;
    
    protected $_page_size = 1000;

    public function __construct($page_size = 1000, $page_num = 1) {
        $this->pageSetup($page_size, $page_num);
    }

    public function rewind() {

        $this->_position = $this->_page_start;
    }

    abstract function current();

    public function key() {
        return $this->_position;
    }

    public function next() {
        ++$this->_position;
    }

    public function valid() {
        
        return (isset($this->_collection[$this->_position]) && $this->_position < $this->_page_stop) ? TRUE : FALSE;
    }
    
    
    
    /*
     * 
     * PAGINATION METHODS
     * 
     * 
     * 
     */
    
    public function pageSetup($page_size = 100, $page_num = 1) {
        
        
        if (is_numeric($page_size)) $page_size = (int)$page_size;
        else $page_size = 100;
        
        $_population = count($this->_collection);
        
        $this->_page_size = ($page_size < 1) ? $_population : $page_size;
        
        // page count...
        $this->_page_count = ($_population > 0) ? ceil($_population / $this->_page_size) : 1;
        
        // current page...
        $this->gotoPage($page_num);
        

        // rewind
        $this->rewind();
        
        return $this;
        
        
    }
    
    protected function gotoPage ($page_num = 1)
    {
        if (is_numeric($page_num) && $page_num >= 1) $page_num = (int)$page_num;
        else $page_num = 1;
        
        // current page...
        $this->_page_num = ($page_num > $this->_page_count) ? $this->_page_count : $page_num;  
        
        // page_start
        $this->_page_start = ($this->_page_num - 1) * $this->_page_size;
        
        $_population = count($this->_collection);
        $_new_page_stop = $this->_page_start + $this->_page_size;
        
        // page stop
        $this->_page_stop = ($_new_page_stop > $_population) ? $_population : $_new_page_stop;
        
                
    }


    public function getPage($page_num = 1) {
        $this->gotoPage($page_num);
        
        $items = array ();
        
        foreach ($this as $item) $items[] = $item;
        
        return $items;
        
    }
    
    
    public function getPageSize() {
        return $this->_page_size;
    }

    
    public function getLastPage() {
        return $this->getLastPage($this->_page_count);
    }
    
    
    public function getLastPageNum() {
        return $this->_page_count;
    }
 
    public function getPageNumber() {
        return $this->_page_num;
    }

    public function getNextPageNum() {
        
        $_next_page = $this->_page_num + 1;
        return ($_next_page > $this->_page_count) ? 0 : $_next_page;
    }
 
    
    public function getPrevPageNum() {
        
        $_prev_page = $this->_page_num - 1;
        return ($_prev_page < 1) ? 0 : $_prev_page;
    }
    
    
    
    public function getPageStart() {
        return $this->_page_start + 1;
    }
    
    
    
    public function getPageStop() {
        $_population = count($this->_collection);
        return $this->_page_stop; // (($this->_page_stop+1) > $_population) ? $_population : $this->_page_stop;
    }
    
    
    
    public function getPageNumbers($offset = 1, $limit = NULL) {
        
        $page_numbers = array ();
        
        $limit = ($limit === NULL || !is_numeric($limit)) ? $this->_page_count : (int)$limit;
        $offset = (!is_numeric($offset) || (int)$offset > $this->_page_count) ? 0 : (int)$offset-1;
        
        $page_count = $this->_page_count;
        $page_num = $this->_page_num;

        if (($page_num+$limit) >= $page_count) 
        $offset = (($page_count-($limit+1)) < 0) ? 0 : $page_count-($limit+1);
        else 
        $offset = $page_num;



        for ($i=$offset; $i<$page_count; $i++) {
                if ($i > $page_num+$limit) break;

               $page_numbers[] = $i+1;
        }    
        
        
        return $page_numbers;
    }
    
    

    public function getIndex($offset, $limit) {
        return $this->getPageNumbers($offset, $limit);
    }
    
    
    public function getPopulation() {
        return count($this->_collection);
    }
    
    

    public function getActualPageSize() {
        return count($this->_collection);
    }
    
    
    
}




