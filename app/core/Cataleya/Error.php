<?php


namespace Cataleya;



/*

CLASS ERROR_HANDLER



* Native PHP Error Logger *

	bool error_log ( string $message [, int $message_type = 0 [, string $destination [, string $extra_headers ]]] )
	message: The error message that should be logged.
	message_type:
					[0] -  message is sent to PHP's system logger, using the Operating System's 
					system logging mechanism or a file, depending on what the error_log configuration 
					directive is set to. This is the default option.
					
					[1] -  message is sent by email to the address in the destination parameter. 
					This is the only message type where the fourth parameter, extra_headers is used.
					
					[2]	-	No longer an option.
					
					[3]	-	message is appended to the file destination. A newline is not automatically 
					added to the end of the message string.
					
					[4]	-	message is sent directly to the SAPI logging handler.

*/



/**
 * Error    This is to help us capture useful information about the Exception (and saving to DB) 
 *          before the script is killed.
 *
 * @package Cataleya 
 * @author Gboyega Dada <gboyega@fpplabs.com>
 * @version $Id$
 */
class Error 
extends \Exception 
implements \Cataleya\System\Event\Observable  
{
	

    protected static $_events = [
        'system/error' 


    ];

    // Error codes...
    const CODE_INVALID_ARGUMENT = 8;
    const CODE_PERMISSION_DENIED = 16;
    const CODE_LOGIN_REQUIRED = 32;

    /**
     * __construct
     *
     * @param mixed $message
     * @param int $code
     * @param Exception $previous
     * @return void
     */
    public function __construct ($message, $code = 0, \Exception $previous = null) 
	{
        if (defined('DEBUG_MODE') && DEBUG_MODE === true) {
            $_message = strval($message);
            $_code = intval($code);
        } else {
            $_message = 'Houston! We have a problem!';
            $_code = 0;
        }

	    parent::__construct($_message, $_code, $previous);
		
        // Roll back any DB tranx
        $_db = \Cataleya\Helper\DBH::getInstance();
        if (!empty($_db)) {
            
            $_db->rollback();

            \Cataleya\System\Event::notify(
                   'system/error', 
                    $this, 
                    array(
                        'scope'=>'system/error', 
                        'logger_id'=>'error.log', 
                        'blob'=> 'Code:' . $code . ' - ' .  str_replace(array("\n", "\r"), " ", $message)
                    ));
        }
		
		
	}
	

        
    
    /**
     * getEvents
     * 
     * @return array
     */
    public static function getEvents() {

        return self::$_events;
    }
    
        
	
	
	
}
	
	
	
	
