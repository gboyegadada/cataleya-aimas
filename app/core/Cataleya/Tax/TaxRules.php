<?php



namespace Cataleya\Tax;



/*
 *
 * 	@Package: Cataleya
 * 	@Class: \Cataleya\Tax\TaxRules
 *
 * 	(c) 2012 Fancy Paper Planes
 *
 *
 *
 */

class TaxRules extends \Cataleya\Collection {
    
    
    // To be used by [\Cataleya\Collection]
    protected $_collection = array ();
    protected $_position = 0;
    protected $_type = '';




    // CONST VALUES 1 - 15 RESERVED FOR [ _Abstract_Collection ]
    const ORDER_BY_TITLE = 16;
    const ORDER_BY_RATE = 17;
    
    const TYPE_SHIPPING_TAX = 'shipping';
    const TYPE_PRODUCT_TAX = 'product';


    public function __construct() 
    {
       
        // Get database handle...
        $this->dbh = \Cataleya\Helper\DBH::getInstance();
		
        // Get error handler
        $this->e = \Cataleya\Helper\ErrorHandler::getInstance();
 
        
    }

    
    
    

    public static function load($type = self::TYPE_PRODUCT_TAX, $sort = array('order_by'=>'', 'order'=>''), $page_size = 100, $page_num = 1) 
    {
	if (!in_array($type, array('product', 'shipping'))) \Cataleya\Helper\ErrorHandler::getInstance()->triggerException('Error in class (' . __CLASS__ . '): [ invalid Tax Rule Type: ' . __FUNCTION__ . ' ] on line ' . __LINE__);

        $_instance = new \Cataleya\Tax\TaxRules ();

        // LOAD
        
            
            
        // TYPE
        switch ($type) {

            case self::TYPE_PRODUCT_TAX:
                $param_type = 'product';
                break;

            case self::TYPE_SHIPPING_TAX:
                $param_type = 'shipping';
                break;

            default:
                $param_type = 'product';
                break;
        }  
        
        $_instance->_type = $param_type;


        if (is_array($sort) && !empty($sort['order_by']) && !empty($sort['order']) && is_int($sort['order_by']) && is_int($sort['order']) )
        {
            

            

            // ASC | DESC
            switch ($sort['order']) {

                case self::ORDER_ASC:
                    $param_order = 'ASC';
                    break;

                case self::ORDER_DESC:
                    $param_order = 'DESC';
                    break;

                default :
                    $param_order = 'ASC';
                    break;
            }  
            
            
            // ORDER BY
            switch ($sort['order_by']) {

                case self::ORDER_BY_SORT:
                    $param_order_by = "a.tax_priority $param_order";
                    break;

                case self::ORDER_BY_TITLE:
                    $param_order_by = "b.title $param_order";
                    break;

                case self::ORDER_BY_RATE:
                    $param_order_by = "a.tax_rate $param_order";
                    break;
                
                case self::ORDER_BY_CREATED:
                    $param_order_by = "a.date_added $param_order";
                    break;

                case self::ORDER_BY_MODIFIED:
                    $param_order_by = "a.last_modified $param_order";
                    break;

                default :
                    $param_order_by = "a.tax_priority $param_order";
                    break;
            }

                  


        }

        // Defaults                
        else {

                $param_order = 'ASC';
                $param_order_by = "a.tax_priority $param_order";

        }    
        
        
        
        
        


        $param_offset = 0;
        $param_limit = 9999999;     
        

        
        // PREPARE SELECT STATEMENT...
        $select_handle = $_instance->dbh->prepare('
                                        SELECT tax_rule_id 
                                        FROM tax_rules a 
                                        INNER JOIN content_languages b 
                                        ON a.description_id = b.content_id 
                                        WHERE a.type = :type 
                                        ORDER BY '. $param_order_by . '   
                                        LIMIT :offset, :limit
                                        ');

        $select_handle->bindParam(':type', $param_type, \PDO::PARAM_STR);
        $select_handle->bindParam(':offset', $param_offset, \PDO::PARAM_INT);
        $select_handle->bindParam(':limit', $param_limit, \PDO::PARAM_INT);



        if (!$select_handle->execute()) $_instance->e->triggerException('
                                                                        Error in class (' . __CLASS__ . '): [ ' . 
                                                                        implode(', ', $select_handle->errorInfo()) . 
                                                                        ' ] on line ' . __LINE__);
        while ($row = $select_handle->fetch(\PDO::FETCH_ASSOC))
        {
            $_instance->_collection[] = $row['tax_rule_id'];
        }
        
        
        $_instance->pageSetup($page_size, $page_num);
        
        return $_instance;
 
        
    }

    
    
    
    
    public function current()
    {

        return \Cataleya\Tax\TaxRule::load($this->_collection[$this->_position]);
        
    }
    
    
    public function getType ()
    {
        return $this->_type;
    }
    
    
}


