<?php


namespace Cataleya\Tax;



/*

CLASS TAX_CLASS

*/



class TaxRule {
    
    
	private $_data = array();
	private $_modified = array();
        private $_description, $_zone;


        private $dbh, $e;
	

	


	/*
	 *
	 * [ __construct ]
	 * ________________________________________________________________
	 *
	 *
	 *
	 */
	 
	 
	public function __construct ()
	{
		
		// Get database handle...
                $this->dbh = \Cataleya\Helper\DBH::getInstance();
		
		// Get error handler
		$this->e = \Cataleya\Helper\ErrorHandler::getInstance();
		
	}
	




	/*
	 *
	 * [ __destruct ]
	 * ________________________________________________________________
	 *
	 *
	 *
	 */

        public function __destruct()
        {
                    $this->saveData();
        }
	




	/*
	 *
	 * [ load ]
	 * ________________________________________________________________
	 *
	 *
	 *
	 */
	 
	 
	public static function load ($id = 0)
	{
		
                if ($id == 0 || filter_var($id, FILTER_VALIDATE_INT) == FALSE) return NULL;
		
		$instance = new \Cataleya\Tax\TaxRule ();
		
		// LOAD: CURRENCY
		static $select_handle, $param_instance_id;
		
			
		if (empty($select_handle)) {
			// PREPARE SELECT STATEMENT...
			$select_handle = $instance->dbh->prepare('SELECT * FROM tax_rules WHERE tax_rule_id = :instance_id LIMIT 1');
			$select_handle->bindParam(':instance_id', $param_instance_id, \PDO::PARAM_INT);
		}
		
		
		$param_instance_id = $id;
		
		if (!$select_handle->execute()) $instance->e->triggerException('
												Error in class (' . __CLASS__ . '): [ ' . 
												implode(', ', $select_handle->errorInfo()) . 
												' ] on line ' . __LINE__);
		
		$instance->_data = $select_handle->fetch(\PDO::FETCH_ASSOC);
                
		if (empty($instance->_data))
		{
			unset($instance);
			return NULL;
		}
                
                // Load description...
                $instance->getDescription();
                
                // Load Tax Zone
                $instance->getTaxZone();
                


		return $instance;	
		
		
	}



	/*
	 *
	 *  [ create ]
	 * ________________________________________________________________
	 * 
	 * 
	 *
	 *
	 */

	 
	static public function create (\Cataleya\Geo\Zone $_tax_zone, $_type = \Cataleya\Tax\TaxRules::TYPE_PRODUCT_TAX, $_rate = 0, $_title = '') 
	{	
                $_rate = \Cataleya\Helper\Validator::float($_rate, 0, 100);
                $_title = \Cataleya\Helper\Validator::name($_title, 1, 200);
                
		if ($_rate === FALSE || $_title === FALSE || !in_array($_type, array('product', 'shipping'))) \Cataleya\Helper\ErrorHandler::getInstance()->triggerException('Error in class (' . __CLASS__ . '): [ invalid argument: ' . __FUNCTION__ . ' ] on line ' . __LINE__);
                
                
                $_description = \Cataleya\Asset\Description::create($_title);
                
                
		// Get database handle...
                $dbh = \Cataleya\Helper\DBH::getInstance();
		
		// Get error handler
		$e = \Cataleya\Helper\ErrorHandler::getInstance();
		
		// construct
		static 
				$insert_handle,  
                                $insert_handle_param_type, 
                                $insert_handle_param_rate,
                                $insert_handle_param_tax_zone_id, 
                                $insert_handle_param_description_id;
		
		if (empty($insert_handle))
		{
			$insert_handle = $dbh->prepare('
                                                        INSERT INTO tax_rules 
                                                        (type, tax_zone_id, tax_rate, description_id, last_modified, date_added) 
                                                        VALUES (:type, :tax_zone_id, :tax_rate, :description_id, NOW(), NOW())
                                                        ');
															
			$insert_handle->bindParam(':type', $insert_handle_param_type, \PDO::PARAM_STR);
                        $insert_handle->bindParam(':tax_rate', $insert_handle_param_rate, \PDO::PARAM_STR);
			$insert_handle->bindParam(':tax_zone_id', $insert_handle_param_tax_zone_id, \PDO::PARAM_INT);
			$insert_handle->bindParam(':description_id', $insert_handle_param_description_id, \PDO::PARAM_INT);
		}
		
                $insert_handle_param_type = $_type;
                $insert_handle_param_rate = $_rate;
                $insert_handle_param_tax_zone_id = $_tax_zone->getID();
                $insert_handle_param_description_id = $_description->getID();
		
		if (!$insert_handle->execute()) $e->triggerException('
										Error in class (' . __CLASS__ . '): [ ' . 
										implode(', ', $insert_handle->errorInfo()) . 
										' ] on line ' . __LINE__);
										
		
		// AUTOLOAD NEW PRODUCT AND RETURN IT
		$instance = self::load($dbh->lastInsertId());
		
		return $instance;
		
		
	}


	 
	 /*
	 *  [ delete ]
	 * ________________________________________________________________
	 * 
	 * 
	 *
	 *
	 */

	 
	public function delete () 
	{
                        
                        
		
			// DELETE 
			$delete_handle_param_instance_id = $this->_data['tax_rule_id'];
			$delete_handle = $this->dbh->prepare('
                                                                DELETE FROM tax_rules 
                                                                WHERE tax_rule_id = :instance_id
                                                                ');
			$delete_handle->bindParam(':instance_id', $delete_handle_param_instance_id, \PDO::PARAM_INT);
			
	
			if (!$delete_handle->execute()) $this->e->triggerException('
											Error in class (' . __CLASS__ . '): [ ' . 
											implode(', ', $delete_handle->errorInfo()) . 
											' ] on line ' . __LINE__);
			
                        // Cleanup...
			$this->getDescription()->delete();

		
		return TRUE;
		
		
	}

        
        
        




        /*
         *
         * [ getID ] 
         *_____________________________________________________
         *
         *
         */

        public function getID()
        {
                return $this->_data['tax_rule_id'];
        }
        
        

        
        
        
        /*
         * 
         * [ getDescription ]
         * 
         * 
         */
        
        
        public function getDescription () 
        {
            if (empty($this->_description)) $this->_description = \Cataleya\Asset\Description::load($this->_data['description_id']);
            
            return $this->_description;
        }

        
        
        
        
        





        public function saveData () 
	{
		

		if (empty($this->_modified)) return;
                $this->_modified = array_unique($this->_modified);
                
		$_update_params = array();
		$key_val_pairs = array();
		
		foreach ($this->_modified as $key) $key_val_pairs[$key] = $key . ' = :' . $key;
		
	
		// Do prepared statement for 'INSERT'...
		$update_handle = $this->dbh->prepare('
                                                        UPDATE tax_rules 
                                                        SET ' . implode (', ', $key_val_pairs) . ', last_modified=now()       
                                                        WHERE tax_rule_id = :instance_id 
                                                        ');
                
		$update_handle->bindParam(':instance_id', $_update_params['tax_rule_id'], \PDO::PARAM_INT);
                $_update_params['tax_rule_id'] = $this->_data['tax_rule_id'];
                

		foreach ($this->_modified as $key) {
                    
                        $_update_params[$key] = $this->_data[$key];                        
			$update_handle->bindParam(':'.$key, $_update_params[$key], \Cataleya\Helper\DBH::getTypeConst($_update_params[$key]));

		}
		
		

		
		if (!$update_handle->execute()) $this->e->triggerException('Error in class (' . __CLASS__ . '): [ ' . implode(', ', $update_handle->errorInfo()) . ' ] on line ' . __LINE__);
		
	}
	
	
       
 
 
        



        /*
         * 
         * [ getTaxZone ]
         * ________________________________________________
         * 
         * 
         */

        public function getTaxZone () 
        {
                                
             if (empty($this->_zone)) $this->_zone = \Cataleya\Geo\Zone::load($this->_data['tax_zone_id']);
            
            return $this->_zone;
        }

        
        
        
      



        /*
         * 
         * [ getType ]
         * ________________________________________________
         * 
         * 
         */

        public function getType () 
        {
            return $this->_data['type'];
        }



        




        /*
         *
         * [ getTaxRuleId ] 
         *_____________________________________________________
         *
         *
         */

        public function getTaxRuleId()
        {
                return $this->_data['tax_rule_id'];
        }







        /*
         *
         * [ setTaxZone ] 
         *_____________________________________________________
         *
         *
         */

        public function setTaxZone(\Cataleya\Geo\Zone $_zone)
        {
                $this->_data['tax_zone_id'] = $_zone->getID();
                $this->_modified[] = 'tax_zone_id';

                return $this;
        }







        /*
         *
         * [ getTaxPriority ] 
         *_____________________________________________________
         *
         *
         */

        public function getTaxPriority()
        {
                return (int)$this->_data['tax_priority'];
        }



        /*
         *
         * [ setTaxPriority ] 
         *_____________________________________________________
         *
         *
         */

        public function setTaxPriority($value = NULL)
        {
                $value = \Cataleya\Helper\Validator::int($value, 0, 4000);
                if ($value === NULL) $this->e->triggerException('Error in class (' . __CLASS__ . '): [ invalid argument for method: ' . __FUNCTION__ . ' ] on line ' . __LINE__);


                $this->_data['tax_priority'] = $value;
                $this->_modified[] = 'tax_priority';

                return $this;
        }



        /*
         *
         * [ getRate ] 
         *_____________________________________________________
         *
         *
         */

        public function getRate()
        {
                return round((float)$this->_data['tax_rate'], 2);
        }



        /*
         *
         * [ setRate ] 
         *_____________________________________________________
         *
         *
         */

        public function setRate($value = NULL)
        {
            
                $value = \Cataleya\Helper\Validator::float($value, 0, 100);
                if ($value === NULL) $this->e->triggerException('Error in class (' . __CLASS__ . '): [ invalid argument for method: ' . __FUNCTION__ . ' ] on line ' . __LINE__);

                $this->_data['tax_rate'] = (float)$value;
                $this->_modified[] = 'tax_rate';

                return $this;
        }
        
     
        
        
        
        

        /*
         *
         * [ calculateTaxAmount ] 
         *_____________________________________________________
         *
         *
         */

        public function calculateTaxAmount(\Cataleya\Store $_Store, $_price = 0.00, $_country_or_state = NULL)
        {
                
                if (!is_numeric($_price) || (float)$_price === 0.00) return 0.00;
                
                if (!($_country_or_state instanceof \Cataleya\Geo\Country) && !($_country_or_state instanceof \Cataleya\Geo\Province)) 
                    $this->e->triggerException('Error in class (' . __CLASS__ . '): [ The 2nd argument of ->getTaxAmount() must be an instance of _Class_Country or an instance of _Class_Province  ] on line ' . __LINE__);
		
                
                // Determine store location
                $_store_loc = $_Store->getDefaultContact();
                $_store_country = $_store_loc->getCountry();
                $_store_country_or_state = ($_store_country->hasProvinces()) ? $_store_loc->getProvince() : $_store_country;
                
                // Only return rate if location is in the same zone as store
                if ($this->getTaxZone()->hasLocation($_country_or_state) && $this->getTaxZone()->hasLocation($_store_country_or_state)) return round(($this->getRate()/100 * (float)$_price), 2);
                else return 0.00;
        }






        /*
         *
         * [ getLastModified ] 
         *_____________________________________________________
         *
         *
         */

        public function getLastModified()
        {
                return new \DateTime($this->_data['last_modified']);
        }






        /*
         *
         * [ getDateAdded ] 
         *_____________________________________________________
         *
         *
         */

        public function getDateAdded()
        {
            $_DateTime = new \DateTime($this->_data['date_added']);
            return $_DateTime;
        }





        
        
        
        


}
	
	
	
	
