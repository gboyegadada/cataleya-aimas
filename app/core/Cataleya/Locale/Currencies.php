<?php



namespace Cataleya\Locale;




/*
 *
 * 	@Package: Cataleya
 * 	@Class: \Cataleya\Locale\Currencies
 *
 * 	(c) 2012 Fancy Paper Planes
 *
 *
 *
 */

class Currencies extends \Cataleya\Collection {
   
    // To be used by [\Cataleya\Collection]
    protected $_collection = array ();
    protected $_position = 0;


    public function __construct() {
       
        // Get database handle...
        $this->dbh = \Cataleya\Helper\DBH::getInstance();
		
        // Get error handler
        $this->e = \Cataleya\Helper\ErrorHandler::getInstance();
        

        // LOAD: COLLECTION
        static $collection_select,
               $param_offset,
               $param_limit;


        if (!isset($collection_select)) {
                // PREPARE SELECT STATEMENT...
                $collection_select = $this->dbh->prepare('SELECT currency_code FROM currencies LIMIT :offset, :limit');

                $collection_select->bindParam(':offset', $param_offset, \PDO::PARAM_INT);
                $collection_select->bindParam(':limit', $param_limit, \PDO::PARAM_INT);
        }


        $param_offset = 0;
        $param_limit = 600;

        if (!$collection_select->execute()) $this->e->triggerException('
                                                                                        Error in class (' . __CLASS__ . '): [ ' . 
                                                                                        implode(', ', $collection_select->errorInfo()) . 
                                                                                        ' ] on line ' . __LINE__);
        while ($row = $collection_select->fetch(\PDO::FETCH_ASSOC))
        {
            $this->_collection[] = $row['currency_code'];
        }
        
        parent::__construct();
 
        
    }

    
    
    
    
    
    public function current()
    {
        return \Cataleya\Locale\Currency::load($this->_collection[$this->_position]);
        
    }



        
        
        
    
}


