<?php


namespace Cataleya\Locale;



/*

CLASS CURRENCY

*/



class Currency   
{
	
	
	private $_data = array();
	private $_modified = array();
        
	private $dbh, $e;
	

	


	/*
	 *
	 * [ __construct ]
	 * ________________________________________________________________
	 *
	 *
	 *
	 */
	 
	 
	private function __construct ()
	{
		
		// Get database handle...
                $this->dbh = \Cataleya\Helper\DBH::getInstance();
		
		// Get error handler
		$this->e = \Cataleya\Helper\ErrorHandler::getInstance();
		
	}
	




	/*
	 *
	 * [ __destruct ]
	 * ________________________________________________________________
	 *
	 *
	 *
	 */

    public function __destruct()
    {
		$this->saveData();
    }
	




	/*
	 *
	 * [ load ]
	 * ________________________________________________________________
	 *
	 *
	 *
	 */
	 
	 
	public static function load ($id = 'NGN')
	{
		
        if (!is_string($id) || preg_match('/^[A-Za-z]{2,3}$/', $id) === FALSE) return NULL;
        
        $id = strtoupper($id);

        if ($id === 'BASE_CURRENCY') return self::loadBaseCurrency();
                
		//if ($id == 0 || filter_var($id, FILTER_VALIDATE_INT) === FALSE) return NULL;
		
		$instance = new \Cataleya\Locale\Currency ();
		
		// LOAD: CURRENCY
		static $instance_select, $param_instance_id;
		
			
		if (!isset($instance_select)) {
			// PREPARE SELECT STATEMENT...
			$instance_select = $instance->dbh->prepare('SELECT * FROM currencies WHERE currency_code = :instance_id LIMIT 1');
			$instance_select->bindParam(':instance_id', $param_instance_id, \PDO::PARAM_STR);
		}
		
		
		$param_instance_id = $id;
		
		if (!$instance_select->execute()) $instance->e->triggerException('
												Error in class (' . __CLASS__ . '): [ ' . 
												implode(', ', $instance_select->errorInfo()) . 
												' ] on line ' . __LINE__);
		
		$instance->_data = $instance_select->fetch(\PDO::FETCH_ASSOC);
                
                if (empty($instance->_data)) return NULL;
               


		return $instance;	
		
		
	}






        
        
	/*
	 *
	 * [ loadBaseCurrency ]
	 * ________________________________________________________________
	 *
	 *
	 *
	 */
	 
	 
	public static function loadBaseCurrency ()
	{
		
		$instance = new \Cataleya\Locale\Currency ();
		
		// LOAD: BASE CURRENCY
		static $instance_select;
		
			
		if (!isset($instance_select)) {
			// PREPARE SELECT STATEMENT...
			$instance_select = $instance->dbh->prepare('SELECT * FROM currencies WHERE is_base_currency = 1 LIMIT 1');
		}
		

		if (!$instance_select->execute()) $instance->e->triggerException('
												Error in class (' . __CLASS__ . '): [ ' . 
												implode(', ', $instance_select->errorInfo()) . 
												' ] on line ' . __LINE__);
		
		$instance->_data = $instance_select->fetch(\PDO::FETCH_ASSOC);
                
                if (empty($instance->_data)) return NULL;
               


		return $instance;	
		
		
	}



        
	/*
	 *
	 *  [ setAsBaseCurrency ]
	 * ________________________________________________________________
	 * 
	 *
	 *
	 *
	 */
	 
	public function setAsBaseCurrency () 
	{
                
                // if already set as base currency...
		if ((int)$this->_data['is_base_currency'] === 1)  return TRUE;
                $_update_params = array();
                
                
                // GET CURRENT BASE CURRENCY
                $base_currency = \Cataleya\Locale\Currency::loadBaseCurrency();
                $new_value_of_old_base = 1 / $this->_data['rate'];
                
                

		// UNSET OLD BASE CURRENCY...
		$update_handle = $this->dbh->prepare('UPDATE currencies SET is_base_currency = 0 WHERE is_base_currency = 1');
                
                if (!$update_handle->execute()) $this->e->triggerException('Error in class (' . __CLASS__ . '): [ ' . implode(', ', $update_handle->errorInfo()) . ' ] on line ' . __LINE__);
                
                
		// RECALC PRICES...
		$update_handle = $this->dbh->prepare('
                                                        UPDATE price_values 
                                                        SET value = (value * :currency_rate) 
                                                        ');
		$update_handle->bindParam(':currency_rate', $_update_params['currency_rate'], \PDO::PARAM_STR);
                $_update_params['currency_rate'] = $this->getRate();
                
                if (!$update_handle->execute()) $this->e->triggerException('Error in class (' . __CLASS__ . '): [ ' . implode(', ', $update_handle->errorInfo()) . ' ] on line ' . __LINE__);
                
                
                
		// RECALC PAYMENT TYPE AMOUNTS...
		$update_handle = $this->dbh->prepare('
                                                        UPDATE payment_types 
                                                        SET max_amount = (max_amount * :currency_rate), min_amount = (min_amount * :currency_rate) 
                                                        ');
		$update_handle->bindParam(':currency_rate', $_update_params['currency_rate'], \PDO::PARAM_STR);
                $_update_params['currency_rate'] = $this->getRate();
                
                if (!$update_handle->execute()) $this->e->triggerException('Error in class (' . __CLASS__ . '): [ ' . implode(', ', $update_handle->errorInfo()) . ' ] on line ' . __LINE__);
                
                
                
                
                
		// SET NEW BASE CURRENCY...
		$update_handle = $this->dbh->prepare('
                                                        UPDATE currencies 
                                                        SET is_base_currency = 1     
                                                        WHERE currency_code = :instance_id 
                                                        ');
		$update_handle->bindParam(':instance_id', $_update_params['currency_code'], \PDO::PARAM_STR);
                $_update_params['currency_code'] = $this->_data['currency_code'];
                
                if (!$update_handle->execute()) $this->e->triggerException('Error in class (' . __CLASS__ . '): [ ' . implode(', ', $update_handle->errorInfo()) . ' ] on line ' . __LINE__);
                
                
                
                // LOAD ALL CURRENCIES
                $select_handle = $this->dbh->prepare('SELECT currency_code, rate FROM currencies');
                if (!$select_handle->execute()) $this->e->triggerException('Error in class (' . __CLASS__ . '): [ ' . implode(', ', $select_handle->errorInfo()) . ' ] on line ' . __LINE__);
                
                $currencies = $select_handle->fetchAll(\PDO::FETCH_ASSOC);
                
                
                // UPDATE CURRENCY RATES
                $_update_params_rate = 0;
                $_update_params_instance_id = '';
                
		$update_handle = $this->dbh->prepare('
                                                        UPDATE currencies 
                                                        SET rate = :rate    
                                                        WHERE currency_code = :currency_code 
                                                        ');
                        
                $update_handle->bindParam(':rate', $_update_params_rate, \PDO::PARAM_STR);
                $update_handle->bindParam(':currency_code', $_update_params_instance_id, \PDO::PARAM_STR);
                
                
                foreach ($currencies as $key=>$row)
                {
                    if ($row['currency_code'] === $this->_data['currency_code'])  
                    {
                        
                    $_update_params_rate = 1;
                    $_update_params_instance_id = $row['currency_code'];
                    
                    } else if ($row['currency_code'] === $base_currency->getCurrencyCode())
                    {
                        
                    $_update_params_rate = $new_value_of_old_base;
                    $_update_params_instance_id = $row['currency_code']; 
                    
                    } else {
                    
                    $_update_params_rate = floatval($row['rate']) * $new_value_of_old_base;
                    $_update_params_instance_id = $row['currency_code'];
                    
                    }
                    
                    if (!$update_handle->execute()) $this->e->triggerException('Error in class (' . __CLASS__ . '): [ ' . implode(', ', $update_handle->errorInfo()) . ' ] on line ' . __LINE__);
		
                }           
                
                $this->setRate(1);
                
                return TRUE;
                
                
      }
        
        
        
        
        /*
         *
         * [ getID ] 
         *_____________________________________________________
         *
         *
         */

        public function getID()
        {
                return $this->_data['currency_code'];
        }




	
	
	public function saveData () 
	{
		

		if (empty($this->_modified)) return;
                
                $this->_modified = array_unique($this->_modified);
                
		$_update_params = array();
		$key_val_pairs = array();
		
		foreach ($this->_modified as $key) $key_val_pairs[$key] = $key . ' = :' . $key;
		
	
		// Do prepared statement for 'INSERT'...
		$update_handle = $this->dbh->prepare('
                                                        UPDATE currencies 
                                                        SET ' . implode (', ', $key_val_pairs) . ', last_updated=now()       
                                                        WHERE currency_code = :instance_id 
                                                        ');
 		$_update_params['currency_code'] = $this->_data['currency_code'];               
		$update_handle->bindParam(':instance_id', $_update_params['currency_code'], \PDO::PARAM_STR);
                
		foreach ($this->_modified as $key) {
                    
                        $_update_params[$key] = $this->_data[$key];                        
			$update_handle->bindParam(':'.$key, $_update_params[$key], \Cataleya\Helper\DBH::getTypeConst($_update_params[$key]));

		}
		
		if (!$update_handle->execute()) $this->e->triggerException('Error in class (' . __CLASS__ . '): [ ' . implode(', ', $update_handle->errorInfo()) . ' ] on line ' . __LINE__);
		
	}
	
	
       
 
 
        
        
        
        
/*
 *
 * [ getCurrencyCode ] 
 *_____________________________________________________
 *
 *
 */

public function getCurrencyCode()
{
	return $this->_data['currency_code'];
}






/*
 *
 * [ getCurrencyName ] 
 *_____________________________________________________
 *
 *
 */

public function getCurrencyName()
{
	return $this->_data['currency_name'];
}







/*
 *
 * [ getNumericCode ] 
 *_____________________________________________________
 *
 *
 */

public function getNumericCode()
{
	return $this->_data['numeric_code'];
}







/*
 *
 * [ getMinorUnit ] 
 *_____________________________________________________
 *
 *
 */

public function getMinorUnit()
{
	return $this->_data['minor_unit'];
}







/*
 *
 * [ getUtfCode ] 
 *_____________________________________________________
 *
 *
 */

public function getUtfCode()
{
	return $this->_data['utf_code'];
}







/*
 *
 * [ getSymbolLeft ] 
 *_____________________________________________________
 *
 *
 */

public function getSymbolLeft()
{
	return $this->_data['symbol_left'];
}







/*
 *
 * [ getSymbolRight ] 
 *_____________________________________________________
 *
 *
 */

public function getSymbolRight()
{
	return $this->_data['symbol_right'];
}







/*
 *
 * [ getDecimalPoint ] 
 *_____________________________________________________
 *
 *
 */

public function getDecimalPoint()
{
	return $this->_data['decimal_point'];
}







/*
 *
 * [ getThousandsPoint ] 
 *_____________________________________________________
 *
 *
 */

public function getThousandsPoint()
{
	return $this->_data['thousands_point'];
}







/*
 *
 * [ getValue ] 
 *_____________________________________________________
 *
 *
 */

public function getValue()
{
	return $this->_data['rate'];
}



/*
 *
 * [ getRate ] alias for [ getValue ]
 *_____________________________________________________
 *
 *
 */

public function getRate()
{
	return $this->getValue();
}




/*
 *
 * [ setValue ] 
 *_____________________________________________________
 *
 *
 */

public function setValue($value)
{
	$this->_data['rate'] = $value;
	$this->_modified[] = 'rate';

	return TRUE;
}



/*
 *
 * [ setRate ] alias for [ setValue ] 
 *_____________________________________________________
 *
 *
 */

public function setRate($value)
{
	return $this->setValue($value);
}




/*
 *
 * [ getLastUpdated ] 
 *_____________________________________________________
 *
 *
 */

public function getLastUpdated()
{
	return $this->_data['last_updated'];
}






/*
 *
 * [ getActive ] 
 *_____________________________________________________
 *
 *
 */

public function getActive()
{
	return $this->_data['active'];
}






/*
 *
 * [ setActive ] 
 *_____________________________________________________
 *
 *
 */

public function setActive($value)
{

        if (filter_var($value, FILTER_VALIDATE_BOOLEAN, FILTER_NULL_ON_FAILURE) === NULL) return FALSE;




        $this->_data['active'] = ($value) ? 1 : 0;
        $this->_modified[] = 'active';

        return TRUE;
}






/*
 *
 * [ getActive ] 
 *_____________________________________________________
 *
 *
 */

public function isActive()
{
        return ((int)$this->_data['active'] === 1);
}





/*
 * 
 * [ enable ]
 * ______________________________________________________
 * 
 * 
 */


public function enable () 
{

    if ((int)$this->_data['active'] === 0) 
    {
        $this->_data['active'] = 1;
        $this->_modified[] = 'active';
    }

    return TRUE;
}





/*
 * 
 * [ disable ]
 * ______________________________________________________
 * 
 * 
 */


public function disable () 
{
    if ((int)$this->_data['active'] === 1) 
    {
        $this->_data['active'] = 0;
        $this->_modified[] = 'active';
    }

    return TRUE;
}



        



        





}
	
	
	
	
