<?php


namespace Cataleya\Locale;



/*

CLASS LANGUAGE

*/



class Language   
{
	
	
	private $_data = array();
	private $_modified = array();
        
	private $dbh, $e;
	

	


	/*
	 *
	 * [ __construct ]
	 * ________________________________________________________________
	 *
	 *
	 *
	 */
	 
	 
	private function __construct ()
	{
		
		// Get database handle...
                $this->dbh = \Cataleya\Helper\DBH::getInstance();
		
		// Get error handler
		$this->e = \Cataleya\Helper\ErrorHandler::getInstance();
		
	}
	




	/*
	 *
	 * [ __destruct ]
	 * ________________________________________________________________
	 *
	 *
	 *
	 */

    public function __destruct()
    {
		$this->saveData();
    }
	




	/*
	 *
	 * [ load ]
	 * ________________________________________________________________
	 *
	 *
	 *
	 */
	 
	 
	public static function load ($id = 'EN')
	{
		
                if (empty($id) || preg_match('/^[A-Za-z]{2,3}$/', $id) === FALSE) return NULL;
                
                $id = strtoupper($id);
                
		//if ($id == 0 || filter_var($id, FILTER_VALIDATE_INT) === FALSE) return NULL;
		
		$instance = new \Cataleya\Locale\Language ();
		
		// LOAD: CURRENCY
		static $instance_select, $param_instance_id;
		
			
		if (!isset($instance_select)) {
			// PREPARE SELECT STATEMENT...
			$instance_select = $instance->dbh->prepare('SELECT * FROM languages WHERE language_code = :instance_id LIMIT 1');
			$instance_select->bindParam(':instance_id', $param_instance_id, \PDO::PARAM_STR);
		}
		
		
		$param_instance_id = $id;
		
		if (!$instance_select->execute()) $instance->e->triggerException('
												Error in class (' . __CLASS__ . '): [ ' . 
												implode(', ', $instance_select->errorInfo()) . 
												' ] on line ' . __LINE__);
		
		$instance->_data = $instance_select->fetch(\PDO::FETCH_ASSOC);
                
               


		return $instance;	
		
		
	}





        /*
         *
         * [ getID ] 
         *_____________________________________________________
         *
         *
         */

        public function getID()
        {
                return $this->_data['language_code'];
        }




	
	
	public function saveData () 
	{
		

		if (empty($this->_modified)) return;
                
                $this->_modified = array_unique($this->_modified);
                
		$_update_params = array();
		$key_val_pairs = array();
		
		foreach ($this->_modified as $key) $key_val_pairs[$key] = $key . ' = :' . $key;
		
	
		// Do prepared statement for 'INSERT'...
		$update_handle = $this->dbh->prepare('
                                                        UPDATE countries 
                                                        SET ' . implode (', ', $key_val_pairs) . '    
                                                        WHERE language_code = :instance_id 
                                                        ');
		$_update_params['language_code'] = $this->_data['language_code'];                
		$update_handle->bindParam(':instance_id', $_update_params['language_code'], \PDO::PARAM_STR);
                
		foreach ($this->_modified as $key) {
                    
                        $_update_params[$key] = $this->_data[$key];                        
			$update_handle->bindParam(':'.$key, $_update_params[$key], \Cataleya\Helper\DBH::getTypeConst($_update_params[$key]));

		}
		
		if (!$update_handle->execute()) $this->e->triggerException('Error in class (' . __CLASS__ . '): [ ' . implode(', ', $update_handle->errorInfo()) . ' ] on line ' . __LINE__);
		
	}
	
	
       
 


/*
 *
 * [ getLanguageCode ] 
 *_____________________________________________________
 *
 *
 */

public function getLanguageCode()
{
	return $this->_data['language_code'];
}






/*
 *
 * [ getName ] 
 *_____________________________________________________
 *
 *
 */

public function getName()
{
	return $this->_data['name'];
}






/*
 *
 * [ getImage ] 
 *_____________________________________________________
 *
 *
 */

public function getImage()
{
	return $this->_data['image'];
}







/*
 *
 * [ getDirectory ] 
 *_____________________________________________________
 *
 *
 */

public function getDirectory()
{
	return $this->_data['directory'];
}







/*
 *
 * [ getSortOrder ] 
 *_____________________________________________________
 *
 *
 */

public function getSortOrder()
{
	return $this->_data['sort_order'];
}







}
	
	
	
	
