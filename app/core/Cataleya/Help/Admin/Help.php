<?php



namespace Cataleya\Help\Admin;




/*
 *
 * 	@Package: Cataleya
 * 	@Class: \Cataleya\Help\Admin\Help
 *
 * 	(c) 2012 Fancy Paper Planes
 *
 *
 *
 */

class Help extends \Cataleya\Collection {
   
    // To be used by [\Cataleya\Collection]
    protected $_collection = array ();
    protected $_position = 0;
    
    
    
    
    
    // CONST VALUES 1 - 15 RESERVED FOR [ _Abstract_Collection ]
    const ORDER_BY_TOPIC_ID = 16;
    const ORDER_BY_TOPIC = 17;


    public function __construct() {
        
        // Get database handle...
        $this->dbh = \Cataleya\Helper\DBH::getInstance();
		
        // Get error handler
        $this->e = \Cataleya\Helper\ErrorHandler::getInstance();
        


        parent::__construct();

        
    }

    
    
    
    
    
   
    
    /*
     * 
     * [ load ]
     * 
     * 
     */
    
    public static function load ($_language_code = '', $sort = array('order_by'=>'', 'order'=>'', 'index'=>''), $page_size = 16, $page_num = 1) 
    {
        
        if (preg_match('/^[A-Za-z]{2}$/', $_language_code) === 0) \Cataleya\Helper\ErrorHandler::getInstance()->triggerException('
										Error in class (' . __CLASS__ . '): [ Invalid arguments for ::create(). Please specify a language code. ] on line ' . __LINE__);
               
        
        $_objct = __CLASS__;
        $instance = new $_objct;
        
        // LOAD
        
        // This is used to determine which column is to be used in the index...
        $_index_with = 'topic';
        
        if (is_array($sort) && !empty($sort['order_by']) && !empty($sort['order']) && is_int($sort['order_by']) && is_int($sort['order']) )
        {
            

            // ASC | DESC
            switch ($sort['order']) {

                case self::ORDER_ASC:
                    $param_order = 'ASC';
                    break;

                case self::ORDER_DESC:
                    $param_order = 'DESC';
                    break;

                default :
                    $param_order = 'ASC';
                    break;
            }  
            
            
            // ORDER BY
            switch ($sort['order_by']) {

                case self::ORDER_BY_TOPIC:
                    $param_order_by = "topic $param_order";
                    break;
                
                case self::ORDER_BY_TOPIC_ID:
                    $param_order_by = "topic_id $param_order";
                    $_index_with = 'topic_id';
                    break;
                
                default :
                    $param_order_by = "topic $param_order";
                    break;
            }

                  


        }

        // Defaults                
        else {

                $param_order = 'ASC';
                $param_order_by = "topic $param_order";

        }    
        
        
        
        // Check if index is specified
        if (is_array($sort) && isset($sort['index']) && is_string($sort['index']))
        {
            $param_regex = (preg_match('/^[a-zA-Z]{1}$/', $sort['index']) > 0) ? '^[' . strtolower($sort['index']) . '].*' : '.*';
        }
        
        else {
            $param_regex = '.*';
        }

                  


        $param_offset = 0;
        $param_limit = 99999999;
        $param_language_code = strtoupper($_language_code);


        // PREPARE SELECT STATEMENT...
        $select_handle = $instance->dbh->prepare('
                                        SELECT topic_id, LOWER(language_code)  
                                        FROM help_topics  

                                WHERE ' . $_index_with . ' REGEXP :regex 
                                AND language_code = :language_code 
                                ORDER BY '. $param_order_by . '   
                                LIMIT :offset, :limit
                                ');

        $select_handle->bindParam(':offset', $param_offset, \PDO::PARAM_INT);
        $select_handle->bindParam(':limit', $param_limit, \PDO::PARAM_INT);
        $select_handle->bindParam(':regex', $param_regex, \PDO::PARAM_STR);
        $select_handle->bindParam(':language_code', $param_language_code, \PDO::PARAM_STR);



        if (!$select_handle->execute()) $instance->e->triggerException('
                                                                                        Error in class (' . __CLASS__ . '): [ ' . 
                                                                                        implode(', ', $select_handle->errorInfo()) . 
                                                                                        ' ] on line ' . __LINE__);
        while ($row = $select_handle->fetch(\PDO::FETCH_ASSOC))
        {
            $instance->_collection[] = strtolower($row['topic_id'] . '.' . $row['language_code']);
        }
        
        
        $instance->pageSetup($page_size, $page_num);

        return $instance;
        
    }

    



    
    
    
    
    
    
    public function current()
    {
        return \Cataleya\Help\Admin\HelpTopic::load($this->_collection[$this->_position]);
        
    }
    
    
    
    


    
    
    /*
     * 
     * [ search ]
     * 
     */
    
    public static function search ($_term = '', $sort = array('order_by'=>'', 'order'=>'', 'index'=>''), $page_size = 100, $page_num = 1) 
    {

            // First, take out funny looking characters...
            $_term = preg_replace('/[^-\s\.A-Za-z0-9@*#+]+/', '', $_term);

            // Trim then split terms by [:space:]
            $terms = explode(' ', trim($_term, ' '));



            // SORT SEARCH TERMS
            $regex = array();
            $regex['firstname'] = $regex['lastname'] = $regex['email_address'] = $regex['telephone'] = array();


            foreach ($terms as $key=>$val) {

                    // Loose any search terms shorter than 3 chars...
                    if (strlen($val) < 2) unset($terms[$key]);
                    
                    // Sort keywords into types e.g. name, email or phone numbers
                    // Also make dots safe...
                    else {

                            if (preg_match('/[-\s\.A-Za-z0-9]{1,60}/', $val)) {
                                    $regex['firstname'][] = $regex['lastname'][] = preg_replace('/[\.]/', '\.', $val); 
                            } 

                            if (preg_match('/^[^@\.]?[\w\._-]+@([\w]+)?\.?([A-Za-z]{2,4})?$/', $val)) {
                                    $regex['email_address'][] = preg_replace('/[\.]/', '\.', $val);
                            } 

                            if (!preg_match('/[^+#*\d]/', $val) && strlen($val) > 5) {
                                    $regex['telephone'][] = preg_replace('/[\.]/', '\.', $val);
                            }

                    }
            }

            
            // Build REGEX
            $regex_concat = '';
            foreach ($regex as $k => $v) {
                    if (count($v) == 0) {$regex[$k] = ''; continue; } // no search terms...
                    elseif (count($v) > 3) $v = array_slice($v, 0, 3); // truncate if there are too many search terms...

                    $regex[$k] = "^.*(" . implode('|', $v) . ").*$";
                    $regex_concat .= ($regex_concat != '') ? ' OR ' : '';
                    $regex_concat .= $k . ' REGEXP :regex_' . $k;


            }

            
            
            // Create instance...
            $_objct = __CLASS__;
            $instance = new $_objct;


            // If nothing made it through the filtering...
            // [ _collection ] will be empty a.k.a no results..
            if ($regex_concat == '') return $instance;



            
            // DO SEARCH...
            $select_handle = \Cataleya\Helper\DBH::getInstance()->prepare('SELECT customer_id 
                                            FROM customers 
                                            WHERE ' . $regex_concat . '  
                                            ORDER BY firstname');


            if ($regex['firstname'] != '') $select_handle->bindParam(':regex_firstname', $regex['firstname'], \PDO::PARAM_STR);
            if ($regex['lastname'] != '') $select_handle->bindParam(':regex_lastname', $regex['lastname'], \PDO::PARAM_STR);
            if ($regex['email_address'] != '') $select_handle->bindParam(':regex_email_address', $regex['email_address'], \PDO::PARAM_STR);
            if ($regex['telephone'] != '') $select_handle->bindParam(':regex_telephone', $regex['telephone'], \PDO::PARAM_STR);



            if (!$select_handle->execute()) \Cataleya\Helper\ErrorHandler::getInstance ()->triggerException('
                                                                        Error in class (' . __CLASS__ . '): [ ' . 
                                                                        implode(', ', $select_handle->errorInfo()) . 
                                                                        ' ] on line ' . __LINE__);





            while ($row = $select_handle->fetch(\PDO::FETCH_ASSOC))$instance->_collection[] = $row['customer_id'];
            


            $instance->pageSetup($page_size, $page_num);

            return $instance;
        
    }
    
    
    
    
    
}


