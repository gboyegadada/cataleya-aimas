<?php


namespace Cataleya\Help\Admin;



/*

CLASS

*/



class HelpTopic   
{
	
	
	private $_data = array();
	private $_modified = array();
        
	private $dbh, $e;
	

	


	/*
	 *
	 * [ __construct ]
	 * ________________________________________________________________
	 *
	 *
	 *
	 */
	 
	 
	private function __construct ()
	{
		
		// Get database handle...
                $this->dbh = \Cataleya\Helper\DBH::getInstance();
		
		// Get error handler
		$this->e = \Cataleya\Helper\ErrorHandler::getInstance();
		
	}
	




	/*
	 *
	 * [ __destruct ]
	 * ________________________________________________________________
	 *
	 *
	 *
	 */

        public function __destruct()
        {
                    $this->saveData();
        }
	




	/*
	 *
	 * [ load ]
	 * ________________________________________________________________
	 *
	 *
	 *
	 */
	 
	 
	public static function load ($id = '')
	{
		
                $_matches = array();
                
                if (!is_string($id) || preg_match('/^(?<id>([A-Za-z0-9\-]+\.?){1,10})\.(?<lang>[A-Za-z]{2})$/', $id, $_matches) === 0) 
                        \Cataleya\Helper\ErrorHandler::getInstance()->triggerException('
										Error in class (' . __CLASS__ . '): [ Invalid arguments for ::create(). Make sure to use "topic.subtopic.language-iso-code" for your topic id. ] on line ' . __LINE__);
               
                
                


		$instance = new \Cataleya\Help\Admin\HelpTopic ();
		
		// LOAD: CURRENCY
		static $select_handle, $param_instance_id, $param_language_code;
		
			
		if (empty($select_handle)) {
			// PREPARE SELECT STATEMENT...
			$select_handle = $instance->dbh->prepare('SELECT * FROM help_topics WHERE topic_id = :instance_id AND language_code = :language_code LIMIT 1');
			$select_handle->bindParam(':instance_id', $param_instance_id, \PDO::PARAM_STR);
                        $select_handle->bindParam(':language_code', $param_language_code, \PDO::PARAM_STR);
		}
		
		
		$param_instance_id = strtolower($_matches['id']);
                $param_language_code = strtoupper($_matches['lang']);
		
		if (!$select_handle->execute()) $instance->e->triggerException('
												Error in class (' . __CLASS__ . '): [ ' . 
												implode(', ', $select_handle->errorInfo()) . 
												' ] on line ' . __LINE__);
		
		$instance->_data = $select_handle->fetch(\PDO::FETCH_ASSOC);
                
		if (empty($instance->_data))
		{
			unset($instance);
			return NULL;
		}



		return $instance;	
		
		
	}



	/*
	 *
	 *  [ create ]
	 * ________________________________________________________________
	 * 
	 * 
	 *
	 *
	 */

	 
	static public function create ($_topic_id = '', $_topic = 'New Help Topic', $_short_text = '', $_long_text = '', $_full_text = '') 
	{
		
		
                if (!is_string($_topic_id) || preg_match('/^(?<id>([A-Za-z0-9\-]+\.?){1,10})\.(?<lang>[A-Za-z]{2})$/', $_topic_id, $_matches) === 0 || !is_string($_topic) || !is_string($_short_text) || !is_string($_long_text) || !is_string($_full_text)) 
                        \Cataleya\Helper\ErrorHandler::getInstance()->triggerException('
										Error in class (' . __CLASS__ . '): [ Invalid arguments for ::create(). Make sure to use "topic.subtopic.language-iso-code" for your topic id. ] on line ' . __LINE__);
               
		 

		
		// Get database handle...
                $dbh = \Cataleya\Helper\DBH::getInstance();
		
		// Get error handler
		$e = \Cataleya\Helper\ErrorHandler::getInstance();
		
		// construct
		static 
				$insert_handle,   
                                $insert_handle_param_topic_id,  
                                $param_language_code, 
                                $insert_handle_param_topic,   
                                $insert_handle_param_short_text,   
                                $insert_handle_param_long_text,   
                                $insert_handle_param_full_text;
		
		if (empty($insert_handle))
		{
			$insert_handle = $dbh->prepare('
                                                        INSERT INTO help_topics 
                                                        (topic_id, language_code, topic, short_text, long_text, full_text, date_added, last_modified) 
                                                        VALUES (:topic_id, :language_code, :topic, :short_text, :long_text, :full_text, NOW(), NOW())
                                                        ');
															
			$insert_handle->bindParam(':topic_id', $insert_handle_param_topic_id, \PDO::PARAM_STR);	
                        $select_handle->bindParam(':language_code', $param_language_code, \PDO::PARAM_STR);
			$insert_handle->bindParam(':topic', $insert_handle_param_topic, \PDO::PARAM_STR);	
			$insert_handle->bindParam(':short_text', $insert_handle_param_short_text, \PDO::PARAM_STR);	
			$insert_handle->bindParam(':long_text', $insert_handle_param_long_text, \PDO::PARAM_STR);		
			$insert_handle->bindParam(':full_text', $insert_handle_param_full_text, \PDO::PARAM_STR);	
		}
		
                $insert_handle_param_topic_id = strtolower($_matches['id']);
                $param_language_code = strtoupper($_matches['lang']);
                $insert_handle_param_topic = filter_var($_topic, FILTER_SANITIZE_STRING);  
                $insert_handle_param_short_text = filter_var($_short_text, FILTER_SANITIZE_STRING);
                $insert_handle_param_long_text = filter_var($_long_text, FILTER_SANITIZE_STRING);
                $insert_handle_param_full_text = filter_var($_full_text, FILTER_SANITIZE_STRING);
		
		if (!$insert_handle->execute()) $e->triggerException('
										Error in class (' . __CLASS__ . '): [ ' . 
										implode(', ', $insert_handle->errorInfo()) . 
										' ] on line ' . __LINE__);
										
		
		// AUTOLOAD NEW PRODUCT AND RETURN IT
		$instance = self::load($dbh->lastInsertId());
		
		return $instance;
		
		
	}


	 
	 /*
	 *  [ delete ]
	 * ________________________________________________________________
	 * 
	 * 
	 *
	 *
	 */

	 
	public function delete () 
	{
		
			// DELETE 
			$delete_handle = $this->dbh->prepare('
													DELETE FROM help_topics 
													WHERE topic_id = :instance_id
													');
			$delete_handle->bindParam(':instance_id', $delete_handle_param_instance_id, \PDO::PARAM_STR);
			
			$delete_handle_param_instance_id = $this->_data['topic_id'];
	
			if (!$delete_handle->execute()) $this->e->triggerException('
											Error in class (' . __CLASS__ . '): [ ' . 
											implode(', ', $delete_handle->errorInfo()) . 
											' ] on line ' . __LINE__);
											
			

		
		// $this = NULL;
		return TRUE;
		
		
	}





        /*
         *
         * [ getID ] 
         *_____________________________________________________
         *
         *
         */

        public function getID()
        {
                return $this->_data['topic_id'];
        }




	
	
	public function saveData () 
	{
		

		if (empty($this->_modified)) return;
                $this->_modified = array_unique($this->_modified);
                
		$_update_params = array();
		$key_val_pairs = array();
		
		foreach ($this->_modified as $key) $key_val_pairs[$key] = $key . ' = :' . $key;
		
	
		// Do prepared statement for 'INSERT'...
		$update_handle = $this->dbh->prepare('
                                                        UPDATE help_topics 
                                                        SET ' . implode (', ', $key_val_pairs) . ', last_modified=now()       
                                                        WHERE topic_id = :instance_id 
                                                        ');
                
		$update_handle->bindParam(':instance_id', $_update_params['topic_id'], \PDO::PARAM_INT);
                $_update_params['topic_id'] = $this->_data['topic_id'];
                
		foreach ($this->_modified as $key) {
                    
                        $_update_params[$key] = $this->_data[$key];                        
			$update_handle->bindParam(':'.$key, $_update_params[$key], \Cataleya\Helper\DBH::getTypeConst($_update_params[$key]));

		}
		
		

		
		if (!$update_handle->execute()) $this->e->triggerException('Error in class (' . __CLASS__ . '): [ ' . implode(', ', $update_handle->errorInfo()) . ' ] on line ' . __LINE__);
		
	}
	
	
       
 
 
        


        /*
         *
         * [ getTopicId ] 
         *_____________________________________________________
         *
         *
         */

        public function getTopicId()
        {
                return $this->_data['topic_id'];
        }







        /*
         *
         * [ getTopic ] 
         *_____________________________________________________
         *
         *
         */

        public function getTopic()
        {
                return $this->_data['topic'];
        }



        /*
         *
         * [ setTopic ] 
         *_____________________________________________________
         *
         *
         */

        public function setTopic($value = NULL)
        {
                if (!is_string($value) || preg_match('/^([A-Za-z0-9\-]+\.?){1,10}$/', $value) === 0) return FALSE;

                $this->_data['topic'] = $value;
                $this->_modified[] = 'topic';

                return TRUE;
        }



        /*
         *
         * [ getShortText ] 
         *_____________________________________________________
         *
         *
         */

        public function getShortText()
        {
                return $this->_data['short_text'];
        }



        /*
         *
         * [ setShortText ] 
         *_____________________________________________________
         *
         *
         */

        public function setShortText($value = NULL)
        {
                if (!is_string($value) || strlen($value) > 2000) return FALSE;

                $this->_data['short_text'] = filter_var($value, FILTER_SANITIZE_STRING);
                $this->_modified[] = 'short_text';

                return TRUE;
        }



        /*
         *
         * [ getLongText ] 
         *_____________________________________________________
         *
         *
         */

        public function getLongText()
        {
                return $this->_data['long_text'];
        }



        /*
         *
         * [ setLongText ] 
         *_____________________________________________________
         *
         *
         */

        public function setLongText($value = NULL)
        {
                if (!is_string($value) || strlen($value) > 20000) return FALSE;

                $this->_data['long_text'] = filter_var($value, FILTER_SANITIZE_STRING);
                $this->_modified[] = 'long_text';

                return TRUE;
        }



        /*
         *
         * [ getFullText ] 
         *_____________________________________________________
         *
         *
         */

        public function getFullText()
        {
                return $this->_data['full_text'];
        }



        /*
         *
         * [ setFullText ] 
         *_____________________________________________________
         *
         *
         */

        public function setFullText($value = NULL)
        {
                if (!is_string($value) || strlen($value) > 2000000) return FALSE;

                $this->_data['full_text'] = filter_var($value, FILTER_SANITIZE_STRING);
                $this->_modified[] = 'full_text';

                return TRUE;
        }



        /*
         *
         * [ getDateAdded ] 
         *_____________________________________________________
         *
         *
         */

        public function getDateAdded()
        {
                return $this->_data['date_added'];
        }





        /*
         *
         * [ getLastModified ] 
         *_____________________________________________________
         *
         *
         */

        public function getLastModified()
        {
                return $this->_data['last_modified'];
        }






        
        
        



        





}
	
	
	
	
