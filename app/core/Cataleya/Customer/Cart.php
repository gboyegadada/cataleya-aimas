<?php


namespace Cataleya\Customer;



/*
 *
 *	@Package: Cataleya
 *	@Class: \Cataleya\Customer\Cart
 *
 *	(c) 2012 Fancy Paper Planes
 *
 *
 *
 */




class Cart 
extends \Cataleya\Collection 
implements \Cataleya\System\Event\Observable 
{
	
	
	private $_data = [];
	private $_modified = [];
    protected $_collection = [];

    protected $_discounts = [];
        
	private $dbh, $e, $_checkout;
	

	
    protected static $_events = [
            'customer/cart/created',
            'customer/cart/updated', 
            'customer/cart/checked-out', 
            'customer/cart/abandoned', 
            'customer/cart/deleted'
        ];


	/*
	 *
	 * [ __construct ]
	 * ________________________________________________________________
	 *
	 *
	 *
	 */
	 
	 
	public function __construct ()
	{
		
		// Get database handle...
                $this->dbh = \Cataleya\Helper\DBH::getInstance();
		
		// Get error handler
		$this->e = \Cataleya\Helper\ErrorHandler::getInstance();
		
	}
	




	/*
	 *
	 * [ __destruct ]
	 * ________________________________________________________________
	 *
	 *
	 *
	 */

        public function __destruct()
        {
                    $this->saveData();
        }
	




	/*
	 *
	 * [ load ]
	 * ________________________________________________________________
         * 
	 * Note: if @param $id is set to NULL or 0, a new cart will be created as returned.
	 *
	 *
	 */
	 
	 
	public static function load (\Cataleya\Store $_Store, $key = NULL)
	{
                
                if ($key === NULL) 
                {
                    // Check if there's CART_COOKIE...
                    if (isset($_COOKIE['_CART'])) $key = filter_var($_COOKIE['_CART'], FILTER_SANITIZE_STRIPPED);
                }
                else if ($key === 0) return self::create ($_Store);
                else if (!is_string($key)) return NULL;
		
		$instance = new \Cataleya\Customer\Cart ();
		
		// LOAD: CART
		static $instance_select, $param_instance_id, $param_store_id;
			
		if (!isset($instance_select)) {
			// PREPARE SELECT STATEMENT...
			$instance_select = $instance->dbh->prepare('SELECT * FROM cart WHERE secure_key = :instance_id AND store_id = :store_id LIMIT 1');
			$instance_select->bindParam(':instance_id', $param_instance_id, \PDO::PARAM_STR);
			$instance_select->bindParam(':store_id', $param_store_id, \PDO::PARAM_INT);
		}

		$param_instance_id = $key;
                $param_store_id = $_Store->getStoreId();
                
		if (!$instance_select->execute()) $instance->e->triggerException('
												Error in class (' . __CLASS__ . '): [ ' . 
												implode(', ', $instance_select->errorInfo()) . 
												' ] on line ' . __LINE__);
		
		$instance->_data = $instance_select->fetch(\PDO::FETCH_ASSOC);
                if (empty($instance->_data)) self::create ($_Store);
                    
                $instance->loadCartItems();
               
                $instance->pageSetup();

		return $instance;	
		
		
	}



	/*
	 *
	 *  [ create ]
	 * ________________________________________________________________
	 * 
	 * 
	 *
	 *
	 */

	 
	static public function create (\Cataleya\Store $_Store) 
	{
		
		
		
		// Get database handle...
                $dbh = \Cataleya\Helper\DBH::getInstance();
		
		// Get error handler
		$e = \Cataleya\Helper\ErrorHandler::getInstance();
                

                
                $_StoreAddress = $_Store->getDefaultContact();
                $_Country = $_StoreAddress->getCountry();
                $_country_or_province = ($_Country->hasProvinces()) 
                                        ? $_StoreAddress->getProvince()
                                        : $_Country;
                
                $_ShippingAddress = \Cataleya\Asset\Contact::create($_country_or_province, array('label' => 'Shipping Address'));
                $_BillingAddress = \Cataleya\Asset\Contact::create($_country_or_province, array('label' => 'Billing Address'));  
                
                $_secure_key = md5( uniqid(rand(), TRUE) ) . '-' . time();

                $_params = array (
                    'store_id'=>$_Store->getID(), 
                    'currency_code'=>$_Store->getCurrencyCode(), 
                    'shipping_address_id'=>$_ShippingAddress->getContactID(), 
                    'billing_address_id'=>$_BillingAddress->getContactID(),
                    'status'=>1, 
                    'secure_key'=>$_secure_key
                );
                
                
                

                $_expected_keys = array (
                    'store_id'=>'int', 
                    'currency_code'=>'string', 
                    'shipping_address_id'=>'string', 
                    'billing_address_id'=>'string', 
                    'status'=>'int', 
                    'secure_key'=>'string'
                );
            
            
            
                $_params = \Cataleya\Helper\DBH::buildParamsArray($_expected_keys, $_params);
		
		// construct
		static $instance_insert;
		
		if (empty($instance_insert))
		{
			$instance_insert = $dbh->prepare('
                                                        INSERT INTO cart 
                                                        (' . implode(', ', $_params['columns']) . ', created, modified) 
                                                        VALUES (' . implode(', ', $_params['placeholders']) . ', NOW(), NOW())
                                                        ');
		}
                
                \Cataleya\Helper\DBH::bindParamsArray($instance_insert, $_params['params']);
                
		if (!$instance_insert->execute()) $e->triggerException('
										Error in class (' . __CLASS__ . '): [ ' . 
										implode(', ', $instance_insert->errorInfo()) . 
										' ] on line ' . __LINE__);
										

                // Make sure cookie is fresh...      
                setcookie(
                    '_CART', // Name
                    $_secure_key, // Value
                    COOKIE_EXPIRES, // Expires
                    COOKIE_PATH, // Path
                    COOKIE_DOMAIN, // Domain
                    COOKIE_SECURE, // Is secure
                    TRUE // HTTP Only
                    );


		// AUTOLOAD AND RETURN
		$instance = self::load($_Store, $_secure_key);
		
					
        \Cataleya\System\Event::notify('customer/cart/created', $instance, array ());	
		return $instance;
		
		
	}
    
    
    
    
    /**
     * getEvents
     * 
     * @return array
     */
    public static function getEvents() {

        return self::$_events;
    }




	 
	 /*
	 *  [ delete ]
	 * ________________________________________________________________
	 * 
	 * 
	 *
	 *
	 */

	 
	public function delete () 
	{
            
            
        // Delete rows containing 'cart_id' in specified tables
        \Cataleya\Helper\DBH::sanitize(array('cart_items', 'cart'), 'cart_id', (int)$this->_data['cart_id']);

					
        \Cataleya\System\Event::notify('customer/cart/deleted', null, array ('id'=>$this->getID()));	
		// $this = NULL;
		return TRUE;
		
		
	}



        
	/*
	 *
	 *  [ destroy ]: Alias for [ delete ]
	 * ________________________________________________________________
	 * 
	 * 
	 *
	 *
	 *
	 */
	 
	public function destroy () 
	{
            return $this->delete();
            
        }               
        
        
        
        


        /*
         *
         * [ getCartId ] 
         *_____________________________________________________
         *
         *
         */

        public function getCartId()
        {
                return $this->_data['cart_id'];
        }
        
        
        public function getID () 
        {
            return $this->_data['cart_id'];
        }











        /*
         *
         * [ getSecureKey ] 
         *_____________________________________________________
         *
         *
         */

        public function getSecureKey()
        {
                return $this->_data['secure_key'];
        }

        
        
        /*
         *
         * [ regenerateSecureKey ] 
         *_____________________________________________________
         *
         *
         */

        public function regenerateSecureKey()
        {
               
                $this->_data['secure_key'] = md5( uniqid(rand(), TRUE) ) . '-' . time(); // To be put in cookie
                $this->_modified[] = 'secure_key';

                return $this->_data['secure_key'];
        }


        
        
        /*
         *
         * [ setCookie ] 
         *_____________________________________________________
         *
         *
         */
        
        public function setCookie () 
        {
                // Make sure cookie is fresh...      
                setcookie(
                    '_CART', // Name
                    $this->getSecureKey(), // Value
                    COOKIE_EXPIRES, // Expires
                    COOKIE_PATH, // Path
                    COOKIE_DOMAIN, // Domain
                    COOKIE_SECURE, // Is secure
                    TRUE // HTTP Only
                    );

        }





        public function saveData () 
	{
		

		if (empty($this->_modified)) return;
                
                $this->_modified = array_unique($this->_modified);
                
		$_update_params = array();
		$key_val_pairs = array();
		
		foreach ($this->_modified as $key) $key_val_pairs[$key] = $key . ' = :' . $key;
		
	
		// Do prepared statement for 'INSERT'...
		$update_handle = $this->dbh->prepare('
                                                        UPDATE cart 
                                                        SET ' . implode (', ', $key_val_pairs) . ', modified=now()       
                                                        WHERE cart_id = :instance_id 
                                                        ');
                
                $_update_params['cart_id'] = $this->_data['cart_id'];                
		$update_handle->bindParam(':instance_id', $_update_params['cart_id'], \PDO::PARAM_INT);

                
		foreach ($this->_modified as $key) {
                    
                        $_update_params[$key] = $this->_data[$key];                        
			$update_handle->bindParam(':'.$key, $_update_params[$key], \Cataleya\Helper\DBH::getTypeConst($_update_params[$key]));

		}
		
		

		
		if (!$update_handle->execute()) $this->e->triggerException('Error in class (' . __CLASS__ . '): [ ' . implode(', ', $update_handle->errorInfo()) . ' ] on line ' . __LINE__);
		
        
        \Cataleya\System\Event::notify('customer/cart/updated', $this, array ());	
        
	}
	
	
       
 
 
        
        
    
    
    public function current()
    {
        return \Cataleya\Customer\Cart\Item::load($this->_collection[$this->_position]['cart_item_id']);
        
    }

        
        
        
        
        
        /*
         * 
         * [ loadCartItems ]
         * 
         * 
         */
        
        private function loadCartItems () 
        {
                            
                
		// LOAD: CART_ITEMS
		static 
                        $instance_select, 
                        $param_cart_id;
		
			
		if (!isset($instance_select)) {
                    
			// PREPARE SELECT STATEMENT...
			$instance_select = $this->dbh->prepare('
                                                                    SELECT cart_item_id, product_id, option_id FROM cart_items 
                                                                    WHERE cart_id = :cart_id'
                                                                    );
			$instance_select->bindParam(':cart_id', $param_cart_id, \PDO::PARAM_INT);
		}
		

                $param_cart_id = $this->_data['cart_id']; 
                        
		
		if (!$instance_select->execute()) $this->e->triggerException('
                                                                                    Error in class (' . __CLASS__ . '): [ ' . 
                                                                                    implode(', ', $instance_select->errorInfo()) . 
                                                                                    ' ] on line ' . __LINE__);
                
		
                $this->_collection = $instance_select->fetchAll(\PDO::FETCH_ASSOC);
                
                return TRUE;
                            
        }
        
        
        
        
        
        
        

        /*
         * 
         *  [ addItem ]
         * ___________________________________________
         * 
         * ARGS:
         *          @product_obj, @product_option_obj
         * 
         * 
         */
        
        
        public function getItem ($id = NULL) 
        {
            
            if ($id == 0 || filter_var($id, FILTER_VALIDATE_INT) == FALSE) return NULL;
            
            
            
            // Check if item is in cart...
            foreach ($this->_collection as $_item_data) 
            {
                if ((int)$id === (int)$_item_data['cart_item_id']) 
                {
                    // Item in cart...
                    return \Cataleya\Customer\Cart\Item::load($_item_data['cart_item_id']);
                }
            }            
            
            // Item not in cart...
            return NULL;
            
        }
        

        

        
        
        
        

        /*
         * 
         *  [ getItemByAttrib ]
         * ___________________________________________
         * 
         * @param \Cataleya\Catalog\Product\Option $_Option
         * 
         * 
         */
        
        
        public function getItemByAttrib (\Cataleya\Catalog\Product\Option $_Option) 
        {
            

            
            // Check if item is in cart...
            foreach ($this->_collection as $_item_data) 
            {
                if ((int)$_Option->getProductId() === (int)$_item_data['product_id'] && (int)$_Option->getID() === (int)$_item_data['option_id'])
                {
                    // Item in cart...
                    return \Cataleya\Customer\Cart\Item::load($_item_data['cart_item_id']);
                }
            }            
            
            // Item not in cart...
            return NULL;
            
        }
        

        
        
        


        /*
         * 
         *  [ addItem ]
         * ___________________________________________
         * 
         * ARGS:
         *          @product_id, @option_id
         * 
         * 
         */
        
        
        public function addItem (\Cataleya\Catalog\Product\Option $_ProductOption, $quantity = 1) 
        {
            
            $option_id = $_ProductOption->getID();

            
            
            // Check if it's already been added...
            foreach ($this->_collection as $_item_data) 
            {
                 
                if ($option_id === $_item_data['option_id'])
                {
                    $cart_item = \Cataleya\Customer\Cart\Item::load($_item_data['cart_item_id']);
                    $cart_item->updateQuantity((float)$quantity);
                    return $cart_item;
                }
            }            
            
            
            
            
            $params = array (
                'cart_id'   =>  $this->_data['cart_id'], 
                'product_id'    =>  $_ProductOption->getProductId(), 
                'option_id' =>  $option_id, 
                'store_id'  =>  $this->getStoreId()
            );
            
            // Create cart_item...
            $cart_item = \Cataleya\Customer\Cart\Item::create($params);
            $cart_item->setQuantity((float)$quantity);
            
            // if successful, add to items _collection...
            if ($cart_item !== NULL) 
            {
                $params['cart_item_id'] = $cart_item->getID();
                $this->_collection[] = $params;
                $this->pageSetup();
                
                
                \Cataleya\System\Event::notify('customer/cart/item/added', $cart_item, array ());
                return $cart_item;
            } else {
                return NULL;
            }
            
        }
        


        
       


        /*
         * 
         *  [ removeItem ]
         * ___________________________________________
         * 
         * ARGS:
         *          @cart_item_id
         * 
         * 
         */
        
        
        public function removeItem ($id = NULL) 
        {
            
            if ($id == 0 || filter_var($id, FILTER_VALIDATE_INT) === FALSE) $this->e->triggerException('Error in class (' . __CLASS__ . '): [ invalid argument for method: ' . __FUNCTION__ . ' ] on line ' . __LINE__);

            
 
            
            // DELETE FROM DATABASE
            // Note: more straight forward than loading the cart_item first and then doing: $cart_item->delete()
            static $instance_delete, $instance_delete_param_instance_id;
            
            if (empty($instance_delete))
            {
                $instance_delete = $this->dbh->prepare('DELETE FROM cart_items WHERE cart_item_id = :instance_id');
                $instance_delete->bindParam(':instance_id', $instance_delete_param_instance_id, \PDO::PARAM_INT);
            }
            
            $instance_delete_param_instance_id = $id;
            
            if (!$instance_delete->execute()) $this->e->triggerException('
                                                                            Error in class (' . __CLASS__ . '): [ ' . 
                                                                            implode(', ', $instance_delete->errorInfo()) . 
                                                                            ' ] on line ' . __LINE__);

            // Remove from _collection array...
            $_cache = $this->_collection;
            $this->_collection = array ();
            
            foreach ($_cache as $_item_data) 
            {
                if ((int)$id === (int)$_item_data['cart_item_id']) continue;
                $this->_collection[] = $_item_data;
            }
            
					
            \Cataleya\System\Event::notify('customer/cart/item/removed', null, array ('id'=>$id));	

            $this->pageSetup();
            
            
        }
            



        
        


        /*
         *
         * [ getCustomerId ] 
         *_____________________________________________________
         *
         *
         */

        public function getCustomerId()
        {
                return $this->_data['customer_id'];
        }



        /*
         *
         * [ setCustomerId ] 
         *_____________________________________________________
         *
         *
         */

        public function setCustomerId($value = NULL)
        {
                if ($value === NULL || !is_numeric($value)) return FALSE;
                
                $this->_data['customer_id'] = $value;
                $this->_modified[] = 'customer_id';

                return TRUE;
        }
        
        
        
        /*
         *
         * [ unsetCustomerId ] 
         *_____________________________________________________
         *
         *
         */

        public function unsetCustomerId()
        {
               
                $this->_data['customer_id'] = NULL;
                $this->_modified[] = 'customer_id';

                return TRUE;
        }
        
        
        
        

        /*
         *
         * [ getCustomer ] 
         *_____________________________________________________
         *
         *
         */

        public function getCustomer()
        {
                return \Cataleya\Customer::load($this->_data['customer_id']);
        }


        /*
         *
         * [ setCustomer ] 
         *_____________________________________________________
         *
         *
         */

        public function setCustomer(\Cataleya\Customer $_Customer)
        {

                $this->_data['customer_id'] = $_Customer->getCustomerId();
                $this->_modified[] = 'customer_id';

                return $this;
        }
        
        
        
        
        
        
        
        
        


        /*
         *
         * [ getStoreId ] 
         *_____________________________________________________
         *
         *
         */

        public function getStoreId()
        {
                return $this->_data['store_id'];
        }
        
        
        
        /*
         *
         * [ getStore ] 
         *_____________________________________________________
         *
         *
         */

        public function getStore()
        {
                return \Cataleya\Store::load($this->_data['store_id']);
        }

        
        
        
        
        /*
         *
         * [ calcShippingCharges ] 
         *_____________________________________________________
         *
         *
         */

        public function calcShippingCharges(\Cataleya\Shipping\ShippingOption $_ShippingOption, $_country_or_state = NULL, $_TAX_INCLUDED = TRUE)
        {

                if (!($_country_or_state instanceof \Cataleya\Geo\Country) && !($_country_or_state instanceof \Cataleya\Geo\Province)) 
                    $this->e->triggerException('Error in class (' . __CLASS__ . '): [ The 2nd argument of ->getTaxAmount() must be an instance of _Geo_Country or an instance of _Geo_Province  ] on line ' . __LINE__);
		

                $_total = $_tax_total = 0;
                $_tax_json = array ();

                $_rate = $_ShippingOption->getRate();
                $_type = $_ShippingOption->getRateType();

                // Calculate shipping cost
                
                switch ($_type)
                {
                    case 'unit': foreach ($this as $_order_item) { $_total += ($_order_item->getQuantity() * $_rate); } break;
                    case 'flat': foreach ($this as $_order_item) { $_total += $_rate; } break;
                    case 'weight': foreach ($this as $_order_item) { $_total += ($_order_item->getProductOption()->getWeight() * $_rate); } break;
                    default: foreach ($this as $_order_item) { $_total += ($_order_item->getQuantity() * $_rate); }
                }
                

                // Calculate shipping tax

                if ($_ShippingOption->isTaxable()) 
                {
                    $_TaxRules = \Cataleya\Tax\TaxRules::load('shipping');

                    foreach ($_TaxRules as $_TaxRule) 
                    {
                        $_amount = $_TaxRule->calculateTaxAmount($this->getStore(), $_total, $_country_or_state);
                        $_tax_total += $_amount;
                
                        
                        if ($_amount > 0) $_tax_json[] = array (
                                                                    'id'=>$_TaxRule->getID(), 
                                                                    'description'=>($_TaxRule->getDescription()->getTitle('EN') . ' (' . $_TaxRule->getRate() . '%)'), 
                                                                    'amount' =>  $_amount, 
                                                                    'pretty_amount' => number_format ($_amount, 2)
                                                                );
                    }


                }
                
                
                $_shipping_cost_json = array (
                    'amount' => $_total, 
                    'amount_tax_included' => $_total + $_tax_total, 
                    'tax_amount' => $_tax_total, 
                    'Taxes' => $_tax_json, 
                    'name' => $_ShippingOption->getDescription()->getTitle('EN'), 
                    'description' => $_ShippingOption->getDescription()->getText('EN')
                );
                
                return $_shipping_cost_json;
                
        }

        
        
        
        
      
        
        /*
         *
         * [ getTaxes ] 
         *_____________________________________________________
         *
         *
         */

        public function getTaxes($_country_or_state = NULL)
        {
            
            if (!($_country_or_state instanceof \Cataleya\Geo\Country) && !($_country_or_state instanceof \Cataleya\Geo\Province)) 
                $this->e->triggerException('Error in class (' . __CLASS__ . '): [ The 2nd argument of ->getTaxAmount() must be an instance of _Geo_Country or an instance of _Geo_Province  ] on line ' . __LINE__);

            $_taxes = array ();
            
            foreach ($this as $_CartItem) 
            {
                
                $_tax_json = $_CartItem->getTaxes($_country_or_state);
                
                
                foreach ($_tax_json as $_TaxRule) {
                    $_id = $_TaxRule['id'];
                    
                    if (isset($_taxes[$_id])) $_taxes[$_id]['amount'] += (float)$_TaxRule['amount'];
                    else $_taxes[$_id] = array (
                        'id' =>  $_id, 
                        'description'=>$_TaxRule['description'],
                        'amount'=>(float)$_TaxRule['amount']
                    );
                    
                    $_taxes[$_id]['pretty_amount'] = number_format($_taxes[$_id]['amount'], 2);
                }
                
            }
            
            $_taxes = array_values($_taxes);
            return $_taxes;
        }
        
        
        



        public function addDiscount(array $_discount, $_replace = FALSE)
        {
            $_params = \Cataleya\Helper\Validator::params([
                'app_handle' => 'handle', 
                'label' => 'name', 
                'description' => 'text',
                'code' => 'alphanum',  
                'type' => 'match:/^(percent|currency)$/i', // or 'currency'
                'value' => 'float', 
                'currency_code' => 'iso3', 
                'less' => 'float'
                ], $_discount
            );

            $_bad_params = [];
            foreach ($_params as $k=>$v) if ($v === false) $_bad_params[] = $k;

            if (!empty($_bad_params)) {
                throw new \Cataleya\Error ('Discount Error: Bad params [ ' . implode(', ', $_params) . ' ]');
            }




            if (!empty($this->_data['discount_json'])) {
                $this->_discounts = json_decode($this->_data['discount_json'], true);
            }

            $_handle = $_params['app_handle'];
            $_code = $_params['discount_code'];
            if (
                isset($this->_discounts[$_handle]) && 
                ($_replace !== TRUE || isset($this->_discounts[$_handle][$_code]))
            ) return FALSE;
            
            if (!isset($this->_discounts[$_handle])) $this->_discounts[$_handle] = [];
            $this->_discounts[$_handle][$_code] = $_params;

            $this->_data['discount_json'] = json_encode($this->_discounts);
            $this->_modified[] = 'discount_json';
            
            return TRUE;
        }
        
        
        
        
       
        
        /*
         *
         * [ getDiscounts ] 
         *_____________________________________________________
         *
         *
         */

        public function getDiscounts()
        {
            if (empty($this->_discounts) && !empty($this->_data['discount_json'])) {
                $this->_discounts = json_decode($this->_data['discount_json'], true);
            }

            return $this->_discounts;
        }
        
        
        
        
        /*
         *
         * [ getTotal ] 
         *_____________________________________________________
         *
         *
         */

        public function getTotal($_Exclude_Discounts = false)
        {
            $_total = 0;
            
            foreach ($this as $_CartItem)  $_total += $_CartItem->getSubtotal($_Exclude_Discounts);
            
            return $_total;
            
            
        
        }
        
        
        
        
        
        
        
        
        
        
        
        
        /* =========================== CHECKOUT ===================== */
        
        


        /**
         * 
         * @param boolean $_GMT
         * @return \DateTime
         * 
         */
        public function getCheckoutDate($_GMT = TRUE)
        {
            if (!is_bool($_GMT)) $this->e->triggerException('Error in class (' . __CLASS__ . '): [ invalid argument for method: ' . __FUNCTION__ . ' ] on line ' . __LINE__);

            $_DateTime = new \DateTime($this->_data['date_created']);
            if (!$_GMT) $_DateTime->setTimezone($this->getStore()->getTimezone());

            return $_DateTime;
        }







        /**
         * 
         * @param boolean $_GMT
         * @return \DateTime
         * 
         */
        public function getDateModified($_GMT = TRUE)
        {
            if (!is_bool($_GMT)) $this->e->triggerException('Error in class (' . __CLASS__ . '): [ invalid argument for method: ' . __FUNCTION__ . ' ] on line ' . __LINE__);

            $datetime = new \DateTime($this->_data['date_modified']);
            if (!$_GMT) $datetime->setTimezone($this->getStore()->getTimezone());

            return $datetime;
        }




        
        
        

        /**
         * 
         * @param \Cataleya\Shipping\ShippingOption $_ShippingOption
         * @return \Cataleya\Customer\Cart
         * 
         */
        public function setShippingOption(\Cataleya\Shipping\ShippingOption $_ShippingOption)
        {

                $this->_data['shipping_option_id'] = $_ShippingOption->getID();
                $this->_modified[] = 'shipping_option_id';

                return $this;
        }
        
        
        


        /**
         * 
         * @return \Cataleya\Shipping\ShippingOption
         * 
         */
        public function getShippingOption()
        {
            if (empty($this->_shipping_option)) $this->_shipping_option = \Cataleya\Shipping\ShippingOption::load ($this->_data['shipping_option_id']);
            
            return $this->_shipping_option;
        }


        
        
        
        


        /**
         * 
         * @param string $_payment_type     This is a plugin key for the plugin (obviously) that will 
         *                                  handle payment option selected by customer. Eg. Plugin key for 
         *                                  PayPal Express plugin.
         * 

         * @return \Cataleya\Customer\Cart
         * 
         */
        public function setPaymentType($_payment_type)
        {

                $this->_data['payment_processor_id'] = strtolower(trim($_payment_type));
                $this->_modified[] = 'payment_processor_id';

                return $this;
        }
        
        
        


        /**
         * 
         * @return string   This is a plugin key for the plugin (obviously) that will 
         *                  handle payment option selected by customer. Eg. Plugin key for 
         *                  PayPal Express plugin.
         * 
         */
        public function getPaymentTypeID()
        {
            return $this->_data['payment_processor_id'];
            
        }


        public function getPaymentType () 
        {
            $_payment_plugin_key = $this->getPaymentTypeID();
            if (empty($_payment_plugin_key)) { return null; }
            
            $_payment_options = \Cataleya\Plugins\Action::trigger(
                'payment.get-options', 
                [ 'params' => [ 'currency_codes' => [ $this->getCurrencyCode() ] ] ], 
                [ $_payment_plugin_key ]
            );

            if (!empty($_payment_options)) { 
                return $_payment_options[0];
            }

            // Invalid payment plugin key (may happen if plugin is deleted)
            $this->_data['payment_processor_id'] = "";
            $this->_modified[] = 'payment_processor_id';

            return null;
        }




        



        /*
         *
         * [ getCurrencyCode ] 
         *_____________________________________________________
         *
         *
         */

        public function getCurrencyCode()
        {
                return $this->_data['currency_code'];
        }



        /*
         *
         * [ getCurrency ] 
         *_____________________________________________________
         *
         *
         */

        public function getCurrency()
        {

                if (empty($this->_currency)) $this->_currency = \Cataleya\Locale\Currency::load($this->_data['currency_code']);
                return $this->_currency;
        }



        
        /*
         *
         * [ getShippingDescription ] 
         *_____________________________________________________
         *
         *
         */

        public function getShippingDescription()
        {
                return $this->_data['shipping_description'];
        }

   
        
        
        

        /**
         * 
         * @param \Cataleya\Asset\Contact $_Contact
         * @return \Cataleya\Customer\Cart
         * 
         */
        public function setShippingAddress (\Cataleya\Asset\Contact $_Contact)
        {
                $this->getShippingAddress()->copy($_Contact);
                
                return $this;
        }
        
        
        
        

        /**
         * 
         * @return \Cataleya\Asset\Contact
         * 
         */
        public function getShippingAddress()
        {
            
                if (empty($this->_shipping_address)) $this->_shipping_address = \Cataleya\Asset\Contact::load ($this->_data['shipping_address_id']);
                return $this->_shipping_address;
        }
        
        
        
        

        /**
         * 
         * Alias for \Cataleya\Customer\Cart::setShippingAddress ()
         * 
         * @param \Cataleya\Asset\Contact $_Contact
         * @return \Cataleya\Customer\Cart
         * 
         */
        public function setRecipient (\Cataleya\Asset\Contact $_Contact)
        {
                return $this->setShippingAddress($_Contact);
        }
        
        



        /**
         * 
         * Alias for \Cataleya\Customer\Cart::getShippingAddress ()
         * 
         * @return \Cataleya\Asset\Contact
         * 
         */
        public function getRecipient()
        {
                return $this->getShippingAddress();
        }
        
        
        


        /**
         * 
         * @param \Cataleya\Asset\Contact $_Contact
         * @return \Cataleya\Customer\Cart
         * 
         */
        public function setBillingAddress (\Cataleya\Asset\Contact $_Contact)
        {
                $this->getBillingAddress()->copy($_Contact);
                
                return $this;
        }
        


        /**
         * 
         * @return \Cataleya\Asset\Contact
         * 
         */
        public function getBillingAddress()
        {
                if (empty($this->_billing_address)) $this->_billing_address = \Cataleya\Asset\Contact::load ($this->_data['billing_address_id']);
                return $this->_billing_address;
        }
        
        
        
        /**
         * 
         * @return boolean
         * 
         */
        public function allInStock() {
            
            $_Store = $this->getStore();
            
            foreach ($this as $_CartItem) {

                if (!$_CartItem->inStock($_Store)) {
                   return false;
                   break;
                }

            }
            
            return true;
                    
        }
        
        
        
        
        /**
         * 
         * @return boolean
         * 
         */
        public function isEmpty() {
            

            return empty($this->_collection);
                    
        }
        


        
        /**
         * 
         * @return boolean
         * 
         */
        public function isReady()
        {                
                
                $_failed_items = array ();
                
                
                // 1. shipping address...
                $_ShippingAddress = $this->getShippingAddress();
                
                    ($_ShippingAddress->getEntryFirstname() === "") ? $_failed_items[] = 'Shipping: First Name' : TRUE;
                    ($_ShippingAddress->getEntryLastname() === "") ? $_failed_items[] = 'Shipping: Last Name' : TRUE;

                    (NULL === $_ShippingAddress->getCountry()) ? $_failed_items[] = 'Shipping: Country' : TRUE;
                    ("" === $_ShippingAddress->getEntryCity()) ? $_failed_items[] = 'Shipping: City' : TRUE;
                    (NULL === $_ShippingAddress->getProvince() && "" === $_ShippingAddress->getEntryProvince()) ? $_failed_items[] = 'Shipping: Province' : TRUE;
                    ("" === $_ShippingAddress->getEntryPostcode()) ? $_failed_items[] = 'Shipping: Postal Code' : TRUE;
                
                
                // 2. Shipping option
                    (NULL === $this->getShippingOption()) ? $_failed_items[] = 'Shipping Option' : TRUE;
                
                    
                
                // 3. Payment option
                    $_PaymentType = $this->getPaymentType();
                    (NULL === $_PaymentType) ? $_failed_items[] = 'Payment Option' : TRUE;
                
                
                // 5. Any items out of stock ?

                    (!$this->allInStock()) ? $_failed_items[] = 'Out of stock.' : FALSE;
                    
                    
                    return empty($_failed_items) ? true : $_failed_items;
                    
        }
        
        
        
        
        
        
        
        
        
        
     


}
	
	
	
	
