<?php


namespace Cataleya\Customer;



/*
 *
 *	@Package: Cataleya
 *	@Class: \Cataleya\Customer\Wishlist
 *
 *	(c) 2012 Fancy Paper Planes
 *
 *
 *
 */




class Wishlist 
extends \Cataleya\Collection    
implements \Cataleya\System\Event\Observable 
{
	
	
	private $_data = array();
	private $_modified = array();
        protected $_collection = array();
        
	private $dbh, $e, $_customer;
	

	
	
    protected static $_events = [
            'customer/wishlist/created',
            'customer/wishlist/updated', 
            'customer/wishlist/deleted'
        ];


	/*
	 *
	 * [ __construct ]
	 * ________________________________________________________________
	 *
	 *
	 *
	 */
	 
	 
	public function __construct ()
	{
		
		// Get database handle...
                $this->dbh = \Cataleya\Helper\DBH::getInstance();
		
		// Get error handler
		$this->e = \Cataleya\Helper\ErrorHandler::getInstance();
		
	}
	




	/*
	 *
	 * [ __destruct ]
	 * ________________________________________________________________
	 *
	 *
	 *
	 */

        public function __destruct()
        {
                    $this->saveData();
        }
	




	/*
	 *
	 * [ load ]
	 * ________________________________________________________________
         * 
	 * Note: if @param $id is set to NULL or 0, a new wishlist will be created as returned.
	 *
	 *
	 */
	 
	 
	public static function load ($key = NULL)
	{
                
                if ($key === NULL || $key === 0) return self::create ();
		else if (!is_string($key)) return NULL;
		
		$instance = new \Cataleya\Customer\Wishlist ();
		
		// LOAD: WISHLIST
		static $instance_select, $param_instance_id;
			
		if (!isset($instance_select)) {
			// PREPARE SELECT STATEMENT...
			$instance_select = $instance->dbh->prepare('SELECT * FROM wishlists WHERE secure_key = :instance_id LIMIT 1');
			$instance_select->bindParam(':instance_id', $param_instance_id, \PDO::PARAM_STR);
		}

		$param_instance_id = $key;
                
		if (!$instance_select->execute()) $instance->e->triggerException('
												Error in class (' . __CLASS__ . '): [ ' . 
												implode(', ', $instance_select->errorInfo()) . 
												' ] on line ' . __LINE__);
		
		$instance->_data = $instance_select->fetch(\PDO::FETCH_ASSOC);
                if (empty($instance->_data)) return NULL; //self::create ();
                    
                $instance->loadWishlistItems();
               
                $instance->pageSetup();

		return $instance;	
		
		
	}



	/*
	 *
	 *  [ create ]
	 * ________________________________________________________________
	 * 
	 * 
	 *
	 *
	 */

	 
	static public function create (\Cataleya\Customer $_Customer) 
	{
		
		
		
		// Get database handle...
                $dbh = \Cataleya\Helper\DBH::getInstance();
		
		// Get error handler
		$e = \Cataleya\Helper\ErrorHandler::getInstance();
		
		// construct
		static 
                        $instance_insert, 
                        $param_secure_key, 
                        $param_customer_id;
		
		if (empty($instance_insert))
		{
			$instance_insert = $dbh->prepare('
                                                        INSERT INTO wishlists 
                                                        (customer_id, created, modified, status, secure_key) 
                                                        VALUES (:customer_id, NOW(), NOW(), 1, :secure_key)
                                                        ');
			$instance_insert->bindParam(':secure_key', $param_secure_key, \PDO::PARAM_STR);
			$instance_insert->bindParam(':customer_id', $param_customer_id, \PDO::PARAM_INT);
		}
                
                $param_secure_key = \Cataleya\Helper::uid(); 
                $param_customer_id = $_Customer->getCustomerId();
                
		if (!$instance_insert->execute()) $e->triggerException('
										Error in class (' . __CLASS__ . '): [ ' . 
										implode(', ', $instance_insert->errorInfo()) . 
										' ] on line ' . __LINE__);
										
		
		// AUTOLOAD AND RETURN
		$instance = self::load($param_secure_key);
		
		return $instance;
		
		
	}

    
    
    
    /**
     * getEvents
     * 
     * @return array
     */
    public static function getEvents() {

        return self::$_events;
    }


	 
	 /*
	 *  [ delete ]
	 * ________________________________________________________________
	 * 
	 * 
	 *
	 *
	 */

	 
	public function delete () 
	{
            
                // Delete rows containing 'wishlist_id' in specified tables
                \Cataleya\Helper\DBH::sanitize(array('wishlist_items', 'wishlists'), 'wishlist_id', $this->getID());

		
		\Cataleya\System\Event::notify('customer/wishlist/deleted', null, array ('id'=>$this->getID()));
		// $this = NULL;
		return TRUE;
		
		
	}



        
	/*
	 *
	 *  [ destroy ]: Alias for [ delete ]
	 * ________________________________________________________________
	 * 
	 * 
	 *
	 *
	 *
	 */
	 
	public function destroy () 
	{
            return $this->delete();
            
        }               
        
        
        
        


        /*
         *
         * [ getWishlistId ] 
         *_____________________________________________________
         *
         *
         */

        public function getWishlistId()
        {
                return $this->_data['wishlist_id'];
        }

        
        public function getID()
        {
                return $this->_data['wishlist_id'];
        }
        
        
        /*
         *
         * [ getSecureKey ] 
         *_____________________________________________________
         *
         *
         */

        public function getSecureKey()
        {
                return $this->_data['secure_key'];
        }

        
        
        /*
         *
         * [ regenerateSecureKey ] 
         *_____________________________________________________
         *
         *
         */

        public function regenerateSecureKey()
        {
               
                $this->_data['secure_key'] = md5( uniqid(rand(), TRUE) ) . '-' . time(); // To be put in cookie
                $this->_modified[] = 'secure_key';

                return $this->_data['secure_key'];
        }



	
	
	public function saveData () 
	{
		

		if (empty($this->_modified)) return;
                
                $this->_modified = array_unique($this->_modified);
                
		$_update_params = array();
		$key_val_pairs = array();
		
		foreach ($this->_modified as $key) $key_val_pairs[$key] = $key . ' = :' . $key;
		
	
		// Do prepared statement for 'INSERT'...
		$update_handle = $this->dbh->prepare('
                                                        UPDATE wishlists 
                                                        SET ' . implode (', ', $key_val_pairs) . ', modified=now()       
                                                        WHERE wishlist_id = :instance_id 
                                                        ');
                
                $_update_params['wishlist_id'] = $this->_data['wishlist_id'];                
		$update_handle->bindParam(':instance_id', $_update_params['wishlist_id'], \PDO::PARAM_INT);

                
		foreach ($this->_modified as $key) {
                    
                        $_update_params[$key] = $this->_data[$key];                        
			$update_handle->bindParam(':'.$key, $_update_params[$key], \Cataleya\Helper\DBH::getTypeConst($_update_params[$key]));

		}
		
		

		
		if (!$update_handle->execute()) $this->e->triggerException('Error in class (' . __CLASS__ . '): [ ' . implode(', ', $update_handle->errorInfo()) . ' ] on line ' . __LINE__);
		
        
		\Cataleya\System\Event::notify('customer/wishlist/saved', $this, array ());
        
        
        
	}
	
	
       
 
 
        
        
    
    
    public function current()
    {
        return \Cataleya\Customer\Wishlist\Item  ::load($this->_collection[$this->_position]['wishlist_item_id']);
        
    }

        
        
        
        
        
        /*
         * 
         * [ loadWishlistItems ]
         * 
         * 
         */
        
        private function loadWishlistItems () 
        {
                            
                
		// LOAD: WISHLIST_ITEMS
		static 
                        $instance_select, 
                        $param_wishlist_id;
		
			
		if (!isset($instance_select)) {
                    
			// PREPARE SELECT STATEMENT...
			$instance_select = $this->dbh->prepare('
                                                                    SELECT wishlist_item_id, product_id, option_id FROM wishlist_items 
                                                                    WHERE wishlist_id = :wishlist_id'
                                                                    );
			$instance_select->bindParam(':wishlist_id', $param_wishlist_id, \PDO::PARAM_INT);
		}
		

                $param_wishlist_id = $this->_data['wishlist_id']; 
                        
		
		if (!$instance_select->execute()) $this->e->triggerException('
                                                                                    Error in class (' . __CLASS__ . '): [ ' . 
                                                                                    implode(', ', $instance_select->errorInfo()) . 
                                                                                    ' ] on line ' . __LINE__);
                
		
                $this->_collection = $instance_select->fetchAll(\PDO::FETCH_ASSOC);
                
                return TRUE;
                            
        }
        
        
        
        
        
        
        

        /*
         * 
         *  [ addItem ]
         * ___________________________________________
         * 
         * ARGS:
         *          @product_obj, @product_option_obj
         * 
         * 
         */
        
        
        public function getItem ($id = NULL) 
        {
            
            if ($id == 0 || filter_var($id, FILTER_VALIDATE_INT) == FALSE) return NULL;
            
            
            
            // Check if item is in wishlist...
            foreach ($this->_collection as $_item_data) 
            {
                if ((int)$id === (int)$_item_data['wishlist_item_id']) 
                {
                    // Item in wishlist...
                    return \Cataleya\Customer\Wishlist\Item  ::load($_item_data['wishlist_item_id']);
                }
            }            
            
            // Item not in wishlist...
            return NULL;
            
        }
        

        

        
        
        
        

        /*
         * 
         *  [ getItemByAttrib ]
         * ___________________________________________
         * 
         * ARGS:
         *          @product_obj, @product_option_obj
         * 
         * 
         */
        
        
        public function getItemByAttrib ($product_id = NULL, $option_id = NULL) 
        {
            
            if (!is_numeric($product_id) || !is_numeric($option_id)) return NULL;
            
            
            
            // Check if item is in wishlist...
            foreach ($this->_collection as $_item_data) 
            {
                if ((int)$product_id === (int)$_item_data['product_id'] && (int)$option_id === (int)$_item_data['option_id'])
                {
                    // Item in wishlist...
                    return \Cataleya\Customer\Wishlist\Item  ::load($_item_data['wishlist_item_id']);
                }
            }            
            
            // Item not in wishlist...
            return NULL;
            
        }
        

        
        
        


        /*
         * 
         *  [ addItem ]
         * ___________________________________________
         * 
         * ARGS:
         *          \Cataleya\Customer\Option $_ProductOption
         * 
         * 
         */
        
        
        public function addItem (\Cataleya\Store $_Store, \Cataleya\Catalog\Product\Option $_ProductOption) 
        {
            
            $product_id = $_ProductOption->getProductId();
            $option_id = $_ProductOption->getID();
            
            $params = array (
                'wishlist_id'   =>  $this->_data['wishlist_id'], 
                'product_id'    =>  $product_id, 
                'option_id' =>  $option_id
            );
            
            
            // Check if it's already been added...
            foreach ($this->_collection as $_item_data) 
            {
                 
                if ($product_id === $_item_data['product_id'] && $option_id === $_item_data['option_id'])
                {
                    $wishlist_item = \Cataleya\Customer\Wishlist\Item::load($_item_data['wishlist_item_id']);
                    $wishlist_item->updateQuantity(1);
                    return $wishlist_item;
                }
            }            
            
            
            // Create wishlist_item...
            $wishlist_item = \Cataleya\Customer\Wishlist\Item::create($params);
            
            
            $_ProductOption->getProduct()->updateFavourites($_Store);
            
            // if successful, add to items _collection...
            if ($wishlist_item !== NULL) 
            {
                $params['wishlist_item_id'] = $wishlist_item->getID();
                $this->_collection[] = $params;
                $this->pageSetup();
                
		\Cataleya\System\Event::notify('customer/wishlist/item/added', $wishlist_item, array ());
                
                return $wishlist_item;
            } else {
                return NULL;
            }
            
        }
        


        
       


        /*
         * 
         *  [ removeItem ]
         * ___________________________________________
         * 
         * ARGS:
         *          @wishlist_item_id
         * 
         * 
         */
        
        
        public function removeItem ($_WishlistItem = NULL) 
        {
            
            $id = ($_WishlistItem instanceof \Cataleya\Customer\Wishlist\Item) 
            ? $_WishlistItem->getID () 
            : $_WishlistItem;
            
            if ($id == 0 || filter_var($id, FILTER_VALIDATE_INT) === FALSE) 
                    $this->e->triggerException(
                            'Error in class (' . __CLASS__ . 
                            '): [ invalid argument for method: ' . 
                            __FUNCTION__ . ' ] on line ' . __LINE__
                            );

            
 
            
            // DELETE FROM DATABASE
            // Note: more straight forward than loading the wishlist_item first and then doing: $wishlist_item->delete()
            static $instance_delete, $instance_delete_param_instance_id;
            
            $instance_delete_param_instance_id = $id;
            
            if (empty($instance_delete))
            {
                $instance_delete = $this->dbh->prepare('DELETE FROM wishlist_items WHERE wishlist_item_id = :instance_id');
                $instance_delete->bindParam(':instance_id', $instance_delete_param_instance_id, \PDO::PARAM_INT);
            }
            
            if (!$instance_delete->execute()) $this->e->triggerException('
                                                                            Error in class (' . __CLASS__ . '): [ ' . 
                                                                            implode(', ', $instance_delete->errorInfo()) . 
                                                                            ' ] on line ' . __LINE__);


            // Remove from _collection array...
            $_cache = $this->_collection;
            $this->_collection = array ();
            
            foreach ($_cache as $_item_data) 
            {
                if ((int)$id === (int)$_item_data['wishlist_item_id']) continue;
                $this->_collection[] = $_item_data;
            }
            

            \Cataleya\System\Event::notify('customer/wishlist/item/removed', null, array ('id'=>$id));
            $this->pageSetup();
            
        }
            

        
        
        



        /*
         * 
         *  [ removeAllItems ]
         * ___________________________________________
         * 
         * 
         * 
         */
        
        
        public function removeAllItems () 
        {

            
            // DELETE FROM DATABASE
            // Note: more straight forward than loading the wishlist_item first and then doing: $wishlist_item->delete()
            static $instance_delete, $instance_delete_param_instance_id;
            
            $instance_delete_param_instance_id = $this->getWishlistId();
            
            if (empty($instance_delete))
            {
                $instance_delete = $this->dbh->prepare('DELETE FROM wishlist_items WHERE wishlist_id = :instance_id');
                $instance_delete->bindParam(':instance_id', $instance_delete_param_instance_id, \PDO::PARAM_INT);
            }
            
            if (!$instance_delete->execute()) $this->e->triggerException('
                                                                            Error in class (' . __CLASS__ . '): [ ' . 
                                                                            implode(', ', $instance_delete->errorInfo()) . 
                                                                            ' ] on line ' . __LINE__);

            // Remove from _collection array...

            $this->_collection = array();
            $this->pageSetup();
            
        }
            

        
        


        /*
         *
         * [ getCustomerId ] 
         *_____________________________________________________
         *
         *
         */

        public function getCustomerId()
        {
                return $this->_data['customer_id'];
        }



        /*
         *
         * [ getCustomer ] 
         *_____________________________________________________
         *
         *
         */

        public function getCustomer()
        {
                if (empty($this->_customer)) $this->_customer = \Cataleya\Customer::load ($this->_data['customer_id']);
                return $this->_customer;
        }



        
        


        /*
         *
         * [ getStoreId ] 
         *_____________________________________________________
         *
         *
         */

        public function getStoreId()
        {
                return $this->_data['store_id'];
        }
        
        
        
        /*
         *
         * [ getStore ] 
         *_____________________________________________________
         *
         *
         */

        public function getStore()
        {
                return \Cataleya\Store::load($this->_data['store_id']);
        }


        
        
        /**
         * 
         * @return boolean
         * 
         */
        public function isEmpty() {
            

            return empty($this->_collection);
                    
        }
        



}
	
	
	
	
