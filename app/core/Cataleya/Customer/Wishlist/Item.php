<?php


namespace Cataleya\Customer\Wishlist;



/*
 *
 *	@Package: Cataleya
 *	@Class: \Cataleya\Customer\Wishlist\Item  
 *
 *	(c) 2012 Fancy Paper Planes
 *
 *
 *
 */




class Item     
implements \Cataleya\System\Event\Observable 
{
	
	
	private $_data = array();
	private $_modified = array();
        
        private $_product_obj = NULL;
        private $_option_obj = NULL;
        
	private $dbh, $e;
	

	
    protected static $_events = [
            'customer/wishlist/item/added',
            'customer/wishlist/item/updated', 
            'customer/wishlist/item/removed'
        ];
	


	/*
	 *
	 * [ __construct ]
	 * ________________________________________________________________
	 *
	 *
	 *
	 */
	 
	 
	private function __construct ()
	{
		
		// Get database handle...
                $this->dbh = \Cataleya\Helper\DBH::getInstance();
		
		// Get error handler
		$this->e = \Cataleya\Helper\ErrorHandler::getInstance();
		
	}
	




	/*
	 *
	 * [ __destruct ]
	 * ________________________________________________________________
	 *
	 *
	 *
	 */

        public function __destruct()
        {
                    $this->saveData();
        }
	




	/*
	 *
	 * [ load ]
	 * ________________________________________________________________
         * 
	 *
	 *
	 */
	 
	 
	public static function load ($id = NULL)
	{
                
               
                // Check data
		if ($id == 0 || filter_var($id, FILTER_VALIDATE_INT) === FALSE) return NULL;
                

                
		$instance = new \Cataleya\Customer\Wishlist\Item   ();
		
		// LOAD: WISHLIST_ITEM
		static 
                        $instance_select, 
                        $param_instance_id;
		
			
		if (!isset($instance_select)) {
                    
			// PREPARE SELECT STATEMENT...
			$instance_select = $instance->dbh->prepare('
                                                                    SELECT * FROM wishlist_items 
                                                                    WHERE wishlist_item_id = :wishlist_item_id 
                                                                    LIMIT 1'
                                                                    );
			$instance_select->bindParam(':wishlist_item_id', $param_instance_id, \PDO::PARAM_INT);
		}
		

                $param_instance_id = $id;
                        
                        
		
		if (!$instance_select->execute()) $instance->e->triggerException('
                                                                                    Error in class (' . __CLASS__ . '): [ ' . 
                                                                                    implode(', ', $instance_select->errorInfo()) . 
                                                                                    ' ] on line ' . __LINE__);
		
		$instance->_data = $instance_select->fetch(\PDO::FETCH_ASSOC);
                
                if (empty($instance->_data)) return NULL;
                

		return $instance;	
		
		
	}



        
        
        
        
	/*
	 *
	 *  [ create ]
	 * ________________________________________________________________
	 * 
	 * 
	 *
	 *
	 */

	 
	static public function create ($params = array ('wishlist_id'=>NULL, 'product_id'=>NULL, 'option_id'=>NULL)) 
	{
		
                // Check data
		if (
                        !is_array($params) 
                        || !isset($params['wishlist_id'], $params['product_id'], $params['option_id']) 
                        || !is_numeric($params['wishlist_id']) 
                        || !is_numeric($params['product_id']) 
                        || !is_numeric($params['option_id']) 
                        
                        ) return NULL;
		 

		
		// Get database handle...
                $dbh = \Cataleya\Helper\DBH::getInstance();
		
		// Get error handler
		$e = \Cataleya\Helper\ErrorHandler::getInstance();
		
		// construct
		static 
				$instance_insert,  
                                $param_wishlist_id, 
                                $param_product_id,  
                                $param_option_id;
		
		if (empty($instance_insert))
		{
			$instance_insert = $dbh->prepare('
                                                        INSERT INTO wishlist_items 
                                                        (wishlist_id, product_id, option_id, date_added) 
                                                        VALUES (:wishlist_id, :product_id, :option_id, NOW())
                                                        ');
															
			$instance_insert->bindParam(':wishlist_id', $param_wishlist_id, \PDO::PARAM_INT);
                        $instance_insert->bindParam(':product_id', $param_product_id, \PDO::PARAM_INT);
                        $instance_insert->bindParam(':option_id', $param_option_id, \PDO::PARAM_INT);
		}
		


                $param_wishlist_id = $params['wishlist_id']; 
                $param_product_id = $params['product_id'];  
                $param_option_id = $params['option_id'];



                if (!$instance_insert->execute()) $e->triggerException('
										Error in class (' . __CLASS__ . '): [ ' . 
										implode(', ', $instance_insert->errorInfo()) . 
										' ] on line ' . __LINE__);
										
		
		// AUTOLOAD NEW ITEM AND RETURN IT
		$instance = self::load($dbh->lastInsertId());
		
		return $instance;
		
		
	}


        
        
    
    /**
     * getEvents
     * 
     * @return array
     */
    public static function getEvents() {

        return self::$_events;
    }
        
        

	 
	 /*
	 *  [ delete ]
	 * ________________________________________________________________
	 * 
	 * 
	 *
	 *
	 */

	 
	public function delete () 
	{
		
			// DELETE 
			$instance_delete_param_wishlist_id = $this->_data['wishlist_id'];
                        $instance_delete_param_product_id = $this->_data['product_id'];
                        $instance_delete_param_option_id = $this->_data['option_id'];
                        
			$instance_delete = $this->dbh->prepare('
                                                                    DELETE FROM wishlist_items 
                                                                    WHERE wishlist_id = :wishlist_id 
                                                                    AND product_id = :product_id 
                                                                    AND option_id = :option_id 
                                                                    ');
			$instance_delete->bindParam(':wishlist_id', $instance_delete_param_wishlist_id, \PDO::PARAM_INT);
                        $instance_delete->bindParam(':product_id', $instance_delete_param_product_id, \PDO::PARAM_INT);
                        $instance_delete->bindParam(':option_id', $instance_delete_param_option_id, \PDO::PARAM_INT);
			
	
			if (!$instance_delete->execute()) $this->e->triggerException('
											Error in class (' . __CLASS__ . '): [ ' . 
											implode(', ', $instance_delete->errorInfo()) . 
											' ] on line ' . __LINE__);
                        
                        
											
			

		return TRUE;
		
		
	}



        

        /*
         *
         * [ updateQuantity ] 
         *_____________________________________________________
         *
         *
         */

        public function updateQuantity($value = NULL)
        {
                if (filter_var($value, FILTER_VALIDATE_INT) === FALSE) return FALSE;
                
                $this->_data['quantity'] = (int)$this->_data['quantity'] + (int)$value;
                $this->_modified[] = 'quantity';
                

                return TRUE;
        }
        
        /*
         *
         * [ setQuantity ] 
         *_____________________________________________________
         *
         *
         */

        public function setQuantity($value = NULL)
        {
                $value = filter_var($value, FILTER_VALIDATE_FLOAT);
                if ($value === FALSE) $this->e->triggerException('Error in class (' . __CLASS__ . '): [ Argument supplied in method: ' . __FUNCTION__ . ' must be a float or integer  ] on line ' . __LINE__);


                $this->_data['quantity'] = $value;
                $this->_modified[] = 'quantity';

                return $this;
        }
        
        


        
        
         /*
         *
         * [ getQuantity ] 
         *_____________________________________________________
         *
         *
         */

        public function getQuantity()
        {
                return (float)$this->_data['quantity'];
        }

       
        
        
        
        
        /*
         *
         * [ getProduct ] 
         *_____________________________________________________
         *
         *
         */

        public function getProduct()
        {
                if ($this->_product_obj === NULL) $this->_product_obj = \Cataleya\Catalog\Product::load ($this->_data['product_id']);
                return $this->_product_obj;
        }


        
        
        /*
         *
         * [ getProductOption ] 
         *_____________________________________________________
         *
         *
         */

        public function getProductOption()
        {
                if ($this->_option_obj === NULL) $this->_option_obj = \Cataleya\Catalog\Product\Option::load ($this->_data['option_id']);
                return $this->_option_obj;
        }

        


        
        
        
        
        


        /*
         *
         * [ getProductID ] 
         *_____________________________________________________
         *
         *
         */

        public function getProductId()
        {
                return $this->_data['product_id'];
        }


        
        
        /*
         *
         * [ getProductOptionID ] 
         *_____________________________________________________
         *
         *
         */

        public function getProductOptionId()
        {
                return $this->_data['option_id'];
        }
        
        

     
        
        
        /*
         *
         * [ getCost ] 
         *_____________________________________________________
         *
         * Returns price WITH sale discounts applied (if any)
         *
         */

        public function getCost(\Cataleya\Store $_Store, $_Exclude_Sale = FALSE)
        {
            
            // $item_product = $this->getProduct();
            $_store_id = $_Store->getID();
            
            $_cost = $this->getProductOption()->getPrice()->getValue($_store_id);

            
            // Calculate sale discount
            $_sale = $this->getProduct()->isOnSale($_Store, true);
            if ($_sale !== FALSE && $_Exclude_Sale !== TRUE) $_cost = $_sale->computeDiscountPrice($_cost);
            

            return $_cost;
            
        }
        
        
        
        
        /*
         *
         * [ getSubtotal ] 
         *_____________________________________________________
         *
         * Returns price WITH coupon discounts applied (if any)
         *
         */

        public function getSubtotal(\Cataleya\Store $_Store, $_Exclude_Sale = FALSE) 
        {
            return ($this->getQuantity() * $this->getCost($_Store, $_Exclude_Sale));
        }
        
        
        
        
        
        /*
         *
         * [ getID ] 
         *_____________________________________________________
         *
         *
         */

        public function getID()
        {
                return $this->_data['wishlist_item_id'];
        }

        
        
        
        /*
         *
         * [ getWishlistID ] 
         *_____________________________________________________
         *
         *
         */

        public function getWishlistId()
        {
                return $this->_data['wishlist_id'];
        }
        
        
        


        /*
         *
         * [ getWishlist ] 
         *_____________________________________________________
         *
         *
         */

        public function getWishlist()
        {
                return \Cataleya\Customer\Wishlist::load($this->_data['wishlist_id']);
        }
        
        
        
        
        
        


        /*
         *
         * [ inStock ] 
         *_____________________________________________________
         *
         *
         */

        public function inStock($_Store = NULL)
        {
                if (empty($_Store) || !($_Store instanceof \Cataleya\Store)) $_Store = $this->getStore ();
                $stock = (float)$this->getProductOption()->getStock()->getValue($_Store);
                
                return ((float)$this->getQuantity() <= $stock);
        }
        
        
      


        /*
         *
         * [ getStock ] 
         *_____________________________________________________
         *
         *
         */

        public function getStock($_Store = NULL)
        {
                if (
                        empty($_Store) 
                        || !($_Store instanceof \Cataleya\Store)
                        ) $_Store = $this->getStore ();
                
                
                return (float)$this->getProductOption()->getStock()->getValue($_Store);
        }
        
        
        
        
        
        
        
        
        
        


        /*
         *
         * [ getStoreId ] 
         *_____________________________________________________
         *
         *
         */

        public function getStoreId()
        {
                return $this->_data['store_id'];
        }
        
        
        
        /*
         *
         * [ getStore ] 
         *_____________________________________________________
         *
         *
         */

        public function getStore()
        {
                return \Cataleya\Store::load($this->_data['store_id']);
        }



        

	
	
	public function saveData () 
	{
		

		if (empty($this->_modified)) return;
                
                $this->_modified = array_unique($this->_modified);
                
		$_update_params = array();
		$key_val_pairs = array();
		
                // stuff to update
		foreach ($this->_modified as $key) $key_val_pairs[$key] = $key . ' = :' . $key;
		

                // Do prepared statement for 'INSERT'...
                $update_handle = $this->dbh->prepare('
                                                        UPDATE wishlist_items 
                                                        SET ' . implode (', ', $key_val_pairs) . '  
                                                        WHERE wishlist_item_id = :instance_id 
                                                        ');
                
                $_update_params['wishlist_item_id'] = $this->_data['wishlist_item_id'];                
                $update_handle->bindParam(':instance_id', $_update_params['wishlist_item_id'], \PDO::PARAM_INT);

		foreach ($this->_modified as $key) {
                    
                        $_update_params[$key] = $this->_data[$key];                        
			$update_handle->bindParam(':'.$key, $_update_params[$key], \Cataleya\Helper\DBH::getTypeConst($_update_params[$key]));

		}
		


                
		
		if (!$update_handle->execute()) $this->e->triggerException('Error in class (' . __CLASS__ . '): [ ' . implode(', ', $update_handle->errorInfo()) . ' ] on line ' . __LINE__);
		
        \Cataleya\System\Event::notify('customer/wishlist/item/updated', $this, array ());
        
        
	}
	
	
       
 
 
        
        
        
        
        
        
        

        
        
        



        





}
	
	
	
	
