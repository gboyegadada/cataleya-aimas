<?php


namespace Cataleya\Store;



/*

CLASS STOREFRONT ORDER

*/



class Checkout 
{
    
    
	private $dbh, $e;
	private $_data = array();
	private $_modified = array();
        
        // data cache
        private 
                $_payment_type, 
                $_shipping_option,
                $_shipping_address,     
                $_billing_address, 
                $_currency, 
                $_store;
    
        // To be used by [\Cataleya\Collection] to iterate privileges assigned to this role...
        protected $_collection = array ();
        protected $_position = 0;

	/*
	 *
	 * [ __construct ]
	 * ________________________________________________________________
	 *
	 *
	 *
	 */
	 
	 
	public function __construct ()
	{
            
                
                
		// Get database handle...
                $this->dbh = \Cataleya\Helper\DBH::getInstance();
		
		// Get error handler
		$this->e = \Cataleya\Helper\ErrorHandler::getInstance();
		
	}
	




	/*
	 *
	 * [ __destruct ]
	 * ________________________________________________________________
	 *
	 *
	 *
	 */

    public function __destruct()
    {
		$this->saveData();
    }
	




	/*
	 *
	 * [ load ]
	 * ________________________________________________________________
	 *
	 *
	 *
	 */
	 
	 
	public static function load (\Cataleya\Customer\Cart $_Cart)
	{
		
                // if ($id == 0 || filter_var($id, FILTER_VALIDATE_INT) == FALSE) return NULL;
		
            

		// Get database handle...
                $dbh = \Cataleya\Helper\DBH::getInstance();
		
		// Get error handler
		$e = \Cataleya\Helper\ErrorHandler::getInstance();
		
		// LOAD: CURRENCY
		static $instance_select, $param_instance_id;
		
			
		if (!isset($instance_select)) {
			// PREPARE SELECT STATEMENT...
			$instance_select = $dbh->prepare('SELECT * FROM checkout WHERE cart_id = :instance_id LIMIT 1');
			$instance_select->bindParam(':instance_id', $param_instance_id, \PDO::PARAM_INT);
		}
		
		
		$param_instance_id = $_Cart->getCartId();
		
		if (!$instance_select->execute()) $e->triggerException('
												Error in class (' . __CLASS__ . '): [ ' . 
												implode(', ', $instance_select->errorInfo()) . 
												' ] on line ' . __LINE__);
		
                $_data = $instance_select->fetch(\PDO::FETCH_ASSOC);
                if (empty($_data))  return self::create($_Cart);
                
		$instance = new \Cataleya\Store\Checkout ();
		$instance->_data = $_data;
                
	
		return $instance;
		
	}
        
        
        
        
        
        /*
         *
         * [ getCart ] 
         *_____________________________________________________
         *
         *
         */

        public function getCart()
        {                
                return \Cataleya\Customer\Cart::load($this->_data['cart_id']);
        }


        
        
        
 
        
        
        
        



	/*
	 *
	 *  [ create ]
	 * ________________________________________________________________
	 * 
	 * 
	 *
	 *
	 */

	 
	static public function create (\Cataleya\Customer\Cart $_Cart) 
	{
		 

		
		// Get database handle...
                $dbh = \Cataleya\Helper\DBH::getInstance();
		
		// Get error handler
		$e = \Cataleya\Helper\ErrorHandler::getInstance();
                
                
                $_Store = $_Cart->getStore();
                
                
                $_StoreAddress = $_Store->getDefaultContact();
                $_Country = $_StoreAddress->getCountry();
                $_country_or_province = ($_Country->hasProvinces()) 
                                        ? $_StoreAddress->getProvince()
                                        : $_Country;
                
                $_ShippingAddress = \Cataleya\Asset\Contact::create($_country_or_province, array('label' => 'Shipping Address'));
                $_BillingAddress = \Cataleya\Asset\Contact::create($_country_or_province, array('label' => 'Billing Address'));
		
		// construct
		static 
				$instance_insert,  
                                $param_store_id,   
                                $param_currency_code, 
                                $param_cart_id, 
                                //$param_shipping_option_id, 
                                $param_shipping_address_id, 
                                $param_billing_address_id;
		
		if (empty($instance_insert))
		{
			$instance_insert = $dbh->prepare('
                                                        INSERT INTO checkout 
                                                        (cart_id, store_id, currency_code, shipping_address_id, billing_address_id, date_created, date_modified) 
                                                        VALUES (:cart_id, :store_id, :currency_code, :shipping_address_id, :billing_address_id, NOW(), NOW())
                                                        ');
															
			$instance_insert->bindParam(':currency_code', $param_currency_code, \PDO::PARAM_STR);
                        $instance_insert->bindParam(':store_id', $param_store_id, \PDO::PARAM_INT);
                        //$instance_insert->bindParam(':shipping_description', $param_shipping_description, \PDO::PARAM_STR);
                        //$instance_insert->bindParam(':customer_id', $param_customer_id, \PDO::PARAM_INT);
                        $instance_insert->bindParam(':cart_id', $param_cart_id, \PDO::PARAM_INT);
                        $instance_insert->bindParam(':shipping_address_id', $param_shipping_address_id, \PDO::PARAM_INT);
                        $instance_insert->bindParam(':billing_address_id', $param_billing_address_id, \PDO::PARAM_INT);
		}
		
                $param_store_id = $_Store->getStoreId();
                $param_currency_code = $_Store->getCurrencyCode();
                $param_cart_id = $_Cart->getCartId();
                //$param_shipping_option_id = $_ShippingOption->getID();
                //$param_shipping_description = $_ShippingOption->getDescription()->getTitle('EN');
                $param_shipping_address_id = $_ShippingAddress->getID();
                $param_billing_address_id = $_BillingAddress->getID();
                
		
		if (!$instance_insert->execute()) $e->triggerException('
										Error in class (' . __CLASS__ . '): [ ' . 
										implode(', ', $instance_insert->errorInfo()) . 
										' ] on line ' . __LINE__);
										
		
		// AUTOLOAD NEW PRODUCT AND RETURN IT
		$instance = self::load($_Cart);
                
		$_Customer = $_Cart->getCustomer();
                if (!empty($_Customer)) $instance->setCustomer($_Customer);
                
		return $instance;
		
		
	}


	 
	 /*
	 *  [ delete ]
	 * ________________________________________________________________
	 * 
	 * 
	 *
	 *
	 */

	 
	public function delete () 
	{
		foreach (\Cataleya\Payment\Transactions::load($this) as $_Tranx) $_Tranx->delete();
                
                // Delete recipient
                $this->getRecipient()->delete();
            
                // Delete rows containing 'store_id' in specified tables
                \Cataleya\Helper\DBH::sanitize(array('checkout'), 'cart_id', $this->getID());

		
		// $this = NULL;
		return TRUE;
		
		
	}




      
    

        /*
         *
         * [ getID ] 
         *_____________________________________________________
         *
         *
         */

        public function getID()
        {
                return $this->_data['cart_id'];
        }
        
        
        
        





        public function saveData () 
	{
		

		if (empty($this->_modified)) return;
                $this->_modified = array_unique($this->_modified);
                
		$_update_params = array();
		$key_val_pairs = array();
		
		foreach ($this->_modified as $key) $key_val_pairs[$key] = $key . ' = :' . $key;
		
	
		// Do prepared statement for 'INSERT'...
		$update_handle = $this->dbh->prepare('
                                                        UPDATE checkout 
                                                        SET ' . implode (', ', $key_val_pairs) . ', date_modified=NOW()       
                                                        WHERE cart_id = :instance_id 
                                                        ');
                
		$update_handle->bindParam(':instance_id', $_update_params['cart_id'], \PDO::PARAM_INT);
                $_update_params['cart_id'] = $this->_data['cart_id'];
                
		foreach ($this->_modified as $key) {
                    
                        $_update_params[$key] = $this->_data[$key];                        
			$update_handle->bindParam(':'.$key, $_update_params[$key], \Cataleya\Helper\DBH::getTypeConst($_update_params[$key]));

		}
		

		
		if (!$update_handle->execute()) $this->e->triggerException('Error in class (' . __CLASS__ . '): [ ' . implode(', ', $update_handle->errorInfo()) . ' ] on line ' . __LINE__);
		
                
                
	}
	
	
       
 
 

        
        
        
        
        
        


        /*
         *
         * [ getCartId ] 
         *_____________________________________________________
         *
         *
         */

        public function getCartId()
        {
                return $this->_data['cart_id'];
        }






        /*
         *
         * [ getCheckoutDate ] 
         *_____________________________________________________
         *
         *
         */

        public function getCheckoutDate($_GMT = TRUE)
        {
            if (!is_bool($_GMT)) $this->e->triggerException('Error in class (' . __CLASS__ . '): [ invalid argument for method: ' . __FUNCTION__ . ' ] on line ' . __LINE__);

            $_DateTime = new \DateTime($this->_data['date_created']);
            if (!$_GMT) $_DateTime->setTimezone($this->getStore()->getTimezone());

            return $_DateTime;
        }







        /*
         *
         * [ getDateModified ] 
         *_____________________________________________________
         *
         *
         */

        public function getDateModified($_GMT = TRUE)
        {
            if (!is_bool($_GMT)) $this->e->triggerException('Error in class (' . __CLASS__ . '): [ invalid argument for method: ' . __FUNCTION__ . ' ] on line ' . __LINE__);

            $datetime = new \DateTime($this->_data['date_modified']);
            if (!$_GMT) $datetime->setTimezone($this->getStore()->getTimezone());

            return $datetime;
        }




        
        
        

        /*
         *
         * [ setShippingOption ] 
         *_____________________________________________________
         *
         *
         */

        public function setShippingOption(\Cataleya\Shipping\ShippingOption $_ShippingOption)
        {

                $this->_data['shipping_option_id'] = $_ShippingOption->getID();
                $this->_modified[] = 'shipping_option_id';

                return $this;
        }
        
        
        


        /*
         *
         * [ getShippingOption ] 
         *_____________________________________________________
         *
         *
         */

        public function getShippingOption()
        {
            if (empty($this->_shipping_option)) $this->_shipping_option = \Cataleya\Shipping\ShippingOption::load ($this->_data['shipping_option_id']);
            
            return $this->_shipping_option;
        }


        
        
        
        


        /*
         *
         * [ setPaymentType ] 
         *_____________________________________________________
         *
         *
         */

        public function setPaymentType(\Cataleya\Payment\PaymentType $_PaymentType)
        {

                $this->_data['payment_type_id'] = $_PaymentType->getID();
                $this->_modified[] = 'payment_type_id';

                return $this;
        }
        
        
        


        /*
         *
         * [ getPaymentType ] 
         *_____________________________________________________
         *
         *
         */

        public function getPaymentType()
        {
            if (empty($this->_payment_type)) $this->_payment_type = \Cataleya\Payment\PaymentType::load ($this->_data['payment_type_id']);
            
            return $this->_payment_type;
        }

        
        
        


        
        /*
         *
         * [ getShippingDescription ] 
         *_____________________________________________________
         *
         *
         */

        public function getShippingDescription()
        {
                return $this->_data['shipping_description'];
        }

   
        
        
        
        
        
        


        /*
         *
         * [ getStoreId ] 
         *_____________________________________________________
         *
         *
         */

        public function getStoreId()
        {
                return $this->_data['store_id'];
        }



        /*
         *
         * [ getStore ] 
         *_____________________________________________________
         *
         *
         */

        public function getStore()
        {
                if (empty($this->_store)) $this->_store = \Cataleya\Store::load ($this->_data['store_id']);
                
                return $this->_store;
        }




        /*
         *
         * [ getCustomerId ] 
         *_____________________________________________________
         *
         *
         */

        public function getCustomerId()
        {
                return $this->_data['customer_id'];
        }
        
        
        
        /*
         *
         * [ getCustomer ] 
         *_____________________________________________________
         *
         *
         */

        public function getCustomer()
        {
                return \Cataleya\Customer::load($this->_data['customer_id']);
        }


        /*
         *
         * [ setCustomer ] 
         *_____________________________________________________
         *
         *
         */

        public function setCustomer(\Cataleya\Customer $_Customer)
        {

                $this->_data['customer_id'] = $_Customer->getCustomerId();
                $this->_modified[] = 'customer_id';

                return $this;
        }
        
        
        
        


        /*
         *
         *  [ setShippingAddress ]
         * ________________________________________________________________
         * 
         * 
         *
         *
         *
         */


        public function setShippingAddress (\Cataleya\Asset\Contact $_Contact)
        {
                $this->getShippingAddress()->copy($_Contact);
                
                return $this;
        }
        
        
        
        

        /*
         *
         * [ getShippingAddress ] 
         *_____________________________________________________
         *
         *
         */

        public function getShippingAddress()
        {
            
                if (empty($this->_shipping_address)) $this->_shipping_address = \Cataleya\Asset\Contact::load ($this->_data['shipping_address_id']);
                return $this->_shipping_address;
        }
        
        
        
        

        /*
         *
         *  [ setRecipient ]
         * ________________________________________________________________
         * 
         * 
         *
         *
         *
         */


        public function setRecipient (\Cataleya\Asset\Contact $_Contact)
        {
                return $this->setShippingAddress($_Contact);
        }
        
        



        /*
         *
         * [ getRecipient ] 
         *_____________________________________________________
         *
         *
         */

        public function getRecipient()
        {
                return $this->getShippingAddress();
        }
        
        
        


        /*
         *
         *  [ setBillingAddress ]
         * ________________________________________________________________
         * 
         * 
         *
         *
         *
         */


        public function setBillingAddress (\Cataleya\Asset\Contact $_Contact)
        {
                $this->getBillingAddress()->copy($_Contact);
                
                return $this;
        }
        


        /*
         *
         * [ getBillingAddress ] 
         *_____________________________________________________
         *
         *
         */

        public function getBillingAddress()
        {
                if (empty($this->_billing_address)) $this->_billing_address = \Cataleya\Asset\Contact::load ($this->_data['billing_address_id']);
                return $this->_billing_address;
        }
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        


        /*
         *
         * [ getOrderStatus ] 
         *_____________________________________________________
         *
         *
         */

        public function getOrderStatus()
        {
                return $this->_data['checkout_status'];
        }



        /*
         *
         * [ setCheckoutStatus ] 
         *_____________________________________________________
         *
         *
         */

        public function setOrderStatus($value = NULL)
        {
                
                $_enum = array('pending', 'shipped', 'delivered', 'cancelled'); // as in table column...
                if ($value===NULL || !is_string($value) || !in_array(strtolower($value), $_enum)) $this->e->triggerException('Error in class (' . __CLASS__ . '): [ Invalid argument for method:' . __FUNCTION__ . ' ] on line ' . __LINE__);

                $value = strtolower($value);
                if ($value === $this->getOrderStatus()) return $this;
                
                $this->_data['checkout_status'] = $value;
                $this->_modified[] = 'checkout_status';
                
                if (file_exists($this->_pdf_path)) unlink ($this->_pdf_path);

                return $this;
        }
        
        
        


        /*
         *
         * [ getCurrencyCode ] 
         *_____________________________________________________
         *
         *
         */

        public function getCurrencyCode()
        {
                return $this->_data['currency_code'];
        }




        /*
         *
         * [ getCurrency ] 
         *_____________________________________________________
         *
         *
         */

        public function getCurrency()
        {

                if (empty($this->_currency)) $this->_currency = \Cataleya\Locale\Currency::load($this->_data['currency_code']);
                return $this->_currency;
        }

        
        
        



        /*
         *
         * [ getExpectedDeliveryDate ] 
         *_____________________________________________________
         *
         *
         */

        public function getExpectedDeliveryDate()
        {
                return $this->_data['exp_delivery_date'];
        }



        /*
         *
         * [ setExpDeliveryDate ] 
         *_____________________________________________________
         *
         *
         */

        public function setExpDeliveryDate($value = NULL)
        {


                if ($value===NULL) $this->e->triggerException('Error in class (' . __CLASS__ . '): [ invalid argument for method: ' . __FUNCTION__ . ' ] on line ' . __LINE__);

                $this->_data['exp_delivery_date'] = $value;
                $this->_modified[] = 'exp_delivery_date';

                return $this;
        }
        
        
        
        
        
        
        

        /*
         * 
         * [ updateFullTextIndex ]
         * 
         */


        public function updateFullTextIndex () 
        {
            
            $_Customer = $this->getCustomer();
            
            // [1] Order + Customer Info
            $_keywords = array (
                    '#'.$this->getOrderNumber(), 
                    $this->getOrderDate(FALSE)->format('H:i:s l, d F Y A, m-d-Y'), 
                    $_Customer->getName(),
                    $_Customer->getEmailAddress(), 
                    $_Customer->getTelephone()
            );
            
            
            
            // [2] Order details
            foreach ($this->_collection as $_item) 
            {
                $_keywords[] = $_item->getItemName() . ' ' . $_item->getItemDescription();
                $_keywords[] = $_item->getSaleDescription();
                $_keywords[] = $_item->getCouponDescription();
            }
            
            
            
            // [3] Transaction
            foreach (\Cataleya\Payment\Transactions::load($this) as $_Tranx) 
            {
                $_keywords[] = '#'.str_pad(strval($_Tranx->getID()), 5, '0', STR_PAD_LEFT);
                $_keywords[] = '#'.strval($_Tranx->getPaypalTransactionId());
                $_keywords[] = $_Tranx->getPaypalPayerEmail();
                break;
            }
            
            
            // [4] Address
            $_Location = $this->getRecipient()->getLocation();
            $_keywords[] = $_Location->getEntryStreetAddress();
            $_keywords[] = $_Location->getEntrySuburb();
            $_keywords[] = $_Location->getEntryCity();
            $_keywords[] = $_Location->getEntryPostcode();
            $_keywords[] = $_Location->getEntryProvince();
            // $_keywords[] = $_Location->getEntryCountry();
            




            static $insert_handle, $param_cart_id, $param_keywords;

            // Build the SQL statement.  
            if (empty($insert_handle)) {
                  $insert_handle = $this->dbh->prepare('
                                            INSERT INTO fulltext_order_index_isam (cart_id, keywords) 
                                            VALUES (:cart_id, :keywords ) 
                                            ON DUPLICATE KEY UPDATE keywords = :keywords
                                            ');
                  $insert_handle->bindParam(':cart_id', $param_cart_id, \PDO::PARAM_INT);
                  $insert_handle->bindParam(':keywords', $param_keywords, \PDO::PARAM_STR);
            }

            // ser params
            $param_cart_id = $this->getID();
            $param_keywords = preg_replace('/([^-\s\.A-Za-z0-9@*#+%]+)/', ' ', implode(' ', array_unique($_keywords)));

            // execute the query.  If successful, return true 
            if (!$insert_handle->execute()) $this->e->triggerException('
                                                                            Error in class (' . __CLASS__ . '): [ ' . 
                                                                            implode(', ', $insert_handle->errorInfo()) . 
                                                                            ' ] on line ' . __LINE__);
            
            
            return $this;

          }
          
          
          
          
          

        
        /*
         *
         * [ isReady ] 
         *_____________________________________________________
         *
         *
         */

        public function isReady()
        {                
                return true;
        }


          
          
        
        

      


	
	
	
}
	
	
	
	
