<?php


namespace Cataleya\Store;



/*

CLASS STOREFRONT ORDER

*/



class OrderDetail   
{
    
        private $_Product, $_ProductOption;


	/*
	 *
	 * [ __construct ]
	 * ________________________________________________________________
	 *
	 *
	 *
	 */
	 
	 
	private function __construct ()
	{
		
		// Get database handle...
                $this->dbh = \Cataleya\Helper\DBH::getInstance();
		
		// Get error handler
		$this->e = \Cataleya\Helper\ErrorHandler::getInstance();
		
	}
	




	/*
	 *
	 * [ __destruct ]
	 * ________________________________________________________________
	 *
	 *
	 *
	 */

    public function __destruct()
    {
		$this->saveData();
    }
	




	/*
	 *
	 * [ load ]
	 * ________________________________________________________________
	 *
	 *
	 *
	 */
	 
	 
	public static function load ($id = 0)
	{
		
                if ($id == 0 || filter_var($id, FILTER_VALIDATE_INT) == FALSE) return NULL;
		
		$instance = new \Cataleya\Store\OrderDetail ();
		
		// LOAD: CURRENCY
		static $instance_select, $param_instance_id;
		
			
		if (!isset($instance_select)) {
			// PREPARE SELECT STATEMENT...
			$instance_select = $instance->dbh->prepare('SELECT * FROM order_detail WHERE order_detail_id = :instance_id LIMIT 1');
			$instance_select->bindParam(':instance_id', $param_instance_id, \PDO::PARAM_INT);
		}
		
		
		$param_instance_id = $id;
		
		if (!$instance_select->execute()) $instance->e->triggerException('
												Error in class (' . __CLASS__ . '): [ ' . 
												implode(', ', $instance_select->errorInfo()) . 
												' ] on line ' . __LINE__);
		
		$instance->_data = $instance_select->fetch(\PDO::FETCH_ASSOC);
                
               


		return $instance;	
		
		
	}



	/*
	 *
	 *  [ create ]
	 * ________________________________________________________________
	 * 
	 * 
	 *
	 *
	 */

	 
	static public function create (\Cataleya\Store\Order $_Order, \Cataleya\Catalog\Product\Option $_ProductOption, array $_options = array ()) 
	{
		
	

		
		// Get database handle...
                $dbh = \Cataleya\Helper\DBH::getInstance();
		
		// Get error handler
		$e = \Cataleya\Helper\ErrorHandler::getInstance();
		
		// construct
		static 
				$instance_insert,  
                                $instance_insert_param_order_id, 
                                $instance_insert_param_product_id, 
                                $instance_insert_param_option_id;
		
		if (empty($instance_insert))
		{
			$instance_insert = $dbh->prepare('
                                                        INSERT INTO order_detail 
                                                        (order_id, product_id, option_id, tax_json) 
                                                        VALUES (:order_id, :product_id, :option_id, "")
                                                        ');
															
                        $instance_insert->bindParam(':order_id', $instance_insert_param_order_id, \PDO::PARAM_INT);
                        $instance_insert->bindParam(':product_id', $instance_insert_param_product_id, \PDO::PARAM_INT);
                        $instance_insert->bindParam(':option_id', $instance_insert_param_option_id, \PDO::PARAM_INT);
		}
		
                $instance_insert_param_order_id = $_Order->getID();
                $instance_insert_param_product_id = $_ProductOption->getProductId();
                $instance_insert_param_option_id = $_ProductOption->getID();
		
		if (!$instance_insert->execute()) $e->triggerException('
										Error in class (' . __CLASS__ . '): [ ' . 
										implode(', ', $instance_insert->errorInfo()) . 
										' ] on line ' . __LINE__);
										
		
		// AUTOLOAD NEW INSTANCE AND RETURN IT
		$instance = self::load($dbh->lastInsertId());
		
                
		return $instance;
		
		
	}


	 
	 /*
	 *  [ delete ]
	 * ________________________________________________________________
	 * 
	 * 
	 *
	 *
	 */

	 
	public function delete () 
	{
		
			// DELETE 
			$instance_delete = $this->dbh->prepare('
													DELETE FROM order_detail 
													WHERE order_detail_id = :instance_id
													');
			$instance_delete->bindParam(':instance_id', $instance_delete_param_instance_id, \PDO::PARAM_INT);
			
			$instance_delete_param_instance_id = $this->getID();
	
			if (!$instance_delete->execute()) $this->e->triggerException('
											Error in class (' . __CLASS__ . '): [ ' . 
											implode(', ', $instance_delete->errorInfo()) . 
											' ] on line ' . __LINE__);
											
			

		
		// $this = NULL;
		return TRUE;
		
		
	}





        /*
         *
         * [ getID ] 
         *_____________________________________________________
         *
         *
         */

        public function getID()
        {
                return $this->_data['order_detail_id'];
        }




	
	
	public function saveData () 
	{
		

		if (empty($this->_modified)) return;
                $this->_modified = array_unique($this->_modified);
                
		$_update_params = array();
		$key_val_pairs = array();
		
		foreach ($this->_modified as $key) $key_val_pairs[$key] = $key . ' = :' . $key;
		
	
		// Do prepared statement for 'INSERT'...
		$update_handle = $this->dbh->prepare('
                                                        UPDATE order_detail 
                                                        SET ' . implode (', ', $key_val_pairs) . '    
                                                        WHERE order_detail_id = :instance_id 
                                                        ');
                
		$update_handle->bindParam(':instance_id', $_update_params['order_detail_id'], \PDO::PARAM_INT);
                $_update_params['order_detail_id'] = $this->getID();
                
		foreach ($this->_modified as $key) {
                    
                        $_update_params[$key] = $this->_data[$key];                        
			$update_handle->bindParam(':'.$key, $_update_params[$key], \Cataleya\Helper\DBH::getTypeConst($_update_params[$key]));

		}
		

		
		if (!$update_handle->execute()) $this->e->triggerException('Error in class (' . __CLASS__ . '): [ ' . implode(', ', $update_handle->errorInfo()) . ' ] on line ' . __LINE__);
		
	}
	
	
       
 
	

        
        
        


        /*
         *
         * [ getOrderId ] 
         *_____________________________________________________
         *
         *
         */

        public function getOrderId()
        {
                return $this->_data['order_id'];
        }
        
        
        
        
        /*
         *
         * [ getOrder ] 
         *_____________________________________________________
         *
         *
         */

        public function getOrder()
        {
                return \Cataleya\Store\Order::load($this->_data['order_id']);
        }





        /*
         *
         * [ getProductId ] 
         *_____________________________________________________
         *
         *
         */

        public function getProductId()
        {
                return $this->_data['product_id'];
        }
        
        
        
        /*
         *
         * [ getProduct ] 
         *_____________________________________________________
         *
         *
         */

        public function getProduct()
        {
                if (empty($this->_Product)) $this->_Product = \Cataleya\Catalog\Product::load ($this->_data['product_id']);
                return $this->_Product;
        }



        /*
         *
         * [ setProduct ] 
         *_____________________________________________________
         *
         *
         */

        public function setProduct(\Cataleya\Catalog\Product $_Product)
        {

                $this->_data['product_id'] = $_Product->getID();
                $this->_modified[] = 'product_id';

                return $this;
        }
        
        
        

        
        
        /*
         *
         * [ getProductOptionId ] 
         *_____________________________________________________
         *
         *
         */

        public function getProductOptionId()
        {
                return $this->_data['option_id'];
        }
        
        


        /*
         *
         * [ getProductOption ] 
         *_____________________________________________________
         *
         *
         */

        public function getProductOption()
        {
                if (empty($this->_ProductOption)) $this->_ProductOption = \Cataleya\Catalog\Product\Option::load ($this->_data['option_id']);
                return $this->_ProductOption;
        }



        /*
         *
         * [ setOptionId ] 
         *_____________________________________________________
         *
         *
         */

        public function setProductOption(\Cataleya\Catalog\Product\Option $_ProductOption)
        {

                $this->_data['option_id'] = $_ProductOption->getID();
                $this->_modified[] = 'option_id';

                return $this;
        }



        /*
         *
         * [ getQuantity ] 
         *_____________________________________________________
         *
         *
         */

        public function getQuantity()
        {
                return (float)$this->_data['quantity'];
        }



        /*
         *
         * [ setQuantity ] 
         *_____________________________________________________
         *
         *
         */

        public function setQuantity($value = NULL)
        {
                $value = filter_var($value, FILTER_VALIDATE_FLOAT);
                if ($value === FALSE) $this->e->triggerException('Error in class (' . __CLASS__ . '): [ Argument supplied in method: ' . __FUNCTION__ . ' must be a float or integer  ] on line ' . __LINE__);


                $this->_data['quantity'] = $value;
                $this->_modified[] = 'quantity';

                return $this;
        }



        /*
         *
         * [ getGiftWrap ] 
         *_____________________________________________________
         *
         *
         */

        public function hasGiftWrap()
        {
                return ((int)$this->_data['gift_wrap'] === 1);
        }



        /*
         *
         * [ setGiftWrap ] 
         *_____________________________________________________
         *
         *
         */

        public function setGiftWrap($value = NULL)
        {
                $value = filter_var($value, FILTER_VALIDATE_BOOLEAN, FILTER_NULL_ON_FAILURE);

                if ($value===NULL) $this->e->triggerException('Error in class (' . __CLASS__ . '): [ invalid argument for method: ' . __FUNCTION__ . ' ] on line ' . __LINE__);

                $this->_data['gift_wrap'] = $value;
                $this->_modified[] = 'gift_wrap';

                return $this;
        }




        /*
         *
         * [ getWrapperId ] 
         *_____________________________________________________
         *
         *
         */

        public function getWrapperId()
        {
                return $this->_data['wrapper_id'];
        }



        /*
         *
         * [ setWrapperId ] 
         *_____________________________________________________
         *
         *
         */

        public function setWrapperId($value = NULL)
        {
                $value = filter_var($value, FILTER_VALIDATE_INT, FILTER_NULL_ON_FAILURE);

                if ($value===NULL) $this->e->triggerException('Error in class (' . __CLASS__ . '): [ invalid argument for method: ' . __FUNCTION__ . ' ] on line ' . __LINE__);

                $this->_data['wrapper_id'] = $value;
                $this->_modified[] = 'wrapper_id';

                return $this;
        }


        
        /*
         *
         * [ getOriginalPrice ] 
         *_____________________________________________________
         *
         *
         */

        public function getOriginalPrice()
        {
                return round($this->_data['original_price'], 2);
        }
        
        
        
        /*
         *
         * [ setOriginalPrice ] 
         *_____________________________________________________
         *
         *
         */

        public function setOriginalPrice($value = NULL)
        {
                $value = filter_var($value, FILTER_VALIDATE_FLOAT, FILTER_NULL_ON_FAILURE);

                if ($value===NULL) $this->e->triggerException('Error in class (' . __CLASS__ . '): [ invalid argument for method: ' . __FUNCTION__ . ' ] on line ' . __LINE__);

                $this->_data['original_price'] = $value;
                $this->_modified[] = 'original_price';

                return $this;
        }



        /*
         *
         * [ getPrice ] 
         *_____________________________________________________
         *
         *
         */

        public function getPrice()
        {
                return round($this->_data['price'], 2);
        }



        /*
         *
         * [ setPrice ] 
         *_____________________________________________________
         *
         *
         */

        public function setPrice($value = NULL)
        {
                $value = filter_var($value, FILTER_VALIDATE_FLOAT, FILTER_NULL_ON_FAILURE);

                if ($value===NULL) $this->e->triggerException('Error in class (' . __CLASS__ . '): [ invalid argument for method: ' . __FUNCTION__ . ' ] on line ' . __LINE__);

                $this->_data['price'] = $value;
                $this->_modified[] = 'price';

                return $this;
        }



        /*
         *
         * [ getSubtotal ] 
         *_____________________________________________________
         *
         *
         */

        public function getSubtotal($_WITH_DISCOUNTS = true)
        {
                if ($_WITH_DISCOUNTS) return round($this->_data['subtotal'], 2);
                else return round(($this->getOriginalPrice() * $this->getQuantity()), 2);
        }
        
        




        /*
         *
         * [ setSubtotal ] 
         *_____________________________________________________
         *
         *
         */

        public function setSubtotal($value = NULL)
        {
                $value = filter_var($value, FILTER_VALIDATE_FLOAT, FILTER_NULL_ON_FAILURE);

                if ($value===NULL) $this->e->triggerException('Error in class (' . __CLASS__ . '): [ invalid argument for method: ' . __FUNCTION__ . ' ] on line ' . __LINE__);

                $this->_data['subtotal'] = $value;
                $this->_modified[] = 'subtotal';

                return $this;
        }

        
        


        



        /*
         *
         * [ getTax ] 
         *_____________________________________________________
         *
         *
         */

        public function getTax()
        {
                return $this->_data['tax'];
        }



        /*
         *
         * [ setTax ] 
         *_____________________________________________________
         *
         *
         */

        public function setTax($value = NULL)
        {
                $value = filter_var($value, FILTER_VALIDATE_FLOAT, FILTER_NULL_ON_FAILURE);

                if ($value===NULL) $this->e->triggerException('Error in class (' . __CLASS__ . '): [ invalid argument for method: ' . __FUNCTION__ . ' ] on line ' . __LINE__);

                $this->_data['tax'] = $value;
                $this->_modified[] = 'tax';

                return $this;
        }
        
        
        

        /*
         *
         * [ setTaxJson ] 
         *_____________________________________________________
         *
         *
         */

        public function setTaxJson(array $_json)
        {

                $this->_data['tax_json'] = json_encode($_json);
                $this->_modified[] = 'tax_json';

                return $this;
        }
        
        
        
        /*
         *
         * [ getTaxes ] 
         *_____________________________________________________
         *
         *
         */

        public function getTaxes()
        {
                if (!empty($this->_data['tax_json'])) return json_decode ($this->_data['tax_json'], true);
                else return array ();
                
        }









        /*
         *
         * [ getProductIsFree ] 
         *_____________________________________________________
         *
         *
         */

        public function getProductIsFree()
        {
                return ((int)$this->_data['product_is_free'] === 1);
        }



        /*
         *
         * [ setProductIsFree ] 
         *_____________________________________________________
         *
         *
         */

        public function setProductIsFree($value = NULL)
        {
                $value = filter_var($value, FILTER_VALIDATE_BOOLEAN, FILTER_NULL_ON_FAILURE);

                if ($value===NULL) $this->e->triggerException('Error in class (' . __CLASS__ . '): [ invalid argument for method: ' . __FUNCTION__ . ' ] on line ' . __LINE__);

                $this->_data['product_is_free'] = $value;
                $this->_modified[] = 'product_is_free';

                return $this;
        }



        /*
         *
         * [ getOneTimeCharges ] 
         *_____________________________________________________
         *
         *
         */

        public function getOneTimeCharges()
        {
                return $this->_data['one_time_charges'];
        }



        /*
         *
         * [ setOneTimeCharges ] 
         *_____________________________________________________
         *
         *
         */

        public function setOneTimeCharges($value = NULL)
        {
                $value = filter_var($value, FILTER_VALIDATE_FLOAT, FILTER_NULL_ON_FAILURE);

                if ($value===NULL) $this->e->triggerException('Error in class (' . __CLASS__ . '): [ invalid argument for method: ' . __FUNCTION__ . ' ] on line ' . __LINE__);

                $this->_data['one_time_charges'] = $value;
                $this->_modified[] = 'one_time_charges';

                return $this;
        }



        /*
         *
         * [ getItemName ] 
         *_____________________________________________________
         *
         *
         */

        public function getItemName()
        {
                return $this->_data['item_name'];
        }



        /*
         *
         * [ setItemName ] 
         *_____________________________________________________
         *
         *
         */

        public function setItemName($value = NULL)
        {


                if ($value===NULL) $this->e->triggerException('Error in class (' . __CLASS__ . '): [ invalid argument for method: ' . __FUNCTION__ . ' ] on line ' . __LINE__);

                $this->_data['item_name'] = $value;
                $this->_modified[] = 'item_name';

                return $this;
        }



        /*
         *
         * [ getItemDescription ] 
         *_____________________________________________________
         *
         *
         */

        public function getItemDescription()
        {
                return $this->_data['item_description'];
        }



        /*
         *
         * [ setItemDescription ] 
         *_____________________________________________________
         *
         *
         */

        public function setItemDescription($value = NULL)
        {


                if ($value===NULL) $this->e->triggerException('Error in class (' . __CLASS__ . '): [ invalid argument for method: ' . __FUNCTION__ . ' ] on line ' . __LINE__);

                $this->_data['item_description'] = $value;
                $this->_modified[] = 'item_description';

                return $this;
        }


        
        
        



        /**
         * 
         * @return string
         * 
         */
        public function getSKU()
        {
                return $this->_data['sku'];
        }



        /**
         * 
         * @param string $value Stock Keeping Unit (32 characters max)
         * @return \Cataleya\Catalog\Product\Option
         * 
         */
        public function setSKU($value = NULL)
        {

                $value = \Cataleya\Helper\Validator::referencecode($value, 1, 200);
                if (empty($value)) $this->e->triggerException('Error in class (' . __CLASS__ . '): [ invalid argument for method: ' . __FUNCTION__ . ' ] on line ' . __LINE__);

                $this->_data['sku'] = strtoupper($value);
                $this->_modified[] = 'sku';

                return $this;
        }



        /**
         * 
         * @return string International Article Number
         * 
         */
        public function getEAN()
        {
                return $this->_data['ean'];
        }



        /**
         * 
         * @param string $value International Article Number (12 characters max)
         * @return \Cataleya\Catalog\Product\Option
         * 
         */
        public function setEAN($value = NULL)
        {

                $value = \Cataleya\Helper\Validator::referencecode($value, 1, 200);
                if (empty($value)) $this->e->triggerException('Error in class (' . __CLASS__ . '): [ invalid argument for method: ' . __FUNCTION__ . ' ] on line ' . __LINE__);

                $this->_data['ean'] = strtoupper($value);
                $this->_modified[] = 'ean';

                return $this;
        }



        /**
         * 
         * @return string Universal Product Code
         * 
         */
        public function getUPC()
        {
                return $this->_data['upc'];
        }



        /**
         * 
         * @param string $value Universal Product Code (12 characters max)
         * @return \Cataleya\Catalog\Product\Option
         * 
         */
        public function setUPC($value = NULL)
        {

                $value = \Cataleya\Helper\Validator::referencecode($value, 1, 200);
                if (empty($value)) $this->e->triggerException('Error in class (' . __CLASS__ . '): [ invalid argument for method: ' . __FUNCTION__ . ' ] on line ' . __LINE__);

                $this->_data['upc'] = strtoupper($value);
                $this->_modified[] = 'upc';

                return $this;
        }


  
        

        /**
         * 
         * @return string International Standard Book Number
         * 
         */
        public function getISBN()
        {
                return $this->_data['isbn'];
        }



        /**
         * 
         * @param string $value International Standard Book Number (45 characters max)
         * @return \Cataleya\Catalog\Product\Option
         * 
         */
        public function setISBN($value = NULL)
        {

                $value = \Cataleya\Helper\Validator::referencecode($value, 1, 200);
                if (empty($value)) $this->e->triggerException('Error in class (' . __CLASS__ . '): [ invalid argument for method: ' . __FUNCTION__ . ' ] on line ' . __LINE__);

                $this->_data['isbn'] = strtoupper($value);
                $this->_modified[] = 'isbn';

                return $this;
        }

        
        
        
        
        
        
        
        
        
        /* ============== FOR MULTIPLE SHIPPING & BILLING ADDRESSESS ========== */
        
        


        /*
         *
         *  [ setShippingAddress ]
         * ________________________________________________________________
         * 
         * 
         *
         *
         *
         */


        public function setShippingAddress (\Cataleya\Asset\Contact $_Contact)
        {
                $this->_data['billing_address_id'] = $_Contact->getContactID();
                $this->_modified[] = 'billing_address_id';
                
                return $this;
        }
        
        
        
        

        /*
         *
         *  [ removeShippingAddress ]
         * ________________________________________________________________
         * 
         * 
         *
         *
         *
         */


        public function removeShippingAddress ()
        {
                $this->_data['billing_address_id'] = NULL;
                $this->_modified[] = 'billing_address_id';
                
                return $this;
        }
        
        
        
        

        /*
         *
         * [ getShippingAddress ] 
         *_____________________________________________________
         *
         *
         */

        public function getShippingAddress()
        {
                if (empty($this->_shipping_address)) $this->_shipping_address = \Cataleya\Asset\Contact::load ($this->_data['shipping_address_id']);
                return $this->_shipping_address;
        }
        
        
        
        
        /*
         *
         *  [ setRecipient ]
         * ________________________________________________________________
         * 
         * 
         *
         *
         *
         */


        public function setRecipient (\Cataleya\Asset\Contact $_Contact)
        {
                return $this->setShippingAddress($_Contact);
        }
        
        

        /*
         *
         * [ removeRecipient ] 
         *_____________________________________________________
         *
         *
         */

        public function removeRecipient()
        {
                return $this->removeShippingAddress();
        }
        



        /*
         *
         * [ getRecipient ] 
         *_____________________________________________________
         *
         *
         */

        public function getRecipient()
        {
                return $this->getShippingAddress();
        }
        
        
        


        /*
         *
         *  [ setBillingAddress ]
         * ________________________________________________________________
         * 
         * 
         *
         *
         *
         */


        public function setBillingAddress (\Cataleya\Asset\Contact $_Contact)
        {
                $this->_data['billing_address_id'] = $_Contact->getContactID();
                $this->_modified[] = 'billing_address_id';
                
                return $this;
        }
        
        
        

        /*
         *
         *  [ removeBillingAddress ]
         * ________________________________________________________________
         * 
         * 
         *
         *
         *
         */


        public function removeBillingAddress ()
        {
                $this->_data['billing_address_id'] = NULL;
                $this->_modified[] = 'billing_address_id';
                
                return $this;
        }
        
        


        /*
         *
         * [ getBillingAddress ] 
         *_____________________________________________________
         *
         *
         */

        public function getBillingAddress()
        {
                if (empty($this->_billing_address)) $this->_billing_address = \Cataleya\Asset\Contact::load ($this->_data['billing_address_id']);
                return $this->_billing_address;
        }
        
        
        
        
        
        
        
        


  



	
}
	
	
	
	
