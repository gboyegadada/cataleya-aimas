<?php



namespace Cataleya\Store;



/*
 *
 * 	@Package: Cataleya
 * 	@Class: \Cataleya\Store\Orders
 *
 * 	(c) 2012 Fancy Paper Planes
 *
 *
 *
 */

class Orders extends \Cataleya\Collection {
    
    
    // To be used by [\Cataleya\Collection]
    protected $_collection = array ();
    protected $_position = 0;
    
    
    // CONST VALUES 1 - 15 RESERVED FOR [ _Abstract_Collection ]
    const ORDER_BY_TITLE = 16;
    const ORDER_BY_DELIVERY_DATE = 17;
    const ORDER_BY_TOTAL = 18;
    const ORDER_BY_ID = 19;
    
    const FILTER_PENDING = 20;
    const FILTER_PROCESSED = 21;
    const FILTER_OVERDUE = 22;
    const FILTER_CANCELLED = 23;
    
    const FILTER_TRANX_PAID = 24;
    const FILTER_TRANX_PENDING = 25;
    const FILTER_TRANX_CANCELLED = 26;
    const FILTER_TRANX_FAILED = 27;
    
    const FILTER_DELIVERY_PENDING = 28;
    const FILTER_DELIVERY_CONFIRMED = 29;


    public function __construct() {
       
        // Get database handle...
        $this->dbh = \Cataleya\Helper\DBH::getInstance();
		
        // Get error handler
        $this->e = \Cataleya\Helper\ErrorHandler::getInstance();
        
        

        parent::__construct();
 
        
    }
    
    
    
    
    /*
     * 
     * [ load ]
     * 
     * 
     */
    
    public static function load (\Cataleya\Store $_Store = NULL, \Cataleya\Customer $_Customer = NULL, $sort = array('order_by'=>'', 'order'=>'', 'index'=>'', 'status'=>'all'), $page_size = 100, $page_num = 1) 
    {
        
        $instance = new \Cataleya\Store\Orders ();
        
        // LOAD
        
        $sort['status'] = (isset($sort['status']) && in_array($sort['status'], array('pending', 'shipped', 'delivered', 'cancelled'))) ? $sort['status'] : '%';
        
        if (is_array($sort) && isset($sort['order_by'], $sort['order']) )
         {
             // ORDER BY
             switch ($sort['order_by']) {

                 case self::ORDER_BY_CREATED:
                     $param_order_by = 'order_date';
                     break;

                 case self::ORDER_BY_MODIFIED:
                     $param_order_by = 'date_modified';
                     break;

                 case self::ORDER_BY_TOTAL:
                     $param_order_by = 'total_cost';
                     break;    

                 case self::ORDER_BY_DELIVERY_DATE:
                     $param_order_by = 'exp_delivery_date';
                     break;                        

                 default :
                     $param_order_by = 'order_date';
                     break;
             }


             // ASC | DESC
             switch ($sort['order']) {

                 case self::ORDER_ASC:
                     $param_order = 'ASC';
                     break;

                 case self::ORDER_DESC:
                     $param_order = 'DESC';
                     break;

                 default :
                     $param_order = 'ASC';
                     break;
             }                    


         }

         // Defaults                
         else {

                 $param_order = 'ASC';
                 $param_order_by = 'order_date';

         }     


      
        
        // Check if index is specified
        if (is_array($sort) && isset($sort['index']) && is_string($sort['index']))
        {
            $param_regex = preg_match('/^[a-zA-Z]{1}$/', $sort['index']) ? '^[' . strtolower($sort['index']) . '].*' : '.*';
        }
        
        else {
            $param_regex = '.*';
        }

                  

        $param_filter = $sort['status'];
        $param_offset = 0;
        $param_limit = 999999999;
        
        $_filter_store = (!empty($_Store)) ? (' AND store_id=' . $_Store->getStoreId()) : '';
        $_filter_customer = (!empty($_Customer)) ? (' AND customer_id=' . $_Customer->getCustomerId()) : '';
        
        
        // PREPARE SELECT STATEMENT...
        $select_handle = $instance->dbh->prepare('
                                                    SELECT order_id 
                                                    FROM orders 
                                                    WHERE order_status LIKE :filter  
                                                    ' . $_filter_store . $_filter_customer . ' 
                                                    ORDER BY '. $param_order_by . ' ' . $param_order .  
                                                    ' LIMIT :offset, :limit
                                        ');
        
        $select_handle->bindParam(':filter', $param_filter, \PDO::PARAM_STR);
        $select_handle->bindParam(':offset', $param_offset, \PDO::PARAM_INT);
        $select_handle->bindParam(':limit', $param_limit, \PDO::PARAM_INT);



        if (!$select_handle->execute()) $instance->e->triggerException('
                                                                        Error in class (' . __CLASS__ . '): [ ' . 
                                                                        implode(', ', $select_handle->errorInfo()) . 
                                                                        ' ] on line ' . __LINE__);
        
        
        while ($row = $select_handle->fetch(\PDO::FETCH_ASSOC))$instance->_collection[] = $row['order_id'];
        
        $instance->pageSetup($page_size, $page_num);
        
        return $instance;
        
        
        
        
    }

    








    public function current()
    {

        return \Cataleya\Store\Order::load($this->_collection[$this->_position]);
        
    }
    
    
    
    
    
    
    
    
    /*
     * 
     * [ search ]
     * 
     */
    
    public static function search ($_term = '', $sort = array('order_by'=>'', 'order'=>'', 'index'=>''), $page_size = 50, $page_num = 1) 
    {
        
        
        

                // This is used to determine which column is to be used in the index...
                // $_index_with = 'name';


            if (is_array($sort) && isset($sort['order_by'], $sort['order']) )
             {
                 // ORDER BY
                 switch ($sort['order_by']) {

                     case self::ORDER_BY_TITLE:
                         $param_order_by = 'keywords';
                         break;

                     case self::ORDER_BY_ID:
                         $param_order_by = 'order_id';
                         break;                        

                     default :
                         $param_order_by = 'keywords';
                         break;
                 }


                 // ASC | DESC
                 switch ($sort['order']) {

                     case self::ORDER_ASC:
                         $param_order = 'ASC';
                         break;

                     case self::ORDER_DESC:
                         $param_order = 'DESC';
                         break;

                     default :
                         $param_order = 'ASC';
                         break;
                 }                    


             }

             // Defaults                
             else {

                     $param_order = 'ASC';
                     $param_order_by = 'order_id';

             }     




            // First, take out funny looking characters...
            $_term = preg_replace('/[^-\s\.A-Za-z0-9@*#+]+/', '', $_term);

            // Trim then split terms by [:space:]
            $terms = explode(' ', trim($_term, ' '));



            // SORT SEARCH TERMS
            $regex = array();
            $regex['keywords'] = array();


            foreach ($terms as $key=>$val) {

                    // Loose any search terms shorter than 3 chars...
                    if (strlen($val) < 2) unset($terms[$key]);
                    
                    // Also make dots safe...
                    else if (preg_match('/[-\s\.A-Za-z0-9@*#+%]{1,100}/', $val))  $regex['keywords'][] = preg_replace('/[\.]/', '\.', $val); 
            }

            
            // Build REGEX
            $regex_concat = '';
            foreach ($regex as $k => $v) {
                    if (count($v) == 0) {$regex[$k] = ''; continue; } // no search terms...
                    elseif (count($v) > 3) $v = array_slice($v, 0, 3); // truncate if there are too many search terms...

                    $regex[$k] = "^.*(" . implode('|', $v) . ").*$";
                    $regex_concat .= ($regex_concat != '') ? ' OR ' : '';
                    $regex_concat .= $k . ' REGEXP :regex_' . $k;


            }

            
            
            // Create instance...
            $_objct = __CLASS__;
            $instance = new $_objct;


            // If nothing made it through the filtering...
            // [ _collection ] will be empty a.k.a no results..
            if ($regex_concat == '') return $instance;




            $param_offset = 0;
            $param_limit = 50;
        
            
            
            // DO SEARCH...
            $select_handle = \Cataleya\Helper\DBH::getInstance()->prepare('
                                                                            SELECT order_id 
                                                                            FROM fulltext_order_index_isam  
                                                                            WHERE ' . $regex_concat . '  
                                                                            ORDER BY '. $param_order_by . '   
                                                                            LIMIT :offset, :limit
                                                                        ');

            $select_handle->bindParam(':offset', $param_offset, \PDO::PARAM_INT);
            $select_handle->bindParam(':limit', $param_limit, \PDO::PARAM_INT);
            
            // bind search search params
            foreach ($regex as $k=>$v) $select_handle->bindParam(':regex_'.$k, $regex[$k], \PDO::PARAM_STR);
            


            if (!$select_handle->execute()) \Cataleya\Helper\ErrorHandler::getInstance ()->triggerException('
                                                                        Error in class (' . __CLASS__ . '): [ ' . 
                                                                        implode(', ', $select_handle->errorInfo()) . 
                                                                        ' ] on line ' . __LINE__);





            while ($row = $select_handle->fetch(\PDO::FETCH_ASSOC))$instance->_collection[] = $row['order_id'];

            $instance->pageSetup($page_size, $page_num);

            return $instance;
        
    }
    

    
    
}


