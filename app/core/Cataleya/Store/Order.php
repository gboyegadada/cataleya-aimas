<?php


namespace Cataleya\Store;



/*

CLASS STOREFRONT ORDER

*/



class Order 
extends \Cataleya\Collection 
implements \Cataleya\System\Event\Observable
{
    
    
	private $dbh, $e;
	private $_data = array();
	private $_modified = array();
        
    protected static $_events = [
            'order/created',
            'order/updated', 
            'order/deleted', 
        
            'order/cancelled', 
            'order/closed', 
        
            'order/status/updated', 
        
            'order/payment/status/updated'
        ];
    
        // data cache
        private 
                $_tax_amount = 0, 
                $_total_cost = 0, 
                $_quantity = 0, 
                $_discounts = [], 
                $_grand_total = 0, 
                $_shipping_option, 
                $_shipping_address,     
                $_billing_address, 
                $_pdf_path, 
                $_currency, 
                $_store;
    
        // To be used by [\Cataleya\Collection] to iterate privileges assigned to this role...
        protected $_collection = array ();
        protected $_position = 0;

	/*
	 *
	 * [ __construct ]
	 * ________________________________________________________________
	 *
	 *
	 *
	 */
	 
	 
	public function __construct ()
	{
            
                
                parent::__construct();
		
		// Get database handle...
        $this->dbh = \Cataleya\Helper\DBH::getInstance();
		
		// Get error handler
		$this->e = \Cataleya\Helper\ErrorHandler::getInstance();
		
	}
	




	/*
	 *
	 * [ __destruct ]
	 * ________________________________________________________________
	 *
	 *
	 *
	 */

    public function __destruct()
    {
		$this->saveData();
    }
	




	/**
         * 
         * @param type $id
         * @return \Cataleya\Store\Order
         * 
         */
	public static function load ($id = 0)
	{
		
                if ($id == 0 || filter_var($id, FILTER_VALIDATE_INT) == FALSE) return NULL;
		
		$instance = new \Cataleya\Store\Order ();
		
		// LOAD: CURRENCY
		static $instance_select, $param_instance_id;
		
			
		if (!isset($instance_select)) {
			// PREPARE SELECT STATEMENT...
			$instance_select = $instance->dbh->prepare('SELECT * FROM orders WHERE order_id = :instance_id LIMIT 1');
			$instance_select->bindParam(':instance_id', $param_instance_id, \PDO::PARAM_INT);
		}
		
		
		$param_instance_id = $id;
		
		if (!$instance_select->execute()) $instance->e->triggerException('
												Error in class (' . __CLASS__ . '): [ ' . 
												implode(', ', $instance_select->errorInfo()) . 
												' ] on line ' . __LINE__);
		
		$instance->_data = $instance_select->fetch(\PDO::FETCH_ASSOC);
                
                // Check if directory is already created and has the proper permissions
                $_PDF_DIR = ROOT_PATH.'var/cache/order-pdfs';
                if (!file_exists($_PDF_DIR)) mkdir($_PDF_DIR , 0755) ;
                // if (!is_writable($_PDF_DIR)) chmod($_PDF_DIR , 0700) ; 
                
                $instance->_pdf_path = $_PDF_DIR.'/order-'.str_pad(strval($instance->getID()), 5, '0', STR_PAD_LEFT).'.pdf';
                return $instance->loadOrderDetails()->pageSetup();
	
		
		
	}
        
        
        
        
        
        /*
         * 
         * [ loadOrderDetails ]
         * 
         * 
         */
        
        private function loadOrderDetails () 
        {
                            
                
		// LOAD: ORDER DETAILS
		static 
                        $instance_select, 
                        $param_instance_id;
		
			
		if (!isset($instance_select)) {
                    
			// PREPARE SELECT STATEMENT...
			$instance_select = $this->dbh->prepare('
                                                                    SELECT order_detail_id, subtotal, tax, quantity FROM order_detail 
                                                                    WHERE order_id = :instance_id'
                                                                    );
			$instance_select->bindParam(':instance_id', $param_instance_id, \PDO::PARAM_INT);
		}
		

                $param_instance_id = $this->getID(); 
                        
		
		if (!$instance_select->execute()) $this->e->triggerException('
                                                                                    Error in class (' . __CLASS__ . '): [ ' . 
                                                                                    implode(', ', $instance_select->errorInfo()) . 
                                                                                    ' ] on line ' . __LINE__);
                
		
                while ($row = $instance_select->fetch(\PDO::FETCH_ASSOC)) 
                {
                    $this->_collection[] = \Cataleya\Store\OrderDetail::load($row['order_detail_id']);
                    
                    // Update tax amount..
                    $this->_tax_amount += round(floatval($row['tax']), 2);
                    
                    // Update total cost..
                    $this->_total_cost += round(floatval($row['subtotal']), 2);
                    
                    // Update quantity
                    $this->_quantity += round(floatval($row['quantity']), 2);
                }
                    
                // Update grand total..
                $this->_grand_total = $this->_total_cost + $this->_tax_amount + $this->getShippingCharges();
                
                
                return $this;
                            
        }
        
        
        
        
 
        
        
        
        
        
    
    /**
     * getEvents
     * 
     * @return array
     */
    public static function getEvents() {

        return self::$_events;
    }
        



	/**
         * 
         * @param \Cataleya\Customer\Cart $_Cart
         * @return \Cataleya\Store\Order
         * 
         */
	static public function create (\Cataleya\Customer\Cart $_Cart) 
	{
		 

		
		// Get database handle...
                $dbh = \Cataleya\Helper\DBH::getInstance();
		
		// Get error handler
		$e = \Cataleya\Helper\ErrorHandler::getInstance();
                
        $dbh->beginTransaction();

        if (!$_Cart->isReady()) return NULL;

        $_Store = $_Cart->getStore();
        $_Customer = $_Cart->getCustomer();
        $_ShippingOption = $_Cart->getShippingOption();

        $_StoreAddress = $_Store->getDefaultContact();
        $_Country = $_StoreAddress->getCountry();
        $_country_or_province = ($_Country->hasProvinces()) 
                                ? $_StoreAddress->getProvince()
                                : $_Country;

        $_ShippingAddress = \Cataleya\Asset\Contact::create($_country_or_province, array('label' => 'Shipping Address'))->copy($_Cart->getShippingAddress());

		// construct
		static 
				$instance_insert,  
                                $param_store_id,   
                                $param_store_name,  
                                $param_currency_code, 
                                $param_customer_id, 
                                $param_shipping_option_id, 
                                $param_shipping_address_id, 
                                $param_payment_processor_id, 
                                $param_shipping_description, 
                                $param_shipping_delivery_days;
		
		if (empty($instance_insert))
		{
			$instance_insert = $dbh->prepare('
                                                        INSERT INTO orders 
                                                        (store_id, store_name, customer_id, currency_code, shipping_option_id, shipping_description, shipping_address_id, payment_processor_id, exp_delivery_date, order_date, date_modified) 
                                                        VALUES (:store_id, :store_name, :customer_id, :currency_code, :shipping_option_id, :shipping_description, :shipping_address_id, :payment_processor_id, DATE_ADD(NOW(), INTERVAL :max_delivery_days DAY), NOW(), NOW())
                                                        ');
															
			$instance_insert->bindParam(':currency_code', $param_currency_code, \PDO::PARAM_STR);
            $instance_insert->bindParam(':store_id', $param_store_id, \PDO::PARAM_INT);
            $instance_insert->bindParam(':store_name', $param_store_name, \PDO::PARAM_STR);
            $instance_insert->bindParam(':shipping_description', $param_shipping_description, \PDO::PARAM_STR);
            $instance_insert->bindParam(':customer_id', $param_customer_id, \PDO::PARAM_INT);
            $instance_insert->bindParam(':shipping_option_id', $param_shipping_option_id, \PDO::PARAM_INT);
            $instance_insert->bindParam(':shipping_address_id', $param_shipping_address_id, \PDO::PARAM_INT);
            $instance_insert->bindParam(':payment_processor_id', $param_payment_processor_id, \PDO::PARAM_STR);
            $instance_insert->bindParam(':max_delivery_days', $param_shipping_delivery_days, \PDO::PARAM_INT);
		}
		
                $param_store_id = $_Store->getStoreId();
                $param_store_name = $_Store->getDescription()->getTitle('EN'); 
                $param_currency_code = $_Store->getCurrencyCode();
                $param_customer_id = $_Customer->getCustomerId();
                $param_shipping_option_id = $_ShippingOption->getID();
                $param_shipping_description = $_ShippingOption->getDescription()->getTitle('EN');
                $param_shipping_delivery_days = $_ShippingOption->getMaxDeliveryDays();
                $param_shipping_address_id = $_ShippingAddress->getID();
                $param_payment_processor_id = $_Cart->getPaymentTypeID();
                
		
		if (!$instance_insert->execute()) $e->triggerException('
										Error in class (' . __CLASS__ . '): [ ' . 
										implode(', ', $instance_insert->errorInfo()) . 
										' ] on line ' . __LINE__);
										
		
		// AUTOLOAD NEW PRODUCT AND RETURN IT
		$instance = self::load($dbh->lastInsertId());
                
        $instance->processCart($_Cart)->saveData();
        
        $dbh->commit();
            
		\Cataleya\System\Event::notify('order/created', $instance, array ());
                
		return $instance;
		
		
	}


	 
	 /*
	 *  [ delete ]
	 * ________________________________________________________________
	 * 
	 * 
	 *
	 *
	 */

	 
	public function delete () 
	{
		// foreach (\Cataleya\Payment\Transactions::load($this) as $_Tranx) $_Tranx->delete();
                
            
        // Delete rows containing 'store_id' in specified tables
        \Cataleya\Helper\DBH::sanitize(array('coupon_redeem_track', 'order_status_history', 'order_detail', 'shipment', 'fulltext_order_index_isam', 'orders'), 'order_id', $this->getID());

		
		// $this = NULL;
		\Cataleya\System\Event::notify('order/deleted', NULL, array ());
                
		return TRUE;
		
		
	}



        public function current()
        {
            return $this->_collection[$this->_position];

        }


      
    

        /*
         *
         * [ getID ] 
         *_____________________________________________________
         *
         *
         */

        public function getID()
        {
                return $this->_data['order_id'];
        }
        
        
        
        
        /*
         *
         * [ getOrderNumber ] 
         *_____________________________________________________
         *
         *
         */
        
        public function getOrderNumber () 
        {
                return str_pad(strval($this->getID()), 5, '0', STR_PAD_LEFT);
        }








        public function saveData () 
	{
		

		if (empty($this->_modified)) return;
                $this->_modified = array_unique($this->_modified);
                
		$_update_params = array();
		$key_val_pairs = array();
		
		foreach ($this->_modified as $key) $key_val_pairs[$key] = $key . ' = :' . $key;
		
	
		// Do prepared statement for 'INSERT'...
		$update_handle = $this->dbh->prepare('
                                                        UPDATE orders 
                                                        SET ' . implode (', ', $key_val_pairs) . ', date_modified=NOW()       
                                                        WHERE order_id = :instance_id 
                                                        ');
                
		$update_handle->bindParam(':instance_id', $_update_params['order_id'], \PDO::PARAM_INT);
                $_update_params['order_id'] = $this->_data['order_id'];
                
		foreach ($this->_modified as $key) {
                    
                        $_update_params[$key] = $this->_data[$key];                        
			$update_handle->bindParam(':'.$key, $_update_params[$key], \Cataleya\Helper\DBH::getTypeConst($_update_params[$key]));

		}
		

		
		if (!$update_handle->execute()) $this->e->triggerException('Error in class (' . __CLASS__ . '): [ ' . implode(', ', $update_handle->errorInfo()) . ' ] on line ' . __LINE__);
		
                
		\Cataleya\System\Event::notify('order/updated', $this, array ('fields'=>$this->_modified));
                $this->_modified = array ();
                
	}
	
	
       
 
 

        
        

        /**
         * Turns cart items into order
         * 
         * @param \Cataleya\Customer\Cart $_Cart
         * @return \Cataleya\Store\Order
         * 
         */
        public function processCart (\Cataleya\Customer\Cart $_Cart) 
        {

            
            // Add line items
            foreach ($_Cart as $_CartItem) 
            {
                $this->addCartItem($_CartItem);
            }

            // Add discounts
            $this->_discounts = $_Cart->getDiscounts();
            if (!empty($this->_discounts)) {

                $this->_data['discount_json'] = json_encode($this->_discounts);
                $this->_modified[] = 'discount_json';

            }
            
            // Calculate Totals
            $this->recalculateTotals ();
            $this->updateFullTextIndex();

            return $this;
        }
        
        
        
        /*
         * 
         * [ addCartItem ]
         * ___________________________________________________
         * 
         * Turns cart items into order
         * 
         */
        
        public function addCartItem (\Cataleya\Customer\Cart\Item $_CartItem, $_CALC_TOTALS = FALSE) 
        {
                    static $App;
                    
                    
                    if (empty($App)) $App = \Cataleya\System::load();
    
                    $_Store = $this->getStore();
                    $_STORE_LANG = $_Store->getLanguageCode();
            

                    $item_product = $_CartItem->getProduct();

                    $item_opt = $_CartItem->getProductOption();
                    $subtotal = $_CartItem->getSubTotal();
                    

                    
                    $_OrderDetail = \Cataleya\Store\OrderDetail::create($this, $item_opt);
                    
                    
                    
                    
                    $_description = [];
                    foreach ($item_opt as $_Attribute) $_description[] = $_Attribute->getValue();
                    
                    $_sku = ($item_opt->getSKU() === "") ? '' : '(' . $item_opt->getSKU() . ')';
                    $_OrderDetail->setItemName($item_product->getDescription()->getTitle($_STORE_LANG) . $_sku)
                                    ->setItemDescription(implode(' - ', $_description))
                                    ->setQuantity($_CartItem->getQuantity())
                                    ->setPrice($_CartItem->getCost()) 
                                    ->setOriginalPrice($_CartItem->getCost(true)) 
                                    ->setSubtotal($subtotal);
                    
                    // Product codes
                    
                    $codes = array (
                        'upc' => $item_opt->getUPC(), 
                        'ean' => $item_opt->getEAN(), 
                        'sku' => $item_opt->getSKU(), 
                        'isbn' => $item_opt->getISBN()
                    );
                    if (!empty($codes['upc'])) $_OrderDetail->setUPC($codes['upc']);
                    if (!empty($codes['ean'])) $_OrderDetail->setEAN($codes['ean']);
                    if (!empty($codes['sku'])) $_OrderDetail->setSKU($codes['sku']);
                    if (!empty($codes['isbn'])) $_OrderDetail->setISBN($codes['isbn']);
                    
                    
                    
                    
                    $item_product->updateQuantitySold($_Store, $_CartItem->getQuantity(), $subtotal);
                    

                    // Calculate tax
                    if ($item_opt->isTaxable()) 
                    {
                        $_TaxRules = \Cataleya\Tax\TaxRules::load('product');
                        $_tax_total = 0;

                        $_address = $this->getShippingAddress();

                        $_country = $_address->getCountry();
                        $_country_or_state = ($_country->hasProvinces()) ? $_address->getProvince() : $_country;
                        $_tax_json = array ();

                        foreach ($_TaxRules as $_TaxRule) 
                        {
                            $_amount = $_TaxRule->calculateTaxAmount($_Store, $_CartItem->getSubtotal(), $_country_or_state);
                            $_tax_total += $_amount;


                            if ($_amount > 0) $_tax_json[] = array (
                                                                        'id'=>$_TaxRule->getID(), 
                                                                        'description'=>($_TaxRule->getDescription()->getTitle('EN') . ' (' . $_TaxRule->getRate() . '%)'), 
                                                                        'amount' =>  $_amount
                                                                    );
                        }
                        
                        $_OrderDetail
                        ->setTaxJson($_tax_json)
                        ->setTax($_tax_total);

                    }
                    
                    
                    $_OrderDetail->saveData();
                    $this->_collection[] = $_OrderDetail;


                    $_Stock = $item_opt->getStock();
                    $_new_stock = $_Stock->removeQuantity($_Store, (float)$_CartItem->getQuantity())->getValue($_Store);
                    

                    
                    // Notify Admin...
                    if ($_new_stock < 1) \Cataleya\System\Event::notify('catalog/stock/out', $_Stock, array ('count'=>1));
                    else if ($_new_stock < 5) \Cataleya\System\Event::notify('catalog/stock/low', $_Stock, array ('count'=>1));

                    if ($_CALC_TOTALS === TRUE) $this->recalculateTotals ();
                    
                    return $this;
        }
        
        
        
        
        
        
        
        




        /*
         * 
         * [ recalculateTotals ]
         * ___________________________________________________
         * 
         * 
         */
        
        
        private function recalculateTotals () 
        {
            
                $this->_grand_total = $this->_tax_amount = $this->_total_cost = $this->_quantity = 0;
                
                foreach ($this->_collection as $_order_item) 
                {
                    
                    // Update tax amount..
                    $this->_tax_amount += $_order_item->getTax();
                    
                    // Update total cost..
                    $this->_total_cost += $_order_item->getSubtotal();
                    
                    
                    // Update quantity
                    $this->_quantity += $_order_item->getQuantity();
                }

                // Add SALE discounts (e.g. Sales --or-- Quantity discounts e.t.c) .. 
                $_discounts = $this->getDiscounts();
                if (!empty($_discounts)) {
                    
                    foreach ($_discounts as $_d) $this->_total_cost -= (float)$_d['less'];

                }
                    
                // Update grand total..
                $this->_grand_total = $this->_total_cost + $this->_tax_amount + $this->getShippingCharges(TRUE);
                
                
                $this->setTotalCost($this->_total_cost + $this->_tax_amount);
                
                return TRUE;
        }
        
        
        

        /*
         *
         * [ setTotalCost ] 
         *_____________________________________________________
         *
         *
         */

        public function setTotalCost($value = NULL)
        {
                $value = filter_var($value, FILTER_VALIDATE_FLOAT, FILTER_NULL_ON_FAILURE);

                if ($value===NULL) $this->e->triggerException('Error in class (' . __CLASS__ . '): [ invalid argument for method: ' . __FUNCTION__ . ' ] on line ' . __LINE__);

                $this->_data['total_cost'] = $value;
                $this->_modified[] = 'total_cost';

                return $this;
        }





        /*
         *
         * [ getOrderId ] 
         *_____________________________________________________
         *
         *
         */

        public function getOrderId()
        {
                return $this->_data['order_id'];
        }






        /*
         *
         * [ getOrderDate ] 
         *_____________________________________________________
         *
         *
         */

        public function getOrderDate($_GMT = TRUE)
        {
            if (!is_bool($_GMT)) $this->e->triggerException('Error in class (' . __CLASS__ . '): [ invalid argument for method: ' . __FUNCTION__ . ' ] on line ' . __LINE__);

            $_DateTime = new \DateTime($this->_data['order_date']);
            if (!$_GMT) $_DateTime->setTimezone($this->getStore()->getTimezone());

            return $_DateTime;
        }







        /*
         *
         * [ getDateModified ] 
         *_____________________________________________________
         *
         *
         */

        public function getDateModified($_GMT = TRUE)
        {
            if (!is_bool($_GMT)) $this->e->triggerException('Error in class (' . __CLASS__ . '): [ invalid argument for method: ' . __FUNCTION__ . ' ] on line ' . __LINE__);

            $datetime = new \DateTime($this->_data['date_modified']);
            if (!$_GMT) $datetime->setTimezone($this->getStore()->getTimezone());

            return $datetime;
        }






        /*
         *
         * [ getShippingOption ] 
         *_____________________________________________________
         *
         *
         */

        public function getShippingOption()
        {
            if (empty($this->_shipping_option)) $this->_shipping_option = \Cataleya\Shipping\ShippingOption::load ($this->_data['shipping_option_id']);
            
            return $this->_shipping_option;
        }






        /*
         *
         * [ getShippingCharges ] 
         *_____________________________________________________
         *
         *
         */

        public function getShippingCharges($_RECALC = FALSE, $_BEFORE_TAX = FALSE)
        {
                if ($_RECALC === TRUE) $this->calcShippingCharges ();
                
                
                if ($_BEFORE_TAX === TRUE) return round(floatval($this->_data['shipping_before_tax']), 2);
                else return round(floatval($this->_data['shipping_charges']), 2);
        }
        
        
        
        /*
         *
         * [ getShippingTaxes ] 
         *_____________________________________________________
         *
         *
         */

        public function getShippingTaxes()
        {
                if (!empty($this->_data['shipping_tax_json'])) return json_decode ($this->_data['shipping_tax_json'], TRUE);
                else return array ();
        }
        
        
        
        /*
         *
         * [ getShippingDescription ] 
         *_____________________________________________________
         *
         *
         */

        public function getShippingDescription()
        {
                return $this->_data['shipping_description'];
        }

   
        
        
        
        /*
         *
         * [ calcShippingCharges ] 
         *_____________________________________________________
         *
         *
         */

        public function calcShippingCharges()
        {


                $_total = $_tax_total = 0;

                $_ShippingOption = $this->getShippingOption();
                $_rate = $_ShippingOption->getRate();
                $_type = $_ShippingOption->getRateType();
                $_address = $this->getShippingAddress();

                $_country = $_address->getCountry();
                $_country_or_state = ($_country->hasProvinces()) ? $_address->getProvince() : $_country;


                /*
                // Calculate shipping cost
                foreach ($this->_collection as $_order_item) 
                {

                    // Update total..
                    $_total += ($_order_item->getQuantity() * $_rate);

                }
                */
                
                

                // Calculate shipping cost
                
                switch ($_type)
                {
                    case 'unit': foreach ($this as $_order_item) { $_total += ($_order_item->getQuantity() * $_rate); } break;
                    case 'flat': foreach ($this as $_order_item) { $_total += $_rate; } break;
                    case 'weight': foreach ($this as $_order_item) { $_total += ($_order_item->getProductOption()->getWeight() * $_rate); } break;
                    default: foreach ($this as $_order_item) { $_total += ($_order_item->getQuantity() * $_rate); }
                }
                
                
                $this->_data['shipping_before_tax'] = $_total;
                
                // Calculate shipping tax

                if ($this->getShippingOption()->isTaxable()) 
                {
                    $_TaxRules = \Cataleya\Tax\TaxRules::load('shipping');
                    $_tax_total = 0;
                    $_tax_json = array ();

                    foreach ($_TaxRules as $_TaxRule) 
                    {
                        $_amount = $_TaxRule->calculateTaxAmount($this->getStore(), $_total, $_country_or_state);
                        $_tax_total += $_amount;
                
                        
                        if ($_amount > 0) $_tax_json[] = array (
                                                                    'id'=>$_TaxRule->getID(), 
                                                                    'description'=>($_TaxRule->getDescription()->getTitle('EN') . ' (' . $_TaxRule->getRate() . '%)'), 
                                                                    'amount' =>  $_amount, 
                                                                    'pretty_amount' => number_format ($_amount, 2)
                                                                );
                    }
                    
                    $this->_data['shipping_tax_json'] = json_encode($_tax_json);
                    $this->_modified[] = 'shipping_tax_json';

                    $_total += $_tax_total;
                }
                
                
                
                
                
                $this->_data['shipping_charges'] = $_total;
                $this->_modified[] = 'shipping_charges';
                $this->_modified[] = 'shipping_before_tax';

                return $this;
        }


        
        
        
        
        
        
        /*
         *
         * [ getDiscounts ] 
         *_____________________________________________________
         *
         *
         */

        public function getDiscounts()
        {
            if (empty($this->_discounts) && !empty($this->_data['discount_json'])) {
                $this->_discounts = json_decode($this->_data['discount_json'], true);
            }

            return $this->_discounts;
        }
        
        
        
        
        
        
        
        /*
         *
         * [ getDiscounts ] 
         *_____________________________________________________
         *
         *
         */

        public function getTaxes()
        {
            $_taxes = array ();
            
            foreach ($this->_collection as $_OrderDetail) 
            {
                
                $_tax_json = $_OrderDetail->getTaxes();
                
                
                foreach ($_tax_json as $_TaxRule) {
                    $_id = $_TaxRule['id'];
                    
                    if (isset($_taxes[$_id])) $_taxes[$_id]['amount'] += (float)$_TaxRule['amount'];
                    else $_taxes[$_id] = array (
                        'id' =>  $_id, 
                        'description'=>$_TaxRule['description'],
                        'amount'=>(float)$_TaxRule['amount']
                    );
                    
                    $_taxes[$_id]['pretty_amount'] = number_format($_taxes[$_id]['amount'], 2);
                }
                
            }
            
            $_taxes = array_values($_taxes);
            return $_taxes;
        }
        
        
        
        
        


        /*
         *
         * [ getStoreId ] 
         *_____________________________________________________
         *
         *
         */

        public function getStoreId()
        {
                return $this->_data['store_id'];
        }



        /*
         *
         * [ getStore ] 
         *_____________________________________________________
         *
         *
         */

        public function getStore()
        {
                if (empty($this->_store)) $this->_store = \Cataleya\Store::load ($this->_data['store_id']);
                
                return $this->_store;
        }




        /*
         *
         * [ getCustomerId ] 
         *_____________________________________________________
         *
         *
         */

        public function getCustomerId()
        {
                return $this->_data['customer_id'];
        }
        
        
        
        /*
         *
         * [ getCustomer ] 
         *_____________________________________________________
         *
         *
         */

        public function getCustomer()
        {
                return \Cataleya\Customer::load($this->_data['customer_id']);
        }


        /*
         *
         * [ setCustomer ] 
         *_____________________________________________________
         *
         *
         */

        public function setCustomer(\Cataleya\Customer $_Customer)
        {

                $this->_data['customer_id'] = $_Customer->getCustomerId();
                $this->_modified[] = 'customer_id';

                return $this;
        }
        
        
        
        



        /*
         *
         *  [ setShippingAddress ]
         * ________________________________________________________________
         * 
         * 
         *
         *
         *
         */


        public function setShippingAddress (\Cataleya\Asset\Contact $_Contact)
        {
                $this->getShippingAddress()->copy($_Contact);
                
                return $this;
        }
        
        
        
        

        /*
         *
         * [ getShippingAddress ] 
         *_____________________________________________________
         *
         *
         */

        public function getShippingAddress()
        {
                if (empty($this->_shipping_address)) $this->_shipping_address = \Cataleya\Asset\Contact::load ($this->_data['shipping_address_id']);
                return $this->_shipping_address;
        }
        
        
        
        
        /*
         *
         *  [ setRecipient ]
         * ________________________________________________________________
         * 
         * 
         *
         *
         *
         */


        public function setRecipient (\Cataleya\Asset\Contact $_Contact)
        {
                return $this->setShippingAddress($_Contact);
        }
        
        



        /*
         *
         * [ getRecipient ] 
         *_____________________________________________________
         *
         *
         */

        public function getRecipient()
        {
                return $this->getShippingAddress();
        }
        
        
        


        /*
         *
         *  [ setBillingAddress ]
         * ________________________________________________________________
         * 
         * 
         *
         *
         *
         */


        public function setBillingAddress (\Cataleya\Asset\Contact $_Contact)
        {
                $this->getBillingAddress()->copy($_Contact);
                
                return $this;
        }
        


        /*
         *
         * [ getBillingAddress ] 
         *_____________________________________________________
         *
         *
         */

        public function getBillingAddress()
        {
                if (empty($this->_billing_address)) $this->_billing_address = \Cataleya\Asset\Contact::load ($this->_data['billing_address_id']);
                return $this->_billing_address;
        }
        
        
        
        
        
        
        
        
        


        /*
         *
         * [ getOrderStatus ] 
         *_____________________________________________________
         *
         *
         */

        public function getOrderStatus()
        {
                return $this->_data['order_status'];
        }



        /*
         *
         * [ setOrderStatus ] 
         *_____________________________________________________
         *
         *
         */

        public function setOrderStatus($value = NULL)
        {
                
                $_enum = array('pending', 'shipped', 'delivered', 'cancelled'); // as in table column...
                if (
                    $value===NULL || 
                    !is_string($value) || 
                    !in_array(strtolower($value), $_enum)
                ) throw new Error ('Invalid argument for method.');

                $value = strtolower($value);
                if ($value === $this->getOrderStatus()) return $this;
                
                $this->_data['order_status'] = $value;
                $this->_modified[] = 'order_status';
                
                if (file_exists($this->_pdf_path)) unlink ($this->_pdf_path);
                
                
                $_AdminUser = \__('dash.user');
                if (!empty($_AdminUser)) $this->setOrderProcessedBy($_AdminUser);
                
                
                \Cataleya\System\Event::notify('order/status/updated', $this, array ('status'=>$value));

                return $this;
        }
        
        
        

        /**
         * 
         * @param string $_payment_type     This is a plugin key for the plugin (obviously) that will 
         *                                  handle payment option selected by customer. Eg. Plugin key for 
         *                                  PayPal Express plugin.
         * 

         * @return \Cataleya\Store\Order
         * 
         */
        public function setPaymentType($_payment_type)
        {

                $this->_data['payment_processor_id'] = strtolower(trim($_payment_type));
                $this->_modified[] = 'payment_processor_id';

                return $this;
        }
        
        
        


        /**
         * 
         * @return string   This is a plugin key for the plugin (obviously) that will 
         *                  handle payment option selected by customer. Eg. Plugin key for 
         *                  PayPal Express plugin.
         * 
         */
        public function getPaymentTypeID()
        {
            return $this->_data['payment_processor_id'];
            
        }


        public function getPaymentType () 
        {
            $_payment_plugin_key = $this->getPaymentTypeID();
            if (empty($_payment_plugin_key)) { return null; }
            
            $_payment_options = \Cataleya\Plugins\Action::trigger(
                'payment.get-options', 
                [ 'params' => [ 'currency_codes' => [ $this->getCurrencyCode() ] ] ], 
                [ $_payment_plugin_key ]
            );

            if (!empty($_payment_options)) { 
                return $_payment_options[0];
            }


            return null;
        }





        /*
         *
         * [ getPaymentStatus ] 
         *_____________________________________________________
         *
         *
         */

        public function getPaymentStatus()
        {

            $_paid = ($this->getTotalPaid() < $this->getGrandTotal()) ? false : true;
            

            if ($_paid === true && $this->_data['payment_status'] !== 'paid') 
            {
                $this->_data['payment_status'] = 'paid';
                $this->_modified[] = 'payment_status';
                
                if (file_exists($this->_pdf_path)) unlink ($this->_pdf_path);
                
                \Cataleya\System\Event::notify('order/payment/status/updated', $this, ['status'=>'paid']);
            }

            return $this->_data['payment_status'];
        }



        /*
         *
         * [ setPaymentStatus ] 
         *_____________________________________________________
         *
         *
         */

        private function setPaymentStatus($value = NULL)
        {
                $_enum = array('pending', 'paid', 'cash on delivery', 'bank transfer', 'cancelled', 'failed'); // as in table column...
                if (
                    $value===NULL || 
                    !is_string($value) || 
                    !in_array(strtolower($value), $_enum)
                ) throw new Error ('Invalid argument for method.');

                $value = strtolower($value);
                if ($value === $this->getPaymentStatus()) return $this;

                $this->_data['payment_status'] = strtolower($value);
                $this->_modified[] = 'payment_status';
                
                if (file_exists($this->_pdf_path)) unlink ($this->_pdf_path);
                
                \Cataleya\System\Event::notify('order/payment/status/updated', $this, array ('status'=>$value));

                return $this;
        }




        /**
         * getReceipts
         *
         * @return array
         */
        public function getReceipts () 
        {
            

            return  \Cataleya\Plugins\Action::trigger(
                    'payment.get-transactions', 
                    [ 'params' => ['order_id'=>$this->getOrderId()]] 
                    );

        }





        /*
         *
         * [ getTotalCost ] 
         *_____________________________________________________
         *
         *
         */

        public function getTotalCost($_RECALC = FALSE, $_SMALL_AMOUNT = false)
        {
                if ($_RECALC === TRUE) $this->recalculateTotals ();
                return ($_SMALL_AMOUNT === true) 
                    ? floatval($this->_total_cost)*100 
                    : (float)$this->_total_cost;
        }



        

        /*
         *
         * [ getTaxAmount ] 
         *_____________________________________________________
         *
         *
         */

        public function getTaxAmount($_RECALC = FALSE)
        {
                if ($_RECALC === TRUE) $this->recalculateTotals ();
                return $this->_tax_amount;
        }
        
        
        

        /*
         *
         * [ getGrandTotal ] 
         *_____________________________________________________
         *
         *
         */

        public function getGrandTotal($_RECALC = FALSE, $_SMALL_AMOUNT = false)
        {
                if ($_RECALC === TRUE) $this->recalculateTotals ();
                return ($_SMALL_AMOUNT === true) 
                    ? floatval($this->_grand_total)*100 
                    : (float)$this->_grand_total;
        }







        /**
         * getTotalPaid
         *
         * @return float
         */
        public function getTotalPaid () 
        {

            $_total_paid = 0;

            $_transactions = $this->getReceipts();
            
            if (!empty($_transactions)) 
            {   
                foreach ($_transactions as $_t) {
                    if ($_t['status'] !== 'paid') continue;

                    $_total_paid += (float)$_t['amount'];
                }
            }

            return $_total_paid;

        }


        





       
        /*
         *
         * [ getQuantity ] 
         *_____________________________________________________
         *
         *
         */

        public function getQuantity()
        {
                return $this->_quantity;
        }


        /*
         *
         * [ getCurrencyCode ] 
         *_____________________________________________________
         *
         *
         */

        public function getCurrencyCode()
        {
                return $this->_data['currency_code'];
        }




        /*
         *
         * [ getCurrency ] 
         *_____________________________________________________
         *
         *
         */

        public function getCurrency()
        {

                if (empty($this->_currency)) $this->_currency = \Cataleya\Locale\Currency::load($this->_data['currency_code']);
                return $this->_currency;
        }

        
        
        



        /*
         *
         * [ getExpectedDeliveryDate ] 
         *_____________________________________________________
         *
         *
         */

        public function getExpectedDeliveryDate()
        {
                return $this->_data['exp_delivery_date'];
        }



        /*
         *
         * [ setExpDeliveryDate ] 
         *_____________________________________________________
         *
         *
         */

        public function setExpDeliveryDate($value = NULL)
        {


                if ($value===NULL) $this->e->triggerException('Error in class (' . __CLASS__ . '): [ invalid argument for method: ' . __FUNCTION__ . ' ] on line ' . __LINE__);

                $this->_data['exp_delivery_date'] = $value;
                $this->_modified[] = 'exp_delivery_date';

                return $this;
        }
        
        
        
        
        
        
        /*
         *
         * [ setOrderProcessedBy ] 
         *_____________________________________________________
         *
         *
         */

        private function setOrderProcessedBy(\Cataleya\Admin\User $_AdminUser)
        {

                $this->_data['order_processed_by'] = $_AdminUser->getAdminID();
                $this->_modified[] = 'order_processed_by';

                return $this;
        }
        
        

        /*
         *
         * [ getOrderProcessedBy ] 
         *_____________________________________________________
         *
         *
         */

        public function getOrderProcessedBy()
        {
                return \Cataleya\Admin\User::load($this->_data['order_processed_by']);
        }
        
        
        
        
        

        /*
         * 
         * [ updateFullTextIndex ]
         * 
         */


        public function updateFullTextIndex () 
        {
            
            $_Customer = $this->getCustomer();
            
            // [1] Order + Customer Info
            $_keywords = array (
                    '#'.$this->getOrderNumber(), 
                    $this->getOrderDate(FALSE)->format('H:i:s l, d F Y A, m-d-Y'), 
                    $_Customer->getName(),
                    $_Customer->getEmailAddress(), 
                    $_Customer->getTelephone()
            );
            
            
            
            // [2] Order details
            foreach ($this->_collection as $_item) 
            {
                $_keywords[] = $_item->getItemName() . ' ' . $_item->getItemDescription();
                
                $_discounts = $this->getDiscounts();
                if (!empty($_discounts)) { 
                    foreach ($_discounts as $_dscnt) $_keywords[] = $_dscnt->{'label'}; 
                }
            }
            
            
            /*
            // [3] Transaction
            foreach (\Cataleya\Payment\Transactions::load($this) as $_Tranx) 
            {
                $_keywords[] = '#'.str_pad(strval($_Tranx->getID()), 5, '0', STR_PAD_LEFT);
                $_keywords[] = '#'.strval($_Tranx->getPaypalTransactionId());
                $_keywords[] = $_Tranx->getPaypalPayerEmail();
                break;
            }
            */
            
            // [4] Address
            $_Location = $this->getShippingAddress();
            $_keywords[] = $_Location->getEntryStreetAddress();
            $_keywords[] = $_Location->getEntrySuburb();
            $_keywords[] = $_Location->getEntryCity();
            $_keywords[] = $_Location->getEntryPostcode();
            $_keywords[] = $_Location->getEntryProvince();
            // $_keywords[] = $_Location->getEntryCountry();
            




            static $insert_handle, $param_order_id, $param_keywords;

            // Build the SQL statement.  
            if (empty($insert_handle)) {
                  $insert_handle = $this->dbh->prepare('
                                            INSERT INTO fulltext_order_index_isam (order_id, keywords) 
                                            VALUES (:order_id, :keywords ) 
                                            ON DUPLICATE KEY UPDATE keywords = :keywords
                                            ');
                  $insert_handle->bindParam(':order_id', $param_order_id, \PDO::PARAM_INT);
                  $insert_handle->bindParam(':keywords', $param_keywords, \PDO::PARAM_STR);
            }

            // ser params
            $param_order_id = $this->getID();
            $param_keywords = preg_replace('/([^-\s\.A-Za-z0-9@*#+%]+)/', ' ', implode(' ', array_unique($_keywords)));

            // execute the query.  If successful, return true 
            if (!$insert_handle->execute()) $this->e->triggerException('
                                                                            Error in class (' . __CLASS__ . '): [ ' . 
                                                                            implode(', ', $insert_handle->errorInfo()) . 
                                                                            ' ] on line ' . __LINE__);
            
            
            return $this;

          }

          
          
        
        

        
        

        /*
         *
         * [ makePDF ] 
         *_____________________________________________________
         *
         *
         */

        public function makePDF($_ALLOW_CACHE = TRUE)
        {
                
                if (file_exists($this->_pdf_path) && $_ALLOW_CACHE === TRUE) return $this->_pdf_path;

                $pdf = new \Cataleya\Helper\PDF();
                $_store_name = $this->getStore()->getDescription()->getTitle('EN');
                $_order_num = str_pad(strval($this->getID()), 5, '0', STR_PAD_LEFT);
                $_store_href = SHOP_ROOT . $this->getStore()->getURLRewrite()->getKeyword() . '/';
                
                $_Customer = $this->getCustomer();
                $_Currency = $this->getCurrency();
                $_Recipient = $this->getRecipient();
                
                $_Shipping = array (
                    'entry_street' => $_Recipient->getEntryStreetAddress() . ', ', 
                    'entry_suburb'  =>  $_Recipient->getEntrySuburb(), 
                    'entry_city'    =>  $_Recipient->getEntryCity(), 
                    'entry_postcode'    =>  $_Recipient->getEntryPostcode(), 
                    'entry_province'    =>  $_Recipient->getEntryProvince() . ', ', 
                    'entry_country' =>  $_Recipient->getEntryCountry()
                );
                
                $_Shipping['entry_suburb'] .= ((empty($_Shipping['entry_suburb'])) ? '' : ', ');
                $_Shipping['entry_city'] .= ((empty($_Shipping['entry_city'])) ? '' : ', ');
                $_Shipping['entry_postcode'] .= ((empty($_Shipping['entry_postcode'])) ? '' : ', ');
                
                
                
                $_TranxID = '#XXXX';
                $_PaymentGateway = 'Unknown';
                $_AmountPaid = 0;
                $_PaymentStatus = 'Unknown';
                
                $_receipts = $this->getReceipts();
                
                if (!empty($_receipts)) 
                {   
                    $_tranx = $_receipts[0];
                    $_multiple = (count($_receipts)>1);

                    $_TranxID = (!$_multiple) ? $_tranx['transaction_ref'] : 'multiple';
                    $_PaymentGateway = (!$_multiple) ? $_tranx['payment_processor'] : 'multiple';
                    $_AmountPaid = number_format($this->getTotalPaid(), 2) . ' ' . $this->getCurrencyCode();
                    $_PaymentStatus = strtoupper($this->getPaymentStatus());
                }
                
                

                // Meta
                $pdf
                ->AliasNbPages()
                ->SetSubject('Order #' . $_order_num . ' (at ' . $_store_name . ')', TRUE)
                ->SetTitle('Order Invoice', TRUE)



                ->AddPage()
                
                        
                /////// [1] BIG BLACK HEADER //////
                
                // white on black
                ->SetTextColor(255, 255, 255)
                ->SetDrawColor(0, 0, 0)
                
                ->SetFont('Arial','B',14)
                ->Cell(130,10, $_store_name, 'B', 0, 'L', TRUE, $_store_href)
                
                ->SetFont('Arial','B',12)
                ->Cell(60,10, 'Order #'.$_order_num, 'B', 0, 'R', TRUE)
                        
                ->Ln()
                
                        
                        
                /////// [2] GENERAL ORDER INFO //////
                
                // back to black on white
                ->SetTextColor(100, 100, 100)
                ->SetDrawColor(220, 220, 220)
                ->SetLineWidth(0.1)
                        
                ->SetFont('Arial','B',9)
                ->Ln(5)


                // row 1 (th) 
                ->Cell(63,8, 'Customer', 'R', 0, 'L') // col 1
                ->Cell(63,8, 'Shipping Address', 'R', 0, 'L')   // col 2      
                ->Cell(63,8, 'Payment', 0, 1, 'L') // col 3
                        
                ->SetFont('Times','',9)
                     
                // row 2
                ->Cell(63,5, $_Customer->getName(), 'R', 0, 'L') // col 1
                ->Cell(63,5, ($_Recipient->getEntryFirstname() . ' ' . $_Recipient->getEntryLastname() . ', '), 'R', 0, 'L')     // col 2    
                ->Cell(63,5, 'Type: '.$_PaymentGateway, 0, 1, 'L') // col 3
                        
                // row 3        
                ->Cell(63,5, 'Tel: '.$_Customer->getTelephone(), 'R', 0, 'L')  // col 1
                ->Cell(63,5, $_Shipping['entry_street'], 'R', 0, 'L')     // col 2  
                ->Cell(63,5, 'Tranx ID: '.$_TranxID, 0, 1, 'L')  // col 3    
                 
                // row 4        
                ->Cell(63,5, 'Email: '.$_Customer->getEmailAddress(), 'R', 0, 'L')  // col 1
                ->Cell(63,5, $_Shipping['entry_suburb'] . $_Shipping['entry_city'] . $_Shipping['entry_postcode'], 'R', 0, 'L')   // col 2 
                ->Cell(63,5, 'Amount: '.$_AmountPaid, 0, 1, 'L')  // col 3
                        
                // row 5        
                ->Cell(63,5, '', 'R', 0, 'L')  // col 1
                ->Cell(63,5, ($_Shipping['entry_province'] . strtoupper($_Shipping['entry_country'])), 'R', 0, 'L')   // col 2     
                ->Cell(63,5, 'Status: '.$_PaymentStatus, 0, 1, 'L')  // col 3 
                        
                // row 6      
                ->Cell(63,5, '', 'RB', 0, 'L') // col 1
                ->Cell(63,5, '', 'RB', 0, 'L')   // col 2     
                ->Cell(63,5, '', 'B', 0, 'L')   // col 3      
                
                ->Ln(5)
                ->SetFont('Arial','',9)  
                ->SetTextColor(150, 150, 150)
                        
                // Quick Info bar (with icons)  
                        
                ->Image(ROOT_PATH.'ui/images/ui/bitmap_icons/icon-calendar-2.gif',10,61,4)->Cell(5,10, '', 0, 0, 'L')        
                ->Cell(52,10, 'Ordered '.$this->getOrderDate(FALSE)->format('h:ia, d M, Y'), 'B', 0, 'L')   // order date
                        
                ->Image(ROOT_PATH.'ui/images/ui/bitmap_icons/icon-basket.gif',69,61,4)->Cell(7,10, '', 0, 0, 'L')        
                ->Cell(18,10, \Cataleya\Helper::countInEnglish($this->getQuantity(), 'item', 'items'), 'B', 0, 'L')   // items
                        
                ->Image(ROOT_PATH.'ui/images/ui/bitmap_icons/icon-flight.gif',93,61,4)->Cell(7,10, '', 'B', 0, 'L')
                ->Cell(120,10, $this->getShippingDescription(), 0, 0, 'L')  // shipping method

                ->Ln()->Cell(189,1, '', 'T', 0);
                

                switch (strtolower($_PaymentStatus)) 
                {
                    case 'paid':
                       $pdf->SetTextColor(170, 200, 160);
                        break;
                    case 'cash on delivery':
                        $pdf->SetTextColor(210, 200, 100);
                        break;
                    case 'bank transfer':
                        $pdf->SetTextColor(210, 200, 100);
                        break;
                    case 'pending':
                        $pdf->SetTextColor(210, 200, 100);
                        break;
                    default:
                        $pdf->SetTextColor(200, 100, 100);
                        break;
                }
                
                $pdf
                ->Ln(1)
                ->SetFont('Arial','B',36)
                ->Cell(185,20, $_PaymentStatus, 0, 0, 'R')
                ->Ln(25)
                        
                ->SetFont('Arial','B',10)
                ->SetTextColor(100, 100, 100)

                ->Cell(90,10, 'Item', 'B', 0, 'L')
                ->Cell(30,10,'Price', 'B', 0, 'L')
                ->Cell(30,10,'Quantity', 'B', 0, 'C')
                ->Cell(40,10,'Total', 'B', 0, 'R');

                


                foreach ($this as $_OrderItem)  
                {

                    $pdf
                    
                    ->Ln()
                    ->SetFont('Arial','B',8)
                    ->SetTextColor(100, 100, 100)

                    ->MultiAlignCell(90,5, $_OrderItem->getItemName() . ' - ' . $_OrderItem->getItemDescription(), 0, 0, 'L')

                    ->SetFont('Arial','',8)
                    ->Cell(30,10, number_format($_OrderItem->getPrice(), 2) . ' ' . $this->getCurrencyCode(), 0, 0, 'L') // price
                    ->Cell(30,10,$_OrderItem->getQuantity(), 0, 0, 'C') // quantity
                    // $pdf->SetTextColor(220, 220, 50);
                    ->Cell(40,10, number_format($_OrderItem->getSubtotal(), 2) . ' ' . $this->getCurrencyCode(), 0, 0, 'R'); // subtotal

                }
                
                
                // DISCOUNTS
                $pdf->Ln(10);
                
                $_discounts = $this->getDiscounts();
                
                foreach ($_discounts as $_discount) 
                {
                    $pdf
                    ->Ln(6)
                    ->Cell(143,10,$_discount['description'], 0, 0, 'R')
                    ->Cell(47,10, 'LESS ' . number_format($_discount['less'], 2) . ' ' . $this->getCurrencyCode(), 0, 0, 'R'); 
                }
                
                
                
                // TAXES
                
                $pdf->Ln(10);
                
                $_taxes = $this->getTaxes();
                
                foreach ($_taxes as $_Tax) 
                {
                    $pdf
                    ->Ln(6)
                    ->Cell(143,10,$_Tax['description'], 0, 0, 'R')
                    ->Cell(47,10, number_format($_Tax['amount'], 2) . ' ' . $this->getCurrencyCode(), 0, 0, 'R'); 
                }
                
                
                
                // SUBTOTAL
                
                $pdf
                
                ->Ln(10)
                ->Cell(143,10,'Subtotal:', 0, 0, 'R')
                ->Cell(47,10, number_format($this->getTotalCost(), 2) . ' ' . $this->getCurrencyCode(), 0, 0, 'R') // subtotal
                
                        
                // SHIPPING CHARGES
                        
                ->Ln(10)
                ->Cell(143,10,'Shipping Charges (' . $this->getShippingDescription() . '):', 0, 0, 'R')
                ->Cell(47,10, number_format($this->getShippingCharges(), 2) . ' ' . $this->getCurrencyCode(), 0, 0, 'R'); // shipping charges
                
                // SHIPPING TAXES
                
                $_shiping_taxes = $this->getShippingTaxes();
                
                foreach ($_shiping_taxes as $_Tax) 
                {
                    $pdf
                    ->Ln(6)
                    ->Cell(143,10,$_Tax['description'], 0, 0, 'R')
                    ->Cell(47,10, number_format($_Tax['amount'], 2) . ' ' . $this->getCurrencyCode(), 0, 0, 'R'); 
                }
                
                
                // GRAND TOTAL
                $pdf
                
                ->Ln(13)
                ->SetFont('Arial','B',9)
                ->SetTextColor(0, 0, 0)
                
                
                ->Cell(143,10,'Total:', 'T', 0, 'R')
                ->Cell(47,10, number_format($this->getGrandTotal(), 2) . ' ' . $this->getCurrencyCode(), 'T', 0, 'R'); // subtotal
                
                
                // RECEIPTS
                $pdf
                ->Ln(25)
                ->SetFont('Arial','B',18)
                ->SetTextColor(210, 200, 50)
                        
                ->Cell(0,15, 'Payment Receipts', 'B', 0, 'L')
                        
                ->Ln(25)
                ->SetFont('Arial','B',10)
                ->SetTextColor(100, 100, 100)
                        

                ->Cell(60,10, 'Payment Processor', 'B', 0, 'L')
                ->Cell(30,10,'Amount', 'B', 0, 'L')
                ->Cell(20,10,'Status', 'B', 0, 'L')
                ->Cell(30,10,'Reference', 'B', 0, 'C')
                ->Cell(40,10,'Date', 'B', 0, 'R');

                

                foreach ($_receipts as $_r)  
                {

                    $pdf
                    
                    ->Ln()
                    ->SetFont('Arial','B',8)
                    ->SetTextColor(100, 100, 100)

                    ->MultiAlignCell(60, 5, $_r['payment_processor'], 0, 0, 'L')

                    ->SetFont('Arial','',8)
                    ->Cell(30,10, number_format($_r['amount'], 2) . ' ' . $this->getCurrencyCode(), 0, 0, 'L') // price
                    ->Cell(20,10, $_r['status'], 0, 0, 'L')
                    ->Cell(30,10,$_r['transaction_ref'], 0, 0, 'C') // quantity
                    //->SetTextColor(220, 0, 0)
                    ->Cell(40,10, $_r['datetime'], 0, 0, 'R'); 

                }
                
                
                $pdf->SetCreator('The Suk App', TRUE);
                $pdf->SetAuthor($_store_name, TRUE);
                
                $pdf->Output($this->_pdf_path, 'F');
                return $this->_pdf_path;
                

        }





	
	
	
}
	
	
	
	
