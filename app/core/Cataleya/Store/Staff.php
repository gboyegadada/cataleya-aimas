<?php



namespace Cataleya\Store;



/*
 *
 * 	@Package: Cataleya
 * 	@Class: \Cataleya\Store\Staff
 *
 * 	(c) 2012 Fancy Paper Planes
 *
 *
 *
 */

class Staff extends \Cataleya\Collection {
    
    
    // To be used by [\Cataleya\Collection]
    protected $_collection = array ();
    protected $_position = 0;
    protected $_store;




    // CONST VALUES 1 - 15 RESERVED FOR [ _Abstract_Collection ]
    const ORDER_BY_FIRSTNAME = 16;
    const ORDER_BY_LASTNAME = 17;
    const ORDER_BY_FNAME_FIRST = 18;
    const ORDER_BY_LNAME_FIRST = 19;
    const ORDER_BY_EMAIL = 20;


    public function __construct(\Cataleya\Store $_Store) {
       
        // Get database handle...
        $this->dbh = \Cataleya\Helper\DBH::getInstance();
		
        // Get error handler
        $this->e = \Cataleya\Helper\ErrorHandler::getInstance();
        
        $this->_store = $_Store;

        parent::__construct();
 
        
    }
    
    
    
    
    /*
     * 
     * [ load ]
     * 
     * 
     */
    
    public static function load (\Cataleya\Store $_Store, $sort = array('order_by'=>'', 'order'=>'', 'index'=>''), $page_size = 16, $page_num = 1) 
    {
        
        $_objct = __CLASS__;
        $instance = new $_objct ($_Store);
        
        // LOAD
        
        // This is used to determine which column is to be used in the index...
        $_index_with = 'firstname';
        
        if (is_array($sort) && !empty($sort['order_by']) && !empty($sort['order']) && is_int($sort['order_by']) && is_int($sort['order']) )
        {
            

            // ASC | DESC
            switch ($sort['order']) {

                case self::ORDER_ASC:
                    $param_order = 'ASC';
                    break;

                case self::ORDER_DESC:
                    $param_order = 'DESC';
                    break;

                default :
                    $param_order = 'ASC';
                    break;
            }  
            
            
            // ORDER BY
            switch ($sort['order_by']) {

                case self::ORDER_BY_FIRSTNAME:
                    $param_order_by = "firstname $param_order";
                    $_index_with = "firstname";
                    break;

                case self::ORDER_BY_LASTNAME:
                    $param_order_by = "lastname $param_order";
                    $_index_with = "lastname";
                    break;
                
                case self::ORDER_BY_CREATED:
                    $param_order_by = "date_created $param_order";
                    break;

                case self::ORDER_BY_MODIFIED:
                    $param_order_by = "last_modified $param_order";
                    break;

                default :
                    $param_order_by = "firstname $param_order, lastname $param_order";
                    break;
            }

                  


        }

        // Defaults                
        else {

                $param_order = 'ASC';
                $param_order_by = "firstname $param_order, lastname $param_order";

        }    
        
        
        
        // Check if index is specified
        if (is_array($sort) && isset($sort['index']) && is_string($sort['index']))
        {
            $param_regex = (preg_match('/^[a-zA-Z]{1}$/', $sort['index']) > 0) ? '^[' . strtolower($sort['index']) . '].*' : '.*';
        }
        
        else {
            $param_regex = '.*';
        }

                  

        
        
        $param_store_id = $_Store->getStoreId();
        $param_offset = 0;
        $param_limit = 999999999;
        
        
        // PREPARE SELECT STATEMENT...
        $select_handle = $instance->dbh->prepare('
                                        SELECT a.admin_id 
                                        FROM admin a 
                                        INNER JOIN staff_members b 
                                        ON a.admin_id = b.admin_id 
                                        WHERE b.store_id = :store_id 
                                        AND ' . $_index_with . ' REGEXP :regex 
                                        ORDER BY '. $param_order_by . '   
                                        LIMIT :offset, :limit
                                        ');
        
        $select_handle->bindParam(':store_id', $param_store_id, \PDO::PARAM_INT);
        $select_handle->bindParam(':offset', $param_offset, \PDO::PARAM_INT);
        $select_handle->bindParam(':limit', $param_limit, \PDO::PARAM_INT);
        $select_handle->bindParam(':regex', $param_regex, \PDO::PARAM_STR);



        if (!$select_handle->execute()) $instance->e->triggerException('
                                                                        Error in class (' . __CLASS__ . '): [ ' . 
                                                                        implode(', ', $select_handle->errorInfo()) . 
                                                                        ' ] on line ' . __LINE__);
        
        
        while ($row = $select_handle->fetch(\PDO::FETCH_ASSOC))$instance->_collection[] = $row['admin_id'];
        
        $instance->pageSetup($page_size, $page_num);
        
        return $instance;
        
        
        
        
    }

    








    public function current()
    {
        
        return \Cataleya\Admin\User::load($this->_collection[$this->_position]);
        
    }
    
    
    
    
    
    
        /*
         *
         * [ getStore ] 
         *_____________________________________________________
         *
         *
         */

        public function getStore()
        {
                return $this->_store;
        }

        
    



        /*
	 *
	 *  [ addStaffMember ]
	 * ________________________________________________________________
	 * 
	 * 
	 *
	 *
	 */
	 
	public function addStaffMember (\Cataleya\Admin\User $_AdminUser) 
	{

 		
		if ($this->hasStaffMember($_AdminUser)) return $this;
			
			
                static 
                                $insert_handle;

                if (empty($insert_handle))
                {
                        $insert_handle = $this->dbh->prepare('
                                                            INSERT INTO staff_members (store_id, admin_id)
                                                            VALUES (:store_id, :admin_id)  
                                                            ');
                }

                $insert_handle->bindParam(':store_id', $insert_handle_param_store_id, \PDO::PARAM_INT);
                $insert_handle->bindParam(':admin_id', $insert_handle_param_admin_id, \PDO::PARAM_INT);
                
                $insert_handle_param_store_id = $this->getStore()->getID();
                $insert_handle_param_admin_id = $_AdminUser->getID();

                if (!$insert_handle->execute()) $this->e->triggerException('
                                                                        Error in class (' . __CLASS__ . '): [ ' . 
                                                                        implode(', ', $insert_handle->errorInfo()) . 
                                                                        ' ] on line ' . __LINE__);

                
                $this->_collection[] = $insert_handle_param_admin_id;
                
                $_Notifications = array ();
                $_Notifications[] = $this->getStore()->getNewOrderNotification();
                $_Notifications[] = $this->getStore()->getUnconfirmedDeliveryNotice();
                
                foreach ($_Notifications as $_Notification) 
                {
                    
                        foreach ($_Notification as $Alert) 
                        {
                            $_AdminUser = $Alert->getRecipient();

                            if ($this->hasStaffMember($_AdminUser)) $Alert->enableEmail()->enableDashboard();
                            else $Alert->disableEmail()->disableDashboard();
                        }
                
                }
                

		
		return $this;
		
	}



        
        
        
        
        

	/*
	 *
	 *  [ removeStaffMember ]
	 * ________________________________________________________________
	 * 
	 * 
	 *
	 *
	 */
	 
	public function removeStaffMember (\Cataleya\Admin\User $_AdminUser) 
	{

 		
		if ($this->hasStaffMember($_AdminUser))
		{
			
			
			
			static 
					$delete_handle, 
                                        $delete_handle_param_store_id, 
                                        $delete_handle_param_admin_id;
			
			if (empty($delete_handle))
			{
				$delete_handle = $this->dbh->prepare('
                                                                    DELETE FROM staff_members  
                                                                    WHERE store_id = :store_id 
                                                                    AND admin_id = :admin_id 
                                                                    ');
				$delete_handle->bindParam(':store_id', $delete_handle_param_store_id, \PDO::PARAM_INT);
				$delete_handle->bindParam(':admin_id', $delete_handle_param_admin_id, \PDO::PARAM_INT);
			}
			
			$delete_handle_param_store_id = $this->getStore()->getID();
			$delete_handle_param_admin_id = $_AdminUser->getID();
	
			if (!$delete_handle->execute()) $this->e->triggerException('
											Error in class (' . __CLASS__ . '): [ ' . 
											implode(', ', $delete_handle->errorInfo()) . 
											' ] on line ' . __LINE__);
											
			
                        foreach ($this->_collection as $key=>$val)
                        {
                            if ((int)$val === (int)$delete_handle_param_admin_id) 
                            {
                                unset ($this->_collection[$key]);
                                break;
                            }
                        }
				
		} 
		
		return $this;
		
	}
        
        
        
        
        
	/*
	 *
	 *  [ hasStaffMember ]
	 * ________________________________________________________________
	 * 
	 * 
	 *
	 *
	 */
	 
	public function hasStaffMember (\Cataleya\Admin\User $_AdminUser) 
	{

		return (in_array ($_AdminUser->getID(), $this->_collection));
			
			
        }
        
        


        
    

    
    
    
}


