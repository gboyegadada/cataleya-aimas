<?php


namespace Cataleya\Agent;



use \Cataleya\Helper\DBH;
use \Cataleya\Error;






class Role extends \Cataleya\Data {
	
			
			
	protected $dbh, $e, $bcrypt;
	protected $_data = array();
    protected $_description = NULL;
	protected $_modified = array();
        
    const TABLE = 'agent_roles';
    const PK = 'role_id';
	
	public function __construct($role_id, $_load_with_handle = false) 
	{

        $_load_with_handle = ($_load_with_handle === true);

        if ($_load_with_handle) $role_id = \Cataleya\Helper\Validator::param($role_id, 'match:/^[a-zA-Z0-9_]+$/');
        else $role_id = filter_var($role_id, FILTER_VALIDATE_INT);		

		if ($role_id === false) throw new Error ('Invalid argument: role_id should be an integer or a string.');
		
		// Get encryption class...
		$this->bcrypt = \Cataleya\Helper\Bcrypt::getInstance();
		
		
		// ATTEMPT TO LOAD DATA
		
		// Do prepared statement...
        $agent_select = DBH::getInstance()->prepare('
                                SELECT * FROM agent_roles 
                                WHERE ' . ( $_load_with_handle ? 'handle' : 'role_id' ) . ' = :role_id 
                                LIMIT 1
                                ');
        $agent_select->bindParam(':role_id', $param_role_id, 
                                            ( $_load_with_handle 
                                                ? \PDO::PARAM_STR 
                                                : \PDO::PARAM_INT 
                                            )
                                );
        
        $param_role_id = $role_id;
        
        // Execute...
        if (!$agent_select->execute()) throw new Error ('DB Error: ' . implode(', ', $agent_select->errorInfo()));



		// Check for results...
		if ( ($this->_data = $agent_select->fetch(\PDO::FETCH_ASSOC)) === false ) 
		{
			$this->_data = array();
            return null;

		} else {
            // Load description...
            $this->_description = \Cataleya\Asset\Description::load($this->_data['description_id']);		

        }
        
        
		
			
	}
	
	
	function __destruct () 
	{
            parent::__destruct();
	}
	



    public function current()
    {
        return \Cataleya\Admin\Privilege::load($this->_collection[$this->_position]);

    }


  




	/**
	 * load
	 *
	 * @param int|string $_role_id
	 * @param bool $_load_with_handle
	 * @return void
	 */
	static public function load ($_role_id = null, $_load_with_handle = false) 
	{

		// get role
		$role = new static ($_role_id, $_load_with_handle);

        /*
		if (empty($role->_data))
		{
			unset($role);
			return NULL;
        }
        */        
		
		
		////////////////// RETURN USER ROLE ////////////////////
		
		return $role;
                
     }


        


	/*
	 *
	 *  [ create ]
	 * ________________________________________________________________
	 * 
	 * 
	 *
	 *
	 *
	 */
	 
	static public function create ($handle = '', $description_title = 'Untitled Role', $description_text = 'No Description', $language_code = 'EN') 
	{

        $_expected = [
                'handle' => 'match:/^[A-Z]+(_[A-Z]+){0,10}$/', 
                'title' => 'plain_text', 
                'description' => 'html', 
                'language_code' => 'iso2'
        ];

        $_params = \Cataleya\Helper\Validator::params(
            $_expected, 
            // Supplied params (arguments)...
            [
                'handle' => $handle, 
                'title' => $description_title, 
                'description' => $description_text, 
                'language_code' => $language_code
            ]
        );

        foreach ($_params as $k=>$v) if ($v === false) throw new \Cataleya\Error('Invalid argument: ' . $k . '. Expected: ' . $_expected[$k]);

		

		/*
		 * Handle description Attribute 
		 *
		 */
		 $description = \Cataleya\Asset\Description::create($_params['title'], $_params['description'], $_params['language_code']);
		 
		                
		// construct
		static 
				$role_insert, 
                $role_insert_param_description_id, 
                $role_insert_param_handle;
		
		if (empty($role_insert))
		{
			$role_insert = DBH::getInstance()->prepare('
                                        INSERT INTO agent_roles  
                                        (handle, description_id, date_created, last_modified) 
                                        VALUES (:handle, :description_id, NOW(), NOW())
                                        ');
															
			$role_insert->bindParam(':description_id', $role_insert_param_description_id, \PDO::PARAM_INT);
			$role_insert->bindParam(':handle', $role_insert_param_handle, \PDO::PARAM_STR);
		}
		
		$role_insert_param_description_id = $description->getID();
        $role_insert_param_handle = $_params['handle'];
		
		if (!$role_insert->execute()) throw new Error('DB Error: '.implode(', ', $role_insert->errorInfo())); 
										
		// AUTOLOAD NEW USER AND RETURN IT
		return self::load(DBH::getInstance()->lastInsertId());
                
		
	}





        
        

        




/*
 *
 * [GETTERS] and [SETTERS] 
 * __________________________________________________________________________________
 *
 *
 *
 */	
	

        
        




	// ID

	public function getID () 
	{
		return $this->_data['role_id'];		
		
	}
        
        
         // ID

	public function getRoleId () 
	{
		return $this->_data['role_id'];		
		
	}
	



        

	/*
	 *
	 *  [ getDescription ]
	 * ________________________________________________________________
	 * 
	 * Will return product description as object
	 *
	 *
	 */
	 
	public function getDescriptionObject () 
	{
		
		return $this->_description;
		
	}

        

	 
	public function getLabel ($_language_code = 'EN') 
	{
		
		return $this->_description->getTitle($_language_code);
		
	}


	 
	public function getName ($_language_code = 'EN') 
	{
		
		return $this->_description->getTitle($_language_code);
		
	}
        


	public function getDescription ($_language_code = 'EN') 
	{
		
		return $this->_description->getText($_language_code);
		
	}

	
	/**
     * is
     *
     * ---------------------------------------
     *
     * Use like $_Role->is('MANUFACTURER')
	 *
	 * @param string | \Cataleya\Admin\Role $_role_handle
	 * @return boolean
	 */
	public function is ($_role_handle = NULL) 
	{

        if (
            !is_string($_role_handle) && 
            (!is_object($_role_handle) || $_role_handle instanceof \Cataleya\Agent\Role)
        ) throw new \Cataleya\Error ('Argument must be a handle like "MANUFACTURER" or an instance of \Cataleya\Agent\Role'); 
            
        return is_string($_role_handle) 
            ? $this->is(strtoupper($_role_handle)) 
            : $_role_handle->is($this->getHandle());
		
	}
	




	// getHandle
	public function getHandle () 
	{
            
            return $this->_data['handle'];
		
	}


        



               


	/*
	 *
	 *  [ delete ]
	 * ________________________________________________________________
	 * 
	 * 
	 *
	 *
	 *
	 */	


	public function delete () 
	{
		

		// THEN DELETE ROLE....
        parent::delete();
        
        
        
    }
	
	

        

	
}




