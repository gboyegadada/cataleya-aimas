<?php


namespace Cataleya\Agent;


use \Cataleya\Helper\DBH;
use \Cataleya\Error;




/*
 *
 * 	@Package: Cataleya
 * 	@Class: \Cataleya\Agent\_
 *
 * 	(c) 2012 Fancy Paper Planes
 *
 *
 *
 */

class _ extends \Cataleya\Collection {
   
    // To be used by [\Cataleya\Collection]
    protected $_collection = array ();
    protected $_position = 0;
    
    // CONST VALUES 1 - 15 RESERVED FOR [ _Abstract_Collection ]
    
    const ORDER_BY_NAME = 16;
    const ORDER_BY_ID = 17;


    public function __construct() {
        

        
        parent::__construct();

        
    }
    
    
    
    

	/*
	 *
	 * [ load ]
	 * ________________________________________________________________
	 *
	 *
	 *
	 */
	 
	 
	public static function load ($_role_handle = null, $sort = ['order_by'=>'', 'order'=>'', 'index'=>''], $page_size = 100, $page_num = 1)
	{
            
            
                $instance = new static ();
                
                

                // This is used to determine which column is to be used in the index...
                // $_index_with = 'title';

                if (is_array($sort) && !empty($sort['order_by']) && !empty($sort['order']) && is_int($sort['order_by']) && is_int($sort['order']) )
                {


                    // ASC | DESC
                    switch ($sort['order']) {

                        case self::ORDER_ASC:
                            $param_order = 'ASC';
                            break;

                        case self::ORDER_DESC:
                            $param_order = 'DESC';
                            break;

                        default :
                            $param_order = 'ASC';
                            break;
                    }  


                    // ORDER BY
                    switch ($sort['order_by']) {

                        case self::ORDER_BY_NAME:
                            $param_order_by = "c.company $param_order";
                            $_index_with = 'c.company';
                            break;

                        case self::ORDER_BY_ID:
                            $param_order_by = "a.agent_id $param_order";
                            $_index_with = 'a.agent_id';
                            break;
                        
                        case self::ORDER_BY_CREATED:
                            $param_order_by = "a.date_added $param_order";
                            $_index_with = 'a.date_added';
                            break;
                        
                        case self::ORDER_BY_MODIFIED:
                            $param_order_by = "a.last_modified $param_order";
                            $_index_with = 'a.last_modified';
                            break;

                        default :
                            $param_order_by = "c.company $param_order";
                            $_index_with = 'c.company';
                            break;
                    }





                }

                // Defaults                
                else {

                        $param_order = 'ASC';
                        $param_order_by = "c.company $param_order";
                        $_index_with = 'c.company';

                }    



                // Check if index is specified
                if (is_array($sort) && isset($sort['index']) && is_string($sort['index']))
                {
                    $param_regex = (preg_match('/^[a-zA-Z]{1}$/', $sort['index']) > 0) ? '^[' . strtolower($sort['index']) . '].*' : '.*';
                }

                else {
                    $param_regex = '.*';
                }


                // Check if role is specified
                $_role_handle = \Cataleya\Helper\Validator::param($_role_handle, 'match:/^[A-Z]+(_[A-Z]+){0,10}$/');
                $_role_filter = ($_role_handle !== false) 
                    ? 'b.handle = "' . $_role_handle . '" AND ' 
                    : '';
                

                $param_offset = 0;
                $param_limit = 99999999;
        
        


                // PREPARE SELECT STATEMENT...
                $select_handle = DBH::getInstance()->prepare('
                                                SELECT a.agent_id  
                                                FROM agents a 
                                                INNER JOIN agent_roles b 
                                                ON a.role_id = b.role_id 
                                                INNER JOIN address_book c 
                                                ON a.contact_id = c.contact_id 
                                                WHERE ' . $_role_filter . $_index_with . ' REGEXP :regex 
                                                ORDER BY '. $param_order_by . '   
                                                LIMIT :offset, :limit
                                                ');

                $select_handle->bindParam(':offset', $param_offset, \PDO::PARAM_INT);
                $select_handle->bindParam(':limit', $param_limit, \PDO::PARAM_INT);
                $select_handle->bindParam(':regex', $param_regex, \PDO::PARAM_STR);
                

                if (!$select_handle->execute()) throw new Error ('DB Error: '.implode(', ', $select_handle->errorInfo()));
                while ($row = $select_handle->fetch(\PDO::FETCH_ASSOC))
                {
                    $instance->_collection[] = $row['agent_id'];
                }

                
                $instance->pageSetup($page_size, $page_num);
                
                return $instance;

            
        }
    
    
    
    
        public function current()
        {
            return \Cataleya\Agent::load($this->_collection[$this->_position]);

        }
        
        
        
        
    
    /*
     * 
     * [ search ]
     * 
     */
    
    public static function search ($_term = '', $sort = array('order_by'=>'', 'order'=>'', 'index'=>''), $page_size = 100, $page_num = 1) 
    {
        

                // This is used to determine which column is to be used in the index...
                // $_index_with = 'title';

                if (is_array($sort) && !empty($sort['order_by']) && !empty($sort['order']) && is_int($sort['order_by']) && is_int($sort['order']) )
                {


                    // ASC | DESC
                    switch ($sort['order']) {

                        case self::ORDER_ASC:
                            $param_order = 'ASC';
                            break;

                        case self::ORDER_DESC:
                            $param_order = 'DESC';
                            break;

                        default :
                            $param_order = 'ASC';
                            break;
                    }  


                    // ORDER BY
                    switch ($sort['order_by']) {

                        case self::ORDER_BY_NAME:
                            $param_order_by = "c.company $param_order";
                            $_index_with = 'c.company';
                            break;

                        case self::ORDER_BY_ID:
                            $param_order_by = "a.agent_id $param_order";
                            $_index_with = 'a.agent_id';
                            break;
                        
                        case self::ORDER_BY_CREATED:
                            $param_order_by = "a.date_added $param_order";
                            $_index_with = 'a.date_added';
                            break;
                        
                        case self::ORDER_BY_MODIFIED:
                            $param_order_by = "a.last_modified $param_order";
                            $_index_with = 'a.last_modified';
                            break;

                        default :
                            $param_order_by = "c.company $param_order";
                            $_index_with = 'c.company';
                            break;
                    }




                }

                // Defaults                
                else {

                        $param_order = 'ASC';
                        $param_order_by = "c.company $param_order";
                        $_index_with = 'c.company';

                }    



            // First, take out funny looking characters...
            $_term = preg_replace('/[^-\s\.A-Za-z0-9@*#+]+/', '', $_term);

            // Trim then split terms by [:space:]
            $terms = explode(' ', trim($_term, ' '));



            // SORT SEARCH TERMS
            $regex = array();
            $regex['title'] = array();


            foreach ($terms as $key=>$val) {

                    // Loose any search terms shorter than 3 chars...
                    if (strlen($val) < 2) unset($terms[$key]);
                    
                    // Also make dots safe...
                    else if (preg_match('/[-\s\.A-Za-z0-9]{1,100}/', $val))  $regex['title'][] = preg_replace('/[\.]/', '\.', $val); 
            }

            
            // Build REGEX
            $regex_concat = '';
            foreach ($regex as $k => $v) {
                    if (count($v) == 0) {$regex[$k] = ''; continue; } // no search terms...
                    elseif (count($v) > 3) $v = array_slice($v, 0, 3); // truncate if there are too many search terms...

                    $regex[$k] = "^.*(" . implode('|', $v) . ").*$";
                    $regex_concat .= ($regex_concat != '') ? ' OR ' : '';
                    $regex_concat .= $k . ' REGEXP :regex_' . $k;


            }

            
            
            // Create instance...
            $_objct = __CLASS__;
            $instance = new $_objct;


            // If nothing made it through the filtering...
            // [ _collection ] will be empty a.k.a no results..
            if ($regex_concat == '') return $instance;




            $param_offset = 0;
            $param_limit = 99999999;
        
            
            
            // DO SEARCH...
            $select_handle = \Cataleya\Helper\DBH::getInstance()->prepare('
                                            SELECT a.agent_id 
                                            FROM agents a  
                                            INNER JOIN address_book c  
                                            ON a.contact_id = c.contact_id 
                                            WHERE ' . $regex_concat . '  
                                            ORDER BY '. $param_order_by . '   
                                            LIMIT :offset, :limit
                                            ');

            $select_handle->bindParam(':offset', $param_offset, \PDO::PARAM_INT);
            $select_handle->bindParam(':limit', $param_limit, \PDO::PARAM_INT);
            
            // bind search search params
            foreach ($regex as $k=>$v) $select_handle->bindParam(':regex_'.$k, $regex[$k], \PDO::PARAM_STR);

            if (!$select_handle->execute()) throw new Error ('DB Error: ' . implode(', ', $select_handle->errorInfo()));


            while ($row = $select_handle->fetch(\PDO::FETCH_ASSOC))$instance->_collection[] = $row['agent_id'];

            $instance->pageSetup($page_size, $page_num);

            return $instance;
        
    }
    
    
    


      
    
    
    
}


