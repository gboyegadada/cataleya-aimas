<?php


namespace Cataleya;



/*
* USAGE
*
* $Core = \Cataleya\Core::getInstance();
* $Core->dbh->prepare()
*
*/

class Core
{
    public $config, $dbh; // handle of the db connexion
    private static $instance;

    private function __construct()
    {
		// Init
		$this->config = \Cataleya\System\Config::load('store.core');


		// Databse handle
                $this->dbh = \Cataleya\Helper\DBH::getInstance();
		
		// Bcrypt
                $this->bcrypt = \Cataleya\Helper\Bcrypt::getInstance();
		
    }

    public static function getInstance()
    {
        if (!isset(self::$instance))
        {
            $object = __CLASS__;
            self::$instance = new $object;
        }
        return self::$instance;
    }



}



