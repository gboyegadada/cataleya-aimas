<?php



namespace Cataleya\Sales;



/*
 *
 * 	@Package: Cataleya
 * 	@Class: \Cataleya\Sales\Sale
 *
 * 	(c) 2012 Fancy Paper Planes
 *
 *
 *
 */

class Sale extends \Cataleya\Catalog\ProductCollection 
{
   
        
    protected $dbh, $e;
    
    
        
    protected $_data = array();
    protected $_modified = array();
	
    private 
                $_description, 
                $_url_rewrite, 
                $_banner_image;
    
        protected 
                $_page_size, 
                $_page_num;

 

   public function __construct() {
        parent::__construct();
        
        // Get database handle...
        $this->dbh = \Cataleya\Helper\DBH::getInstance();
		
        // Get error handler
        $this->e = \Cataleya\Helper\ErrorHandler::getInstance();
        

        
    }
    
    
  
    public function __destruct() {
        $this->saveData();
    }

    







      
    
	/*
	 *
	 * [ load ]
	 * ________________________________________________________________
	 *
	 *
	 *
	 */
	 
	 
	public static function load ($id = 0, $filters = array(), $sort = NULL, $page_size = 16, $last_seen = 0, $page_num = 0)
	{
		
		if ($id == 0 || filter_var($id, FILTER_VALIDATE_INT) === FALSE || !is_numeric($page_size) || !is_numeric($page_num)) return NULL;
                
		
                // Create [Sale] instance...
                $_objct = __CLASS__;
                $instance = new $_objct;
                
		
                
		// LOAD: SALE INFO
		static $select_handle1, $param_sale_id;
		
			
		if (!isset($select_handle1)) {
			// PREPARE SELECT STATEMENT...
			$select_handle1 = $instance->dbh->prepare(''
                                . 'SELECT a.*, c.count '
                                . 'FROM sale a '
                                . 'INNER JOIN collections c '
                                . 'ON a.collection_id = c.collection_id '
                                . 'WHERE a.sale_id = :sale_id LIMIT 1'
                                . '');
			$select_handle1->bindParam(':sale_id', $param_sale_id, \PDO::PARAM_INT);
		}
		
		
		$param_sale_id = $id;
		
		if (!$select_handle1->execute()) $instance->e->triggerException('
                                                                            Error in class (' . __CLASS__ . '): [ ' . 
                                                                            implode(', ', $select_handle1->errorInfo()) . 
                                                                            ' ] on line ' . __LINE__);

		$instance->_data = $select_handle1->fetch(\PDO::FETCH_ASSOC);
		if (empty($instance->_data)) return NULL;
		

		// LOAD: DESCRIPTION
		$instance->_description = \Cataleya\Asset\Description::load($instance->_data['description_id']);		


		
		// LOAD: URL_REWRITE
		//$instance->_url_rewrite = \Cataleya\Asset\URLRewrite::load($instance->_data['url_rewrite_id']);
                
                
                
                
                
                /////////////////// LOAD ASSOCIATED PRODUCTS /////////////////////
                //$filters[] = $instance;
                $instance->_loadCollection($filters, $sort, $page_size, $last_seen, $page_num);
		
                // RETURN SALE INSTANCE
		return $instance;	
		
		
	}




	/*
	 *
	 *  [ create ]
	 * ________________________________________________________________
	 * 
	 * 
	 *
	 *
	 *
	 */
	 
	static public function create (\Cataleya\Store $_Store, array $_options = array ('title' => '', 'description' => '', 'language_code' => 'EN', 'discount_type' => \Cataleya\Catalog\Price::TYPE_REDUCTION_PERCENT, 'amount' => 0, 'active' => TRUE)) 
	{
		
		
 		
                // Validate arguments
		if (
                                !isset($_options['discount_type'], $_options['discount_amount'], $_options['is_active']) || 
                        
                                (
				$_options['discount_type'] !== \Cataleya\Catalog\Price::TYPE_REDUCTION && 
				$_options['discount_type'] !== \Cataleya\Catalog\Price::TYPE_REDUCTION_PERCENT 
                                        )
				
				) \Cataleya\Helper\ErrorHandler::getInstance()->triggerException('Error in class (' . __CLASS__ . '): [ Invalid arguments supplied in method: ' . __FUNCTION__ . '  ] on line ' . __LINE__);
            
		
                $_options['title'] = filter_var($_options['title'], FILTER_SANITIZE_STRIPPED);
                $_options['description'] = ''; //filter_var($_options['description'], FILTER_SANITIZE_STRIPPED);
                $_options['language_code'] = \Cataleya\Helper\Validator::iso($_options['language_code'], 2, 2);
                
                if ($_options['title'] === FALSE || $_options['description'] === FALSE || $_options['language_code'] === FALSE) \Cataleya\Helper\ErrorHandler::getInstance()->triggerException('Error in class (' . __CLASS__ . '): [ Bad param in method: ' . __FUNCTION__ . '  ] on line ' . __LINE__);
                    
                
                
            
                // //// BEGIN TRANSACTION /////
                \Cataleya\Helper\DBH::getInstance()->beginTransaction();
                
                $_params = array (
                    'store_id'  =>  $_Store->getStoreId(), 
                    'description_id'  =>  0, 
                    'collection_id' => self::_initCollection(), 
                    'amount'    =>  \Cataleya\Helper\Validator::float($_options['discount_amount']), 
                    'discount_type' =>  $_options['discount_type'], 
//                    'sale_begin'    =>  date('Y-m-d'), // \Cataleya\Helper\Validator::date($_options['start_date']), 
//                    'sale_end'    =>  date('Y-m-d'), // \Cataleya\Helper\Validator::date($_options['end_date']), 
//                    'indefinite'    =>  TRUE, // \Cataleya\Helper\Validator::bool($_options['valid_until_never']), 
                    'is_active' =>  \Cataleya\Helper\Validator::bool($_options['is_active']), 
                    'sort_order' => (isset($_options['sort_order']) && is_numeric($_options['sort_order'])) ? (int)$_options['sort_order'] : 0
                );
                

                if ($_params['discount_type'] === \Cataleya\Catalog\Price::TYPE_REDUCTION_PERCENT && $_params['amount'] > 100) $_params['amount'] = FALSE;
                
                
                foreach ($_params as $_k=>$_param) if (($_param === FALSE && $_k !== 'is_active') || ($_k === 'is_active' && $_param === NULL)) \Cataleya\Helper\ErrorHandler::getInstance()->triggerException('Error in class (' . __CLASS__ . '): [ Bad param (' . $_k . ') in method: ' . __FUNCTION__ . '  ] on line ' . __LINE__);
                    
            				
            
            
				
		/*
		 * Handle description Attribute 
		 *
		 */
		 
		 $_description = \Cataleya\Asset\Description::create($_options['title'], $_options['description'], $_options['language_code']);
		 $_params['description_id'] = $_description->getID();
                 

		 
		/*
		 * Handle url_rewrite Attribute 
		 *


		 $_url_rewrite = \Cataleya\Asset\URLRewrite::create($_options['title'], 'sale.php', 0);
		 
		 */
                 
                 
		// Get database handle...
                $dbh = \Cataleya\Helper\DBH::getInstance();
		
		// Get error handler
		$e = \Cataleya\Helper\ErrorHandler::getInstance();
		
		// construct
		static 
				$insert_handle;
		
		if (empty($insert_handle))
		{
			$insert_handle = $dbh->prepare('
                                                    INSERT INTO sale 
                                                    (store_id, description_id, collection_id, discount_type, amount, is_active, sort_order, indefinite, date_created, last_modified) 
                                                    VALUES (:store_id, :description_id, :collection_id, :discount_type, :amount, :is_active, :sort_order, 1, NOW(), NOW())
                                                    ');

		}
		

		foreach ($_params as $key=>$param) $insert_handle->bindParam(':'.$key, $_params[$key], \Cataleya\Helper\DBH::getTypeConst($param));
                
		
		if (!$insert_handle->execute()) $e->triggerException('
                                                                    Error in class (' . __CLASS__ . '): [ ' . 
                                                                    implode(', ', $insert_handle->errorInfo()) . 
                                                                    ' ] on line ' . __LINE__);

		
		// AUTOLOAD NEW PRODUCT AND RETURN IT
		$instance = self::load($dbh->lastInsertId());
		// $_url_rewrite->setTargetPath('sales.php?id=' . $instance->_data['sale_id']);
                
                // //// COMMIT TRANSACTION /////
                \Cataleya\Helper\DBH::getInstance()->commit();
		
		
		return $instance;
		
		
	}

        
        






	
	
	public function saveData () 
	{
		

		if (empty($this->_modified)) return;
                
                $this->_modified = array_unique($this->_modified);
                
		$_update_params = array();
		$key_val_pairs = array();
		
		foreach ($this->_modified as $key) $key_val_pairs[$key] = $key . ' = :' . $key;
		
	
		// Do prepared statement for 'INSERT'...
		$update_handle = $this->dbh->prepare('
												UPDATE sale 
												SET ' . implode (', ', $key_val_pairs) . ', last_modified=now()       
												WHERE sale_id = :sale_id 
												');
		$update_handle->bindParam(':sale_id', $_update_params['sale_id'], \PDO::PARAM_INT);
		$_update_params['sale_id'] = $this->_data['sale_id'];
                
		foreach ($this->_modified as $key) {
                    
                        $_update_params[$key] = $this->_data[$key];                        
			$update_handle->bindParam(':'.$key, $_update_params[$key], \Cataleya\Helper\DBH::getTypeConst($_update_params[$key]));

		}
		
		if (!$update_handle->execute()) $this->e->triggerException('Error in class (' . __CLASS__ . '): [ ' . implode(', ', $update_handle->errorInfo()) . ' ] on line ' . __LINE__);
		
	}
        
        
        
        
	

	 
	 /*
	 *  [ delete ]
	 * ________________________________________________________________
	 * 
	 * 
	 *
	 *
	 */

	 
	public function delete () 
	{
		
            
            
            // //// BEGIN TRANSACTION /////
            \Cataleya\Helper\DBH::getInstance()->beginTransaction();

            


                \Cataleya\Helper\DBH::sanitize(['sale'], 'sale_id', $this->getID());
                \Cataleya\Helper\DBH::sanitize(array('collections'), 'collection_id', $this->getCollectionID());
                \Cataleya\Helper\DBH::sanitize(array('content'), 'content_id', $this->_data['description_id']);
                        
                
                
                // //// COMMIT TRANSACTION /////
                \Cataleya\Helper\DBH::getInstance()->commit();
                
		return TRUE;
		
		
	}





        
        
        
	/*
	 *
	 *  [ setBannerImage ]
	 * ________________________________________________________________
	 * 
	 * 
	 *
	 *
	 *

	 

	public function setBannerImage (\Cataleya\Asset\Image $image)
	{
		$this->_data['image_id'] = $image->getID();
		$this->_banner_image = $image;
		
		$this->_modified[] = 'image_id';
		
	}

	 */



	/*
	 *
	 *  [ usetBannerImage ]
	 * ________________________________________________________________
	 * 
	 * 
	 *
	 *
	 *

	 

	public function usetBannerImage ()
	{
		$this->_data['image_id'] = NULL;
		$this->_banner_image = NULL;
		
		$this->_modified[] = 'image_id';
		
	}

	 */
        
        


	/*
	 *
	 *  [ getBannerImage ]
	 * ________________________________________________________________
	 * 
	 * Returns image object
	 *
	 *
	 *

	 

	public function getBannerImage ()
	{
		if (empty($this->_banner_image) )
		{
			$this->_banner_image = \Cataleya\Asset\Image::load($this->_data['image_id']);
		}
		
		return $this->_banner_image;
		
	}

	 */

        
      /*
       * 
       * Misc [getters]
       * 
       */
        

        
       
        
        
        /*
         *
         * [ getSize ] 
         *_____________________________________________________
         *
         *
         */

        public function getSize()
        {
                return count($this->_collection);
        }                
        
        
        
        
        /*
         *
         * [ getSaleId ] 
         *_____________________________________________________
         *
         *
         */

        public function getSaleId()
        {
                return $this->_data['sale_id'];
        } 
        
        
        
        
        /*
         *
         * [ getID ] 
         *_____________________________________________________
         *
         *
         */

        public function getID()
        {
                return $this->_data['sale_id'];
        }  

        
        
         /*
         *
         * [ getDescription ] 
         *_____________________________________________________
         *
         * Will return Sale description attribute as object
         *
         */

        public function getDescription()
        {
                return $this->_description;
        } 

 
 
 	/*
	 *
	 *  [ getURLRewrite ]
	 * ________________________________________________________________
	 * 
	 * Will return Sale url_rewrite attribute as object
	 *
	 *

	 
	public function getURLRewrite () 
	{
		
		return $this->_url_rewrite;
		
	}
         
	 */

        
        
        
        
        
       
        
        /*
        *
        * [ getStartDate ]
        * ______________________________________________________ 
        *
        */

       public function getStartDate()
       {
               return $this->_data['sale_begin'];
       } 
       
       
       
        /*
        *
        * [ getEndDate ]
        * ______________________________________________________ 
        *
        */

       public function getEndDate()
       {
               return $this->_data['sale_end'];
       } 
       
   
  
        

        
        
        
        
        
        /*
         *
         * [ getSortOrder ] 
         *_____________________________________________________
         *
         *
         */

        public function getSortOrder()
        {
                return $this->_data['sort_order'];
        }

 
        
        /*
         *
         * [ getDateCreated ] 
         * ______________________________________________
         *
         */

        public function getDateCreated()
        {
                return $this->_data['date_created'];
        }
        
        
        
        
        /*
         *
         * [ getLastModified ]
         * ______________________________________________ 
         *
         */
        
        public function getLastModified()
        {

                return $this->_data['last_modified'];
        }        

        
        
        
        
        
  
        /*
         *
         * [ isExpired ] 
         *_____________________________________________________
         *
         *
         */

        public function isOver()
        {
                return (strtotime($this->_data['sale_end']) < time() && !$this->isIndefinite()) ? TRUE : FALSE;
        }
        
        
        
      

        /*
         *
         * [ isIndefinite ] 
         *_____________________________________________________
         *
         *
         */

        public function isIndefinite()
        {
                return ((int)$this->_data['indefinite'] === 1) ? TRUE : FALSE;
        }
       
       
       
        
        
        
        
        
        

        /*
         *
         * [ isOn ] 
         *_____________________________________________________
         *
         *
         */

        public function isOn()
        {
                return (!$this->isOver() && $this->isActive()) ? TRUE : FALSE;
        }
        
        
        
        /*
         *
         * [ isActive ] 
         *_____________________________________________________
         *
         *
         */

        public function isActive()
        {
                return ((int)$this->_data['is_active'] === 1) ? TRUE : FALSE;
        }



        
            

        /*
         * 
         * [ enable ]
         * ______________________________________________________
         * 
         * 
         */


        public function enable () 
        {

            if ((int)$this->_data['is_active'] === 0) 
            {
                $this->_data['is_active'] = 1;
                $this->_modified[] = 'is_active';
            }

            return $this;
        }





        /*
         * 
         * [ disable ]
         * ______________________________________________________
         * 
         * 
         */


        public function disable () 
        {
            if ((int)$this->_data['is_active'] === 1) 
            {
                $this->_data['is_active'] = 0;
                $this->_modified[] = 'is_active';
            }

            return $this;
        }
        
        
        
        
        
        /*
         *
         * [ getStoreId ] 
         *_____________________________________________________
         *
         *
         */

        public function getStoreId()
        {
                return $this->_data['store_id'];
        }
        
        
        
        /*
         *
         * [ getStore ] 
         *_____________________________________________________
         *
         *
         */

        public function getStore()
        {
                return \Cataleya\Store::load($this->_data['store_id']);
        }

        
        
        
        
        
        
        
        
        /*
         * 
         * [ startSale ]
         * ______________________________________________________
         * 
         * 
         */


        public function startSale () 
        {

            if ((int)$this->_data['is_active'] === 0) 
            {
                $this->_data['is_active'] = 1;
                $this->_modified[] = 'is_active';
            }

            return $this;
        }

        
        
        
        
        
        /*
         * 
         * [ endSale ]
         * ______________________________________________________
         * 
         * 
         */


        public function endSale () 
        {
            if ((int)$this->_data['is_active'] === 1) 
            {
                $this->_data['is_active'] = 0;
                $this->_modified[] = 'is_active';
            }

            return $this;
        }
        
      
        
        

        

        
        
        
        /*
         * 
         *  [ getDiscountFor ]
         * 
         * 
         */
        
        public function getDiscountFor (\Cataleya\Catalog\Product $_product) 
        {
            
        }












        /*
       * 
       * Misc [ SETTERS ]
       * 
       */
        
         
        /*
        *
        * [ setSortOrder ]
        * ______________________________________________________ 
        *
        */

       public function setSortOrder($value)
       {

               $this->_data['sort_order'] = $value;
               $this->_modified[] = 'sort_order';

               return $this;
       }       

       
       
       
        /*
        *
        * [ getDiscountType ]
        * ______________________________________________________ 
        *
        */

       public function getDiscountType()
       {

               return $this->_data['discount_type'];

       }     
       
       
       
       
        /*
        *
        * [ setDiscountType ]
        * ______________________________________________________ 
        *
        */

       public function setDiscountType($value = NULL)
       {

            // Validate discount type...
            if (
                            empty($value) || 

                            (
                            $value !== \Cataleya\Catalog\Price::TYPE_REDUCTION && 
                            $value !== \Cataleya\Catalog\Price::TYPE_REDUCTION_PERCENT 
                                    )

                            ) $this->e->triggerException('Error in class (' . __CLASS__ . '): [ Argument supplied in method: ' . __FUNCTION__ . ' must be an instance of appropriate discount type  ] on line ' . __LINE__);
            

           
               $this->_data['discount_type'] = $value;
               $this->_modified[] = 'discount_type';
               return $this;

       }    
       
       
       
       
        /*
        *
        * [ setDiscountAmount ]
        * ______________________________________________________ 
        *
        */

       public function setDiscountAmount($value = NULL)
       {


            // Validate discount type...
           $value = filter_var($value, FILTER_VALIDATE_FLOAT);
           
           if ($value === FALSE) $this->e->triggerException('Error in class (' . __CLASS__ . '): [ Argument supplied in method: ' . __FUNCTION__ . ' must be a float or integer  ] on line ' . __LINE__);
           
           if ($this->getDiscountType() === \Cataleya\Catalog\Price::TYPE_REDUCTION_PERCENT && $value > 100) $this->e->triggerException('Error in class (' . __CLASS__ . '): [ Argument supplied in method: ' . __FUNCTION__ . ' must be a float or integer and less than 100  ] on line ' . __LINE__);
           
           
            $this->_data['amount'] = $value;
            $this->_modified[] = 'amount';
            return $this;

       }    

       
       
        /*
        *
        * [ getDiscountAmount ]
        * ______________________________________________________ 
        *
        */

       public function getDiscountAmount()
       {

               return round($this->_data['amount'], 2);

       }   
       
       
       
       
        /*
         *
         * [ computeDiscountPrice ] 
         *_____________________________________________________
         *
         * Returns price WITH coupon discounts applied (if any)
         *
         */

        public function computeDiscountPrice($_cost = NULL)
        {
            
            // Validate cost argument...
           $_cost = filter_var($_cost, FILTER_VALIDATE_FLOAT);
           
           if ($_cost === FALSE) $this->e->triggerException('Error in class (' . __CLASS__ . '): [ Argument supplied in method: ' . __FUNCTION__ . ' must be a float or integer  ] on line ' . __LINE__);
           
                       
            // Check if SALE is active
            if (!$this->isOn()) return $_cost;
            
            
            // Calculate discount

            // Percentage reduction
            if ($this->getDiscountType() === \Cataleya\Catalog\Price::TYPE_REDUCTION_PERCENT && $_cost > 0) 
            {
                $_cost = $_cost - (($this->getDiscountAmount()/100) * $_cost);
            }

            // or normal price reduction
            else if ($this->getDiscountType() === \Cataleya\Catalog\Price::TYPE_REDUCTION && $_cost > $this->getDiscountAmount()) 
            {
                $_cost = $_cost - $this->getDiscountAmount();
            }

            return round($_cost, 2);
            
        }
       


        
}


