<?php



namespace Cataleya\Sales;



/*
 *
 * 	@Package: Cataleya
 * 	@Class: \Cataleya\Sales\QuantityDiscount
 *
 * 	(c) 2012 Fancy Paper Planes
 *
 *
 *
 */

class QuantityDiscount implements \Cataleya\Asset {
    
	
	private $_data = array();
	private $_modified = array();
        
	private $dbh, $e;
	

	


	/*
	 *
	 * [ __construct ]
	 * ________________________________________________________________
	 *
	 *
	 *
	 */
	 
	 
	private function __construct ()
	{
		
		// Get database handle...
                $this->dbh = \Cataleya\Helper\DBH::getInstance();
		
		// Get error handler
		$this->e = \Cataleya\Helper\ErrorHandler::getInstance();
		
	}
	


	/*
	 *
	 * [ load ]
	 * ________________________________________________________________
	 * 
	 * Load instance of object from existing data (database)
	 *
	 *
	 *
	 */
	 
	static public function load ($_id = NULL) 
        {
            
            
        }





	/*
	 *
	 * [ create ]
	 * ________________________________________________________________
	 * 
	 * Create new attribute
	 *
	 *
	 */
	 
	static public function create () 
        {
            
            
        }


        
        
        
        
        
        
        
        
        /*
         *
         * [ getID ] 
         *_____________________________________________________
         *
         *
         */

        public function getID()
        {
                return $this->_data['qty_discount_id'];
        }



        
        
	/*
	 *
	 *  [ Attribute ] interface method: [ getClassName ]
	 * ________________________________________________________________
	 * 
	 * 
	 *
	 *
	 *
	 */
	 
	public function getClassName () 
	{
		
		
		return __CLASS__;
			
		
		
	}

        
        
        
        
}


