<?php


namespace Cataleya\System\Event;





/**
 * Observable
 *
 * @package Cataleya
 * @author Gboyega Dada <gboyega@fpplabs.com>
 * @version $Id$
 */
interface Observable 
{
	
	/**
     * getEvents    Return all observable events in an array like this:
     *              [ 'product/deleted', 'product/created', 'product/updated' ]
	 *
	 * @return array
	 */
	public static function getEvents();
	
	
}
	
	
	
