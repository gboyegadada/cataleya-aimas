<?php


namespace Cataleya\System\Event;



/*

INTERFACE LISTENER

*/



interface Listener 
{
	
	/*
		method: onChanged 
				
				Called by an 'Observable' object if the 'Listener' object is registered 
				with it. Sort of like signing up for the 'Observable' objects's newsletter.
		
		$listener_name: A sort of 'To' label like in emails. 'Listener' Object will only act 
						if it matches it's own 'address' or 'name'.
						
		$data: 'Body' of message (like email). 
	
	
	*/
	
        
        /**
         * 
         * @return string
         * 
         */
        public function getScope();
        
        /**
         * 
         * @param string $_event
         * @param array $_data
         * 
         */
        public function notify($_event, array $_data = array());
	
	
	
}
	
	
	
