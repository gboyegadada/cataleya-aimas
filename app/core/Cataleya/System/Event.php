<?php


namespace Cataleya\System;
use \Cataleya\Error;
use Cataleya\Helper\DBH;



/**
 *
 * 	@Package Cataleya
 * 	@Class \Cataleya\System\Event
 *
 * 	(c) 2015 Fancy Paper Planes
 *
 *
 *
 */

class Event
{
    
    


    protected static $_listeners = [];
    protected static $_registry = [];
    protected static $_is_ready = false;
    


    protected static $_events = array (
        
        // Products...
        'product/*', 
        'product/created', 
        'product/updated', 
        'product/deleted', 
        
        // Orders
        'order/*', 
        'order/created', 
        'order/updated', 
        'order/deleted', 
        'order/archived', 
        'order/closed', 
        'order/cancelled', 
        
        // Stores
        'store/*', 
        'store/created', 
        'store/update', 
        'store/opened', 
        'store/closed', 
        
        // Payment Types
        'payment-type/*', 
        'payment-type/created', 
        'payment-type/updated', 
        'payment-type/deleted', 
        
        // Transaction
        'transaction/*', 
        'transaction/started', 
        'transaction/completed', 
        'transaction/cancelled', 
        'transaction/failed', 
        'transaction/error', 
        'transaction/deleted', 
        'transaction/status/updated', 
        
        // APP
        'app/*', 
        'app/initialized',
        'app/on',
        'app/off',
        'app/error', 
        'app/init/notificationsRegistered',
        'app/init/hooksRegistered', 
        
        // HTTP REQUEST
        'request/headersSent', 
        'request/requestTokenFailed', 
        'request/fingerPrintFailed', 
        'request/missingFingerPrint', 
        
        // Admin User
        'adminUser/created', 
        'adminUser/updated', 
        'adminUser/deleted', 
        'adminUser/enabled', 
        'adminUser/disabled', 
        'adminUser/failedLogin', 
        
        // Customer
        'customer/created', 
        'customer/updated', 
        'customer/deleted', 
        'customer/enabled', 
        'customer/disabled', 
        'customer/failedLogin', 
        
        // Cart 
        'cart/itemAdded', 
        'cart/itemRemoved', 
        
        // Wishlist 
        'wishlist/itemAdded', 
        'wishlist/itemRemoved', 
        
        // Emailer
        'emailer/error', 
        
        // Session
        'session/error'
        
    );
    
    

    public function __construct() {
       
        
    }
    
    


    

    /**
     * initRegistry
     *
     * @param bool $_reload
     * @return void
     */
    private static function initRegistry ($_reload=false) {
        
        if (!empty(self::$_registry) && $_reload !== true) return;

        static $_select_handle;
        self::$_registry = [];

        if (empty($_select_handle)) {
            $_select_handle = DBH::getInstance()->prepare(
                'SELECT * FROM event_registry '
            );
        }

        if (!$_select_handle->execute()) {
            return;
            // throw new \Exception(implode(', ', $_select_handle->errorInfo())); // \Cataleya\Error(implode(', ', $_select_handle->errorInfo()), $_select_handle->errorCode()); 
        }


        while ($row = $_select_handle->fetch(\PDO::FETCH_ASSOC)) {
            self::$_registry[$row['event']] = $row['owner'];
        }
        
        self::$_is_ready = true;
        
       

    }



    
    
    /**
     * register
     *
     * @param string $_listener App Controller class name
     * @param string|array $_event  An event (ex: 'product/delete') or an array of events.
     * @return void
     */
    public static function register($_event, $_owner_class_name) 
    {
        if (!is_array($_event)) $_event = [ $_event ];

        foreach ($_event as $_e) 
        {
            if (!is_string($_e))  throw new Error ('Invalid argument (2).');
            else if (!self::isEvent($_e)) throw new Error ('"' . (string)$_e . '" is not a valid event.');
        }

        self::initRegistry();
            

        static $_insert_handle;

        // 5. Register hooks.
        if (empty($_insert_handle)) $_insert_handle = DBH::getInstance()->prepare( 
            'INSERT INTO event_registry ' . 
            '(event, owner, date_added) ' . 
            'VALUES (:event, :owner, NOW()) '
                                        );
        foreach ($_event as $_e) 
        {

            if (isset(self::$_registry[$_e])) 
            {
                if ($_owner_class_name !== self::$_registry[$_e]) {
                    throw new Error ('Event already registered to another observable! You cannot register the same event to more than 1 owner/observable.');
                } else continue;
            }

            /**
             *
             * 1. event:        System event that your plugin app wants to listen to.
             * 2. observable:   The class name of the Observable (ex: "product/create" => "\Cataleya\Catalog\Product")
             *
             */
            $_params = [
                'event' => $_e, 
                'owner' => $_owner_class_name
            ];

            DBH::bindParamsArray($_insert_handle, $_params);
                    
                    
            if (!$_insert_handle->execute()) {
                throw new Error('DB Error: '.implode(', ', $_insert_handle->errorInfo()), $_insert_handle->errorCode()); 
            }

            self::$_registry[$_e] = $_owner_class_name;

        }


    }


    /**
     * add  Alias for ::register()
     *
     * @param string $_listener App Controller class name
     * @param string|array $_event  An event (ex: 'product/delete') or an array of events.
     * @return void
     */
    public static function add($_event, $_owner_class_name) 
    {

        return self::register($_event, $_owner_class_name);

    }
    

        
    /**
     * 
     * @param $_scope string | array
     * 
     */
    public static function remove ($_scope = []) 
    {
        
        $_scopes = (is_array($_scope)) ? $_scope : [ $_scope ];
        self::initRegistry();


        static $_delete_handle, $_param_event;

        // Remove hooks.
        if (empty($_delete_handle)) 
        {
            $_delete_handle = DBH::getInstance()->prepare( 
                            'DELETE FROM events_registry ' . 
                            'WHERE event = :event '
                        );
            $_delete_handle->bindParam(':event', $_param_event, \PDO::PARAM_STR);
        }
        foreach ($_scopes as $_sc) 
        {
            foreach (self::getEvents($_sc) as $_e=>$_owner) 
            {

                $_param_event = $_e; 
                        
                        
                if (!$_delete_handle->execute()) {
                    throw new Error('DB Error: ' . implode(', ', $_insert_handle->errorInfo()), $_insert_handle->errorCode()); 
                }

                
                unset (self::$_registry[$_e]);
                
            }
        }
        
        return true;
        
    }
    

    
    /**
     * exists
     *
     * @param string $_event
     * @return boolean
     */
    public static function exists($_scope) 
    {
        if (!self::isEvent($_scope)) throw new \Exception ('Invalid argument.'.$_scope);

        // if (preg_match('!([a-zA-Z\-_]+)(\/[a-zA-Z\-_]+)+$!', $_str) < 1) return false;


        self::initRegistry();

        if ($_scope === '*') return true;


        foreach (self::$_registry as $_event=>$_owner) 
        {
            if (self::inScope($_event, $_scope)) { return true; break; }
        }
        
        return false;

    }
    

    public static function getEvents($_scope = '*') 
    {

        if (!self::isEvent($_scope)) throw new Error ("Invalid event scope");

        self::initRegistry();

        if ($_scope === '*') return self::$_registry;


        $_list = [];

        foreach (self::$_registry as $_event=>$_owner) 
        {
            if (self::inScope($_event, $_scope)) $_list[$_event] = $_owner;
        }

        return $_list;

    }



    /**
     * hasListeners
     *
     * @param string $_event
     * @return boolean
     */
    public static function hasListeners ($_event) 
    {
        return (self::isEvent($_event) && array_key_exists($_event, self::$_listeners));
    }






    /**
     * inScope  Returns true if an event (ex: 'catalog/product/delete') is part 
     *          of a scope (ex: 'catalog/*')
     *
     * @param string $_event
     * @param string $_scope
     * @return boolean
     */
    public static function inScope($_event, $_scope = '*') 
    {

        if (
            !self::isEvent($_event) || 
            preg_match('!^(?<sc>([a-zA-Z\-_]+)(\/[a-zA-Z\-_]+){0,20})(\/\*)?$!', $_scope, $m) < 1
        ) throw new Error ('Invalid scope.');

        return (strpos($_event, $m['sc'].'/') === 0 || $_event === $m['sc']) ? true : false;


    }





    /**
     * 
     * @param string | \Cataleya\System\Event\Listener $_Listener
     * @param $_event string | array
     * 
     */
    public static function addListener ($_listener, $_scope = []) 
    {
        
        try {
            
            list ($_listener_entry, $_scopes) = self::digestListener($_listener, $_scope);
            
        } catch (\Cataleya\Error $e) {
            
            throw new \Cataleya\Error ('Error adding listener!', 0, $e);
            
        }

        foreach ($_scopes as $_sc) 
        {
            foreach (self::getEvents($_sc) as $_e=>$_o) 
            {
                if (!isset(self::$_listeners[$_e])) self::$_listeners[$_e] = [];

                self::$_listeners[$_e][] = $_listener_entry;
            }
        }
        return true;
        
    }
    
    
    
    
    

    /**
     * 
     * @param string | \Cataleya\System\Event\Listener $_listener
     * @param $_event string | array
     * 
     */
    public static function removeListener ($_listener, $_scope = []) 
    {
        try {
            
            list ($_listener, $scopes) = self::digestListener($_listener, $_scope);
            
        } catch (\Cataleya\Error $e) {
            
            throw new \Cataleya\Error ('Error removing listener!', 0, $e);
            
        }
        
        foreach (self::$_listeners as $_e=>$_ls) 
        {
            foreach ($_scopes as $_sc) 
            {
                if (
                    !self::isEvent($_sc) || 
                    !self::inScope($_e, $_sc) 
                ) continue;
                
                $k = array_search($_listener, $_ls, true);
                
                if ($k !== false) array_slice (self::$_listeners[$_e], $k, 1);
                
            }
        }
        
        return true;
        
    }
    
    
    
    /**
     * 
     * @param type $_listener
     * @param type $_event
     * @return type
     * @throws \Cataleya\Error
     */
    private static function digestListener($_listener, $_event) 
    {
        
        if (!is_string($_event) && !is_array($_event)) throw new \Cataleya\Error ('Invalid argument (2): Must be a string or array of strings.');
        $_listener_entry = null;
        
        switch (gettype($_listener)) 
        {
            case "object":
                if (!self::isListener($_listener)) throw new \Cataleya\Error ('Invalid argument (1): Must be object|class|func AND/OR must implement "\Cataleya\System\Event\Listener". Gorrit?');
                
                if (empty($_event)) $_event = $_listener->getScope();
                $_listener_entry = [
                    'type' => 'o', // object
                    'listener' => $_listener 
                ];
                break;
                
            case "string":
                if (!self::isListener($_listener)) throw new \Cataleya\Error ('Invalid argument (1): Must be object|class|func AND/OR must implement "\Cataleya\System\Event\Listener". Gorrit?');                
                $_listener_entry = [
                    'type' => ((class_exists($_listener)) ? "c" : "f"), // c - class | f - func
                    'listener' => $_listener 
                ];
                
                if (empty($_event) && $_listener_entry['type'] === "c") $_event = \forward_static_call([ $_listener, 'getScope' ]);
                else if (empty($_event)) throw new \Cataleya\Error ('Invalid argument (2): No scope!');
                break;
                
            default:
                throw new \Cataleya\Error ('Invalid argument (1): Must be object|class|func AND/OR must implement "\Cataleya\System\Event\Listener". Gorrit?');
                
                break;
        }
        
        if (!is_array($_event)) $_event = [ $_event ];
        
        return [ $_listener_entry, $_event ];
        
    }

    
    
    /**
     * 
     * @return array
     * 
     */
    public static function getScopes() {
        throw new Error ("Oops: [ Sys/Event::getScopes ] isn't implemented yet and I don't know if I ever will.");
    }
    
    
    
    
    /**
     * 
     * @param string $_str
     * @return boolean
     * 
     */
    public static function isEvent($_str) {

        return ( 
            !is_string($_str) || 
            empty($_str) || 
            ($_str !== '*' && preg_match('!^([a-zA-Z\-_]+)(\/[a-zA-Z\-_]+){0,20}(\/\*)?$!', $_str) == false)  
        ) ? false : true;

    }
    
    
    public static function isListener ($_listener) 
    {
        $_class_name = (is_object($_listener)) 
                ? get_class($_listener) 
                : $_listener;
        
        if ((is_string($_listener) && function_exists($_listener))) return true;
        
        
        
        if (
                !is_string($_class_name) || 
                !class_exists($_class_name)
                ) throw new \Cataleya\Error ('Invalid argument: Must be a class or object instance!');
        
        $rc = new \ReflectionClass($_class_name);
        return $rc->implementsInterface("\Cataleya\System\Event\Listener");
    }

    
    /**
     * @param string $_event    IMPORTANT: This is NOT a scope!! It's a single, finite event like
     *                          "product/deleted". "product/*" not allowed!
     * 
     * @param \Cataleya\System\Event\Observable $_Observable
     * @param array $_data
     * @return boolean
     * 
     */
    public static function notify($_event, \Cataleya\System\Event\Observable $_Observable=NULL, array $_params = array ()) 
    {
        
        if (
            !self::exists($_event) || 
            !self::hasListeners($_event)
        ) return false; 
            
        
        $_data = [
            'this' => $_Observable, 
            'event' => $_event, 
            'params' => $_params
        ];


        foreach (self::$_listeners[$_event] as $ls) { self::notifyListener($ls, $_event, $_data);  }

        // Notify Apps that RSVPd...
        \Cataleya\Plugins\Hook::notify($_event, $_data);

        
        return true;
       
    }
    
    
    
    /**
     * 
     * @param mixed $_listener
     * @param string $_event
     * @param array $_data
     * @throws \Cataleya\Error
     */
    private static function notifyListener ($_listener, $_event, $_data) 
    {
        switch ($_listener['type']) 
        {
            case 'c':
                \forward_static_call([ $_listener['listener'], 'notify' ], $_event, $_data);
                break;
            case 'f': 
                \forward_static_call($_listener['listener'], $_event, $_data);
                break;
            case 'o':
                $_listener['listener']->notify($_event, $_data);
                break;
            default:
                throw new \Cataleya\Error ('Unable to determine listener type!');
                break;
        }
        
    
    }
    
    

        

 
}


