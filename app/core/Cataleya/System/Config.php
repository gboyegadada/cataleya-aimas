<?php


namespace Cataleya\System;





class Config extends \Cataleya\Collection  implements \Cataleya\System\Event\Observable  
{	

	protected $Core;
	
	private $_listeners = array();
	private $_data = array();
    protected $_collection = array ();
    protected $_position = 0;
	private $_modified = array();
	private $_config_id;
	private $e;
	private $dbh;
	
	
    
        protected static $_events = [
            
            
        ];
	
	


	
	
	//////////// ERROR_INFO /// RETURNS ERRORS AS ARRAY //////////
	
	public function errorInfo ()
	{
		
		return $this->_errors;
		
	}



	
	// __DESTRUCT ////////////
	
	public function __destruct () 
	{
		
		$this->save();
		
	}
		
		

    public function getID () 
    {
        return $this->_config_id;
    }
	


	//////////// __CONSTRUCT //////////
	
	public function __construct ($config_id, $new=FALSE) 
	{
		
		$config_id = filter_var($config_id, FILTER_SANITIZE_STRING);
		
		if ($config_id === FALSE || $config_id === NULL) $this->e->triggerException('Error in class (' . __CLASS__ . '): [ Invalid ID ] on line ' . __LINE__);
		
		

		// Set Config_id
		$this->_config_id = $config_id;
		
		// Get database handle...
        $this->dbh = \Cataleya\Helper\DBH::getInstance();
		
		// Add logger
		// \Cataleya\System\Event::addListener(\Cataleya\Helper\Logger::create('config/change'));
		
		// Get error handler
		$this->e = \Cataleya\Helper\ErrorHandler::getInstance();
		
		// Make sure __destruct method is called when script exits.
		// register_shutdown_function(array($this, '__destruct'));
		


        if (!$new) {
                //////  LOAD EXISTING CONFIG USING SPECIFIED 'CONFIG_ID' /////////

            // Attempt to load config data...
            static $config_select, $param_config_id;


            // Do prepared statement...
            if (empty($config_select)) {
            $config_select = $this->dbh->prepare('
                                                    SELECT name, value FROM config  
                                                    WHERE config_id = :config_id 
                                                    ORDER BY name 
                                                    ');
            $config_select->bindParam(':config_id', $param_config_id, \PDO::PARAM_STR);
            }

            $param_config_id = $this->_config_id;

            // Execute...
            if (!$config_select->execute())  $this->e->triggerException ('Error in class (' . __CLASS__ . '): [ ' . implode(', ', $config_select->errorInfo()) . ' ] on line ' . __LINE__);



            // Take table rows and put content in $_data...
            while ($row = $config_select->fetch(\PDO::FETCH_ASSOC))
            {
                    
                    $_name = base64_decode($row['name']);
                    $_value = \Cataleya\Helper\Bcrypt::decrypt($row['value']);

                    if (!isset($this->_data[$_name])) {
                                $this->_data[$_name] = $_value;

                                } else if (is_array($this->_data[$_name])) {
                                        $this->_data[$_name][] = $_value;
                                } else {
                                        $this->_data[$_name] = array($this->_data[$_name], $_value);
                                }

                        }

                        if ( empty($this->_data) ) return NULL; // $this->e->triggerException ('Error in class (' . __CLASS__ . '): [ Config not found ] on line ' . __LINE__);

                        
                        
                        
                        if (!isset($this->_data['name'])) $this->_data['name'] = $this->_config_id;
                        if (!isset($this->_data['description'])) $this->_data['description'] = 'no-description';



                } else {



                        $this->_data['config_id'] = $this->_config_id;
                        $this->_data['name'] = $this->_config_id;
                        $this->_data['description'] = 'no-description';




                }
		
	
                $this->_collection = array_keys($this->_data);
                parent::__construct();
	
		
        }
	
	
        

    
    /**
     * getEvents
     * 
     * @return array
     */
    public static function getEvents() {

        return self::$_events;
    }

        
        
        
	
        // Over-ride
        public function current()
        {
            return $this->_data[$this->_collection[$this->_position]];

        }

        
        // Over-ride
        function key() {
            return $this->_collection[$this->_position];
        }
        

	
	
	// GETTER ////////////
	
	public function __get ($name)
	{
		if (array_key_exists($name, $this->_data)) return $this->_data[$name];
		else return NULL;
		
	}
	
	
	
	
	// SETTER ////////////
	
	public function __set ($name, $value)
	{
		if (array_key_exists($name, $this->_data)) {
			$this->_data[$name] = $value;
			
			if (!in_array($name, $this->_modified)) $this->_modified[] = $name;
			return TRUE;
		}
		
	}
	


	// NEW PARAM ////////////
	
	public function newParam ($name, $default_value='')
	{
		if (!array_key_exists($name, $this->_data)) {
			$this->_data[$name] = $default_value;
			
			if (!in_array($name, $this->_modified)) $this->_modified[] = $name;
			return TRUE;
		} else {
			
			$this->e->triggerError('Error in class (' . __CLASS__ . '): [ Entry already exists ] on line ' . __LINE__);
			return FALSE;
		}
		
	}
	
	
	
		
	// GETTER ////////////
	
	public function getParam ($name)
	{
		if (array_key_exists($name, $this->_data)) return $this->_data[$name];
		else return NULL;
		
	}
	
	
	
	
	// SETTER ////////////
	
	public function setParam ($name, $value)
	{
		if (array_key_exists($name, $this->_data)) {
			$this->_data[$name] = $value;
			
			if (!in_array($name, $this->_modified)) $this->_modified[] = $name;
			return TRUE;
        } else {
            return FALSE;
        }
		
	}
	
	//// LOAD ///////
	
	static public function load ($config_id)
	{
		
		// Create instance...
		$object =  __CLASS__;
		$_instance = new $object ($config_id, FALSE);
                
                if (!empty($_instance->_data)) return $_instance;
                else return NULL;
		
		
		
	}
	

    /**
     * create
     *
     * @param string $config_id
     * @return \Cataleya\System\Config
     */
	static public function create ($config_id, array $_params = []) 
	{
		
		// Create instance...
		$object =  __CLASS__;
		$_this = new $object ($config_id, TRUE);
		
        foreach ($_params as $k => $v) { $_this->newParam($k, $v);  }

        $_this->save();

        return $_this;
		
	}



    /**
     * init
     *
     * @param string $config_id
     * @return \Cataleya\System\Config
     */
    static public function init ($config_id, array $_params = []) 
    {
        return self::create($config_id, $_params);

    }





	
	/**
	 * drop     Remove config completely from database.
	 *
	 * @return void
	 */
	public function drop ()
	{


		//////// First remove config rows from 'Config' table //////////////////////////
		static $config_delete, $del_param_config_id, $del_param_name;
		
		
		// Do prepared statement for 'DELETE'...
		if (empty($config_delete)) {
		$config_delete = $this->dbh->prepare('DELETE FROM config WHERE config_id = :config_id');
		$config_delete->bindParam(':config_id', $del_param_config_id, \PDO::PARAM_STR);
		}
		
		
		// Execute 'DELETE' statement...
		foreach ($this->_modified as $name)
		{
		$del_param_config_id = $this->_config_id;
        if (!$config_delete->execute()) return $this->e->triggerError('Error in class (' 
                                                                . __CLASS__ . '): [ ' 
                                                                . implode(', ', $config_delete->errorInfo()) 
                                                                . ' ] on line ' . __LINE__);
		
		}
		
	
		$this->_modified = [];
        $this->_data = [];    
	    $this->_collection = [];
            
		
	}
	





    /**
     * delete   Alias for ::drop()
     *
     * @return void
     */
    public function delete () 
    {
        return $this->drop();
    }




	
	/**
	 * save
	 *
	 * @return void
	 */
	public function save ()
	{

        if (empty($this->_data)) return;

		//////// First remove config rows from 'Config' table //////////////////////////
		static $config_delete, $del_param_config_id, $del_param_name;
		
		
		// Do prepared statement for 'DELETE'...
		if (empty($config_delete)) {
		$config_delete = $this->dbh->prepare('DELETE FROM config WHERE config_id = :config_id AND name = :name');
		$config_delete->bindParam(':config_id', $del_param_config_id, \PDO::PARAM_STR);
		$config_delete->bindParam(':name', $del_param_name, \PDO::PARAM_STR);
		}
		
		
		// Execute 'DELETE' statement...
		foreach ($this->_modified as $name)
		{
		$del_param_config_id = $this->_config_id;
		$del_param_name = base64_encode($name);
		if (!$config_delete->execute()) return $this->e->triggerError('Error in class (' . __CLASS__ . '): [ ' . implode(', ', $config_delete->errorInfo()) . ' ] on line ' . __LINE__);
		
		}
		
		
		///////////// The insert new rows ////////////////
		static $config_insert, $insert_param_config_id, $insert_param_name, $insert_param_value;
		
		// Do prepared statement for 'INSERT'...
		if (empty($config_insert)) {
		$config_insert = $this->dbh->prepare('
														INSERT INTO config (config_id, name, value, last_modified)   
														VALUES (:config_id, :name, :value, NOW())  
														');
		$config_insert->bindParam(':config_id', $insert_param_config_id, \PDO::PARAM_STR);
		$config_insert->bindParam(':name', $insert_param_name, \PDO::PARAM_STR);
		$config_insert->bindParam(':value', $insert_param_value, \PDO::PARAM_STR);
		}


		
		foreach ($this->_modified as $name) {
			
			// If value is an array, save as multiple entries...
			if (is_array($this->_data[$name])) {
				
				
				foreach ($this->_data[$name] as $v2) {
						$insert_param_config_id = $this->_config_id;
						$insert_param_name = base64_encode($name);
						$insert_param_value = \Cataleya\Helper\Bcrypt::encrypt($v2);
						
						if (!$config_insert->execute()) return $this->e->triggerException('Error in class (' . __CLASS__ . '): [ ' . implode(', ', $config_insert->errorInfo()) . ' ] on line ' . __LINE__);
				}
				
				
				

			// Else save normally as a single entry...
			} else {
				
				$insert_param_config_id = $this->_config_id;
				$insert_param_name = base64_encode($name);
				$insert_param_value = \Cataleya\Helper\Bcrypt::encrypt($this->_data[$name]);
						
				if (!$config_insert->execute()) return $this->e->triggerException('Error in class (' . __CLASS__ . '): [ ' . implode(', ', $config_insert->errorInfo()) . ' ] on line ' . __LINE__);
				
			}
			
		}
					
		
		
	}
	



}



