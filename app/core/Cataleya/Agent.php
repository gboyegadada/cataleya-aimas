<?php


namespace Cataleya;

use \Cataleya\Helper\DBH;
use \Cataleya\Error;


class Agent {
	
	
	private $_data = array();
	
	private 
            $_logo, 
            $_contact, 
            $_role;
				

	private $_modified = array();
    protected 
            $_page_size, 
            $_page_num;

	private $dbh, $e;
	




	/**
	 * __construct
	 *
	 * @return void
	 */
	private function __construct ()
	{
		
		// Get database handle...
        $this->dbh = \Cataleya\Helper\DBH::getInstance();
		
		
	}
	




    /**
     * __destruct
     *
     * @return void
     */
    public function __destruct()
    {
		$this->saveData();
    }
	




	/**
	 * load
	 *
	 * @param int $id
	 * @return void
	 */
	public static function load ($id = 0)
	{
		
		if ($id == 0 || filter_var($id, FILTER_VALIDATE_INT) === FALSE) return NULL;
		
		$agent = new static ();
		
		// LOAD: AGENT INFO
		static $select_handle, $param_id;
		
			
		if (!isset($select_handle)) {
			// PREPARE SELECT STATEMENT...
			$select_handle = DBH::getInstance()->prepare('SELECT * FROM agents WHERE agent_id = :agent_id LIMIT 1');
			$select_handle->bindParam(':agent_id', $param_id, \PDO::PARAM_INT);
		}
		
		
		$param_id = $id;
		if (!$select_handle->execute()) throw new Error ('DB Error: '.implode(', ', $select_handle->errorInfo())); 
		
		$agent->_data = $select_handle->fetch(\PDO::FETCH_ASSOC);

        if (empty($agent->_data))
        {
            unset ($agent); return NULL;
        }
                
               

        // LOAD: DISPLAY PICTURE
        $agent->_logo = \Cataleya\Asset\Image::load($agent->_data['logo_image_id']);
        
        // LOAD: LOCATION
        $agent->_contact = \Cataleya\Asset\Contact::load($agent->_data['contact_id']);

        
        return $agent;	
		
		
	}



	/**
	 * create
	 *
	 * @param \Cataleya\Agent\Role $_Role
	 * @param string $name
	 * @param string $code
	 * @return void
	 */
	static public function create (\Cataleya\Agent\Role $_Role, $name = '', $code = '') 
	{
		
        if (empty($code) && !empty($name)) $code = \Cataleya\Helper::codify($name, 2);

        $code = $_tmp_code = \Cataleya\Helper::codify($name, 2); $_num = 1;
        while (DBH::ping('agents', 'code', $_tmp_code) === true) $_tmp_code = $code . strval($_num++);

        $code = $_tmp_code;

        $_expected = [
                'code' => 'match:/^[A-Z0-9]{0,10}$/', 
                'name' => 'name', 
        ];

        $_params = \Cataleya\Helper\Validator::params(
            $_expected, 
            // Supplied params (arguments)...
            [
                'code' => $code, 
                'name' => $name
            ]
        );

        foreach ($_params as $k=>$v) if ($v === false) throw new \Cataleya\Error('Invalid argument: ' . $k . ' should be : ' . $_expected[$k]);
				
        $_Contact = \Cataleya\Asset\Contact::create(null, [ 
            'firstname' => $_params['name'], 
            'label' => $_params['name'], 
            'company' => $_params['name'] 
        ]);
                 

		
		// construct
		static 
				$insert_handle, 
                $insert_handle_param_contact_id, 
                $insert_handle_param_code, 
                $insert_handle_param_role_id;
		
		if (empty($insert_handle))
		{
			$insert_handle = DBH::getInstance()->prepare('' . 
                    'INSERT INTO agents ' . 
                    '(role_id, contact_id, code, ' . 
                    'date_added, last_modified) ' . 
                    'VALUES (:role_id, :contact_id, :code, ' . 
                    'NOW(), NOW())
                    ');
															
			$insert_handle->bindParam(':role_id', $insert_handle_param_role_id, \PDO::PARAM_INT);
            $insert_handle->bindParam(':contact_id', $insert_handle_param_contact_id, \PDO::PARAM_INT);
            $insert_handle->bindParam(':code', $insert_handle_param_code, \PDO::PARAM_STR);
		}
		
        $insert_handle_param_role_id = $_Role->getID();
        $insert_handle_param_contact_id = $_Contact->getID();
        $insert_handle_param_code = $code;
		
		if (!$insert_handle->execute()) throw new Error ('DB Error: ' . implode(', ', $insert_handle->errorInfo())); 
										
		
		// AUTOLOAD NEW PRODUCT AND RETURN IT
		$agent = self::load(DBH::getInstance()->lastInsertId());
                
		
		return $agent;
		
		
	}






    /*
     *
     * [ getID ] 
     *_____________________________________________________
     *
     *
     */

    public function getID()
    {
            return $this->_data['agent_id'];
    }




	
	
	public function saveData () 
	{
		

		if (empty($this->_modified)) return;
                
        $this->_modified = array_unique($this->_modified);
                
		$_update_params = array();
		$key_val_pairs = array();
		
		foreach ($this->_modified as $key) $key_val_pairs[$key] = $key . ' = :' . $key;
		
	
		// Do prepared statement for 'INSERT'...
		$update_handle = $this->dbh->prepare('
                        UPDATE agents 
                        SET last_modified = NOW(), '.implode (', ', $key_val_pairs).', last_modified=now()       
                        WHERE agent_id = :agent_id 
                        ');
		$update_handle->bindParam(':agent_id', $_update_params['agent_id'], \PDO::PARAM_INT);
		foreach ($this->_modified as $key) {
                    
            $_update_params[$key] = $this->_data[$key];                        
			$update_handle->bindParam(':'.$key, $_update_params[$key], DBH::getTypeConst($_update_params[$key]));

		}
		
		
		$_update_params['agent_id'] = $this->_data['agent_id'];
		foreach ($this->_modified as $key) {
			$_update_params[$key] = $this->_data[$key];
		}
		
		if (!$update_handle->execute()) throw new Error ('DB Error: ' . implode(', ', $update_handle->errorInfo()));
		
	}
	
	
       
 
        
        

	public function delete () 
	{


                        
                       
			// DELETE AGENT
			$delete_handle = $this->dbh->prepare('
													DELETE FROM agents 
													WHERE agent_id = :agent_id
													');
			$delete_handle->bindParam(':agent_id', $delete_handle_param_id, \PDO::PARAM_INT);
			
			$delete_handle_param_id = $this->_data['agent_id'];
	
			if (!$delete_handle->execute()) $this->e->triggerException('
											Error in class (' . __CLASS__ . '): [ ' . 
											implode(', ', $delete_handle->errorInfo()) . 
											' ] on line ' . __LINE__);
											
			
			// CLEAN UP...
                        if (!empty($this->_collection)) $this->_description->delete();  
                        if (!empty($this->_contact)) $this->_contact->delete(); 
                        if (!empty($this->_logo)) $this->_logo->destroy(); 
                        
 
		
		// $this = NULL;
		return TRUE;
		
		
	}




        
        



	/**
	 * setLogo
	 *
	 * @param \Cataleya\Asset\Image $image
	 * @return void
	 */
	public function setLogo (\Cataleya\Asset\Image $image)
	{
		if (!in_array('logo_image_id', $this->_modified)) $this->_modified[] = 'logo_image_id';
                
                
		$this->_data['logo_image_id'] = $image->getID();

                // Remove old image
		if ($this->_logo->getID() != 0) 
                {
                    $this->saveData();
                    $this->_logo->destroy();
                }		
                
		$this->_logo = $image; 
                
                
                
 
		
		return $this;              
                
		
	}





	/**
	 * unsetLogo
	 *
	 * @return void
	 */
	public function unsetLogo ()
	{
		$this->_data['logo_image_id'] = NULL;
		
		
		if (!in_array('logo_image_id', $this->_modified)) $this->_modified[] = 'logo_image_id';
                $this->saveData();
                $this->_logo->destroy();
		$this->_logo = NULL;
	}





	/**
	 * getLogo
	 *
	 * @return void
	 */
	public function getLogo ()
	{
		if (empty($this->_logo) )
		{
			$this->_logo = \Cataleya\Asset\Image::load($this->_data['logo_image_id']);
		}
		
		return $this->_logo;
		
	}




    /**
     * getImageId
     *
     * @return void
     */
    public function getImageId()
    {
            return $this->_data['logo_image_id'];
    }
    
    /**
     * 
     * @return string
     */
    public function getCode() {
        return $this->_data['code'];
    }




	/**
     * is
     *
     * ---------------------------------------
     *
     * Use like $_Role->is('SUPPLIER')
	 *
	 * @param string | \Cataleya\Agent\Role $_role_handle
	 * @return boolean
	 */
	public function is ($_role_handle = NULL) 
	{

        return $this->getRole()->is($_role_handle);
		
	}
	
	


	// getRole	
	public function getRole () 
	{
            if (empty($this->_role)) 
            {
                $this->_role = \Cataleya\Agent\Role::load($this->_data['role_id']);
            }
            
            return $this->_role;
		
	}
	





    /**
     * getCompanyName
     *
     * @return void
     */
    public function getCompanyName()
    {
            return $this->getContact()->getEntryCompanyName();
    }


    /**
     * setCompanyName
     *
     * @param string $value
     * @return void
     */
    public function setCompanyName($value)
    {
            return $this->getContact()->setEntryCompanyName($value);
    }


    /**
     * getContactName
     *
     * @return void
     */
    public function getContactName()
    {
            return $this->getContact()->getEntryName();
    }




    /**
     * setContactName
     *
     * @param mixed $value
     * @return void
     */
    public function setContactName($value)
    {
            $this->getContact()->setEntryName($value);

            return $this;
    }





    /**
     * getContactFirstName
     *
     * @return void
     */
    public function getContactFirstName()
    {
            return $this->getContact()->getEntryFirstName();
    }




    /**
     * setContactFirstName
     *
     * @param mixed $value
     * @return void
     */
    public function setContactFirstName($value)
    {
            $this->getContact()->setEntryFirstName($value);

            return $this;
    }





    /**
     * getContactLastName
     *
     * @return void
     */
    public function getContactLastName()
    {
            return $this->getContact()->getEntryLastName();
    }




    /**
     * setContactLastName
     *
     * @param mixed $value
     * @return void
     */
    public function setContactLastName($value)
    {
            $this->getContact()->setEntryLastName($value);

            return $this;
    }







    /*
     *
     * [ getTelephone ] 
     *_____________________________________________________
     *
     *
     */

    public function getTelephone()
    {
            return $this->getContact()->getEntryWorkPhone();
    }




    /*
     *
     * [ setTelephone ] 
     *_____________________________________________________
     *
     *
     */

    public function setTelephone($value)
    {
            $this->getContact()->setEntryWorkPhone($value);

            return $this;
    }





    /*
     *
     * [ getEmail ] 
     *_____________________________________________________
     *
     *
     */

    public function getEmail()
    {
            return $this->getContact()->getEntryEmail();
    }






    /*
     *
     * [ setEmail ] 
     *_____________________________________________________
     *
     *
     */

    public function setEmail($value)
    {
            $this->getContact()->setEntryEmail($value);

            return $this;
    }




    
    

    /*
     *
     * [ getWebsite ] 
     *_____________________________________________________
     *
     *
     */

    public function getWebsite()
    {
            return $this->getContact()->getEntryWebsite();
    }






    /*
     *
     * [ setWebsite ] 
     *_____________________________________________________
     *
     *
     */

    public function setWebsite($value)
    {
            $this->getContact()->setEntryWebsite($value);

            return $this;
    }




    
    
    
    
    
    



    /*
     *
     * [ getDateAdded ] 
     *_____________________________________________________
     *
     *
     */

    public function getDateAdded()
    {
            return $this->_data['date_added'];
    }







    /*
     *
     * [ getDateModified ] 
     *_____________________________________________________
     *
     *
     */

    public function getDateModified()
    {
            return $this->_data['last_modified'];
    }




    /*
     *
     * [ getActive ] 
     *_____________________________________________________
     *
     *
     */

    public function getActive()
    {
            return $this->_data['active'];
    }




    /*
     *
     * [ setActive ] 
     *_____________________________________________________
     *
     *
     */

    public function setActive($value)
    {
            $this->_data['active'] = $value;
            $this->_modified[] = 'active';

            return $this;
    }







        

	/*
	 *
	 *  [ getContact ]
	 * ________________________________________________________________
	 * 
	 * Returns image object
	 *
	 *
	 *
	 */
	 

	public function getContact ()
	{
		if (empty($this->_contact) )
		{
			$this->_contact = \Cataleya\Asset\Contact::load($this->_data['contact_id']);
		}
		
		return $this->_contact;
		
	}



        
        
 
        
        
        
        
        



}
	
	
	
	
