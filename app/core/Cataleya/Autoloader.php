<?php


namespace Cataleya;

/**
 * General utility class, not to be instantiated.
 *
 * @package Cataleya
 *
 * @author Gboyega Dada <gboyega@fpplabs.com>
 * 
 * (c) 2015 Fancy Paper Planes
 * 
 */
abstract class Autoloader 
{
    public static $initialized = false;
    public static $inits = array();


    /**
     * Registers an initializer callable that will be called the first time
     * an Em class is autoloaded.
     *
     * This enables you to tweak the default configuration in a lazy way.
     *
     * @param mixed $callable A valid PHP callable that will be called when autoloading the first FPP class
     */
    public static function init($callable)
    {
        self::$inits[] = $callable;
    }

    /**
     * Internal autoloader for spl_autoload_register().
     *
     * @param string $class
     */
    public static function autoload($class)
    {

        // namespace \Cataleya\Apps
        if (0 === strpos($class, 'Cataleya\Apps\\')) {
            $class = str_replace('Cataleya\Apps\\', '', $class);

            $path = CATALEYA_PLUGINS_PATH.DIRECTORY_SEPARATOR.str_replace('\\', DIRECTORY_SEPARATOR, $class).'.php';

        } 

        // namesapace \Cataleya
        else if (0 === strpos($class, 'Cataleya')) {
        
            //$path = __DIR__.DIRECTORY_SEPARATOR.'..'.DIRECTORY_SEPARATOR.str_replace('\\', DIRECTORY_SEPARATOR, $class).'.php';
            $path = ROOT_PATH.'app/core/'.str_replace('\\', DIRECTORY_SEPARATOR, $class).'.php';

            if (!file_exists($path)) { 
                return; 
            } 
            
        } 
        
        // Load from library (app_root/library)
        else {
        
            $n = preg_split('![_\\\]!', $class);
            
            if (!isset($n[0])) { return; }
            
            $lib_path = LIB_PATH.$n[0];
            
            
            if (count($n) === 1) { $lib_path = $lib_path . '.php'; }
            
            
            if (!file_exists($lib_path) || (is_dir($lib_path) && !file_exists($lib_path.'/.fpp.inc'))) { return; }
            
            
            
            $path = LIB_PATH.DIRECTORY_SEPARATOR.str_replace(array('\\', '_'), DIRECTORY_SEPARATOR, $class).'.php';
            
            if (!file_exists($path)) { return; }
        }
        

        require $path;
        

        if (self::$inits && !self::$initialized) {
            self::$initialized = true;
            foreach (self::$inits as $init) {
                call_user_func($init);
            }
        }
    }

    /**
     * Configure autoloading using \Em\Autoloader.
     *
     * This is designed to play nicely with other autoloaders.
     *
     * @param mixed $callable A valid PHP callable that will be called when autoloading the first FPP class
     */
    public static function registerAutoload($callable = null, $prepend = false)
    {
        if (null !== $callable) {
            self::$inits[] = $callable;
        }
        
        if (PHP_VERSION_ID < 50300) {
            spl_autoload_register(array(__CLASS__, 'autoload'));
        } else {
            spl_autoload_register(array(__CLASS__, 'autoload'), true, $prepend);
        }
        
    }
}
