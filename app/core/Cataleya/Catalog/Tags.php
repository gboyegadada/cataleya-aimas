<?php



namespace Cataleya\Catalog;




/*
 *
 * 	@Package: Cataleya
 * 	@Class: \Cataleya\Catalog\Tags
 *
 * 	(c) 2012 Fancy Paper Planes
 *
 *
 *
 */

class Tags extends \Cataleya\Collection {
   

    // To be used by [\Cataleya\Collection]
    protected $_collection = array ();
    protected $_position = 0;
    protected $_show_hidden = FALSE;
    protected $_store;
    
    // CONST VALUES 1 - 15 RESERVED FOR [ _Abstract_Collection ]
    
    const ORDER_BY_NAME = 16;
    const ORDER_BY_ID = 17;


    public function __construct() {
        
        // Get database handle...
        $this->dbh = \Cataleya\Helper\DBH::getInstance();
		
        // Get error handler
        $this->e = \Cataleya\Helper\ErrorHandler::getInstance();
        

        
        parent::__construct();

        
    }
    
    
    
    
    
    

	/**
         * 
         * @param array $sort
         * @param int $page_size
         * @param int $page_num
         * @return \Cataleya\Catalog\Tags
         */
	public static function load ($sort = array(), $page_size = 100, $page_num = 1)
	{
            
            
                $_objct = __CLASS__;
                $instance = new $_objct;
                
                

                // This is used to determine which column is to be used in the index...
                // $_index_with = 'title';

                if (is_array($sort) && !empty($sort['order_by']) && !empty($sort['order']) && is_int($sort['order_by']) && is_int($sort['order']) )
                {

                    // ASC | DESC
                    switch ($sort['order']) {

                        case self::ORDER_ASC:
                            $param_order = 'ASC';
                            break;

                        case self::ORDER_DESC:
                            $param_order = 'DESC';
                            break;

                        default :
                            $param_order = 'ASC';
                            break;
                    }  


                    // ORDER BY
                    switch ($sort['order_by']) {

                        case self::ORDER_BY_NAME:
                            $param_order_by = "title $param_order";
                            $_index_with = 'title';
                            break;

                        case self::ORDER_BY_ID:
                            $param_order_by = "a.tag_id $param_order";
                            $_index_with = 'a.tag_id';
                            break;
                        
                        case self::ORDER_BY_CREATED:
                            $param_order_by = "a.date_added $param_order";
                            $_index_with = 'a.date_added';
                            break;
                        
                        case self::ORDER_BY_MODIFIED:
                            $param_order_by = "a.last_modified $param_order";
                            $_index_with = 'a.last_modified';
                            break;

                        default :
                            $param_order_by = "title $param_order";
                            $_index_with = 'title';
                            break;
                    }





                }

                // Defaults                
                else {

                        $param_order = 'ASC';
                        $param_order_by = "title $param_order";
                        $_index_with = 'title';

                }    

                
                
                
                    
                // Show hidden products? (not categories!!)
                if (is_array($sort) && isset($sort['show_hidden']) && is_bool($sort['show_hidden'])) $instance->_show_hidden = ($sort['show_hidden']) ? TRUE : FALSE;
                $instance->_store = (isset($sort['Store']) && $sort['Store'] instanceof \Cataleya\Store) ? $sort['Store'] : NULL;


                
                
                
                
                


                // Check if index is specified
                if (is_array($sort) && isset($sort['index']) && is_string($sort['index']))
                {
                    $param_regex = (preg_match('/^[a-zA-Z]{1}$/', $sort['index']) > 0) ? '^[' . strtolower($sort['index']) . '].*' : '.*';
                }

                else {
                    $param_regex = '.*';
                }

                
                $_store_filter = (isset($sort['Store']) && is_object($sort['Store']) && $sort['Store'] instanceof \Cataleya\Store) 
                        ? ' AND store_id = ' . $sort['Store']->getID()
                        : '';


                $param_offset = 0;
                $param_limit = 99999999;
        
        


                // PREPARE SELECT STATEMENT...
                $select_handle = $instance->dbh->prepare(''
                                                . 'SELECT tag_id, keyword ' 
                                                . 'FROM tags a '
                                                . 'INNER JOIN content_languages b '
                                                . 'ON a.description_id = b.content_id '
                                                . 'INNER JOIN url_rewrite c '
                                                . 'ON a.url_rewrite_id = c.url_rewrite_id '
                                                . 'WHERE ' . $_index_with . ' REGEXP :regex '
                                                . $_store_filter 
                                                . ' ORDER BY '. $param_order_by     
                                                . ' LIMIT :offset, :limit ' 
                                                . '');

                $select_handle->bindParam(':offset', $param_offset, \PDO::PARAM_INT);
                $select_handle->bindParam(':limit', $param_limit, \PDO::PARAM_INT);
                $select_handle->bindParam(':regex', $param_regex, \PDO::PARAM_STR);
                

                if (!$select_handle->execute()) $instance->e->triggerException('
                                                                                                Error in class (' . __CLASS__ . '): [ ' . 
                                                                                                implode(', ', $select_handle->errorInfo()) . 
                                                                                                ' ] on line ' . __LINE__);

                
                // while ($row = $select_handle->fetch(\PDO::FETCH_ASSOC))$instance->_collection[] = $row;

                $instance->_collection = $select_handle->fetchAll(\PDO::FETCH_ASSOC);
                
                $instance->pageSetup($page_size, $page_num);
                
                return $instance;

            
        }
    
    
    
    
    
    

    
    
    
    
    /**
     * 
     * @return \Cataleya\Catalog\Tag
     * 
     */
    public function current()
    {
        return \Cataleya\Catalog\Tag::load($this->_collection[$this->_position]['tag_id'], array('show_hidden'=>$this->_show_hidden));
        
    }


      
    /*
     *
     * [ getTagByKeyword ] 
     *_____________________________________________________
     *
     *
     */

    public function getTagByKeyword($keyword = NULL, $sort = NULL, $page_size = 16, $page_num = 1)
    {
            if (!is_string($keyword)) return NULL;

            foreach ($this->_collection as $tag) 
            {
                if ($tag['keyword'] === trim(strtolower($keyword)) ) {
                    return \Cataleya\Catalog\Tag::load($tag['tag_id'], $sort, $page_size, $page_num);
                    break;
                }
            }
            
            return NULL;
    }    
    
    
    
    
    
    /*
     * 
     * [ search ]
     * 
     */
    
    public static function search ($_term = '', $sort = array('order_by'=>'', 'order'=>'', 'index'=>''), $page_size = 100, $page_num = 1) 
    {
        

                // This is used to determine which column is to be used in the index...
                // $_index_with = 'title';

                if (is_array($sort) && !empty($sort['order_by']) && !empty($sort['order']) && is_int($sort['order_by']) && is_int($sort['order']) )
                {


                    // ASC | DESC
                    switch ($sort['order']) {

                        case self::ORDER_ASC:
                            $param_order = 'ASC';
                            break;

                        case self::ORDER_DESC:
                            $param_order = 'DESC';
                            break;

                        default :
                            $param_order = 'ASC';
                            break;
                    }  


                    // ORDER BY
                    switch ($sort['order_by']) {

                        case self::ORDER_BY_NAME:
                            $param_order_by = "title $param_order";
                            $_index_with = 'title';
                            break;

                        case self::ORDER_BY_ID:
                            $param_order_by = "tag_id $param_order";
                            $_index_with = 'tag_id';
                            break;
                        
                        case self::ORDER_BY_CREATED:
                            $param_order_by = "date_added $param_order";
                            $_index_with = 'date_added';
                            break;
                        
                        case self::ORDER_BY_MODIFIED:
                            $param_order_by = "last_modified $param_order";
                            $_index_with = 'last_modified';
                            break;

                        default :
                            $param_order_by = "title $param_order";
                            $_index_with = 'title';
                            break;
                    }




                }

                // Defaults                
                else {

                        $param_order = 'ASC';
                        $param_order_by = "title $param_order";
                        $_index_with = 'title';

                }    



            // First, take out funny looking characters...
            $_term = preg_replace('/[^-\s\.A-Za-z0-9@*#+]+/', '', $_term);

            // Trim then split terms by [:space:]
            $terms = explode(' ', trim($_term, ' '));



            // SORT SEARCH TERMS
            $regex = array();
            $regex['title'] = array();


            foreach ($terms as $key=>$val) {

                    // Loose any search terms shorter than 3 chars...
                    if (strlen($val) < 2) unset($terms[$key]);
                    
                    // Also make dots safe...
                    else if (preg_match('/[-\s\.A-Za-z0-9]{1,100}/', $val))  $regex['title'][] = preg_replace('/[\.]/', '\.', $val); 
            }

            
            // Build REGEX
            $regex_concat = '';
            foreach ($regex as $k => $v) {
                    if (count($v) == 0) {$regex[$k] = ''; continue; } // no search terms...
                    elseif (count($v) > 3) $v = array_slice($v, 0, 3); // truncate if there are too many search terms...

                    $regex[$k] = "^.*(" . implode('|', $v) . ").*$";
                    $regex_concat .= ($regex_concat != '') ? ' OR ' : '';
                    $regex_concat .= $k . ' REGEXP :regex_' . $k;


            }

            
            
            // Create instance...
            $_objct = __CLASS__;
            $instance = new $_objct;


            // If nothing made it through the filtering...
            // [ _collection ] will be empty a.k.a no results..
            if ($regex_concat == '') return $instance;




            $param_offset = 0;
            $param_limit = 99999999;
        
            
            
            // DO SEARCH...
            $select_handle = \Cataleya\Helper\DBH::getInstance()->prepare('
                                            SELECT tag_id, keyword  
                                            FROM tags a 
                                            INNER JOIN content_languages b 
                                            ON a.description_id = b.content_id 
                                            INNER JOIN url_rewrite c 
                                            ON a.url_rewrite_id = c.url_rewrite_id 
                                            WHERE ' . $regex_concat . '  
                                            ORDER BY '. $param_order_by . '   
                                            LIMIT :offset, :limit
                                            ');

            $select_handle->bindParam(':offset', $param_offset, \PDO::PARAM_INT);
            $select_handle->bindParam(':limit', $param_limit, \PDO::PARAM_INT);
            
            // bind search search params
            foreach ($regex as $k=>$v) $select_handle->bindParam(':regex_'.$k, $regex[$k], \PDO::PARAM_STR);

            if (!$select_handle->execute()) \Cataleya\Helper\ErrorHandler::getInstance ()->triggerException('
                                                                        Error in class (' . __CLASS__ . '): [ ' . 
                                                                        implode(', ', $select_handle->errorInfo()) . 
                                                                        ' ] on line ' . __LINE__);





            // while ($row = $select_handle->fetch(\PDO::FETCH_ASSOC))$instance->_collection[] = $row;
            
            $instance->_collection = $select_handle->fetchAll(\PDO::FETCH_ASSOC);

            $instance->pageSetup($page_size, $page_num);

            return $instance;
        
    }
    
    

    
    
    
}


