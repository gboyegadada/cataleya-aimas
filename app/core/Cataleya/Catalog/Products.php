<?php


namespace Cataleya\Catalog;





/**
 * class Products
 *
 * @author Fancy Paper Planes <gboyega@fancypaperplanes.com>
 */
class Products extends \Cataleya\Collection {
    
    // To be used by [\Cataleya\Collection]
    private $_collection = array ();
    private $_position = 0;


    public function __construct() {
       
        // Get database handle...
        $this->dbh = \Cataleya\Helper\DBH::getInstance();
		
        // Get error handler
        $this->e = \Cataleya\Helper\ErrorHandler::getInstance();
        

        // LOAD: PRODUCTS
        static $products_select;


        if (!isset($products_select)) {
                // PREPARE SELECT STATEMENT...
                $products_select = $this->dbh->prepare('SELECT product_id FROM products LIMIT :offset, :limit');

                $products_select->bindParam(':offset', $param_offset, \PDO::PARAM_INT);
                $products_select->bindParam(':limit', $param_limit, \PDO::PARAM_INT);
        }


        $param_offset = 0;
        $param_limit = 999999999999;

        if (!$products_select->execute()) $this->e->triggerException('
                                                                                        Error in class (' . __CLASS__ . '): [ ' . 
                                                                                        implode(', ', $products_select->errorInfo()) . 
                                                                                        ' ] on line ' . __LINE__);
        while ($row = $products_select->fetch(\PDO::FETCH_ASSOC))
        {
            $this->_collection[] = $row['product_id'];
        }
        
        parent::__construct();
 
        
    }

    
    
    
    


    /**
     *
     * 
     * @return \Cataleya\Catalog\Product (object)
     *
     *
     */
    public function current()
    {
        return \Cataleya\Catalog\Product::load($this->_collection[$this->_position]);
        
    }


    
    
}


