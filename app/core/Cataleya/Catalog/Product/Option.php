<?php


namespace Cataleya\Catalog\Product;


use \Cataleya\Helper\DBH;
use \Cataleya\Error;



/*
 *
 *	@Package: Cataleya
 *	@Class: \Cataleya\Catalog\Product\Option
 *
 *	(c) 2012 Fancy Paper Planes
 *
 *
 *
 */


class Option 
extends \Cataleya\Collection 
implements \Cataleya\System\Event\Observable 

{

/*- - -- -- -- -- -- -- -- -- - - */

	private $_auto_save = TRUE;
	
	private $_data = array();
        
	private $_modified = array();
								
	private $dbh, $e, $_price, $_stock, $_image;
	
    
    // To be used by [\Cataleya\Collection] to iterate privileges assigned to this role...
    protected $_collection = array ();
    protected $_position = 0;
	
    protected static $_events = [
            'product/option/created',
            'product/option/updated', 
            'product/option/deleted'
        ];
	
	public function __construct ()
	{
	
            
                parent::__construct();
                
		// Get database handle...
                $this->dbh = \Cataleya\Helper\DBH::getInstance();
		
		// Get error handler
		$this->e = \Cataleya\Helper\ErrorHandler::getInstance();
		
	}
        
        
  
        
        
        
	public function __destruct ()
	{
		if ($this->_auto_save) $this->save();
	}
	

        
	
	/**
         * 
         * @param integer $option_id
         * @return \Cataleya\Catalog\Product\Option
         * 
         */
	public static function load ($option_id = 0)
	{
	
		if ($option_id == 0 || filter_var($option_id, FILTER_VALIDATE_INT) === FALSE) return NULL;
		$option = new \Cataleya\Catalog\Product\Option();
		
		static 
                        $option_select, 
                        $option_select_param_option_id;
		
		if (empty($option_select))
		{
			$option_select = $option->dbh->prepare('
												SELECT * 
												FROM product_options 
												WHERE option_id = :option_id 
                                                                                                LIMIT 1
												');
			$option_select->bindParam(':option_id', $option_select_param_option_id, \PDO::PARAM_INT);
		}
		
		$option_select_param_option_id = $option_id;
		
		if (!$option_select->execute()) $option->e->triggerException('
										Error in class (' . __CLASS__ . '): [ ' . 
										implode(', ', $option_select->errorInfo()) . 
										' ] on line ' . __LINE__);
                
        $option->_data = $option_select->fetch(\PDO::FETCH_ASSOC);
                
		if (empty($option->_data)) return NULL;
		
                // load attributes
                $option->loadAttributes();
		
		
		return $option;
	
	
	}
	
	
	/**
         * 
         * @param \Cataleya\Catalog\Product $_Product
         * @param array $_Attributes array of objects (\Cataleya\Catalog\Product\Attribute)
         * @return \Cataleya\Catalog\Product\Option
         * 
         */
	public static function create (\Cataleya\Catalog\Product $_Product, $_Attributes = array ())
	{
            
           

            // 1. Get option instance
            //$option = new \Cataleya\Catalog\Product\Option();
            
            
            // 2. Init
            $_data = array (
              
                'option_id' => NULL, 
                'product_id'    =>  $_Product->getID(), 
                'product_type_id' => $_Product->getProductTypeID(), 
                'price_id'  =>  \Cataleya\Catalog\Price::create()->getID(), 
                'stock_id'  =>  \Cataleya\Catalog\Product\Stock::create()->getID()
                
            );
            
            
            
            // 5. Save to database
            
            $_params = array();
            $cols_and_placeholders = array();
            foreach ($_data as $key=>$val) $cols_and_placeholders[$key] = ':' . $key;
	                 


            $insert_handle = \Cataleya\Helper\DBH::getInstance()->prepare('
                                                    INSERT INTO product_options (' . implode (', ', array_keys($cols_and_placeholders)) . ') 
                                                    VALUES (' . implode (', ', array_values($cols_and_placeholders)) . ')
                                                        ');
            
            
            foreach ($_data as $key=>$val) {

                    $_params[$key] = $_data[$key];
                    $insert_handle->bindParam(':'.$key, $_params[$key], \Cataleya\Helper\DBH::getTypeConst($_params[$key]));
            }
	            

            if (!$insert_handle->execute()) \Cataleya\Helper\ErrorHandler::getInstance()->triggerException('
                                                                            Error in class (' . __CLASS__ . '): [ ' . 
                                                                            implode(', ', $insert_handle->errorInfo()) . 
                                                                            ' ] on line ' . __LINE__);

            $_data['option_id'] = \Cataleya\Helper\DBH::getInstance()->lastInsertId();
            

            $instance = self::load($_data['option_id']);
            
            
            
            
            foreach ($_Attributes as $_Attribute) 
            {
                
                
                if ($_Attribute instanceof \Cataleya\Catalog\Product\Attribute) $instance->addAttribute($_Attribute);
                else throw new Error ('WTF: [' . gettype($_Attribute) . '] in Option line ' . __LINE__);
                
            }

            if (\Cataleya\System::load()->generatedSKUEnabled()) $instance->generateSKU();
            
            
            \Cataleya\System\Event::notify('product/option/created', $instance, array ());
            return $instance;
            
	
	}
        

    /**
     * getEvents
     * 
     * @return array
     */
    public static function getEvents() {

        return self::$_events;
    }



        
        /**
         * 
         * @return \Cataleya\Catalog\Product\Attribute
         * 
         */
        public function current()
        {
            return \Cataleya\Catalog\Product\Attribute::load($this->_collection[$this->_position]['attribute_id']);

        }

        
        
        
        
        
	
	
        
        /**
         * 
         * @return \Cataleya\Catalog\Product
         * 
         */
        public function getProduct () 
        {
            return \Cataleya\Catalog\Product::load($this->_data['product_id']);
        }
        
        
        
        /**
         * 
         * @return integer
         * 
         */
        public function getProductId () 
        {
            return (int)$this->_data['product_id'];
        }
        
        
        
        

        
        /**
         *
         * getProductType 
         *
         * Returns instance Product Type
         * 
         * @return \Cataleya\Catalog\Product\Type
         *
         */
        public function getProductType()
        {
                return \Cataleya\Catalog\Product\Type::load($this->_data['product_type_id']);
        }
        



        
        
        /**
         *
         * getProductTypeID
         * 
         * Returns Product Type ID..
         *
         * @return integer
         *
         */
        public function getProductTypeID()
        {
               return $this->_data['product_type_id'];
        }
         






        /**
         * 
         * private loadAttributes
         * 
         */
        private function loadAttributes ()
        {
            
            
                
                $this->_collection = array ();
                
		static 
                        $select_handle, 
                        $select_handle_param_option_id;
		
		if (empty($select_handle))
		{
			$select_handle = $this->dbh->prepare('
                            SELECT b.attribute_id, b.attribute_type_id 
                            FROM options_to_attributes a 
                            INNER JOIN attributes b 
                            ON a.attribute_id = b.attribute_id 
                            INNER JOIN attribute_types c 
                            ON b.attribute_type_id = c.attribute_type_id 
                            WHERE a.option_id = :option_id 
                            ORDER BY c.sort_order
                        ');
			$select_handle->bindParam(':option_id', $select_handle_param_option_id, \PDO::PARAM_INT);
		}
		
		$select_handle_param_option_id = $this->_data['option_id'];
		
		if (!$select_handle->execute()) $this->e->triggerException('
										Error in class (' . __CLASS__ . '): [ ' . 
										implode(', ', $select_handle->errorInfo()) . 
										' ] on line ' . __LINE__);
                
                while ($row = $select_handle->fetch(\PDO::FETCH_ASSOC)) 
                {
                
                    $this->_collection[] = $row;
                }
                
                $this->pageSetup();
                
        }
        
        
        
        
        /**
         * 
         * 
         * @return \Cataleya\Catalog\Price
         * 
         */
        public function getPrice () 
        {
            if (empty($this->_price)) $this->_price = \Cataleya\Catalog\Price::load ($this->_data['price_id']);
            
            return $this->_price;
        }




        
        /**
         * 
         * @return \Cataleya\Catalog\Product\Stock
         * 
         */
        public function getStock () 
        {
            if (empty($this->_stock)) $this->_stock = \Cataleya\Catalog\Product\Stock::load ($this->_data['stock_id']);
            
            return $this->_stock;
        }


        
        
        
        
        
        
        


	/**
         * 
         * @param \Cataleya\Asset\Image $image
         * @return \Cataleya\Catalog\Product\Option
         * 
         */
	public function setPreviewImage (\Cataleya\Asset\Image $image) 
	{
		$this->_modified[] = 'image_id';
        $this->getPreviewImage();
                
        // set new image
		$this->_data['image_id'] = $image->getID();
                
        // Remove old image
		if ($this->_image->getID() !== 0) 
        {
            $this->_image->destroy();
        }
                
 		$this->_image = $image;
        $this->save();
		
		return $this;
		
	}


        
        
    /**
     * 
     * @return \Cataleya\Catalog\Product\Option
     * 
     */
	public function unsetPreviewImage () 
	{
                
        // Remove old image
		if ($this->_data['image_id'] == 0 || $this->_data['image_id'] === NULL) return $this;
        $this->getPreviewImage();
                
 		// $this->_modified[] = 'image_id';
        
        $this->_data['image_id'] = NULL;
        // $this->save();
        
        $this->_image->destroy();
        $this->_image = NULL;
		
		return $this;
		
	}



	/**
     * 
     * @return \Cataleya\Asset\Image
     * 
     */
	public function getPreviewImage () 
	{
		// if ($this->_data['image_id'] == 0 || $this->_data['image_id'] === NULL) return NULL;
                
		if (empty($this->_image)) $this->_image = \Cataleya\Asset\Image::load ($this->_data['image_id']);
                
        return $this->_image;
		
	}











        /*
	 *
	 * [SETTERS] AND [GETTERS]
	 * ________________________________________________________________
	 *
	 *
	 *
	 */
	 

	 



	/**
     * 
     * @param \Cataleya\Catalog\Product\Attribute $_Attribute
     * @return \Cataleya\Catalog\Product\Option
     * 
     */
	public function addAttribute (\Cataleya\Catalog\Product\Attribute $_Attribute)
	{

                
            
		if ($this->hasAttribute($_Attribute)) return $this;
                
        foreach ($this as $_OptionAttribute) {
            if ($_Attribute->isRelatedTo($_OptionAttribute)) $this->removeAttribute ($_OptionAttribute);
        }
        
    
    
        static 
                        $insert_handle;

        if (empty($insert_handle))
        {
                $insert_handle = $this->dbh->prepare('
                                                    INSERT INTO options_to_attributes (option_id, attribute_id)
                                                    VALUES (:option_id, :attribute_id)  
                                                    ');
        }

        $insert_handle->bindParam(':option_id', $insert_handle_param_option_id, \PDO::PARAM_INT);
        $insert_handle->bindParam(':attribute_id', $insert_handle_param_attribute_id, \PDO::PARAM_INT);
        
        $insert_handle_param_option_id = $this->getID();
        $insert_handle_param_attribute_id = $_Attribute->getID();

        if (!$insert_handle->execute()) $this->e->triggerException('
                                                                Error in class (' . __CLASS__ . '): [ ' . 
                                                                implode(', ', $insert_handle->errorInfo()) . 
                                                                ' ] on line ' . __LINE__);

        /*
        $this->_collection[] = array (
            'attribute_id' => $_Attribute->getID(), 
            'attribute_type_id' => $_Attribute->getAttributeTypeID() 
        );
         * 
         */
        
        $this->loadAttributes();
                
		return $this;

		
	}
	
        
        
        
        
        
	/**
     * 
     * @param \Cataleya\Catalog\Product\Attribute $_Attribute
     * @return boolean
     * 
     */
	public function hasAttribute (\Cataleya\Catalog\Product\Attribute $_Attribute) 
	{

        $_match = false;
        
        foreach ($this->_collection as $_attrib_data) {
            if ((int)$_Attribute->getID() === (int)$_attrib_data['attribute_id']) { 
                $_match = true;
                break;
            }
        }
                
		return $_match;
			
    }
        
        
        
        

        
	/**
     * 
     * @param \Cataleya\Catalog\Product\Attribute\Type $_AttributeType
     * @return boolean
     * 
     */
	public function hasAttributeType (\Cataleya\Catalog\Product\Attribute\Type $_AttributeType) 
	{
            
        $_match = false;
        
        foreach ($this->_collection as $_attrib_data) {
            if ((int)$_AttributeType->getID() === (int)$_attrib_data['attribute_type_id']) { 
                $_match = true;
                break;
            }
        }
                
		return $_match;
			
			
    }
        
        
        


        
   

        
        
        
        
        

	/**
     * 
     * @param \Cataleya\Catalog\Product\Attribute\Type $_AttributeType
     * @return \Cataleya\Catalog\Product\Attribute|NULL
     * 
     */
	public function getAttribute (\Cataleya\Catalog\Product\Attribute\Type $_AttributeType)
	{
            
        foreach ($this as $_Attribute) 
        {
            if ($_Attribute->isLike($_AttributeType)) return $_Attribute;
        }
        
        
        return NULL;
		
	}
	



        

        
        
        

	/**
     * 
     * @param \Cataleya\Catalog\Product\Attribute $_Attribute
     * @return \Cataleya\Catalog\Product\Option
     * 
     */
	public function removeAttribute (\Cataleya\Catalog\Product\Attribute $_Attribute)
	{
            
           if (!$this->hasAttribute($_Attribute)) return $this;
           
           
           
           
           
            static 
                    $delete_handle;


            if (empty($delete_handle)) $delete_handle = DBH::getInstance()->prepare('DELETE FROM options_to_attributes WHERE option_id = :option_id AND attribute_id = :attribute_id ');
            
            $delete_handle->bindParam(':option_id', $delete_handle_param_option_id, \PDO::PARAM_INT);
            $delete_handle->bindParam(':attribute_id', $delete_handle_param_attribute_id, \PDO::PARAM_INT);


            $delete_handle_param_option_id = $this->getID();
            $delete_handle_param_attribute_id = $_Attribute->getID();

            if (!$delete_handle->execute())  throw new Error ('DB Error: '.implode(', ', $delete_handle->errorInfo()));
            
            $this->loadAttributes();
            return $this;

		
	}
	
        
        
        
	 
	 
	/**
         * 
         * @param boolean $auto_save
         * 
         */
	 public function setAutosave ($auto_save = FALSE)
	 {
		 $this->_auto_save = $auto_save;
		 
	 }
	 
	
	

        
        
        
        

        
        
        /**
         * 
         * @return boolean
         * 
         */
	public function save () 
	{
		
            
                
                // Save data...
		if (empty($this->_modified)) return TRUE;
                
                $this->_modified = array_unique($this->_modified);
                
		$_update_params = array();
		$key_val_pairs = array();
		
		foreach ($this->_modified as $key) $key_val_pairs[$key] = $key . ' = :' . $key;
		
	
		// Do prepared statement for 'INSERT'...
		$update_handle = $this->dbh->prepare('
                                                        UPDATE product_options 
                                                        SET ' . implode (', ', $key_val_pairs) . '    
                                                        WHERE option_id = :instance_id 
                                                        ');
                
		$update_handle->bindParam(':instance_id', $_update_params['option_id'], \PDO::PARAM_INT);
                $_update_params['option_id'] = $this->_data['option_id'];
                
		foreach ($this->_modified as $key) {
                    
                        $_update_params[$key] = $this->_data[$key];                        
			$update_handle->bindParam(':'.$key, $_update_params[$key], \Cataleya\Helper\DBH::getTypeConst($_update_params[$key]));

		}
		

		
		if (!$update_handle->execute()) $this->e->triggerException('Error in class (' . __CLASS__ . '): [ ' . implode(', ', $update_handle->errorInfo()) . ' ] on line ' . __LINE__);
		  
		\Cataleya\System\Event::notify('product/option/updated', $this, array ('fields'=>$this->_modified));
        $this->_modified = array();
                
        return TRUE;
                
        }
	
	
       
 












        /*
	 *
	 * [ __DESTRUCT ]
	 * ________________________________________________________________
	 *
	 * 
	 *
	 */
	 

        
        
        
        
        

	/**
         * 
         * @return boolean
         * 
         */
	public function delete () 
	{


            
            // Delete rows containing 'option_id' in specified tables
            // \Cataleya\Helper\DBH::sanitize(array('cart_items', 'options_to_attributes'), 'option_id', $this->getID());
            if ((int)$this->_data['image_id'] > 0) \Cataleya\Helper\DBH::sanitize(array('images'), 'image_id', $this->_data['image_id']);

            // DELETE 
            $delete_handle = $this->dbh->prepare('DELETE FROM product_options WHERE option_id = :instance_id');

            $delete_handle_param_instance_id = $this->getID();
            $delete_handle->bindParam(':instance_id', $delete_handle_param_instance_id, \PDO::PARAM_INT);

            if (!$delete_handle->execute()) $this->e->triggerException('
                                                                            Error in class (' . __CLASS__ . '): [ ' . 
                                                                            implode(', ', $delete_handle->errorInfo()) . 
                                                                            ' ] on line ' . __LINE__);




            $this->_auto_save = FALSE;

            \Cataleya\System\Event::notify('product/option/deleted', NULL, array ('option_id'=>$this->getID()));
                                               
            return TRUE;
		
		
	}




	/**
         * 
         * @return boolean
         * 
         */
	public function destroy () 
	{
            return $this->delete();
            
        }        


        
        
	/**
         * 
         * @return integer
         * 
         */
	public function getID () 
	{
		
		
		return (int)$this->_data['option_id'];
			
		
		
	}
        
        
        
        
        
       
        
        /**
         * 
         * @return boolean
         * 
         */
        public function isTaxable()
        {
                return ((int)$this->_data['is_taxable'] === 1) ? TRUE : FALSE;
        }



        
            

        /**
         * 
         * @return \Cataleya\Catalog\Product\Option
         * 
         */
        public function makeTaxable () 
        {

            if ((int)$this->_data['is_taxable'] === 0) 
            {
                $this->_data['is_taxable'] = 1;
                $this->_modified[] = 'is_taxable';
            }

            return $this;
        }





        /**
         * 
         * @return \Cataleya\Catalog\Product\Option
         * 
         */
        public function makeNonTaxable () 
        {
            if ((int)$this->_data['is_taxable'] === 1) 
            {
                $this->_data['is_taxable'] = 0;
                $this->_modified[] = 'is_taxable';
            }

            return $this;
        }
        
        
        
  



        /**
         * 
         * @return string
         * 
         */
        public function getSKU($_raw = false)
        {
            $_raw_sku = $this->_data['sku'];

            return ($_raw === true) 
                ? $_raw_sku  
                : substr($_raw_sku, strrpos($_raw_sku, '_')+1);
        }



        /**
         * 
         * @param string $value Stock Keeping Unit (32 characters max)
         * @return \Cataleya\Catalog\Product\Option
         * 
         */
        public function setSKU($value = NULL)
        {

            $value = \Cataleya\Helper\Validator::referencecode($value, 1, 200);
            if (empty($value)) throw new Error ('SKU should only have letters, numbers and max length of 32 characters.');

            $_sku_prefix = '';

            if (empty($_p)) $_p = $this->getProduct();
            $_sku_prefix .= $_p->getManufacturer()->getCode() . '_';
            $_sku_prefix .= $_p->getSupplier()->getCode() . '_';


            $this->_data['sku'] = $_sku_prefix . strtoupper($value);
            $this->_modified[] = 'sku';

            return $this;
        }



        /**
         * 
         * @return \Cataleya\Catalog\Product\Option
         * 
         */
        public function generateSKU()
        {

            $_format = \Cataleya\System::load()->getSKUFormat();
            $_partials = explode('-', $_format);

            $_p = $this->getProduct();
            $_m_code = $_p->getManufacturer()->getCode();
            $_s_code = $_p->getSupplier()->getCode();

            $_sku = $_m_code.'_'.$_s_code.'_';

            for ($i=0,$l=count($_partials); $i<$l; $i++) 
            {
                switch ($_partials[$i]) 
                {
                case 'PT':
                    $_ProductType = $this->getProductType();
                    $_sku .= $_ProductType->getCode();
                    break;

                case 'SN':
                    $_sku .= \Cataleya\System::load()->getSKUSerialNumber();
                    break;
 
                case 'ID':
                    $_pid = str_pad($this->getProductId(), 2, 0, STR_PAD_LEFT);
                    $_oid = str_pad($this->getID(), 2, 0, STR_PAD_LEFT);
                    $_sku .= $_pid . $_oid;
                    break;
 
                case 'M':
                    $_sku .= $_m_code;
                    break;
                    
                case 'S':
                    $_sku .= $_s_code;
                    break;
                    
                case 'A':
                    foreach ($this as $attr) $_sku .= $attr->getCode();
                    break;
                }

            }

            $this->_data['sku'] = strtoupper($_sku);
            $this->_modified[] = 'sku';

            return $this;
        }






        /**
         * 
         * @return string International Article Number
         * 
         */
        public function getEAN()
        {
                return $this->_data['ean'];
        }



        /**
         * 
         * @param string $value International Article Number (13 characters max)
         * @return \Cataleya\Catalog\Product\Option
         * 
         */
        public function setEAN($value = NULL)
        {

                $value = \Cataleya\Helper\Validator::referencecode($value, 1, 200);
                if (empty($value)) $this->e->triggerException('Error in class (' . __CLASS__ . '): [ invalid argument for method: ' . __FUNCTION__ . ' ] on line ' . __LINE__);

                $this->_data['ean'] = strtoupper($value);
                $this->_modified[] = 'ean';

                return $this;
        }



        /**
         * 
         * @return string Universal Product Code
         * 
         */
        public function getUPC()
        {
                return $this->_data['upc'];
        }



        /**
         * 
         * @param string $value Universal Product Code. This should be 12 characters 
         * max but I've seen Shopify codes excede this so I'm leaving at 32 chars max. What can I say?
         * 
         * @return \Cataleya\Catalog\Product\Option
         * 
         */
        public function setUPC($value = NULL)
        {

                $value = \Cataleya\Helper\Validator::referencecode($value, 1, 200);
                if (empty($value)) $this->e->triggerException('Error in class (' . __CLASS__ . '): [ invalid argument for method: ' . __FUNCTION__ . ' ] on line ' . __LINE__);

                $this->_data['upc'] = strtoupper($value);
                $this->_modified[] = 'upc';

                return $this;
        }


  
        

        /**
         * 
         * @return string International Standard Book Number
         * 
         */
        public function getISBN()
        {
                return $this->_data['isbn'];
        }



        /**
         * 
         * @param string $value International Standard Book Number (45 characters max)
         * @return \Cataleya\Catalog\Product\Option
         * 
         */
        public function setISBN($value = NULL)
        {

                $value = \Cataleya\Helper\Validator::referencecode($value, 1, 200);
                if (empty($value)) $this->e->triggerException('Error in class (' . __CLASS__ . '): [ invalid argument for method: ' . __FUNCTION__ . ' ] on line ' . __LINE__);

                $this->_data['isbn'] = strtoupper($value);
                $this->_modified[] = 'isbn';

                return $this;
        }
        
        
        
        
        /**
         * 
         * @return string
         * 
         */
        public function getWeightUnit()
        {
                return $this->_data['weight_unit'];
        }
        
        
        
        
        /**
         * 
         * @param string $value lb, oz, kg, g
         * @return \Cataleya\Catalog\Product\Option
         * 
         */
        public function setWeightUnit($value = NULL)
        {
                static $_units = array ('kg', 'g', 'oz', 'lb');

                if (!is_string($value) || !in_array(strtolower($value), $_units)) $this->e->triggerException('Error in class (' . __CLASS__ . '): [ invalid argument for method: ' . __FUNCTION__ . ' ] on line ' . __LINE__);

                $this->_data['weight_unit'] = strtolower($value);
                $this->_modified[] = 'weight_unit';

                return $this;
        }
        
        
        
        /**
         * 
         * @param string $value Weight in 
         * @return \Cataleya\Catalog\Product\Option
         * 
         */
        public function setWeight($value = NULL)
        {
                $original_val = $value;
                
                $value = \Cataleya\Helper\Validator::float($value, 0, 1000032);
                if ($value === FALSE) $this->e->triggerException('Error in class (' . __CLASS__ . '): [ invalid argument for method: ' . __FUNCTION__ . ' ] on line ' . __LINE__ . ' v: ' . $original_val);

                switch ($this->getWeightUnit()) {
                    case 'g':
                        //do nothing..
                        break;
                    case 'kg':
                        $value = $value*1000;
                        break;
                    case 'oz':
                        $value = $value*28.3495;
                        break;
                    case 'lb':
                        $value = $value*453.592;
                        break;
                }
                
                
                $this->_data['grams'] = $value;
                $this->_modified[] = 'grams';

                return $this;
        }
        
        
        
        
        public function getWeight($_unit=null) 
        {
            
            if ((float)$this->_data['grams'] == 0) return 0;
            
            if ($_unit === null) $_unit = $this->getWeightUnit();
            
            $value = (float)$this->_data['grams'];
            
            switch ($_unit) {
                case 'g':
                    //do nothing..
                    break;
                case 'kg':
                    $value = $value/1000;
                    break;
                case 'oz':
                    $value = $value/28.3495;
                    break;
                case 'lb':
                    $value = $value/453.592;
                    break;
            }


            
            return $value;
        }









        /*_ _ _ _ _ _ _ _ _ _ _ _*/

}

