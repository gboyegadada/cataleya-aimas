<?php


namespace Cataleya\Catalog\Product;

use \Cataleya\Helper\DBH;
use \Cataleya\Error;


/*

CLASS PRODUCT TYPE

*/



class Type   
extends \Cataleya\Collection  
implements \Cataleya\System\Event\Observable 
{
	
	
	private $_data = array();
	private $_modified = array();
        
	private $dbh, $e, $_config;
	


    // To be used by [\Cataleya\Collection] to iterate privileges assigned to this role...
    protected $_collection = array ();
    protected $_position = 0;
    
            
    protected static $_events = [
            'catalog/product/type/created',
            'catalog/product/type/enabled',
            'catalog/product/type/disabled',
            'catalog/product/type/updated', 
            'catalog/product/type/deleted'
        ];


	/*
	 *
	 * [ __construct ]
	 * ________________________________________________________________
	 *
	 *
	 *
	 */
	 
	 
	public function __construct ()
	{
		
            
                parent::__construct();
                
		// Get database handle...
                $this->dbh = \Cataleya\Helper\DBH::getInstance();
		
		// Get error handler
		$this->e = \Cataleya\Helper\ErrorHandler::getInstance();
		
	}
	




	/*
	 *
	 * [ __destruct ]
	 * ________________________________________________________________
	 *
	 *
	 *
	 */

        public function __destruct()
        {
                    $this->saveData();
        }
	




	/**
         * 
         * @param integer $id
         * @return \Cataleya\Catalog\Product\Type
         * 
         */
	public static function load ($id = 0)
	{
		
                if ($id == 0 || filter_var($id, FILTER_VALIDATE_INT) == FALSE) return NULL;
                
		
		$instance = new \Cataleya\Catalog\Product\Type ();
		
		// LOAD DATA
		static $instance_select, $param_instance_id;
		
			
		if (!isset($instance_select)) {
			// PREPARE SELECT STATEMENT...
			$instance_select = $instance->dbh->prepare('SELECT * FROM product_types WHERE product_type_id = :instance_id LIMIT 1');
			$instance_select->bindParam(':instance_id', $param_instance_id, \PDO::PARAM_INT);
		}
		
		
		$param_instance_id = $id;
		
		if (!$instance_select->execute()) $instance->e->triggerException('
												Error in class (' . __CLASS__ . '): [ ' . 
												implode(', ', $instance_select->errorInfo()) . 
												' ] on line ' . __LINE__);
		
		$instance->_data = $instance_select->fetch(\PDO::FETCH_ASSOC);
                
                $instance->loadAttributeTypes ();


		return $instance;	
		
		
	}



	/**
         * 
         * @param array $_config e.g. array (
         *       'name' => 'string', 
         *       'name_plural' => 'string', 
         *       'code' => 'string', 
         *       'image_id' => \Cataleya\Asset\Image $_Image, 
         *       'has_ean' => 'string', 
         *       'has_upc' => 'string', 
         *       'has_sku' => 'string', 
         *       'has_isbn' => 'string'
         *   );
         * 
         * @return \Cataleya\Catalog\Product\Type
         * 
         */
	static public function create ($_config = array ()) 
	{
		 
            $_expected_keys = array (
                'name' => 'string', 
                'name_plural' => 'string', 
                'code' => 'string', 
                'image_id' => '\Cataleya\Asset\Image', 
                'has_ean' => 'string', 
                'has_upc' => 'string', 
                'has_sku' => 'string', 
                'has_isbn' => 'string'
            );


            $_code_src = (isset($_config['name']) && is_string($_config['name'])) ? $_config['name'] : 'item';

            $_code = $_tmp_code = \Cataleya\Helper::codify($_code_src, 2); $_num = 1;
            while (DBH::ping('product_types', 'code', $_tmp_code) === true) $_tmp_code = $_code . strval($_num++);

            $_code = $_tmp_code;

            
            
            $_default_params = array ( 
                'name' => 'Item', 
                'name_plural' => 'Items', 
                'image_id' => NULL, 
                'code' => $_code 
                );
            $_config = array_merge($_default_params, $_config);
            $_params = \Cataleya\Helper\DBH::buildParamsArray($_expected_keys, $_config);
            
            //$_params['params'] = array_merge($_default_params, $_params['params']);
            
            
            
            


            // Get database handle...
            $dbh = \Cataleya\Helper\DBH::getInstance();

            // Get error handler
            $e = \Cataleya\Helper\ErrorHandler::getInstance();

            // construct
            static $insert_handle;

            if (empty($insert_handle))
            {
                    $insert_handle = $dbh->prepare('
                                                    INSERT INTO product_types  
                                                    (' . implode(', ', $_params['columns']) . ') 
                                                    VALUES (' . implode(', ', $_params['placeholders']) . ')
                                                    ');
            }

            \Cataleya\Helper\DBH::bindParamsArray($insert_handle, $_params['params']);
            
            
            if (!$insert_handle->execute()) $e->triggerException('
                                                                            Error in class (' . __CLASS__ . '): [ ' . 
                                                                            implode(', ', $insert_handle->errorInfo()) . 
                                                                            ' ] on line ' . __LINE__);


            // AUTOLOAD NEW PRODUCT AND RETURN IT
            $instance = self::load($dbh->lastInsertId());

            \Cataleya\System\Event::notify('catalog/product/type/created', $instance, array ());
            return $instance;
		
		
	}

        
        


    /**
     * getEvents
     * 
     * @return array
     */
    public static function getEvents() {

        return self::$_events;
    }


        /*
         * 
         * 
         * [ loadAttributeTypes ]
         * ________________________________________________________________
         * 
         * Note: Internal method only...
         * 
         */
        
        private function loadAttributeTypes ()
        {
            
            
                $this->_collection = array ();
                
		static 
                        $select_handle, 
                        $select_handle_param_instance_id;
		
		if (empty($select_handle))
		{
			$select_handle = $this->dbh->prepare('
												SELECT attribute_type_id 
												FROM product_types_to_attribute_types 
												WHERE product_type_id = :instance_id 
                                                                                                ORDER BY sort_order
												');
			$select_handle->bindParam(':instance_id', $select_handle_param_instance_id, \PDO::PARAM_INT);
		}
		
		$select_handle_param_instance_id = $this->_data['product_type_id'];
		
		if (!$select_handle->execute()) $this->e->triggerException('
										Error in class (' . __CLASS__ . '): [ ' . 
										implode(', ', $select_handle->errorInfo()) . 
										' ] on line ' . __LINE__);
                
                
                while ($row = $select_handle->fetch(\PDO::FETCH_ASSOC)) $this->_collection[] = $row['attribute_type_id'];
                
                
                $this->pageSetup(200);
                
        }
        
        
        

        public function current()
        {
            return \Cataleya\Catalog\Product\Attribute\Type::load($this->_collection[$this->_position]);

        }

        
        
        
        
        
        

	 
	 /*
	 *  [ delete ]
	 * ________________________________________________________________
	 * 
	 * 
	 *
	 *
	 */

	 
	public function delete () 
	{
            
                        // 1st: We have to delete all associated [ Products ].
                        // Not sure how best to do this.
            
                        // Delete rows containing 'product_type_id' in specified tables
                        \Cataleya\Helper\DBH::sanitize(array('product_types_to_attribute_types'), 'product_type_id', $this->getID());
                        
		
			// DELETE 
			$instance_delete = $this->dbh->prepare('DELETE FROM product_types WHERE product_type_id = :instance_id');
			$instance_delete->bindParam(':instance_id', $this->_data['product_type_id'], \PDO::PARAM_INT);
			
	
			if (!$instance_delete->execute()) $this->e->triggerException('
											Error in class (' . __CLASS__ . '): [ ' . 
											implode(', ', $instance_delete->errorInfo()) . 
											' ] on line ' . __LINE__);
                        
        \Cataleya\System\Event::notify('catalog/product/type/created', null, array_merge($this->_data, ['id'=>$this->getID()]));
		return TRUE;
		
		
	}

        
        
        
        
        
        




        /*
         *
         * [ getID ] 
         *_____________________________________________________
         *
         *
         */

        public function getID()
        {
                return $this->_data['product_type_id'];
        }



        
        



        


        /*
         *
         * [ isActive ] 
         *_____________________________________________________
         *
         *
         */

        public function isActive()
        {
                return ((int)$this->_data['active'] === 1);
        }




        /*
         *
         * [ setActive ] 
         *_____________________________________________________
         *
         *
         */

        public function setActive($value)
        {

                if (filter_var($value, FILTER_VALIDATE_BOOLEAN, FILTER_NULL_ON_FAILURE) === NULL) return FALSE;




                $this->_data['active'] = ($value) ? 1 : 0;
                $this->_modified[] = 'active';

                return $this;
        }
        
        
        
        
        
        
        


        /*
         * 
         * [ enable ]
         * ______________________________________________________
         * 
         * 
         */


        public function enable () 
        {

            if ((int)$this->_data['active'] === 0) 
            {
                $this->_data['active'] = 1;
                $this->_modified[] = 'active';
            }

            \Cataleya\System\Event::notify('catalog/product/type/enabled', $this, []);
            return $this;
        }





        /*
         * 
         * [ disable ]
         * ______________________________________________________
         * 
         * 
         */


        public function disable () 
        {
            if ((int)$this->_data['active'] === 1) 
            {
                $this->_data['active'] = 0;
                $this->_modified[] = 'active';
            }

            \Cataleya\System\Event::notify('catalog/product/type/disabled', $this, []);
            return $this;
        }
        
        
        
        
        
        
        

        /**
         *
         * Singular name of product type e.g. Dress, Tee, DVD
         * 
         * @return string
         */
        public function getName()
        {
                return $this->_data['name'];
        }
        
        
        
        /**
         *
         * Plural name of product type e.g. Dresses, Tees, DVDs
         * 
         * @return string
         */
        public function getNamePlural()
        {
                return $this->_data['name_plural'];
        }
        
        
        
        

        

        /**
         * 
         * Returns short code to help identify product type in ShopFront templates
         * and so on.
         * 
         * @return string
         */
        public function getCode()
        {
                return $this->_data['code'];
        }
        
        
        


        /*
         *
         * [ getIcon ] 
         *_____________________________________________________
         *
         *
         */

        public function getIcon()
        {
                return $this->_data['icon'];
        }

        
                /*
         *
         * [ getIconHref ] 
         *_____________________________________________________
         *
         *
         */

        public function getIconHref()
        {
                return BASE_URL . 'ui/images/ui/product-type-icons/' . $this->getIcon();
        }


        /*
         *
         * [ setIcon ] 
         *_____________________________________________________
         *
         *
         */

        public function setIcon($value = NULL)
        {
                if (!\Cataleya\Helper\Validator::isFilename($value)) $this->e->triggerException('Error in class (' . __CLASS__ . '): [ invalid argument for method: ' . __FUNCTION__ . ' ] on line ' . __LINE__);


                $this->_data['icon'] = $value;
                $this->_modified[] = 'icon';

                return $this;
        }

        
        



        /*
         *
         * [ setName ] 
         *_____________________________________________________
         *
         *
         */

        public function setName($value = NULL)
        {
                if ($value===NULL || !is_string($value)) return FALSE;

                $this->_data['name'] = $value;
                $this->_modified[] = 'name';

                return $this;
        }



        
        
        
        /*
         *
         * [ setNamePlural ] 
         *_____________________________________________________
         *
         *
         */

        public function setNamePlural($value = NULL)
        {
                if ($value===NULL || !is_string($value)) return FALSE;

                $this->_data['name_plural'] = $value;
                $this->_modified[] = 'name_plural';

                return $this;
        }
        
        
        

        /**
         * 
         * @param string $value
         * @return \Cataleya\Catalog\Product\Type
         * 
         */
        public function setCode($value = NULL)
        {
            
                $value = \Cataleya\Helper\Validator::token($value, 1, 160);
                
                if ($value === FALSE) $this->e->triggerException('Error in class (' . __CLASS__ . '): [ invalid argument for method: ' . __FUNCTION__ . ' ] on line ' . __LINE__);


                $this->_data['code'] = $value;
                $this->_modified[] = 'code';

                return $this;
        }

        
        
        
        
        
        
        



	/**
         * 
         * @param \Cataleya\Catalog\Product\Attribute\Type $_AttributeType
         * @return \Cataleya\Catalog\Product\Type
         * 
         */
	public function addAttributeType (\Cataleya\Catalog\Product\Attribute\Type $_AttributeType)
	{

                
            
		if ($this->hasAttributeType($_AttributeType)) return $this;
			
			
                static 
                                $insert_handle;

                if (empty($insert_handle))
                {
                        $insert_handle = $this->dbh->prepare('
                                                            INSERT INTO product_types_to_attribute_types (product_type_id, attribute_type_id)
                                                            VALUES (:product_type_id, :attribute_type_id)  
                                                            ');
                }

                $insert_handle->bindParam(':product_type_id', $insert_handle_param_product_type_id, \PDO::PARAM_INT);
                $insert_handle->bindParam(':attribute_type_id', $insert_handle_param_attribute_type_id, \PDO::PARAM_INT);
                
                $insert_handle_param_product_type_id = $this->getID();
                $insert_handle_param_attribute_type_id = $_AttributeType->getID();

                if (!$insert_handle->execute()) $this->e->triggerException('
                                                                        Error in class (' . __CLASS__ . '): [ ' . 
                                                                        implode(', ', $insert_handle->errorInfo()) . 
                                                                        ' ] on line ' . __LINE__);

                
                $this->_collection[] = $insert_handle_param_attribute_type_id;
                

		$this->pageSetup();
        
        
        \Cataleya\System\Event::notify('catalog/product/type/updated', $this, []);
		return $this;

		
	}
	
        
        
        
        
        
        

	/*
	 *
	 * [ REMOVE ATTRIBUTE TYPE ]
	 * ________________________________________________________________
	 *
	 *
	 *
	 *
	 */
	 
	 
	public function removeAttributeType (\Cataleya\Catalog\Product\Attribute\Type $_AttributeType)
	{
            
           if (!$this->hasAttributeType($_AttributeType)) return $this;
           
           
            static 
                    $delete_handle;


            if (empty($delete_handle)) $delete_handle = $this->dbh->prepare('DELETE FROM product_types_to_attribute_types WHERE product_type_id = :product_type_id AND attribute_type_id = :attribute_type_id ');
            
            $delete_handle->bindParam(':product_type_id', $delete_handle_param_product_type_id, \PDO::PARAM_INT);
            $delete_handle->bindParam(':attribute_id', $delete_handle_param_attribute_type_id, \PDO::PARAM_INT);


            $delete_handle_param_product_type_id = $this->getID();
            $delete_handle_param_attribute_type_id = $_AttributeType->getID();

            if (!$delete_handle->execute())  $this->e->triggerException('
                                                                         Error in class (' . __CLASS__ . '): [ ' . 
                                                                                                        implode(', ', $delete_handle->errorInfo()) . 
                                                                                                        ' ] on line ' . __LINE__);
            
            $this->loadAttributeTypes();
            
            \Cataleya\System\Event::notify('catalog/product/type/updated', $this, []);
            return $this;

		
	}
	
        
        
        
        
        
        
        
        
        
        
        
        
	/*
	 *
	 *  [ hasAttributeType ]
	 * ________________________________________________________________
	 * 
	 * 
	 *
	 *
	 */
	 
	public function hasAttributeType (\Cataleya\Catalog\Product\Attribute\Type $_AttributeType) 
	{

		return (in_array ($_AttributeType->getID(), $this->_collection));
			
			
        }
        
        
        
        
        
        
        
        
        
        
        

        
        
        
        /* ================= SUPPORTED CODES ==================== */
        
        
        
        
        /**
         *
         * @param void
         * @return boolean
         *
         */
        public function hasUPC()
        {
                return ((int)$this->_data['has_upc'] === 1);
        }



        /**
         *
         * @param void
         * @return \Cataleya\System $this
         *
         */
        public function enableUPC()
        {

                $this->_data['has_upc'] = TRUE;
                $this->_modified[] = 'has_upc';

                return $this;
        }
        
        
        
        

        /**
         *
         * @param void
         * @return \Cataleya\System $this
         *
         */
        public function disableUPC()
        {

                $this->_data['has_upc'] = FALSE;
                $this->_modified[] = 'has_upc';

                return $this;
        }
        
        
        
        
        
        

        
        /**
         *
         * @param void
         * @return boolean
         *
         */
        public function hasEAN()
        {
                return ((int)$this->_data['has_ean'] === 1);
        }



        /**
         *
         * @param void
         * @return \Cataleya\System $this
         *
         */
        public function enableEAN()
        {

                $this->_data['has_ean'] = TRUE;
                $this->_modified[] = 'has_ean';

                return $this;
        }
        
        
        
        

        /**
         *
         * @param void
         * @return \Cataleya\System $this
         *
         */
        public function disableEAN()
        {

                $this->_data['has_ean'] = FALSE;
                $this->_modified[] = 'has_ean';

                return $this;
        }
        
        
        
        
        
        

        
        /**
         *
         * @param void
         * @return boolean
         *
         */
        public function hasISBN()
        {
                return ((int)$this->_data['has_isbn'] === 1);
        }



        /**
         *
         * @param void
         * @return \Cataleya\System $this
         *
         */
        public function enableISBN()
        {

                $this->_data['has_isbn'] = TRUE;
                $this->_modified[] = 'has_isbn';

                return $this;
        }
        
        
        
        

        /**
         *
         * @param void
         * @return \Cataleya\System $this
         *
         */
        public function disableISBN()
        {

                $this->_data['has_isbn'] = FALSE;
                $this->_modified[] = 'has_isbn';

                return $this;
        }
        
        
        
        
        

        
        /**
         *
         * @param void
         * @return boolean
         *
         */
        public function hasSKU()
        {
                return ((int)$this->_data['has_sku'] === 1);
        }



        /**
         *
         * @param void
         * @return \Cataleya\System $this
         *
         */
        public function enableSKU()
        {

                $this->_data['has_sku'] = TRUE;
                $this->_modified[] = 'has_sku';

                return $this;
        }
        
        
        
        

        /**
         *
         * @param void
         * @return \Cataleya\System $this
         *
         */
        public function disableSKU()
        {

                $this->_data['has_sku'] = FALSE;
                $this->_modified[] = 'has_sku';

                return $this;
        }
        
        
        
        
        
        
        
        
        
        
        
        

        
       
        
        




        
	
	
	public function saveData () 
	{
		

		if (empty($this->_modified)) return;
		$_update_params = array();
		$key_val_pairs = array();
		
		foreach ($this->_modified as $key) $key_val_pairs[$key] = $key . ' = :' . $key;
		
	
		// Do prepared statement for 'INSERT'...
		$update_handle = $this->dbh->prepare('
                                                        UPDATE product_types 
                                                        SET ' . implode (', ', $key_val_pairs) . '    
                                                        WHERE product_type_id = :instance_id 
                                                        ');
                
		$update_handle->bindParam(':instance_id', $_update_params['product_type_id'], \PDO::PARAM_INT);
                $_update_params['product_type_id'] = $this->_data['product_type_id'];
                
		foreach ($this->_modified as $key) {
                    
                        $_update_params[$key] = $this->_data[$key];                        
			$update_handle->bindParam(':'.$key, $_update_params[$key], \Cataleya\Helper\DBH::getTypeConst($_update_params[$key]));

		}

		
		if (!$update_handle->execute()) $this->e->triggerException('Error in class (' . __CLASS__ . '): [ ' . implode(', ', $update_handle->errorInfo()) . ' ] on line ' . __LINE__);
		
        
        \Cataleya\System\Event::notify('catalog/product/type/updated', $this, [ ]);
        
        $this->_modified = array();
	}
	
	
       
 
 
        
        
        
        
        
        
        

        
        
        



        





}
	
	
	
	
