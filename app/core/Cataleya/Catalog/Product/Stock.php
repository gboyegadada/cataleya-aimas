<?php


namespace Cataleya\Catalog\Product;




/*
 *
 *	@Package: Cataleya
 *	@Class: \Cataleya\Catalog\Product\Stock
 *
 *	(c) 2012 Fancy Paper Planes
 *
 *
 *
 */


class Stock implements \Cataleya\Asset, \Cataleya\System\Event\Observable  

{


	private $_values = array();
	private $_data = array();

	private $dbh, $e;
				
				
        
    protected static $_events = [
            'catalog/product/stock/out',
            'catalog/product/stock/low', 
            'catalog/product/stock/updated'
        ];


	/*
	 *
	 * [ __construct ]
	 * ________________________________________________________________
	 * 
	 * 
	 *
	 *
	 *
	 */
	 
	private function __construct () 
	{
		
		// Get database handle...
                 $this->dbh = \Cataleya\Helper\DBH::getInstance();
		
		// Get error handler
		$this->e = \Cataleya\Helper\ErrorHandler::getInstance();
		
		// initialize
		
		
	}



	/*
	 *
	 * [ __destruct ]
	 * ________________________________________________________________
	 * 
	 * 
	 *
	 *
	 *
	 */
	 
	public function __destruct () 
	{
		
		
		
		
		
	}




	/*
	 *
	 *  [ Attribute ] interface method: [ load ]
	 * ________________________________________________________________
	 * 
	 * 
	 *
	 *
	 *
	 */
	 
	static public function load ($id = 0) 
	{
		
		if ($id === 0 || filter_var($id, FILTER_VALIDATE_INT) === FALSE) return NULL;
		
		
		///// CREATE AN INSTANCE OF [\Cataleya\Catalog\Product\Stock] /////////////////////////
		$stock = new \Cataleya\Catalog\Product\Stock();


		///// LOAD STOCK /////////////////////////
		static $select_handle, $select_handle_param_stock_id;
		
		if (empty($select_handle))
		{
		$select_handle = $stock->dbh->prepare('SELECT * FROM product_stock WHERE stock_id = :stock_id LIMIT 1');
		$select_handle->bindParam(':stock_id', $select_handle_param_stock_id, \PDO::PARAM_INT);
		}
		
		$select_handle_param_stock_id = $id;
		
		if (!$select_handle->execute()) $stock->e->triggerException('
										Error in class (' . __CLASS__ . '): [ ' . 
										implode(', ', $select_handle->errorInfo()) . 
										' ] on line ' . __LINE__);
                $stock->_data = $select_handle->fetch(\PDO::FETCH_ASSOC);
		if (empty($stock->_data)) 
                {
                    unset($stock);
                    return NULL;
                }
		
		
		

		///// LOAD STOCK QUANTITIES /////////////////////////
		static $select_handle2, $select_handle2_param_stock_id;
		
		if (empty($select_handle2))
		{
			$select_handle2 = $stock->dbh->prepare('
												SELECT * 
												FROM product_stock_quantity 
												WHERE stock_id = :stock_id 
												LIMIT 20 
												');
			$select_handle2->bindParam(':stock_id', $select_handle2_param_stock_id, \PDO::PARAM_INT);
		}
		
		$select_handle2_param_stock_id = $id;
		
		if (!$select_handle2->execute()) $stock->e->triggerException('
										Error in class (' . __CLASS__ . '): [ ' . 
										implode(', ', $select_handle2->errorInfo()) . 
										' ] on line ' . __LINE__);
										
		while ($stock_row = $select_handle2->fetch(\PDO::FETCH_ASSOC)) 
		{
			$stock->_values[$stock_row['store_id']] = $stock_row['quantity'];
		} 
		
		
		return $stock;
			
		
		
	}





	/*
	 *
	 *  [ Attribute ] interface method: [ create ]
	 * ________________________________________________________________
	 * 
	 * 
	 *
	 *
	 *
	 */
	 
	static public function create ($void = NULL) 
	{
				
		///// CREATE AN INSTANCE OF [\Cataleya\Catalog\Product\Stock] /////////////////////////
		$stock = new \Cataleya\Catalog\Product\Stock();
		
		
		
		///// DO NEW INSERT /////////////////////////
		static $stock_insert;
		
		if (empty($stock_insert))
		{
			$stock_insert = $stock->dbh->prepare('INSERT INTO product_stock (stock_id) VALUES (NULL)');
		}
		
		
		if (!$stock_insert->execute()) $stock->e->triggerException('
										Error in class (' . __CLASS__ . '): [ ' . 
										implode(', ', $stock_insert->errorInfo()) . 
										' ] on line ' . __LINE__);
										
		
		$stock->_data['stock_id'] = $stock->dbh->lastInsertId();
		
		
		return $stock;
		
	}






    /**
     * getEvents
     * 
     * @return array
     */
    public static function getEvents() {

        return self::$_events;
    }



	/*
	 *
	 *  [ Attribute ] interface method: [ getID ]
	 * ________________________________________________________________
	 * 
	 * 
	 *
	 *
	 *
	 */
	 
	public function getID () 
	{
		
		
		return $this->_data['stock_id'];
			
		
		
	}






	/*
	 *
	 *  [ Attribute ] interface method: [ getClassName ]
	 * ________________________________________________________________
	 * 
	 * 
	 *
	 *
	 *
	 */
	 
	public function getClassName () 
	{
		
		
		return __CLASS__;
			
		
		
	}



	/*
	 *
	 *  [ getValue ]
	 * ________________________________________________________________
	 * 
	 *
	 *
	 *
	 */
	 
	public function getValue ($_Store = NULL) 
	{
		if ($_Store instanceof \Cataleya\Store) $store_id = $_Store->getStoreId ();
                else if (is_numeric($_Store)) $store_id = $_Store;
                
		if (isset($store_id) && isset($this->_values[$store_id])) return $this->_values[$store_id];
		else return 0;
		
	}




	/*
	 *
	 *  [ removeQuantity ]
	 * ________________________________________________________________
	 * 
	 * 
	 *
	 *
	 *
	 */
	 
	public function removeQuantity (\Cataleya\Store $_Store, $value = NULL) 
	{
                $value = filter_var($value, FILTER_VALIDATE_FLOAT, FILTER_NULL_ON_FAILURE);
                if ($value===NULL || $value < 0) $this->e->triggerException('Error in class (' . __CLASS__ . '): [ invalid argument for method: ' . __FUNCTION__ . ' ] on line ' . __LINE__);
                
                $store_id = $_Store->getID();
                
		$_qty = (empty($this->_values[$store_id])) ? 0 : (float)$this->_values[$store_id];
                
                if ($value > $_qty) $this->e->triggerException('Error in class (' . __CLASS__ . '): [ Unable to update stock quantity because stock is low or out in: ' . __FUNCTION__ . ' ] on line ' . __LINE__);
                
                
                else $_new_qty = $_qty - $value ;
				
		
               return $this->setValue($_Store, $_new_qty);

        }
        
        
        
        
        
	/*
	 *
	 *  [ addQuantity ]
	 * ________________________________________________________________
	 * 
	 * 
	 *
	 *
	 *
	 */
	 
	public function addQuantity (\Cataleya\Store $_Store, $value = NULL) 
	{
                $value = filter_var($value, FILTER_VALIDATE_FLOAT, FILTER_NULL_ON_FAILURE);
                if ($value===NULL || $value < 0) $this->e->triggerException('Error in class (' . __CLASS__ . '): [ invalid argument for method: ' . __FUNCTION__ . ' ] on line ' . __LINE__);
                
                $store_id = $_Store->getID();
                
		$_qty = (empty($this->_values[$store_id])) ? 0 : (float)$this->_values[$store_id];
                
                $_new_qty = $_qty + $value ;
				
		
               return $this->setValue($_Store, $_new_qty);

        }
        
        
        
        
        
        
        

	/*
	 *
	 *  [ setValue ]
	 * ________________________________________________________________
	 * 
	 * 
	 *
	 *
	 *
	 */
	 
	public function setValue (\Cataleya\Store $_Store, $value = NULL) 
	{
        $value = filter_var($value, FILTER_VALIDATE_FLOAT, FILTER_NULL_ON_FAILURE);
        if ($value===NULL) $this->e->triggerException('Error in class (' . __CLASS__ . '): [ invalid argument for method: ' . __FUNCTION__ . ' ] on line ' . __LINE__);

        $old_value = $this->getValue($_Store);

		// construct
		
		static 
				$stock_insert, 
				$stock_insert_param_stock_id, 
				$stock_insert_param_id, 
				$stock_insert_param_qty;
		
		if (empty($stock_insert))
		{
			$stock_insert = $this->dbh->prepare('
                                                            INSERT INTO product_stock_quantity (stock_id, store_id, quantity) 
                                                            VALUES (:stock_id, :store_id, :quantity) 
                                                            ON DUPLICATE KEY UPDATE quantity = :quantity 
                                                            ');
			$stock_insert->bindParam(':stock_id', $stock_insert_param_stock_id, \PDO::PARAM_INT);
			$stock_insert->bindParam(':store_id', $stock_insert_param_id, \PDO::PARAM_INT);
			$stock_insert->bindParam(':quantity', $stock_insert_param_qty, \PDO::PARAM_STR);
		}
		
		$stock_insert_param_stock_id = $this->_data['stock_id'];
		$stock_insert_param_id = $_Store->getStoreId();
		$stock_insert_param_qty = $value;
		
		if (!$stock_insert->execute()) $this->e->triggerException('
										Error in class (' . __CLASS__ . '): [ ' . 
										implode(', ', $stock_insert->errorInfo()) . 
										' ] on line ' . __LINE__);
										
		
		$this->_values[$stock_insert_param_id] = $value;
		
        \Cataleya\System\Event::notify('catalog/product/stock/updated', $this, [ 
            'id' => $this->_data['stock_id'], 
            'store_id' => $_Store->getStoreId(), 
            'old_quantity' => (float)$old_value, 
            'new_quantity' => (float)$value 
                ]);
		
		return $this;
		
	}




        
        
	/*
	 *
	 *  [ update ]
	 * ________________________________________________________________
	 * 
	 * increment or decrease stock
	 *
	 *
	 *
	 */
	 
	public function update (\Cataleya\Store $_Store, $value = NULL) 
	{
		if (($value !== '-' && $value !== '+')) return FALSE;
                
                $store_id = $_Store->getID();
                
                
		if (empty($this->_values[$store_id])) 
                {
                    return FALSE;
                }
                
                else if ((int)$this->_values[$store_id]['quantity'] < 1 && $value === '-') 
                {
                    return FALSE;
                }
                
                else
                {
                    $new_qty = ($value === '-') ? (int)$this->_values[$store_id]['quantity'] - 1 : (int)$this->_values[$store_id]['quantity'] + 1;
                }                
				
		
               return $this->setValue($_Store, $new_qty);
		
	}


        
        


	/*
	 *
	 *  [ delete ]
	 * ________________________________________________________________
	 * 
	 * 
	 *
	 *
	 *
	 */
	 
	public function delete () 
	{


                        
                        
			// 2. Delete stock...
            
			$entry_delete = $this->dbh->prepare('DELETE FROM product_stock WHERE stock_id = :id');
			$entry_delete->bindParam(':id', $entry_delete_param_id, \PDO::PARAM_INT);
			
			$entry_delete_param_id = $this->_data['stock_id'];
	
			if (!$entry_delete->execute()) $this->e->triggerException('
											Error in class (' . __CLASS__ . '): [ ' . 
											implode(', ', $entry_delete->errorInfo()) . 
											' ] on line ' . __LINE__);
                        
                        
                        
            return TRUE;
		
		
	}




	/*
	 *
	 *  [ destroy ]: Alias for [ delete ]
	 * ________________________________________________________________
	 * 
	 * 
	 *
	 *
	 *
	 */
	 
	public function destroy () 
	{
            return $this->delete();
            
        }        




}

