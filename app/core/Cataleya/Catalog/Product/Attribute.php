<?php


namespace Cataleya\Catalog\Product;

use \Cataleya\Helper\DBH;

/*

CLASS ATTRIBUTE

*/



class Attribute   
extends \Cataleya\Data 
{
	
	
	private $_data = array();
	private $_modified = array();
        
	private $dbh, $e, $_config, $_AttributeType;
	

	
        const TABLE = 'attributes';
        const PK = 'attribute_id';


	/*
	 *
	 * [ __construct ]
	 * ________________________________________________________________
	 *
	 *
	 *
	 */
	 
	 
	private function __construct ()
	{
		
		// Get database handle...
                $this->dbh =DBH::getInstance();
		
		// Get error handler
		$this->e = \Cataleya\Helper\ErrorHandler::getInstance();
		
	}
	




	/*
	 *
	 * [ __destruct ]
	 * ________________________________________________________________
	 *
	 *
	 *
	 */

    public function __destruct()
    {
		$this->saveData();
    }
	




	/*
	 *
	 * [ load ]
	 * ________________________________________________________________
	 *
	 *
	 *
	 */
	 
	 
	public static function load ($id = 0)
	{
		
        if ($id == 0 || filter_var($id, FILTER_VALIDATE_INT) == FALSE) return NULL;
                
		
		$instance = new \Cataleya\Catalog\Product\Attribute ();
		
		// LOAD DATA
		static $instance_select, $param_instance_id;
		
			
		if (!isset($instance_select)) {
			// PREPARE SELECT STATEMENT...
			$instance_select = $instance->dbh->prepare('SELECT * FROM attributes WHERE attribute_id = :instance_id LIMIT 1');
			$instance_select->bindParam(':instance_id', $param_instance_id, \PDO::PARAM_INT);
		}
		
		
		$param_instance_id = $id;
		
		if (!$instance_select->execute()) $instance->e->triggerException('
												Error in class (' . __CLASS__ . '): [ ' . 
												implode(', ', $instance_select->errorInfo()) . 
												' ] on line ' . __LINE__);
		
		$instance->_data = $instance_select->fetch(\PDO::FETCH_ASSOC);
                
               


		return $instance;	
		
		
	}



	/*
	 *
	 *  [ create ]
	 * ________________________________________________________________
	 * 
	 * 
	 *
	 *
	 */

	 
	static public function create (\Cataleya\Catalog\Product\Attribute\Type $_AttributeType, $value = "") 
	{
		 

        $value = strval($value);
        $_code_src = (!empty($value)) ? $value : 'item';

        $_code = $_tmp_code = \Cataleya\Helper::codify($_code_src, 2); $_num = 1;
        while (DBH::ping('attributes', 'code', $_tmp_code) === true) $_tmp_code = $_code . strval($_num++);

        $_code = $_tmp_code;



		// Get database handle...
                $dbh =DBH::getInstance();
		
		// Get error handler
		$e = \Cataleya\Helper\ErrorHandler::getInstance();
		
		// construct
		static 
				$instance_insert,  
                                $instance_insert_param_attribute_type_id, 
                                $instance_insert_param_value, 
                                $instance_insert_param_code;
		
		if (empty($instance_insert))
		{
			$instance_insert = $dbh->prepare('
                                                        INSERT INTO attributes  
                                                        (attribute_type_id, value, code) 
                                                        VALUES (:attribute_type_id, :value, :code)
                                                        ');
															
			$instance_insert->bindParam(':value', $instance_insert_param_value, \PDO::PARAM_STR);
			$instance_insert->bindParam(':code', $instance_insert_param_code, \PDO::PARAM_STR);
            $instance_insert->bindParam(':attribute_type_id', $instance_insert_param_attribute_type_id, \PDO::PARAM_INT);
		}
		
        $instance_insert_param_value = (!empty($value)) ? $value : 'no value'; 
        $instance_insert_param_code = $_code; 
        $instance_insert_param_attribute_type_id = $_AttributeType->getID(); 

		if (!$instance_insert->execute()) $e->triggerException('
										Error in class (' . __CLASS__ . '): [ ' . 
										implode(', ', $instance_insert->errorInfo()) . 
										' ] on line ' . __LINE__);
										
		
		// AUTOLOAD NEW PRODUCT AND RETURN IT
		$instance = self::load($dbh->lastInsertId());
		
		return $instance;
		
		
	}


	 
	 /*
	 *  [ delete ]
	 * ________________________________________________________________
	 * 
	 * 
	 *
	 *
	 */

	 
	public function delete () 
	{
		
            
                        // Remove associated cart items
                        $instance_delete = $this->dbh->prepare('
                            DELETE FROM product_options a 
                            WHERE a.option_id 
                            IN (
                            SELECT option_id 
                            FROM options_to_attributes b 
                            WHERE b.attribute_id = :instance_id )
                            ');
                        

                        $instance_delete->bindParam(':instance_id', $this->_data['attribute_id'], \PDO::PARAM_INT);
                        
			if (!$instance_delete->execute()) $this->e->triggerException('
											Error in class (' . __CLASS__ . '): [ ' . 
											implode(', ', $instance_delete->errorInfo()) . 
											' ] on line ' . __LINE__);
                        
                        
                        // Delete rows containing attribute_id in specified tables
                        //DBH::sanitize(array('options_to_attributes'), 'attribute_id', $this->getID());
                        
                    
                        
			// DELETE 
			$instance_delete = $this->dbh->prepare('DELETE FROM attributes WHERE attribute_id = :instance_id');
			$instance_delete->bindParam(':instance_id', $this->_data['attribute_id'], \PDO::PARAM_INT);
			
	
			if (!$instance_delete->execute()) $this->e->triggerException('
											Error in class (' . __CLASS__ . '): [ ' . 
											implode(', ', $instance_delete->errorInfo()) . 
											' ] on line ' . __LINE__);
                        
		return TRUE;
		
		
	}





        /*
         *
         * [ getID ] 
         *_____________________________________________________
         *
         *
         */

        public function getID()
        {
                return $this->_data['attribute_id'];
        }



        
        



        /**
         * 
         * Get Code: Returns short code to help identify attribute type in ShopFront templates
         * and so on.
         * 
         * @return string
         */
        public function getCode()
        {
            if (empty($this->_data['code']) && !empty($this->_data['value'])) $this->generateCode();

            return $this->_data['code'];
        }
        
        
        

        

        /**
         * 
         * Generate Code: Generates short code to help identify attributes in ShopFront templates
         * and so on.
         * 
         * @return string
         */
        public function generateCode()
        {
            $value = strval($this->getValue());
            $_code_src = (!empty($value)) ? $value : 'item';

            $_code = $_tmp_code = \Cataleya\Helper::codify($_code_src, 2); $_num = 1;
            while (DBH::ping('attributes', 'code', $_tmp_code) === true) $_tmp_code = $_code . strval($_num++);

            $_code = $_tmp_code;


            $this->_data['code'] = $_code;
            $this->_modified[] = 'code';

            return $this;
        }
        
        
        
        
        
        
        
        
        
        

        /*
         *
         * [ getValue ] 
         *_____________________________________________________
         *
         *
         */

        public function getValue()
        {
                return $this->_data['value'];
        }
        
        
        



        /*
         *
         * [ setValue ] 
         *_____________________________________________________
         *
         *
         */

        public function setValue($value = NULL)
        {
                if ($value===NULL || (!is_string($value) && !is_numeric($value))) return $this;

                $this->_data['value'] = strval($value);
                $this->_modified[] = 'value';
                $this->generateCode();

                return $this;
        }



        
        
       
        
        
        /*
         *
         * getAttributeType 
         *
         * @return: \Cataleya\Catalog\Product\Attribute\Type
         *
         */

        public function getAttributeType()
        {
                if (!($this->_AttributeType instanceof \Cataleya\Catalog\Product\Attribute\Type)) $this->_AttributeType = \Cataleya\Catalog\Product\Attribute\Type::load($this->_data['attribute_type_id']);
                return $this->_AttributeType;
        }
        
        
        

        
        
        /*
         *
         * [ getAttributeTypeID ] 
         *_____________________________________________________
         *
         *
         */

        public function getAttributeTypeID()
        {
                return $this->_data['attribute_type_id'];
        }
        

        
        
        /*
         *
         * [ isLike ] 
         *_____________________________________________________
         *
         * Check if [Attribute] is part of [AttributeType]
         *
         */

        public function isLike(\Cataleya\Catalog\Product\Attribute\Type $_AttributeType)
        {
                return ((int)$_AttributeType->getID() === (int)$this->_data['attribute_type_id']) ? TRUE : FALSE;
        }
        
        
        

        
        /*
         *
         * [ isRelatedTo ] 
         *_____________________________________________________
         *
         * Check if [Attribute] is part of [Attribute]
         *
         */

        public function isRelatedTo(\Cataleya\Catalog\Product\Attribute $_Attribute)
        {
                return ((int)$_Attribute->getAttributeTypeID() === (int)$this->_data['attribute_type_id']) ? TRUE : FALSE;
        }

        
	
        
        
        
        
        
        

        
        
        



        





}
	
	
	
	
