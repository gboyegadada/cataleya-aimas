<?php


namespace Cataleya\Catalog\Product\Attribute;



/*

CLASS ATTRIBUTE TYPE

*/



class Type  
extends \Cataleya\Collection   
{
	
	
	private $_data = array();
	private $_modified = array();
        
	private $dbh, $e, $_config, $_ProductType;
	

        // To be used by [\Cataleya\Collection] to iterate privileges assigned to this role...
        protected $_collection = array ();
        protected $_position = 0;
	


	/*
	 *
	 * [ __construct ]
	 * ________________________________________________________________
	 *
	 *
	 *
	 */
	 
	 
	public function __construct ()
	{
		
                parent::__construct();
                
		// Get database handle...
                $this->dbh = \Cataleya\Helper\DBH::getInstance();
		
		// Get error handler
		$this->e = \Cataleya\Helper\ErrorHandler::getInstance();
		
	}
	




	/*
	 *
	 * [ __destruct ]
	 * ________________________________________________________________
	 *
	 *
	 *
	 */

    public function __destruct()
    {
		$this->saveData();
    }
	




	/**
         * 
         * @param integer $id
         * @return \Cataleya\Catalog\Product\Attribute\Type
         * 
         */
	public static function load ($id = 0)
	{
		
                if ($id == 0 || filter_var($id, FILTER_VALIDATE_INT) == FALSE) return NULL;
                
		
		$instance = new \Cataleya\Catalog\Product\Attribute\Type ();
		
		// LOAD DATA
		static $instance_select, $param_instance_id;
		
			
		if (!isset($instance_select)) {
			// PREPARE SELECT STATEMENT...
			$instance_select = $instance->dbh->prepare('SELECT * FROM attribute_types WHERE attribute_type_id = :instance_id LIMIT 1');
			$instance_select->bindParam(':instance_id', $param_instance_id, \PDO::PARAM_INT);
		}
		
		
		$param_instance_id = $id;
		
		if (!$instance_select->execute()) $instance->e->triggerException('
												Error in class (' . __CLASS__ . '): [ ' . 
												implode(', ', $instance_select->errorInfo()) . 
												' ] on line ' . __LINE__);
		
		$instance->_data = $instance_select->fetch(\PDO::FETCH_ASSOC);
                
               
                $instance->loadAttributes();

		return $instance;	
		
		
	}



	/**
         * 
         * @param array $_config e.g. array (
         *       'name' => 'string', 
         *       'name_plural' => 'string', 
         *       'code' => 'string', 
         *       'sort_order' => 'integer'
         *   );
         * 
         * @return \Cataleya\Catalog\Product\Attribute
         * 
         */
	static public function create (array $_config = array ()) 
	{
		 

            
            
 
            $_expected_keys = array (
                'name' => 'string', 
                'name_plural' => 'string', 
                'code' => 'string', 
                'sort_order' => 'integer'
            );
            
            
            $_default_params = array ( 
                'name' => 'Item', 
                'name_plural' => 'Items',
                'code' => NULL
                        );
            
            $_config = array_merge($_default_params, $_config);
            $_params = \Cataleya\Helper\DBH::buildParamsArray($_expected_keys, $_config);
            
            // $_params['params'] = array_merge($_default_params, $_params['params']);
            

            // Get database handle...
            $dbh = \Cataleya\Helper\DBH::getInstance();

            // Get error handler
            $e = \Cataleya\Helper\ErrorHandler::getInstance();

            // construct
            static $insert_handle;

            if (empty($insert_handle))
            {
                    $insert_handle = $dbh->prepare('
                                                    INSERT INTO attribute_types  
                                                    (name, name_plural, sort_order) 
                                                    VALUES (:name, :name_plural, :sort_order)
                                                    ');
            }
            

            \Cataleya\Helper\DBH::bindParamsArray($insert_handle, $_params['params']);
            

            if (!$insert_handle->execute()) $e->triggerException('
                                                                    Error in class (' . __CLASS__ . '): [ ' . 
                                                                    implode(', ', $insert_handle->errorInfo()) . 
                                                                    ' ] on line ' . __LINE__);


            // AUTOLOAD NEW PRODUCT AND RETURN IT
            $instance = self::load($dbh->lastInsertId());

            return $instance;
		
		
	}
        
        
        
        
        

        /*
         * 
         * 
         * [ loadAttributes ]
         * ________________________________________________________________
         * 
         * Note: Internal method only...
         * 
         */
        
        private function loadAttributes ()
        {
            
                $this->_collection = array ();
                
		static 
                        $select_handle, 
                        $select_handle_param_instance_id;
		
		if (empty($select_handle))
		{
			$select_handle = $this->dbh->prepare('
												SELECT * 
												FROM attributes 
												WHERE attribute_type_id = :instance_id 
                                                                                                ORDER BY sort_order
												');
			$select_handle->bindParam(':instance_id', $select_handle_param_instance_id, \PDO::PARAM_INT);
		}
		
		$select_handle_param_instance_id = $this->_data['attribute_type_id'];
		
		if (!$select_handle->execute()) $this->e->triggerException('
										Error in class (' . __CLASS__ . '): [ ' . 
										implode(', ', $select_handle->errorInfo()) . 
										' ] on line ' . __LINE__);
                
                while ($row = $select_handle->fetch(\PDO::FETCH_ASSOC)) 
                {
                
                    $this->_collection[] = $row['attribute_id'];
                }
                
                $this->pageSetup();
                
        }
        


	 
	 /*
	 *  [ delete ]
	 * ________________________________________________________________
	 * 
	 * 
	 *
	 *
	 */

	 
	public function delete () 
	{
		
                        // Delete Attributes
                        foreach ($this as $_Attribute) $_Attribute->delete ();
            
                        // Delete rows containing 'attribute_type_id' in specified tables
                        \Cataleya\Helper\DBH::sanitize(array('product_types_to_attribute_types'), 'attribute_type_id', $this->getID());
                        
			// DELETE 
			$instance_delete = $this->dbh->prepare('DELETE FROM attribute_types WHERE attribute_type_id = :instance_id');
			$instance_delete->bindParam(':instance_id', $this->_data['attribute_type_id'], \PDO::PARAM_INT);
			
	
			if (!$instance_delete->execute()) $this->e->triggerException('
											Error in class (' . __CLASS__ . '): [ ' . 
											implode(', ', $instance_delete->errorInfo()) . 
											' ] on line ' . __LINE__);
                        
		return TRUE;
		
		
	}





        /*
         *
         * [ getID ] 
         *_____________________________________________________
         *
         *
         */

        public function getID()
        {
                return $this->_data['attribute_type_id'];
        }


        /**
         * 
         * @return \Cataleya\Catalog\Product\Attribute
         * 
         */
        public function current()
        {
            return \Cataleya\Catalog\Product\Attribute::load($this->_collection[$this->_position]);

        }

        
        
        



        
        
        
        

        
        
        
        
        
        
        
        

        /**
         * 
         * @return integer
         * 
         */
        public function getSortOrder()
        {
                return (int)$this->_data['sort_order'];
        }




        /**
         * 
         * @param integer $value
         * @return \Cataleya\Catalog\Product\Attribute\Type
         * 
         */
        public function setSortOrder($value)
        {

                $value = filter_var($value, FILTER_VALIDATE_INT, FILTER_NULL_ON_FAILURE);
                
                if ($value === NULL) $this->e->triggerException('Error in class (' . __CLASS__ . '): [ invalid argument for method: ' . __FUNCTION__ . ' ] on line ' . __LINE__);




                $this->_data['sort_order'] = (int)$value;
                $this->_modified[] = 'sort_order';

                return $this;
        }
        
        
        
        
        

        /**
         * 
         * @return string
         * 
         */
        public function getName()
        {
                return $this->_data['name'];
        }
        
        
        
        /**
         * 
         * @return string
         * 
         */
        public function getNamePlural()
        {
                return $this->_data['name_plural'];
        }
        
        
        


        /**
         * 
         * Get Code: Returns short code to help identify attribute type in ShopFront templates
         * and so on.
         * 
         * @return string
         */
        public function getCode()
        {
                return $this->_data['code'];
        }
        
        
        
        
        
        
        
        /**
         * 
         * @return \Cataleya\Catalog\Product\Type
         * 
         */
        public function getProductType()
        {
                if (!($this->_ProductType instanceof \Cataleya\Catalog\Product\Type)) $this->_ProductType = \Cataleya\Catalog\Product\Type::load($this->_data['product_type_id']);
                return $this->_ProductType;
        }
        
        



        /**
         * 
         * Set Name: Set Attribute Type Name/Label e.g. 'Color', 'Size'.
         * 
         * @param string $value
         * @return \Cataleya\Catalog\Product\Attribute\Type
         */
        public function setName($value = NULL)
        {
                $value = \Cataleya\Helper\Validator::name($value, 1, 250);
                
                if ($value === FALSE) $this->e->triggerException('Error in class (' . __CLASS__ . '): [ invalid argument for method: ' . __FUNCTION__ . ' ] on line ' . __LINE__);


                $this->_data['name'] = $value;
                $this->_modified[] = 'name';

                return $this;
        }



        
        
        
        /**
         * 
         * Set Plural Name:Set Attribute Type plural name/label e.g. 'Colors', 'Sizes'.
         * 
         * @param string $value
         * @return \Cataleya\Catalog\Product\Attribute\Type
         * 
         */
        public function setNamePlural($value = NULL)
        {
                $value = \Cataleya\Helper\Validator::name($value, 1, 250);
                
                if ($value === FALSE) $this->e->triggerException('Error in class (' . __CLASS__ . '): [ invalid argument for method: ' . __FUNCTION__ . ' ] on line ' . __LINE__);


                $this->_data['name_plural'] = $value;
                $this->_modified[] = 'name_plural';

                return $this;
        }
        
        
        
        

        /**
         * 
         * @param string $value
         * @return \Cataleya\Catalog\Product\Type
         * 
         */
        public function setCode($value = NULL)
        {
            
                $value = \Cataleya\Helper\Validator::token($value, 1, 160);
                
                if ($value === FALSE) $this->e->triggerException('Error in class (' . __CLASS__ . '): [ invalid argument for method: ' . __FUNCTION__ . ' ] on line ' . __LINE__);


                $this->_data['code'] = $value;
                $this->_modified[] = 'code';

                return $this;
        }

        




        
	
	
	public function saveData () 
	{
		

		if (empty($this->_modified)) return;
		$_update_params = array();
		$key_val_pairs = array();
		
		foreach ($this->_modified as $key) $key_val_pairs[$key] = $key . ' = :' . $key;
		
	
		// Do prepared statement for 'INSERT'...
		$update_handle = $this->dbh->prepare('
                                                        UPDATE attribute_types 
                                                        SET ' . implode (', ', $key_val_pairs) . '    
                                                        WHERE attribute_type_id = :instance_id 
                                                        ');
                
		$update_handle->bindParam(':instance_id', $_update_params['attribute_type_id'], \PDO::PARAM_INT);
                $_update_params['attribute_type_id'] = $this->_data['attribute_type_id'];
                
		foreach ($this->_modified as $key) {
                    
                        $_update_params[$key] = $this->_data[$key];                        
			$update_handle->bindParam(':'.$key, $_update_params[$key], \Cataleya\Helper\DBH::getTypeConst($_update_params[$key]));

		}

		
		if (!$update_handle->execute()) $this->e->triggerException('Error in class (' . __CLASS__ . '): [ ' . implode(', ', $update_handle->errorInfo()) . ' ] on line ' . __LINE__);
		$this->_modified = array ();
	}
	
	
       
 
 
        
        
        
        
        
        
        

        
        
        



        





}
	
	
	
	
