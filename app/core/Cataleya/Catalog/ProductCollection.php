<?php


namespace Cataleya\Catalog;



/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Collection
 *
 * @author Gboyega Dada for Fancy Paper Planes.
 */
abstract class ProductCollection 
extends \Cataleya\Collection {
    
   

    // To be used by [\Cataleya\Collection]
    protected $_collection = array ();
    protected $_collection_ids = array ();
    protected $_has_collections = false;
    protected $_position = 0;
    protected $_population = 0;
    
    protected $_collection_id = 0;
    
    protected $_page_offset = 0;
    protected $_prev_page_offset = 0;


    protected $_tables = array();
    protected $_filter_statements = array();



    // CONST VALUES 1 - 15 RESERVED FOR [ _Abstract_Collection ]
    const ORDER_BY_TITLE = 16;
    const ORDER_BY_VIEWS = 17;
    const ORDER_BY_SALES = 18;
    const ORDER_BY_PRICE = 19;
    const ORDER_BY_ID = 20;
    
    // FILTERS
    const FILTER_ON_SALE = 21;
    const FILTER_NOT_ON_SALE = 22;
    const FILTER_ALL = 23;
    
    
    
    
    
    
    protected static function _initCollection() {

            
            // construct
            $dbh = \Cataleya\Helper\DBH::getInstance();
            $error = \Cataleya\Helper\ErrorHandler::getInstance();
            
            static $insert_handle;

            if (empty($insert_handle))
            {
                    $insert_handle = $dbh->prepare(''
                            . 'INSERT INTO collections (count) '
                            . 'VALUES (0)'
                            . '');
            }

            
            
            if (!$insert_handle->execute()) $error->triggerException('
                                                                            Error in class (' . __CLASS__ . '): [ ' . 
                                                                            implode(', ', $insert_handle->errorInfo()) . 
                                                                            ' ] on line ' . __LINE__);
            

            return $dbh->lastInsertId();
    }
    
    
    
    
    /**
     * 
     * @param array $filters
     * @param array $sort
     * @param int $page_size
     * @param int $page_num
     * @return \Cataleya\Catalog\ProductCollection
     * 
     */
    protected function _loadCollection(
                $filters = array(), 
                $sort = array(
                    'order_by'=>'', 
                    'order'=>'', 
                    'index'=>''
                    ), 
                $page_size = 16, 
                $last_seen = 0, 
                $page_num = 1
                ) 
        {


        
            // Check if Collection is initialized...
            // if ($this->hasCollection() && NULL === $this->getCollectionID()) $this->_initCollection ();



            


            
            
            if (is_array($sort) && isset($sort['order_by'], $sort['order']) )
             {
                 // ORDER BY
                 switch ($sort['order_by']) {

                     case self::ORDER_BY_TITLE:
                         $param_order_by = 'dc.title';
                         $_sort_key = 'title';
                         break;

                     case self::ORDER_BY_SORT:
                         if ($this->_has_collections || $this->hasCollection()) { 
                             
                            $param_order_by = 'c.sort_order';
                            $_sort_key = 'sort_order';
                            
                            break; 
                         }
                         

                     case self::ORDER_BY_CREATED:
                         $param_order_by = 'p.date_added';
                         $_sort_key = 'date_added';
                         break;

                     case self::ORDER_BY_MODIFIED:
                         $param_order_by = 'p.last_modified';
                         $_sort_key = 'last_modified';
                         break;

                     case self::ORDER_BY_VIEWS:
                         $param_order_by = 'p.views';
                         $_sort_key = 'views';
                         break;    

                     case self::ORDER_BY_SALES:
                         $param_order_by = 'p.total_sold';
                         $_sort_key = 'total_sold';
                         break; 

    //                 case self::ORDER_BY_PRICE:
    //                     $param_order_by = 'prices.value';
    //                     break;                        

                     default :
                         $param_order_by = 'dc.title';
                         $_sort_key = 'title';
                         break;
                 }


                 // ASC | DESC
                 switch ($sort['order']) {

                     case self::ORDER_ASC:
                         $_sort_order = SORT_ASC;
                         break;

                     case self::ORDER_DESC:
                         $_sort_order = SORT_DESC;
                         break;

                     default :
                         $_sort_order = SORT_ASC;
                         break;
                 }                    


             }

             // Defaults                
             else {

                     $_sort_order = SORT_ASC;
                     $_sort_key = 'title';
                     $param_order_by = 'dc.title';

             }     


            $_show_featured = (
                    isset($filters['featured'])) 
                    ? 'AND p.featured' : '';

            $_show_hidden = (
                    isset($sort['show_hidden']) 
                    && $sort['show_hidden'] === TRUE
                    ) 
                    ? '' : 'AND p.display_status';

            
            $_filter_statements = implode(' AND ', $this->buildFilterQuery($filters));

            if (!empty($_filter_statements)) {

                $_filter_statements = ' AND ' . $_filter_statements;
            }
            
            
            if ($this->hasCollection()) {

                $_filter_statements = ' AND c.collection_id=' . $this->getCollectionID(). $_filter_statements;
            }

            // Check if index is specified
            if (is_array($sort) && isset($sort['index']) && is_string($sort['index']))
            {
                $param_regex = preg_match('/^[a-zA-Z]{1}$/', $sort['index']) 
                               ? '^' . strtolower($sort['index']) 
                               : '.*';

            }

            else {
                $param_regex = '.*';
            }



            $param_limit;
            $param_cursor;
            $_index_with = 'dc.title';
            

            
            
            // Last seen...
            $_cursor_sql = ($last_seen !== 0) 
                    ? 'p.product_id ' . (($page_num < 0) ? '>=' : '<') . ' :cursor AND ' 
                    : '';
            
            
            
            // We need page number to be UNSIGNED from here on...
            if ($page_num < 0) { $page_num *= (-1); }
            
            // ...and ZERO based from here on...
            $page_num = ($page_num > 0) ? $page_num-1 : 0;
            
            
            
            
            if ($this->_has_collections || $this->hasCollection()):

            // PREPARE SELECT STATEMENT...
                
            $_qstr = '
                    SELECT p.product_id, ' . $param_order_by . '     
                    FROM products p 
                    INNER JOIN content_languages dc 
                    ON p.description_id = dc.content_id 
                    INNER JOIN products_to_collections c 
                    ON p.product_id = c.product_id 
                    WHERE ' 
                    . $_cursor_sql 
                    . $_index_with . ' REGEXP :regex   
                    ' . $_show_hidden . '   
                    ' . $_show_featured . ' 
                    ' . $_filter_statements . '  
                    ORDER BY p.product_id DESC' 
                    . ' LIMIT :limit';
            
            else:

            // PREPARE SELECT STATEMENT...
                
            $_qstr = '
                    SELECT p.product_id, ' . $param_order_by . '  
                    FROM products p 
                    INNER JOIN content_languages dc 
                    ON p.description_id = dc.content_id 
                    WHERE ' 
                    . $_cursor_sql 
                    . $_index_with . ' REGEXP :regex   
                    ' . $_show_hidden . '   
                    ' . $_show_featured . ' 
                    ' . $_filter_statements . '  
                    GROUP BY p.product_id  
                    ORDER BY p.product_id DESC' 
                    . ' LIMIT :limit';
            endif;



            $select_handle = $this->dbh->prepare($_qstr);
            $select_handle->bindParam(':regex', $param_regex, \PDO::PARAM_STR);
            
            $select_handle->bindParam(':limit', $param_limit, \PDO::PARAM_INT);
            
            
            // Last seen...
            if ($last_seen !== 0) {
                $select_handle->bindParam(':cursor', $param_cursor, \PDO::PARAM_INT);
                $param_cursor = $last_seen;
            }
            
            $param_limit = $page_size + ($page_num * $page_size);
            


            if (!$select_handle->execute()) { 
                
                
                
                $this->e->triggerException('
                                        Error in class (' . __CLASS__ . '): [ ' . 
                                        implode(', ', $select_handle->errorInfo()) . 
                                        ' ] on line ' . __LINE__);
                
            }

            
            
                
            $_sort_array = array ();
            
            while ($row = $select_handle->fetch(\PDO::FETCH_ASSOC)) 
            {
                $this->_collection[] = $row; // $_load_product_data ? $row : $row['product_id'];
                $_sort_array[] = $row[$_sort_key];
            }
            
            if ($param_limit > $page_size) { 
                $this->_collection = array_slice($this->_collection, ($page_num*$page_size)); // array_values(array_unique($this->_collection));
                $_sort_array = array_slice($_sort_array, ($page_num*$page_size));
            }
            
            $_row_count = count($this->_collection);
            $this->_page_offset = ((int)$page_size === $_row_count) ? $this->_collection[$_row_count-1]['product_id'] : $last_seen;
            
            
            
            array_multisort($_sort_array, $_sort_order, SORT_STRING, $this->_collection);
            
            $_sorted_rows = $this->_collection;
            $this->_collection = array();
            foreach ($_sorted_rows as $v) {
               $this->_collection[] = $v['product_id'];
            }
            
            
            
            $this->_collection_ids = array_values(array_unique($this->_collection_ids));
            
            
            $this->pageSetup($page_size, 1);

            return $this;


    }
    
    
    
    
    
    
    
    
    




    

    
    
    

    /**
     * 
     * @return boolean
     * 
     */
    public function hasCollection()
    {
            return (property_exists($this, '_data') && is_array($this->_data) && array_key_exists('collection_id', $this->_data));
    }  
    
    

    /**
     * 
     * @return int Collection ID
     * 
     */
    public function getCollectionID()
    {
            return $this->hasCollection() ? $this->_data['collection_id'] : NULL;
    }  

    
    

    public function getPageOffset() {
        return $this->_page_offset;
    }
    
    
    private function buildFilterQuery($objects = array()) 
    {
        
        
        $_statements = array();
        
        
        $_classes = array(
            
            'Cataleya\Store' => array(
                                    'table' => 'products_to_collections', 
                                    'col' => 't.store_id', 
                                    'keys' => array(), 
                                    'sql' => 'p.product_id IN (
                                            SELECT DISTINCT product_id 
                                            FROM products_to_collections pc 
                                            INNER JOIN tags t 
                                            ON pc.collection_id = t.collection_id 
                                            WHERE {{id}}
                                            ) '
                                ), 
            
            'Cataleya\Catalog\Tag' => array(
                                    'table' => 'products_to_collections', 
                                    'col' => 'collection_id', 
                                    'keys' => array(), 
                                    'sql' => 'p.product_id IN (
                                            SELECT DISTINCT product_id 
                                            FROM products_to_collections 
                                            WHERE {{id}}
                                            ) '
                                ), 
            
            'Cataleya\Sales\Sale' => array(
                                    'table' => 'products_to_collections', 
                                    'col' => 'collection_id', 
                                    'keys' => array(), 
                                    'sql' => 'p.product_id IN (
                                            SELECT DISTINCT product_id 
                                            FROM products_to_collections 
                                            WHERE sale_id = {{id}}
                                            ) '
                                 ), 
            
            'Cataleya\Sales\Coupon' => array(
                                'table' => 'products_to_collections', 
                                'col' => 'collection_id', 
                                'keys' => array(), 
                                'sql' => 'p.product_id IN (
                                            SELECT DISTINCT product_id 
                                            FROM products_to_collections
                                            WHERE {{id}}
                                            ) '
                                 ), 
            
            'Cataleya\Catalog\Product\Attribute' => array(
                                'table' => 'products_to_attributes', 
                                'col' => 'attribute_id', 
                                'keys' => array(), 
                                'do_not_update' => true, 
                                'sql' => 'p.product_id IN (
                                            SELECT DISTINCT product_id 
                                            FROM product_options po
                                            INNER JOIN options_to_attributes oa
                                            ON po.option_id = oa.option_id 
                                            WHERE {{id}}
                                            ) '
                                ), 
            
            'Cataleya\Catalog\PriceRange' => array(
                                'table' => 'price_values', 
                                'col' => 'store_id', 
                                'keys' => array(), 
                                'params' => array(),
                                'do_not_update' => true, 
                                'sql' => 'p.product_id IN (
                                            SELECT DISTINCT product_id 
                                            FROM product_options po
                                            INNER JOIN price_values pv
                                            ON pv.price_id = po.price_id 
                                            WHERE pv.value BETWEEN {{min}} AND {{max}} 
                                            AND pv.store_id = {{id}}
                                            )'
                                )
            
        );
        
        
        foreach ($objects as $obj) {
            
            if (!is_object($obj) || !array_key_exists(get_class($obj), $_classes)) continue;
            $_classname = get_class($obj);
            
            $_is_collection = $obj instanceof \Cataleya\Catalog\ProductCollection;
            
            $_id = $_is_collection ? $obj->getCollectionID() : $obj->getID();
            $this->_has_collections = $_is_collection; // ($obj instanceof \Cataleya\Store) ? true : $_is_collection;
            
            $_classes[$_classname]['keys'][] = $_id;
            
            // multiple instances will not be allowed for price ranges.
            if ($_classname == '\Cataleya\Catalog\PriceRange') {
                $_classes[$_classname]['params'] = array (
                    'id' => $_id, 
                    'min' => $obj->getMin(), 
                    'max' => $obj->getMax()+1
                        );
            }
            
        }
        
        
        
        
        
   
        foreach ($_classes as $k => $_class) {
            
                    if (empty($_class['keys'])) continue;
                    $_where = array();

                    
                    foreach ($_class['keys'] as $_id) {
                        $_where[] = $_class['col'] . ' = ' . $_id;
                    }
                    
                    if ($k == '\Cataleya\Catalog\PriceRange') {
                        
                        $_statements[$k] = $_class['sql'];
                        foreach ($_class['params'] as $param => $v) 
                        {
                        $_statements[$k] = str_replace(
                                                "{{" . $param . "}}", 
                                                $v, 
                                                $_statements[$k]
                                                );
                        }

                    } else {
                    
                        $_where = implode(' OR ', $_where);
                        $_statements[$k] = str_replace(
                                                "{{id}}", 
                                                $_where, 
                                                $_class['sql']
                                                );
                    }
                    $this->_tables[$k] = $_class;
        }
        
        
        $this->_filter_statements = $_statements;
        
        
        return $this->_filter_statements;
        
    }



    


    /**
     *
     * 
     * @return \Cataleya\Catalog\Product (object)
     *
     *
     */
    public function current()
    {
        return \Cataleya\Catalog\Product::load($this->_collection[$this->_position]);
        
    }


    



    /**
     * 
     * @param \Cataleya\Catalog\Product $_product
     * @return \Cataleya\Catalog\ProductCollection
     * 
     */
    public function addProduct (\Cataleya\Catalog\Product $_product, $_add_to_all=FALSE) 
    {

            

            if (!$this->hasCollection()) { return $this; }
            
            $_Store = $this->getStore();
            $_product_already_in_store = $_Store->hasProduct($_product);
        
            $_add_to_all = FALSE; // I don't like the idea of UPDATING MULTIPLE collections at once!

            static  $insert_handle
            ,       $insert_handle_param_instance_id
            ,       $insert_handle_param_product_id;
            
            
            if (empty($insert_handle)):
                
            // INSERT: product into collection...
            $insert_handle = $this->dbh->prepare('
                                                INSERT IGNORE INTO products_to_collections (collection_id, product_id, date_added, last_modified)
                                                VALUES (:instance_id, :product_id, NOW(), NOW())  
                                                ');


            $insert_handle->bindParam(':instance_id', $insert_handle_param_instance_id, \PDO::PARAM_INT);
            $insert_handle->bindParam(':product_id', $insert_handle_param_product_id, \PDO::PARAM_INT);
            
            
            endif;
            
            $insert_handle_param_product_id = $_product->getID();
            $insert_handle_param_instance_id = $this->getCollectionID();
            
            if (!$insert_handle->execute()) $this->e->triggerException('
                                                                    Error in class (' . __CLASS__ . '): [ ' . 
                                                                    implode(', ', $insert_handle->errorInfo()) . 
                                                                    ' ] on line ' . __LINE__);
            
            $_row_count = $insert_handle->rowCount();
            $this->_updateCounter($_row_count);

            if (!$_product_already_in_store) { $_Store->updateProductCount($_row_count); }

            /*
            //// For multiple Stores, Sales, Coupons e.t.c ///
            $this->dbh->beginTransaction();
            
            if ($_add_to_all===TRUE):
            foreach ($this->_collection_ids as $key) {
                $insert_handle_param_instance_id = $key;

                if (!$insert_handle->execute()) $this->e->triggerException('
                                                                        Error in class (' . __CLASS__ . '): [ ' . 
                                                                        implode(', ', $insert_handle->errorInfo()) . 
                                                                        ' ] on line ' . __LINE__);
                $this->_updateCounter(1, $key);
                
            }
            else:
                $insert_handle_param_instance_id = $this->getCollectionID();
                if (!$insert_handle->execute()) $this->e->triggerException('
                                                                        Error in class (' . __CLASS__ . '): [ ' . 
                                                                        implode(', ', $insert_handle->errorInfo()) . 
                                                                        ' ] on line ' . __LINE__);
                $this->_updateCounter(1);
                
                if (!$_product_already_in_store) { $_Store->updateProductCount(1); }
                
                // Sneak an insert into ROOT CATEGORY OF STORE
                //if (!$this->isRoot()) {
                //    $this->getStore()->getRootCategory()->addProduct($_product);
                //}
            endif;
            
            $this->dbh->commit();
            */
            
            

            $this->_collection[] = $insert_handle_param_product_id;


            return $this;

    }









    /**
     * 
     * @param \Cataleya\Catalog\Product $_product
     * @return \Cataleya\Catalog\ProductCollection
     * 
     */
    public function removeProduct (\Cataleya\Catalog\Product $_product, $_remove_from_all=FALSE) 
    {


        if (!$this->hasCollection()) { return $this; }

        
        $_remove_from_all = FALSE; // I don't like the idea of UPDATING MULTIPLE collections at once!
            
        static  $delete_handle
        ,       $delete_handle_param_instance_id
        ,       $delete_handle_param_product_id;


        if (empty($delete_handle)):
                
        $delete_handle = $this->dbh->prepare('
                                            DELETE FROM products_to_collections   
                                            WHERE product_id = :product_id 
                                            AND collection_id = :instance_id'
                );


        $delete_handle->bindParam(':instance_id', $delete_handle_param_instance_id, \PDO::PARAM_INT);
        $delete_handle->bindParam(':product_id', $delete_handle_param_product_id, \PDO::PARAM_INT);

        endif;
        
        
        $delete_handle_param_instance_id = $this->getCollectionID();
        $delete_handle_param_product_id = $_product->getID();

        if (!$delete_handle->execute()) $this->e->triggerException('
                                                                Error in class (' . __CLASS__ . '): [ ' . 
                                                                implode(', ', $delete_handle->errorInfo()) . 
                                                                ' ] on line ' . __LINE__);
        
        
        $_row_count = $delete_handle->rowCount();
        
        if ($_row_count > 0) {
            $this->_updateCounter(-1);

            $_Store = $this->getStore();

            if (!$_Store->hasProduct($_product)) { $_Store->updateProductCount(-1); }
        
        }
        
        
        //
        //// For multiple Stores, Sales, Coupons e.t.c ///
        /*
        $this->dbh->beginTransaction();
        
        if ($_remove_from_all===TRUE):
        foreach ($this->_collection_ids as $key) {
            $delete_handle_param_instance_id = $key;

            if (!$delete_handle->execute()) $this->e->triggerException('
                                                                    Error in class (' . __CLASS__ . '): [ ' . 
                                                                    implode(', ', $delete_handle->errorInfo()) . 
                                                                    ' ] on line ' . __LINE__);
            $this->_updateCounter(-1, $key);

        }
        else:
            $delete_handle_param_instance_id = $this->getCollectionID();
            if (!$delete_handle->execute()) $this->e->triggerException('
                                                                    Error in class (' . __CLASS__ . '): [ ' . 
                                                                    implode(', ', $delete_handle->errorInfo()) . 
                                                                    ' ] on line ' . __LINE__);
            $this->_updateCounter(-1);
        
            $_Store = $this->getStore();

            //if (!$_Store->hasProduct($_product)) { $_Store->updateProductCount(-1); }
        endif;
        $this->dbh->commit();
         
         */
        
        
        
        
        foreach ($this->_collection as $key=>$val)
        {
            if ((int)$val === (int)$delete_handle_param_product_id) 
            {
                unset ($this->_collection[$key]);
                break;
            }
        }

        // } 

        return $this;

    }
    
    
    
    /**
     * 
     * @param int $_increment
     * @param int $_collection_id
     * @return \Cataleya\Catalog\ProductCollection
     * 
     */
    private function _updateCounter ($_increment=0, $_collection_id=NULL) 
    {

        if (!$this->hasCollection()) { return $this; }
        
        static  $update_handle
        ,       $update_handle_param_instance_id
        ,       $update_handle_param_increment;


        if (empty($update_handle)):
        // UPDATE: collection counter...
        $update_handle = $this->dbh->prepare('
                                            UPDATE collections 
                                            SET count = count + (:increment)  
                                            WHERE collection_id = :instance_id 
                                            ');

        $update_handle->bindParam(':instance_id', $update_handle_param_instance_id, \PDO::PARAM_INT);
        $update_handle->bindParam(':increment', $update_handle_param_increment, \PDO::PARAM_INT);
        endif;
        
        
        
        $update_handle_param_instance_id = ($_collection_id === NULL) ? $this->getCollectionID() : $_collection_id;
        $update_handle_param_increment = $_increment;


        if (!$update_handle->execute()) $this->e->triggerException('
                                                                    Error in class (' . __CLASS__ . '): [ ' . 
                                                                    implode(', ', $update_handle->errorInfo()) . 
                                                                    ' ] on line ' . __LINE__);
        $this->_data['count'] += $_increment;
        
        return $this;
        
    }





    /**
     * hasProduct
     *
     * @param \Cataleya\Catalog\Product $_product
     * @return boolean
     */
    public function hasProduct (\Cataleya\Catalog\Product $_product) 
    {

            return (in_array ($_product->getID(), $this->_collection));


    }


    /**
     * hasProductID
     *
     * @param integer $_product_id
     * @return booolean
     */
    public function hasProductID ($_product_id) 
    {
        

            return (is_int($_product_id) && in_array ((int)$_product_id, $this->_collection));


    }
    
    

	
    public function saveData () 
    {

    }
    
    
    
    

    public function getPopulation() {
        return ($this->hasCollection()) ? $this->_data['count'] : count($this->_collection);
    }

    

    public function getPageStart() {
        return $this->_collection[0];
    }
    
    
    
    public function getPageStop() {
        return $this->_collection[count($this->_collection)-1];
    }
    
    
    
    
    
}




