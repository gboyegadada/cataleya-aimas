<?php


namespace Cataleya\Catalog;
use \Cataleya\Error;



/*
 *
 *	@Package: Cataleya
 *	@Class: \Cataleya\Catalog\Price
 *
 *	(c) 2012 Fancy Paper Planes
 *
 *
 *
 */


class Price implements \Cataleya\Asset, \Cataleya\System\Event\Observable 

{


	private $_values = array();
	private $_data = array();
    private $_modified = array();

    private $dbh, $e;
	        
    protected static $_events = [
            'catalog/price/updated'
        ];			
				

/*
 * Class CONSTANTS
 * ___________________________________________
 *
 * Use like: \Cataleya\Catalog\Price::create(\Cataleya\Catalog\Price::TYPE_ABS)
 *
 *
*/

				
	const TYPE_ABS = '(=)';
	const TYPE_REDUCTION = '(-)';
	const TYPE_ADDITIONAL = '(+)';
	const TYPE_PERCENT = '(%)';
	const TYPE_REDUCTION_PERCENT = '(-)(%)';
	const TYPE_ADDITIONAL_PERCENT = '(+)(%)';
				
    const FILTER_PRODUCT_MIN = 1;
    const FILTER_PRODUCT_MAX = 2;
				



	/*
	 *
	 * [ __construct ]
	 * ________________________________________________________________
	 * 
	 * 
	 *
	 *
	 *
	 */
	 
	private function __construct () 
	{
		
		// Get database handle...
        $this->dbh = \Cataleya\Helper\DBH::getInstance();
		
		// Get error handler
		$this->e = \Cataleya\Helper\ErrorHandler::getInstance();
		
		// initialize
		
		
	}



	/*
	 *
	 * [ __destruct ]
	 * ________________________________________________________________
	 * 
	 * 
	 *
	 *
	 *
	 */
	 
	public function __destruct () 
	{
		
		
		
		
		
	}




	/*
	 *
	 *  [ Attribute ] interface method: [ load ]
	 * ________________________________________________________________
	 * 
	 * 
	 *
	 *
	 *
	 */
	 
	static public function load ($id = 0) 
	{
		
		if ($id == 0 || filter_var($id, FILTER_VALIDATE_INT) === FALSE) return NULL;
		
                
		///// CREATE AN INSTANCE OF [\Cataleya\Catalog\Price] /////////////////////////
		$price = new \Cataleya\Catalog\Price();

		
		///// CHECK IF PRICE ID IS VALID /////////////////////////
		static $select_handle, $select_handle_param_price_id;
		
		if (empty($select_handle))
		{
		$select_handle = $price->dbh->prepare('SELECT * FROM prices WHERE price_id = :price_id LIMIT 1');
		$select_handle->bindParam(':price_id', $select_handle_param_price_id, \PDO::PARAM_INT);
		}
		
		$select_handle_param_price_id = $id;
		
		if (!$select_handle->execute()) throw new Error('DB Error: ' . implode(', ', $select_handle->errorInfo()));
                
                
		
		

        $price->_data = $select_handle->fetch(\PDO::FETCH_ASSOC);
        if (empty($price->_data)) 
        {
            unset($price);
            return NULL;
        }


		


		$price->loadValues();
		

		return $price;
			
		
		
	}





	/*
	 *
	 *  [ Attribute ] interface method: [ create ]
	 * ________________________________________________________________
	 * 
	 * 
	 *
	 *
	 *
	 */
	 
	static public function create ($price_type = self::TYPE_ABS) 
	{
		if (
				$price_type !== self::TYPE_ABS && 
				$price_type !== self::TYPE_REDUCTION && 
				$price_type !== self::TYPE_ADDITIONAL && 
				$price_type !== self::TYPE_PERCENT && 
				$price_type !== self::TYPE_REDUCTION_PERCENT && 
				$price_type !== self::TYPE_ADDITIONAL_PERCENT
				
				) return FALSE;
				
		///// CREATE AN INSTANCE OF [\Cataleya\Catalog\Price] /////////////////////////
		$price = new \Cataleya\Catalog\Price();
		
		
		
		///// DO NEW INSERT /////////////////////////
		static $insert_handle, $insert_handle_param_price_type;
		
		if (empty($insert_handle))
		{
			$insert_handle = $price->dbh->prepare('INSERT INTO prices (type) VALUES (:price_type)');
			$insert_handle->bindParam(':price_type', $insert_handle_param_price_type, \PDO::PARAM_STR);
		}
		
		$insert_handle_param_price_type = $price_type;
		
		if (!$insert_handle->execute()) throw new Error('DB Error: ' . implode(', ', $insert_handle->errorInfo()));
										
		
                // Init _data array
                $price->_data = array (
                    'price_id'  =>  $price->dbh->lastInsertId(), 
                    'type'  =>  $price_type
                );
		
		
		return $price;
		
	}




    /**
     * getEvents
     * 
     * @return array
     */
    public static function getEvents() {

        return self::$_events;
    }



	/**
	 * getProductPrice
	 *
	 * @param \Cataleya\Catalog\Product $_Product 
	 * @param int $_filter
	 * @return \Cataleya\Catalog\Price
	 */
	static public function getProductPrice (\Cataleya\Catalog\Product $_Product, $_filter = self::FILTER_PRODUCT_MIN) 
	{
		
                
		///// CREATE AN INSTANCE OF [\Cataleya\Catalog\Price] /////////////////////////
		$price = new \Cataleya\Catalog\Price();

		
		///// CHECK IF PRICE ID IS VALID /////////////////////////
		static $select_handle, $select_handle_param_id;
		
		if (empty($select_handle))
		{
            $select_handle = $price->dbh->prepare('' . 
                'SELECT * FROM price_values a ' . 
                'INNER JOIN product_options b ' . 
                'ON a.price_id = b.price_id ' .
                'WHERE b.product_id = :id ' . 
                'ORDER BY a.value ASC ' . 
                'LIMIT 1'
            );
		$select_handle->bindParam(':id', $select_handle_param_id, \PDO::PARAM_INT);
		}
		
		$select_handle_param_id = $_Product->getID();
		
		if (!$select_handle->execute()) throw new Error('DB Error: ' . implode(', ', $select_handle->errorInfo()));
                
                
		
		

        $price->_data = $select_handle->fetch(\PDO::FETCH_ASSOC);
        if (empty($price->_data)) 
        {
            unset($price);
            return NULL;
        }

		$price->loadValues();
		

		return $price;
			
		
		
	}



    private function loadValues () 
    {

		///// GET PRICE VALUES /////////////////////////
		static $select_handle2, $select_handle2_param_price_id;
		
		if (empty($select_handle2))
		{
			$select_handle2 = $this->dbh->prepare('
                                        SELECT * 
                                        FROM price_values 
                                        WHERE price_id = :price_id 
                                        LIMIT 100 
                                        ');
			$select_handle2->bindParam(':price_id', $select_handle2_param_price_id, \PDO::PARAM_INT);
		}
		
		$select_handle2_param_price_id = $this->getID();
		
		if (!$select_handle2->execute()) throw new Error('DB Error: ' . implode(', ', $select_handle2->errorInfo()));
										
		while ($row = $select_handle2->fetch(\PDO::FETCH_ASSOC)) 
		{
			$this->_values[(int)$row['store_id']] = floatval($row['value']);
						
		} 
		


    }








	/*
	 *
	 *  [ Attribute ] interface method: [ getID ]
	 * ________________________________________________________________
	 * 
	 * 
	 *
	 *
	 *
	 */
	 
	public function getID () 
	{
		
		
		return $this->_data['price_id'];
			
		
		
	}






	/*
	 *
	 *  [ Attribute ] interface method: [ getClassName ]
	 * ________________________________________________________________
	 * 
	 * 
	 *
	 *
	 *
	 */
	 
	public function getClassName () 
	{
		
		
		return __CLASS__;
			
		
		
	}



	/*
	 *
	 *  [ getValue ]
	 * ________________________________________________________________
	 * 
	 *
	 *
	 *
	 */
	 
	public function getValue ($_Store = NULL) 
	{
		if ($_Store instanceof \Cataleya\Store) $store_id = (int)$_Store->getStoreId ();
                else if (is_numeric($_Store)) { $store_id = (int)$_Store; $_Store = \Cataleya\Store::load($store_id); }
                
		if ($_Store === NULL || !isset($this->_values[$store_id])) return NULL;
                
		
                $currency = \Cataleya\Locale\Currency::load($_Store->getCurrencyCode());
                $value = $this->_values[$store_id] * floatval($currency->getRate());  
                
                if ($currency->getRate() > 100) $value = round($value, 1); // A small fix for weak currencies...
                return round($value, 2);
            

		
	}





	/*
	 *
	 *  [ setValue ]
	 * ________________________________________________________________
	 * 
	 * 
	 *
	 *
	 *
	 */
	 
	public function setValue ($_Store = NULL, $value = NULL) 
	{
            
		if ($_Store instanceof \Cataleya\Store) $store_id = (int)$_Store->getStoreId ();
                else if (is_numeric($_Store)) { $store_id = (int)$_Store; $_Store = \Cataleya\Store::load($store_id); }
                
                $value = filter_var($value, FILTER_VALIDATE_FLOAT, FILTER_FLAG_ALLOW_FRACTION | FILTER_FLAG_ALLOW_THOUSAND);
		if ($_Store === NULL || $value === FALSE) return FALSE;

                
                $currency = \Cataleya\Locale\Currency::load($_Store->getCurrencyCode());
                $value = $value / floatval($currency->getRate());
				
		
		// construct
		
		static 
				$insert_handle, 
				$insert_handle_param_price_id, 
				$insert_handle_param_id, 
				$insert_handle_param_value;
		
		if (empty($insert_handle))
		{
			$insert_handle = $this->dbh->prepare('
                                                                INSERT INTO price_values (price_id, store_id, value) 
                                                                VALUES (:price_id, :store_id, :value) 
                                                                ON DUPLICATE KEY UPDATE value = :value 
                                                                ');
			$insert_handle->bindParam(':price_id', $insert_handle_param_price_id, \PDO::PARAM_INT);
			$insert_handle->bindParam(':store_id', $insert_handle_param_id, \PDO::PARAM_INT);
			$insert_handle->bindParam(':value', $insert_handle_param_value, \PDO::PARAM_STR);
		}
		
		$insert_handle_param_price_id = $this->_data['price_id'];
		$insert_handle_param_id = $store_id;
		$insert_handle_param_value = $value;
		
		if (!$insert_handle->execute()) throw new Error('DB Error: ' . implode(', ', $insert_handle->errorInfo()));
										
		$price->_values[$store_id] = $value;
		
		
		return TRUE;
		
	}



	/*
	 *
	 *  [ Attribute ] interface method: [ getPriceType ]
	 * ________________________________________________________________
	 * 
	 * 
	 *
	 *
	 *
	 */
	 
	public function getPriceType () 
	{
		
		
		return $this->_data['type'];
			
		
		
	}




        
        
        
	/*
	 *
	 *  [ delete ]
	 * ________________________________________________________________
	 * 
	 * 
	 *
	 *
	 *
	 */
	 
	public function delete () 
	{

                        
                        
			// 2. Delete price...
            
			$entry_delete = $this->dbh->prepare('DELETE FROM prices WHERE price_id = :id');
			$entry_delete->bindParam(':id', $entry_delete_param_id, \PDO::PARAM_INT);
			
			$entry_delete_param_id = $this->_data['price_id'];
	
			if (!$entry_delete->execute()) throw new Error('DB Error: ' . implode(', ', $entry_delete->errorInfo()));
                        
                        
                        
                        
                        return TRUE;
		
		
	}




	/*
	 *
	 *  [ destroy ]: Alias for [ delete ]
	 * ________________________________________________________________
	 * 
	 * 
	 *
	 *
	 *
	 */
	 
	public function destroy () 
	{
            return $this->delete();
            
        }        



        
        
	
	

       
        
        
}

