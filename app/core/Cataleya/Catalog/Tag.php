<?php



namespace Cataleya\Catalog;



/*
 *
 * 	@Package: Cataleya
 * 	@Class: \Cataleya\Catalog\Tag
 *
 * 	(c) 2012 Fancy Paper Planes
 *
 *
 *
 */

class Tag extends \Cataleya\Catalog\ProductCollection
{

    // To be used by [\Cataleya\Collection]
    // protected $_collection_obj;

    /*
    protected $_collection = array ();
    protected $_position = 0;

    // CONST VALUES 1 - 15 RESERVED FOR [ _Abstract_Collection ]
    const ORDER_BY_TITLE = 16;
    const ORDER_BY_VIEWS = 17;
    const ORDER_BY_SALES = 18;
    const ORDER_BY_PRICE = 19;

     */

    protected $_data = array();
    protected $_modified = array();

    private $_sub_categories = array ();

    private
                $_description,
                $_url_rewrite, $_slug = '',
                $_banner_image;

    protected
                $_page_size,
                $_page_num;

    public function __construct() {
    parent::__construct();

        // Get database handle...
        $this->dbh = \Cataleya\Helper\DBH::getInstance();

        // Get error handler
        $this->e = \Cataleya\Helper\ErrorHandler::getInstance();



    }



    public function __destruct() {
        $this->saveData();
    }











	/**
         *
         * @param int $id
         * @param array $filters
         * @param array $sort
         * @param int $page_size
         * @param int $page_num
         * @return \Cataleya\Catalog\Tag
         *
         */
	public static function load ($id = 0, $filters = array(), $sort = NULL, $page_size = 16, $last_seen = 0, $page_num = 1)
	{
		if (!is_numeric($page_size) || !is_numeric($page_num)) \Cataleya\Helper\ErrorHandler::getInstance()->triggerException('Error in class (\Cataleya\Catalog\Tag): [ invalid argument (invalid number or pagesize) for method: ' . __FUNCTION__ . ' ] on line ' . __LINE__);



		$load_with_slug = false;

                // arg is tag_id
		if (filter_var($id, FILTER_VALIDATE_INT) !== FALSE && (int)$id > 0)  {} // Do nothing

                // arg is a keyword
                else if (is_string($id) && filter_var($id, FILTER_VALIDATE_REGEXP, array('options'=>array('regexp' => '/^(([A-Za-z0-9]+\-?){1,50})$/'))) !== FALSE) { $load_with_slug = true; }

                // Fire burn this ID !!
                else \Cataleya\Helper\ErrorHandler::getInstance()->triggerException('Error in class (' . __CLASS__ . '): [ invalid argument (invalid id or keyword) for method: ' . __FUNCTION__ . ' ] on line ' . __LINE__);


                if (!defined ('IS_ADMIN_FLAG') && IS_ADMIN_FLAG === FALSE) {
                    $id .= '-' . \Cataleya\Front\Shop::getInstance()->getID();
                }

                // Create [tag] instance...
                $_objct = __CLASS__;
                $instance = new $_objct;

		$row = 	($load_with_slug)
                        ? $instance->loadDataWithKeyword($id)
                        : $instance->loadDataWithID($id);

		if (empty($row))
		{
                    return NULL;

		} else {


                    $instance->_data = $row;
                }



		// LOAD: DESCRIPTION

		/*
		 * I want multiple language support here so [$this->_description] will actually be an
		 * array of Description objects in different languages.
		 *
		 */

		$instance->_description = \Cataleya\Asset\Description::load($instance->_data['description_id']);




                $instance->_page_size = $page_size;
                $instance->_page_num = $page_num;


                // LOAD: PRODUCTS





                /////////////////// LOAD ASSOCIATED PRODUCTS /////////////////////
                // $filters[] = $instance;
                $instance->_loadCollection($filters, $sort, $page_size, $last_seen,$page_num);

                // $instance->_collection_obj = \Cataleya\Catalog::load($filters);

                // $instance->_collection = $instance->_collection_obj->getIDs();
                // $instance->pageSetup($sort, $page_size, $page_num);

                // RETURN TAG INSTANCE
		return $instance;


	}








        protected function loadDataWithKeyword ($instance_id) {

		///// CHECK IF DESCRIPTION ID IS VALID /////////////////////////
		static
					$select_handle,
					$select_handle_param_instance_id;


		if (!isset($select_handle)) {
			// PREPARE SELECT STATEMENT...
			$select_handle = $this->dbh->prepare('
                            SELECT a.*, c.count FROM tags a
                            INNER JOIN collections c
                            ON a.collection_id = c.collection_id
                            INNER JOIN url_rewrite b
                            ON a.url_rewrite_id = b.url_rewrite_id
                            WHERE b.keyword = :instance_id
                            LIMIT 1
                            ');
			$select_handle->bindParam(':instance_id', $select_handle_param_instance_id, \PDO::PARAM_STR);
		}


		$select_handle_param_instance_id = $instance_id;

		if (!$select_handle->execute()) $this->e->triggerException('
										Error in class (' . __CLASS__ . '): [ ' .
										implode(', ', $select_handle->errorInfo()) .
										' ] on line ' . __LINE__);

		$row = 	$select_handle->fetch(\PDO::FETCH_ASSOC);

                return $row;

        }






        protected function loadDataWithID ($instance_id) {

		///// CHECK IF DESCRIPTION ID IS VALID /////////////////////////
		static
					$select2_handle,
					$select2_handle_param_instance_id;



		if (empty($select2_handle))
		{
			$select2_handle = $this->dbh->prepare('
                                    SELECT a.*, c.count FROM tags a
                                    INNER JOIN collections c
                                    ON a.collection_id = c.collection_id
                                    WHERE a.tag_id = :instance_id
                                    LIMIT 1
                                    ');
			$select2_handle->bindParam(':instance_id', $select2_handle_param_instance_id, \PDO::PARAM_INT);
		}



		$select2_handle_param_instance_id = $instance_id;

		if (!$select2_handle->execute()) $this->e->triggerException('
										Error in class (' . __CLASS__ . '): [ ' .
										implode(', ', $select2_handle->errorInfo()) .
										' ] on line ' . __LINE__);

		$row = 	$select2_handle->fetch(\PDO::FETCH_ASSOC);


                // if (empty($row)) \Cataleya\Helper\ErrorHandler::getInstance()->triggerException('data could not be read...' . $instance_id . ':' . __LINE__);

                return $row;

        }










	/**
         *
         * @param \Cataleya\Store $_Store
         * @param string $description_title
         * @param string $description_text
         * @param string $language_code
         * @return \Cataleya\Catalog\Tag
         *
         */
	static public function create (\Cataleya\Store $_Store, $description_title = '', $description_text = '', $language_code = 'EN')
	{



		/*
		 * Handle description Attribute
		 *
		 */

		 $description = \Cataleya\Asset\Description::create($description_title, $description_text, $language_code);




		/*
		 * Handle meta_keywords Attribute
		 *
		 */

		 $meta_keywords = \Cataleya\Asset\Text::create($description_title, $language_code);



		/*
		 * Handle meta_description Attribute
		 *
		 */

		 $meta_description = \Cataleya\Asset\Text::create($description_title, $language_code);




		/*
		 * Handle url_rewrite Attribute
		 *
		 */

		 $url_rewrite = \Cataleya\Asset\URLRewrite::create($_Store, $description_title, 'category.php', 0);


		// Get database handle...
                $dbh = \Cataleya\Helper\DBH::getInstance();

		// Get error handler
		$e = \Cataleya\Helper\ErrorHandler::getInstance();











		// construct
		static
				$insert_handle,
				$insert_handle_param_store_id,
				$insert_handle_param_description_id,
                                $insert_handle_param_collection_id,
                                $insert_handle_param_meta_keywords_text_id,
                                $insert_handle_param_meta_description_text_id,
                                $insert_handle_param_url_rewrite_id,
                                $insert_handle_param_is_root;

		if (empty($insert_handle))
		{
			$insert_handle = $dbh->prepare('
                                                    INSERT INTO tags
                                                    (store_id, is_root, description_id, collection_id, meta_keywords_text_id, meta_description_text_id, url_rewrite_id, date_added, last_modified)
                                                    VALUES (:store_id, :is_root, :description_id, :collection_id, :meta_keywords_text_id, :meta_description_text_id, :url_rewrite_id, NOW(), NOW())
                                                    ');

			$insert_handle->bindParam(':store_id', $insert_handle_param_store_id, \PDO::PARAM_INT);
			$insert_handle->bindParam(':is_root', $insert_handle_param_is_root, \PDO::PARAM_INT);
			$insert_handle->bindParam(':description_id', $insert_handle_param_description_id, \PDO::PARAM_INT);
			$insert_handle->bindParam(':collection_id', $insert_handle_param_collection_id, \PDO::PARAM_INT);
                        $insert_handle->bindParam(':meta_keywords_text_id', $insert_handle_param_meta_keywords_text_id, \PDO::PARAM_INT);
                        $insert_handle->bindParam(':meta_description_text_id', $insert_handle_param_meta_description_text_id, \PDO::PARAM_INT);
			$insert_handle->bindParam(':url_rewrite_id', $insert_handle_param_url_rewrite_id, \PDO::PARAM_INT);
		}

		$insert_handle_param_store_id = $_Store->getID();
		$insert_handle_param_is_root = ($_Store->hasRootCategory(TRUE)) ? 0 : 1; // If store does not have a root category yet, make this the root category.
		$insert_handle_param_description_id = $description->getID();
                $insert_handle_param_collection_id = self::_initCollection();
                $insert_handle_param_meta_keywords_text_id = $meta_keywords->getID();
                $insert_handle_param_meta_description_text_id = $meta_description->getID();
		$insert_handle_param_url_rewrite_id = $url_rewrite->getID();

		if (!$insert_handle->execute()) $e->triggerException('
                                                                    Error in class (' . __CLASS__ . '): [ ' .
                                                                    implode(', ', $insert_handle->errorInfo()) .
                                                                    ' ] on line ' . __LINE__);


		// AUTOLOAD NEW PRODUCT AND RETURN IT
		$instance = self::load($dbh->lastInsertId());
		// $url_rewrite->setTargetPath('category.php?c=' . $instance->getID());

                if ($insert_handle_param_is_root === 0) {
                    $instance->setParent($_Store->getRootCategory());
                } else {
                    $instance->setHandle('all');
                }

		return $instance;


	}









	public function saveData ()
	{


		if (empty($this->_modified)) return;

                $this->_modified = array_unique($this->_modified);

		$_update_params = array();
		$key_val_pairs = array();

		foreach ($this->_modified as $key) $key_val_pairs[$key] = $key . ' = :' . $key;


		// Do prepared statement for 'INSERT'...
		$update_handle = $this->dbh->prepare('
												UPDATE tags
												SET ' . implode (', ', $key_val_pairs) . ', last_modified=now()
												WHERE tag_id = :tag_id
												');
		$_update_params['tag_id'] = $this->_data['tag_id'];
		$update_handle->bindParam(':tag_id', $_update_params['tag_id'], \PDO::PARAM_INT);

		foreach ($this->_modified as $key) {

                        $_update_params[$key] = $this->_data[$key];
			$update_handle->bindParam(':'.$key, $_update_params[$key], \Cataleya\Helper\DBH::getTypeConst($_update_params[$key]));

		}

		if (!$update_handle->execute()) $this->e->triggerException('Error in class (' . __CLASS__ . '): [ ' . implode(', ', $update_handle->errorInfo()) . ' ] on line ' . __LINE__);

                $this->_modified = array();
	}








	/*
	 *
	 *  [ delete ]
	 * ________________________________________________________________
	 *
	 *
	 *
	 *
	 *
	 */

	public function delete ()
	{

            $this->_modified = array (); // prevent save


            // //// BEGIN TRANSACTION /////
            \Cataleya\Helper\DBH::getInstance()->beginTransaction();



                        // Delete rows containing 'product_id' in specified tables
                        //\Cataleya\Helper\DBH::sanitize(array('sub_categories'), 'member_tag_id', $this->getID());





                        // 1. Remove Subcategories
                        // $this->removeAllSubCategories();

                        // 2. Remove Product Tags
                        \Cataleya\Helper\DBH::sanitize(array('tags'), 'tag_id', $this->getID());
                        \Cataleya\Helper\DBH::sanitize(array('collections'), 'collection_id', $this->getCollectionID());
                        \Cataleya\Helper\DBH::sanitize(array('content'), 'content_id', $this->_data['description_id']);
                        \Cataleya\Helper\DBH::sanitize(array('text'), 'text_id', $this->_data['meta_description_text_id']);
                        \Cataleya\Helper\DBH::sanitize(array('text'), 'text_id', $this->_data['meta_keywords_text_id']);
                        \Cataleya\Helper\DBH::sanitize(array('url_rewrite'), 'url_rewrite_id', $this->_data['url_rewrite_id']);
                        if ((int)$this->_data['image_id'] > 0) \Cataleya\Helper\DBH::sanitize(array('images'), 'image_id', $this->_data['image_id']);

                        // 3. Unset Primary Image
                        //$this->unsetBannerImage();

                        // 4. Delete url_rewrite
                        //$this->getURLRewrite()->delete();

                        // 5. Delete description
                        //$this->getDescription()->delete();


                         // 6. Delete description
                        //$this->getMetaDescription()->delete();

                         // 7. Delete description
                        //$this->getMetaKeywords()->delete();



                // //// COMMIT TRANSACTION /////
                \Cataleya\Helper\DBH::getInstance()->commit();


		// $this = NULL;
		return TRUE;


	}




















	/*
	 *
	 *  [ setBannerImage ]
	 * ________________________________________________________________
	 *
	 *
	 *
	 *
	 *
	 */


	public function setBannerImage (\Cataleya\Asset\Image $image)
	{
                $this->unsetBannerImage ();

		$this->_data['image_id'] = $image->getID();
		$this->_banner_image = $image;

		$this->_modified[] = 'image_id';

	}





	/*
	 *
	 *  [ unsetBannerImage ]
	 * ________________________________________________________________
	 *
	 *
	 *
	 *
	 *
	 */


	public function unsetBannerImage ()
	{
    if (!empty($this->_data['image_id'])) 
    {

      $this->getBannerImage()->destroy();
  		$this->_data['image_id'] = NULL;
  		$this->_banner_image = NULL;

  		$this->_modified[] = 'image_id';

    }

    return $this;

	}






	/*
	 *
	 *  [ getBannerImage ]
	 * ________________________________________________________________
	 *
	 * Returns image object
	 *
	 *
	 *
	 */


	public function getBannerImage ()
	{
		if (empty($this->_banner_image) )
		{
			$this->_banner_image = \Cataleya\Asset\Image::load($this->_data['image_id']);
		}

		return $this->_banner_image;

	}




      /*
       *
       * Misc [getters]
       *
       */






        /*
         *
         * [ getSize ]
         *_____________________________________________________
         *
         *
         */

        public function getSize()
        {
                return count($this->_collection);
        }




        /*
         *
         * [ getTagId ]
         *_____________________________________________________
         *
         *
         */

        public function getTagId()
        {
                return $this->_data['tag_id'];
        }




        /*
         *
         * [ getID ]
         *_____________________________________________________
         *
         *
         */

        public function getID()
        {
                return $this->_data['tag_id'];
        }



         /*
         *
         * [ getDescription ]
         *_____________________________________________________
         *
         * Will return tag description attribute as object
         *
         */

        public function getDescription()
        {
                if (empty($this->_description)) $this->_description = \Cataleya\Asset\Description::load($this->_data['description_id']);
                return $this->_description;
        }




	/*
	 *
	 *  [ getMetaDescription ]
	 * ________________________________________________________________
	 *
	 * Will return product meta_description as [ _Asset_Text ] object
	 *
	 *
	 */

	public function getMetaDescription ()
	{
		if (empty($this->_meta_description)) $this->_meta_description = \Cataleya\Asset\Text::load ($this->_data['meta_description_text_id']);
		return $this->_meta_description;

	}






	/*
	 *
	 *  [ getMetaKeywords ]
	 * ________________________________________________________________
	 *
	 * Will return product meta_keywords as [ _Asset_Text ] object
	 *
	 *
	 */

	public function getMetaKeywords ()
	{

                if (empty($this->_meta_keywords)) $this->_meta_keywords = \Cataleya\Asset\Text::load($this->_data['meta_keywords_text_id']);
		return $this->_meta_keywords;

	}









        /**
         *
         *
         * Will return product URL Rewrite (object)
         *
         * @return \Cataleya\Asset\URLRewrite
         *
         */
	public function getURLRewrite ()
	{
		if (empty($this->_url_rewrite)) $this->_url_rewrite = \Cataleya\Asset\URLRewrite::load ($this->_data['url_rewrite_id']);
		return $this->_url_rewrite;

	}







	/**
	 *
	 * @return string
	 *
	 *
	 *
	 */

	public function getHandle ()
	{

		return $this->getURLRewrite()->getKeyword();

	}






	/**
	 *
	 * @param string $keyword
	 *
	 *
	 *
	 */
	public function setHandle ($keyword = '')
	{
		return $this->getURLRewrite()->setKeyword($keyword);


	}









        /*
         *
         * [ getSortOrder ]
         *_____________________________________________________
         *
         *
         */

        public function getSortOrder()
        {
                return $this->_data['sort_order'];
        }



        /*
         *
         * [ getDateAdded ]
         * ______________________________________________
         *
         */

        public function getDateAdded()
        {
                return $this->_data['date_added'];
        }




        /*
         *
         * [ getLastModified ]
         * ______________________________________________
         *
         */

        public function getLastModified()
        {

                return $this->_data['last_modified'];
        }






        /**
         *
         * @return int Store ID
         */
        public function getStoreId()
        {
                return $this->_data['store_id'];
        }



        /**
         *
         * @return \Cataleya\Store
         *
         */
        public function getStore()
        {
                return \Cataleya\Store::load($this->_data['store_id']);
        }







        /**
         *
         * @return boolean
         *
         */
        public function isRoot()
        {
                return ((int)$this->_data['is_root'] === 1) ? TRUE : FALSE;
        }





        /*
         *
         * [ getStatus ]
         *_____________________________________________________
         *
         *
         */

        public function getStatus()
        {
                return $this->_data['status'];
        }



        /*
         *
         * [ isActive ]
         *_____________________________________________________
         *
         *
         */

        public function isActive()
        {
                return ((int)$this->_data['status'] === 1) ? TRUE : FALSE;
        }





        /*
         *
         * [ enable ]
         * ______________________________________________________
         *
         *
         */


        public function enable ()
        {

            if ((int)$this->_data['status'] === 0)
            {
                $this->_data['status'] = 1;
                $this->_modified[] = 'status';
            }

            return TRUE;
        }





        /*
         *
         * [ disable ]
         * ______________________________________________________
         *
         *
         */


        public function disable ()
        {
            if ((int)$this->_data['status'] === 1)
            {
                $this->_data['status'] = 0;
                $this->_modified[] = 'status';
            }

            return TRUE;
        }





       /*
       *
       * Misc [ SETTERS ]
       *
       */


        /*
        *
        * [ setSortOrder ]
        * ______________________________________________________
        *
        */

       public function setSortOrder($value)
       {

               $this->_data['sort_order'] = $value;
               $this->_modified[] = 'sort_order';

               return TRUE;
       }




        /*
         *
         * [ setStatus ]
         *_____________________________________________________
         *
         *
         */

        public function setStatus($value)
        {
                $this->_data['status'] = $value;
                $this->_modified[] = 'status';

                return TRUE;
        }






        public function updateMetaKeywords ()
        {

                // Generate meta keywords...
                $_blob = $this->_description->getTitle('EN') . ' ' . $this->_description->getText('EN');

                // 1. keys from name and description
                $keywords = explode(' ', preg_replace('/([^A-Za-z0-9\W]+|[\.]+)/', '', $_blob));


                // 2. Keys from products

                foreach ($this as $_product)
                {
                   $_keys =  explode(' ', preg_replace('/([^A-Za-z0-9\W]+|[\.]+)/', '', $_product->getDescription()->getTitle('EN')));

                   $keywords = array_merge($keywords, $_keys);
                }


                $keywords = array_unique($keywords);

                // 3. Save new keywords
                $this->getMetaKeywords()->setText(implode(', ', $keywords), 'EN');
                $this->getMetaDescription()->setText($this->_description->getText('EN'), 'EN');



        }









        /*
	 *
	 *  [ setParent ]
	 * ________________________________________________________________
	 *
	 *
	 *
	 *
	 */

	public function setParent (\Cataleya\Catalog\Tag $_Tag)
	{

                $this->_data['parent_id'] = $_Tag->getID();
                $this->_modified[] = 'parent_id';

                return TRUE;

	}









	/*
	 *
	 *  [ removeSubCategory ]
	 * ________________________________________________________________
	 *
	 *
	 *
	 *
	 */

	public function removeSubCategory (\Cataleya\Catalog\Tag $_Tag)
	{


		if ($this->hasSubCategory($_Tag))
		{







                        // Do prepared statement for 'INSERT'...
                        $update_handle = $this->dbh->prepare('
                                                                                                        UPDATE tags
                                                                                                        SET parent_id = NULL, last_modified=now()
                                                                                                        WHERE tag_id = :tag_id
                                                                                                        ');


			$update_handle_param_tag_id = $_Tag->getID();
                        $update_handle->bindParam(':tag_id', $update_handle_param_tag_id, \PDO::PARAM_INT);








			if (!$update_handle->execute()) $this->e->triggerException('Error in class (' . __CLASS__ . '): [ ' . implode(', ', $update_handle->errorInfo()) . ' ] on line ' . __LINE__);



                        foreach ($this->_sub_categories as $key=>$val)
                        {
                            if ((int)$val === (int)$update_handle_param_tag_id)
                            {
                                unset ($this->_sub_categories[$key]);
                                break;
                            }
                        }

		}

		return $this;

	}










	/*
	 *
	 *  [ removeAllSubCategories ]
	 * ________________________________________________________________
	 *
	 *
	 *
	 *
	 */

	public function removeAllSubCategories ()
	{




                        // Do prepared statement for 'INSERT'...
                        $update_handle = $this->dbh->prepare('
                                                                                                        UPDATE tags
                                                                                                        SET parent_id = NULL, last_modified=now()
                                                                                                        WHERE parent_id = :tag_id
                                                                                                        ');


			$update_handle_param_tag_id = $this->_data['tag_id'];
                        $update_handle->bindParam(':tag_id', $update_handle_param_tag_id, \PDO::PARAM_INT);








			if (!$update_handle->execute()) $this->e->triggerException('Error in class (' . __CLASS__ . '): [ ' . implode(', ', $update_handle->errorInfo()) . ' ] on line ' . __LINE__);



                        foreach ($this->_sub_categories as $key=>$val)
                        {
                            if ((int)$val === (int)$update_handle_param_tag_id)
                            {
                                unset ($this->_sub_categories[$key]);
                                break;
                            }
                        }

		return $this;

	}









	/*
	 *
	 *  [ getParent ]
	 * ________________________________________________________________
	 *
	 *
	 *
	 *
	 */

	public function getParent ($_AUTO_LOAD = TRUE)
	{
               if (!is_numeric($this->_data['parent_id'])) return NULL;

               return ($_AUTO_LOAD == TRUE) ? self::load($this->_data['parent_id']) : (int)$this->_data['parent_id'];


        }






	/*
	 *
	 *  [ getParent ]
	 * ________________________________________________________________
	 *
	 *
	 *
	 *
	 */

	public function isParent (\Cataleya\Catalog\Tag $_Tag)
	{
            return (is_numeric($this->_data['parent_id']) && (int)$this->_data['parent_id'] === (int)$_Tag->getID());

     }






	/*
	 *
	 *  [ hasParent ]
	 * ________________________________________________________________
	 *
	 *
	 *
	 *
	 */

	public function hasParent ()
	{


               return (is_numeric($this->_data['parent_id']));


        }








	/*
	 *
	 *  [ hasSubCategory ]
	 * ________________________________________________________________
	 *
	 *
	 *
	 *
	 */

	public function hasSubCategory (\Cataleya\Catalog\Tag $_Tag)
	{

                $this->_loadSubCategories (FALSE);
		return (in_array ((int)$_Tag->getID(), $this->_sub_categories));


        }





    /**
     *
     * @param boolean $_FORCE_RELOAD
     * @return array
     */
	public function getSubCategories ($_FORCE_RELOAD = FALSE)
	{
                $_collection = array ();
                $this->_loadSubCategories ($_FORCE_RELOAD);

                foreach ($this->_sub_categories as $_id) $_collection[] = \Cataleya\Catalog\Tag::load ($_id);

		return $_collection;

    }




    /**
     *
     * @param boolean $_FORCE_RELOAD
     * @return array
     */
    public function getChildren ($_FORCE_RELOAD = FALSE)
    {
        return $this->getSubCategories($_FORCE_RELOAD);
    }







        /*
	 *
	 *  [ _loadSubCategories ]
	 * ________________________________________________________________
	 *
	 *
	 *
	 *
	 */

	private function _loadSubCategories ($_FORCE_RELOAD = FALSE)
	{

               if ($_FORCE_RELOAD === FALSE && !empty($this->_sub_categories)) return $this->_sub_categories;
                // LOAD
               static
                       $select_handle;


               if (!isset($select_handle)) {
                       // PREPARE SELECT STATEMENT...
                       $select_handle = $this->dbh->prepare('SELECT tag_id FROM tags WHERE parent_id = :tag_id');

               }

               $param_tag_id = $this->_data['tag_id'];
               $select_handle->bindParam(':tag_id', $param_tag_id, \PDO::PARAM_INT);



               if (!$select_handle->execute()) $this->e->triggerException('
                                                                            Error in class (' . __CLASS__ . '): [ ' .
                                                                            implode(', ', $select_handle->errorInfo()) .
                                                                            ' ] on line ' . __LINE__);

               $this->_sub_categories = array ();
               while ($row = $select_handle->fetch(\PDO::FETCH_ASSOC))
               {
                   $this->_sub_categories[] = (int)$row['tag_id'];
               }

                return $this->_sub_categories;
	}










}
