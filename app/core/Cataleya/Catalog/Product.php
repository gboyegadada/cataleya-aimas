<?php


namespace Cataleya\Catalog;


use \Cataleya\Helper\DBH;
use \Cataleya\Error;


/*

CLASS STOREFRONT PRODUCT

*/



class Product 
extends \Cataleya\Collection 
implements \Cataleya\System\Event\Observable  
{
	
	
	private $_data = array();
	
	private 
				$_description, 
                                $_meta_description,
                                $_meta_keywords,
				$_url_rewrite, 
				$_primary_image;
        
				
	private $_base_images = array ();
	protected $_collection = array ();
	protected $_attributes = array ();
	private $_product_group = array ();
	protected $_sales = array ();
         
    private $_product_tags = array (); // tag IDs only!!
    private $_product_collections = array (); // collection IDs only

	private $_modified = array();
	
								
	private $dbh, $e, $_ProductType;
        
        
    protected static $_events = [
            'catalog/product/added',
            'catalog/product/updated', 
            'catalog/product/removed'
        ];
        
/*
 * These constants will be public so you can do something like: 
 * $product->getOptions(\Cataleya\Catalog\Product::OPTIONS_FETCH_ALL)
 *
 */
	const OPTIONS_FETCH_ALL = 1;
        

	


	/*
	 *
	 * [ __construct ]
	 * ________________________________________________________________
	 *
         * NOTICE
         * 
         * Even though this is public, it is not meant to be used when creating an instance of this class.
         * It's only public because it is extending the class [_Abstract_Collection ] whose __construct method is also public.
         * 
         * To create an instance of this class, use \Cataleya\Catalog\Product::create() OR \Cataleya\Catalog\Product::load($product_id)
	 *
	 *
	 */
	 
	public function __construct ()
	{
		
                parent::__construct();
                
		// Get database handle...
                $this->dbh = \Cataleya\Helper\DBH::getInstance();
		
		// Get error handler
		$this->e = \Cataleya\Helper\ErrorHandler::getInstance();
		
	}
	




	/*
	 *
	 * [ __destruct ]
	 * ________________________________________________________________
	 *
	 *
	 *
	 */

    public function __destruct()
    {
		$this->saveData();
		
    }
	




	/**
         * 
         * @param int|array $_id_or_data
         * @return \Cataleya\Catalog\Product
         * 
         */
	public static function load ($_id_or_data = 0)
	{
		
            

		if (is_scalar ($_id_or_data) && ($_id_or_data == 0 || filter_var($_id_or_data, FILTER_VALIDATE_INT) === FALSE)) { 
                    
                    return NULL;
                }
                
                
		$product = new \Cataleya\Catalog\Product ();
		
                
                if (is_array($_id_or_data)) {
                    $product->_data = $_id_or_data;
                }
                
                
                
                if (empty($product->_data)):
		// LOAD: PRODUCT INFO
		static $product_select, $param_product_id;
		
			
		if (!isset($product_select)) {
			// PREPARE SELECT STATEMENT...
			$product_select = $product->dbh->prepare(
                                   'SELECT * FROM products 
                                    WHERE product_id = :product_id LIMIT 1'
                                );
			$product_select->bindParam(
                                ':product_id', 
                                $param_product_id, 
                                \PDO::PARAM_INT
                                );
		}
		
		
		$param_product_id = $_id_or_data;
		
		if (!$product_select->execute()) $product->e->triggerException('
                                Error in class (' . __CLASS__ . '): [ ' . 
                                implode(', ', $product_select->errorInfo()) . 
                                ' ] on line ' . __LINE__);
		
		$product->_data = $product_select->fetch(\PDO::FETCH_ASSOC);
                
                endif;
                
                
		if (empty($product->_data)) return NULL;
		
                

		$product->_loadOptions();
                $product->_loadAttributes();
                $product->_loadSales();
                $product->_loadProductTags();
                
                
		return $product;	
		
		
	}



	/**
         * 
         * @param \Cataleya\Catalog\Product\Type $_ProductType
         * @param string $description_title
         * @param string $description_text
         * @param string $language_code
         * @return \Cataleya\Catalog\Product
         * 
         */
	static public function create (
                \Cataleya\Catalog\Product\Type $_ProductType, 
                \Cataleya\Agent $_Manufacturer, 
                \Cataleya\Agent $_Supplier, 
                $description_title = '', 
                $description_text = '', 
                $language_code = 'EN'
                ) 
	{
		
		

		/*
		 * Handle description Attribute 
		 *
		 */
		 
		 $description = \Cataleya\Asset\Description::create($description_title, $description_text, $language_code);
		 

                 
                 
		/*
		 * Handle meta_keywords Attribute 
		 *
		 */
		 
		 $meta_keywords = \Cataleya\Asset\Text::create($description_title, $language_code);
		 
		 
                 
		/*
		 * Handle meta_description Attribute 
		 *
		 */
		 
		 $meta_description = \Cataleya\Asset\Description::create($description_title, $description_text, $language_code);
		 
		 
		/*
		 * Handle url_rewrite Attribute 
		 *
		 */

		 $url_rewrite = \Cataleya\Asset\URLRewrite::create(NULL, $description_title, 'product.php', 0);
                 
                 


		
		// Get database handle...
                $dbh = \Cataleya\Helper\DBH::getInstance();
		
		// Get error handler
		$e = \Cataleya\Helper\ErrorHandler::getInstance();
		
		// construct
		static 
				$insert_handle, 
				$insert_handle_param_product_type_id, 
				$insert_handle_param_manufacturer_id, 
				$insert_handle_param_supplier_id, 
				$insert_handle_param_description_id, 
                $insert_handle_param_meta_keywords_text_id, 
                $insert_handle_param_meta_description_id, 
                $insert_handle_param_url_rewrite_id;
                
                
                
                $insert_handle_param_product_type_id = $_ProductType->getID();
                $insert_handle_param_manufacturer_id = $_Manufacturer->getID();
                $insert_handle_param_supplier_id = $_Supplier->getID();
                $insert_handle_param_description_id = $description->getID();
                $insert_handle_param_meta_keywords_text_id = $meta_keywords->getID();
                $insert_handle_param_meta_description_id = $meta_description->getID();
                $insert_handle_param_url_rewrite_id = $url_rewrite->getID();
                
		
		if (empty($insert_handle))
		{
			$insert_handle = $dbh->prepare('
                INSERT INTO products 
                (product_type_id, manufacturer_id, supplier_id, description_id, meta_keywords_text_id, meta_description_id, url_rewrite_id, date_added, last_modified) 
                VALUES (:product_type_id, :manufacturer_id, :supplier_id, :description_id, :meta_keywords_text_id, :meta_description_id, :url_rewrite_id, NOW(), NOW())
                    ');
																
			$insert_handle->bindParam(':product_type_id', $insert_handle_param_product_type_id, \PDO::PARAM_INT);
			$insert_handle->bindParam(':manufacturer_id', $insert_handle_param_manufacturer_id, \PDO::PARAM_INT);
			$insert_handle->bindParam(':supplier_id', $insert_handle_param_supplier_id, \PDO::PARAM_INT);
			$insert_handle->bindParam(':description_id', $insert_handle_param_description_id, \PDO::PARAM_INT);
            $insert_handle->bindParam(':meta_keywords_text_id', $insert_handle_param_meta_keywords_text_id, \PDO::PARAM_INT);
            $insert_handle->bindParam(':meta_description_id', $insert_handle_param_meta_description_id, \PDO::PARAM_INT);
			$insert_handle->bindParam(':url_rewrite_id', $insert_handle_param_url_rewrite_id, \PDO::PARAM_INT);
		}

		
		if (!$insert_handle->execute()) $e->triggerException('
										Error in class (' . __CLASS__ . '): [ ' . 
										implode(', ', $insert_handle->errorInfo()) . 
										' ] on line ' . __LINE__);
										
		
		// AUTOLOAD NEW PRODUCT AND RETURN IT
		$instance = self::load($dbh->lastInsertId());
		$url_rewrite->setTargetPath('product.php?p=' . $instance->_data['product_id']);
                

               /*
                // Generate arrays of arrays of attributes.
                $_a = array ();
                
                

                foreach ($_ProductType as $_AttributeType) 
                {
                    //$_c[] = $_AttributeType->getID();
                    
                    $_b = array ();
                    
                    foreach ($_AttributeType as $_Attribute) 
                    {
                        $_b[] = $_Attribute;
                    }
                    
                    $_a[] = $_b;
                }
                
                
                
                
                // Cartesian product of all Attribute arrays in $_ab.
                // Build array with all combinations of attributes..
                $_ab = \Cataleya\Helper::CartesianProduct($_a);
                
                
                // Create Product Options..
                foreach ($_ab as $_Attributes) \Cataleya\Catalog\Product\Option::create ($instance, $_Attributes);
                
                */
                
                
                // Load options...
                $instance->_loadOptions();
                
                \Cataleya\System::load()->updateProductCount(1);
                
		\Cataleya\System\Event::notify('catalog/product/created', $instance, array ());
		return $instance;
		
		
	}





    /**
     * getEvents
     * 
     * @return array
     */
    public static function getEvents() {

        return self::$_events;
    }





	
	
	public function saveData () 
	{
		

		if (empty($this->_modified)) return;
                
                $this->_modified = array_unique($this->_modified);
                
		$_update_params = array();
		$key_val_pairs = array();
		
		foreach ($this->_modified as $key) $key_val_pairs[$key] = $key . ' = :' . $key;
		
	
		// Do prepared statement for 'INSERT'...
		$update_handle = $this->dbh->prepare('
                                                    UPDATE products 
                                                    SET last_modified = NOW(), ' . implode (', ', $key_val_pairs) . ', last_modified=now()       
                                                    WHERE product_id = :product_id 
                                                    ');
                
                // Make sure product_id is added to params (it could skipped cause it's never modified...)
		$update_handle->bindParam(':product_id', $_update_params['product_id'], \PDO::PARAM_INT);
                $_update_params['product_id'] = $this->_data['product_id'];
                
                
                // add modified attributes...
		foreach ($this->_modified as $key) {
                    
                        $_update_params[$key] = $this->_data[$key];                        
			$update_handle->bindParam(':'.$key, $_update_params[$key], \Cataleya\Helper\DBH::getTypeConst($_update_params[$key]));

		}
                
                // Execute...
		if (!$update_handle->execute()) $this->e->triggerException('Error in class (' . __CLASS__ . '): [ ' . implode(', ', $update_handle->errorInfo()) . ' ] on line ' . __LINE__);
                
		\Cataleya\System\Event::notify('catalog/product/updated', $this, array ('fields'=>$this->_modified));
        $this->_modified = array ();
		
	}
	
	

        

        public function current()
        {
            return $this->_collection[$this->_position];

        }

          
        
        

	/*
	 *
	 *  [ delete ]
	 * ________________________________________________________________
	 * 
	 * 
	 *
	 *
	 *
	 */
	 
	public function delete () 
	{
 

                $this->_base_images = array ();
                $_collection_ids = $this->getCollectionIDs();
                
                // update collection counts... (this problem will not exist in new iteration)
                static  $update_handle
                ,       $update_handle_param_instance_id
                ,       $update_handle_param_increment;
                
                $update_handle_param_increment = -1;


                if (empty($update_handle)):
                // UPDATE: collection counter...
                $update_handle = $this->dbh->prepare('
                                                    UPDATE collections 
                                                    SET count = count + (:increment)  
                                                    WHERE collection_id = :instance_id 
                                                    ');

                $update_handle->bindParam(':instance_id', $update_handle_param_instance_id, \PDO::PARAM_INT);
                $update_handle->bindParam(':increment', $update_handle_param_increment, \PDO::PARAM_INT);
                endif;
                
                foreach ($_collection_ids as $id) 
                {
                    $update_handle_param_instance_id = $id;
                    
                    if (!$update_handle->execute()) $this->e->triggerException('
                                                                                Error in class (' . __CLASS__ . '): [ ' . 
                                                                                implode(', ', $update_handle->errorInfo()) . 
                                                                                ' ] on line ' . __LINE__);
                }
                
                

                \Cataleya\Helper\DBH::sanitize(array('products'), 'product_id', $this->getID());
                \Cataleya\Helper\DBH::sanitize(array('images'), 'image_id', $this->getPrimaryImageId());
                \Cataleya\Helper\DBH::sanitize(array('content'), 'content_id', $this->_data['description_id']);
                \Cataleya\Helper\DBH::sanitize(array('text'), 'text_id', $this->_data['meta_description_id']);
                \Cataleya\Helper\DBH::sanitize(array('text'), 'text_id', $this->_data['meta_keywords_text_id']);
                \Cataleya\Helper\DBH::sanitize(array('url_rewrite'), 'url_rewrite_id', $this->_data['url_rewrite_id']);
                

                \Cataleya\System::load()->updateProductCount(-1);
                \Cataleya\System\Event::notify('catalog/product/deleted', NULL, array ('product_id'=>$this->getID()));
		
		// $this = NULL;
		return TRUE;
		
		
	}




        
                
      
        
	


	/*
	 *
	 *  [ GETTERS ]
	 * ________________________________________________________________
	 * 
	 * 
	 *
	 *
	 *
	 */
	 




        /*
         *
         * [ getProductId ] 
         *_____________________________________________________
         *
         *
         */

        public function getProductId()
        {
                return (int)$this->_data['product_id'];
        }
        
        
        
        /*
         *
         * [ getID ] 
         *_____________________________________________________
         *
         *
         */

        public function getID()
        {
                return (int)$this->_data['product_id'];
        }
        
        
        
        
        
        
        
        /**
         *
         * @return \Cataleya\Catalog\Product\Type
         *
         */
        public function getProductType()
        {
                if (!($this->_ProductType instanceof \Cataleya\Catalog\Product\Type)) $this->_ProductType = \Cataleya\Catalog\Product\Type::load($this->_data['product_type_id']);
                return $this->_ProductType;
        }
        



        
        
        /*
         *
         * getProductTypeID
         * 
         * Returns Product Type ID..
         *
         * @return: integer
         *
         */

        public function getProductTypeID()
        {
               return $this->_data['product_type_id'];
        }
         




        /**
         * getManufacturer
         *
         * @return \Cataleya\Agent
         */
        public function getManufacturer () 
        {


            return \Cataleya\Agent::load($this->_data['manufacturer_id']) ;

        }



        /**
         * getSupplier
         *
         * @return \Cataleya\Agent
         */
        public function getSupplier () 
        {

            return \Cataleya\Agent::load($this->_data['supplier_id']) ;

        }


        


        /**
         * getBasePrice
         *
         * @return \Cataleya\Catalog\Price
         */
        public function getBasePrice () 
        {

            return \Cataleya\Catalog\Price::getProductPrice($this); 

        }
        
	



	/*
	 *
	 *  [ setDefaultOption ]
	 * ________________________________________________________________
	 * 
	 * 
	 *
	 *
	 *
	 */
	 

	public function setDefaultOption (\Cataleya\Catalog\Product\Option $_Option)
	{
		$this->_data['default_option_id'] = $_Option->getID();
		
		if (!in_array('default_option_id', $this->_modified)) $this->_modified[] = 'default_option_id';
                
                return $this;
		
	}





	/*
	 *
	 *  [ usetDefaultOption ]
	 * ________________________________________________________________
	 * 
	 * 
	 *
	 *
	 *
	 */
	 

	public function unsetDefaultOption ()
	{
		$this->_data['default_option_id'] = NULL;
		
		if (!in_array('default_option_id', $this->_modified)) $this->_modified[] = 'default_option_id';
                
                return $this;
		
	}





	/*
	 *
	 *  [ getDefaultOption ]
	 * ________________________________________________________________
	 * 
	 *
	 *
	 *
	 */
	 

	public function getDefaultOption ()
	{
		if (is_int($this->_data['default_option_id']) && (int)$this->_data['default_option_id'] > 0 )
		{
                    $_opt = \Cataleya\Catalog\Product\Option::load($this->_data['default_option_id']);
                    if (!empty($_opt)) return $_opt;
                    else {
                        $this->_data['default_option_id'] = 0;
                        $this->_modified[] = 'default_option_id';
                        return NULL;
                    }
		}
		
                else if (!empty ($this->_collection)) {
                    $this->_data['default_option_id'] = $this->_collection[0]->getID();
                    $this->_modified[] = 'default_option_id';
                    return $this->_collection[0];
                }
                
                else return NULL;
		
	}




        /*
         *
         * [ getDefaultOptionId ] 
         *_____________________________________________________
         *
         *
         */

        public function getDefaultOptionId()
        {
                return $this->_data['default_option_id'];
        }


        

	



	/*
	 *
	 *  [ setPrimaryImage ]
	 * ________________________________________________________________
	 * 
	 * 
	 *
	 *
	 *
	 */
	 

	public function setPrimaryImage (\Cataleya\Asset\Image $image)
	{
                
		$this->_data['primary_image_id'] = $image->getID();
		$this->_primary_image = $image;
		
		if (!in_array('primary_image_id', $this->_modified)) $this->_modified[] = 'primary_image_id';
		
	}





	/*
	 *
	 *  [ usetPrimaryImage ]
	 * ________________________________________________________________
	 * 
	 * 
	 *
	 *
	 *
	 */
	 

	public function unsetPrimaryImage ()
	{
            
                if (!empty($this->_data['primary_image_id'])):
                $this->addBaseImage(\Cataleya\Asset\Image::load($this->_data['primary_image_id']));
                
		$this->_data['primary_image_id'] = NULL;
		$this->_primary_image = NULL;
		
		if (!in_array('primary_image_id', $this->_modified)) $this->_modified[] = 'primary_image_id';
                endif;
                
                return $this;
		
	}





	/*
	 *
	 *  [ getPrimaryImage ]
	 * ________________________________________________________________
	 * 
	 * Returns image object
	 *
	 *
	 *
	 */
	 

	public function getPrimaryImage ()
	{
		if (empty($this->_primary_image) )
		{
			$this->_primary_image = \Cataleya\Asset\Image::load($this->_data['primary_image_id']);
		}
		
		return $this->_primary_image;
		
	}




        /*
         *
         * [ getPrimaryImageId ] 
         *_____________________________________________________
         *
         *
         */

        public function getPrimaryImageId()
        {
                return $this->_data['primary_image_id'];
        }





	/*
	 *
	 *  [ addBaseImage ]
	 * ________________________________________________________________
	 * 
	 * 
	 *
	 *
	 *
	 */
	 

	public function addBaseImage (\Cataleya\Asset\Image $image)
	{
		
			static 
                                $product_image_insert, 
                                $param_product_id, 
                                $param_image_id;
			
				
			if (!isset($product_image_insert)) {
				// PREPARE SELECT STATEMENT...
				$product_image_insert = $this->dbh->prepare('
													INSERT IGNORE INTO product_images (product_id, image_id)   
													VALUES (:product_id, :image_id)
													');
													
				$product_image_insert->bindParam(':product_id', $param_product_id, \PDO::PARAM_INT);
				$product_image_insert->bindParam(':image_id', $param_image_id, \PDO::PARAM_INT);
			}
			
			
			$param_product_id = $this->_data['product_id'];
			$param_image_id = $image->getID();
			
			if (!$product_image_insert->execute())  $this->e->triggerException('
												Error in class (' . __CLASS__ . '): [ ' . 
												implode(', ', $product_image_insert->errorInfo()) . 
												' ] on line ' . __LINE__);
			
			
			// Reset base image cache (it will be repopulated the next time [getBaseImages] is called.
			$this->_base_images = array ();
                        
                        if ((int)$this->getPrimaryImage()->getID() === 0) $this->setPrimaryImage ($image);
                        
                        return $this;

	}



        
        
	/*
	 *
	 *  [ removeBaseImage ]
	 * ________________________________________________________________
	 * 
	 * 
	 *
	 *
	 *
	 */
	 

	public function removeBaseImage (\Cataleya\Asset\Image $image)
	{

                        
                        
                        // Reset base image cache (it will be repopulated the next time [getBaseImages] is called.
			$this->_base_images = array ();
                        
                        
                        
                        // Destroy image...
                        // MySQL will automatically remove associations with product.
                        $image->destroy();
                        
                        
                        return true;

	}


        
        
        


	/*
	 *
	 *  [ getBaseImages ]
	 * ________________________________________________________________
	 * 
	 * Returns array of image objects
	 *
	 *
	 *
	 */
	 

	public function getBaseImages ()
	{
		if (empty($this->_base_images) )
		{

		
		
			static $product_images_select, $param_product_id;
			
				
			if (!isset($product_images_select)) {
				// PREPARE SELECT STATEMENT...
				$product_images_select = $this->dbh->prepare('
													SELECT * FROM product_images 
													WHERE product_id = :product_id
													');
													
				$product_images_select->bindParam(':product_id', $param_product_id, \PDO::PARAM_INT);
			}
			
			
			$param_product_id = $this->_data['product_id'];
			
			if (!$product_images_select->execute())  $this->e->triggerException('
												Error in class (' . __CLASS__ . '): [ ' . 
												implode(', ', $product_images_select->errorInfo()) . 
												' ] on line ' . __LINE__);
			
			while ($row = $product_images_select->fetch(\PDO::FETCH_ASSOC)) 
			{
				$this->_base_images[] = \Cataleya\Asset\Image::load($row['image_id']);
			}
			
			
		}
		
		return $this->_base_images;
		
	}








	/*
	 *
	 *  [ _loadOptions ]
	 * ________________________________________________________________
	 * 
	 * 
	 *
	 *
	 *
	 */
	 

	private function _loadOptions ($_FORCE_RELOAD = FALSE)
	{
            
            
                // LOAD OPTIONS FROM DB
            
		if (empty($this->_collection) || $_FORCE_RELOAD === TRUE)
		{
                        $this->_collection = array ();
		
		
			static $product_options_select, $param_product_id;
			
				
			if (!isset($product_options_select)) {
				// PREPARE SELECT STATEMENT...
				$product_options_select = $this->dbh->prepare('
                                                                                SELECT * FROM product_options 
                                                                                WHERE product_id = :product_id
                                                                                ');
													
				$product_options_select->bindParam(':product_id', $param_product_id, \PDO::PARAM_INT);
			}
			
			
			$param_product_id = $this->_data['product_id'];
			
			if (!$product_options_select->execute())  $this->e->triggerException('
												Error in class (' . __CLASS__ . '): [ ' . 
												implode(', ', $product_options_select->errorInfo()) . 
												' ] on line ' . __LINE__);
			
                        
			while ($row = $product_options_select->fetch(\PDO::FETCH_ASSOC)) 
			{
                                $this->_collection[] = \Cataleya\Catalog\Product\Option::load($row['option_id']);
                                
                                
			}
	
                       
			
		}
                
                
                $this->pageSetup();
                
                
                return $this;
		
        }
        
        



	/*
	 *
	 *  [ getOptions ]
	 * ________________________________________________________________
	 * 
	 * Returns array of [Option] objects
	 *
	 *
	 *
	 */
	 

	public function getOptions ()
	{
            
          
                $this->_loadOptions();
                
                return $this->_collection;
                
                
		
	}

        
        
        
        
        
        

        
        
        /* ============== ATTRIBUTES ASSOCIATED WITH THIS PRODUCT =============== */
        
        


        /*
         * 
         * 
         * [ _loadAttributes ]
         * ________________________________________________________________
         * 
         * Note: Internal method only...
         * 
         */
        
        private function _loadAttributes ($_FORCE_RELOAD = FALSE)
        {
            
            
                if (!empty($this->_attributes) && $_FORCE_RELOAD !== TRUE) return $this;
                
                
                $this->_attributes = array ();
                
		static 
                        $select_handle, 
                        $select_handle_param_instance_id;
		
		if (empty($select_handle))
		{
			$select_handle = $this->dbh->prepare('
                                                SELECT a.attribute_id, a.attribute_type_id 
                                                FROM product_options o 
                                                INNER JOIN options_to_attributes oa 
                                                ON o.option_id = oa.option_id 
                                                INNER JOIN attributes a 
                                                ON oa.attribute_id = a.attribute_id 
                                                WHERE o.product_id = :instance_id 
                                                ORDER BY a.sort_order
                                                ');
			$select_handle->bindParam(':instance_id', $select_handle_param_instance_id, \PDO::PARAM_INT);
		}
		
		$select_handle_param_instance_id = $this->getID();
		
		if (!$select_handle->execute()) $this->e->triggerException('
										Error in class (' . __CLASS__ . '): [ ' . 
										implode(', ', $select_handle->errorInfo()) . 
										' ] on line ' . __LINE__);
                
                while ($row = $select_handle->fetch(\PDO::FETCH_ASSOC)) 
                {
                
                    $this->_attributes[$row['attribute_id']] = $row;
                }
                
                
                return $this;
                
                
        }
        
        
        
        
        
        


	/*
	 *
	 *  [ getAttributes ]
	 * ________________________________________________________________
	 * 
         * @param: \Cataleya\Catalog\Product\Attribute\Type $_AttributeType
	 * @return: array
	 *
	 *
	 *
	 */
	 

	public function getAttributes (\Cataleya\Catalog\Product\Attribute\Type $_AttributeType = NULL)
	{
            
          
                $this->_loadAttributes();
                
                $_les_enfants = array ();
                $_filter_on = (!empty($_AttributeType));
                     
                foreach ($this->_attributes as $_attrib_id=>$_attrib_data) {
                    if ($_filter_on && (int)$_AttributeType->getID() !== (int)$_attrib_data['attribute_type_id']) continue; 
                    
                    $_les_enfants[$_attrib_id] = \Cataleya\Catalog\Product\Attribute::load($_attrib_id);
                }
                
                
                return $_les_enfants;
                
                
		
	}

        
        


	/*
	 *
	 *  [ hasAttribute ]
	 * ________________________________________________________________
	 * 
         * @param: \Cataleya\Catalog\Product\Attribute $_Attribute
	 * @return: boolean
	 *
	 *
	 *
	 */
	 

	public function hasAttribute (\Cataleya\Catalog\Product\Attribute $_Attribute)
	{
            
          
                $this->_loadAttributes();
                
                return (array_key_exists($_Attribute->getID(), $this->_attributes));
                
                
		
	}

        
        


        
        
        
        
        
    
        
        
       
        /**
         * 
         * @return \Cataleya\Asset\Description
         * 
         */
        public function getDescription()
        {
                if (empty($this->_description)) $this->_description = \Cataleya\Asset\Description::load($this->_data['description_id']);
                return $this->_description;
        } 
        
        
        

	/**
         * 
         * @return \Cataleya\Asset\Content
         * 
         */
	public function getMetaDescription () 
	{
		if (empty($this->_meta_description)) $this->_meta_description = \Cataleya\Asset\Description::load ($this->_data['meta_description_id']);
		return $this->_meta_description;
                
		
	}
        
        
        

        
	/**
	 *
	 * @param string $text
         * @param string $language_code ISO2 Language Code e.g. 'EN', 'FR'
	 *
	 */
	public function setMetaTitle ($text, $language_code) 
	{

		return $this->getMetaDescription()->setTitle($text, $language_code);
                
		
	}
        
        
        
	/**
	 *
	 * @param string $text
         * @param string $language_code ISO2 Language Code e.g. 'EN', 'FR'
	 *
	 */
	public function setMetaDescription ($text, $language_code) 
	{

		return $this->getMetaDescription()->setText($text, $language_code);
                
		
	}
        
        
  
        
        
        
	/**
         * 
         * @return \Cataleya\Asset\Text
         * 
         */
	public function getMetaKeywords () 
	{
		
                if (empty($this->_meta_keywords)) $this->_meta_keywords = \Cataleya\Asset\Text::load($this->_data['meta_keywords_text_id']);
		return $this->_meta_keywords;
		
	}
        
        
        
        

        
	/**
	 *
	 * @param string $text
         * @param string $language_code ISO2 Language Code e.g. 'EN', 'FR'
	 *
	 */
	public function setMetaKeywords ($text, $language_code) 
	{

		return $this->getMetaKeywords()->setText($text, $language_code);
                
		
	}
        
        
        
        
        
        

	

        /**
         *
         *  
         * Will return product URL Rewrite (object)
         * 
         * @return \Cataleya\Asset\URLRewrite
         *
         */
	public function getURLRewrite () 
	{
		if (empty($this->_url_rewrite)) $this->_url_rewrite = \Cataleya\Asset\URLRewrite::load ($this->_data['url_rewrite_id']);
		return $this->_url_rewrite;
		
	}
        
        
        
        
 	/**
         * 
         * @return string
         * 
         */
	public function getHandle () 
	{
		
		return $this->getURLRewrite()->getKeyword();
		
	}




	/**
	 *
	 * @param string $keyword
	 *
	 *
	 *
	 */
	public function setHandle ($keyword = null) 
	{
                if (!is_string($keyword)) return FALSE;
                

		return $this->getURLRewrite()->setKeyword($keyword, TRUE);
                
		
	}



        
        /*
	 *
	 *  [ getProductTagsIDs ]
	 * ________________________________________________________________
	 * 
	 * 
	 *
	 *
	 */
	 
	public function getProductTagIDs () 
	{
                return $this->_loadProductTags();
	}
    
    
    
    
    /*
	 *
	 *  [ getCollectionIDs ]
	 * ________________________________________________________________
	 * 
	 * 
	 *
	 *
	 */
	 
	public function getCollectionIDs () 
	{
            $this->_loadProductTags();
            return $this->_product_collections;
	}



        
        
        
        /*
	 *
	 *  [ _loadProductTags ]
	 * ________________________________________________________________
	 * 
	 * 
	 *
	 *
	 */
	 
	private function _loadProductTags () 
	{

                // LOAD: PRODUCT TAGS
            
               static 
                       $tags_select, 
                       $param_product_id;


               if (!isset($tags_select)) {
                       // PREPARE SELECT STATEMENT...
                       $tags_select = $this->dbh->prepare(''
                               . 'SELECT DISTINCT t.tag_id, t.collection_id '
                               . 'FROM products_to_collections pc '
                               . 'INNER JOIN tags t '
                               . 'ON pc.collection_id = t.collection_id '
                               . 'WHERE pc.product_id = :product_id'
                               . '');

                       $tags_select->bindParam(':product_id', $param_product_id, \PDO::PARAM_INT);

               }


               $param_product_id = $this->_data['product_id'];


               if (!$tags_select->execute()) $this->e->triggerException('
                                                                            Error in class (' . __CLASS__ . '): [ ' . 
                                                                            implode(', ', $tags_select->errorInfo()) . 
                                                                            ' ] on line ' . __LINE__);
               
               $this->_product_tags = $this->_product_collections = [];
               while ($row = $tags_select->fetch(\PDO::FETCH_ASSOC))
               {
                   $this->_product_tags[] = $row['tag_id'];
                   $this->_product_collections[] = $row['collection_id'];
               }                

               
                
		return $this->_product_tags;
	}



        
        
        
        

        
        /*
	 *
	 *  [ _loadSales ]
	 * ________________________________________________________________
	 * 
	 * 
	 *
	 *
	 */
	 
	private function _loadSales () 
	{

                // LOAD: SALES
               static 
                       $select_handle;

               
               if (!isset($select_handle)) {
                       // PREPARE SELECT STATEMENT...
                       $select_handle = $this->dbh->prepare(''
                               . 'SELECT DISTINCT s.sale_id '
                               . 'FROM products_to_collections pc '
                               . 'INNER JOIN sale s '
                               . 'ON pc.collection_id = s.collection_id '
                               . 'WHERE pc.product_id = :product_id'
                               . '');

               }

               $param_product_id = $this->_data['product_id'];
               $select_handle->bindParam(':product_id', $param_product_id, \PDO::PARAM_INT);



               if (!$select_handle->execute()) $this->e->triggerException('
                                                                            Error in class (' . __CLASS__ . '): [ ' . 
                                                                            implode(', ', $select_handle->errorInfo()) . 
                                                                            ' ] on line ' . __LINE__);
               $this->_sales = array ();
               while ($row = $select_handle->fetch(\PDO::FETCH_ASSOC))
               {
                   $this->_sales[] = $row['sale_id'];
               }   
               
               
                
                return $this->_sales;
	}
        
        
        
        

        
       


        
        
        
	/*
	 *
	 *  [ hasTag ]
	 * ________________________________________________________________
	 * 
	 * 
	 *
	 *
	 */
	 
	public function hasTag (\Cataleya\Catalog\Tag $_tag) 
	{

		return (in_array ($_tag->getID(), $this->_product_tags));
			
			
        }
        
        

        
        

        /*
         *
         * [ getProductDateAdded ] 
         *_____________________________________________________
         *
         *
         */

        public function getProductDateAdded()
        {
                return $this->_data['date_added'];
        }        


        
        


        
        

        /*
         *
         * [ getProductLastModified ] 
         *_____________________________________________________
         *
         *
         */

        public function getProductLastModified()
        {
                return $this->_data['last_modified'];
        }







        /*
         *
         * [ getProductDateAvailable ] 
         *_____________________________________________________
         *
         *
         */

        public function getProductDateAvailable()
        {
                return $this->_data['date_available'];
        }




        /*
         *
         * [ getProductWeight ] 
         *_____________________________________________________
         *
         *
         */

        public function getProductWeight()
        {
                return $this->_data['product_weight'];
        }





        /*
         *
         * [ getProductStatus ] 
         *_____________________________________________________
         *
         *
         */

        public function getProductStatus()
        {
                return $this->_data['product_status'];
        }






        /*
         *
         * [ getTotalSold ] 
         *_____________________________________________________
         *
         *
         */

        public function getTotalSold()
        {
                return $this->_data['total_sold'];
        }



        /*
         *
         * [ getProductQuantityOrderMin ] 
         *_____________________________________________________
         *
         *
         */

        public function getProductQuantityOrderMin()
        {
                return $this->_data['quantity_order_min'];
        }








        /*
         *
         * [ getProductIsFree ] 
         *_____________________________________________________
         *
         *
         */

        public function getProductIsFree()
        {
                return $this->_data['is_free'];
        }




        /*
         *
         * [ getProductIsCall ] 
         *_____________________________________________________
         *
         *
         */

        public function getProductIsCall()
        {
                return $this->_data['is_call'];
        }



        
        


        /*
         *
         * [ getProductQuantityOrderMax ] 
         *_____________________________________________________
         *
         *
         */

        public function getProductQuantityOrderMax()
        {
                return $this->_data['quantity_order_max'];
        }





        /*
         *
         * [ getProductSortOrder ] 
         *_____________________________________________________
         *
         *
         */

        public function getProductSortOrder()
        {
                return $this->_data['sort_order'];
        }




        /*
         *
         * [ getHatched ] 
         *_____________________________________________________
         *
         *
         */

        public function getHatched()
        {
                return $this->_data['hatched'];
        }




        







	/*
	 *
	 *  [ SETTERS ]
	 * ________________________________________________________________
	 * 
	 * 
	 *
	 *
	 *
	 */
         
         
         
 /*
 *
 * [ setProductStatus ] 
 *_____________________________________________________
 *
 *
 */

public function setProductStatus($value)
{
	$this->_data['product_status'] = $value;
	$this->_modified[] = 'product_status';

	return TRUE;
}





/*
 *
 * [ setTotalSold ] 
 *_____________________________________________________
 *
 *
 */

public function setTotalSold($value = NULL)
{
        $value = filter_var($value, FILTER_VALIDATE_INT, FILTER_NULL_ON_FAILURE);
        if ($value === NULL) return FALSE;

	$this->_data['total_sold'] = $value;
	$this->_modified[] = 'total_sold';

	return TRUE;
}




/*
 *
 * [ setTotalSold ] 
 *_____________________________________________________
 *
 *
 */

public function updateTotalSold($value = NULL)
{
        $value = filter_var($value, FILTER_VALIDATE_INT, FILTER_NULL_ON_FAILURE);

	$this->_data['total_sold'] += ($value === NULL) ? 1 : $value;
	$this->_modified[] = 'total_sold';

	return TRUE;
}





/*
 *
 * [ setProductQuantityOrderMin ] 
 *_____________________________________________________
 *
 *
 */

public function setProductQuantityOrderMin($value)
{
	$this->_data['quantity_order_min'] = $value;
	$this->_modified[] = 'quantity_order_min';

	return TRUE;
}




/*
 *
 * [ setProductQuantityOrderMax ] 
 *_____________________________________________________
 *
 *
 */

public function setProductQuantityOrderMax($value)
{
	$this->_data['quantity_order_max'] = $value;
	$this->_modified[] = 'quantity_order_max';

	return TRUE;
}












/*
 *
 * [ setProductIsFree ] 
 *_____________________________________________________
 *
 *
 */

public function setProductIsFree($value)
{
	$this->_data['is_free'] = $value;
	$this->_modified[] = 'is_free';

	return TRUE;
}



/*
 *
 * [ setProductIsCall ] 
 *_____________________________________________________
 *
 *
 */

public function setProductIsCall($value)
{
	$this->_data['is_call'] = $value;
	$this->_modified[] = 'is_call';

	return TRUE;
}




/*
 *
 * [ setProductIsAlwaysFreeShipping ] 
 *_____________________________________________________
 *
 *
 */

public function setProductIsAlwaysFreeShipping($value)
{
	$this->_data['is_always_free_shipping'] = $value;
	$this->_modified[] = 'is_always_free_shipping';

	return TRUE;
}




/*
 *
 * [ setProductSortOrder ] 
 *_____________________________________________________
 *
 *
 */

public function setProductSortOrder($value)
{
	$this->_data['sort_order'] = $value;
	$this->_modified[] = 'sort_order';

	return TRUE;
}




/*
 *
 * [ setHatched ] 
 *_____________________________________________________
 *
 *
 */

public function setHatched($value = NULL)
{
        $value = filter_var($value, FILTER_VALIDATE_BOOLEAN, FILTER_NULL_ON_FAILURE);
        if ($value === NULL) return FALSE;
                
	$this->_data['hatched'] = $value;
	$this->_modified[] = 'hatched';

	return TRUE;
}





        /*
         *
         * [ setViews ] 
         *_____________________________________________________
         *
         *
         */

        public function setViews($value = NULL)
        {
                $value = filter_var($value, FILTER_VALIDATE_INT, FILTER_NULL_ON_FAILURE);
                if ($value === NULL) return FALSE;
                
                $this->_data['views'] = $value;
                $this->_modified[] = 'views';

                return TRUE;
        }
        
        

        
        
        /*
         *
         * [ updateViews ] 
         *_____________________________________________________
         *
         *


        public function updateViews($value = NULL)
        {
                $value = filter_var($value, FILTER_VALIDATE_INT, FILTER_NULL_ON_FAILURE);
                
                
                $this->_data['views'] += ($value === NULL) ? 1 : $value;
                $this->_modified[] = 'views';

                return TRUE;
        }


          */       
        
        



        

       

        /**
         * 
         * @return boolean
         * 
         */
        public function isDisplayed()
        {
                return ((int)$this->_data['display_status'] === 1) ? TRUE : FALSE;
        }
        
        
        
        
        /**
         * 
         * @return boolean
         * 
         */
        public function isHidden()
        {
                return ((int)$this->_data['display_status'] === 0) ? TRUE : FALSE;
        }
        
        
        
        

        /**
         * 
         * @return \Cataleya\Catalog\Product
         * 
         */
        public function display () 
        {

            return $this->show();
        }





        
            

        /**
         * 
         * @return \Cataleya\Catalog\Product
         * 
         */
        public function show () 
        {

            if ((int)$this->_data['display_status'] === 0) 
            {
                $this->_data['display_status'] = 1;
                $this->_modified[] = 'display_status';
            }

            return $this;
        }





        /**
         * 
         * @return \Cataleya\Catalog\Product
         * 
         */
        public function hide () 
        {
            if ((int)$this->_data['display_status'] === 1) 
            {
                $this->_data['display_status'] = 0;
                $this->_modified[] = 'display_status';
            }

            return $this;
        }
        
        

        
        
        

 
        
        /**
         * 
         * @return boolean
         * 
         */
        public function isFeatured()
        {
                return ((int)$this->_data['featured'] === 1) ? TRUE : FALSE;
        }
        
        
            

        /**
         * 
         * @return \Cataleya\Catalog\Product
         * 
         */
        public function feature () 
        {

            if ((int)$this->_data['featured'] === 0) 
            {
                $this->_data['featured'] = 1;
                $this->_modified[] = 'featured';
            }

            return $this;
        }





        /**
         * 
         * @return \Cataleya\Catalog\Product
         * 
         */
        public function unFeature () 
        {
            if ((int)$this->_data['featured'] === 1) 
            {
                $this->_data['featured'] = 0;
                $this->_modified[] = 'featured';
            }

            return $this;
        }
        
        

        
        
        
        
        
        
        
        
        
        


        

        public function updateMetaKeywords ()
        {

        // Generate meta keywords...

        // 1. keys from product name
        $keywords = explode(' ', preg_replace('/([^A-Za-z0-9\W]+|[\.]+)/', '', $this->getDescription()->getTitle('EN')));


        
        // 2. Keys from attributes
        
        foreach ($this as $_Option)
        {
           foreach ($_Option as $_Attribute) $keywords[] =  preg_replace('/([^A-Za-z0-9\W]+|[\.]+)/', '', $_Attribute->getValue());
        }

        
        
        

        // 3. Keys from tags
        $tag_ids = $this->getProductTagIDs();

        foreach ($tag_ids as $tag_id)
        {
           $tag_title = \Cataleya\Catalog\Tag::load($tag_id)->getDescription()->getTitle('EN');
           $tag_keys =  explode(' ', preg_replace('/([^A-Za-z0-9\W]+|[\.]+)/', '', $tag_title));

           $keywords = array_merge($keywords, $tag_keys);
        }


        $keywords = array_unique($keywords);

        // 4. Save new keywords
        $this->getMetaDescription()->setText($this->getDescription()->getText('EN'), 'EN');
        $this->getMetaKeywords()->setText(implode(', ', $keywords), 'EN');
        
        
        return $this;

        }







        /*
         * 
         * [ updateFullTextIndex ]
         * 
         */


        public function updateFullTextIndex () 
        {

            static $insert_handle, $param_product_id, $param_keywords;

            // Build the SQL statement.  
            if (empty($insert_handle)) {
                  $insert_handle = $this->dbh->prepare('
                                            INSERT INTO fulltext_catalog_index_isam (product_id, keywords) 
                                            VALUES (:product_id, :keywords ) 
                                            ON DUPLICATE KEY UPDATE keywords = :keywords
                                            ');
                  $insert_handle->bindParam(':product_id', $param_product_id, \PDO::PARAM_INT);
                  $insert_handle->bindParam(':keywords', $param_keywords, \PDO::PARAM_STR);
            }

            // ser params
            $param_product_id = $this->getProductId();
            $param_keywords = preg_replace('/([^A-Za-z0-9\'\-%]+)/', ' ', $this->getMetaKeywords()->getText('EN'));
            $param_keywords .= $this->getReferenceCode() . ' ' . $this->getEAN() . ' ' . $this->getUPC();

            // execute the query.  If successful, return true 
            if (!$insert_handle->execute()) $this->e->triggerException('
                                                                            Error in class (' . __CLASS__ . '): [ ' . 
                                                                            implode(', ', $insert_handle->errorInfo()) . 
                                                                            ' ] on line ' . __LINE__);
            
            
            return $this;

          }

          
          
          
          
          
          /*
           * 
           * [ isOnSale ]
           * ___________________________________________________________________
           * 
           * Returns [ sale ] object if product is on sale in specified [ store ]
           * OR
           * Returns FALSE
           * 
           */
          
          
          public function isOnSale (\Cataleya\Store $_Store, $_return_sale_object=FALSE) 
          {
		static 
				$select_handle, 
                                $param_product_id, 
				$param_store_id;
		
		if (!isset($select_handle)) {
			// PREPARE SELECT STATEMENT...
			$select_handle = $this->dbh->prepare('
                                SELECT a.sale_id 
                                FROM sale a 
                                INNER JOIN products_to_collections b 
                                ON a.collection_id = b.collection_id 
                                WHERE b.product_id = :product_id 
                                AND a.store_id = :store_id 
                                AND a.is_active = 1 
                                AND (a.sale_end > NOW() OR a.indefinite = 1) 
                                LIMIT 1 
                                ');
			$select_handle->bindParam(':product_id', $param_product_id, \PDO::PARAM_INT);
                        $select_handle->bindParam(':store_id', $param_store_id, \PDO::PARAM_INT);
		}
		
		$param_product_id = $this->getID();
		$param_store_id = $_Store->getStoreId();
		
		if (!$select_handle->execute()) $this->e->triggerException('
                                                                            Error in class (' . __CLASS__ . '): [ ' . 
                                                                            implode(', ', $select_handle->errorInfo()) . 
                                                                            ' ] on line ' . __LINE__);

		$row = $select_handle->fetch(\PDO::FETCH_ASSOC);
                
                return (!empty($row)) 
                        ? ($_return_sale_object ? \Cataleya\Sales\Sale::load($row['sale_id']) : TRUE)  
                        : FALSE;
                
                
                
          }
          
          
          
          
          
          ////////////////////////// STATS /////////////////////////////////
          
          
          /*
           * 
           * [ updateViews ]
           * ___________________________________________________________
           * 
           * 
           */
          
          public function updateViews (\Cataleya\Store $_Store) 
          {
                return \Cataleya\Helper\Stats::updateProductTally($_Store, $this, array('views'=>1));
                
          }


          
          /*
           * 
           * [ getViews ]
           * ___________________________________________________________
           * 
           * 
           */
          
          public function getViews (\Cataleya\Store $_Store = NULL, $_duration = 1, $_interval = \Cataleya\Helper\Stats::INTERVAL_MONTH) 
          {
                $_stats = $this->getStats($_Store, $_duration, $_interval);
                return $_stats['tally_views'];                
          }
          
          
          
          
          /*
           * 
           * [ getStats ]
           * ___________________________________________________________
           * 
           * 
           */
          
          public function getStats (\Cataleya\Store $_Store = NULL, $_duration = 1, $_interval = \Cataleya\Helper\Stats::INTERVAL_MONTH) 
          {
                static $_options = array ('duration'=>0, 'interval'=>0, 'id'=>0);
                static $_cache = array ();
                
                if ($_options['duration'] !== $_duration || $_options['interval'] !== $_interval || $_options['id'] !== $this->getID() || empty($_cache)) 
                {
                
                    $_cache = \Cataleya\Helper\Stats::getProductTally($_Store, $this, $_duration, $_interval);
                    $_options['duration'] = $_duration;
                    $_options['interval'] = $_interval;
                    $_options['id'] = $this->getID();
                }
                
                return $_cache;
                
          }
          
          
          
          
          
          
          /*
           * 
           * [ updateQuantitySold ]
           * ___________________________________________________________
           * 
           * 
           */
          
          public function updateQuantitySold (\Cataleya\Store $_Store, $_qty = 1, $_subtotal = 0) 
          {
                $_qty = filter_var($_qty, FILTER_VALIDATE_INT, FILTER_NULL_ON_FAILURE);
                $_subtotal = filter_var($_subtotal, FILTER_VALIDATE_FLOAT, FILTER_NULL_ON_FAILURE);
                
                if ($_qty === NULL || $_subtotal === NULL) \Cataleya\Helper\ErrorHandler::getInstance()->triggerException('Error in class (' . __CLASS__ . '): [ invalid argument for method: ' . __FUNCTION__ . ' ] on line ' . __LINE__);

                  
                return \Cataleya\Helper\Stats::updateProductTally($_Store, $this, array('orders'=>(int)$_qty, 'subtotal'=>(float)$_subtotal));
                
          }


          
          /*
           * 
           * [ getQuantitySold ]
           * ___________________________________________________________
           * 
           * 
           */
          
          public function getQuantitySold (\Cataleya\Store $_Store = NULL, $_duration = 1, $_interval = \Cataleya\Helper\Stats::INTERVAL_MONTH) 
          {
                $_stats = $this->getStats($_Store, $_duration, $_interval);
                return $_stats['tally_orders'];
                
          }
          
          
          
          
          /*
           * 
           * [ updateFavourites ]
           * ___________________________________________________________
           * 
           * 
           */
          
          public function updateFavourites (\Cataleya\Store $_Store) 
          {
                return \Cataleya\Helper\Stats::updateProductTally($_Store, $this, array('wishlists'=>1));
                
          }


          
          /*
           * 
           * [ getFavourites ]
           * ___________________________________________________________
           * 
           * 
           */
          
          public function getFavourites (\Cataleya\Store $_Store = NULL, $_duration = 1, $_interval = \Cataleya\Helper\Stats::INTERVAL_MONTH) 
          {
                $_stats = $this->getStats($_Store, $_duration, $_interval);
                return $_stats['tally_wishlists'];
                
          }
          
          
          
          


  

        
       
         
        /*
	 *
	 *  [ addToProductGroup ]
	 * ________________________________________________________________
	 * 
	 * 
	 *
	 *
	 */
	 
	public function addToProductGroup (\Cataleya\Catalog\Product $_product) 
	{

 		
		if ($this->inProductGroup($_product)) return $this;
			
			
                static 
                                $insert_handle;

                if (empty($insert_handle))
                {
                        $insert_handle = $this->dbh->prepare('
                                                            INSERT INTO product_group (product_id, member_product_id)
                                                            VALUES (:product_id, :member_product_id)  
                                                            ');
                }

                $insert_handle->bindParam(':product_id', $insert_handle_param_product_id, \PDO::PARAM_INT);
                $insert_handle->bindParam(':member_product_id', $insert_handle_param_member_product_id, \PDO::PARAM_INT);
                
                $insert_handle_param_product_id = $this->getID();
                $insert_handle_param_member_product_id = $_product->getID();

                if (!$insert_handle->execute()) $this->e->triggerException('
                                                                        Error in class (' . __CLASS__ . '): [ ' . 
                                                                        implode(', ', $insert_handle->errorInfo()) . 
                                                                        ' ] on line ' . __LINE__);

                
                $this->_product_group[] = $insert_handle_param_product_id;
                

		
		return $this;
		
	}



        
        
        
        
        

	/*
	 *
	 *  [ removeProduct ]
	 * ________________________________________________________________
	 * 
	 * 
	 *
	 *
	 */
	 
	public function removeFromProductGroup (\Cataleya\Catalog\Product $_product) 
	{

 		
		if ($this->inProductGroup($_product))
		{
			
			
			
			static 
					$delete_handle, 
                                        $delete_handle_param_member_product_id, 
                                        $delete_handle_param_product_id;
			
			if (empty($delete_handle))
			{
				$delete_handle = $this->dbh->prepare('
                                                                    DELETE FROM product_group  
                                                                    WHERE member_product_id = :member_product_id 
                                                                    AND product_id = :product_id 
                                                                    ');
				$delete_handle->bindParam(':member_product_id', $delete_handle_param_member_product_id, \PDO::PARAM_INT);
				$delete_handle->bindParam(':product_id', $delete_handle_param_product_id, \PDO::PARAM_INT);
			}
			
                        
			$delete_handle_param_product_id = $this->getID();
			$delete_handle_param_member_product_id = $_product->getID();
	
			if (!$delete_handle->execute()) $this->e->triggerException('
											Error in class (' . __CLASS__ . '): [ ' . 
											implode(', ', $delete_handle->errorInfo()) . 
											' ] on line ' . __LINE__);
											
			
                        foreach ($this->_product_group as $key=>$val)
                        {
                            if ((int)$val === (int)$delete_handle_param_product_id) 
                            {
                                unset ($this->_product_group[$key]);
                                break;
                            }
                        }
				
		} 
		
		return $this;
		
	}
        
        
        
        
        
       
        
	/*
	 *
	 *  [ inProductGroup ]
	 * ________________________________________________________________
	 * 
	 * 
	 *
	 *
	 */
	 
	public function inProductGroup (\Cataleya\Catalog\Product $_product) 
	{

                $this->_loadProductGroup (FALSE);
		return (in_array ((int)$_product->getID(), $this->_product_group));
			
			
        }
        
        
        
        
        
	/*
	 *
	 *  [ getProductGroup ]
	 * ________________________________________________________________
	 * 
	 * 
	 *
	 *
	 */
	 
	public function getProductGroup ($_FORCE_RELOAD = FALSE) 
	{
                $_collection = array ();
                $this->_loadProductGroup ($_FORCE_RELOAD);
                
                foreach ($this->_product_group as $_id) $_collection[] = \Cataleya\Catalog\Product::load ($_id);
                
		return $_collection;
			
        }
        
        
        
        
        

        
        /*
	 *
	 *  [ _loadProductGroup ]
	 * ________________________________________________________________
	 * 
	 * 
	 *
	 *
	 */
	 
	private function _loadProductGroup ($_FORCE_RELOAD = FALSE) 
	{

               if ($_FORCE_RELOAD === FALSE && !empty($this->_product_group)) return $this->_product_group;
                // LOAD
               static 
                       $select_handle;


               if (!isset($select_handle)) {
                       // PREPARE SELECT STATEMENT...
                       $select_handle = $this->dbh->prepare('SELECT member_product_id FROM product_group WHERE product_id = :product_id');

               }

               $param_product_id = $this->_data['product_id'];
               $select_handle->bindParam(':product_id', $param_product_id, \PDO::PARAM_INT);



               if (!$select_handle->execute()) $this->e->triggerException('
                                                                            Error in class (' . __CLASS__ . '): [ ' . 
                                                                            implode(', ', $select_handle->errorInfo()) . 
                                                                            ' ] on line ' . __LINE__);

               $this->_product_group = array ();
               while ($row = $select_handle->fetch(\PDO::FETCH_ASSOC))
               {
                   $this->_product_group[] = (int)$row['member_product_id'];
               }   
                
                return $this->_product_group;
	}
        
        
        
        




        /*
         *
         * [ getReferenceCode ] 
         *_____________________________________________________
         *
         *
         */

        public function getReferenceCode()
        {
                return $this->_data['reference'];
        }



        /*
         *
         * [ setReferenceCode ] 
         *_____________________________________________________
         *
         *
         */

        public function setReferenceCode($value = NULL)
        {

                $value = \Cataleya\Helper\Validator::referencecode($value, 1, 200);
                if (empty($value)) $this->e->triggerException('Error in class (' . __CLASS__ . '): [ invalid argument for method: ' . __FUNCTION__ . ' ] on line ' . __LINE__);

                $this->_data['reference'] = strtoupper($value);
                $this->_modified[] = 'reference';

                return $this;
        }



        /*
         *
         * [ getEAN ] 
         *_____________________________________________________
         *
         *
         */

        public function getEAN()
        {
                return $this->_data['ean'];
        }



        /*
         *
         * [ setEAN ] 
         *_____________________________________________________
         *
         *
         */

        public function setEAN($value = NULL)
        {

                $value = \Cataleya\Helper\Validator::referencecode($value, 1, 200);
                if (empty($value)) $this->e->triggerException('Error in class (' . __CLASS__ . '): [ invalid argument for method: ' . __FUNCTION__ . ' ] on line ' . __LINE__);

                $this->_data['ean'] = strtoupper($value);
                $this->_modified[] = 'ean';

                return $this;
        }



        /*
         *
         * [ getUPC ] 
         *_____________________________________________________
         *
         *
         */

        public function getUPC()
        {
                return $this->_data['upc'];
        }



        /*
         *
         * [ setUPC ] 
         *_____________________________________________________
         *
         *
         */

        public function setUPC($value = NULL)
        {

                $value = \Cataleya\Helper\Validator::referencecode($value, 1, 200);
                if (empty($value)) $this->e->triggerException('Error in class (' . __CLASS__ . '): [ invalid argument for method: ' . __FUNCTION__ . ' ] on line ' . __LINE__);

                $this->_data['upc'] = strtoupper($value);
                $this->_modified[] = 'upc';

                return $this;
        }


  


}
	
	
	
	
