<?php


namespace Cataleya\Catalog;



/*

CLASS PRICE RANGE

*/



class PriceRange  
{
	

    private $_data = array();
    private $_Store;





    /*
     *
     * [ __construct ]
     * ________________________________________________________________
     *
     *
     *
     */


    private function __construct (\Cataleya\Store $_Store, $MIN = 0, $MAX = 100)
    {

            $currency = \Cataleya\Locale\Currency::load($_Store->getCurrencyCode());
            $value = $value / floatval($currency->getRate());

            $this->_data['min'] = (float)$MIN / floatval($currency->getRate()); 
            $this->_data['max'] = (float)$MAX / floatval($currency->getRate());
            

            $this->_Store = $_Store;

    }





    /*
     *
     * [ __destruct ]
     * ________________________________________________________________
     *
     *
     *
     */

    public function __destruct()
    {

    }





    /*
     *
     * [ load ]
     * ________________________________________________________________
     *
     *
     *
     */


    public static function load  (\Cataleya\Store $_Store, $MIN = 0, $MAX = 100) 
    {



            $_obj = __CLASS__;
            $instance = new $_obj ($_Store, $MIN, $MAX);


            return $instance;


    }







    /*
     *
     * [ getID ] 
     *_____________________________________________________
     *
     *
     */

    public function getID()
    {
            return $this->_Store->getID();
    }



        


    /*
     *
     * [ getMin ] 
     *_____________________________________________________
     *
     *
     */

    public function getMin()
    {
            return $this->_data['min'];
    }






    /*
     *
     * [ setMin ] 
     *_____________________________________________________
     *
     *
     */

    public function setMin($value = NULL)
    {
            $currency = \Cataleya\Locale\Currency::load($this->_Store->getCurrencyCode());
            $this->_data['min'] = (float)$value / floatval($currency->getRate());

            return $this;
    }







    /*
     *
     * [ getMax ] 
     *_____________________________________________________
     *
     *
     */

    public function getMax()
    {
            return $this->_data['max'];
    }






    /*
     *
     * [ setMax ] 
     *_____________________________________________________
     *
     *
     */

    public function setMax($value = NULL)
    {

            $currency = \Cataleya\Locale\Currency::load($this->_Store->getCurrencyCode());
            $this->_data['max'] = (float)$value / floatval($currency->getRate());

            return $this;
    }








        





}
	
	
	
	
