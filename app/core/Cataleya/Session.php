<?php


namespace Cataleya;



/*

CLASS

*/




// if (!defined('IS_ADMIN_FLAG')) {
//   die('Illegal Access');
// }




class Session 
implements \Cataleya\System\Event\Observable {

	

        
	private $dbh, $e;
    private static $_session_life = 1440;
    private static $_instance;
    private static $_session_status = FALSE;




    protected static $_events = [
            'session/error'
        ];




    public function __construct() {

    // Get database handle...
    $this->dbh = \Cataleya\Helper\DBH::getInstance();

    // Get error handler
    $this->e = \Cataleya\Helper\ErrorHandler::getInstance();


    session_set_save_handler(
        array($this, "open"),
        array($this, "close"),
        array($this, "read"),
        array($this, "write"),
        array($this, "destroy"),
        array($this, "gc")
    );



    }


        
    

	/*
	 *
	 * [ load ]
	 * ________________________________________________________________
	 *
	 *
	 *
	 */
	 
	 
	public static function load ()
	{
            
            if (empty(self::$_instance)) 
            {
                $_objct = __CLASS__;
                self::$_instance = new $_objct;
            }
            
            return self::$_instance;
                
        }
        
        
    
    /**
     * getEvents
     * 
     * @return array
     */
    public static function getEvents() {

        return self::$_events;
    }
        

        
        /*
         * 
         * 
         *  [ setSessionLifeTime ]
         * 
         * 
         */
        
        
        public static function setSessionLifeTime ($_value = NULL) 
        {
            $_value = filter_var($_value, FILTER_VALIDATE_INT);
            if ($_value === FALSE) return FALSE;
            
            self::$_session_life = $_value;
        }

                /** 
        * Open function is the constructor, it prepares the session connection 
        * 
        * @param string $savePath The storage location of the session. Unnecessary for our operations 
        * @param string $sessionId The ID of the session, unnecessary in the constructor for us 
        * @return bool 
        */ 
       public function open( $savePath, $sessionID ) 
        { 
                self::$_session_status = TRUE;
                return true;

        } 

        /** 
        * The close function destroys the open database connection. 
        * 
        * This function is unnecessary for our purposes, since open database connections are killed automatically by PHP at the end of scripts. 
        * However, to be explicit I'm including the proper contents here. Note that this function MUST EXIST, but could easily be empty. 
        * 
        * @return bool 
        */ 
       public function close( ) 
        { 
          self::$_session_status = FALSE;
          return true; 
        } 


        /** 
        * The read function is what actually loads the session from the database. 
        * 
        * @param string $sessionID The 32-character session ID, from $_COOKIE['PHPSESSID'] 
        * @return string 
        */ 
       public function read( $sessionID ) 
        { 


            static $read_select, $param_sesskey;

            // prepare select statement...
            if (!isset($read_select)) {
            $read_select = $this->dbh->prepare('SELECT value FROM sessions WHERE sesskey = :sesskey'); 
            $read_select->bindParam(':sesskey', $param_sesskey, \PDO::PARAM_STR);
            }

            $param_sesskey = $sessionID;

            // execute...
            if (!$read_select->execute()) return '';
            //if the query succeeded... 
            $sess_data = $read_select->fetch(\PDO::FETCH_ASSOC);

            if ( !empty($sess_data) ) return \Cataleya\Helper\Bcrypt::decrypt($sess_data['value']); 
            else return '';  
        } 

        /** 
        * The write function writes the existing session data to the database AND UPDATES the most recent timestamp 
        * 
        * @param string $sessionID The 32-character session ID, from $_COOKIE['PHPSESSID'] 
        * @param string $sessionData The serialized contents of the session 
        * @return bool 
        */ 
        function write( $sessionID, $sessionData ) 
        { 

            static $sess_insert;

            // Build the SQL statement.  Note that since we have the session ID set up as a primary key, we can use the INSERT ... ON DUPLICATE KEY UPDATE syntax: 
            if (empty($sess_insert)) {
                  $sess_insert = $this->dbh->prepare('INSERT INTO sessions (sesskey, value, expiry) VALUES (:sesskey, :value, NOW() ) ' . 
              'ON DUPLICATE KEY UPDATE value = :value, expiry = NOW()');
            }
            

            // ser params
            $param_sesskey = $sessionID;
            $param_value = \Cataleya\Helper\Bcrypt::encrypt($sessionData);
            
            $sess_insert->bindParam(':sesskey', $param_sesskey, \PDO::PARAM_STR);
            $sess_insert->bindParam(':value', $param_value, \PDO::PARAM_STR);


            // execute the query.  If successful, return true 
            if ($sess_insert->execute()) return true;
            else { 
                \Cataleya\System\Event::notify('session/error', $this, array (
                    'logger_id'=>'error.log', 
                    'blob'=> 'Unable to write session data.'.' in '. $_SERVER['SCRIPT_NAME'], 
                    'save_now'=>TRUE
                        ));
                return false; 
                
            }

        } 

        /** 
        * Destroys the session ID passed in whenever session_destroy() is called by your scripts 
        * 
        * @param string $sessionID The 32-character session ID, from $_COOKIE['PHPSESSID'] 
        * @return bool 
        */ 
        public function destroy( $sessionID ) 
        { 

          static $sess_delete, $param_sesskey;

          // prepare delete statement...
          if (empty($sess_delete)) {
                $sess_delete = $this->dbh->prepare('DELETE FROM sessions WHERE sesskey = :sesskey');
                $sess_delete->bindParam(':sesskey', $param_sesskey, \PDO::PARAM_STR);
          }


          // set params
          $param_sesskey = $sessionID;

          // execute the query.  If successful, return true 
          if ($sess_delete->execute()) return true;
          else return false;

        } 

        /** 
        * Garbage collection happens at random, and destroys ALL sessions older than the lifetime passed in (in seconds) 
        * 
        * @param int $sessionMaxLifetime The maximum lifetime (in seconds) of sessions in your storage engine 
        * @return bool 
        */ 
        public function gc( $sessionMaxLifetime ) 
        { 

          static $sess_delete, $param_sess_max_lifetime;



          // prepare delete statement...
          if (empty($sess_delete)) {
              
                $sess_delete = $this->dbh->prepare('DELETE FROM sessions WHERE expiry < DATE_SUB( NOW(), INTERVAL :sess_max_lifetime second )');
                $sess_delete->bindParam(':sess_max_lifetime', $param_sess_max_lifetime, \PDO::PARAM_INT);
                
          }


          // set params (using our own custom session expiry time...
          $param_sess_max_lifetime = empty(self::$_session_life) ? $sessionMaxLifetime : self::$_session_life;

          // execute the query.  If successful, return true 
          if ($sess_delete->execute()) return true;
          else return false;


        } 

        
        
        // Check if a session is active
        
        public static function sessionStatus() 
        {
            return self::$_session_status;
        }



        // MISC SESSION FUNCS ////////////



        
        // Getting rid of session cookies properly...
        public static function kill_session($session_name = '') {
                $session_name = empty($session_name) ? SESSION_NAME : $session_name;
                
                if (!self::sessionStatus()) 
                {
                    session_name($session_name);
                    session_start();
                }
                    
                $params = session_get_cookie_params();
                setcookie(session_name($session_name), '', time() - 42000,
                        $params["path"], $params["domain"],
                        $params["secure"], $params["httponly"]
                );

                session_unset();
                session_destroy();
        }				



        // Set strict session cookie

        public static function init_session_cookie($_options = array ()) 
        {
            $_options['path'] = !isset($_options['path']) ? '/' : $_options['path'];

            $_domain = (defined('DOMAIN')) ? DOMAIN : $_SERVER['HTTP_HOST'];
            $_ssl = (defined('SSL_REQUIRED') && SSL_REQUIRED) ? TRUE : FALSE;
            
            session_set_cookie_params ( 0, $_options['path'], $_domain, $_ssl, TRUE );

        }







}


	
	
	
