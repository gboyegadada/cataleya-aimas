<?php


namespace Cataleya\Asset;



/*

CLASS

*/



class Contact   
{


    private $_data = array();
    private $_modified = array();
    private $_country_obj;

    private $dbh, $e;





    /*
     *
     * [ __construct ]
     * ________________________________________________________________
     *
     *
     *
     */


    private function __construct ()
    {

            // Get database handle...
            $this->dbh = \Cataleya\Helper\DBH::getInstance();

            // Get error handler
            $this->e = \Cataleya\Helper\ErrorHandler::getInstance();

    }





    /*
     *
     * [ __destruct ]
     * ________________________________________________________________
     *
     *
     *
     */

    public function __destruct()
    {
                $this->saveData();
    }





    /**
     * 
     * @param int $id
     * @return \Cataleya\Asset\Contact
     * 
     */
    public static function load ($id = 0)
    {

            if ($id == 0 || filter_var($id, FILTER_VALIDATE_INT) == FALSE) return NULL;

            $_objct = __CLASS__;
            $instance = new $_objct ();

            // LOAD DATA
            static $select_handle, $param_instance_id;


            if (empty($select_handle)) {
                    // PREPARE SELECT STATEMENT...
                    $select_handle = $instance->dbh->prepare('SELECT * FROM address_book WHERE contact_id = :instance_id LIMIT 1');
                    $select_handle->bindParam(':instance_id', $param_instance_id, \PDO::PARAM_INT);
            }


            $param_instance_id = $id;

            if (!$select_handle->execute()) $instance->e->triggerException('
                                                                                            Error in class (' . __CLASS__ . '): [ ' . 
                                                                                            implode(', ', $select_handle->errorInfo()) . 
                                                                                            ' ] on line ' . __LINE__);

            $instance->_data = $select_handle->fetch(\PDO::FETCH_ASSOC);

            if (empty($instance->_data))
            {
                    unset($instance);
                    return NULL;
            }



            return $instance;	


    }



    
    /**
     * 
     * @param \Cataleya\Geo\Country|\Cataleya\Geo\Province $_country_or_province
     * @param array $_details
     * @return \Cataleya\Asset\Contact
     * 
     */
    static public function create ($_country_or_province = null, $_details = []) 
    {




            $_Country = $_Province = null;

            // Country ...
            if ($_country_or_province instanceof \Cataleya\Geo\Country) 
            {
                if ($_country_or_province->hasProvinces()) throw new Error ('' . 
                    'First argument must be a Province of the country ' . 
                    'you specified - required if a country has provinces.'
                );
                else $_Country = $_country_or_province;
            }

            // Province...
            else if ($_country_or_province instanceof \Cataleya\Geo\Province) 
            {
                $_Country = $_country_or_province->getCountry();
                $_Province = $_country_or_province;
            }

            // Invalid argument...
            else 
            {
                $_Country = \Cataleya\Geo\Country::load('NG');
                $_Province = \Cataleya\Geo\Province::loadByISO('LA', 'NG');
            }                


                
            /*
             * Create location attribute 
             *
             */

             $_defaults = array (
                'gender'  =>  'undisclosed', 
                'firstname'   => '',
                'lastname'    =>  '',
                'company' =>  '',
                'email' =>  '',
                'work_phone' =>  '',
                'home_phone' =>  '',
                'fax' => ''
             );


             if (!is_array($_details)) $_details = array ();
             foreach ($_defaults as $k => $v) if (!isset($_details[$k])) {
                 $_details[$k] = $v;
             } 



            if (!isset($_details['label']) || !is_string($_details['label'])) $_details['label'] = 'New Entry - '. date(\DateTime::RSS);
            $_details['label'] = preg_replace('/[^-\s\.A-Za-z0-9@,]+/', '', $_details['label']);

            

            // Get database handle...
            $dbh = \Cataleya\Helper\DBH::getInstance();

            // Get error handler
            $e = \Cataleya\Helper\ErrorHandler::getInstance();


            if (isset($_details['email'])) $_details['email'] = \Cataleya\Helper\Validator::email($_details['email']);

            $_params = array ( 
                'label' => $_details['label'], 
                'gender'  =>  (isset($_details['gender']) && in_array($_details['gender'], ['male', 'female', 'undisclosed'])) ? 'undisclosed' : $_details['gender'], 
                'firstname'   => \Cataleya\Helper\Validator::string($_details['firstname'], 0, 120),
                'lastname'    =>  \Cataleya\Helper\Validator::string($_details['lastname'], 0, 120),
                'company' =>  \Cataleya\Helper\Validator::string($_details['company'], 0, 120),
                'email' =>  \Cataleya\Helper\Validator::email($_details['email']),
                'work_phone' => \Cataleya\Helper\Validator::phone($_details['work_phone'], 1, 34),
                'home_phone' => \Cataleya\Helper\Validator::phone($_details['home_phone'], 1, 34),
                'fax' => \Cataleya\Helper\Validator::phone($_details['fax'], 1, 34), 
                'province_id' => (!empty($_Province)) ? $_Province->getProvinceId() : NULL, 
                'country_code' => $_Country->getCountryCode()
            );


            $_col_str = $_val_str = [];

            foreach ($_params as $key=>$val) {

                 If (!is_numeric($val) && !is_string($val) && !is_null($val)) $_params[$key] = $val= '';
                 $_col_str[] = $key;
                 $_val_str[] = ':'.$key;
            }

            // construct
            static $insert_handle = NULL;

            if (empty($insert_handle))
            {
                    $insert_handle = $dbh->prepare('
                                INSERT INTO address_book 
                                (' . implode(', ', $_col_str) . ') 
                                VALUES (' . implode(', ', $_val_str) . ')
                                ');
            }


            foreach ($_params as $key=>$val) {


                    $insert_handle->bindParam(
                            ':'.$key, 
                            $_params[$key], 
                            \Cataleya\Helper\DBH::getTypeConst($val)
                            );

            }



            if (!$insert_handle->execute()) {
                
                $e->triggerException('
                            Error in class (' . __CLASS__ . '): [ ' . 
                            implode(', ', $insert_handle->errorInfo()) . 
                            ' ] on line ' . __LINE__ );
            }


            // AUTOLOAD NEW PRODUCT AND RETURN IT
            $instance = self::load($dbh->lastInsertId());

            return $instance;


    }



     /*
     *  [ delete ]
     * ________________________________________________________________
     * 
     * 
     *
     *
     */


    public function delete () 
    {

            // Cache location atribute
            // $_Location = $this->getLocation();

            // DELETE 
            $delete_handle = $this->dbh->prepare('
                                    DELETE FROM address_book
                                    WHERE contact_id = :instance_id
                                    ');

            $param_instance_id = $this->getID();
            $delete_handle->bindParam(
                                    ':instance_id', 
                                    $param_instance_id, 
                                    \PDO::PARAM_INT
                                    );


            if (!$delete_handle->execute()) $this->e->triggerException('
                            Error in class (' . __CLASS__ . '): [ ' . 
                            implode(', ', $delete_handle->errorInfo()) . 
                            ' ] on line ' . __LINE__);

            // delete location attribute
            // $_Location->delete();


            return TRUE;


    }





    /*
     *
     * [ getID ] 
     *_____________________________________________________
     *
     *
     */

    public function getID()
    {
            return $this->_data['contact_id'];
    }






    public function saveData () 
    {


            if (empty($this->_modified)) return;
            $this->_modified = array_unique($this->_modified);

            $_update_params = array();
            $key_val_pairs = array();

            foreach ($this->_modified as $key) $key_val_pairs[$key] = $key . 
                                                ' = :' . $key;


            // Do prepared statement for 'INSERT'...
            $update_handle = $this->dbh->prepare('
                                    UPDATE address_book 
                                    SET ' . implode (', ', $key_val_pairs) . '    
                                    WHERE contact_id = :instance_id 
                                    ');

            $update_handle->bindParam(
                                    ':instance_id', 
                                    $_update_params['contact_id'], 
                                    \PDO::PARAM_INT
                                    );
            $_update_params['contact_id'] = $this->_data['contact_id'];

            foreach ($this->_modified as $key) {

                    $_update_params[$key] = $this->_data[$key];                        
                    $update_handle->bindParam(
                            ':'.$key, $_update_params[$key], 
                            \Cataleya\Helper\DBH::getTypeConst($_update_params[$key]));

            }




            if (!$update_handle->execute()) $this->e->triggerException(
                    'Error in class (' . __CLASS__ . 
                    '): [ ' . implode(', ', $update_handle->errorInfo()) . 
                    ' ] on line ' . __LINE__);

    }









    /*
     *
     * [ getID ] 
     *_____________________________________________________
     *
     *
     */

    public function getContactID()
    {
            return $this->getID();
    }







    /*
     *
     * [ getEntryLabel ] 
     *_____________________________________________________
     *
     *
     */

    public function getEntryLabel()
    {
            return $this->_data['label'];
    }



    /*
     *
     * [ setEntryLabel ] 
     *_____________________________________________________
     *
     *
     */

    public function setEntryLabel($value)
    {
            $value = \Cataleya\Helper\Validator::string($value, 0, 120);
            if ($value===FALSE)  $this->e->triggerException('Error in class (' . 
                    __CLASS__ . '): [ invalid argument for method: ' . 
                    __FUNCTION__ . ' ] on line ' . __LINE__
                    );
            
            $this->_data['label'] = $value;
            $this->_modified[] = 'label';

            return $this;
    }






    /*
     *
     * [ getGender ] 
     *_____________________________________________________
     *
     *
     */

    public function getEntryGender()
    {
            return $this->_data['gender'];
    }



    /*
     *
     * [ setGender ] 
     *_____________________________________________________
     *
     *
     */

    public function setEntryGender($value = NULL)
    {

            if (!in_array($value, array('male', 'female', 'm', 'f', 'undisclosed')))  $this->e->triggerException('Error in class (' . 
                    __CLASS__ . '): [ invalid argument for method: ' . 
                    __FUNCTION__ . ' ] on line ' . __LINE__
                    );
            $this->_data['gender'] = $value;
            $this->_modified[] = 'gender';

            return $this;
    }



    /*
     *
     * [ getCompany ] 
     *_____________________________________________________
     *
     *
     */

    public function getEntryCompanyName()
    {
            return $this->_data['company'];
    }



    /*
     *
     * [ setCompany ] 
     *_____________________________________________________
     *
     *
     */

    public function setEntryCompanyName($value = NULL)
    {


            $value = \Cataleya\Helper\Validator::string($value, 0, 120);
            if ($value===FALSE)  $this->e->triggerException('Error in class (' . 
                    __CLASS__ . '): [ invalid argument for method: ' . 
                    __FUNCTION__ . ' ] on line ' . __LINE__
                    );

            $this->_data['company'] = $value;
            $this->_modified[] = 'company';

            return $this;
    }
    
    
    

    
    
    
    
    
    /*
     *
     * [ getName ] 
     *_____________________________________________________
     *
     *
     */

    public function getEntryName($_last_name_first = FALSE)
    {
            return ($_last_name_first) 
                    ? $this->_data['lastname'] . ' ' . $this->_data['firstname'] 
                    : $this->_data['firstname'] . ' ' . $this->_data['lastname'];
    }

    
    
    
    /*
     *
     * [ setName ] 
     *_____________________________________________________
     *
     *
     */

    public function setEntryName($value = NULL)
    {

        if (is_string($value)):
            
            $_tmp = explode(' ', trim($value), 2);

            // First name...
            if (isset($_tmp[0])) $this->setEntryFirstname ($_tmp[0]);

            // Last name...
            if (isset($_tmp[1])) $this->setEntryLastname ($_tmp[1]);
            
        endif;

        return $this;
    }




    /*
     *
     * [ getFirstname ] 
     *_____________________________________________________
     *
     *
     */

    public function getEntryFirstname()
    {
            return $this->_data['firstname'];
    }



    /*
     *
     * [ setFirstname ] 
     *_____________________________________________________
     *
     *
     */

    public function setEntryFirstname($value = NULL)
    {


            $value = \Cataleya\Helper\Validator::string($value, 0, 120);
            if ($value===FALSE)  $this->e->triggerException('Error in class (' . 
                    __CLASS__ . '): [ invalid argument for method: ' . 
                    __FUNCTION__ . ' ] on line ' . __LINE__
                    );

            $this->_data['firstname'] = $value;
            $this->_modified[] = 'firstname';

            return $this;
    }



    /*
     *
     * [ getLastname ] 
     *_____________________________________________________
     *
     *
     */

    public function getEntryLastname()
    {
            return $this->_data['lastname'];
    }



    /*
     *
     * [ setLastname ] 
     *_____________________________________________________
     *
     *
     */

    public function setEntryLastname($value = NULL)
    {

            $value = \Cataleya\Helper\Validator::string($value, 0, 120);
            if ($value===FALSE) 
                    $this->e->triggerException('Error in class (' . 
                    __CLASS__ . '): [ invalid argument for method: ' . 
                    __FUNCTION__ . ' ] on line ' . __LINE__
                    );

            $this->_data['lastname'] = $value;
            $this->_modified[] = 'lastname';

            return $this;
    }






    /*
     *
     * [ getEmail ] 
     *_____________________________________________________
     *
     *
     */

    public function getEntryEmail()
    {
            return $this->_data['email'];
    }



    /*
     *
     * [ setEmail ] 
     *_____________________________________________________
     *
     *
     */

    public function setEntryEmail($value = NULL)
    {


            $value = \Cataleya\Helper\Validator::email($value);
            if ($value===FALSE)  
                    $this->e->triggerException('Error in class (' . 
                    __CLASS__ . '): [ invalid argument for method: ' . 
                    __FUNCTION__ . ' ] on line ' . __LINE__
                    );

            $this->_data['email'] = $value;
            $this->_modified[] = 'email';

            return $this;
    }





    /*
     *
     * [ getWebsite ] 
     *_____________________________________________________
     *
     *
     */

    public function getEntryWebsite()
    {
            return $this->_data['website'];
    }



    /*
     *
     * [ setWebsite ] 
     *_____________________________________________________
     *
     *
     */

    public function setEntryWebsite($value = NULL)
    {

            if (!empty($value)):
            $value = \Cataleya\Helper\Validator::url($value);
            if ($value===FALSE)  $this->e->triggerException('Error in class (' . 
                    __CLASS__ . '): [ invalid argument for method: ' . 
                    __FUNCTION__ . ' ] on line ' . __LINE__
                    );
            endif;

            $this->_data['website'] = empty($value) ? '' : $value;
            $this->_modified[] = 'website';

            return $this;
    }

    
    


    /*
     *
     * [ getURL ] 
     *_____________________________________________________
     *
     *
     */

    public function getEntryURL()
    {
            return $this->getWebsite();
    }



    /*
     *
     * [ setURL ] 
     *_____________________________________________________
     *
     *
     */

    public function setEntryURL($value = NULL)
    {
            return $this->setWebsite($value);
    }

    
    
    
    



    /*
     *
     * [ getWorkPhone ] 
     *_____________________________________________________
     *
     *
     */

    public function getEntryWorkPhone()
    {
            return $this->_data['work_phone'];
    }



    /*
     *
     * [ setWorkPhone ] 
     *_____________________________________________________
     *
     *
     */

    public function setEntryWorkPhone($value = NULL)
    {

            if (!empty($value)):
            $value = \Cataleya\Helper\Validator::phone($value, 1, 34);
            if ($value===FALSE) 
                    $this->e->triggerException('Error in class (' . 
                    __CLASS__ . '): [ invalid argument for method: ' . 
                    __FUNCTION__ . ' ] on line ' . __LINE__
                    );
            endif;

            $this->_data['work_phone'] = empty($value) ? '' : $value;
            $this->_modified[] = 'work_phone';

            return $this;
    }








    /*
     *
     * [ getHomePhone ] 
     *_____________________________________________________
     *
     *
     */

    public function getEntryHomePhone()
    {
            return $this->_data['home_phone'];
    }



    /*
     *
     * [ setHomePhone ] 
     *_____________________________________________________
     *
     *
     */

    public function setEntryHomePhone($value = NULL)
    {

            if (!empty($value)):
            $value = \Cataleya\Helper\Validator::phone($value, 1, 34);
            if ($value===FALSE) 
                    $this->e->triggerException('Error in class (' . 
                    __CLASS__ . '): [ invalid argument for method: ' . 
                    __FUNCTION__ . ' ] on line ' . __LINE__
                    );
            endif;

            $this->_data['home_phone'] = empty($value) ? '' : $value;
            $this->_modified[] = 'home_phone';

            return $this;
    }












    /*
     *
     * [ getFax ] 
     *_____________________________________________________
     *
     *
     */

    public function getEntryFax()
    {
            return $this->_data['home_phone'];
    }



    /*
     *
     * [ setFax ] 
     *_____________________________________________________
     *
     *
     */

    public function setEntryFax($value = NULL)
    {

            if (!empty($value)):
            $value = \Cataleya\Helper\Validator::phone($value, 1, 34);
            if ($value===FALSE) 
                    $this->e->triggerException('Error in class (' . 
                    __CLASS__ . '): [ invalid argument for method: ' . 
                    __FUNCTION__ . ' ] on line ' . __LINE__
                    );
            endif;

            $this->_data['fax'] = empty($value) ? '' : $value;
            $this->_modified[] = 'fax';

            return $this;
    }











    /*
     *
     * [ getEntryStreetAddress ] 
     *_____________________________________________________
     *
     *
     */

    public function getEntryStreetAddress()
    {
            return $this->_data['street_address1'];
    }



    /*
     *
     * [ setEntryStreetAddress ] 
     *_____________________________________________________
     *
     *
     */

    public function setEntryStreetAddress($value)
    {
            if (!empty($value)):
            $value = \Cataleya\Helper\Validator::address($value, 1, 150);
            if ($value===FALSE) 
                    $this->e->triggerException('Error in class (' . 
                    __CLASS__ . '): [ invalid argument for method: ' . 
                    __FUNCTION__ . ' ] on line ' . __LINE__
                    );
            endif;
            
            $this->_data['street_address1'] = empty($value) ? '' : $value;
            $this->_modified[] = 'street_address1';

            return $this;
    }






    /*
     *
     * [ getEntryStreetAddress2 ] 
     *_____________________________________________________
     *
     *
     */

    public function getEntryStreetAddress2()
    {
            return $this->_data['street_address2'];
    }



    /*
     *
     * [ setEntryStreetAddress2 ] 
     *_____________________________________________________
     *
     *
     */

    public function setEntryStreetAddress2($value)
    {
            if (!empty($value)):
            $value = \Cataleya\Helper\Validator::address($value, 1, 150);
            if ($value===FALSE) 
                    $this->e->triggerException('Error in class (' . 
                    __CLASS__ . '): [ invalid argument for method: ' . 
                    __FUNCTION__ . ' ] on line ' . __LINE__
                    );
            endif;
            
            $this->_data['street_address2'] = empty($value) ? '' : $value;
            $this->_modified[] = 'street_address2';

            return $this;
    }







    /*
     *
     * [ getEntrySuburb ] 
     *_____________________________________________________
     *
     *
     */

    public function getEntrySuburb()
    {
            return $this->getEntryStreetAddress2();
    }



    /*
     *
     * [ setEntrySuburb ] 
     *_____________________________________________________
     *
     *
     */

    public function setEntrySuburb($value)
    {
            return $this->setEntryStreetAddress2($value);
    }



    /*
     *
     * [ getEntryPostcode ] 
     *_____________________________________________________
     *
     *
     */

    public function getEntryPostcode()
    {
            return $this->_data['postcode'];
    }



    /*
     *
     * [ setEntryPostcode ] 
     *_____________________________________________________
     *
     *
     */

    public function setEntryPostcode($value)
    {
            if (!empty($value)):
            $value = \Cataleya\Helper\Validator::postalcode($value);
            if ($value===FALSE) 
                    $this->e->triggerException('Error in class (' . 
                    __CLASS__ . '): [ invalid argument for method: ' . 
                    __FUNCTION__ . ' ] on line ' . __LINE__
                    );
            endif;
            
            $this->_data['postcode'] = empty($value) ? '' : $value;
            $this->_modified[] = 'postcode';

            return $this;
    }



    /*
     *
     * [ getEntryCity ] 
     *_____________________________________________________
     *
     *
     */

    public function getEntryCity()
    {
            return $this->_data['city'];
    }



    /*
     *
     * [ setEntryCity ] 
     *_____________________________________________________
     *
     *
     */

    public function setEntryCity($value)
    {
            if (!empty($value)):
            $value = \Cataleya\Helper\Validator::name($value, 1, 100);
            if ($value===FALSE) 
                    $this->e->triggerException('Error in class (' . 
                    __CLASS__ . '): [ invalid argument for method: ' . 
                    __FUNCTION__ . ' ] on line ' . __LINE__
                    );
            endif;
            
            $this->_data['city'] = empty($value) ? '' : $value;
            $this->_modified[] = 'city';

            return $this;
    }



    /*
     *
     * [ getEntryState ] 
     *_____________________________________________________
     *
     *
     */

    public function getEntryState()
    {
            return $this->getEntryProvince();
    }


    /*
     *
     * [ setEntryState ] 
     *_____________________________________________________
     *
     *
     */

    public function setEntryState($value)
    {
            return $this->setEntryProvince($value);
    }



    /*
     *
     * [ getEntryProvince ] 
     *_____________________________________________________
     *
     *
     */

    public function getEntryProvince()
    {
            $_Province = $this->getProvince();
            return (!empty($_Province)) ? $_Province->getPrintableName() : $this->_data['province'];
    }



    /*
     *
     * [ setEntryState ] 
     *_____________________________________________________
     *
     *
     */

    public function setEntryProvince($value)
    {
            if (!empty($value)):
            $value = \Cataleya\Helper\Validator::name($value, 1, 100);
            if ($value===FALSE) 
                    $this->e->triggerException('Error in class (' . 
                    __CLASS__ . '): [ invalid argument for method: ' . 
                    __FUNCTION__ . ' ] on line ' . __LINE__
                    );
            endif;
            
            $this->_data['province'] = empty($value) ? '' : $value;
            $this->_modified[] = 'province';

            return $this;
    }



    /*
     *
     * [ getProvinceId ] 
     *_____________________________________________________
     *
     *
     */

    public function getProvinceId()
    {
            return $this->_data['province_id'];
    }


    /*
     *
     * [ getProvince ] 
     *_____________________________________________________
     *
     *
     */

    public function getProvince()
    {       
            if (!empty($this->_data['province_id']) && (int)$this->_data['province_id'] !== 0)
            {
                return \Cataleya\Geo\Province::load ($this->_data['province_id']);
            }

            else return NULL;
    }






    /*
     *
     * [ setProvince ] 
     *_____________________________________________________
     *
     *
     */

    public function setProvince(\Cataleya\Geo\Province $_Province)
    {
            $_Country = $this->getCountry();
            if ($_Country instanceof \Cataleya\Geo\Country && (!$_Country->hasProvinces() || !$_Country->hasProvinces($_Province))) $this->e->triggerException('Error in class (' . __CLASS__ . '): [ invalid argument for method: ' . __FUNCTION__ . ' ] on line ' . __LINE__);


            $this->_data['province_id'] = $_Province->getProvinceId();
            $this->_modified[] = 'province_id';

            return $this;
    }



    /*
     *
     * [ getEntryCountryCode ] 
     *_____________________________________________________
     *
     *
     */

    public function getEntryCountryCode()
    {
            return $this->_data['country_code'];
    }



    /*
     *
     * [ getEntryCountry ] 
     *_____________________________________________________
     *
     *
     */

    public function getEntryCountry()
    {
            return $this->getCountry()->getPrintableName();
    }




    /*
     *
     * [ getCountry ] 
     *_____________________________________________________
     *
     *
     */

    public function getCountry()
    {
            if ($this->_country_obj === NULL) $this->_country_obj = \Cataleya\Geo\Country::load($this->_data['country_code']);

            return $this->_country_obj;
    }






    /*
     *
     * [ setCountry ] 
     *_____________________________________________________
     *
     *
     */

    public function setCountry(\Cataleya\Geo\Country $_Country)
    {
            $_Province = $this->getProvince();
            if ($_Province instanceof \Cataleya\Geo\Province && $_Province->getCountryCode() !== $_Country->getCountryCode()) $this->e->triggerException('Error in class (' . __CLASS__ . '): [ invalid argument for method: ' . __FUNCTION__ . ' ] on line ' . __LINE__);

            $this->_country_obj = $_Country;
            $this->_data['country_code'] = $_Country->getCountryCode();
            $this->_modified[] = 'country_code';

            return $this;
    }





    /*
     *
     * [ setEntryCountryCode ] 
     *_____________________________________________________
     *
     *
     */

    public function setEntryCountryCode($value)
    {
            $this->_country_obj = NULL;
            $this->_data['country_code'] = $value;
            $this->_modified[] = 'country_code';

            return $this;
    }



    /*
     *
     * [ getEntryLatitude ] 
     *_____________________________________________________
     *
     *
     */

    public function getEntryLatitude()
    {
            return $this->_data['latitude'];
    }



    /*
     *
     * [ setEntryLatitude ] 
     *_____________________________________________________
     *
     *
     */

    public function setEntryLatitude($value = NULL)
    {
            if (!empty($value)):
            $value = filter_var($value, FILTER_VALIDATE_FLOAT, FILTER_NULL_ON_FAILURE);

            if ($value===NULL) $this->e->triggerException('Error in class (' . __CLASS__ . '): [ invalid argument for method: ' . __FUNCTION__ . ' ] on line ' . __LINE__);
            endif;
            
            $this->_data['latitude'] = empty($value) ? '0.00000' : $value;
            $this->_modified[] = 'latitude';

            return $this;
    }



    /*
     *
     * [ getEntryLongtitude ] 
     *_____________________________________________________
     *
     *
     */

    public function getEntryLongtitude()
    {
            return $this->_data['longtitude'];
    }



    /*
     *
     * [ setLongtitude ] 
     *_____________________________________________________
     *
     *
     */

    public function setEntryLongtitude($value = NULL)
    {
            if (!empty($value)):
            $value = filter_var($value, FILTER_VALIDATE_FLOAT, FILTER_NULL_ON_FAILURE);

            if ($value===NULL) $this->e->triggerException('Error in class (' . __CLASS__ . '): [ invalid argument for method: ' . __FUNCTION__ . ' ] on line ' . __LINE__);
            endif;
            
            $this->_data['longtitude'] = empty($value) ? '0.00000' : $value;
            $this->_modified[] = 'longtitude';

            return $this;
    }




    
    

        /**
         * 
         * @param \Cataleya\Asset\Contact $_Contact
         * @return \Cataleya\Asset\Contact
         * 
         */
        public function copy (\Cataleya\Asset\Contact $_Contact)
        {
                $this
                        ->setEntryLabel($_Contact->getEntryLabel())
                        ->setEntryCompanyName($_Contact->getEntryCompanyName())
                        ->setEntryFirstname($_Contact->getEntryFirstname())
                        ->setEntryLastname($_Contact->getEntryLastname())
                        ->setEntryGender($_Contact->getEntryGender())
                        
                        ->setEntryEmail($_Contact->getEntryEmail())
                        ->setEntryWorkPhone($_Contact->getEntryWorkPhone())
                        ->setEntryHomePhone($_Contact->getEntryHomePhone())
                        ->setEntryFax($_Contact->getEntryFax())
                        ->setEntryWebsite($_Contact->getEntryWebsite())
                        
                        ->setEntryStreetAddress($_Contact->getEntryStreetAddress())
                        ->setEntryStreetAddress2($_Contact->getEntryStreetAddress2())
                        ->setEntryCity($_Contact->getEntryCity())
                        ->setEntryProvince($_Contact->getEntryProvince())
                        ->setEntryPostcode($_Contact->getEntryPostcode())
                        ->setEntryCountryCode($_Contact->getEntryCountryCode())
                        
                        ->setEntryLongtitude($_Contact->getEntryLongtitude())
                        ->setEntryLatitude($_Contact->getEntryLatitude());
                
                $_Province = $_Contact->getProvince();
                if ($_Province instanceof \Cataleya\Geo\Province) $this->setProvince ($_Province);
                
                return $this;
        } 
    




    /*
     *
     * [ getLocation ] 
     *_____________________________________________________
     *
     *

    public function getLocation()
    {
            if (empty($this->_location)) $this->_location = \Cataleya\Geo\Location::load ($this->_data['location_id']);
            return $this->_location;
    }

     */


    /*
     *
     * [ getLocationId ] 
     *_____________________________________________________
     *
     *

    public function getLocationId()
    {
            return $this->_data['location_id'];
    }


     */






        





}
	
	
	
