<?php


namespace Cataleya\Asset;




/*
 *
 *	@Package: Cataleya
 *	@Class: \Cataleya\Asset\Description
 *
 *	(c) 2012 Fancy Paper Planes
 *
 *
 *
 */


class Description 
extends \Cataleya\Asset\Content 
implements \Cataleya\Asset

{
	
	protected 
                
         $_data, 
	 $_content_languages = array(), 
	 $_modified = array(),  
	 $_content_modified = array(), 
         $_johny_jus_come = false, 
                
         $_content_type, 
         $_url_rewrite;


	protected 
			$dbh, 
			$e;
			
			
			






	/**
         * 
         * @param integer $instance_id
         * @return \Cataleya\Asset\Content
         * 
         */
	public static function load ($instance_id = 0) 
	{
		
		// construct
                $_objct = __CLASS__;
                
                $instance = new $_objct ();
		
                $row = $instance->loadDataWithID($instance_id);	
                
                
		if (empty($row))
		{ 
                    
                    
                    return NULL;
		} else {
                    $instance->_data = $row;
                }
                
                
                

                $instance->_loadEntries();
                
                return $instance;
		
	}
        
        
        
        

        
        protected function loadDataWithID ($instance_id) {

		///// CHECK IF DESCRIPTION ID IS VALID /////////////////////////
		static 
					$select2_handle, 
					$select2_handle_param_instance_id;

			
		
		if (empty($select2_handle))
		{
			$select2_handle = $this->dbh->prepare('SELECT * FROM content WHERE content_id = :instance_id LIMIT 1');
			$select2_handle->bindParam(':instance_id', $select2_handle_param_instance_id, \PDO::PARAM_INT);
		}
                
                
		
		$select2_handle_param_instance_id = $instance_id;
		
		if (!$select2_handle->execute()) $this->e->triggerException('
										Error in class (' . __CLASS__ . '): [ ' . 
										implode(', ', $select2_handle->errorInfo()) . 
										' ] on line ' . __LINE__);
										
		$row = 	$select2_handle->fetch(\PDO::FETCH_ASSOC);
                
                
                if (empty($row)) \Cataleya\Helper\ErrorHandler::getInstance()->triggerException('data could not be read...' . $instance_id . ':' . __LINE__);
                
                return $row;
                
        }





        /**
         * 
         * @param string $title
         * @param string $text
         * @param string $language_code
         * @return \Cataleya\Asset\Content
         * 
         */
	public static function create ($title = 'Untitled', $text = 'No description', $language_code = 'EN')  // defaults to english (EN)
	{


                
                
                $_params = array ( 
                'content_type' => self::TYPE_DESCRIPTION
                        );
                
                $_cols_str = $_vals_str = [];

                foreach ($_params as $key=>$val) {

                     If (!is_numeric($val) && !is_string($val) && !is_null($val)) { $_params[$key] = $val= ''; }
                     
                     $_cols_str[] = $key;
                     $_vals_str[] = ':'.$key;
                }
                

                // Get database handle...
                $dbh = \Cataleya\Helper\DBH::getInstance();

                // Get error handler
                $e = \Cataleya\Helper\ErrorHandler::getInstance();
                
                
		
		static $insert_handle;
		
		if (empty($insert_handle))
		{
			$insert_handle = $dbh->prepare('INSERT INTO content (date_added, last_modified, ' . implode(', ', $_cols_str) . ') VALUES (NOW(), NOW(), ' . implode(', ', $_vals_str) . ')');
		
   
                        foreach ($_params as $key=>$val) {


                                $insert_handle->bindParam(
                                        ':'.$key, 
                                        $_params[$key], 
                                        \Cataleya\Helper\DBH::getTypeConst($val)
                                        );

                        }
                        
                }

		if (!$insert_handle->execute()) $instance->e->triggerException('
										Error in class (' . __CLASS__ . '): [ ' . 
										implode(', ', $insert_handle->errorInfo()) . 
										' ] on line ' . __LINE__);
										
		$_instance_id = $dbh->lastInsertId();
		$instance = self::load($_instance_id);
		
		if ($title !== NULL && $text !== NULL)
		{
		if(!is_string($language_code) || strlen($language_code) != 2) $language_code = 'EN';
		$language_code = strtoupper($language_code);
		
		$instance->saveEntry($title, $text, $language_code);
		}
		
                $instance->_johny_jus_come = TRUE;
                
		return $instance;
		
	}
        
        
        
        
        
        
        
        
        
        
        
        
        






	/*
	 *
	 *  [ SETTERS ]
	 * ________________________________________________________________
	 * 
	 * 
	 *
	 *
	 *
	 */
	 






        
        
        






}

