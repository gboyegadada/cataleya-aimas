<?php


namespace Cataleya\Asset;
use Cataleya\Error;



/*
 *
 *	@Package: Cataleya
 *	@Class: \Cataleya\Asset\Content
 *
 *	(c) 2012 Fancy Paper Planes
 *
 *
 *
 */


class Content

{
	
	protected 
                
         $_data, 
	 $_content_languages = array(), 
	 $_modified = array(),  
	 $_content_modified = array(), 
         $_johny_jus_come = false, 
                
         $_content_type, 
         $_url_rewrite;


	protected 
			$dbh, 
			$e;
			
			
        
        const TYPE_PAGE = 2;
        const TYPE_POST = 3;
        const TYPE_AUTOSAVE = 4;
        const TYPE_COMMENT = 5;
        const TYPE_COMMENT_REPLY = 6;
        const TYPE_DESCRIPTION = 7;
        
        const TYPE_REVISION = 8;
			



	/*
	 *
	 * [ __construct ]
	 * ________________________________________________________________
	 * 
	 * 
	 *
	 *
	 *
	 */
	 
	protected function __construct ($instance_id = 0) 
	{
		
		// Get database handle...
                $this->dbh = \Cataleya\Helper\DBH::getInstance();
		
		// Get error handler
		$this->e = \Cataleya\Helper\ErrorHandler::getInstance();
                
                
		
		
	}



	/*
	 *
	 * [ __destruct ]
	 * ________________________________________________________________
	 * 
	 * 
	 *
	 *
	 *
	 */
	 
	public function __destruct () 
	{
		
		
		$this->save();
		
		
	}



        
        

        
        
        
        
        
        
        /**
         * Load content entries from [content_languages] table.
         *
         * @return \Cataleya\Asset\Content
         */
        
        protected function _loadEntries () {


		
		static 
					$select_handle, 
					$select_handle_param_content_id;
		
		if (empty($select_handle))
		{
			$select_handle = $this->dbh->prepare('SELECT * FROM content_languages WHERE content_id = :content_id');
			$select_handle->bindParam(':content_id', $select_handle_param_content_id, \PDO::PARAM_INT);
		}
		
		$select_handle_param_content_id = $this->getID();
		
		if (!$select_handle->execute()) $this->e->triggerException('
										Error in class (' . __CLASS__ . '): [ ' . 
										implode(', ', $select_handle->errorInfo()) . 
										' ] on line ' . __LINE__);
										
										
		while ($row = $select_handle->fetch(\PDO::FETCH_ASSOC)) 
		{
			$this->_content_languages[$row['language_code']] = $row;
			
		}
		
		
		return $this;
        }

        










        public function save () 
	{
		

		if (empty($this->_modified)) return;
                
                $this->_modified = array_unique($this->_modified);
                
		$_update_params = array();
		$key_val_pairs = array();
		
		foreach ($this->_modified as $key) $key_val_pairs[$key] = $key . ' = :' . $key;
		
	
		// Do prepared statement for 'INSERT'...
		$update_handle = $this->dbh->prepare('
												UPDATE content 
												SET ' . implode (', ', $key_val_pairs) . ', last_modified=now()       
												WHERE content_id = :content_id 
												');
		$_update_params['content_id'] = $this->_data['content_id'];                
		$update_handle->bindParam(':content_id', $_update_params['content_id'], \PDO::PARAM_INT);
                
		foreach ($this->_modified as $key) {
                    
                        $_update_params[$key] = $this->_data[$key];                        
			$update_handle->bindParam(':'.$key, $_update_params[$key], \Cataleya\Helper\DBH::getTypeConst($_update_params[$key]));

		}
		
		if (!$update_handle->execute()) $this->e->triggerException('Error in class (' . __CLASS__ . '): [ ' . implode(', ', $update_handle->errorInfo()) . ' ] on line ' . __LINE__);
                
                
                $this->_saveContentData();
		
	}
        
        
        



	/*
	 *
	 *  [ save ]
	 * ________________________________________________________________
	 * 
	 * 
	 *
	 *
	 *
	 */
	 
	protected function _saveContentData () 
	{
		
            if (empty($this->_content_modified) && !$this->_johny_jus_come) return $this;
            
            $this->_content_modified = array_unique($this->_content_modified);
                
            foreach ($this->_content_languages as $key=>$content)
            {
		
                if (!in_array( $key, $this->_content_modified) && !$this->_johny_jus_come) continue;
                
		static 
                        $update_handle, 
                        $update_handle_param_instance_id, 
                        $update_handle_param_language_code, 
                        $update_handle_param_title, 
                        $update_handle_param_text, 
                        $update_handle_param_description;
		
		if (empty($update_handle))
		{
			$update_handle = $this->dbh->prepare('
													UPDATE content_languages  
													SET title = :title, 
													text = :text, 
                                                                                                        description = :description, 
													language_code = :language_code, 
                                                                                                        last_modified = NOW(), 
													WHERE content_id = :content_id 
													');
			$update_handle->bindParam(':content_id', $update_handle_param_instance_id, \PDO::PARAM_INT);
			$update_handle->bindParam(':language_code', $update_handle_param_language_code, \PDO::PARAM_STR);
			$update_handle->bindParam(':title', $update_handle_param_title, \PDO::PARAM_STR);
			$update_handle->bindParam(':text', $update_handle_param_text, \PDO::PARAM_STR);
			$update_handle->bindParam(':description', $update_handle_param_description, \PDO::PARAM_STR);
		}
		
		$update_handle_param_instance_id = $this->getID();
		$update_handle_param_language_code = $content['language_code'];
		$update_handle_param_title = $content['title'];
		$update_handle_param_text = $content['text'];
		$update_handle_param_description = (!is_string($content['description'])) ? '' : $content['description'];

		if (!$update_handle->execute()) $this->e->triggerException('
										Error in class (' . __CLASS__ . '): [ ' . 
										implode(', ', $update_handle->errorInfo()) . 
										' ] on line ' . __LINE__);
										
		
            }
		
		return $this;
		
	}




	/*
	 *
	 *  [ Attribute ] interface method: [ getID ]
	 * ________________________________________________________________
	 * 
	 * 
	 *
	 *
	 *
	 */
	 
	public function getID () 
	{
		
		
		return $this->_data['content_id'];
			
		
		
	}
        
        
        


	/**
	 *
	 * Get content type...
         * 
         * @return int Returns Content Type (integer value)
	 *
	 *
	 */
	public function getContentType () 
	{
		
		
		return $this->_data['content_type'];
			
		
		
	}
        
        
        
        

	/**
	 *
	 * Get content type...
         * 
         * @return int Returns Content Type (integer value)
	 *
	 *
	 */
	public function isLike (\Cataleya\Asset\Content $_Content) 
	{
		
		
		return ($_Content->getContentType() === $this->_data['content_type']);
			
		
		
	}
        




	/*
	 *
	 *  [ Attribute ] interface method: [ getClassName ]
	 * ________________________________________________________________
	 * 
	 * 
	 *
	 *
	 *
	 */
	 
	public function getClassName () 
	{
		
		
		return __CLASS__;
			
		
		
	}



	/*
	 *
	 *  [ GETTERS ]
	 * ________________________________________________________________
	 * 
	 * 
	 *
	 *
	 *
	 */
	 




	/*
	 *
	 *  [ getTitle ]
	 * ________________________________________________________________
	 * 
	 * 
	 *
	 *
	 *
	 */
	 
	public function getTitle ($language_id = NULL, $_length=-1) 
	{
		
		if ($language_id === NULL) $this->e->triggerException('Error in class (' . __CLASS__ . '): [ Invalid language ID ] on line ' . __LINE__);
		
		if (isset ($this->_content_languages[$language_id])) { 
                    
                    $text = $this->_content_languages[$language_id]['title'];
                    return ($_length > 1 && strlen($text) > $_length) ? (substr($text, 0, $_length-2).'..') : $text;
                } else {
                    return '';
                }	
		
		
	}




	/*
	 *
	 *  [ getText ]
	 * ________________________________________________________________
	 * 
	 * 
	 *
	 *
	 *
	 */
	 
	public function getText ($language_id = NULL, $_length=-1) 
	{
		
		
		if ($language_id === NULL) $this->e->triggerException('Error in class (' . __CLASS__ . '): [ Invalid language ID ] on line ' . __LINE__);
		
		if (isset ($this->_content_languages[$language_id])) {
                    
                    $text = $this->_content_languages[$language_id]['text'];
                    return ($_length > 1 && strlen($text) > $_length) ? (substr($text, 0, $_length-2).'..') : $text;
                    
                } else {
                    return '';
                }
			
			
		
		
	}




	/*
	 *
	 *  [ getContent ]
	 * ________________________________________________________________
	 * 
	 * 
	 *
	 *
	 *
	 */
	 
	public function getContent ($language_id = NULL) 
	{
		
		
		if ($language_id === NULL) return array ();
		
		if (isset ($this->_content_languages[$language_id])) return $this->_content_languages[$language_id];
		else return array ();
			
			
		
		
	}





	/*
	 *
	 *  [ getDateAdded ]
	 * ________________________________________________________________
	 * 
	 * 
	 *
	 *
	 *
	 */
	 
	public function getDateAdded () 
	{
		
		
		return $this->_date_added;
			
		
		
	}








	/*
	 *
	 *  [ SETTERS ]
	 * ________________________________________________________________
	 * 
	 * 
	 *
	 *
	 *
	 */
	 



	/*
	 *
	 *  [ saveEntry ]
	 * ________________________________________________________________
	 * 
	 * 
	 *
	 *
	 *
	 */
	 
	public function saveEntry ($title = 'Untitled', $text = '', $language_code = 'EN') 
	{

        $description = ""; 
        
        // Ok - [ $description ] is a TODO .. not even sure I remember where "description" 
        // is supposed to come in exactly but I think what I had in mind is entities like "page"s 
        // where a page will have a title, body (text) and description.
		
		$title = \Cataleya\Helper\Validator::text($title, 0, 500);
        $title = (is_string($title) && trim($title) === '') ? '...' : $title;
		$text = \Cataleya\Helper\Validator::html($text, 0, 40000);
        $insert_handle_param_description = \Cataleya\Helper\Validator::text($description, 0, 1000);
        $language_code = \Cataleya\Helper\Validator::iso($language_code, 2, 2);
                
		
		if($language_code === FALSE || $title === FALSE)  $this->e->triggerException('Error in class (' . __CLASS__ . '): [ invalid argument for method: ' . __FUNCTION__ . ' ] on line ' . __LINE__);

		
		
		// construct
		
		static 
				$insert_handle, 
                $insert_handle_param_content_id, 
				$insert_handle_param_language_code, 
				$insert_handle_param_title, 
				$insert_handle_param_text, 
				$insert_handle_param_description;
		
		if (empty($insert_handle))
		{
			$insert_handle = $this->dbh->prepare('
                                                                INSERT INTO content_languages 
                                                                (content_id, title, text, description, language_code, last_modified, date_added) 
                                                                 VALUES (:content_id, :title, :text, :description, :language_code, NOW(), NOW()) 
                                                                 ON DUPLICATE KEY UPDATE title = :title, text = :text, description = :description, last_modified = NOW()
                                                                ');
			$insert_handle->bindParam(':content_id', $insert_handle_param_content_id, \PDO::PARAM_INT);
			$insert_handle->bindParam(':language_code', $insert_handle_param_language_code, \PDO::PARAM_STR);
			$insert_handle->bindParam(':title', $insert_handle_param_title, \PDO::PARAM_STR);
			$insert_handle->bindParam(':text', $insert_handle_param_text, \PDO::PARAM_STR);
			$insert_handle->bindParam(':description', $insert_handle_param_description, \PDO::PARAM_STR);
		}
		
		$insert_handle_param_content_id = $this->getID();
		$insert_handle_param_language_code = $language_code;
		$insert_handle_param_title = $title;
		$insert_handle_param_text = ($text === FALSE) ? '' : $text;
		$insert_handle_param_description = (empty($insert_handle_param_description)) ? '' : $insert_handle_param_description;

		if (!$insert_handle->execute()) $this->e->triggerException('
										Error in class (' . __CLASS__ . '): [ ' . 
										implode(', ', $insert_handle->errorInfo()) . 
										' ] on line ' . __LINE__);
										
		
		$this->_content_languages[$language_code] = array (
                                                                'title'	=>	$title, 
                                                                'text'	=>	$text, 
                                                                'language_code'	=>	$language_code, 
                                                                'content_id'	=>	$this->getID(), 
                                                                'last_modified'	=>	''
                                                                );
		
		return $this;
		
		
	}





	/*
	 *
	 *  [ setTitle ]
	 * ________________________________________________________________
	 * 
	 * 
	 *
	 *
	 *
	 */
	 
	public function setTitle ($title = NULL, $language_id = NULL) 
	{

		$title = \Cataleya\Helper\Validator::text($title, 0, 500);
                $title = (is_string($title) && trim($title) === '') ? '...' : $title;
                $language_id = \Cataleya\Helper\Validator::iso($language_id, 2, 2);
                
                
		if($language_id === FALSE || $title === FALSE)  throw new Error ('Invalid argument');


		if (isset ($this->_content_languages[$language_id]))
		{
			$this->saveEntry($title, $this->_content_languages[$language_id]['text'], $language_id);
                        if (!in_array($language_id, $this->_content_modified)) $this->_content_modified[] = $language_id;
			
		} else {
			$this->saveEntry($title, '', $language_id);
			
		}
		
		return $this;
		
		
	}




	/*
	 *
	 *  [ setText ]
	 * ________________________________________________________________
	 * 
	 * 
	 *
	 *
	 *
	 */
	 
	public function setText ($text = NULL, $language_id = NULL) 
	{
		
		$text = \Cataleya\Helper\Validator::html($text, 0, 40000);
        $language_id = \Cataleya\Helper\Validator::iso($language_id, 2, 2);
                
		if($language_id === FALSE || $text === FALSE)  throw new Error ('Invalid argument');


		if (isset ($this->_content_languages[$language_id]))
		{
			$this->saveEntry($this->_content_languages[$language_id]['title'], $text, $language_id);
                        if (!in_array($language_id, $this->_content_modified)) $this->_content_modified[] = $language_id;
			
		} else {
			$this->saveEntry('...', $text, $language_id);
			
		}
		
		return $this;
		
		
	}






	/*
	 *
	 *  [ deleteEntry ]
	 * ________________________________________________________________
	 * 
	 * 
	 *
	 *
	 *
	 */
	 
	public function deleteEntry ($language_id = NULL) 
	{
		
		if ($title === NULL || $language_id === NULL) return FALSE;

		if (isset ($this->_content_languages[$language_id]))
		{
			
			$language_code = strtoupper($language_code);
			
			// construct
			
			static 
					$delete_handle, 
					$delete_handle_param_language_code;
			
			if (empty($delete_handle))
			{
				$delete_handle = $this->dbh->prepare('
                                                                    DELETE FROM content_languages 
                                                                    WHERE content_id = :content_id
                                                                    AND language_code = :language_code 
                                                                    ');
				$delete_handle->bindParam(':content_id', $delete_handle_param_content_id, \PDO::PARAM_INT);
				$delete_handle->bindParam(':language_code', $delete_handle_param_language_code, \PDO::PARAM_STR);
			}
			
			$delete_handle_param_content_id = $this->getID();
			$delete_handle_param_language_code = $language_code;
	
			if (!$delete_handle->execute()) $this->e->triggerException('
											Error in class (' . __CLASS__ . '): [ ' . 
											implode(', ', $delete_handle->errorInfo()) . 
											' ] on line ' . __LINE__);
											
			
			unset ($this->_content_languages[$language_code]);
				
		} 
		
		return $this;
		
		
	}






	/*
	 *
	 *  [ delete ]
	 * ________________________________________________________________
	 * 
	 * 
	 *
	 *
	 *
	 */
	 
	public function delete () 
	{
			// CLEAN UP...
                        // \Cataleya\Helper\DBH::sanitize(array('content_languages'), 'content_id', $this->getID());
                        

                        
                        
				
			// DELETE DESCRIPTION ....
			$descr_delete = $this->dbh->prepare('
                                                            DELETE FROM content 
                                                            WHERE content_id = :content_id
                                                            ');
			$descr_delete->bindParam(':content_id', $descr_delete_param_content_id, \PDO::PARAM_INT);
			
			$descr_delete_param_content_id = $this->getID();
	
			if (!$descr_delete->execute()) $this->e->triggerException('
											Error in class (' . __CLASS__ . '): [ ' . 
											implode(', ', $descr_delete->errorInfo()) . 
											' ] on line ' . __LINE__);
											
			

		// $this = NULL;
		return TRUE;
		
		
	}
        
        
        
        
        
        
        
        
        
        
        

        
        
        
	/*
	 *
	 *  [ getParent ]
	 * ________________________________________________________________
	 * 
	 * 
	 *
	 *
	 */
	 
	public function getParent ($_AUTO_LOAD = TRUE) 
	{
               if (!is_numeric($this->_data['parent_id'])) return NULL;
               
               return ($_AUTO_LOAD == TRUE) ? self::load($this->_data['parent_id']) : (int)$this->_data['parent_id'];
			
			
        }
        
        
        
        
        
        
        
        
        
        
        
	/*
	 *
	 *  [ hasParent ]
	 * ________________________________________________________________
	 * 
	 * 
	 *
	 *
	 */
	 
	public function hasParent () 
	{

               
               return (is_numeric($this->_data['parent_id']));
			
			
        }
        
        
        
        
        
        
       
        
	/*
	 *
	 *  [ hasChild ]
	 * ________________________________________________________________
	 * 
	 * 
	 *
	 *
	 */
	 
	public function hasChild (self $_Content) 
	{

                $this->_loadChildren (FALSE);
		return (in_array ((int)$_Content->getID(), $this->_children));
			
			
        }
        
        
        
        
        
	/*
	 *
	 *  [ getChildren ]
	 * ________________________________________________________________
	 * 
	 * 
	 *
	 *
	 */
	 
	public function getChildren ($_FORCE_RELOAD = FALSE) 
	{
                $_collection = array ();
                $this->_loadChildren ($_FORCE_RELOAD);
                
                foreach ($this->_children as $_id) $_collection[] = self::load ($_id);
                
		return $_collection;
			
        }
        
        
        
        
        

        
        /*
	 *
	 *  [ _loadChildren ]
	 * ________________________________________________________________
	 * 
	 * 
	 *
	 *
	 */
	 
	private function _loadChildren ($_FORCE_RELOAD = FALSE) 
	{

               if ($_FORCE_RELOAD === FALSE && !empty($this->_children)) return $this->_children;
                // LOAD
               static 
                       $select_handle;


               if (!isset($select_handle)) {
                       // PREPARE SELECT STATEMENT...
                       $select_handle = $this->dbh->prepare('SELECT content_id FROM content WHERE parent_id = :content_id');

               }

               $param_content_id = $this->getID();
               $select_handle->bindParam(':content_id', $param_content_id, \PDO::PARAM_INT);



               if (!$select_handle->execute()) $this->e->triggerException('
                                                                            Error in class (' . __CLASS__ . '): [ ' . 
                                                                            implode(', ', $select_handle->errorInfo()) . 
                                                                            ' ] on line ' . __LINE__);

               $this->_children = array ();
               while ($row = $select_handle->fetch(\PDO::FETCH_ASSOC))
               {
                   $this->_children[] = (int)$row['content_id'];
               }   
                
                return $this->_children;
	}
        
        
        
        
        

        /*
         *
         * [ isActive ] 
         *_____________________________________________________
         *
         *
         */

        public function isActive()
        {
                return ((int)$this->_data['status'] === 1) ? TRUE : FALSE;
        }





        /*
         * 
         * [ enable ]
         * ______________________________________________________
         * 
         * 
         */


        public function enable () 
        {

            if ((int)$this->_data['status'] === 0) 
            {
                $this->_data['status'] = 1;
                $this->_modified[] = 'sratus';
            }

            return $this;
        }





        /*
         * 
         * [ disable ]
         * ______________________________________________________
         * 
         * 
         */


        public function disable () 
        {
            if ((int)$this->_data['status'] === 1) 
            {
                $this->_data['status'] = 0;
                $this->_modified[] = 'status';
            }

            return $this;
        }





        






}

