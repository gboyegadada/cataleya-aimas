<?php


namespace Cataleya\Asset;




/*
 *
 *	@Package: Cataleya
 *	@Class: \Cataleya\Asset\Banner
 *
 *	(c) 2012 Fancy Paper Planes
 *
 *
 *
 */


class Banner 
//implements \Cataleya\Asset

{
	
	private 
				$_image_id, 
				$_file_name, 
				$_image_type, 
				$_date_added, 
				$_hatched;
				


/*
 * These constants will be public so you can do something like: 
 * $color->image->getHref(\Cataleya\Asset\Banner::THUMB)
 *
 */
	const TINY = 1;
	const THUMB = 2;
	const LARGE = 3;
	const ZOOM = 4;
	
	
/*
 * Directory names
 *
 */
 
 	private $_DIR = array (
							self::TINY => 'tiny',
							self::THUMB => 'thumb',
							self::LARGE => 'large',
							self::ZOOM => 'zoom'
							);
				


				
/*
 * Helpers 
 *
 */
				
	private $dbh, $e, $core, $image, $base_url;
				
				



	/*
	 *
	 * [ __construct ]
	 * ________________________________________________________________
	 * 
	 * 
	 *
	 *
	 *
	 */
	 
	private function __construct () 
	{
		
		// Get database handle...
        $this->dbh = \Cataleya\Helper\DBH::getInstance();
		
		// Get error handler
		$this->e = \Cataleya\Helper\ErrorHandler::getInstance();
		
		// Get Core
		$this->core = \Cataleya\Core::getInstance();
		
		// Set base url
		$this->base_url = $this->core->config->serverRoot . $this->core->config->storeRoot;
		$this->base_url .= 'ui/images/catalog/';
		
		// initialize
		$this->_image_id = 0;
		$this->_file_name = '';
		$this->_image_type = 2; // defaults jpeg i.e. value of IMAGETYPE_JPEG constant.
		$this->_hatched = 0;
		$this->_date_added = NULL;
		
		
	}



	/*
	 *
	 * [ __destruct ]
	 * ________________________________________________________________
	 * 
	 * 
	 *
	 *
	 *
	 */
	 
	public function __destruct () 
	{
		
		
		
		
		
	}




	/*
	 *
	 *  [ Attribute ] interface method: [ load ]
	 * ________________________________________________________________
	 * 
	 * 
	 *
	 *
	 *
	 */
	 
	static public function load ($image_id = 0) 
	{
		
		if ((int)$image_id === 0) return NULL;
		$image = new \Cataleya\Asset\Banner();
		
		static $image_select, $img_select_param_id;
		
		if (empty($image_select))
		{
			$image_select = $image->dbh->prepare('SELECT * FROM product_images WHERE image_id = :image_id LIMIT 1');
			$image_select->bindParam(':image_id', $img_select_param_image_id, \PDO::PARAM_INT);
		}
		

		$img_select_param_image_id = $image_id;
		
		if (!$image_select->execute()) $image->e->triggerException('
										Error in class (' . __CLASS__ . '): [ ' . 
										implode(', ', $image_select->errorInfo()) . 
										' ] on line ' . __LINE__);
		
		if ($img_row = $image_select->fetch(\PDO::FETCH_ASSOC)) 
		{
			$image->_image_id = $img_row['image_id'];
			$image->_file_name = $img_row['file_name'];
			$image->_image_type = $img_row['image_type'];
			$image->_hatched = $img_row['hatched'];
			$image->_date_added = $img_row['date_added'];
		} else {
			
			unset($image);
			return NULL;
		}
		
		
		return $image;
			
		
		
	}





	/*
	 *
	 *  [ Attribute ] interface method: [ create ]
	 * ________________________________________________________________
	 * 
	 * 
	 *
	 *
	 *
	 */
	 
	static public function create ($src = '', $min_width = 600, $min_height = 0) 
	{
		
		if ($src == '' || !file_exists($src) || !is_uploaded_file($src)) return NULL;
		
		// if only width is set, assume bounding box is square...
		if ((int)$min_width > 0 && $min_height == 0) $min_height = $min_width;
		
			
		
		// Get image info...
		$img_info = getimagesize($src);
		
		// If file is not a valid image or is CMYK return NULL.
		if ($img_info === FALSE || $img_info['channels'] != 3 ) return NULL;
		
		// Check image type. We only play with JPG, JPEG200, PNG, and GIF.
		if ($img_info[2] != IMAGETYPE_JPEG && $img_info[2] != IMAGETYPE_PNG && $img_info[2] != IMAGETYPE_GIF && $img_info[2] != IMAGETYPE_JPEG2000) return NULL;

		// image mime
		$image->_image_type = $img_info[2];
		
		
		
		
		// if image is smaller than target width and hieght return NULL...
		if ($img_info[0] < $min_width || $img_info[1] < $min_height) return NULL;
		
		
		// construct
		$image = new \Cataleya\Asset\Banner();
		
		static $image_insert, $image_insert_param_id;
		
		if (empty($image_insert))
		{
			$image_insert = $image->dbh->prepare('
													INSERT INTO product_images (image_type, date_added) 
													VALUES (:image_type, NOW())
													');
			$image_insert->bindParam(':image_id', $image_insert_param_image_id, \PDO::PARAM_INT);
		}
		
		
		if (!$image_insert->execute()) $image->e->triggerException('
										Error in class (' . __CLASS__ . '): [ ' . 
										implode(', ', $image_insert->errorInfo()) . 
										' ] on line ' . __LINE__);
										
		
		$image->_image_id = $image->dbh->lastInsertId();
		
		
		// Write image file...
		$image->_file_name = 'img_' . $image->_image_id . image_type_to_extension($img_info[2]);
		$new_path = CAT_DIR . 'temp/' . $image->_file_name;
		move_uploaded_file($src, $new_path);
		
		
		return $image;
			
		
		
	}






	/*
	 *
	 *  [ Attribute ] interface method: [ getID ]
	 * ________________________________________________________________
	 * 
	 * 
	 *
	 *
	 *
	 */
	 
	static public function getID () 
	{
		
		
		return $this->_image_id;
			
		
		
	}






	/*
	 *
	 *  [ Attribute ] interface method: [ getClassName ]
	 * ________________________________________________________________
	 * 
	 * 
	 *
	 *
	 *
	 */
	 
	static public function getClassName () 
	{
		
		
		return __CLASS__;
			
		
		
	}





	/*
	 *
	 * [ bake ]
	 * ________________________________________________________________
	 * 
	 * ARGS: 
	 *			$params = array (
	 *								'rotate'	=> Rotate angle. Set to '0' to skip (or don't set at all).
	 *								'sw'	=> Selection Width (from user interface)
	 *								'sh'	=> Selection Height (from user interface)
	 *								'sx'	=> Selection Origin (top left corner of selection)
	 *								'sy'	=> Selection Origin (top left corner of selection)
	 *							);
	 *
	 *			$make_sizes: Save image to all sizes ('Tiny', 'Thumb', 'Large', and 'Zoom')
	 *							using default pixel dimensions.
	 *
	 */
	 
	public function bake ($params = array('rotate'=>0, 'sw'=>0, 'sh'=>0, 'sx'=>0, 'sy'=>0), $make_sizes = FALSE) 
	{
		
		if (!is_array($params) || !isset($params['sw'], $params['sh'], $params['sx'], $params['sy'])) return FALSE;
		
		
		$params['rotate'] = isset($params['rotate']) ? (int)$params['rotate'] : 0;
		if ($params['sw'] < 10 || $params['sh'] < 10) return FALSE;
		
		
		$image_path = $imagefilepath_full = CAT_DIR . 'temp/' . $image->_file_name;
		$save_to_path = CAT_DIR . 'full/' . $this->_file_name;
		
		if ($image->_hatched == 0 || !file_exists($image_path)) return FALSE;
		
		// load image file...
		$image = \Cataleya\Asset\ImageCanvas::load($image_path);
		
		// rotate if needed..
		if ($params['rotate'] != 0)	$image->rotate($params['rotate']);
		
		// crop using selection data...
		$image->crop($params['sw'], $params['sh'], $params['sx'], $params['sy']);
		
		// save
		$image->save($save_to_path, 100);
		
		
		if ($make_sizes)
		{
			
			$this->makeTiny();
			$this->makeThumb();
			$this->makeLarge();
			$this->makeZoom();
			
		}
		
		return TRUE;
		
		
		
	}








	/*
	 *
	 * We can make just the sizes we're going to be needing...
	 * with the following methods at any time we choose.
	 * 
	 * ________________________________________________________________
	 * 
	 * Note: 1. These are only available in [Admin_Asset_Banner] and not in [Store_Asset_Banner]
	 *       2. The [ _make ] method is private.
	 *
	 *
	 *
	 */








	/*
	 *
	 * [ makeTiny ]
	 * ________________________________________________________________
	 * 
	 * ARGS: 
	 *			Size of bounding box i.e. $square_dim x $square_dim
	 *
	 *
	 */
	 
	public function makeTiny ($square_dim = 40) 
	{
		
		$image_path = $imagefilepath_full = CAT_DIR . 'full/' . $image->_file_name;
		$save_to_path = CAT_DIR . 'tiny/' . $this->_file_name;
		
		if ($image->_hatched == 0 || !file_exists($image_path)) return FALSE;
		
		
		$image = \Cataleya\Asset\ImageCanvas::load($image_path);
		
		if ($image->getHeight() > $image->getWidth()) $image->resizeToHeight($square_dim);
		else $image->resizeToWidth($square_dim);
			
		$image->save($save_to_path, 80);
		
		return TRUE;
		
		
	}




	/*
	 *
	 * [ makeThumb ]
	 * ________________________________________________________________
	 * 
	 * ARGS: 
	 *			Size of bounding box i.e. $square_dim x $square_dim
	 *
	 *
	 */
	 
	public function makeThumb ($square_dim = 300) 
	{
		
		$image_path = $imagefilepath_full = CAT_DIR . 'full/' . $image->_file_name;
		$save_to_path = CAT_DIR . 'thumb/' . $this->_file_name;
		
		if ($image->_hatched == 0 || !file_exists($image_path)) return FALSE;
		
		
		$image = \Cataleya\Asset\ImageCanvas::load($image_path);
		
		if ($image->getHeight() > $image->getWidth()) $image->resizeToHeight($square_dim);
		else $image->resizeToWidth($square_dim);
			
		$image->save($save_to_path, 80);
		
		
		
	}




	/*
	 *
	 * [ makeLarge ]
	 * ________________________________________________________________
	 * 
	 * 
	 * ARGS: 
	 *			Size of bounding box i.e. $square_dim x $square_dim
	 *
	 *
	 */
	 
	public function makeLarge ($square_dim = 600) 
	{
		
		
		$image_path = $imagefilepath_full = CAT_DIR . 'full/' . $image->_file_name;
		$save_to_path = CAT_DIR . 'large/' . $this->_file_name;
		
		if ($image->_hatched == 0 || !file_exists($image_path)) return FALSE;
		
		
		$image = \Cataleya\Asset\ImageCanvas::load($image_path);
		
		if ($image->getHeight() > $image->getWidth()) $image->resizeToHeight($square_dim);
		else $image->resizeToWidth($square_dim);
			
		$image->save($save_to_path, 80);
		
		
	}





	/*
	 *
	 * [ makeZoom ]
	 * ________________________________________________________________
	 * 
	 * ARGS: 
	 *			Size of bounding box i.e. $square_dim x $square_dim
	 *
	 *
	 */
	 
	public function makeZoom ($square_dim = 1200) 
	{
		
		
		$image_path = $imagefilepath_full = CAT_DIR . 'full/' . $image->_file_name;
		$save_to_path = CAT_DIR . 'zoom/' . $this->_file_name;
		
		if ($image->_hatched == 0 || !file_exists($image_path)) return FALSE;
		
		
		$image = \Cataleya\Asset\ImageCanvas::load($image_path);
		
		if ($image->getHeight() > $image->getWidth()) $image->resizeToHeight($square_dim);
		else $image->resizeToWidth($square_dim);
			
		$image->save($save_to_path, 80);
		
		
	}





	/*
	 *
	 * [ Getters ] and [ Setters ]
	 * 
	 * ________________________________________________________________
	 * 
	 * 
	 *       
	 *
	 *
	 *
	 */








	/*
	 *
	 * [ getHref ]
	 * ________________________________________________________________
	 * 
	 * ARGS: 
	 *			use like: $color->image->getHref(\Cataleya\Asset\Banner::THUMB)
	 *
	 *
	 */
	 
	public function getHref ($image_size = self::LARGE) 
	{
		
		
		$image_path = CAT_DIR . $this->_DIR[$image_size] . '/' . $this->_file_name;
		
		if ($image->_hatched == 0 || !file_exists($image_path)) return FALSE;
		
		$href = $this->base_url . $this->_DIR[$image_size] . '/' . $this->_file_name;
		
		return $href;
		
		
	}







}

