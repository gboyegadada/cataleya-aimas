<?php


namespace Cataleya\Asset;




/*
 *
 *	@Package: Cataleya
 *	@Class: \Cataleya\Asset\Text
 *
 *	(c) 2012 Fancy Paper Planes
 *
 *
 *
 */


class Text implements \Cataleya\Asset

{
	
	private 
			$_id;
			
	private $_contents = array();
			
	private $_modified = array();


	private 
			$dbh, 
			$e;
			
			
			



	/*
	 *
	 * [ __construct ]
	 * ________________________________________________________________
	 * 
	 * 
	 *
	 *
	 *
	 */
	 
	private function __construct () 
	{
		
		// Get database handle...
                $this->dbh = \Cataleya\Helper\DBH::getInstance();
		
		// Get error handler
		$this->e = \Cataleya\Helper\ErrorHandler::getInstance();
		
		
	}



	/*
	 *
	 * [ __destruct ]
	 * ________________________________________________________________
	 * 
	 * 
	 *
	 *
	 *
	 */
	 
	public function __destruct () 
	{
		
		
		$this->save();
		
		
	}




	/**
         * 
         * @param integer $_id
         * @return \Cataleya\Asset\Text
         * 
         */
	static public function load ($_id = 0) 
	{
		
		if ($_id == 0) return NULL;
		$text = new \Cataleya\Asset\Text();
		$text->_id = $_id;
		
		///// CHECK IF TEXT ID IS VALID /////////////////////////
		static 
					$lookup_handle, 
					$lookup_handle_param_text_id;
		
		if (empty($lookup_handle))
		{
			$lookup_handle = $text->dbh->prepare('SELECT * FROM text WHERE text_id = :text_id LIMIT 1');
			$lookup_handle->bindParam(':text_id', $lookup_handle_param_text_id, \PDO::PARAM_INT);
		}
		
		$lookup_handle_param_text_id = $_id;
		
		if (!$lookup_handle->execute()) $text->e->triggerException('
										Error in class (' . __CLASS__ . '): [ ' . 
										implode(', ', $lookup_handle->errorInfo()) . 
										' ] on line ' . __LINE__);
										
		$lookup_handle_row = 	$lookup_handle->fetch(\PDO::FETCH_ASSOC);			
		if (empty($lookup_handle_row))
		{ 
			unset($text);
			return NULL;
		}
		



		///// GET TEXT /////////////////////////
		static 
					$select_handle, 
					$select_handle_param_text_id;
		
		if (empty($select_handle))
		{
			$select_handle = $text->dbh->prepare('SELECT * FROM text_content WHERE text_id = :text_id LIMIT 1');
			$select_handle->bindParam(':text_id', $select_handle_param_text_id, \PDO::PARAM_INT);
		}
		
		$select_handle_param_text_id = $_id;
		
		if (!$select_handle->execute()) $text->e->triggerException('
										Error in class (' . __CLASS__ . '): [ ' . 
										implode(', ', $select_handle->errorInfo()) . 
										' ] on line ' . __LINE__);
										
										
		while ($text_row = $select_handle->fetch(\PDO::FETCH_ASSOC)) 
		{
			$text->_contents[$text_row['language_code']] = $text_row;
			
		}
		
		
		return $text;
			
		
		
	}





	/**
         * 
         * @param string $content
         * @param string $language_code
         * @return \Cataleya\Asset\Text
         * 
         */
	static public function create ($content = 'No text', $language_code = 'EN')  // defaults to english (EN)
	{
		
		// construct
		$text = new \Cataleya\Asset\Text();
		
		static $insert_handle;
		
		if (empty($insert_handle))
		{
			$insert_handle = $text->dbh->prepare('INSERT INTO text (date_added) VALUES (NOW())');
		}

		if (!$insert_handle->execute()) $text->e->triggerException('
										Error in class (' . __CLASS__ . '): [ ' . 
										implode(', ', $insert_handle->errorInfo()) . 
										' ] on line ' . __LINE__);
										
		$text->_id = $text->dbh->lastInsertId();
		
		
		if ($text != NULL)
		{
		if(!is_string($language_code) || strlen($language_code) != 2) $language_code = 'EN';
		$language_code = strtoupper($language_code);
		
		$text->setText($content, $language_code);
		}
		
		return $text;
		
	}






	/*
	 *
	 *  [ save ]
	 * ________________________________________________________________
	 * 
	 * 
	 *
	 *
	 *
	 */
	 
	public function save () 
	{
		
            if (empty($this->_modified)) return TRUE;
            
            $this->_modified = array_unique($this->_modified);
                
            foreach ($this->_contents as $key=>$content)
            {
		
                if (!in_array( $key, $this->_modified)) continue;
                
		static 
                        $update_handle, 
                        $update_handle_param_id, 
                        $update_handle_param_language_code, 
                        $update_handle_param_text;
		
		if (empty($update_handle))
		{
			$update_handle = $this->dbh->prepare('
													UPDATE text_content  
													SET text = :text, 
													language_code = :language_code  	
													WHERE text_id = :text_id 
													');
			$update_handle->bindParam(':text_id', $update_handle_param_id, \PDO::PARAM_INT);
			$update_handle->bindParam(':language_code', $update_handle_param_language_code, \PDO::PARAM_STR);
			$update_handle->bindParam(':text', $update_handle_param_text, \PDO::PARAM_STR);
		}
		
		$update_handle_param_id = $this->_id;
		$update_handle_param_language_code = $content['language_code'];
		$update_handle_param_text = $content['text'];

		if (!$update_handle->execute()) $this->e->triggerException('
										Error in class (' . __CLASS__ . '): [ ' . 
										implode(', ', $update_handle->errorInfo()) . 
										' ] on line ' . __LINE__);
										
		
            }
		
		return TRUE;
		
	}




	/*
	 *
	 *  [ Attribute ] interface method: [ getID ]
	 * ________________________________________________________________
	 * 
	 * 
	 *
	 *
	 *
	 */
	 
	public function getID () 
	{
		
		
		return $this->_id;
			
		
		
	}






	/*
	 *
	 *  [ Attribute ] interface method: [ getClassName ]
	 * ________________________________________________________________
	 * 
	 * 
	 *
	 *
	 *
	 */
	 
	public function getClassName () 
	{
		
		
		return __CLASS__;
			
		
		
	}



	/*
	 *
	 *  [ GETTERS ]
	 * ________________________________________________________________
	 * 
	 * 
	 *
	 *
	 *
	 */
	 














	/*
	 *
	 *  [ getDateAdded ]
	 * ________________________________________________________________
	 * 
	 * 
	 *
	 *
	 *
	 */
	 
	public function getDateAdded () 
	{
		
		
		return $this->_date_added;
			
		
		
	}








	/*
	 *
	 *  [ SETTERS ]
	 * ________________________________________________________________
	 * 
	 * 
	 *
	 *
	 *
	 */
	 



	/*
	 *
	 *  [ setText ]
	 * ________________________________________________________________
	 * 
	 * 
	 *
	 *
	 *
	 */
	 
	public function setText ($text = NULL, $language_code = NULL) 
	{
		
		$text = strval($text);
		
		if(!is_string($language_code) || strlen($language_code) != 2) return FALSE;
		
		
		$language_code = strtoupper($language_code);
		
		// construct
		
		static 
				$insert_handle, 
                                $insert_handle_param_text_id, 
				$insert_handle_param_language_code,
				$insert_handle_param_text;
		
		if (empty($insert_handle))
		{
			$insert_handle = $this->dbh->prepare('
                                                                INSERT INTO text_content 
                                                                (text_id, text, language_code, last_modified) 
                                                                 VALUES (:text_id, :text, :language_code, NOW()) 
                                                                 ON DUPLICATE KEY UPDATE text = :text, last_modified = NOW()
                                                                ');
			$insert_handle->bindParam(':text_id', $insert_handle_param_text_id, \PDO::PARAM_INT);
			$insert_handle->bindParam(':language_code', $insert_handle_param_language_code, \PDO::PARAM_STR);
			$insert_handle->bindParam(':text', $insert_handle_param_text, \PDO::PARAM_STR);
		}
		
		$insert_handle_param_text_id = $this->_id;
		$insert_handle_param_language_code = $language_code;
		$insert_handle_param_text = $text;

		if (!$insert_handle->execute()) $this->e->triggerException('
										Error in class (' . __CLASS__ . '): [ ' . 
										implode(', ', $insert_handle->errorInfo()) . 
										' ] on line ' . __LINE__);
										
		
		$this->_contents[$language_code] = array (
                                                                'text'	=>	$text, 
                                                                'language_code'	=>	$language_code, 
                                                                'text_id'	=>	$this->_id, 
                                                                'last_modified'	=>	''
                                                                );
		
		return TRUE;
		
		
	}








	/*
	 *
	 *  [ getText ]
	 * ________________________________________________________________
	 * 
	 * 
	 *
	 *
	 *
	 */
	 
	public function getText ($language_id = NULL) 
	{
		
		
		if ($language_id === NULL) $this->e->triggerException('Error in class (' . __CLASS__ . '): [ Invalid language ID ] on line ' . __LINE__);
		
		if (isset ($this->_contents[$language_id])) return $this->_contents[$language_id]['text'];
		else return '';
			
			
		
		
	}






	/*
	 *
	 *  [ unsetText ]
	 * ________________________________________________________________
	 * 
	 * 
	 *
	 *
	 *
	 */
	 
	public function unsetText ($language_code = NULL) 
	{
		
		if ($language_code === NULL) return FALSE;

		if (isset ($this->_contents[$language_code]))
		{
			
			$language_code = strtoupper($language_code);
			
			// construct
			
			static 
					$delete_handle, 
					$delete_handle_param_language_code;
			
			if (empty($delete_handle))
			{
				$delete_handle = $this->dbh->prepare('
														DELETE FROM text_content 
														WHERE text_id = :text_id
														AND language_code = :language_code 
														');
				$delete_handle->bindParam(':text_id', $delete_handle_param_text_id, \PDO::PARAM_INT);
				$delete_handle->bindParam(':language_code', $delete_handle_param_language_code, \PDO::PARAM_STR);
			}
			
			$delete_handle_param_text_id = $this->_id;
			$delete_handle_param_language_code = $language_code;
	
			if (!$delete_handle->execute()) $this->e->triggerException('
											Error in class (' . __CLASS__ . '): [ ' . 
											implode(', ', $delete_handle->errorInfo()) . 
											' ] on line ' . __LINE__);
											
			
			unset ($this->_contents[$language_code]);
				
		} 
		
		return TRUE;
		
		
	}






	/*
	 *
	 *  [ delete ]
	 * ________________________________________________________________
	 * 
	 * 
	 *
	 *
	 *
	 */
	 
	public function delete () 
	{
			// CLEAN UP...
			$delete_handle = $this->dbh->prepare('
													DELETE FROM text_content 
													WHERE text_id = :text_id
													');
			$delete_handle->bindParam(':text_id', $delete_handle_param_text_id, \PDO::PARAM_INT);
			
			$delete_handle_param_text_id = $this->_id;
	
			if (!$delete_handle->execute()) $this->e->triggerException('
											Error in class (' . __CLASS__ . '): [ ' . 
											implode(', ', $delete_handle->errorInfo()) . 
											' ] on line ' . __LINE__);
                        
                        
				
			// DELETE TEXT ....
			$delete_handle2 = $this->dbh->prepare('
													DELETE FROM text 
													WHERE text_id = :text_id
													');
			$delete_handle2->bindParam(':text_id', $delete_handle2_param_text_id, \PDO::PARAM_INT);
			
			$delete_handle2_param_text_id = $this->_id;
	
			if (!$delete_handle2->execute()) $this->e->triggerException('
											Error in class (' . __CLASS__ . '): [ ' . 
											implode(', ', $delete_handle2->errorInfo()) . 
											' ] on line ' . __LINE__);
											
			

		// $this = NULL;
		return TRUE;
		
		
	}






}

