<?php


namespace Cataleya\Asset;




/*
 *
 *	@Package: Cataleya
 *	@Class: \Cataleya\Asset\Image
 *
 *	(c) 2012 Fancy Paper Planes
 *
 *
 *
 */


class Image implements \Cataleya\Asset

{
	
	private 
				$_image_id, 
				$_image_file, 
				$_image_type, 
				$_date_added, 
				$_hatched;
        
        
        private $_optimized_save = FALSE;
        private $_canvas = NULL;
				


/*
 * These constants will be public so you can do something like: 
 * $color->image->getHref(\Cataleya\Asset\Image::THUMB)
 *
 */
	const TINY = 1;
	const THUMB = 2;
	const LARGE = 3;
	const ZOOM = 4;
        const FULL = 5;
	const TEMP_PREVIEW = 6;
	const TEMP = 7;
        
        const SRC_UPLOAD = 0;
        const SRC_FILE = 1;
        const SRC_URL = 2;


        /*
 * Directory names
 *
 */
 
 	private static $_DIR = array (
							self::TINY => 'tiny/',
							self::THUMB => 'thumb/',
							self::LARGE => 'large/',
							self::ZOOM => 'zoom/', 
                                                        self::FULL => 'full/', 
							self::TEMP_PREVIEW => 'temp/prev_', 
                                                        self::TEMP => 'temp/'
							);
				


				
/*
 * Helpers 
 *
 */
				
	private $dbh, $e, $core, $image, $base_url;
				
				



	/*
	 *
	 * [ __construct ]
	 * ________________________________________________________________
	 * 
	 * 
	 *
	 *
	 *
	 */
	 
	private function __construct () 
	{
		
		// Get database handle...
        $this->dbh = \Cataleya\Helper\DBH::getInstance();
		
		// Get error handler
		$this->e = \Cataleya\Helper\ErrorHandler::getInstance();
		
		// Get Core
		$this->core = \Cataleya\Core::getInstance();
		
		// Set base url
		$this->base_url = (SSL_REQUIRED ? 'https://' : 'http://') . HOST . '/' . ROOT_URI;
		$this->base_url .= 'ui/images/catalog/';
		
		// initialize
		$this->_image_id = 0;
		$this->_image_file = '';
		$this->_image_type = 2; // defaults jpeg i.e. value of IMAGETYPE_JPEG constant.
		$this->_hatched = 0;
		$this->_date_added = NULL;
		
		
	}



	/*
	 *
	 * [ __destruct ]
	 * ________________________________________________________________
	 * 
	 * 
	 *
	 *
	 *
	 */
	 
	public function __destruct () 
	{
		
		
		
		
		
	}




	/*
	 *
	 *  [ Attribute ] interface method: [ load ]
	 * ________________________________________________________________
	 * 
	 * 
	 *
	 *
	 *
	 */
	 
	static public function load ($image_id = 0) 
	{
		
		$image = new \Cataleya\Asset\Image();
                
                $image_id = ($image_id === NULL) ? 0 : intval($image_id);
                
		if ($image_id == 0) $img_row = array(); 
                else $img_row = $image->loadImageInfo($image_id); 
                
		if (!empty($img_row)) 
		{
			$image->_image_id = $img_row['image_id'];
			$image->_description_id = $img_row['description_id'];
			$image->_image_file = $img_row['image_file'];
			$image->_image_type = $img_row['image_type'];
			$image->_hatched = $img_row['hatched'];
			$image->_date_added = $img_row['date_added'];
		} else {
                    
			$image->_image_id = 0;
			$image->_description_id = NULL;
			$image->_image_file = 'transparent_pixel.gif';
			$image->_image_type = 4;
			$image->_hatched = 1;
			$image->_date_added = 0;
		}
		
		
		return $image;
			
		
		
	}


        
        /*
         * 
         *  [ loadImageInfo ]
         * ______________________________________________________
         * 
         * NOTE: Internal method only...
         * 
         * 
         */
        
        
        private function loadImageInfo ($image_id)
        {
            
 		static $image_select, $img_select_param_image_id;
		
		if (empty($image_select))
		{
			$image_select = $this->dbh->prepare('SELECT * FROM images WHERE image_id = :image_id LIMIT 1');
			$image_select->bindParam(':image_id', $img_select_param_image_id, \PDO::PARAM_INT);
		}
		

		$img_select_param_image_id = $image_id;
		
		if (!$image_select->execute()) $this->e->triggerException('
										Error in class (' . __CLASS__ . '): [ ' . 
										implode(', ', $image_select->errorInfo()) . 
										' ] on line ' . __LINE__);
		return $image_select->fetch(\PDO::FETCH_ASSOC);
                
            
        }




        /**
         * 
         * @param integer $_SRC
         * 
         * 1. To create from UPLOAD use: \Cataleya\Asset\Image::SRC_UPLOAD 
         * 2. To create from FILE use: \Cataleya\Asset\Image::SRC_FILE
         * 3. To create from URL use: \Cataleya\Asset\Image::SRC_URL
         * 
         */
        static public function create ($_SRC = self::SRC_UPLOAD) {
            
            
            switch ($_SRC) {
                case self::SRC_UPLOAD:
                    return call_user_func_array(array(__CLASS__, 'createFromFile'), func_get_args());
                    break;
                
                case self::SRC_FILE:
                    return call_user_func_array(array(__CLASS__, 'createFromFile'), func_get_args());
                    break;
                
                case self::SRC_URL:
                    return call_user_func_array(array(__CLASS__, 'createFromURL'), func_get_args());
                    break;
                
                default:
                    \Cataleya\Helper\ErrorHandler::getInstance()->triggerException('
                                        Error in class (' . __CLASS__ . '): [ Unknown source! ] on line ' . __LINE__);
                    break;
            }
            
        }





        /**
         * 
         * @param string $_TYPE
         * @param float $min_width
         * @param float $min_height
         * @param float $prev_width
         * @param float $prev_height
         * @return \Cataleya\Asset\Image
         * 
         */
	static private function createFromFile ($_TYPE = self::SRC_UPLOAD, $src = '', $min_width = 100, $min_height = 0, $prev_width = NULL, $prev_height = NULL) 
	{
		
                $_is_upload = ($_TYPE === self::SRC_UPLOAD) ? true : false;
                
                
		if ($src == '' || !file_exists($src) || ($_is_upload && !is_uploaded_file($src))) return NULL;
		
		// if only width is set, assume bounding box is square...
		if ((int)$min_width > 0 && $min_height == 0) $min_height = $min_width;
		
			
		
		// Get image info...
		$img_info = getimagesize($src);
		
		// If file is not a valid image or is CMYK return NULL.
		if ($img_info === FALSE || $img_info['channels'] != 3 ) return NULL;
		
		// Check image type. We only play with JPG, JPEG200, PNG, and GIF.
		if ($img_info[2] != IMAGETYPE_JPEG && $img_info[2] != IMAGETYPE_PNG && $img_info[2] != IMAGETYPE_GIF && $img_info[2] != IMAGETYPE_JPEG2000) return NULL;

		
		
		
		// if image is smaller than target width and height return NULL...
		//if ($img_info[0] < $min_width || $img_info[1] < $min_height) return NULL;
		
		
		// image mime
                $_uid = sha1_file($src) . image_type_to_extension($img_info[2]);
                
		// Write image file...
		$new_path = CAT_DIR . 'temp/' . $_uid;
                
                // Detect possible file attack...
                $_moved = ($_is_upload) 
                        ? move_uploaded_file($src, $new_path) 
                        : copy($src, $new_path);
                
                
                // Remove from temp folder (incase image was download from url to var/uploads)
                // if (!$_is_upload) { @unlink($src); }

                
                // Upload failed...
		if (!$_moved) {  return NULL; }
                
		
                
		// construct
		$image = new \Cataleya\Asset\Image();
		
                
                
                
		
		static $image_insert, $image_insert_param_image_type, $image_insert_param_image_file;
		
		if (empty($image_insert))
		{
			$image_insert = $image->dbh->prepare('
                                                                INSERT INTO images (image_type, image_file, date_added) 
                                                                VALUES (:image_type, :image_file, NOW())
                                                            ');
			$image_insert->bindParam(':image_type', $image_insert_param_image_type, \PDO::PARAM_INT);
			$image_insert->bindParam(':image_file', $image_insert_param_image_file, \PDO::PARAM_INT);
		}
		
		$image_insert_param_image_type = $img_info[2];
                $image_insert_param_image_file = $_uid;
                
		if (!$image_insert->execute()) $image->e->triggerException('
										Error in class (' . __CLASS__ . '): [ ' . 
										implode(', ', $image_insert->errorInfo()) . 
										' ] on line ' . __LINE__);
										
		$image->_image_id = $image->dbh->lastInsertId();
		$image->_image_type = $img_info[2];
                $image->_image_file = $_uid;
		
		
		
		
		// Make a preview if we are asked to...
		if ($prev_width != NULL && $prev_height != NULL)
                {
                    // load image file...
                    $preview_img = \Cataleya\Helper\ImageCanvas::load($new_path);

                    // scale to specified size...
                    $preview_img->smartResize($prev_width, $prev_height);

                    // save preview
                    $preview_path = CAT_DIR . 'temp/prev_' . $_uid;
                    $preview_img->save($preview_path, 70);
                }
                
                
		
		
		
		return $image;
			
		
		
	}
        
        
        
        
        /**
         * 
         * @param string $_TYPE
         * @param string $url
         * @param float $min_width
         * @param float $min_height
         * @param float $prev_width
         * @param float $prev_height
         * @return \Cataleya\Asset\Image
         * 
         */
        static private function createFromURL ($_TYPE, $url, $min_width = 600, $min_height = 0, $prev_width = NULL, $prev_height = NULL) {
            if (
                    $_TYPE !== self::SRC_URL 
                    || preg_match( '/^(https?)\:\/\/\b/i', $url) < 1 
                    || preg_match( '/[^\?]+\.(jpe?g|jpe|gif|png)\b/i', $url, $matches ) < 1
                    ) { return NULL; }
            
            $image = @file_get_contents($url);
            
            if ($image === FALSE) {
                // Link broken or no connection?...
                return NULL;
            }
            
            $_dir = ROOT_PATH . 'var/uploads/';
            if (!file_exists($_dir))  mkdir($_dir);
            
            $new_path = $_dir . sha1($image) . '.' . $matches[1];
            
            file_put_contents($new_path, $image);
            
            return self::createFromFile(self::SRC_FILE, $new_path, $min_width, $min_height, $prev_width, $prev_height);
            
        }
        
        
        
        
        
        
        
        
        
        
        
        /*
         * 
         * @method private
         * @name _saveFileName
         * 
         */
        
        private function _saveFileName($_filename = NULL) {
		
		$this->_image_file = (is_string($_filename)) 
                        ? $_filename 
                        : 'img_' . $this->getID() . image_type_to_extension($this->getImageType());
		
                // Save file name...
		static 
                        $image_update, 
                        $image_update_param_image_id, 
                        $image_update_param_image_file;
		
		if (empty($image_update))
		{
			$image_update = $this->dbh->prepare('
													UPDATE images SET image_file = :image_file  
													WHERE image_id = :image_id 
													');
			$image_update->bindParam(':image_id', $image_update_param_image_id, \PDO::PARAM_INT);
			$image_update->bindParam(':image_file', $image_update_param_image_file, \PDO::PARAM_STR);
		}
		
		$image_update_param_image_id = $this->getID();
		$image_update_param_image_file = $this->_image_file;

		if (!$image_update->execute()) $this->e->triggerException('
										Error in class (' . __CLASS__ . '): [ ' . 
										implode(', ', $image_update->errorInfo()) . 
										' ] on line ' . __LINE__);
                
                return $this;
        }




        /*
	 *
	 *  [ destroy ]
	 * ________________________________________________________________
	 * 
	 * 
	 *
	 *
	 *
	 */
        
        public function destroy ()
        {
            
            // If image is default (a.k.a tranparent_pixel a.k.a ficticious!)
            if ($this->_image_id == 0) return TRUE;
            
            // Remove from database...
            static 
                    $image_delete,  
                    $param_image_id;


            if (!isset($image_delete)) {
                    // PREPARE SELECT STATEMENT...
                    $image_delete = $this->dbh->prepare('
                                                            DELETE FROM images    
                                                            WHERE image_id = :image_id
                                                            ');


                    $image_delete->bindParam(':image_id', $param_image_id, \PDO::PARAM_INT);
            }


            $param_image_id = $this->_image_id;

            if (!$image_delete->execute())  $this->e->triggerException('
                                                                        Error in class (' . __CLASS__ . '): [ ' . 
                                                                        implode(', ', $image_delete->errorInfo()) . 
                                                                        ' ] on line ' . __LINE__);


            
            // Remove files...
            foreach (self::$_DIR as $dir)
            {
                $image_path = CAT_DIR . $dir . $this->_image_file;
                if (file_exists($image_path))  unlink($image_path);
	
            }
             
             
            
            
            return TRUE;
			
			            
        }
        
        
        
        
        
        
        
        /*
	 *
	 * @method public
         * @name duplicate
	 *
	 */
        
        public function duplicate ()
        {
            
            // If image is default (a.k.a tranparent_pixel a.k.a ficticious!)
            if ($this->_image_id == 0) return $this;
            
            static $image_insert, $image_insert_param_image_type;

            if (empty($image_insert))
            {
                    $image_insert = $this->dbh->prepare('
                                                            INSERT INTO images (image_type, hatched, date_added) 
                                                            VALUES (:image_type, 1, NOW())
                                                        ');
                    $image_insert->bindParam(':image_type', $image_insert_param_image_type, \PDO::PARAM_INT);
            }

            $image_insert_param_image_type = $this->getImageType();
            if (!$image_insert->execute()) $this->e->triggerException('
                                                                            Error in class (' . __CLASS__ . '): [ ' . 
                                                                            implode(', ', $image_insert->errorInfo()) . 
                                                                            ' ] on line ' . __LINE__);

            $_NewImage = \Cataleya\Asset\Image::load($this->dbh->lastInsertId());
            
            
            // Write image file...
            $_NewImage->_saveFileName();

            $src = CAT_DIR . 'full/' . $this->getFileName();
            $new_path = CAT_DIR . 'full/' . $_NewImage->getFileName();

            // Copy file...
            if (!copy($src, $new_path))
            {
                $_NewImage->destroy();
                
                // $this->e->triggerException('Error in class (' . __CLASS__ . '): [ Image file copy failed. ] on line ' . __LINE__);
                return NULL;
            }
            
            
            $_NewImage->makeZoom();
            $_NewImage->makeLarge();
            $_NewImage->makeThumb();
            $_NewImage->makeTiny();

            
            
            return $_NewImage;
			
			            
        }




        /*
	 *
	 *  [ Attribute ] interface method: [ getID ]
	 * ________________________________________________________________
	 * 
	 * 
	 *
	 *
	 *
	 */
	 
	public function getID () 
	{
		
		
		return $this->_image_id;
			
		
		
	}
        
        
        /*
         * 
         * @method public
         * @name getImageType
         * @return int 
         * 
         */
        
        
	public function getImageType () 
	{
		
		
		return $this->_image_type;
			
		
		
	}


        
        
        
        /*
         * 
         * @method public
         * @name getFileName
         * @return int 
         * 
         */
        
        
	public function getFileName () 
	{
		
		
		return $this->_image_file;
			
		
		
	}




	/*
	 *
	 *  [ Attribute ] interface method: [ getClassName ]
	 * ________________________________________________________________
	 * 
	 * 
	 *
	 *
	 *
	 */
	 
	public function getClassName () 
	{
		
		
		return __CLASS__;
			
		
		
	}





	/*
	 *
	 * [ bake ]
	 * ________________________________________________________________
	 * 
	 * ARGS: 
	 *			$params = array (
	 *								'rotate'	=> Rotate angle. Set to '0' to skip (or don't set at all).
	 *								'sw'	=> Selection Width (from user interface)
	 *								'sh'	=> Selection Height (from user interface)
	 *								'sx'	=> Selection Origin (top left corner of selection)
	 *								'sy'	=> Selection Origin (top left corner of selection)
	 *							);
	 *
	 *			$make_sizes: Save image to all sizes ('Tiny', 'Thumb', 'Large', and 'Zoom')
	 *							using default pixel dimensions.
	 *
	 */
	 
	public function bake (array $params = array('rotate'=>0, 'sw'=>0, 'sh'=>0, 'sx'=>0, 'sy'=>0), $make_sizes = FALSE) 
	{
		
                // If image is default (a.k.a tranparent_pixel a.k.a ficticious!)
                if ($this->_image_id == 0) return FALSE;

            
                
		$image_path = $imagefilepath_full = CAT_DIR . 'temp/' . $this->_image_file;
                

		
		// Get image info...
		list($w, $h) = getimagesize($image_path);
		
                
                $params['sw'] = (isset($params['sw']) && $params['sw'] > 0) ? floatval($params['sw']) : $w;
                $params['sh'] = (isset($params['sh']) && $params['sh'] > 0) ? floatval($params['sh']) : $h;
                $params['sx'] = (isset($params['sx']) && $params['sx'] > 0) ? floatval($params['sx']) : 0;
                $params['sy'] = (isset($params['sy']) && $params['sy'] > 0) ? floatval($params['sy']) : 0;
                
		
		$params['rotate'] = isset($params['rotate']) ? (int)$params['rotate'] : 0;
		if ($params['sw'] < 10 || $params['sh'] < 10) return FALSE;
		
		
		$save_to_path = CAT_DIR . 'full/' . $this->_image_file;
		
		if (!file_exists($image_path)) return FALSE;
		
		// load image file...
		$this->_canvas = \Cataleya\Helper\ImageCanvas::load($image_path);
		
		// rotate if needed..
		if ($params['rotate'] != 0)	$this->_canvas->rotate($params['rotate']);
		
		// crop using selection data...
		$this->_canvas->crop($params['sw'], $params['sh'], $params['sx'], $params['sy']);
		
		// save
		$this->_canvas->save($save_to_path, 100);
                
		$this->_hatched = 1;
		
		// Update image info...
		static 
                        $image_update, 
                        $image_update_param_image_id,
                        $image_update_param_hatched;
		
		if (empty($image_update))
		{
			$image_update = $this->dbh->prepare('
                                                            UPDATE images SET hatched = :hatched  
                                                            WHERE image_id = :image_id 
                                                        ');
			$image_update->bindParam(':image_id', $image_update_param_image_id, \PDO::PARAM_INT);
			$image_update->bindParam(':hatched', $image_update_param_hatched, \PDO::PARAM_INT);
		}
		
		$image_update_param_image_id = $this->_image_id;
		$image_update_param_hatched = $this->_hatched;

		if (!$image_update->execute()) $this->e->triggerException('
										Error in class (' . __CLASS__ . '): [ ' . 
										implode(', ', $image_update->errorInfo()) . 
										' ] on line ' . __LINE__);
		
		
		
		// Make sizes if we are asked to.
		if ($make_sizes)
		{
                    $this->_optimized_save = TRUE; // Generate sizes using only one image handle...
                    
                    
                    $this->makeZoom();
                    $this->makeLarge();
                    $this->makeThumb();
                    $this->makeTiny();
                    
 		} else {
                    $this->_optimized_save = FALSE;
                    $this->_canvas->flush();
                    $this->_canvas = NULL;
                }
		
                $preview_path = CAT_DIR . 'temp/prev_' . $this->_image_file;
                    
		// Remove temp files
                unlink($image_path);
                if (file_exists($preview_path)) { unlink($preview_path); }
		
		
		return TRUE;
		
		
		
	}








	/*
	 *
	 * We can make just the sizes we're going to be needing...
	 * with the following methods at any time we choose.
	 * 
	 * ________________________________________________________________
	 * 
	 * Note: 1. These are only available in [Admin_Asset_Image] and not in [Store_Asset_Image]
	 *       2. The [ _make ] method is private.
	 *
	 *
	 *
	 */








	/*
	 *
	 * [ makeTiny ]
	 * ________________________________________________________________
	 * 
	 * ARGS: 
	 *			Size of bounding box i.e. $square_dim x $square_dim
	 *
	 *
	 */
	 
	public function makeTiny ($square_dim = 120) 
	{
            
            $this->make(\Cataleya\Asset\Image::TINY, $square_dim);
 		
            return TRUE;
		
		
	}




	/*
	 *
	 * [ makeThumb ]
	 * ________________________________________________________________
	 * 
	 * ARGS: 
	 *			Size of bounding box i.e. $square_dim x $square_dim
	 *
	 *
	 */
	 
	public function makeThumb ($square_dim = 300) 
	{
		
            $this->make(\Cataleya\Asset\Image::THUMB, $square_dim);		
            return TRUE;
		
	}




	/*
	 *
	 * [ makeLarge ]
	 * ________________________________________________________________
	 * 
	 * 
	 * ARGS: 
	 *			Size of bounding box i.e. $square_dim x $square_dim
	 *
	 *
	 */
	 
	public function makeLarge ($square_dim = 600) 
	{
		
		
            $this->make(\Cataleya\Asset\Image::LARGE, $square_dim);		
            return TRUE;
		
		
	}





	/*
	 *
	 * [ makeZoom ]
	 * ________________________________________________________________
	 * 
	 * ARGS: 
	 *			Size of bounding box i.e. $square_dim x $square_dim
	 *
	 *
	 */
	 
	public function makeZoom ($square_dim = 1200) 
	{
		
		
            $this->make(\Cataleya\Asset\Image::ZOOM, $square_dim);		
            return TRUE;

		
		
	}



        
        
        /*
	 *
	 * [ make ]
	 * ________________________________________________________________
	 * 
	 * ARGS: 
	 *			Size of bounding box i.e. $square_dim x $square_dim
	 *
	 *
	 */
	 
	public function make ($type = \Cataleya\Asset\Image::LARGE, $square_dim = NULL) 
	{
		
                // If image is default (a.k.a tranparent_pixel a.k.a ficticious!)
                if ($this->_image_id == 0) return FALSE;
                
                $imagefilepath_full = $src = CAT_DIR . 'full/' . $this->_image_file;
                if (!file_exists($imagefilepath_full)) return FALSE;
                

		$save_to_path = CAT_DIR . self::$_DIR[$type] . $this->_image_file;
                
                if ($square_dim === NULL)
                {
                    switch ($type)
                    {
 
                        case \Cataleya\Asset\Image::ZOOM:
                            $square_dim = 1200;
                            break;

                         case \Cataleya\Asset\Image::LARGE:
                            $square_dim = 600;
                            break;

                        case \Cataleya\Asset\Image::THUMB:
                            $square_dim = 300;
                            break;
                        
                        case \Cataleya\Asset\Image::TINY:
                            $square_dim = 120;
                            break;

                   }
                }
                
                
		
		
		if ($this->_canvas === NULL)
                {
                    $image_path = $imagefilepath_full;
                    if ($this->_hatched == 0 || !file_exists($image_path)) return FALSE;
                    
                    $this->_canvas = \Cataleya\Helper\ImageCanvas::load($image_path);
                }
		
                
                // So that we only call [->getHeight] and [->getWidth] when we have to.
		static $resize_to_height = NULL;
                if (empty($resize_to_height)) $resize_to_height = ($this->_canvas->getHeight() > $this->_canvas->getWidth());
                
                // Resize based on orientation...
		if ($resize_to_height) $this->_canvas->resizeToHeight($square_dim);
		else $this->_canvas->resizeToWidth($square_dim);
                
                // Save...
		$this->_canvas->save($save_to_path, 100);
                
                // Destroy image canvas if not in 'optimize' mode...
                if (!$this->_optimized_save && $this->_canvas != NULL) 
                {
                    $this->_canvas->flush();
                    $this->_canvas = NULL;
                }
		
		
	}




	/*
	 *
	 * [ Getters ] and [ Setters ]
	 * 
	 * ________________________________________________________________
	 * 
	 * 
	 *       
	 *
	 *
	 *
	 */








	/*
	 *
	 * [ getHref ]
	 * ________________________________________________________________
	 * 
	 * ARGS: 
	 *			use like: $color->image->getHref(\Cataleya\Asset\Image::THUMB)
	 *
	 *
	 */
	 
	public function getHref ($image_size = self::LARGE) 
	{
		
                // If image is default (a.k.a tranparent_pixel a.k.a ficticious!)
                if ($this->_image_id == 0) $image_size = self::FULL;

		
		$image_path = CAT_DIR . self::$_DIR[$image_size] . $this->_image_file;
		
		if ( ($this->_hatched == 0 && $image_size != self::TEMP_PREVIEW) || !file_exists($image_path)) return FALSE;
		
		$href = $this->base_url . self::$_DIR[$image_size] . $this->_image_file;
		
		return $href;
		
		
	}


	/*
	 *
	 * [ getHrefs ]
	 * ________________________________________________________________
	 * 
	 * ARGS: 
	 *			use like: $color->image->getHrefs()
	 *
	 *
	 */
	 
	public function getHrefs () 
	{
		
            return array (
                
                'zoom'  => $this->getHref(self::ZOOM),
                'full' => $this->getHref(self::FULL), 
                'large' => $this->getHref(self::LARGE), 
                'thumb' => $this->getHref(self::THUMB), 
                'tiny'  => $this->getHref(self::TINY)
            );
		
		
	}


        
        

	/*
	 *
	 * [ getFileName ]
	 * ________________________________________________________________
	 * 
	 * 
	 *
	 *
	 *
	 */
	 
	public function getSize ($image_format = self::LARGE) 
	{
		
               // If image is default (a.k.a tranparent_pixel a.k.a ficticious!)
                if ($this->_image_id == 0) $image_format = self::FULL;

		
		$image_path = CAT_DIR . self::$_DIR[$image_format] . $this->_image_file;
		
		if ( ($this->_hatched == 0 && $image_format != self::TEMP_PREVIEW) || !file_exists($image_path)) return FALSE;
		
		return getimagesize($image_path);
			
		
		
	}
        
        
        
        private static function _remove_files ($_file, $_folders) {
                
            // Remove files...
            foreach ($_folders as $dir)
            {
                $image_path = CAT_DIR . $dir . $_file;
                if (file_exists($image_path))  { unlink($image_path); }
            }
                
                
        }


        
        
        public static function gc () {
            
            
            $image_path = CAT_DIR . self::$_DIR[self::FULL];
            $_files = \Cataleya\Helper::readFolder($image_path, 'jpeg|jpg|png');
            $_deleted = 0;
            
            
            static 

            $select_handle, 
            $delete_handle, 
            $delete_handle2;



            if (empty($select_handle)):
            $delete_handle2 = \Cataleya\Helper\DBH::getInstance()->prepare('DELETE FROM images WHERE hatched=0 AND date_added < (NOW()-INTERVAL 20 MINUTE)');
            $select_handle = \Cataleya\Helper\DBH::getInstance()->prepare('SELECT uid FROM files WHERE locks=0');
            $delete_handle = \Cataleya\Helper\DBH::getInstance()->prepare('DELETE FROM files WHERE locks=0');
            endif;
            
            
            
            if (!$delete_handle2->execute()) { 

                \Cataleya\Helper\ErrorHandler::getInstance()->triggerException('
                                        Error in class (' . __CLASS__ . '): [ ' . 
                                        implode(', ', $delete_handle2->errorInfo()) . 
                                        ' ] on line ' . __LINE__);

            }

            if (!$select_handle->execute()) { 

                \Cataleya\Helper\ErrorHandler::getInstance()->triggerException('
                                        Error in class (' . __CLASS__ . '): [ ' . 
                                        implode(', ', $select_handle->errorInfo()) . 
                                        ' ] on line ' . __LINE__);

            }

            $_uids = array ();
            while ($row = $select_handle->fetch(\PDO::FETCH_ASSOC)) { $_uids[]=$row['uid']; }


            
            
            
            
            foreach ($_files as $_f) {
                if (in_array($_f, $_uids)) { self::_remove_files($_f, self::$_DIR); $_deleted++; }

                // Delete only as much as (15 * versions) files at a time...
                //if ($_deleted++ >= 20) { return; }
            }
            
            
            
            // Remove from DB...
            if (!$delete_handle->execute()) { 

                \Cataleya\Helper\ErrorHandler::getInstance()->triggerException('
                                        Error in class (' . __CLASS__ . '): [ ' . 
                                        implode(', ', $delete_handle->errorInfo()) . 
                                        ' ] on line ' . __LINE__);

            }
            
            return $_deleted;
            
            
        }





}

