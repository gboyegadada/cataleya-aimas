<?php


namespace Cataleya\Asset;




/*
 *
 *	@Package: Cataleya
 *	@Class: \Cataleya\Asset\URLRewrite
 *
 *	(c) 2012 Fancy Paper Planes
 *
 *
 *
 */


class URLRewrite implements \Cataleya\Asset

{
	
	private $_data = array();
			
	private $_modified = FALSE;


	private 
			$dbh, 
			$e;
			
			
			



	/*
	 *
	 * [ __construct ]
	 * ________________________________________________________________
	 * 
	 * 
	 *
	 *
	 *
	 */
	 
	private function __construct () 
	{
		
		// Get database handle...
        $this->dbh = \Cataleya\Helper\DBH::getInstance();
		
		// Get error handler
		$this->e = \Cataleya\Helper\ErrorHandler::getInstance();
		
		
	}



	/*
	 *
	 * [ __destruct ]
	 * ________________________________________________________________
	 * 
	 * 
	 *
	 *
	 *
	 */
	 
	public function __destruct () 
	{
		
		
		$this->save();
		
		
	}




	/*
	 *
	 *  [ Attribute ] interface method: [ load ]
	 * ________________________________________________________________
	 * 
	 * 
	 *
	 *
	 *
	 */
	 
	static public function load ($id = NULL) 
	{
		
		if ($id == 0 || filter_var($id, FILTER_VALIDATE_INT) === FALSE) return NULL;
		
		$rewrite = new \Cataleya\Asset\URLRewrite();
		
		static 
				$rewrite_select, 
				$rewrite_select_param_url_rewrite_id;
		
		if (empty($rewrite_select))
		{
			$rewrite_select = $rewrite->dbh->prepare('SELECT * FROM url_rewrite WHERE url_rewrite_id = :url_rewrite_id LIMIT 1');
			$rewrite_select->bindParam(':url_rewrite_id', $rewrite_select_param_url_rewrite_id, \PDO::PARAM_STR);
		}
		
		$rewrite_select_param_url_rewrite_id = $id;
		
		if (!$rewrite_select->execute()) $rewrite->e->triggerException('
										Error in class (' . __CLASS__ . '): [ ' . 
										implode(', ', $rewrite_select->errorInfo()) . 
										' ] on line ' . __LINE__);
										
										
		if ($rewrite_row = $rewrite_select->fetch(\PDO::FETCH_ASSOC)) 
		{
			$rewrite->_data = $rewrite_row;
			
		} else {
			
			unset($rewrite);
			return NULL;
		}
		
		
		return $rewrite;
			
		
		
	}





	/*
	 *
	 *  [ Attribute ] interface method: [ create ]
	 * ________________________________________________________________
	 * 
	 * 
	 *
	 *
	 *
	 */
	 
	static public function create (\Cataleya\Store $_Store=NULL, $keyword = '', $target_path = '', $is_admin = 0) 
	{
		
		$url = new \Cataleya\Asset\URLRewrite ();
		$is_admin = (intval($is_admin) > 1) ? 0 : intval($is_admin);
		
		$keyword = $url->makeSafeKeyword($keyword);
                $_store_id = ($_Store !== NULL) ? $_Store->getID() : NULL;

		
		//// Create new REWRITE...
		static 
				$insert_handle;
                
                
				$insert_handle_param_keyword;
				$insert_handle_param_target_path; 
				$insert_handle_param_is_admin;
                                $insert_handle_param_store_id;
                                
                
		
		if (empty($insert_handle))
		{
			$insert_handle = $url->dbh->prepare('
                                                                INSERT INTO url_rewrite (store_id, keyword, target_path, is_admin) 
                                                                VALUES (:store_id, :keyword, :target_path, :is_admin) 
                                                                ');
		}
		
                
                $insert_handle->bindParam(':keyword', $insert_handle_param_keyword, \PDO::PARAM_STR);
                $insert_handle->bindParam(':target_path', $insert_handle_param_target_path, \PDO::PARAM_STR);
                $insert_handle->bindParam(':is_admin', $insert_handle_param_is_admin, \PDO::PARAM_INT);
		$insert_handle->bindParam(':store_id', $insert_handle_param_store_id, \Cataleya\Helper\DBH::getTypeConst($_store_id));
                        
		$insert_handle_param_keyword = $keyword;
		$insert_handle_param_target_path = $target_path;
		$insert_handle_param_is_admin = $is_admin;
                $insert_handle_param_store_id = ($_Store !== NULL) ? $_Store->getID() : NULL;
                

		if (!$insert_handle->execute()) $url->e->triggerException('
										Error in class (' . __CLASS__ . '): [ ' . 
										implode(', ', $insert_handle->errorInfo()) . 
										' ] on line ' . __LINE__);
										
		
		$url->_data['url_rewrite_id'] = $url->dbh->lastInsertId();
		$url->_data['keyword'] = $keyword;
		$url->_data['target_path'] = $target_path;
		$url->_data['is_admin'] = $is_admin;
		$url->_data['store_id'] = $is_admin;
		
		return $url;
				
	}






	/*
	 *
	 *  [ save ]
	 * ________________________________________________________________
	 * 
	 * 
	 *
	 *
	 *
	 */
	 
	public function save () 
	{
		
		if (!$this->_modified) return TRUE;
                
		
		static 
				$rewrite_update,
				$rewrite_update_param_url_rewrite_id, 
				$rewrite_update_param_keyword, 
				$rewrite_update_param_target_path, 
				$rewrite_update_param_is_admin;
		
		if (empty($rewrite_update))
		{
			$rewrite_update = $this->dbh->prepare('
													UPDATE url_rewrite 
													SET keyword = :keyword, 
														target_path = :target_path, 
														is_admin = :is_admin 
													WHERE url_rewrite_id = :url_rewrite_id 
													');
			$rewrite_update->bindParam(':url_rewrite_id', $rewrite_update_param_url_rewrite_id, \PDO::PARAM_INT);
			$rewrite_update->bindParam(':keyword', $rewrite_update_param_keyword, \PDO::PARAM_STR);
			$rewrite_update->bindParam(':target_path', $rewrite_update_param_target_path, \PDO::PARAM_STR);
			$rewrite_update->bindParam(':is_admin', $rewrite_update_param_is_admin, \PDO::PARAM_INT);
		}
		
		$rewrite_update_param_url_rewrite_id = $this->_data['url_rewrite_id'];
		$rewrite_update_param_keyword = $this->_data['keyword'];
		$rewrite_update_param_target_path = $this->_data['target_path'];
		$rewrite_update_param_is_admin = $this->_data['is_admin'];

		if (!$rewrite_update->execute()) $this->e->triggerException('
										Error in class (' . __CLASS__ . '): [ ' . 
										implode(', ', $rewrite_update->errorInfo()) . 
										' ] on line ' . __LINE__);
										
		
		
		
		return TRUE;
		
	}


        
        
        
        
        

	/*
	 *
	 *  [ delete ]
	 * ________________________________________________________________
	 * 
	 * 
	 *
	 *
	 *
	 */
	 
	public function delete () 
	{
			// CLEAN UP...
			$entry_delete = $this->dbh->prepare('DELETE FROM url_rewrite WHERE url_rewrite_id = :id');
			$entry_delete->bindParam(':id', $entry_delete_param_id, \PDO::PARAM_INT);
			
			$entry_delete_param_id = $this->_data['url_rewrite_id'];
	
			if (!$entry_delete->execute()) $this->e->triggerException('
											Error in class (' . __CLASS__ . '): [ ' . 
											implode(', ', $entry_delete->errorInfo()) . 
											' ] on line ' . __LINE__);
                        
                        

		return TRUE;
		
		
	}




        
        
        
        
        
        
        
        
        
        



	/*
	 *
	 *  [ Attribute ] interface method: [ getID ]
	 * ________________________________________________________________
	 * 
	 * 
	 *
	 *
	 *
	 */
	 
	public function getID () 
	{
		
		
		return (!empty($this->_data['url_rewrite_id'])) ? $this->_data['url_rewrite_id'] : 0;
			
		
		
	}






	/*
	 *
	 *  [ Attribute ] interface method: [ getClassName ]
	 * ________________________________________________________________
	 * 
	 * 
	 *
	 *
	 *
	 */
	 
	public function getClassName () 
	{
		
		
		return __CLASS__;
			
		
		
	}



	/*
	 *
	 *  [ GETTERS ]
	 * ________________________________________________________________
	 * 
	 * 
	 *
	 *
	 *
	 */
	 




	/*
	 *
	 *  [ getKeyword ]
	 * ________________________________________________________________
	 * 
	 * 
	 *
	 *
	 *
	 */
	 
	public function getKeyword () 
	{
		
		
		return $this->_data['keyword'];
			
		
		
	}




	/*
	 *
	 *  [ getTarget ]
	 * ________________________________________________________________
	 * 
	 * 
	 *
	 *
	 *
	 */
	 
	public function getTarget () 
	{
		
		
		return $this->_data['target_path'];
			
		
		
	}






	/*
	 *
	 *  [ isAdmin ]
	 * ________________________________________________________________
	 * 
	 * 
	 *
	 *
	 *
	 */
	 
	public function isAdmin () 
	{
		
		
		return (intval($this->_data['is_admin']) === 1);
			
		
		
	}





        /*
         *
         *  @return int Store ID
         *
         *
         */
        public function getStoreId()
        {
                return $this->_data['store_id'];
        }



        /*
         *
         * [ getStore ] 
         *_____________________________________________________
         *
         *
         */

        public function getStore()
        {
            
            
                return (is_numeric($this->_data['store_id'])) 
                                ? \Cataleya\Store::load((int)$this->_data['store_id']) 
                                : NULL;
                
        }






	/*
	 *
	 *  [ SETTERS ]
	 * ________________________________________________________________
	 * 
	 * 
	 *
	 *
	 *
	 */
	 




	/*
	 *
	 *  [ setName ]
	 * ________________________________________________________________
	 * 
	 * 
	 *
	 *
	 *
	 */
	 
	public function setKeyword ($value = '') 
	{
		
		if ($value == '' || !is_string($value)) return FALSE;
                
                $value = $this->makeSafeKeyword($value);
                if (empty($value)) return FALSE;
                
                
		$this->_data['keyword'] = $value;
		if(!$this->_modified) $this->_modified = TRUE;
		
		return TRUE;
		
		
	}
        
        
        
        



	/*
	 *
	 *  [ makeSafeKeyword ]
	 * ________________________________________________________________
	 * 
	 * 
	 *
	 *
	 *
	 */
	 
	public function makeSafeKeyword ($keyword = '', \Cataleya\Store $_Store=NULL) 
	{
		
            
		
		if ($keyword === '') 
		{
                    $keyword = 'untitled-' . time();
		
		} else {
			
                    $keyword = $this->_sanitizeTitleWithDashes($keyword);
                    $_store_id = ($_Store !== NULL) ? $_Store->getID() : ((isset($this->_data['store_id']) && is_numeric($this->_data['store_id'])) ? (int)$this->_data['store_id'] : NULL);

                    static $rw_entry_check;
                    
                    
                    $rw_entry_check_param_keyword; 
                    $rw_entry_check_param_url_rewrite_id;
                    $rw_entry_check_param_store_id;
                    $rw_entry_check_param_store_id;

                    if (empty($rw_entry_check)) 
                    {
                        // first check if there's an existing entry...
                        $rw_entry_check = $this->dbh->prepare(''
                                . 'SELECT 1 FROM url_rewrite '
                                . 'WHERE keyword = :keyword '
                                . 'AND url_rewrite_id != :url_rewrite_id '
                                // . 'AND store_id != :store_id'
                                . '');
                    }
                    
                    $rw_entry_check->bindParam(':keyword', $rw_entry_check_param_keyword, \PDO::PARAM_STR);
                    $rw_entry_check->bindParam(':url_rewrite_id', $rw_entry_check_param_url_rewrite_id, \PDO::PARAM_INT);
                    // $rw_entry_check->bindParam(':store_id', $rw_entry_check_param_store_id, \Cataleya\Helper\DBH::getTypeConst($_store_id));

                    $rw_entry_check_param_keyword = $keyword;
                    $rw_entry_check_param_url_rewrite_id = $this->getID();
                    $rw_entry_check_param_store_id = $_store_id;

                    
                    
                    if (!$rw_entry_check->execute())  $this->e->triggerException('
                                                                                    Error in class (' . __CLASS__ . '): [ ' . 
                                                                                    implode(', ', $rw_entry_check->errorInfo()) . 
                                                                                    ' ] on line ' . __LINE__);


                    // if keyword already exists, append a timestamp + random number to resolve conflict
                    $row = $rw_entry_check->fetch(\PDO::FETCH_ASSOC); 				
                    if ( !empty($row) ) {

                        $keyword .= '-' . time() . '-' . rand(10, 200);
                    }
                
		}
		
		
		// Return safe keyword
		return $keyword;	
		
		
	}

        
        
        
        
        
        
        




	/*
	 *
	 *  [ setTargetPath ]
	 * ________________________________________________________________
	 * 
	 * 
	 *
	 *
	 *
	 */
	 
	public function setTargetPath ($value = '') 
	{
		
		if ($value == '' || !is_string($value)) return FALSE;
		$this->_data['target_path'] = $value;
		if(!$this->_modified) $this->_modified = TRUE;
		
		return TRUE;
		
		
	}






	/*
	 *
	 *  [ setLanguage ]
	 * ________________________________________________________________
	 * 
	 * 
	 *
	 *
	 *
	 */
	 
	public function setIsAdmin ($value = NULL) 
	{
		
		if ($value === NULL || !is_int($value) || $value > 1) return FALSE;
		$this->_data['is_admin'] = $value;
		if(!$this->_modified) $this->_modified = TRUE;
		
		return TRUE;
		
		
	}





	/**
	 * 
	 * Sanitizes title, replacing whitespace and a few other characters with dashes.
	 *
	 * Limits the output to alphanumeric characters, underscore (_) and dash (-).
	 * Whitespace becomes a dash.
	 *
	 *
	 * @param string $title The title to be sanitized.
	 * @param string $raw_title Optional. Not used.
	 * @param string $context Optional. The operation for which the string is sanitized.
	 * @return string The sanitized title.
	 */
	private function _sanitizeTitleWithDashes($title, $raw_title = '', $context = 'display') {
		$title = strip_tags($title);
		// Preserve escaped octets.
		$title = preg_replace('|%([a-fA-F0-9][a-fA-F0-9])|', '---$1---', $title);
		// Remove percent signs that are not part of an octet.
		$title = str_replace('%', '', $title);
		// Restore octets.
		$title = preg_replace('|---([a-fA-F0-9][a-fA-F0-9])---|', '%$1', $title);
	
		if (seems_utf8($title)) {
			if (function_exists('mb_strtolower')) {
				$title = mb_strtolower($title, 'UTF-8');
			}
			$title = utf8_uri_encode($title, 200);
		}
	
		$title = strtolower($title);
		$title = preg_replace('/&.+?;/', '', $title); // kill entities
		$title = str_replace('.', '-', $title);
	
		if ( 'save' == $context ) {
			// Convert nbsp, ndash and mdash to hyphens
			$title = str_replace( array( '%c2%a0', '%e2%80%93', '%e2%80%94' ), '-', $title );
	
			// Strip these characters entirely
			$title = str_replace( array(
				// iexcl and iquest
				'%c2%a1', '%c2%bf',
				// angle quotes
				'%c2%ab', '%c2%bb', '%e2%80%b9', '%e2%80%ba',
				// curly quotes
				'%e2%80%98', '%e2%80%99', '%e2%80%9c', '%e2%80%9d',
				'%e2%80%9a', '%e2%80%9b', '%e2%80%9e', '%e2%80%9f',
				// copy, reg, deg, hellip and trade
				'%c2%a9', '%c2%ae', '%c2%b0', '%e2%80%a6', '%e2%84%a2',
			), '', $title );
	
			// Convert times to x
			$title = str_replace( '%c3%97', 'x', $title );
		}
	
		$title = preg_replace('/[^%a-z0-9 _-]/', '', $title);
		$title = preg_replace('/\s+/', '-', $title);
		$title = preg_replace('|-+|', '-', $title);
		$title = trim($title, '-');
	
		return $title;
	}
        
        






}

