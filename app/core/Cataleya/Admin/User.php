<?php


namespace Cataleya\Admin;







/*
 *
 * [ Cataleya \ Admin \ Class \ admin \ User ]
 * ________________________________________________________________________
 *
 * (c) 2012 Fancy Paper Planes
 *
 *
*/





// if (!defined ('IS_ADMIN_FLAG') || !IS_ADMIN_FLAG) die("Illegal Access");






class User 
extends \Cataleya\Data 
implements \Cataleya\System\Event\Observable {
	
			
			
	protected $dbh, $e, $bcrypt;
	protected $_data = array();
	protected $_role, $_display_picture; 
	protected $_modified = array();
    protected $_store_key = NULL;
    
    protected static $_events = [
            'admin/user/created',
            'admin/user/updated', 
            'admin/user/deleted', 
        
            'admin/user/enabled', 
            'admin/user/disabled', 
            
            'admin/user/logged-in',
            'admin/user/logged-out',  
            'admin/user/login-failed' 
        ];

    const TABLE = 'admin';
    const PK = 'admin_id';
	
	
	private function __construct($admin_id) 
	{
            
                // Do a chrone job everytime this class file is loaded
                self::garbageCollect();
                
		// Get error handler
		$this->e = \Cataleya\Helper\ErrorHandler::getInstance();
		
		$admin_email = filter_var($admin_id, FILTER_VALIDATE_EMAIL);
		$admin_id = filter_var($admin_id, FILTER_VALIDATE_INT);
		
		if ($admin_email === FALSE && $admin_id === FALSE) 
		{
			if (defined('DEBUG_MODE') && DEBUG_MODE === TRUE) 
			{
			$this->e->triggerException('Error in class (' . __CLASS__ . '): [ Invalid username ] on line ' . __LINE__);
			}
			
			$this->_data = array();
			return;
		}
		
		// Get database handle...
		$this->dbh = \Cataleya\Helper\DBH::getInstance();
		
		// Get encryption class...
		$this->bcrypt = \Cataleya\Helper\Bcrypt::getInstance();
		
		
		// ATTEMPT TO LOAD CUSTOMER DATA
		
		static $admin_select, $param_id;
		
		
		$param_id = ($admin_email !== FALSE) ? $admin_email : $admin_id;
                
		// Do prepared statement...
		if (empty($admin_select)) {
		$admin_select = $this->dbh->prepare('
                                                        SELECT * FROM admin 
                                                        WHERE admin_id = :admin_id 
                                                        OR admin_email = :admin_id 
                                                        LIMIT 1
                                                        ');
		}
		
		$admin_select->bindParam(':admin_id', $param_id, \PDO::PARAM_STR);
		
		// Execute...
		if (!$admin_select->execute()) $this->e->triggerError('Error in class (' . __CLASS__ . '): [ ' . implode(', ', $admin_select->errorInfo()) . ' ] on line ' . __LINE__);
		
		// Check for results...
		if ( ($this->_data = $admin_select->fetch(\PDO::FETCH_ASSOC)) === FALSE ) 
		{
			$this->_data = array();
		} else {
                    // Load user permissions...
                    
                }

		
			
	}
	
	
	function __destruct () 
	{
		
            $this->saveData();
		
	}
	



 


	/*
	 *
	 *  [ load ]
	 * ________________________________________________________________
	 * 
	 * 
	 *
	 *
	 *
	 */
	 
	static public function load ($_id = FALSE) 
	{
		if ($_id === FALSE) return NULL;

		// Validate User ID
		$user = new \Cataleya\Admin\User ($_id);
		if (empty($user->_data))
		{
			unset($user);
			return NULL;
		}			
		
		
		////////////////// RETURN USER ////////////////////
		
		return $user;
                
         }



        
        


	/**
	 * create
	 *
	 * @param string $username
	 * @param string $password
	 * @param \Cataleya\Admin\Role $_Role
	 * @param string $options
	 * @param 'lastname'=>'Lastname') $'lastname'=>'Lastname')
	 * @return void
	 */
	static public function create ($username = '', $password = '', \Cataleya\Admin\Role $_Role = NULL, $options = array('firstname'=>'Firstname', 'lastname'=>'Lastname')) 
	{
		
		if (filter_var($username, FILTER_VALIDATE_EMAIL) === FALSE || $password === '' || $_Role === NULL) return NULL;

		 

		
		// Get database handle...
                $dbh = \Cataleya\Helper\DBH::getInstance();
		
		// Get error handler
		$e = \Cataleya\Helper\ErrorHandler::getInstance();

		// Get encryption class...
		$bcrypt = \Cataleya\Helper\Bcrypt::getInstance();
                
                
                // //// START TRANSACTION /////
                $dbh->beginTransaction();
                
		// construct
		static 
				$admin_insert, 
                $admin_insert_param_email, 
                $admin_insert_param_pass, 
                $admin_insert_param_firstname, 
                $admin_insert_param_lastname, 
                $admin_insert_param_admin_phone, 
                $admin_insert_param_role_id;
		
		if (empty($admin_insert))
		{
			$admin_insert = $dbh->prepare('
                                                        INSERT INTO admin 
                                                        (admin_email, admin_pass, firstname, lastname, admin_phone, role_id, date_created, last_modified) 
                                                        VALUES (:admin_email, :admin_pass, :firstname, :lastname, :admin_phone, :role_id, NOW(), NOW())
                                                        ');
															
			$admin_insert->bindParam(':admin_email', $admin_insert_param_email, \PDO::PARAM_STR);
			$admin_insert->bindParam(':admin_pass', $admin_insert_param_pass, \PDO::PARAM_STR);
            $admin_insert->bindParam(':firstname', $admin_insert_param_firstname, \PDO::PARAM_STR);
            $admin_insert->bindParam(':lastname', $admin_insert_param_lastname, \PDO::PARAM_STR);
            $admin_insert->bindParam(':admin_phone', $admin_insert_param_admin_phone, \PDO::PARAM_STR);
            $admin_insert->bindParam(':role_id', $admin_insert_param_role_id, \PDO::PARAM_INT);
		}
		
		$admin_insert_param_email = $username;
		$admin_insert_param_pass = $bcrypt->hash($password);
        
        $admin_insert_param_role_id = $_Role->getID();
        
        $admin_insert_param_firstname = isset($options['firstname']) ? $options['firstname'] : 'Firstname';
        $admin_insert_param_lastname = isset($options['lastname']) ? $options['lastname'] : 'Lastname';
        $admin_insert_param_admin_phone = isset($options['admin_phone']) ? $options['admin_phone'] : '';
		
		if (!$admin_insert->execute()) $e->triggerException('
										Error in class (' . __CLASS__ . '): [ ' . 
										implode(', ', $admin_insert->errorInfo()) . 
										' ] on line ' . __LINE__);
										
		// Cache last insert id
                $_last_id = $dbh->lastInsertId();
                
                

                // SETUP NOTIFICATIONS
                static $insert_handle2;
                
                if (empty($insert_handle2)) 
                {
                    $insert_handle2 = $dbh->prepare('
                        INSERT INTO alerts (notification_id, admin_id) 
                        SELECT notification_id, :admin_id 
                        FROM notification_profile 
                     ');
                    
                }
                
                $insert_handle2_params_id = $_last_id;
                $insert_handle2->bindParam(':admin_id', $insert_handle2_params_id, \PDO::PARAM_INT);
                
		if (!$insert_handle2->execute()) \Cataleya\Helper\ErrorHandler::getInstance()->triggerException('
										Error in class (' . __CLASS__ . '): [ ' . 
										implode(', ', $insert_handle2->errorInfo()) . 
										' ] on line ' . __LINE__);
                
                
                // Notify sambady anybady!!
                $_current_user = (defined('SESSION_PREFIX') && SESSION_PREFIX === 'ADMIN_') ? $_SESSION[SESSION_PREFIX.'NAME'] : 'Setup';
                $_log_entry = $_current_user . ' has created a new admin account for ' . $admin_insert_param_firstname . ' ' . $admin_insert_param_lastname . '.';
                
                
                
                // //// COMMIT TRANSACTION /////
                $dbh->commit();
                
                
		// AUTOLOAD NEW USER AND RETURN IT
		$admin = self::load($_last_id);
                
                
                \Cataleya\System::load()->updateAdminUserCount(1);
                \Cataleya\System\Event::notify('admin/user/created', $admin, array (
                    'scope'=>'admin/user/created', 
                    'count'=>1, 
                    'logger_id'=>'admin.log', 
                    'blob'=>$_log_entry,
                    'fields' => $admin->_data
                    ));
                
		
		return $admin;
		
		
	}


    
    /**
     * getEvents
     * 
     * @return array
     */
    public static function getEvents() {

        return self::$_events;
    }







    public function saveData () 
    {
        $_m = $this->_modified;
        
        parent::saveData();
        
        
        if (!empty($_m)) {
                \Cataleya\System\Event::notify('admin/user/updated', $this, array (
                    'fields' => $_m
                    ));
        }
        

    }



/*
 *
 * [ AUTHENTICATE ] 
 * __________________________________________________________________________________
 *
 *
 *
 */	
	
	
	





	///// ADMIN LOGIN /////////////////

	public static function login ($_id = FALSE, $_pass = FALSE)
	{
		if ($_id === FALSE || $_pass === FALSE) return NULL;
		
		// Validate User ID
		$user = new \Cataleya\Admin\User ($_id);
		if (empty($user->_data))
		{
			unset($user);       
                        \Cataleya\Helper\Stats::updateFailedLogins(TRUE);
                        if (\Cataleya\Helper\Stats::detectBruteForce(TRUE)) sleep(4);
                        
			return NULL;
		}
		
		
		// Validate Password
		if (FALSE === $user->bcrypt->verify($_pass, $user->_data['admin_pass']))
		{       
                        \Cataleya\Helper\Stats::updateFailedLogins(TRUE);
                        if (\Cataleya\Helper\Stats::detectBruteForce(TRUE)) sleep(4);
                        
                        $_update_params_admin_id = $user->_data['admin_id'];
                        $_update_params_geo_loc = isset ($_SESSION[SESSION_PREFIX.'GEO_LOC']) ? $_SESSION[SESSION_PREFIX.'GEO_LOC'] : 'Unknown';
                        $update_handle = $user->dbh->prepare('
                                            UPDATE admin 
                                            SET is_online = 0, failed_logins = failed_logins+1, last_failed_geo_loc = :geo_loc, last_failed_attempt = NOW(), last_modified = NOW()    
                                            WHERE admin_id = :admin_id 
                                            ');
                        $update_handle->bindParam(':admin_id', $_update_params_admin_id, \PDO::PARAM_INT);
                        $update_handle->bindParam(':geo_loc', $_update_params_geo_loc, \PDO::PARAM_STR);
                        if (!$update_handle->execute()) $user->e->triggerException('Error in class (' . __CLASS__ . '): [ ' . implode(', ', $update_handle->errorInfo()) . ' ] on line ' . __LINE__);
		
            \Cataleya\System\Event::notify('admin/user/login-failed', $user, array ());
			// unset($user);
			return NULL;
		}
		
		
		
		///////////// UPDATE LOGIN STATUS ///////////////////////////
		
		$_update_params = array();

		// Do prepared statement for 'UPDATE'...
		$update_handle = $user->dbh->prepare('
                UPDATE admin 
                SET is_online=:is_online, last_login_geo_loc=:geo_loc, last_login_date=NOW(), last_modified=NOW()    
                WHERE admin_id=:admin_id 
												');
		$update_handle->bindParam(':admin_id', $_update_params['admin_id'], \PDO::PARAM_INT);
		$update_handle->bindParam(':is_online', $_update_params['is_online'], \PDO::PARAM_STR);
        $update_handle->bindParam(':geo_loc', $_update_params_geo_loc, \PDO::PARAM_STR);
		
		
		$_update_params['admin_id'] = $user->_data['admin_id'];
		$_update_params['is_online'] = $user->_data['is_online'] = base64_encode(mcrypt_create_iv(32)) . ':' . (CURRENT_DT+LOGIN_EXPIRES_AFTER);
        $_update_params_geo_loc = isset ($_SESSION[SESSION_PREFIX.'GEO_LOC']) 
            ? $_SESSION[SESSION_PREFIX.'GEO_LOC'] 
            : 'Unknown';
                        
		
		if (!$update_handle->execute()) $user->e->triggerException('Error in class (' . __CLASS__ . '): [ ' . implode(', ', $update_handle->errorInfo()) . ' ] on line ' . __LINE__);
		
		
		////////////////// RETURN USER ////////////////////
		

        \Cataleya\System\Event::notify('admin/user/logged-in', $user, array ());
		return $user;
		
	}
	



        
        



	///// ADMIN AUTH // Returns $USER instance if online or NULL otherwise /////////////////

	public static function authenticate ($_id = FALSE, $_is_online = FALSE, $_auto_logout = TRUE)
	{
		if ($_id === FALSE || $_is_online === FALSE) return NULL;

		
		$_update_params = array();
        
		// 1. User exists..
		$user = new \Cataleya\Admin\User ($_id);
		if (empty($user->_data))
		{
			unset($user);
			return null;
		}
        
        // 2. Token is set...
		if ($user->_data['is_online'] === '0' || $user->_data['is_online'] === 0) 
        {
            // No: not online
			unset($user);
            return null;
        }
        
        
        // 3. Validate token...
		list($token, $timestamp) = explode(':', $user->_data['is_online']);
		if ($token == $_is_online )
		{
			
			if ( (int)$timestamp < (int)CURRENT_DT )
			{
				$this->logout();
                return null;
			}
            
            // Set new timestamp
            $_update_params['is_online'] = $token . ':' . (CURRENT_DT+LOGIN_EXPIRES_AFTER);
			
		} else {
			// $this->logout();
            unset($user);
            return null;
        }
        
        
        
        
		
		
		///////////// UPDATE LOGIN STATUS ///////////////////////////
		

		// Do prepared statement for 'UPDATE'...
		$update_handle = $user->dbh->prepare('
                                                        UPDATE admin 
                                                        SET is_online = :is_online, last_modified = NOW()    
                                                        WHERE admin_id = :admin_id 
                                                        ');
		$update_handle->bindParam(':admin_id', $_update_params['admin_id'], \PDO::PARAM_INT);
		$update_handle->bindParam(':is_online', $_update_params['is_online'], \PDO::PARAM_STR);
		
		$_update_params['admin_id'] = $user->_data['admin_id'];
		

        // update session (save old login token)
        $_SESSION[SESSION_PREFIX.'PREV_LOGIN_TOKIN'] = $user->_data['is_online'];
        $user->_data['is_online'] = $_update_params['is_online'];




		

		
		if (!$update_handle->execute()) $user->e->triggerException('Error in class (' . __CLASS__ . '): [ ' . implode(', ', $update_handle->errorInfo()) . ' ] on line ' . __LINE__);
		
		
		
			
		// Return $USER (or NULL!!) ...
		return $user;
		
		
		
	}
        
        
        
        
        

	///// LOGOUT /////////////////

	public function logout ()
	{
		
		
		///////////// UPDATE LOGIN STATUS ///////////////////////////
		
		$_update_params = array();

		// Do prepared statement for 'UPDATE'...
		$update_handle = $this->dbh->prepare('
                                                        UPDATE admin 
                                                        SET is_online = 0  
                                                        WHERE admin_id = :instance_id 
                                                        ');
		$update_handle->bindParam(':instance_id', $_update_params['admin_id'], \PDO::PARAM_INT);
		
		
		$_update_params['admin_id'] = $this->_data['admin_id'];
		
		if (!$update_handle->execute()) $this->e->triggerException('Error in class (' . __CLASS__ . '): [ ' . implode(', ', $update_handle->errorInfo()) . ' ] on line ' . __LINE__);
		
		$this->deleteStoreKey();
        
        
        \Cataleya\System\Event::notify('admin/user/logged-out', $this, array ());
		return TRUE;
		
	}
	






        
	///// CONFIRM PASSWORD /////////////////

	public function confirmPassword ($_pass = FALSE)
	{
		if ($_pass === FALSE) return FALSE;
		
		// Validate Password
		return $this->bcrypt->verify($_pass, $this->_data['admin_pass']);
		
	}
        
        
        
        

        /*
         *
         * [ setPassword ] 
         *_____________________________________________________
         *
         *
         */

        public function setPassword($value = NULL)
        {
                if ($value===NULL || !is_string($value) || strlen($value) < 2) return FALSE;

                // Before we set new password, update password history
                $this->_data['prev_pass3'] = $this->_data['prev_pass2'];
                $this->_data['prev_pass2'] = $this->_data['prev_pass1'];
                $this->_data['prev_pass1'] = $this->_data['admin_pass'];
                
                // Save new password...
                $this->_data['admin_pass'] = $this->bcrypt->hash($value);
                array_push($this->_modified, 'admin_pass', 'prev_pass1', 'prev_pass2', 'prev_pass3');

                return $this;
        }

	


        



/*
 *
 * [GETTERS] and [SETTERS] 
 * __________________________________________________________________________________
 *
 *
 *
 */	
	

        
        
	/**
     * is
     *
     * ---------------------------------------
     *
     * Use like $_Role->is('SHOP_OWNER')
	 *
	 * @param string | \Cataleya\Admin\Role $_role_handle
	 * @return boolean
	 */
	public function is ($_role_handle = NULL) 
	{

        return $this->getRole()->is($_role_handle);
		
	}
	
	


	// getRole	
	public function getRole () 
	{
            if (empty($this->_role)) 
            {
                $this->_role = \Cataleya\Admin\Role::load($this->_data['role_id']);
            }
            
            return $this->_role;
		
	}
	
        
    // setPermission

	public function setRole (\Cataleya\Admin\Role $role) 
	{

            $this->_role = $role;
            $this->_data['role_id'] = $role->getID();
		
            return $this;		
		
	}

	


	
	
       
        
        
	 /*
	 *  [ delete ]
	 * ________________________________________________________________
	 * 
	 * 
	 *
	 *
	 */

	 
	public function delete () 
	{
            
                        if (!defined ('IS_ADMIN_FLAG') || !IS_ADMIN_FLAG || !defined ('CAN_DELETE_ADMIN_ACC') || !CAN_DELETE_ADMIN_ACC) die("Illegal Access");
            
                        
                        // //// START TRANSACTION /////
                        $this->dbh->beginTransaction();
                        
                        // Remove any store_keys (just in case)
                        // $this->deleteStoreKey();
                        
			// REMOVE ALERTS			
			$delete_handle1_param_instance_id = $this->_data['admin_id'];
			$delete_handle1 = $this->dbh->prepare('
                                                                DELETE FROM alerts 
                                                                WHERE admin_id = :instance_id
                                                                ');
			$delete_handle1->bindParam(':instance_id', $delete_handle1_param_instance_id, \PDO::PARAM_INT);

	
			if (!$delete_handle1->execute()) $this->e->triggerException('
											Error in class (' . __CLASS__ . '): [ ' . 
											implode(', ', $delete_handle1->errorInfo()) . 
											' ] on line ' . __LINE__);
				
		
			// DELETE PROFILE			
                        parent::delete();
											
			
                        
                        
                        // //// COMMIT TRANSACTION /////
                        $this->dbh->commit();
                        
                        
                        // Notify sambady anybady!!
                        // Hopefully there'll be anyone left to notify !!!
                        $_log_entry = $this->getName() . '\'s account has just been removed by ' . $_SESSION[SESSION_PREFIX.'NAME'];
                        
                        
                        \Cataleya\System::load()->updateAdminUserCount(-1);
                        \Cataleya\System\Event::notify('admin/user/deleted', NULL, array (
                            'scope'=>'admin/user/deleted', 
                            'count'=>1,
                            'logger_id'=>'admin.log', 
                            'blob'=>$_log_entry,
                            'fields' => $this->_data
                            ));

		
		// $this = NULL;
		return TRUE;
		
		
	}




	
	

        
        
        // Garbage Collection
        public static function garbageCollect () 
        {
                static $update_handle = NULL;
                
		// Do prepared statement for 'UPDATE'...
                
                if (empty($update_handle)) 
                {
                    $update_handle = \Cataleya\Helper\DBH::getInstance()->prepare('
                                                                        UPDATE admin SET is_online=0, last_modified=NOW()       
                                                                        WHERE last_modified < DATE_SUB( NOW(), INTERVAL 900 second )
                                                                        ');
                }
                
                if (!$update_handle->execute()) \Cataleya\Helper\ErrorHandler::getInstance()->triggerException('Error in class (' . __CLASS__ . '): [ ' . implode(', ', $update_handle->errorInfo()) . ' ] on line ' . __LINE__);
		
                
                return TRUE;
        }
        
        
        
        
        
	/*
	 *
	 *  [ getStores ]
	 * ________________________________________________________________
	 * 
	 * 
	 *
	 *
	 *
	 */
        
        
        
        public function getStores () 
        {
            
        }







        /*
	 *
	 *  [ setDisplayPicture ]
	 * ________________________________________________________________
	 * 
	 * 
	 *
	 *
	 *
	 */
	 

	public function setDisplayPicture (\Cataleya\Asset\Image $image)
	{
		if (!in_array('image_id', $this->_modified)) $this->_modified[] = 'image_id';
                
                $this->getDisplayPicture();
		$this->_data['image_id'] = $image->getID();

                // Remove old image
		if ($this->_display_picture->getID() != 0) 
                {
                    $this->saveData();
                    $this->_display_picture->destroy();
                }		
                
		$this->_display_picture = $image; 
                
                
                
 
		
		return $this;              
                
		
	}





	/*
	 *
	 *  [ unsetDisplayPicture ]
	 * ________________________________________________________________
	 * 
	 * 
	 *
	 *
	 *
	 */
	 

	public function unsetDisplayPicture ()
	{
		$this->_data['image_id'] = NULL;
		
		
		if (!in_array('image_id', $this->_modified)) $this->_modified[] = 'image_id';
                $this->saveData();
                $this->_display_picture->destroy();
		$this->_display_picture = NULL;
	}





	/*
	 *
	 *  [ getDisplayPicture ]
	 * ________________________________________________________________
	 * 
	 * Returns image object
	 *
	 *
	 *
	 */
	 

	public function getDisplayPicture ()
	{
		if (empty($this->_display_picture) )
		{
			$this->_display_picture = \Cataleya\Asset\Image::load($this->_data['image_id']);
		}
		
		return $this->_display_picture;
		
	}




        /*
         *
         * [ getImageId ] 
         *_____________________________________________________
         *
         *
         */

        public function getImageId()
        {
                return $this->_data['image_id'];
        }











        // Firstname
	
	public function getFirstname () 
	{
		return $this->_data['firstname'];		
		
	}
	


	public function setFirstname ($new_val = NULL) 
	{		

		if ($new_val === NULL) 
		{
			return false;
		} else {
			$this->_data['firstname'] = $new_val;
			 if (!in_array('firstname', $this->_modified)) $this->_modified[] = 'firstname';
		}

		return $this;		
		
	}

	
	
	
	// Lastname

	public function getLastname () 
	{
		return $this->_data['lastname'];		
		
	}
	


	public function setLastname ($new_val = NULL) 
	{
		if ($new_val === NULL) 
		{
			return false;
		} else {
			$this->_data['lastname'] = $new_val;
			 if (!in_array('lastname', $this->_modified)) $this->_modified[] = 'lastname';
		}
		
		return $this;		
		
	}


	
	// Full name...

	public function getName () 
	{
		return ($this->_data['firstname'] . ' ' . $this->_data['lastname']);
		
	}
	



	
	// Telephone

	public function getPhone () 
	{
		return $this->_data['admin_phone'];		
		
	}
        
        
	// Telephone

	public function getTelephone () 
	{
		return $this->_data['admin_phone'];		
		
	}
	


	public function setPhone ($new_val = NULL) 
	{
		if ($new_val === NULL) 
		{
			return false;
		} else {
			$this->_data['admin_phone'] = $new_val;
			 if (!in_array('admin_phone', $this->_modified)) $this->_modified[] = 'admin_phone';
		}
		
		return $this;		
		
	}


        
        
	// Admin ID

	public function getID () 
	{
		return $this->_data['admin_id'];		
		
	}
	


	// Admin ID

	public function getAdminID () 
	{
		return $this->_data['admin_id'];		
		
	}
	





	// Login Token

	public function getLoginToken () 
	{
            $token = 0;
            
            if (strlen(strval($this->_data['is_online'])) > 1)
            {
                list($token, $timestamp) = explode(':', $this->_data['is_online']);

                if ( (int)$timestamp < CURRENT_DT )
                {
                        $this->_data['is_online'] = $token = 0;
                        $this->_modified[] = 'is_online';
                }
            }
                
            return $token;		
		
	}
	






	// Admin Email

	public function getEmail () 
	{
		return $this->_data['admin_email'];		
		
	}
        
        
	// Admin Email

	public function getEmailAddress () 
	{
		return $this->_data['admin_email'];		
		
	}
	


	public function setEmail ($new_val = NULL) 
	{
		if ($new_val === NULL) 
		{
			return false;
		} else {
			$this->_data['admin_email'] = $new_val;
			 if (!in_array('admin_email', $this->_modified)) $this->_modified[] = 'admin_email';
		}
		
		return $this;		
		
	}
        
        
        
        
        /*
         *
         * [ isOnline ] 
         *_____________________________________________________
         *
         *
         */

        public function isOnline()
        {
                return (strval($this->getLoginToken()) === '0') ? FALSE : TRUE;
        }


        
        
        

        /*
         *
         * [ isActive ] 
         *_____________________________________________________
         *
         *
         */

        public function isActive()
        {
                return ((int)$this->_data['active'] === 1) ? TRUE : FALSE;
        }


        
            

        /*
         * 
         * [ enable ]
         * ______________________________________________________
         * 
         * 
         */


        public function enable () 
        {

            if ((int)$this->_data['active'] === 0) 
            {
                $this->_data['active'] = 1;
                $this->_modified[] = 'active';
            }

            \Cataleya\System\Event::notify('admin/user/enabled', $this, array ());
            return $this;
        }





        /*
         * 
         * [ disable ]
         * ______________________________________________________
         * 
         * 
         */


        public function disable () 
        {
            if ((int)$this->_data['active'] === 1) 
            {
                $this->_data['active'] = 0;
                $this->_modified[] = 'active';
            }

            \Cataleya\System\Event::notify('admin/user/disabled', $this, array ());
            return $this;
        }
        
        
        

	// Last Login

	public function getLastLogin () 
	{
		return date('D, d M Y H:i', strtotime($this->_data['last_login_date']));		
		
	}
        
        
        
 	// Last Login Geo Location

	public function getLastLoginGeoLoc () 
	{
		return $this->_data['last_login_geo_loc'];		
		
	}
        
        
        
 	// Last Failed Login

	public function getLastFailedLogin () 
	{
		return date('D, d M Y H:i', strtotime($this->_data['last_failed_attempt']));		
		
	}
        
        
 	// Last Failed Login Geo Location

	public function getLastFailedLoginGeoLoc () 
	{
		return $this->_data['last_failed_geo_loc'];		
		
	}
        
        
        
 	// Date Created

	public function getDateCreated () 
	{

		return date('D, d M Y H:i', strtotime($this->_data['date_created']));		
		
	}
        
        
        
 	// Failed Logins

	public function getFailedLogins () 
	{
		return $this->_data['failed_logins'];		
		
	}
        
        
        
        
        
        
        
        
        /*
         * 
         * public: [ makeStoreKey ] 
         * _______________________________________________
         * 
         * 
         */
        
        public function makeStoreKey ($_force_regen = FALSE) 
        {
                
                if (!empty($this->_store_key) && $_force_regen !== TRUE) return $this->_store_key;
                
                
                static 
                        $insert_handle, 
                        $insert_handle_params_id, 
                        $insert_handle_params_key;
                
                if (empty($insert_handle)) 
                {
                    $insert_handle = $this->dbh->prepare('
                        REPLACE INTO store_keychain (admin_id, key_hash, date_added) 
                        VALUES (:admin_id, :key_hash, NOW())  
                     ');
                    
                      $insert_handle->bindParam(':admin_id', $insert_handle_params_id, \PDO::PARAM_INT);
                      $insert_handle->bindParam(':key_hash', $insert_handle_params_key, \PDO::PARAM_STR);
                    
                }
                
                $insert_handle_params_id = $this->getAdminID();
                $insert_handle_params_key = \Cataleya\Helper::digest($this->getName());
                
		if (!$insert_handle->execute()) $this->e->triggerException('
										Error in class (' . __CLASS__ . '): [ ' . 
										implode(', ', $insert_handle->errorInfo()) . 
										' ] on line ' . __LINE__);
                
                $this->_store_key = $insert_handle_params_key;
                return $this->_store_key;
                
        }
        
        
        
        
        
        /*
         * 
         * private: [ deleteStoreKey ]
         * ___________________________________________________________________________________
         *  
         * This is meant to be a private method but will cause problems in [ ::load() ] 
         * since the '$this' keyword will not available yet. 
         * 
         */
        
        public function deleteStoreKey () 
        {
              
			
                static 
                        $delete_handle, 
                        $delete_handle_param_instance_id;
                
                

                if (empty($insert_handle)) 
                {
                    $delete_handle = $this->dbh->prepare('
                                                            DELETE FROM store_keychain 
                                                            WHERE admin_id = :instance_id
                                                            ');
                    $delete_handle->bindParam(':instance_id', $delete_handle_param_instance_id, \PDO::PARAM_INT);
                }
                
                $delete_handle_param_instance_id = $this->getAdminID();
                if (!$delete_handle->execute()) $this->e->triggerException('
                                                                                Error in class (' . __CLASS__ . '): [ ' . 
                                                                                implode(', ', $delete_handle->errorInfo()) . 
                                                                                ' ] on line ' . __LINE__);
		
                $this->_store_key = NULL;
                return $this;
                
        }
        
        
        
        
        
        /*
         * 
         * public static: [ validateStoreKey ]
         * _____________________________________________________________________
         * 
         * Returns true if store_key is valid
         * 
         * 
         */
        
        
        public static function validateStoreKey ($_key = NULL) 
        {
            if (!is_string($_key) || filter_var($_key, FILTER_VALIDATE_REGEXP, array('options'=>array('regexp' => '/^[A-Za-z0-9]{10,200}$/'))) === FALSE) return FALSE;
            
		static $select_handle, $param_key;
		
		
		// Do prepared statement...
		if (empty($select_handle)) {
		$select_handle = \Cataleya\Helper\DBH::getInstance()->prepare('
                                                        SELECT admin_id FROM store_keychain 
                                                        WHERE key_hash = :key 
                                                        LIMIT 1
                                                        ');
		$select_handle->bindParam(':key', $param_key, \PDO::PARAM_STR);
		}
		
		$param_key = $_key;
		
		// Execute...
		if (!$select_handle->execute()) \Cataleya\Helper\ErrorHandler::getInstance()->triggerError('Error in class (' . __CLASS__ . '): [ ' . implode(', ', $select_handle->errorInfo()) . ' ] on line ' . __LINE__);
                
                // Any matches ?
                $_row = $select_handle->fetch(\PDO::FETCH_ASSOC);
                if (empty($_row)) return FALSE;
                
                
                // Load user 
                $_user = self::load($_row['admin_id']);
                if ($_user === NULL) \Cataleya\Helper\ErrorHandler::getInstance()->triggerError('Error in class (' . __CLASS__ . '): [ user not found ] on line ' . __LINE__);
		
                // Check online status
                if ($_user->isOnline()) 
                {
                    // Online
                    return TRUE;
                }
                
                else {
                    
                    // offline
                    $_user->deleteStoreKey();
                    return FALSE;
                }
                
        }
        



	
}






