<?php


namespace Cataleya\Admin;





use \Cataleya\Helper\DBH;
use \Cataleya\Error;



/*
 *
 * [ Cataleya \ Admin \ Role ]
 * ________________________________________________________________________
 *
 * (c) 2012 Fancy Paper Planes
 *
 *
*/





// if (!defined ('IS_ADMIN_FLAG') || !IS_ADMIN_FLAG) die("Illegal Access");





class Role extends \Cataleya\HybridData {
	
			
			
	protected $dbh, $e, $bcrypt;
	protected $_data = array();
    protected $_description = NULL;
	protected $_permissions = array(); 
    protected $_permissions_modified = FALSE; 
	protected $_modified = array();
        
    // To be used by [\Cataleya\Collection] to iterate privileges assigned to this role...
    protected $_collection = array ();
    protected $_position = 0;
    
    
    const TABLE = 'admin_roles';
    const PK = 'role_id';
	
	public function __construct($role_id, $_load_with_handle = false) 
	{

        $_load_with_handle = ($_load_with_handle === true);

        if ($_load_with_handle) $role_id = \Cataleya\Helper\Validator::param($role_id, 'match:/^[a-zA-Z0-9_]+$/');
        else $role_id = filter_var($role_id, FILTER_VALIDATE_INT);		

		if ($role_id === false) throw new Error ('Invalid argument: role_id should be an integer or a string.');
		
		// Get encryption class...
		$this->bcrypt = \Cataleya\Helper\Bcrypt::getInstance();
		
		
		// ATTEMPT TO LOAD DATA
		
		// Do prepared statement...
        $agent_select = DBH::getInstance()->prepare('
                                SELECT * FROM admin_roles 
                                WHERE ' . ( $_load_with_handle ? 'handle' : 'role_id' ) . ' = :role_id 
                                LIMIT 1
                                ');
        $agent_select->bindParam(':role_id', $param_role_id, 
                                            ( $_load_with_handle 
                                                ? \PDO::PARAM_STR 
                                                : \PDO::PARAM_INT 
                                            )
                                );
        
        $param_role_id = $role_id;
        
        // Execute...
        if (!$agent_select->execute()) throw new Error ('DB Error: ' . implode(', ', $agent_select->errorInfo()));



		// Check for results...
		if ( ($this->_data = $agent_select->fetch(\PDO::FETCH_ASSOC)) === false ) 
		{
			$this->_data = array();
            return null;

		} else {
                    // Load user permissions...
                    $this->loadPermissions();
                    
                    // Load description...
                    $this->_description = \Cataleya\Asset\Description::load($this->_data['description_id']);		

                }
                
                
                parent::__construct();
		
		
			
	}
	
	
	function __destruct () 
	{
            parent::__destruct();
            $this->savePermissions();		
	}
	



        public function current()
        {
            return \Cataleya\Admin\Privilege::load($this->_collection[$this->_position]);

        }


      
    
    


	/**
	 * load
	 *
	 * @param int|string $_role_id
	 * @param bool $_load_with_handle
	 * @return void
	 */
	static public function load ($_role_id = null, $_load_with_handle = false) 
	{

		// get role
		$role = new static ($_role_id, $_load_with_handle);

        /*
		if (empty($role->_data))
		{
			unset($role);
			return NULL;
        }
        */        
		
		
		////////////////// RETURN USER ROLE ////////////////////
		
		return $role;
                
     }



        
        


	/*
	 *
	 *  [ create ]
	 * ________________________________________________________________
	 * 
	 * 
	 *
	 *
	 *
	 */
	 
	static public function create ($handle, $description_title = 'Untitled Admin Role', $description_text = 'No Description', $language_code = 'EN') 
	{

        $_expected = [
                'handle' => 'match:/^[A-Z]+(_[A-Z]+){0,10}$/', 
                'title' => 'plain_text', 
                'description' => 'html', 
                'language_code' => 'iso2'
        ];

        $_params = \Cataleya\Helper\Validator::params(
            $_expected, 

            // Supplied params (arguments)...
            [
                'handle' => $handle, 
                'title' => $description_title, 
                'description' => $description_text, 
                'language_code' => $language_code
            ]);

        foreach ($_params as $k=>$v) if ($v === false) throw new \Cataleya\Error('Invalid argument: ' . $k . '. Expected: ' . $_expected[$k]);

		
		// Get database handle...
        $dbh = \Cataleya\Helper\DBH::getInstance();
		

		/*
		 * Handle description Attribute 
		 *
		 */
		 $description = \Cataleya\Asset\Description::create($_params['title'], $_params['description'], $_params['language_code']);
		 
		                
		// construct
		static 
				$role_insert, 
                $role_insert_param_description_id, 
                $role_insert_param_handle;
		
		if (empty($role_insert))
		{
			$role_insert = $dbh->prepare('
                                        INSERT INTO admin_roles  
                                        (handle, description_id, date_created, last_modified) 
                                        VALUES (:handle, :description_id, NOW(), NOW())
                                        ');
															
			$role_insert->bindParam(':description_id', $role_insert_param_description_id, \PDO::PARAM_INT);
			$role_insert->bindParam(':handle', $role_insert_param_handle, \PDO::PARAM_STR);
		}
		
		$role_insert_param_description_id = $description->getID();
        $role_insert_param_handle = $_params['handle'];
		
		if (!$role_insert->execute()) throw new \Cataleya\Error('DB Error: '.implode(', ', $role_insert->errorInfo()) . gettype($_params['handle'])); 
										
		// AUTOLOAD NEW USER AND RETURN IT
		return self::load($dbh->lastInsertId());
                
		
	}





        
        




        
        
        /*
         * 
         *  [ loadPermissions ]
         * _________________________________________________________________
         * 
         * 
         * 
         */
        
        
        private function loadPermissions ()
        {
		
		static 
                        $permissions_select, 
                        $param_role_id;
		
		
		// Do prepared statement...
		if (empty($permissions_select)) {
		$permissions_select = DBH::getInstance()->prepare('
                                                            SELECT * FROM permissions 
                                                            WHERE role_id = :role_id 
                                                            ');
		$permissions_select->bindParam(':role_id', $param_role_id, \PDO::PARAM_INT);
		}
		
		$param_role_id = $this->_data['role_id'];
		
		// Execute...
		if (!$permissions_select->execute()) throw new Error('DB Error: '.implode(', ',$permissions_select->errorInfo()));
		
		// Check for results...
		while ($row = $permissions_select->fetch(\PDO::FETCH_ASSOC)) 
		{
			$this->_permissions[$row['privilege_id']] = $row;
                        if ((int)$row['permission'] === 1) $this->_collection[] = $row['privilege_id'];
		}            
            
        }
        
        
        
        
        




/*
 *
 * [GETTERS] and [SETTERS] 
 * __________________________________________________________________________________
 *
 *
 *
 */	
	

        
        




	// ID

	public function getID () 
	{
		return $this->_data['role_id'];		
		
	}
        
        
         // ID

	public function getRoleId () 
	{
		return $this->_data['role_id'];		
		
	}
	



        

	/*
	 *
	 *  [ getDescription ]
	 * ________________________________________________________________
	 * 
	 * Will return product description as object
	 *
	 *
	 */
	 
	public function getDescription () 
	{
		
		return $this->_description;
		
	}

        
        

	
	/**
     * is
     *
     * ---------------------------------------
     *
     * Use like $_Role->is('SHOP_OWNER')
	 *
	 * @param string | \Cataleya\Admin\Role $_role_handle
	 * @return boolean
	 */
	public function is ($_role_handle = NULL) 
	{

        if (
            !is_string($_role_handle) && 
            (!is_object($_role_handle) || $_role_handle instanceof \Cataleya\Admin\Role)
        ) throw new \Cataleya\Error ('Argument must be a handle like "SHOP_ASSISTANT" or an instance of \Cataleya\Admin\Role'); 
            
        return is_string($_role_handle) 
            ? ($this->getHandle() === trim(strtoupper($_role_handle))) 
            : $_role_handle->is($this->getHandle());
		
	}
	




	// getHandle
	public function getHandle () 
	{
            
            return $this->_data['handle'];
		
	}


        
	
	// getPermission
	public function hasPrivilege ($privilege_id = NULL) 
	{
            if ($this->is('ROOT')) return true;
            
            if (is_object($privilege_id)) $privilege_id = $privilege_id->getID(); // makes it possible to pass an [_Admin_Privilege] class directly...

            if ($privilege_id === NULL || !is_string($privilege_id) || empty($privilege_id) || !isset($this->_permissions[$privilege_id])) return FALSE;
            
            return ((int)$this->_permissions[$privilege_id]['permission'] === 1);
		
	}
	
        
    // givePrivilege
	public function givePrivilege ($privilege_id = NULL) 
	{
		if ($privilege_id === NULL || !is_string($privilege_id) || empty($privilege_id)) return false;
                
                else $this->setPrivilege($privilege_id, 1);
		
		return true;		
		
	}
        
        
        // givePrivileges

	public function givePrivileges ($privileges = array ()) 
	{
		if (!is_array($privileges) || empty($privileges)) return false;
                
                foreach ($privileges as $privilege_id) $this->setPrivilege($privilege_id, 1);
		
		return true;		
		
	}
        
        
        
        // removePrivileges

	public function removePrivileges ($privileges = array ()) 
	{
		if (!is_array($privileges) || empty($privileges)) return false;
                
                foreach ($privileges as $privilege_id) $this->setPrivilege($privilege_id, 0);
		
		return true;		
		
	}
        
        
        
        // removePrivilege

	public function removePrivilege ($privilege_id = NULL) 
	{
		if ($privilege_id === NULL || !is_string($privilege_id) || empty($privilege_id)) return false;
                
                else $this->setPrivilege($privilege_id, 0);
		
		return true;		
		
	}
        
        
        
        
        
        // setPrivilege

	private function setPrivilege ($privilege_id = NULL, $value = NULL) 
	{
		if (
                        $privilege_id === NULL 
                        || !is_string($privilege_id) 
                        || empty($privilege_id) 
                        || $value === NULL 
                        || (int)$value > 1
                        ) 
		{
                       return false;
                        
		} else {
                    
                        $privilege_id = strtoupper(trim($privilege_id));
                        $value = (int)$value;
                        
                        if (isset($this->_permissions[$privilege_id])) 
                        {
                            if ((int)$this->_permissions[$privilege_id]['permission'] === (int)$value) return TRUE;
                            else $this->_permissions[$privilege_id]['permission'] = (int)$value;
                            
                        } else {
                            
                            if ((int)$value === 0) return TRUE;
                            
                            // Initialize...
                            if (\Cataleya\Admin\Privilege::load($privilege_id) === NULL) return FALSE;
                            
                            $this->_permissions[$privilege_id] = array (
                                                                    'privilege_id' => $privilege_id, 
                                                                    'role_id' => $this->_data['role_id'], 
                                                                    'permission' => (int)$value
                                                                  );
                        }
			
			$this->_permissions_modified = TRUE;
		}
		
		return true;		
		
	}
        
          

	


	/*
	 *
	 *  [ savePermissions ]
	 * ________________________________________________________________
	 * 
	 * 
	 *
	 *
	 *
	 */
	 
	private function savePermissions () 
	{
		
            if ($this->_permissions_modified !== TRUE) return;

		
		// construct
		
		static 
				$permission_insert, 
                                $permission_insert_param_role_id, 
				$permission_insert_param_privilege_id, 
				$permission_insert_param_permission;
		
		if (empty($permission_insert))
		{
			$permission_insert = DBH::getInstance()->prepare('
                                                                    INSERT INTO permissions (role_id, privilege_id, permission, last_modified) 
                                                                    VALUES(:role_id, :privilege_id, :permission, NOW()) 
                                                                    ON DUPLICATE KEY UPDATE permission = :permission, last_modified = NOW()
                                                                    ');
			$permission_insert->bindParam(':role_id', $permission_insert_param_role_id, \PDO::PARAM_INT);
			$permission_insert->bindParam(':privilege_id', $permission_insert_param_privilege_id, \PDO::PARAM_STR);
			$permission_insert->bindParam(':permission', $permission_insert_param_permission, \PDO::PARAM_INT);
		}
		
                

                foreach ($this->_permissions as $perm)
                {
                    $permission_insert_param_role_id = $this->_data['role_id'];
                    $permission_insert_param_privilege_id = $perm['privilege_id'];
                    $permission_insert_param_permission = $perm['permission'];
                    
                    if (!$permission_insert->execute()) throw new Error ('DB Error: '.implode(', ', $permission_insert->errorInfo()));
                                                                                             
                    
                }


		
		return TRUE;
		
		
	}



	
	

     
        
        
        



               


	/*
	 *
	 *  [ delete ]
	 * ________________________________________________________________
	 * 
	 * 
	 *
	 *
	 *
	 */	


	public function delete () 
	{
		
                // DELETE PERMISSIONS...
 		$perms_delete = DBH::getInstance()->prepare('
                                                    DELETE FROM permissions      
                                                    WHERE role_id = :role_id 
                                                    ');
		$perms_delete->bindParam(':role_id', $perms_delete_params_role_id, \PDO::PARAM_INT);
                $perms_delete_params_role_id = $this->getID();
		
		if (!$perms_delete->execute()) throw new Error ('DB Error: ' . implode(', ', $perms_delete->errorInfo()));
		
   

		// THEN DELETE ROLE....
                parent::delete();
                
                
                
                }
	
	

        

	
}




