<?php


namespace Cataleya\Admin\Notification;



/*

CLASS

*/



class Alert    
{
	
	
	private $_data = array();
	private $_modified = array();
        
	private $dbh, $e;
	

	


	/*
	 *
	 * [ __construct ]
	 * ________________________________________________________________
	 *
	 *
	 *
	 */
	 
	 
	private function __construct ()
	{
		
		// Get database handle...
                $this->dbh = \Cataleya\Helper\DBH::getInstance();
		
		// Get error handler
		$this->e = \Cataleya\Helper\ErrorHandler::getInstance();
		
	}
	




	/*
	 *
	 * [ __destruct ]
	 * ________________________________________________________________
	 *
	 *
	 *
	 */

        public function __destruct()
        {
                    $this->saveData();
        }
	




	/*
	 *
	 * [ load ]
	 * ________________________________________________________________
	 *
	 *
	 *
	 */
	 
	 
	public static function load ($id = 0)
	{
		
                if ($id == 0 || filter_var($id, FILTER_VALIDATE_INT) == FALSE) return NULL;
		
		$instance = new \Cataleya\Admin\Notification\Alert ();
		
		// LOAD DATA
		static $select_handle, $param_instance_id;
		
			
		if (empty($select_handle)) {
			// PREPARE SELECT STATEMENT...
			$select_handle = $instance->dbh->prepare('SELECT * FROM alerts WHERE alert_id = :instance_id LIMIT 1');
			$select_handle->bindParam(':instance_id', $param_instance_id, \PDO::PARAM_INT);
		}
		
		
		$param_instance_id = $id;
		
		if (!$select_handle->execute()) $instance->e->triggerException('
												Error in class (' . __CLASS__ . '): [ ' . 
												implode(', ', $select_handle->errorInfo()) . 
												' ] on line ' . __LINE__);
		
		$instance->_data = $select_handle->fetch(\PDO::FETCH_ASSOC);
                
		if (empty($instance->_data))
		{
			unset($instance);
			return NULL;
		}



		return $instance;	
		
		
	}



	/*
	 *
	 *  [ create ]
	 * ________________________________________________________________
	 * 
	 * 
	 *
	 *
	 */

	 
	static public function create (\Cataleya\Admin\Notification $_notification_profile, \Cataleya\Admin\User $_user, array $_options = array()) 
	{    
		 

		
		// Get database handle...
                $dbh = \Cataleya\Helper\DBH::getInstance();
		
		// Get error handler
		$e = \Cataleya\Helper\ErrorHandler::getInstance();
		
		// construct
		static 
				$insert_handle,  
                                $insert_handle_param_notification_id, 
                                $insert_handle_param_admin_id, 
                                $insert_handle_param_email, 
                                $insert_handle_param_dashboard;
		
		if (empty($insert_handle))
		{
			$insert_handle = $dbh->prepare('
                                                        INSERT INTO alerts 
                                                        (notification_id, admin_id, to_email, to_dashboard) 
                                                        VALUES (:notification_id, :admin_id, :to_email, :to_dashboard)
                                                        ');
															
			$insert_handle->bindParam(':notification_id', $insert_handle_param_notification_id, \PDO::PARAM_INT);
                        $insert_handle->bindParam(':admin_id', $insert_handle_param_admin_id, \PDO::PARAM_INT);
                        $insert_handle->bindParam(':to_email', $insert_handle_param_email, \PDO::PARAM_BOOL);
                        $insert_handle->bindParam(':to_dashboard', $insert_handle_param_dashboard, \PDO::PARAM_BOOL);
		}
		
                $insert_handle_param_notification_id = $_notification_profile->getID();
                $insert_handle_param_admin_id = $_user->getAdminID();
                $insert_handle_param_email = (isset($_options['to_email']) && is_bool($_options['to_email'])) ? $_options['to_email'] : FALSE;
                $insert_handle_param_dashboard = (isset($_options['to_dashboard']) && is_bool($_options['to_dashboard'])) ? $_options['to_dashboard'] : FALSE;
		
		if (!$insert_handle->execute()) $e->triggerException('
										Error in class (' . __CLASS__ . '): [ ' . 
										implode(', ', $insert_handle->errorInfo()) . 
										' ] on line ' . __LINE__);
										
		
		// AUTOLOAD NEW PRODUCT AND RETURN IT
		$instance = self::load($dbh->lastInsertId());
		
		return $instance;
		
		
	}


	 
	 /*
	 *  [ delete ]
	 * ________________________________________________________________
	 * 
	 * 
	 *
	 *
	 */

	 
	public function delete () 
	{
		
			// DELETE 
			$delete_handle = $this->dbh->prepare('
													DELETE FROM alerts 
													WHERE alert_id = :instance_id
													');
			$delete_handle->bindParam(':instance_id', $delete_handle_param_instance_id, \PDO::PARAM_INT);
			
			$delete_handle_param_instance_id = $this->_data['alert'];
	
			if (!$delete_handle->execute()) $this->e->triggerException('
											Error in class (' . __CLASS__ . '): [ ' . 
											implode(', ', $delete_handle->errorInfo()) . 
											' ] on line ' . __LINE__);
											
			

		
		// $this = NULL;
		return TRUE;
		
		
	}





        /*
         *
         * [ getID ] 
         *_____________________________________________________
         *
         *
         */

        public function getID()
        {
                return $this->_data['alert_id'];
        }




	
	
	public function saveData () 
	{
		

		if (empty($this->_modified)) return;
                $this->_modified = array_unique($this->_modified);
                
		$_update_params = array();
		$key_val_pairs = array();
		
		foreach ($this->_modified as $key) $key_val_pairs[$key] = $key . ' = :' . $key;
		
	
		// Do prepared statement for 'INSERT'...
		$update_handle = $this->dbh->prepare('
                                                        UPDATE alerts 
                                                        SET ' . implode (', ', $key_val_pairs) . '    
                                                        WHERE alert_id = :instance_id 
                                                        ');
                
		$update_handle->bindParam(':instance_id', $_update_params['alert_id'], \PDO::PARAM_INT);
                $_update_params['alert_id'] = $this->_data['alert_id'];
                
		foreach ($this->_modified as $key) {
                    
                        $_update_params[$key] = $this->_data[$key];                        
			$update_handle->bindParam(':'.$key, $_update_params[$key], \Cataleya\Helper\DBH::getTypeConst($_update_params[$key]));

		}
		
		

		
		if (!$update_handle->execute()) $this->e->triggerException('Error in class (' . __CLASS__ . '): [ ' . implode(', ', $update_handle->errorInfo()) . ' ] on line ' . __LINE__);
		
	}
	
	
       
 
 
        
        
        
        

        /*
         *
         * [ getAlertId ] 
         *_____________________________________________________
         *
         *
         */

        public function getAlertId()
        {
                return $this->_data['alert_id'];
        }






        /*
         *
         * [ getAdminId ] 
         *_____________________________________________________
         *
         *
         */

        public function getAdminId()
        {
                return $this->_data['admin_id'];
        }


        
        /*
         *
         * [ getRecipient ] 
         *_____________________________________________________
         *
         *
         */

        public function getRecipient()
        {
                return \Cataleya\Admin\User::load($this->_data['admin_id']);
        }






        /*
         *
         * [ getNotificationId ] 
         *_____________________________________________________
         *
         *
         */

        public function getNotificationId()
        {
                return $this->_data['notification_id'];
        }







        /*
         *
         * [ emailEnabled ] 
         *_____________________________________________________
         *
         *
         */

        public function emailEnabled()
        {
                return ((int)$this->_data['to_email'] === 1) ? TRUE : FALSE;
        }



        /*
         *
         * [ setToEmail ] 
         *_____________________________________________________
         *
         *
         */

        private function setToEmail($value = NULL)
        {
                $value = filter_var($value, FILTER_VALIDATE_BOOLEAN, FILTER_NULL_ON_FAILURE);

                if ($value===NULL) $this->e->triggerException('Error in class (' . __CLASS__ . '): [ invalid argument for method: ' . __FUNCTION__ . ' ] on line ' . __LINE__);

                $this->_data['to_email'] = $value;
                $this->_modified[] = 'to_email';

                return $this;
        }
        
        
        
        /*
         *
         * [ enableEmail ] 
         *_____________________________________________________
         *
         *
         */

        public function enableEmail()
        {
                $this->setToEmail(1);
                return $this;
        }
        
        
        /*
         *
         * [ disableEmail ] 
         *_____________________________________________________
         *
         *
         */

        public function disableEmail()
        {
                $this->setToEmail(0);
                return $this;
        }


        
        
        
        



        /*
         *
         * [ getToDashboard ] 
         *_____________________________________________________
         *
         *
         */

        public function dashboardEnabled()
        {
                return ((int)$this->_data['to_dashboard'] === 1) ? TRUE : FALSE;
        }
        
        



        /*
         *
         * [ setToDashboard ] 
         *_____________________________________________________
         *
         *
         */

        private function setToDashboard($value = NULL)
        {
                $value = filter_var($value, FILTER_VALIDATE_BOOLEAN, FILTER_NULL_ON_FAILURE);

                if ($value===NULL) $this->e->triggerException('Error in class (' . __CLASS__ . '): [ invalid argument for method: ' . __FUNCTION__ . ' ] on line ' . __LINE__);

                $this->_data['to_dashboard'] = $value;
                $this->_modified[] = 'to_dashboard';

                return $this;
        }
        
        
        
        /*
         *
         * [ enableDashboard ] 
         *_____________________________________________________
         *
         *
         */

        public function enableDashboard()
        {
                $this->setToDashboard(1);
                return $this;
        }
        
        
        /*
         *
         * [ disableDashboard ] 
         *_____________________________________________________
         *
         *
         */

        public function disableDashboard()
        {
                $this->setToDashboard(0);
                return $this;
        }




        /*
         *
         * [ isDisplayed ] 
         *_____________________________________________________
         *
         *
         */

        public function isDisplayed()
        {
                return ((int)$this->_data['is_displayed'] === 1);
        }



        /*
         *
         * [ setDisplayed ] 
         *_____________________________________________________
         *
         *
         */

        private function setDisplayed($value = NULL)
        {
                $value = filter_var($value, FILTER_VALIDATE_BOOLEAN, FILTER_NULL_ON_FAILURE);

                if ($value===NULL) $this->e->triggerException('Error in class (' . __CLASS__ . '): [ invalid argument for method: ' . __FUNCTION__ . ' ] on line ' . __LINE__);

                $this->_data['is_displayed'] = $value;
                $this->_modified[] = 'is_displayed';

                return $this;
        }
        
        
        
        /*
         *
         * [ markDisplayed ] 
         *_____________________________________________________
         *
         *
         */

        public function markDisplayed()
        {
                $this->setDisplayed(TRUE);
                return $this;
        }
        
        
        
        /*
         *
         * [ markAsNotDisplayed ] 
         *_____________________________________________________
         *
         *
         */

        public function markAsNotDisplayed()
        {
                $this->setDisplayed(FALSE);
                return $this;
        }
        
        
        
        



        /*
         *
         * [ isRead ] 
         *_____________________________________________________
         *
         *
         */

        public function isRead()
        {
                return ((int)$this->_data['is_read'] === 1);
        }



        /*
         *
         * [ setRead ] 
         *_____________________________________________________
         *
         *
         */

        public function setRead($value = NULL)
        {
                $value = filter_var($value, FILTER_VALIDATE_BOOLEAN, FILTER_NULL_ON_FAILURE);

                if ($value===NULL) $this->e->triggerException('Error in class (' . __CLASS__ . '): [ invalid argument for method: ' . __FUNCTION__ . ' ] on line ' . __LINE__);

                $this->_data['is_read'] = $value;
                $this->_modified[] = 'is_read';

                return $this;
        }
        
        
        
        
        /*
         *
         * [ markRead ] 
         *_____________________________________________________
         *
         *
         */

        public function markRead()
        {
                $this->setRead(TRUE);
                return $this;
        }
        
        
        
        /*
         *
         * [ markUnread ] 
         *_____________________________________________________
         *
         *
         */

        public function markUnread()
        {
                $this->setRead(FALSE);
                return $this;
        }
        
        



        /*
         *
         * [ getCount ] 
         *_____________________________________________________
         *
         *
         */

        public function getCount()
        {
                return $this->_data['tally'];
        }



        /*
         *
         * [ setCount ] 
         *_____________________________________________________
         *
         *
         */

        public function setCount($value = NULL)
        {
                $value = filter_var($value, FILTER_VALIDATE_INT, FILTER_NULL_ON_FAILURE);

                if ($value===NULL) $this->e->triggerException('Error in class (' . __CLASS__ . '): [ invalid argument for method: ' . __FUNCTION__ . ' ] on line ' . __LINE__);

                $this->_data['tally'] = $value;
                $this->_modified[] = 'tally';

                return $this;
        }




        
        
        



        





}
	
	
	
	
