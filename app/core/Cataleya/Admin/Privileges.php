<?php


namespace Cataleya\Admin;





/**
 * Class \Cataleya\Admin\Privileges
 *
 * @author Fancy Paper Planes <gboyega@fancypaperplanes.com>
 */



// if (!defined ('IS_ADMIN_FLAG') || !IS_ADMIN_FLAG) die("Illegal Access");




class Privileges extends \Cataleya\Collection {
    
    // To be used by [\Cataleya\Collection]
    protected $_collection = array ();
    protected $_position = 0;


    public function __construct() {
       
        // Get database handle...
        $this->dbh = \Cataleya\Helper\DBH::getInstance();
		
        // Get error handler
        $this->e = \Cataleya\Helper\ErrorHandler::getInstance();
        

        // LOAD: PRODUCTS
        static 
                $select_handle, 
                $param_offset, 
                $param_limit;


        if (!isset($select_handle)) {
                // PREPARE SELECT STATEMENT...
                $select_handle = $this->dbh->prepare('SELECT privilege_id FROM privileges LIMIT :offset, :limit');

                $select_handle->bindParam(':offset', $param_offset, \PDO::PARAM_INT);
                $select_handle->bindParam(':limit', $param_limit, \PDO::PARAM_INT);
        }


        $param_offset = 0;
        $param_limit = 1000;

        if (!$select_handle->execute()) $this->e->triggerException('
                                                                                        Error in class (' . __CLASS__ . '): [ ' . 
                                                                                        implode(', ', $select_handle->errorInfo()) . 
                                                                                        ' ] on line ' . __LINE__);
        while ($row = $select_handle->fetch(\PDO::FETCH_ASSOC))
        {
            $this->_collection[] = $row['privilege_id'];
        }
        
         parent::__construct();
 
        
    }

    
    

    /*
     *
     *  [ load ]
     * ________________________________________________________________
     * 
     * 
     *
     *
     *
     */

    static public function load () 
    {
            // get role
            $class = __CLASS__;
            return new $class;

     }

    
    
    public function current()
    {
        return \Cataleya\Admin\Privilege::load($this->_collection[$this->_position]);
        
    }
    
    
    


    
    
}



