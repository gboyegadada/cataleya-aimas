<?php


namespace Cataleya\Admin;



/*  

CLASS NOTIFICATION 

*/


class Notification 
extends \Cataleya\Collection 
implements \Cataleya\System\Event\Listener 
{

	private $_data = array();
	private $_modified = array();
        
        // To be used by [\Cataleya\Collection]
        protected $_collection = array ();
        protected $_position = 0;
        
        // CONST VALUES 1 - 15 RESERVED FOR [ _Abstract_Collection ]
    

	private $dbh, $e, $_description;
	private static $_template_path;
	private static $_base_url;
        
        private static $_instances = array ();


	

    public function __construct($notification_keyword)
    {
		

        
		// Get database handle...
                $this->dbh = \Cataleya\Helper\DBH::getInstance();
		
		// Get error handler
		$this->e = \Cataleya\Helper\ErrorHandler::getInstance();
                
                // set temp dir
                self::$_template_path = 'admin-notification.email.html.twig';
                
		// Set base url
		self::$_base_url = (SSL_REQUIRED ? 'https://' : 'http://') . HOST . '/' . ROOT_URI;
		
		///////////////// Load notification /////////////////////
		static $notification_select;
		
		if (empty($notification_select)) {
			$notification_select = $this->dbh->prepare('SELECT * FROM notification_profile WHERE keyword = :keyword LIMIT 1');
		}
                

		$notification_select_param_keyword = $notification_keyword;
		$notification_select->bindParam(':keyword', $notification_select_param_keyword, \PDO::PARAM_STR);
				
		if (!$notification_select->execute()) $this->e->triggerException('Error in class (' . __CLASS__ . '): [ ' . implode(', ', $notification_select->errorInfo()) . ' ] on line ' . __LINE__);
		
		$this->_data = $notification_select->fetch(\PDO::FETCH_ASSOC);
		
		if (empty($this->_data)) return NULL;
                
                

		

		
		///////////// Load ALERTS ////////////////
		static $recipients_select, $recipients_select_param_notification_id;
		
		// Do prepared statement for 'SELECT'...
		if (empty($recipients_select)) {
		$recipients_select = $this->dbh->prepare('
                                                                SELECT alert_id, admin_id   
                                                                FROM alerts 
                                                                WHERE notification_id = :notification_id 
                                                                ');
		$recipients_select->bindParam(':notification_id', $recipients_select_param_notification_id, \PDO::PARAM_INT);
		}
		
		$recipients_select_param_notification_id = $this->_data['notification_id'];
		if (!$recipients_select->execute()) $this->e->triggerException('Error in class (' . __CLASS__ . '): [ ' . implode(', ', $recipients_select->errorInfo()) . ' ] on line ' . __LINE__);
		
		//while ($row = $recipients_select->fetch(\PDO::FETCH_ASSOC)) $this->_collection[] = $row;
                
                $this->_collection = $recipients_select->fetchAll(\PDO::FETCH_ASSOC);
                
                parent::__construct();
		
		
		
    }
    



	/* ___________DESTRUCT _______________________________________________*/

        public function __destruct()
        {
                    $this->saveData();
        }
	

    
        public function current()
        {
            return \Cataleya\Admin\Notification\Alert::load($this->_collection[$this->_position]['alert_id']);

        }
        
        
      



	/* ___________ LOAD _______________________________________________*/
	
	public static function load($notification_keyword)
	{
            
            $notification_keyword = filter_var($notification_keyword, FILTER_VALIDATE_REGEXP, array('options'=>array('regexp' => '/^(([A-Za-z0-9\-]+\.?){1,10})$/')));
            if ($notification_keyword===FALSE) \Cataleya\Helper\ErrorHandler::getInstance()->triggerException('Error in class (' . __CLASS__ . '): [ invalid notification keyword ] on line ' . __LINE__);

            
            if (!isset(self::$_instances[$notification_keyword]))
            {			
                // Create instance...
                $object =  __CLASS__;
		self::$_instances[$notification_keyword] = new $object ($notification_keyword);
                
            }
            
            if (!empty(self::$_instances[$notification_keyword]->_data) && self::$_instances[$notification_keyword] !== NULL) 
            {
                return self::$_instances[$notification_keyword];
                
            } else {
                unset (self::$_instances[$notification_keyword]);
                return NULL;
            }
		
		
	}
        
        
        
        
	
	/*
	 *
	 *  [ create ]
	 * ________________________________________________________________
	 * 
	 * 
	 *
	 *
	 */

	 
	static public function create (array $_config = array ()) 
	{
		 
                if (empty($_config) ) \Cataleya\Helper\ErrorHandler::getInstance()->triggerException('
										Error in class (' . __CLASS__ . '): [ Invalid params ] on line ' . __LINE__);
                

                
                $_config = filter_var_array($_config, array(	
										'keyword'	=>	array('filter'		=>	FILTER_VALIDATE_REGEXP, 
														'options'	=>	array('regexp' => '/^(([A-Za-z0-9\-]+\.?){1,10})$/')
																), 
										'scope'	=>	array('filter'		=>	FILTER_VALIDATE_REGEXP, 
														'options'	=>	array('regexp' => '/^(([A-Za-z0-9\-]+\/?){1,30}\*?)$/')
																), 
										'icon'	=>	array('filter'		=>	FILTER_VALIDATE_REGEXP, 
														'options'	=>	array('regexp' => '/^(([A-Za-z0-9\-]+\.?){1,10})\.([A-Za-z]{3,4})$/')
																), 
										'language_code'	=>	array('filter'		=>	FILTER_VALIDATE_REGEXP, 
														'options'	=>	array('regexp' => '/^[A-Za-z]{2,3}$/')
																), 
										'title'	=>	array('filter'		=>	FILTER_VALIDATE_REGEXP, 
														'options'	=>	array('regexp' => '/^[A-Za-z0-9,\-\W]{2,240}$/')
																), 
										'description'	=>	array('filter'		=>	FILTER_VALIDATE_REGEXP, 
														'options'	=>	array('regexp' => '/^[A-Za-z0-9,\-\W]{2,1500}$/')
																), 
                                                                                'entity_name'	=>	array('filter'		=>	FILTER_VALIDATE_REGEXP, 
                                                                                                'options'	=>	array('regexp' => '/^[A-Za-z0-9\- ]{2,60}$/')
                                                                                                                ), 
                                                                                'entity_name_plural'	=>	array('filter'		=>	FILTER_VALIDATE_REGEXP, 
                                                                                                'options'	=>	array('regexp' => '/^[A-Za-z0-9\- ]{2,60}$/')
                                                                                                                ), 
                                                                                'notification_text'	=>	array('filter'		=>	FILTER_VALIDATE_REGEXP, 
                                                                                                'options'	=>	array('regexp' => '/^[A-Za-z0-9,\-\W]{2,1000}$/')
                                                                                                                ), 
                                                                                'send_after_period'	=>	FILTER_VALIDATE_INT, 
                                                                                'send_after_interval'	=>	array('filter'		=>	FILTER_VALIDATE_REGEXP, 
                                                                                                'options'	=>	array('regexp' => '/^(second|minute|hour|day|week|month|year|decade|century)s?$/')
                                                                                                                ), 

										'send_after_increments'	=>	array('filter'		=>	FILTER_VALIDATE_INT, 
																'options'	=>	array('min_range' => 0, 'max_range' => 200)
																), 
										'request_uri'	=>	FILTER_VALIDATE_URL

										)
								);
                
                foreach ($_config as $k=>$v) 
                {
                        if ($v === FALSE || $v === NULL) 
                        \Cataleya\Helper\ErrorHandler::getInstance()->triggerException('
										Error in class (' . __CLASS__ . '): [ Invalid param:' . $k . ' ] on line ' . __LINE__);
                }
               
                // Verify language code
                $_language = \Cataleya\Locale\Language::load($_config['language_code']);
                if ($_language === NULL) \Cataleya\Helper\ErrorHandler::getInstance()->triggerException('Error in class (' . __CLASS__ . '): [ Invalid language code ] on line ' . __LINE__);
                
                
		// Get database handle...
                $dbh = \Cataleya\Helper\DBH::getInstance();
		
                // //// START TRANSACTION /////
                $dbh->beginTransaction();
                
                /*
                 * Handle description Attribute 
                 *
                 */

                 $_description = \Cataleya\Asset\Description::create($_config['title'], $_config['description'], $_config['language_code']);

		

		// Get error handler
		$e = \Cataleya\Helper\ErrorHandler::getInstance();
                
                

		
		// construct
		static 
				$instance_insert;
                
                $instance_insert_params = $_config;
                $instance_insert_params['description_id'] = $_description->getID();
		
		if (empty($instance_insert))
		{
			$instance_insert = $dbh->prepare('
                                                        INSERT INTO notification_profile   
                                                        (keyword, scope, entity_name, entity_name_plural, description_id, notification_text, send_after_period, send_after_interval, send_after_increments, request_uri, icon, date_added, last_modified) 
                                                        VALUES (:keyword, :scope, :entity_name, :entity_name_plural, :description_id, :notification_text, :send_after_period, :send_after_interval, :send_after_increments, :request_uri, :icon, NOW(), NOW())
                                                        ');

		}
                
                
															
                $instance_insert->bindParam(':keyword', $instance_insert_params['keyword'], \PDO::PARAM_STR);						
                $instance_insert->bindParam(':scope', $instance_insert_params['scope'], \PDO::PARAM_STR);
                $instance_insert->bindParam(':entity_name', $instance_insert_params['entity_name'], \PDO::PARAM_STR);
                $instance_insert->bindParam(':entity_name_plural', $instance_insert_params['entity_name_plural'], \PDO::PARAM_STR);
                $instance_insert->bindParam(':notification_text', $instance_insert_params['notification_text'], \PDO::PARAM_STR);
                $instance_insert->bindParam(':send_after_period', $instance_insert_params['send_after_period'], \PDO::PARAM_INT);
                $instance_insert->bindParam(':send_after_interval', $instance_insert_params['send_after_interval'], \PDO::PARAM_STR);
                $instance_insert->bindParam(':send_after_increments', $instance_insert_params['send_after_increments'], \PDO::PARAM_INT);
                $instance_insert->bindParam(':description_id', $instance_insert_params['description_id'], \PDO::PARAM_INT);
                $instance_insert->bindParam(':request_uri', $instance_insert_params['request_uri'], \PDO::PARAM_STR);
                $instance_insert->bindParam(':icon', $instance_insert_params['icon'], \PDO::PARAM_STR);
		
       
		if (!$instance_insert->execute()) $e->triggerException('
										Error in class (' . __CLASS__ . '): [ ' . 
										implode(', ', $instance_insert->errorInfo()) . 
										' ] on line ' . __LINE__);
										

                
		// AUTOLOAD RETURN IT
		$instance = self::load($instance_insert_params['keyword']);
                
                
                // SETUP ALERTS
                static $insert_handle2;
                
                if (empty($insert_handle2)) 
                {
                    $insert_handle2 = $dbh->prepare('
                        INSERT INTO alerts (notification_id, admin_id) 
                        SELECT :notification_id, admin_id 
                        FROM admin 
                     ');
                    
                    
                }
                
                $insert_handle2_params_id = $instance->getID();
                $insert_handle2->bindParam(':notification_id', $insert_handle2_params_id, \PDO::PARAM_INT);
                
		if (!$insert_handle2->execute()) \Cataleya\Helper\ErrorHandler::getInstance()->triggerException('
										Error in class (' . __CLASS__ . '): [ ' . 
										implode(', ', $insert_handle2->errorInfo()) . 
										' ] on line ' . __LINE__);
                
                
		
                
                // //// COMMIT TRANSACTION /////
                $dbh->commit();
		
		return $instance;
		
		
	}
        



	/*
	 *
	 *  [ delete ]
	 * ________________________________________________________________
	 * 
	 * 
	 *
	 *
	 *
	 */
	 
	public function delete () 
	{
                        
                        $this->getDescription()->delete();
                        
                        // Delete rows containing 'store_id' in specified tables
                        \Cataleya\Helper\DBH::sanitize(array('alerts', 'notification_profile'), 'notification_id', $this->getID());
            
							
 
		
		// $this = NULL;
		return TRUE;
		
		
	}



        
        
        
	
	///////////// get ALERT for the specified user // return as [ \Cataleya\Admin\Notification\Alert ] ////////////////
	

	
	public function getAlert (\Cataleya\Admin\User $_user) 
	{
		
            foreach ($this->_collection as $_entry) 
            {
                if ($_entry['admin_id'] === $_user->getAdminID()) return \Cataleya\Admin\Notification\Alert::load($_entry['alert_id']);
            }
            
            // No user match (highly unlikely)
            $this->e->triggerException('Error in class (' . __CLASS__ . '): [ Notification Alert for ' . $_user->getName() . ' not found ] on line ' . __LINE__);
		
	}
	
	
	



	
        
        
        /**
         * 
         * @return string
         * 
         */
        public function getScope() {
            
            return $this->_data['scope'];
            
        }
        
        
        
        

        /**
         * 
         * @param type $value
         * @return \Cataleya\Admin\Notification
         * 
         */
        public function setScope($value = NULL)
        {
                $value = filter_var($value, FILTER_VALIDATE_REGEXP, array('options'=>array('regexp' => '/^(([A-Za-z0-9\-]+\/?){1,30}\*?)$/')));
                if ($value===FALSE) $this->e->triggerException('Error in class (' . __CLASS__ . '): [ invalid argument for method: ' . __FUNCTION__ . ' ] on line ' . __LINE__);
		

                $this->_data['scope'] = $value;
                $this->_modified[] = 'scope';

                return $this;
        }
	
        
	
	/*
	
		IMPORTANT!!
		________________________________________________________________
		
		in [ method: notify ], [ arg: $_count ] must an integer in the form (+n)
		
		As you were!
	
	
	*/
	
	
	public function notify($_event, array $_data = array ())
	{
        $_p = $_data['params'];

        $_count = (isset($_p['count']) && is_numeric($_p['count'])) ? (int)$_count : 1;

        $_count = filter_var($_count, FILTER_VALIDATE_INT, FILTER_NULL_ON_FAILURE);
        if ($_count === NULL) return FALSE;       

//   Note: I think this part is redundant now. Will also remove 'abs_count' column after observing class for a while.          
//
//		if (($this->_data['abs_count']+$_count) > 0) 
//		{
//			$this->_data['abs_count'] += $_count;
//			if (!in_array('abs_count', $this->_modified)) $this->_modified[] = 'abs_count';
//		}



        if ($_count > 0) 
        {
                $this->_data['increments_since_last_sent'] += $_count;
                $this->_modified[] = 'increments_since_last_sent';
        }

		
		static $_time_unit_to_secs = array();
		
		if (empty($_time_unit_to_secs)) {
                
                $_days_in_month = (int)date("t");
		$_time_unit_to_secs = array (
										'years' => (60*60*24*365), 
										'year' => (60*60*24*365), 
										'months' => (60*60*24*$_days_in_month), 
										'month' => (60*60*24*$_days_in_month), 
										'weeks' => (60*60*24*7), 
										'week' => (60*60*24*7), 
										'days' => (60*60*24), 
										'day' => (60*60*24), 
										'hours' => (60*60), 
										'hour' => (60*60), 
										'hrs' => (60*60), 
										'hr' => (60*60), 
										'minutes' => 60, 
										'minute' => 60, 
										'mins' => 60, 
										'min' => 60, 
										'seconds' => 1, 
										'second' => 1,
										'secs' => 1, 
										'sec' => 1
										);
		}
		

                $time_digit = (int)$this->_data['send_after_period'];
                $time_unit = strtolower($this->_data['send_after_interval']);
                
                
                
                // SEND EMAILS
		$_last_sent = $this->_data['last_sent_at'] + ($time_digit * $_time_unit_to_secs[$time_unit]);
		
		if ($this->_data['increments_since_last_sent'] > $this->_data['send_after_increments'])
		{
			$this->emailNotification();
		}
		
		else if ( time() > $_last_sent && $this->_data['increments_since_last_sent'] > 0)
		{
			$this->emailNotification();
		}
                
                
                // SEND ALERTS TO DASHBOARD
                $this->updateDashboard($_count);

		
		
		return TRUE;
		
	}
    
	
        
        
        
        
	

	
	 
	
	

	///////////////////////////// SEND EMAIL NOTIFICATION /////////////
	
	private function emailNotification () 
	{
		

		$_to = array();
		
		foreach ($this as $alert)
		{
            if (!$alert->emailEnabled()) continue;

            $_recipient = $alert->getRecipient();
			$_to[$_recipient->getEmailAddress()] = $_recipient->getName();
			
		}
                
                
		if (empty($_to)) return;
		
                
                
                


        $temp_vars = array ();

        $temp_vars['header_title'] = 'Store Notification: ' . $this->getDescription()->getTitle('EN');	
        $temp_vars['title'] = $temp_vars['inline_title'] = $this->getDescription()->getTitle('EN');		
		$temp_vars['icon'] = $temp_vars['title_icon'] = $this->getIconHref();
		$temp_vars['notification'] = \Cataleya\Helper::countInEnglish($this->getIncrementsSinceLastSent(), $this->_data['entity_name'], $this->_data['entity_name_plural']) . ' ' . $this->_data['notification_text'];
                
                $href = $this->getRequestUri();
							
		$temp_vars['href'] = $this->_data['request_uri'];
		$temp_vars['what_to_do'] = 'Please visit this link to view affected items:';	
		
		$emailer = \Cataleya\Helper\Emailer::getInstance($this->_data['keyword']);
		
		$emailer->To = $_to;
		$emailer->From = array('no-reply@'.EMAIL_HOST=>'Store Notification');
		$emailer->Sender = 'no-reply@'.EMAIL_HOST;
		$emailer->ReplyTo = 'no-reply@'.EMAIL_HOST;
		$emailer->Subject = 'Store notification: ' . $this->getDescription()->getTitle('EN');
		$emailer->TextBody = $temp_vars['notification'] . $temp_vars['what_to_do'] . $href;
                
        $emailer->setHTMLBody(__('dash.skin')->render(self::$_template_path, $temp_vars));

        if ($emailer->send() === 0) {

            \Cataleya\System\Event::notify('emailer/error', $emailer, array (
                'logger_id'=>'emailer.log', 
                'blob'=>'One or more notification emails could not be sent in class: ' . __CLASS__ . ', on line ' . __LINE__
                    ));

        }

        $this->setIncrementsSinceLastSent(0);
        $this->setLastSentAt(time());
                
		
	}
        
        
        
        
        
        /*
         * 
         * private: [ updateDashboard ]
         * 
         */
        
        
        private function updateDashboard ($_count) 
        {
            
            if (filter_var($_count, FILTER_VALIDATE_INT, FILTER_NULL_ON_FAILURE) === NULL) return FALSE;
            
            static $update_handle, $notification_id, $increment;
            
            if (empty($update_handle)) 
            {
                $update_handle = $this->dbh->prepare('
                                                        UPDATE alerts 
                                                        SET tally = tally + :increment, is_displayed=0, is_read=0 
                                                        WHERE notification_id = :notification_id AND to_dashboard = 1 
                                                        ');
                $update_handle->bindParam(':notification_id', $notification_id, \PDO::PARAM_INT);
                $update_handle->bindParam(':increment', $increment, \PDO::PARAM_INT);
            }
            
            $increment = (int)$_count;
            $notification_id = $this->getNotificationId();
            
            if (!$update_handle->execute()) $this->e->triggerException('Error in class (' . __CLASS__ . '): [ ' . implode(', ', $update_handle->errorInfo()) . ' ] on line ' . __LINE__);
		
            
            
        }

        















        public function saveData () 
	{
		

		if (empty($this->_modified)) return;
                $this->_modified = array_unique($this->_modified);
                
		$_update_params = array();
		$key_val_pairs = array();
		
		foreach ($this->_modified as $key) $key_val_pairs[$key] = $key . ' = :' . $key;
		
	
		// Do prepared statement for 'INSERT'...
		$update_handle = $this->dbh->prepare('
												UPDATE notification_profile 
												SET ' . implode (', ', $key_val_pairs) . ', last_modified=now()     
												WHERE notification_id = :notification_id 
												');
		$update_handle->bindParam(':notification_id', $_update_params['notification_id'], \PDO::PARAM_INT);
		$_update_params['notification_id'] = $this->_data['notification_id'];
                
		foreach ($this->_modified as $key) {
                    
                        $_update_params[$key] = $this->_data[$key];                        
			$update_handle->bindParam(':'.$key, $_update_params[$key], \Cataleya\Helper\DBH::getTypeConst($_update_params[$key]));

		}
		
		if (!$update_handle->execute()) $this->e->triggerException('Error in class (' . __CLASS__ . '): [ ' . implode(', ', $update_handle->errorInfo()) . ' ] on line ' . __LINE__);
		
                $this->_modified = array ();
	}
        
        
        
        
        
        
        
        
        
        
        
        //////////////////////////////////////// GETTERS + SETTERS ////////////////////////////////////////////////////
        
        

        /*
         *
         * [ getNotificationId ] 
         *_____________________________________________________
         *
         *
         */

        public function getNotificationId()
        {
                return $this->_data['notification_id'];
        }
        
        
        
        /*
         *
         * [ getNotificationId ] 
         *_____________________________________________________
         *
         *
         */

        public function getID()
        {
                return $this->_data['notification_id'];
        }






        /*
         *
         * [ getKeyword ] 
         *_____________________________________________________
         *
         *
         */

        public function getKeyword()
        {
                return $this->_data['keyword'];
        }



        /*
         *
         * [ setKeyword ] 
         *_____________________________________________________
         *
         *
         */

        public function setKeyword($value = NULL)
        {
                $value = filter_var($value, FILTER_VALIDATE_REGEXP, array('options'=>array('regexp' => '/^(([A-Za-z0-9\-]+\.?){1,10})$/')));
                if ($value===FALSE) $this->e->triggerException('Error in class (' . __CLASS__ . '): [ invalid argument for method: ' . __FUNCTION__ . ' ] on line ' . __LINE__);
		

                $this->_data['keyword'] = $value;
                $this->_modified[] = 'keyword';

                return $this;
        }



        /*
         *
         * [ getDescriptionId ] 
         *_____________________________________________________
         *
         *
         */

        public function getDescriptionId()
        {
                return $this->_data['description_id'];
        }


        
        /*
         * 
         * [ getDescription ]
         * 
         * 
         */
        
        
        public function getDescription () 
        {
            if (empty($this->_description)) $this->_description = \Cataleya\Asset\Description::load($this->_data['description_id']);
            
            return $this->_description;
        }

        
        


        /*
         *
         * [ getIcon ] 
         *_____________________________________________________
         *
         *
         */

        public function getIcon()
        {
                return $this->_data['icon'];
        }

        
                /*
         *
         * [ getIconHref ] 
         *_____________________________________________________
         *
         *
         */

        public function getIconHref()
        {
                return self::$_base_url . 'ui/images/ui/notification-icons/' . $this->getIcon();
        }


        /*
         *
         * [ setIcon ] 
         *_____________________________________________________
         *
         *
         */

        public function setIcon($value = NULL)
        {
                if (!\Cataleya\Helper\Validator::isFilename($value)) $this->e->triggerException('Error in class (' . __CLASS__ . '): [ invalid argument for method: ' . __FUNCTION__ . ' ] on line ' . __LINE__);


                $this->_data['icon'] = $value;
                $this->_modified[] = 'icon';

                return $this;
        }



        /*
         *
         * [ getEntityName ] 
         *_____________________________________________________
         *
         *
         */

        public function getEntityName()
        {
                return $this->_data['entity_name'];
        }



        /*
         *
         * [ setEntityName ] 
         *_____________________________________________________
         *
         *
         */

        public function setEntityName($value = NULL)
        {

                $value = filter_var($value, FILTER_VALIDATE_REGEXP, array('options'=>array('regexp' => '/^[A-Za-z0-9\- ]{2,60}$/')));
                if ($value===FALSE) $this->e->triggerException('Error in class (' . __CLASS__ . '): [ invalid argument for method: ' . __FUNCTION__ . ' ] on line ' . __LINE__);


                $this->_data['entity_name'] = $value;
                $this->_modified[] = 'entity_name';

                return $this;
        }



        /*
         *
         * [ getEntityNamePlural ] 
         *_____________________________________________________
         *
         *
         */

        public function getEntityNamePlural()
        {
                return $this->_data['entity_name_plural'];
        }



        /*
         *
         * [ setEntityNamePlural ] 
         *_____________________________________________________
         *
         *
         */

        public function setEntityNamePlural($value = NULL)
        {
                $value = filter_var($value, FILTER_VALIDATE_REGEXP, array('options'=>array('regexp' => '/^[A-Za-z0-9\- ]{2,60}$/')));
                if ($value===FALSE) $this->e->triggerException('Error in class (' . __CLASS__ . '): [ invalid argument for method: ' . __FUNCTION__ . ' ] on line ' . __LINE__);


                $this->_data['entity_name_plural'] = $value;
                $this->_modified[] = 'entity_name_plural';

                return $this;
        }



        /*
         *
         * [ getNotificationText ] 
         *_____________________________________________________
         *
         *
         */

        public function getNotificationText()
        {
                return $this->_data['notification_text'];
        }



        /*
         *
         * [ setNotificationText ] 
         *_____________________________________________________
         *
         *
         */

        public function setNotificationText($value = NULL)
        {
                $value = filter_var($value, FILTER_VALIDATE_REGEXP, array('options'=>array('regexp' => '/^[A-Za-z0-9,\-\W]{2,1000}$/')));
                if ($value===FALSE) $this->e->triggerException('Error in class (' . __CLASS__ . '): [ invalid argument for method: ' . __FUNCTION__ . ' ] on line ' . __LINE__);


                $this->_data['notification_text'] = $value;
                $this->_modified[] = 'notification_text';

                return $this;
        }



        /*
         *
         * [ getSendAfterPeriod ] 
         *_____________________________________________________
         *
         *
         */

        public function getSendAfterPeriod()
        {
                return (int)$this->_data['send_after_period'];
        }



        /*
         *
         * [ setSendAfterPeriod ] 
         *_____________________________________________________
         *
         *
         */

        public function setSendAfterPeriod($value = NULL)
        {
                $value = filter_var($value, FILTER_VALIDATE_INT);
                if ($value===FALSE) $this->e->triggerException('Error in class (' . __CLASS__ . '): [ invalid argument for method: ' . __FUNCTION__ . ' ] on line ' . __LINE__);


                $this->_data['send_after_period'] = $value;
                $this->_modified[] = 'send_after_period';

                return $this;
        }
        
        
        

        /*
         *
         * [ getSendAfterPeriod ] 
         *_____________________________________________________
         *
         *
         */

        public function getSendAfterInterval()
        {
                return $this->_data['send_after_interval'];
        }



        /*
         *
         * [ setSendAfterPeriod ] 
         *_____________________________________________________
         *
         *
         */

        public function setSendAfterInterval($value = NULL)
        {
                $value = filter_var($value, FILTER_VALIDATE_REGEXP, array('options'=>array('regexp' => '/^(second|minute|hour|day|week|month|year|decade|century)s?$/')));
                if ($value===FALSE) $this->e->triggerException('Error in class (' . __CLASS__ . '): [ invalid argument for method: ' . __FUNCTION__ . ' ] on line ' . __LINE__);


                $this->_data['send_after_interval'] = $value;
                $this->_modified[] = 'send_after_interval';

                return $this;
        }
        
        
        



        /*
         *
         * [ getSendAfterIncrements ] 
         *_____________________________________________________
         *
         *
         */

        public function getSendAfterIncrements()
        {
                return $this->_data['send_after_increments'];
        }



        /*
         *
         * [ setSendAfterIncrements ] 
         *_____________________________________________________
         *
         *
         */

        public function setSendAfterIncrements($value = NULL)
        {
                if ($value===NULL || filter_var($value, FILTER_VALIDATE_INT) === FALSE) $this->e->triggerException('Error in class (' . __CLASS__ . '): [ invalid argument for method: ' . __FUNCTION__ . ' ] on line ' . __LINE__);


                $this->_data['send_after_increments'] = (int)$value;
                $this->_modified[] = 'send_after_increments';

                return $this;
        }



        /*
         *
         * [ getIncrementsSinceLastSent ] 
         *_____________________________________________________
         *
         *
         */

        public function getIncrementsSinceLastSent()
        {
                return (int)$this->_data['increments_since_last_sent'];
        }



        /*
         *
         * [ setIncrementsSinceLastSent ] 
         *_____________________________________________________
         *
         *
         */

        private function setIncrementsSinceLastSent($value = NULL)
        {
                if ($value===NULL || filter_var($value, FILTER_VALIDATE_INT) === FALSE) $this->e->triggerException('Error in class (' . __CLASS__ . '): [ invalid argument for method: ' . __FUNCTION__ . ' ] on line ' . __LINE__);


                $this->_data['increments_since_last_sent'] = (int)$value;
                $this->_modified[] = 'increments_since_last_sent';

                return $this;
        }



        /*
         *
         * [ getLastSentAt ] 
         *_____________________________________________________
         *
         *
         */

        public function getLastSentAt()
        {
                return $this->_data['last_sent_at'];
        }



        /*
         *
         * [ setLastSentAt ] 
         *_____________________________________________________
         *
         *
         */

        private function setLastSentAt($value = NULL)
        {
                $value = filter_var($value, FILTER_VALIDATE_INT, FILTER_NULL_ON_FAILURE);

                if ($value===NULL) $this->e->triggerException('Error in class (' . __CLASS__ . '): [ invalid argument for method: ' . __FUNCTION__ . ' ] on line ' . __LINE__);

                $this->_data['last_sent_at'] = $value;
                $this->_modified[] = 'last_sent_at';

                return $this;
        }








        /*
         *
         * [ getRequestUri ] 
         *_____________________________________________________
         *
         *
         */

        public function getRequestUri()
        {
                return $this->_data['request_uri'];
        }



        /*
         *
         * [ setRequestUri ] 
         *_____________________________________________________
         *
         *
         */

        public function setRequestUri($value = NULL)
        {
                $value = filter_var($value, FILTER_VALIDATE_URL, FILTER_NULL_ON_FAILURE);
                if ($value===NULL) $this->e->triggerException('Error in class (' . __CLASS__ . '): [ invalid argument for method: ' . __FUNCTION__ . ' ] on line ' . __LINE__);


                $this->_data['request_uri'] = $value;
                $this->_modified[] = 'request_uri';

                return $this;
        }



        /*
         *
         * [ getDateAdded ] 
         *_____________________________________________________
         *
         *
         */

        public function getDateAdded()
        {
                return $this->_data['date_added'];
        }






        /*
         *
         * [ getLastModified ] 
         *_____________________________________________________
         *
         *
         */

        public function getLastModified()
        {
                return $this->_data['last_modified'];
        }




	
	
	
	
}



