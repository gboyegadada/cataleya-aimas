<?php


namespace Cataleya\Admin;







/*
 *
 * [ Cataleya \ Admin \ Privilege ]
 * ________________________________________________________________________
 *
 * (c) 2012 Fancy Paper Planes
 *
 *
*/





// if (!defined ('IS_ADMIN_FLAG') || !IS_ADMIN_FLAG) die("Illegal Access");





class Privilege extends \Cataleya\Data {
	

	protected $_data = array();
	protected $_modified = array();
        protected $_description;
        
        const TABLE = 'privileges';
        const PK = 'privilege_id';
        
        protected $dbh, $e, $bcrypt;
	
	
	private function __construct($privilege_id) 
	{
		// Get error handler
		$this->e = \Cataleya\Helper\ErrorHandler::getInstance();
		

		$privilege_id = filter_var($privilege_id, FILTER_SANITIZE_STRIPPED);
		
		if ($privilege_id === FALSE) 
		{
			if (defined('DEBUG_MODE') && DEBUG_MODE === TRUE) 
			{
			$this->e->triggerException('Error in class (' . __CLASS__ . '): [ Invalid username ] on line ' . __LINE__);
			}
			
			$this->_data = array();
			return;
		}
		
		// Get database handle...
		$this->dbh = \Cataleya\Helper\DBH::getInstance();
		
		// Get encryption class...
		$this->bcrypt = \Cataleya\Helper\Bcrypt::getInstance();
		
		
		// ATTEMPT TO LOAD CUSTOMER DATA
		
		static $privilege_select, 
                        $param_privilege_id;
		
		
		// Do prepared statement...
		if (empty($privilege_select)) {
		$privilege_select = $this->dbh->prepare('
                                                            SELECT * FROM privileges 
                                                            WHERE privilege_id = :privilege_id 
                                                            LIMIT 1
                                                            ');
		$privilege_select->bindParam(':privilege_id', $param_privilege_id, \PDO::PARAM_STR);
		}
		
		$param_privilege_id = $privilege_id;
		
		// Execute...
		if (!$privilege_select->execute()) $this->e->triggerError('Error in class (' . __CLASS__ . '): [ ' . implode(', ', $privilege_select->errorInfo()) . ' ] on line ' . __LINE__);
		
		// Check for results...
		if ( ($this->_data = $privilege_select->fetch(\PDO::FETCH_ASSOC)) === FALSE ) 
		{
			$this->_data = array();
		} else {
                    
                    // Load description...
                    $this->_description = \Cataleya\Asset\Description::load($this->_data['description_id']);		

                }
		
		
			
	}
	
	




	/*
	 *
	 *  [ load ]
	 * ________________________________________________________________
	 * 
	 * 
	 *
	 *
	 *
	 */
	 
	static public function load ($_privilege_id = FALSE) 
	{
		if ($_privilege_id === FALSE) return NULL;

		// get privilege
		$privilege = new \Cataleya\Admin\Privilege ($_privilege_id);
		if (empty($privilege->_data))
		{
			unset($privilege);
			return NULL;
		}			
		
		
		////////////////// RETURN USER ROLE ////////////////////
		
		return $privilege;
                
         }



        
        


	/*
	 *
	 *  [ create ]
	 * ________________________________________________________________
	 * 
	 * 
	 *
	 *
	 *
	 */
	 
	static public function create ($privilege_id = NULL, $description_title = '', $description_text = '', $language_code = 'EN') 
	{
		
                if ($privilege_id === NULL || !is_string($privilege_id) || empty($privilege_id)) return NULL;
		
		// Get database handle...
                $dbh = \Cataleya\Helper\DBH::getInstance();
		
		// Get error handler
		$e = \Cataleya\Helper\ErrorHandler::getInstance();

		/*
		 * Handle description Attribute 
		 *
		 */
		 
                 
		 $description = \Cataleya\Asset\Description::create($description_title, $description_text, $language_code);
		 
		                
		// construct
		static 
				$privilege_insert, 
                                $privilege_insert_param_description_id, 
                                $privilege_insert_param_privilege_id;
		
		if (empty($privilege_insert))
		{
			$privilege_insert = $dbh->prepare('
                                                        INSERT INTO privileges (privilege_id, description_id) 
                                                        VALUES (:privilege_id, :description_id)
                                                        ');
															
			$privilege_insert->bindParam(':description_id', $privilege_insert_param_description_id, \PDO::PARAM_INT);
                        $privilege_insert->bindParam(':privilege_id', $privilege_insert_param_privilege_id, \PDO::PARAM_STR);
		}
		
		$privilege_insert_param_description_id = $description->getID();
                $privilege_insert_param_privilege_id = $privilege_id;
		
		if (!$privilege_insert->execute()) 
                {
                    $description->delete();
                    $e->triggerException('
                                            Error in class (' . __CLASS__ . '): [ ' . 
                                            implode(', ', $privilege_insert->errorInfo()) . 
                                            ' ] on line ' . __LINE__);
                    return NULL;
                } 
                    
										
		
		// AUTOLOAD NEW ADMIN_PAGE AND RETURN IT
		$privilege = self::load($dbh->lastInsertId());
		
		return $privilege;
		
		
	}





        
  
	 
	 /*
	 *  [ delete ]
	 * ________________________________________________________________
	 * 
	 * 
	 *
	 *
	 */

	 
	public function delete () 
	{
             // prevent deletion !!
             return;
                        
        }


        
        



/*
 *
 * [GETTERS] and [SETTERS] 
 * __________________________________________________________________________________
 *
 *
 *
 */	
	

        
        




	// ID

	public function getID () 
	{
		return $this->_data['privilege_id'];		
		
	}
	


        

	/*
	 *
	 *  [ getDescription ]
	 * ________________________________________________________________
	 * 
	 * Will return product description as object
	 *
	 *
	 */
	 
	public function getDescription () 
	{
		
		return $this->_description;
		
	}

        
        

   

	
}




