<?php



namespace Cataleya;



/*
 *
 * 	@Package: Cataleya
 * 	@Class: \Cataleya\Catalog
 *
 * 	(c) 2012 Fancy Paper Planes
 *
 *
 *
 */

class Catalog extends \Cataleya\Catalog\ProductCollection {
    
    


    public function __construct() {
       
        // Get database handle...
        $this->dbh = \Cataleya\Helper\DBH::getInstance();
		
        // Get error handler
        $this->e = \Cataleya\Helper\ErrorHandler::getInstance();
        
        

        parent::__construct();
 
        
    }
    
    
    
    
    /**
     * 
     * @param array $filters
     * @param array $sort
     * @param int $page_size
     * @param int $page_num
     * @return \Cataleya\Catalog
     */
    public static function load(
            $filters = array(), 
            $sort = array(
                'order_by'=>'', 
                'order'=>'', 
                'index'=>''
                ), 
            $page_size = 16, 
            $last_seen = 0, 
            $page_num = 0
            ) 
    {
        
        
        $_objct = __CLASS__;
        $instance = new $_objct;

		
        /////////////////// LOAD ASSOCIATED PRODUCTS /////////////////////
        $instance->_loadCollection($filters, $sort, $page_size, $last_seen, $page_num);
        
        return $instance;
        
        
        
        
    }

    

    


    /*
     * 
     * [ search ]
     * 
     */
    
    public static function search ($_term = '', $sort = array('order_by'=>'', 'order'=>'', 'index'=>''), $page_size = 100, $page_num = 1) 
    {
        
        
        

                // This is used to determine which column is to be used in the index...
                // $_index_with = 'name';


            if (is_array($sort) && isset($sort['order_by'], $sort['order']) )
             {
                 // ORDER BY
                 switch ($sort['order_by']) {

                     case self::ORDER_BY_TITLE:
                         $param_order_by = 'keywords';
                         break;

                     case self::ORDER_BY_ID:
                         $param_order_by = 'product_id';
                         break;                        

                     default :
                         $param_order_by = 'keywords';
                         break;
                 }


                 // ASC | DESC
                 switch ($sort['order']) {

                     case self::ORDER_ASC:
                         $param_order = 'ASC';
                         break;

                     case self::ORDER_DESC:
                         $param_order = 'DESC';
                         break;

                     default :
                         $param_order = 'ASC';
                         break;
                 }                    


             }

             // Defaults                
             else {

                     $param_order = 'ASC';
                     $param_order_by = 'product_id';

             }     




            // First, take out funny looking characters...
            $_term = preg_replace('/[^-\s\.A-Za-z0-9@*#+]+/', '', $_term);

            // Trim then split terms by [:space:]
            $terms = explode(' ', trim($_term, ' '));



            // SORT SEARCH TERMS
            $regex = array();
            $regex['keywords'] = array();


            foreach ($terms as $key=>$val) {

                    // Loose any search terms shorter than 3 chars...
                    if (strlen($val) < 2) unset($terms[$key]);
                    
                    // Also make dots safe...
                    else if (preg_match('/[-\s\.A-Za-z0-9]{1,100}/', $val))  $regex['keywords'][] = preg_replace('/[\.]/', '\.', $val); 
            }

            
            // Build REGEX
            $regex_concat = '';
            foreach ($regex as $k => $v) {
                    if (count($v) == 0) {$regex[$k] = ''; continue; } // no search terms...
                    elseif (count($v) > 3) $v = array_slice($v, 0, 3); // truncate if there are too many search terms...

                    $regex[$k] = "^.*(" . implode('|', $v) . ").*$";
                    $regex_concat .= ($regex_concat != '') ? ' OR ' : '';
                    $regex_concat .= $k . ' REGEXP :regex_' . $k;


            }

            
            
            // Create instance...
            $_objct = __CLASS__;
            $instance = new $_objct;


            // If nothing made it through the filtering...
            // [ _collection ] will be empty a.k.a no results..
            if ($regex_concat == '') return $instance;




            $param_offset = 0;
            $param_limit = 40;
     
            
            // DO SEARCH...
            $select_handle = \Cataleya\Helper\DBH::getInstance()->prepare('
                                                                            SELECT product_id 
                                                                            FROM fulltext_catalog_index_isam  
                                                                            WHERE ' . $regex_concat . '  
                                                                            ORDER BY '. $param_order_by . '   
                                                                            LIMIT :offset, :limit
                                                                        ');

            $select_handle->bindParam(':offset', $param_offset, \PDO::PARAM_INT);
            $select_handle->bindParam(':limit', $param_limit, \PDO::PARAM_INT);
            
            // bind search search params
            foreach ($regex as $k=>$v) $select_handle->bindParam(':regex_'.$k, $regex[$k], \PDO::PARAM_STR);
            


            if (!$select_handle->execute()) \Cataleya\Helper\ErrorHandler::getInstance ()->triggerException('
                                                                        Error in class (' . __CLASS__ . '): [ ' . 
                                                                        implode(', ', $select_handle->errorInfo()) . 
                                                                        ' ] on line ' . __LINE__);





            while ($row = $select_handle->fetch(\PDO::FETCH_ASSOC))$instance->_collection[] = $row['product_id'];

            $instance->pageSetup($page_size, $page_num);

            return $instance;
        
    }
    
    
        
    
    
    




    
    public function getIDs()
    {

        return $this->_collection;
        
    }
    
    
    
    
    
    
    





    
    
}


