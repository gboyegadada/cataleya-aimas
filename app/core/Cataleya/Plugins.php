<?php

namespace Cataleya;


/**
 * Package
 *
 * @package Cataleya
 * @author Gboyega Dada <gboyega@fpplabs.com>
 * @version $Id$
 */
class Plugins {

    const FILTER_ALL = 0;
    const FILTER_ON = 1;
    const FILTER_OFF = 2;

}
