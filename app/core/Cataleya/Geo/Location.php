<?php


namespace Cataleya\Geo;



/*

CLASS LOCATION

*/



class Location   
{
	
	
	private $_data = array();
	private $_modified = array();
        
        private $_country_obj = NULL;

	private $dbh, $e;
	

	


	/*
	 *
	 * [ __construct ]
	 * ________________________________________________________________
	 *
	 *
	 *
	 */
	 
	 
	private function __construct ()
	{
		
		// Get database handle...
                $this->dbh = \Cataleya\Helper\DBH::getInstance();
		
		// Get error handler
		$this->e = \Cataleya\Helper\ErrorHandler::getInstance();
		
	}
	




	/*
	 *
	 * [ __destruct ]
	 * ________________________________________________________________
	 *
	 *
	 *
	 */

    public function __destruct()
    {
		$this->saveData();
    }
	




	/*
	 *
	 * [ load ]
	 * ________________________________________________________________
	 *
	 *
	 *
	 */
	 
	 
	public static function load ($id = 0)
	{
		
		if ($id == 0 || filter_var($id, FILTER_VALIDATE_INT) === FALSE) return NULL;
		
		$location = new \Cataleya\Geo\Location ();
		
		// LOAD: PRODUCT INFO
		static $location_select, $param_location_id;
		
			
		if (!isset($location_select)) {
			// PREPARE SELECT STATEMENT...
			$location_select = $location->dbh->prepare('SELECT * FROM locations WHERE location_id = :location_id LIMIT 1');
			$location_select->bindParam(':location_id', $param_location_id, \PDO::PARAM_INT);
		}
		
		
		$param_location_id = $id;
		
		if (!$location_select->execute()) $location->e->triggerException('
												Error in class (' . __CLASS__ . '): [ ' . 
												implode(', ', $location_select->errorInfo()) . 
												' ] on line ' . __LINE__);
		
		$location->_data = $location_select->fetch(\PDO::FETCH_ASSOC);
                
               


		return $location;	
		
		
	}



	/*
	 *
	 *  [ create ]
	 * ________________________________________________________________
	 * 
	 * 
	 *
	 *
	 *
	 */
	 
	static public function create ($_country_or_province = NULL, $label = 'New Location') 
	{
		
		$_Country = $_Province = NULL;
				
                // Country ...
                if ($_country_or_province instanceof \Cataleya\Geo\Country) 
                {
                    if ($_country_or_province->hasProvinces()) \Cataleya\Helper\ErrorHandler::getInstance()->triggerException('Error in class (' . __CLASS__ . '): [ The 1st argument of ' . __FUNCTION__ . ' must be a Province of the country you specified - required if a country has provinces. ] on line ' . __LINE__);
                    else $_Country = $_country_or_province;
                }

                // Province...
                else if ($_country_or_province instanceof \Cataleya\Geo\Province) 
                {
                    $_Country = $_country_or_province->getCountry();
                    $_Province = $_country_or_province;
                }

                // Invalid argument...
                else \Cataleya\Helper\ErrorHandler::getInstance()->triggerException('Error in class (' . __CLASS__ . '): [ The 1st argument of ' . __FUNCTION__ . ' must be an instance of \Cataleya\Geo\Country or an instance of \Cataleya\Geo\Province  ] on line ' . __LINE__);



		
		// Get database handle...
                $dbh = \Cataleya\Helper\DBH::getInstance();
		
		// Get error handler
		$e = \Cataleya\Helper\ErrorHandler::getInstance();
		
		// construct
		static 
				$location_insert,  
                                $location_insert_param_entry_label, 
                                $location_insert_param_country_code, 
                                $location_insert_param_province_id;
                
                
                $location_insert_param_province_id = (!empty($_Province)) ? $_Province->getProvinceId() : NULL;
                $location_insert_param_country_code = $_Country->getCountryCode();
                $location_insert_param_entry_label = $label;
		
		if (empty($location_insert))
		{
			$location_insert = $dbh->prepare('
                                                        INSERT INTO locations 
                                                        (entry_label, entry_country_code, province_id) 
                                                        VALUES (:entry_label, :country_code, :province_id)
                                                        ');
				
                        $location_insert->bindParam(':province_id', $location_insert_param_province_id, \Cataleya\Helper\DBH::getTypeConst($location_insert_param_province_id));
			$location_insert->bindParam(':country_code', $location_insert_param_country_code, \PDO::PARAM_STR);
                        $location_insert->bindParam(':entry_label', $location_insert_param_entry_label, \PDO::PARAM_STR);
		}
		

		
		if (!$location_insert->execute()) $e->triggerException('
										Error in class (' . __CLASS__ . '): [ ' . 
										implode(', ', $location_insert->errorInfo()) . 
										' ] on line ' . __LINE__);
										
		
		// AUTOLOAD NEW PRODUCT AND RETURN IT
		$location = self::load($dbh->lastInsertId());
		
		return $location;
		
		
	}




        

	/*
	 *
	 *  [ delete ]
	 * ________________________________________________________________
	 * 
	 * 
	 *
	 *
	 *
	 */
	 
	public function delete () 
	{
		
			// DELETE 
			$location_delete = $this->dbh->prepare('
													DELETE FROM locations 
													WHERE location_id = :location_id
													');
			$location_delete->bindParam(':location_id', $location_delete_param_location_id, \PDO::PARAM_INT);
			
			$location_delete_param_location_id = $this->_data['location_id'];
	
			if (!$location_delete->execute()) $this->e->triggerException('
											Error in class (' . __CLASS__ . '): [ ' . 
											implode(', ', $location_delete->errorInfo()) . 
											' ] on line ' . __LINE__);
											
			

		
		// $this = NULL;
		return TRUE;
		
		
	}





        /*
         *
         * [ getID ] 
         *_____________________________________________________
         *
         *
         */

        public function getID()
        {
                return $this->_data['location_id'];
        }




	
	
	public function saveData () 
	{
		

		if (empty($this->_modified)) return;
                
                $this->_modified = array_unique($this->_modified);
                
		$_update_params = array();
		$key_val_pairs = array();
		
		foreach ($this->_modified as $key) $key_val_pairs[$key] = $key . ' = :' . $key;
		
	
		// Do prepared statement for 'INSERT'...
		$update_handle = $this->dbh->prepare('
                                                        UPDATE locations 
                                                        SET ' . implode (', ', $key_val_pairs) . '    
                                                        WHERE location_id = :location_id 
                                                        ');
		$_update_params['location_id'] = $this->_data['location_id'];                
		$update_handle->bindParam(':location_id', $_update_params['location_id'], \PDO::PARAM_INT);
                
		foreach ($this->_modified as $key) {
                    
                        $_update_params[$key] = $this->_data[$key];                        
			$update_handle->bindParam(':'.$key, $_update_params[$key], \Cataleya\Helper\DBH::getTypeConst($_update_params[$key]));

		}
		
		if (!$update_handle->execute()) $this->e->triggerException('Error in class (' . __CLASS__ . '): [ ' . implode(', ', $update_handle->errorInfo()) . ' ] on line ' . __LINE__);
		
	}
	
	
       
 
 
        
        
        
        
        
        
        

        
        
        
        
        

/*
 *
 * [ getLocationId ] 
 *_____________________________________________________
 *
 *
 */

public function getLocationId()
{
	return $this->_data['location_id'];
}






/*
 *
 * [ getEntryLabel ] 
 *_____________________________________________________
 *
 *
 */

public function getEntryLabel()
{
	return $this->_data['entry_label'];
}



/*
 *
 * [ setEntryLabel ] 
 *_____________________________________________________
 *
 *
 */

public function setEntryLabel($value)
{
	$this->_data['entry_label'] = $value;
	$this->_modified[] = 'entry_label';

	return $this;
}



/*
 *
 * [ getEntryStreetAddress ] 
 *_____________________________________________________
 *
 *
 */

public function getEntryStreetAddress()
{
	return $this->_data['entry_street_address'];
}



/*
 *
 * [ setEntryStreetAddress ] 
 *_____________________________________________________
 *
 *
 */

public function setEntryStreetAddress($value)
{
	$this->_data['entry_street_address'] = $value;
	$this->_modified[] = 'entry_street_address';

	return $this;
}



/*
 *
 * [ getEntrySuburb ] 
 *_____________________________________________________
 *
 *
 */

public function getEntrySuburb()
{
	return $this->_data['entry_suburb'];
}



/*
 *
 * [ setEntrySuburb ] 
 *_____________________________________________________
 *
 *
 */

public function setEntrySuburb($value)
{
	$this->_data['entry_suburb'] = $value;
	$this->_modified[] = 'entry_suburb';

	return $this;
}



/*
 *
 * [ getEntryPostcode ] 
 *_____________________________________________________
 *
 *
 */

public function getEntryPostcode()
{
	return $this->_data['entry_postcode'];
}



/*
 *
 * [ setEntryPostcode ] 
 *_____________________________________________________
 *
 *
 */

public function setEntryPostcode($value)
{
	$this->_data['entry_postcode'] = $value;
	$this->_modified[] = 'entry_postcode';

	return $this;
}



/*
 *
 * [ getEntryCity ] 
 *_____________________________________________________
 *
 *
 */

public function getEntryCity()
{
	return $this->_data['entry_city'];
}



/*
 *
 * [ setEntryCity ] 
 *_____________________________________________________
 *
 *
 */

public function setEntryCity($value)
{
	$this->_data['entry_city'] = $value;
	$this->_modified[] = 'entry_city';

	return $this;
}



/*
 *
 * [ getEntryState ] 
 *_____________________________________________________
 *
 *
 */

public function getEntryState()
{
	return $this->getEntryProvince();
}


/*
 *
 * [ getEntryProvince ] 
 *_____________________________________________________
 *
 *
 */

public function getEntryProvince()
{
        $_Province = $this->getProvince();
	return (!empty($_Province)) ? $_Province->getPrintableName() : $this->_data['entry_state'];
}



/*
 *
 * [ setEntryState ] 
 *_____________________________________________________
 *
 *
 */

public function setEntryState($value)
{
	$this->_data['entry_state'] = $value;
	$this->_modified[] = 'entry_state';

	return $this;
}



/*
 *
 * [ getProvinceId ] 
 *_____________________________________________________
 *
 *
 */

public function getProvinceId()
{
	return $this->_data['province_id'];
}


/*
 *
 * [ getProvince ] 
 *_____________________________________________________
 *
 *
 */

public function getProvince()
{       
        if (!empty($this->_data['province_id']) && (int)$this->_data['province_id'] !== 0)
        {
            return \Cataleya\Geo\Province::load ($this->_data['province_id']);
        }
        
        else return NULL;
}






/*
 *
 * [ setProvince ] 
 *_____________________________________________________
 *
 *
 */

public function setProvince(\Cataleya\Geo\Province $_Province)
{
        $_Country = $this->getCountry();
        if ($_Country instanceof \Cataleya\Geo\Country && (!$_Country->hasProvinces() || !$_Country->hasProvinces($_Province))) $this->e->triggerException('Error in class (' . __CLASS__ . '): [ invalid argument for method: ' . __FUNCTION__ . ' ] on line ' . __LINE__);

            
	$this->_data['province_id'] = $_Province->getProvinceId();
	$this->_modified[] = 'province_id';

	return $this;
}



/*
 *
 * [ getEntryCountryCode ] 
 *_____________________________________________________
 *
 *
 */

public function getEntryCountryCode()
{
	return $this->_data['entry_country_code'];
}



/*
 *
 * [ getEntryCountry ] 
 *_____________________________________________________
 *
 *
 */

public function getEntryCountry()
{
	return $this->getCountry()->getPrintableName();
}




/*
 *
 * [ getCountry ] 
 *_____________________________________________________
 *
 *
 */

public function getCountry()
{
        if ($this->_country_obj === NULL) $this->_country_obj = \Cataleya\Geo\Country::load($this->_data['entry_country_code']);
        
        return $this->_country_obj;
}






/*
 *
 * [ setCountry ] 
 *_____________________________________________________
 *
 *
 */

public function setCountry(\Cataleya\Geo\Country $_Country)
{
        $_Province = $this->getProvince();
        if ($_Province instanceof \Cataleya\Geo\Province && $_Province->getCountryCode() !== $_Country->getCountryCode()) $this->e->triggerException('Error in class (' . __CLASS__ . '): [ invalid argument for method: ' . __FUNCTION__ . ' ] on line ' . __LINE__);

        $this->_country_obj = $_Country;
	$this->_data['entry_country_code'] = $_Country->getCountryCode();
	$this->_modified[] = 'entry_country_code';

	return $this;
}





/*
 *
 * [ setEntryCountryCode ] 
 *_____________________________________________________
 *
 *
 */

public function setEntryCountryCode($value)
{
        $this->_country_obj = NULL;
	$this->_data['entry_country_code'] = $value;
	$this->_modified[] = 'entry_country_code';

	return $this;
}



/*
 *
 * [ getLatitude ] 
 *_____________________________________________________
 *
 *
 */

public function getLatitude()
{
	return $this->_data['latitude'];
}



/*
 *
 * [ setLatitude ] 
 *_____________________________________________________
 *
 *
 */

public function setLatitude($value)
{
	$this->_data['latitude'] = $value;
	$this->_modified[] = 'latitude';

	return $this;
}



/*
 *
 * [ getLongtitude ] 
 *_____________________________________________________
 *
 *
 */

public function getLongtitude()
{
	return $this->_data['longtitude'];
}



/*
 *
 * [ setLongtitude ] 
 *_____________________________________________________
 *
 *
 */

public function setLongtitude($value)
{
	$this->_data['longtitude'] = $value;
	$this->_modified[] = 'longtitude';

	return $this;
}




        





}
	
	
	
	
