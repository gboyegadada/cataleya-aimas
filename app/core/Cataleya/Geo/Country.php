<?php


namespace Cataleya\Geo;



/*

CLASS COUNTRY

*/



class Country extends \Cataleya\Collection     
{
	
	
	private $_data = array();
	private $_modified = array();
        protected $_collection = array();
        
	private $dbh, $e;
	

	


	/*
	 *
	 * [ __construct ]
	 * ________________________________________________________________
	 *
	 *
	 *
	 */
	 
	 
	public function __construct ()
	{
		
		// Get database handle...
                $this->dbh = \Cataleya\Helper\DBH::getInstance();
		
		// Get error handler
		$this->e = \Cataleya\Helper\ErrorHandler::getInstance();
                
                parent::__construct();
            		
	}
	




	/*
	 *
	 * [ __destruct ]
	 * ________________________________________________________________
	 *
	 *
	 *
	 */

        public function __destruct()
        {
                    $this->saveData();
        }





	/*
	 *
	 * [ load ]
	 * ________________________________________________________________
	 *
	 *
	 *
	 */
	 
	 
	public static function load ($id = '')
	{
		
        if (empty($id) || preg_match('/^[A-Za-z]{2,3}$/', $id) === 0) return NULL;
        
        $id = strtoupper($id);
                
		//if ($id == 0 || filter_var($id, FILTER_VALIDATE_INT) === FALSE) return NULL;
		
		$instance = new \Cataleya\Geo\Country ();
		
		// LOAD: CURRENCY
		static $instance_select, $param_instance_id;
		
			
		if (!isset($instance_select)) {
			// PREPARE SELECT STATEMENT...
			$instance_select = $instance->dbh->prepare('SELECT * FROM countries WHERE country_code = :instance_id LIMIT 1');
			$instance_select->bindParam(':instance_id', $param_instance_id, \PDO::PARAM_STR);
		}
		
		
		$param_instance_id = $id;
		
		if (!$instance_select->execute()) $instance->e->triggerException('
												Error in class (' . __CLASS__ . '): [ ' . 
												implode(', ', $instance_select->errorInfo()) . 
												' ] on line ' . __LINE__);
		
		$instance->_data = $instance_select->fetch(\PDO::FETCH_ASSOC);
                
               $instance->loadProvinces();


		return $instance;	
		
		
	}





        /*
         *
         * [ getID ] 
         *_____________________________________________________
         *
         *
         */

        public function getID()
        {
                return $this->_data['country_code'];
        }




	
	
	public function saveData () 
	{
		

		if (empty($this->_modified)) return;
                
                $this->_modified = array_unique($this->_modified);
                
		$_update_params = array();
		$key_val_pairs = array();
		
		foreach ($this->_modified as $key) $key_val_pairs[$key] = $key . ' = :' . $key;
		
	
		// Do prepared statement for 'INSERT'...
		$update_handle = $this->dbh->prepare('
                                                        UPDATE countries 
                                                        SET ' . implode (', ', $key_val_pairs) . '     
                                                        WHERE country_code = :instance_id 
                                                        ');
		
		$_update_params['country_code'] = $this->_data['country_code'];
                $update_handle->bindParam(':instance_id', $_update_params['country_code'], \PDO::PARAM_STR);
                
		foreach ($this->_modified as $key) {
                    
                        $_update_params[$key] = $this->_data[$key];                        
			$update_handle->bindParam(':'.$key, $_update_params[$key], \Cataleya\Helper\DBH::getTypeConst($_update_params[$key]));

		}
		
		if (!$update_handle->execute()) $this->e->triggerException('Error in class (' . __CLASS__ . '): [ ' . implode(', ', $update_handle->errorInfo()) . ' ] on line ' . __LINE__);
		
	}
	
	
       
        
    

        public function current()
        {
            return \Cataleya\Geo\Province::load($this->_collection[$this->_position]['province_id']);

        }





        /*
         *
         * [ getCountryCode ] 
         *_____________________________________________________
         *
         *
         */

        public function getCountryCode()
        {
                return $this->_data['country_code'];
        }







        /*
         *
         * [ getName ] 
         *_____________________________________________________
         *
         *
         */

        public function getName()
        {
                return $this->_data['name'];
        }







        /*
         *
         * [ getPrintableName ] 
         *_____________________________________________________
         *
         *
         */

        public function getPrintableName()
        {
                return $this->_data['printable_name'];
        }




        

        /*
         *
         * [ getIso2 ] 
         *_____________________________________________________
         *
         *
         */

        public function getIso2()
        {
                return $this->_data['country_code'];
        }





        /*
         *
         * [ getIso3 ] 
         *_____________________________________________________
         *
         *
         */

        public function getIso3()
        {
                return $this->_data['iso3'];
        }







        /*
         *
         * [ getNumcode ] 
         *_____________________________________________________
         *
         *
         */

        public function getNumcode()
        {
                return $this->_data['numcode'];
        }



        /*
         * 
         * [ hasProvinces ]
         * __________________________________________________
         * 
         * 
         */


        public function hasProvinces () 
        {
            return ((int)$this->_data['has_states'] === 1);
        }



        /*
         * 
         * [ hasStates ]
         * __________________________________________________
         * 
         * Note: alias for hasProvinces()
         * 
         */


        public function hasStates () 
        {
            return ((int)$this->_data['has_states'] === 1);
        }




        /*
         * 
         * [ enableProvinces ]
         * ______________________________________________________
         * 
         * 
         */


        public function enableProvinces ($_force_reload = FALSE) 
        {
            $_reload = FALSE;
            
            if ((int)$this->_data['has_states'] === 0) 
            {
                $this->_data['has_states'] = 1;
                $this->_modified[] = 'has_states';
                
                $_reload = TRUE;
            }
            
            // Load existing provinces
            if ($_reload || $_force_reload) $this->loadProvinces();

            return TRUE;
        }





        /*
         * 
         * [ disableProvinces ]
         * ______________________________________________________
         * 
         * 
         */


        public function disableProvinces () 
        {
            if ((int)$this->_data['has_states'] === 1) 
            {
                $this->_data['has_states'] = 0;
                $this->_modified[] = 'has_states';
            }

            return TRUE;
        }
        
        
        


        /*
         * 
         * [ provincesEnabled ]
         * ______________________________________________________
         * 
         * 
         */


        public function provincesEnabled () 
        {
            return ((int)$this->_data['has_states'] === 1); 
            
        }


        
        
        
        
        

	/*
	 *
	 *  [ addProvince ]
	 * ________________________________________________________________
	 * 
	 * 
	 *
	 *
	 */

	 
	public function addProvince ($_iso_code = '', $_name = '') 
	{
		if (empty($_iso_code) || !is_string($_iso_code) || preg_match('/^[A-Za-z]{2,3}$/', $_iso_code) === 0) return NULL;
                $_name = (!empty($_name) && is_string($_name)) ? $_name : 'New Province-' . strval(rand(1111, microtime()));
                
                
                // Check for duplicate ISO
                foreach ($this->_collection as $row) 
                {
                    if ($row['iso_code'] === trim(strtoupper($_iso_code))) {return NULL; break; }
                }
		
		static 
				$instance_insert,  
                                $instance_insert_param_iso_code, 
                                $instance_insert_param_country_code, 
                                $instance_insert_param_name, 
                                $instance_insert_param_printable_name;
		
		if (empty($instance_insert))
		{
			$instance_insert = $this->dbh->prepare('
                                                        INSERT INTO provinces 
                                                        (iso_code, country_code, name, printable_name, date_added, last_modified) 
                                                        VALUES (:iso_code, :country_code, :name, :printable_name, NOW(), NOW())
                                                        ');
															
			$instance_insert->bindParam(':country_code', $instance_insert_param_country_code, \PDO::PARAM_STR);
                        $instance_insert->bindParam(':iso_code', $instance_insert_param_iso_code, \PDO::PARAM_STR);
                        $instance_insert->bindParam(':name', $instance_insert_param_name, \PDO::PARAM_STR);
                        $instance_insert->bindParam(':printable_name', $instance_insert_param_printable_name, \PDO::PARAM_STR);
		}
		
                $instance_insert_param_country_code = $this->getCountryCode();
                $instance_insert_param_iso_code = strtoupper($_iso_code);
                $instance_insert_param_printable_name = ucwords($_name);
                $instance_insert_param_name = strtoupper($_name);
		
		if (!$instance_insert->execute()) $this->e->triggerException('
										Error in class (' . __CLASS__ . '): [ ' . 
										implode(', ', $instance_insert->errorInfo()) . 
										' ] on line ' . __LINE__);
				
                $_province_id = $this->dbh->lastInsertId();
                
		// AUTO-ENABLE PROVINCES
                $this->enableProvinces(TRUE);
										
                
                // Auto load province
                return \Cataleya\Geo\Province::load($_province_id);
		
		
	}
        
        
        
        
        /*
         * 
         * [ removeProvince ]
         * ______________________________________________________________
         * 
         * 
         */
        
        public function removeProvince ($_id = 0) 
        {
            if ($_id === 0 || filter_var($_id, FILTER_VALIDATE_INT) === FALSE) return FALSE;
            
            foreach ($this->_collection as $k=>$p) 
            {
                if ((int)$p['province_id'] === (int)$_id) 
                {
                    \Cataleya\Geo\Province::load($p['province_id'])->delete();
                    unset($this->_collection[$k]);
                    return TRUE;
                }
            }
            
            return FALSE;
            
        }








        /*
         * 
         * [ loadProvinces ]
         * ________________________________________________
         * 
         * 
         */

        private function loadProvinces () 
        {

                        
                if (!$this->hasProvinces()) return FALSE;

                static $select_handle, $param_instance_id;


                if (empty($select_handle)) {
                        // PREPARE SELECT STATEMENT...
                        $select_handle = $this->dbh->prepare('SELECT province_id, iso_code FROM provinces WHERE country_code = :instance_id');
                        $select_handle->bindParam(':instance_id', $param_instance_id, \PDO::PARAM_STR);
                }


                $param_instance_id = $this->getCountryCode();

                if (!$select_handle->execute()) $this->e->triggerException('
                                                                            Error in class (' . __CLASS__ . '): [ ' . 
                                                                            implode(', ', $select_handle->errorInfo()) . 
                                                                            ' ] on line ' . __LINE__);
                
                
                // reset collection
                $this->_collection = array ();
                
                while ($row = $select_handle->fetch(\PDO::FETCH_ASSOC)) 
                {
                    $this->_collection[] = $row;
                }

                $this->pageSetup(1000, 1);


        }




}
	
	
	
	
