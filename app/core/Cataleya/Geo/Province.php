<?php


namespace Cataleya\Geo;



use \Cataleya\Helper\DBH;
use \Cataleya\Error;




class Province   
{
	
	
	private $_data = array();
	private $_modified = array();
        
	private $dbh, $e;
	

	


	/*
	 *
	 * [ __construct ]
	 * ________________________________________________________________
	 *
	 *
	 *
	 */
	 
	 
	private function __construct ()
	{
		
		// Get database handle...
                $this->dbh = \Cataleya\Helper\DBH::getInstance();
		
		// Get error handler
		$this->e = \Cataleya\Helper\ErrorHandler::getInstance();
		
	}
	




	/*
	 *
	 * [ __destruct ]
	 * ________________________________________________________________
	 *
	 *
	 *
	 */

    public function __destruct()
    {
		$this->saveData();
    }
	




	/*
	 *
	 * [ load ]
	 * ________________________________________________________________
	 *
	 *
	 *
	 */
	 
	 
	public static function load ($id = 0)
	{
		
                if ($id === 0 || filter_var($id, FILTER_VALIDATE_INT) === FALSE) return NULL;
		
		$instance = new \Cataleya\Geo\Province ();
		
		// LOAD: CURRENCY
		static $instance_select, $param_instance_id;
		
			
		if (!isset($instance_select)) {
			// PREPARE SELECT STATEMENT...
			$instance_select = $instance->dbh->prepare('SELECT * FROM provinces WHERE province_id = :instance_id LIMIT 1');
			$instance_select->bindParam(':instance_id', $param_instance_id, \PDO::PARAM_INT);
		}
		
		
		$param_instance_id = $id;
		
		if (!$instance_select->execute()) $instance->e->triggerException('
												Error in class (' . __CLASS__ . '): [ ' . 
												implode(', ', $instance_select->errorInfo()) . 
												' ] on line ' . __LINE__);
		
		$instance->_data = $instance_select->fetch(\PDO::FETCH_ASSOC);
                
               


		return $instance;	
		
		
	}
        
        
        
        
	/**
	 * loadByISO
	 *
	 * @param string $_province_iso
	 * @param string $_country_iso
	 * @return \Cataleya\Geo\Province
	 */
	public static function loadByISO ($_province_iso = 'LA', $_country_iso = 'NG')
	{
		
        $_expected = [
                'province_iso' => 'iso', 
                'country_iso' => 'iso'
        ];

        $_params = \Cataleya\Helper\Validator::params(
            $_expected, 
                
            // Supplied params (arguments)...
            [
                'province_iso' => $_province_iso,  
                'country_iso' => $_country_iso, 
            ]
        );

        foreach ($_params as $k=>$v) if ($v === false) throw new Error('Invalid argument: ' . $k . '. Expected: ' . $_expected[$k]);
		
		$instance = new static ();
		
		// LOAD: CURRENCY
		static $instance_select, $param_instance_id;
		
			
		if (!isset($instance_select)) {
			// PREPARE SELECT STATEMENT...
            $instance_select = DBH::getInstance()->prepare('' . 
                'SELECT * FROM provinces ' . 
                'WHERE country_code = :country_iso ' . 
                'AND iso_code = :province_iso  LIMIT 1'
            );
			$instance_select->bindParam(':country_iso', $param_country_iso, \PDO::PARAM_STR);
			$instance_select->bindParam(':province_iso', $param_province_iso, \PDO::PARAM_STR);
		}
		
		$param_province_iso = $_params['province_iso'];
		$param_country_iso = $_params['country_iso'];
		
		if (!$instance_select->execute()) throw new Error ('DB Error: ' . implode(', ', $instance_select->errorInfo()));

		$instance->_data = $instance_select->fetch(\PDO::FETCH_ASSOC);

        if (!empty($instance->_data)) { return $instance; }
        else { $instance = null; return null; }
		
		
	}
        
        





	
	
	public function saveData () 
	{
		

		if (empty($this->_modified)) return;
                
                $this->_modified = array_unique($this->_modified);
                
		$_update_params = array();
		$key_val_pairs = array();
		
		foreach ($this->_modified as $key) $key_val_pairs[$key] = $key . ' = :' . $key;
		
	
		// Do prepared statement for 'INSERT'...
		$update_handle = $this->dbh->prepare('
                                                        UPDATE provinces 
                                                        SET ' . implode (', ', $key_val_pairs) . ', last_modified=now()       
                                                        WHERE province_id = :instance_id 
                                                        ');
                
		$_update_params['province_id'] = $this->_data['province_id'];                
		$update_handle->bindParam(':instance_id', $_update_params['province_id'], \PDO::PARAM_STR);
                
		foreach ($this->_modified as $key) {
                    
                        $_update_params[$key] = $this->_data[$key];                        
			$update_handle->bindParam(':'.$key, $_update_params[$key], \Cataleya\Helper\DBH::getTypeConst($_update_params[$key]));

		}
		
		if (!$update_handle->execute()) $this->e->triggerException('Error in class (' . __CLASS__ . '): [ ' . implode(', ', $update_handle->errorInfo()) . ' ] on line ' . __LINE__);
		
	}
	
	
       
 
        
	 
	 /*
	 *  [ delete ]
	 * ________________________________________________________________
	 * 
	 * 
	 *
	 *
	 */

	 
	public function delete () 
	{
                        // Remove existing ties to zones
                        $zones = \Cataleya\Geo\Zones::load();
                        foreach ($zones as $zone) $zone->removeProvince($this);
		
			// DELETE 
			$instance_delete = $this->dbh->prepare('
                                                                DELETE FROM provinces 
                                                                WHERE province_id = :instance_id
                                                                ');
			$instance_delete->bindParam(':instance_id', $instance_delete_param_instance_id, \PDO::PARAM_INT);
			
			$instance_delete_param_instance_id = $this->_data['province_id'];
	
			if (!$instance_delete->execute()) $this->e->triggerException('
											Error in class (' . __CLASS__ . '): [ ' . 
											implode(', ', $instance_delete->errorInfo()) . 
											' ] on line ' . __LINE__);
		
		// $this = NULL;
		return TRUE;
		
		
	}


        
        


        /*
         *
         * [ getID ] 
         *_____________________________________________________
         *
         *
         */

        public function getID()
        {
                return $this->_data['province_id'];
        }





        /*
         *
         * [ getProvinceId ] 
         *_____________________________________________________
         * 
         * Note: Alias for getID()
         *
         */

        public function getProvinceId()
        {
                return $this->_data['province_id'];
        }





        /*
         *
         * [ getStateId ] 
         *_____________________________________________________
         *
         * Note: Alias for getID()
         *
         */

        public function getStateId()
        {
                return $this->_data['province_id'];
        }





        /*
         *
         * [ getCountryCode ] 
         *_____________________________________________________
         *
         *
         */

        public function getCountryCode()
        {
                return $this->_data['country_code'];
        }
        
        
        

        /*
         *
         * [ getCountry ] 
         *_____________________________________________________
         *
         *
         */

        public function getCountry()
        {

                if (empty($this->_country)) $this->_country = \Cataleya\Geo\Country::load($this->_data['country_code']);
                
                return $this->_country;
        }



        /*
         *
         * [ setCountryCode ] 
         *_____________________________________________________
         *
         *
         */

        public function setCountryCode($value = NULL)
        {
                if (empty($value) || preg_match('/^[A-Za-z]{2,3}$/', $value) === 0) return FALSE;

                $this->_data['country_code'] = strtoupper($value);
                $this->_modified[] = 'country_code';

                return TRUE;
        }



        /*
         *
         * [ getIsoCode ] 
         *_____________________________________________________
         *
         *
         */

        public function getIsoCode()
        {
                return $this->_data['iso_code'];
        }



        /*
         *
         * [ setIsoCode ] 
         *_____________________________________________________
         *
         *
         */

        public function setIsoCode($value = NULL)
        {
                if (empty($value) || preg_match('/^[A-Za-z]{2,3}$/', $value) === 0) return FALSE;

                $this->_data['iso_code'] = strtoupper($value);
                $this->_modified[] = 'iso_code';

                return TRUE;
        }



        /*
         *
         * [ getPrintableName ] 
         *_____________________________________________________
         *
         *
         */

        public function getPrintableName()
        {
                return $this->_data['printable_name'];
        }



        /*
         *
         * [ setPrintableName ] 
         *_____________________________________________________
         *
         *
         */

        public function setPrintableName($value = NULL)
        {
                if ($value===NULL || !is_string($value)) return FALSE;

                $this->_data['printable_name'] = ucwords($value);
                $this->_modified[] = 'printable_name';

                return TRUE;
        }



        /*
         *
         * [ getName ] 
         *_____________________________________________________
         *
         *
         */

        public function getName()
        {
                return $this->_data['name'];
        }



        /*
         *
         * [ setName ] 
         *_____________________________________________________
         *
         *
         */

        public function setName($value = NULL)
        {
                if ($value===NULL) return FALSE;

                $this->_data['name'] = strtoupper($value);
                $this->_modified[] = 'name';

                return TRUE;
        }



        /*
         *
         * [ getDateAdded ] 
         *_____________________________________________________
         *
         *
         */

        public function getDateAdded()
        {
                return $this->_data['date_added'];
        }









}
	
	
	
	
