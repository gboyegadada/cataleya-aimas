<?php


namespace Cataleya\Geo;



/*

CLASS ZONE

*/



class Zone   
{
	
	
	private $_data = array();
	private $_modified = array();
        
        private $_provinces = array ();
        private $_countries = array ();
        


        private $dbh, $e;
	

	


	/*
	 *
	 * [ __construct ]
	 * ________________________________________________________________
	 *
	 *
	 *
	 */
	 
	 
	private function __construct ()
	{
		
		// Get database handle...
                $this->dbh = \Cataleya\Helper\DBH::getInstance();
		
		// Get error handler
		$this->e = \Cataleya\Helper\ErrorHandler::getInstance();
		
	}
	




	/*
	 *
	 * [ __destruct ]
	 * ________________________________________________________________
	 *
	 *
	 *
	 */

        public function __destruct()
        {
                    $this->saveData();
        }
	




	/*
	 *
	 * [ load ]
	 * ________________________________________________________________
	 *
	 *
	 *
	 */
	 
	 
	public static function load ($id = 0)
	{
		
                if ($id == 0 || filter_var($id, FILTER_VALIDATE_INT) == FALSE) return NULL;
		
		$instance = new \Cataleya\Geo\Zone ();
		
		// LOAD: ZONE
		static $instance_select, $param_instance_id;
		
			
		if (empty($instance_select)) {
			// PREPARE SELECT STATEMENT...
			$instance_select = $instance->dbh->prepare('SELECT * FROM zones WHERE zone_id = :instance_id LIMIT 1');
			$instance_select->bindParam(':instance_id', $param_instance_id, \PDO::PARAM_INT);
		}
		
		
		$param_instance_id = $id;
		
		if (!$instance_select->execute()) $instance->e->triggerException('
												Error in class (' . __CLASS__ . '): [ ' . 
												implode(', ', $instance_select->errorInfo()) . 
												' ] on line ' . __LINE__);
		
		$instance->_data = $instance_select->fetch(\PDO::FETCH_ASSOC);
                
                $instance->loadCountries();
                $instance->loadProvinces();


		return $instance;	
		
		
	}



	/*
	 *
	 *  [ create ]
	 * ________________________________________________________________
	 * 
	 * 
	 *
	 *
	 */

	 
	static public function create ($_name = '') 
	{


                $_name = (!empty($_name) && is_string($_name)) ? $_name : 'New Zone-' . strval(rand(1111, microtime()));
                
		// Get database handle...
                $dbh = \Cataleya\Helper\DBH::getInstance();
		
		// Get error handler
		$e = \Cataleya\Helper\ErrorHandler::getInstance();
		
		// construct
		static 
				$instance_insert,  
                                $instance_insert_param_name; 
		
		if (empty($instance_insert))
		{
			$instance_insert = $dbh->prepare('
                                                        INSERT INTO zones (zone_name, date_added, last_modified) VALUES (:zone_name, NOW(), NOW())
                                                        ');
															
			$instance_insert->bindParam(':zone_name', $instance_insert_param_name, \PDO::PARAM_STR);
		}
		
                $instance_insert_param_name = $_name;
		
		if (!$instance_insert->execute()) $e->triggerException('
										Error in class (' . __CLASS__ . '): [ ' . 
										implode(', ', $instance_insert->errorInfo()) . 
										' ] on line ' . __LINE__);
										
		
		// AUTOLOAD NEW PRODUCT AND RETURN IT
		$instance = self::load($dbh->lastInsertId());
		
		return $instance;
		
		
	}


	 
	 /*
	 *  [ delete ]
	 * ________________________________________________________________
	 * 
	 * 
	 *
	 *
	 */

	 
	public function delete () 
	{
		
                        // Delete connections with provinces
			$delete_handle_1_param_instance_id = $this->_data['zone_id'];
			$delete_handle_1 = $this->dbh->prepare('
                                                                DELETE FROM zones_to_provinces 
                                                                WHERE zone_id = :instance_id
                                                                ');
			$delete_handle_1->bindParam(':instance_id', $delete_handle_1_param_instance_id, \PDO::PARAM_INT);
	
			if (!$delete_handle_1->execute()) $this->e->triggerException('
											Error in class (' . __CLASS__ . '): [ ' . 
											implode(', ', $delete_handle_1->errorInfo()) . 
											' ] on line ' . __LINE__);
                        
                        // Delete connections with countries
                        $delete_handle_2_param_instance_id = $this->_data['zone_id'];
			$delete_handle_2 = $this->dbh->prepare('
                                                                DELETE FROM zones_to_countries 
                                                                WHERE zone_id = :instance_id
                                                                ');
			$delete_handle_2->bindParam(':instance_id', $delete_handle_2_param_instance_id, \PDO::PARAM_INT);
	
			if (!$delete_handle_2->execute()) $this->e->triggerException('
											Error in class (' . __CLASS__ . '): [ ' . 
											implode(', ', $delete_handle_2->errorInfo()) . 
											' ] on line ' . __LINE__);
                        
                        
                        
                        
                        
                        // IMPORTANT NOTICE
                        // ______________________________________________________________________________________________
                        // 
                        // You will have to remove connections with [ shipping options ] and [ tax rates ] as well.
                        // The question is how.
                        //
            
                        
                        
            
			// DELETE ZONE
			$instance_delete = $this->dbh->prepare('
													DELETE FROM zones 
													WHERE zone_id = :instance_id
													');
			$instance_delete->bindParam(':instance_id', $instance_delete_param_instance_id, \PDO::PARAM_INT);
			
			$instance_delete_param_instance_id = $this->_data['zone_id'];
	
			if (!$instance_delete->execute()) $this->e->triggerException('
											Error in class (' . __CLASS__ . '): [ ' . 
											implode(', ', $instance_delete->errorInfo()) . 
											' ] on line ' . __LINE__);
                        
                        
                        
		
		// $this = NULL;
		return TRUE;
		
		
	}





        /*
         *
         * [ getID ] 
         *_____________________________________________________
         *
         *
         */

        public function getID()
        {
                return $this->_data['zone_id'];
        }




	
	
	public function saveData () 
	{
		

		if (empty($this->_modified)) return;
                $this->_modified = array_unique($this->_modified);
                
		$_update_params = array();
		$key_val_pairs = array();
		
		foreach ($this->_modified as $key) $key_val_pairs[$key] = $key . ' = :' . $key;
		
	
		// Do prepared statement for 'INSERT'...
		$info_update = $this->dbh->prepare('
                                                        UPDATE zones 
                                                        SET ' . implode (', ', $key_val_pairs) . ', last_modified=NOW()    
                                                        WHERE zone_id = :instance_id 
                                                        ');
                
		$info_update->bindParam(':instance_id', $_update_params['zone_id'], \PDO::PARAM_INT);
                $_update_params['zone_id'] = $this->_data['zone_id'];
                
		foreach ($this->_modified as $key) {
                    
                        if ($this->_data[$key] === NULL) $param_type = \PDO::PARAM_NULL;
                        else if (is_int($this->_data[$key])) $param_type = \PDO::PARAM_INT;
                        else $param_type = \PDO::PARAM_STR;
                        
			$info_update->bindParam(':'.$key, $_update_params[$key], $param_type);
                        $_update_params[$key] = $this->_data[$key];
                        
		}
		
		

		
		if (!$info_update->execute()) $this->e->triggerException('Error in class (' . __CLASS__ . '): [ ' . implode(', ', $info_update->errorInfo()) . ' ] on line ' . __LINE__);
		
	}
	
	
       
 
 
        
        
        
        
        



        /*
         * 
         * [ loadProvinces ]
         * ________________________________________________
         * 
         * 
         */

        private function loadProvinces () 
        {

                        static $select_handle, $param_instance_id;


                        if (!isset($select_handle)) {
                                // PREPARE SELECT STATEMENT...
                                $select_handle = $this->dbh->prepare('SELECT province_id FROM zones_to_provinces WHERE zone_id = :instance_id');
                                $select_handle->bindParam(':instance_id', $param_instance_id, \PDO::PARAM_STR);
                        }


                        $param_instance_id = $this->getID();

                        if (!$select_handle->execute()) $this->e->triggerException('
                                                                                                        Error in class (' . __CLASS__ . '): [ ' . 
                                                                                                        implode(', ', $select_handle->errorInfo()) . 
                                                                                                        ' ] on line ' . __LINE__);
                        
                        while ($row = $select_handle->fetch(\PDO::FETCH_ASSOC)) $this->_provinces[] = $row['province_id'];
                        
                        return $this;

        }
        

        
        
        



        /*
         * 
         * [ loadCountries ]
         * ________________________________________________
         * 
         * 
         */

        private function loadCountries () 
        {

                        static $select_handle, $param_instance_id;


                        if (!isset($select_handle)) {
                                // PREPARE SELECT STATEMENT...
                                $select_handle = $this->dbh->prepare('SELECT country_code FROM zones_to_countries WHERE zone_id = :instance_id');
                                $select_handle->bindParam(':instance_id', $param_instance_id, \PDO::PARAM_STR);
                        }


                        $param_instance_id = $this->getID();

                        if (!$select_handle->execute()) $this->e->triggerException('
                                                                                                        Error in class (' . __CLASS__ . '): [ ' . 
                                                                                                        implode(', ', $select_handle->errorInfo()) . 
                                                                                                        ' ] on line ' . __LINE__);
                        
                        
                        while ($row = $select_handle->fetch(\PDO::FETCH_ASSOC)) $this->_countries[] = $row['country_code'];
                        
                        return $this;

        }
        
        
        
        
        /*
         * 
         * public: [ getCountries ] 
         * 
         */
        
        public function getCountries () 
        {
            $countries = array();
            
            foreach ($this->_countries as $_id) $countries[] = \Cataleya\Geo\Country::load($_id);
            
            return $countries;
        
        }
        
        
        
        
        
        /*
         * 
         * public: [ getProvinces ] 
         * 
         */
        
        public function getProvinces () 
        {
            $provinces = array();
            
            foreach ($this->_provinces as $_id) $provinces[] = \Cataleya\Geo\Province::load($_id);
            
            return $provinces;
        
        }
         
        
        
       
        
        
        
        /*
         * [ hasCountry ]
         * _______________________________________________
         * 
         * RETURNS TRUE: if zone has specified country and the specified country has no provinces or sub regions
         * RETURNS FALSE OTHERWISE
         * 
         * If the specified country has provinces, you must specify a PROVINCE and use ->hasProvince($_state) OR ->hasLocation($_state)
         * 
         * 
         */
        
        
        public function hasCountry (\Cataleya\Geo\Country $_country, $_require_provinces = TRUE, $_simulate_exclude_list = TRUE) 
        {
            $_code = $_country->getCountryCode();
            $_has = (in_array($_code, $this->_countries) && (!$_country->hasProvinces() || $_require_provinces === FALSE));
            
            if ($_simulate_exclude_list === TRUE) return ($this->getListType() === 'normal') ?  $_has : !$_has;
            else return $_has;
        }
        
        
        
        
        
        
        /*
         * [ hasProvince ]
         * _______________________________________________
         * 
         * 
         */
        
        
        public function hasProvince (\Cataleya\Geo\Province $_province, $_simulate_exclude_list = TRUE) 
        {
            $_has = (in_array($_province->getID(), $this->_provinces));
            
            if ($_simulate_exclude_list === TRUE) return ($this->getListType() === 'normal') ?  $_has : !$_has;
            else return $_has;
        }
        
        
        
        
        
        
        
        
        
        /*
         * [ hasLocation ]
         * _______________________________________________
         * 
         * 
         */
        
        
        public function hasLocation ($_country_or_state = NULL, $_simulate_exclude_list = TRUE) 
        {
            // Country ...
            if ($_country_or_state instanceof \Cataleya\Geo\Country) return $this->hasCountry($_country_or_state, TRUE, $_simulate_exclude_list);
            
            // Province...
            else if ($_country_or_state instanceof \Cataleya\Geo\Province) return $this->hasProvince($_country_or_state, $_simulate_exclude_list);
            
            // Invalid argument...
            else $this->e->triggerException('Error in class (' . __CLASS__ . '): [ The 2nd argument of ->hasLocation() must be an instance of _Class_Country or an instance of _Class_Province  ] on line ' . __LINE__);
            
            
        }
        
        
        
        
        
        
        
        

	/*
	 *
	 *  [ addProvince ]
	 * ________________________________________________________________
	 * 
	 * 
	 *
	 *
	 */

	 
	public function addProvince ($_province = NULL) 
	{
                $_province = (!is_object($_province)) ? \Cataleya\Geo\Province::load($_province) : $_province;
                
		if ($_province === NULL) return FALSE;
                
                // Check if province has already been added
                if ($this->hasProvince($_province, FALSE)) return $this;
		
		static 
				$instance_insert,  
                                $instance_insert_param_zone_id, 
                                $instance_insert_param_province_id;
		
		if (empty($instance_insert))
		{
			$instance_insert = $this->dbh->prepare('
                                                        INSERT INTO zones_to_provinces (zone_id, province_id) 
                                                        VALUES (:zone_id, :province_id)
                                                        ');
															
			$instance_insert->bindParam(':zone_id', $instance_insert_param_zone_id, \PDO::PARAM_INT);
                        $instance_insert->bindParam(':province_id', $instance_insert_param_province_id, \PDO::PARAM_INT);
		}
		
                $instance_insert_param_zone_id = $this->getID();
                $instance_insert_param_province_id = $_province->getProvinceId();
		
		if (!$instance_insert->execute()) $this->e->triggerException('
										Error in class (' . __CLASS__ . '): [ ' . 
										implode(', ', $instance_insert->errorInfo()) . 
										' ] on line ' . __LINE__);
										
		
		
                if (!$this->hasCountry($_province->getCountry(), FALSE, FALSE)) $this->addCountry($_province->getCountry());
                $this->_provinces[] = $this->dbh->lastInsertId();
                
                return $this;
		
		
	}
        
        
        
        
        

	/*
	 *
	 *  [ removeProvince ]
	 * ________________________________________________________________
	 * 
	 * 
	 *
	 *
	 */

	 
	public function removeProvince ($_province = NULL) 
	{
                $_province = (!is_object($_province)) ? \Cataleya\Geo\Province::load($_province) : $_province;
                
		if ($_province === NULL) return FALSE;
                
                // Check if province has been previously added
                if (!$this->hasProvince($_province, FALSE)) return $this;
		
		static 
				$delete_handle,  
                                $delete_handle_param_instance_id;
                
                
               $delete_handle_param_instance_id = $_province->getID();
		
		if (empty($delete_handle))
		{
                    $delete_handle = $this->dbh->prepare('
                                                            DELETE FROM zones_to_provinces 
                                                            WHERE province_id = :instance_id
                                                            ');
                    $delete_handle->bindParam(':instance_id', $delete_handle_param_instance_id, \PDO::PARAM_INT);

                }
                

                if (!$delete_handle->execute()) $this->e->triggerException('
                                                                                Error in class (' . __CLASS__ . '): [ ' . 
                                                                                implode(', ', $delete_handle->errorInfo()) . 
                                                                                ' ] on line ' . __LINE__);
		
                // Remove id from cache...
                foreach ($this->_provinces as $k=>$p) 
                {
                    if ((int)$p === (int)$delete_handle_param_instance_id) 
                    {
                        unset($this->_provinces[$k]);
                        break;
                    }
                }
                
                return $this;
		
		
	}
        
        
        

	/*
	 *
	 *  [ addCountry ]
	 * ________________________________________________________________
	 * 
	 * 
	 *
	 *
	 */

	 
	public function addCountry ($_country = NULL) 
	{
                $_country = (!is_object($_country)) ? \Cataleya\Geo\Country::load($_country) : $_country;
                
		if ($_country === NULL) return FALSE;
                
                // Check if country has already been added
                if ($this->hasCountry($_country, FALSE, FALSE)) return $this;
		
                
		static 
				$instance_insert,  
                                $instance_insert_param_zone_id, 
                                $instance_insert_param_country_code;
		
		if (empty($instance_insert))
		{
			$instance_insert = $this->dbh->prepare('
                                                        INSERT INTO zones_to_countries (zone_id, country_code) 
                                                        VALUES (:zone_id, :country_code)
                                                        ');
															
			$instance_insert->bindParam(':zone_id', $instance_insert_param_zone_id, \PDO::PARAM_INT);
                        $instance_insert->bindParam(':country_code', $instance_insert_param_country_code, \PDO::PARAM_STR);
		}
		
                $instance_insert_param_zone_id = $this->getID();
                $instance_insert_param_country_code = $_country->getCountryCode();
		
		if (!$instance_insert->execute()) $this->e->triggerException('
										Error in class (' . __CLASS__ . '): [ ' . 
										implode(', ', $instance_insert->errorInfo()) . 
										' ] on line ' . __LINE__);

		
                $this->_countries[] = $instance_insert_param_country_code;
                
                return $this;
		
		
	}
        
        
        
        
        
        
	/*
	 *
	 *  [ removeProvince ]
	 * ________________________________________________________________
	 * 
	 * 
	 *
	 *
	 */

	 
	public function removeCountry ($_country = NULL) 
	{
                $_country = (!is_object($_country)) ? \Cataleya\Geo\Country::load($_country) : $_country;
                
		if ($_country === NULL) return FALSE;
                
                // Check if country has been previously added
                if (!$this->hasCountry($_country, FALSE, FALSE)) return $this;
                
                
                // Remove provinces first...
                foreach ($_country as $_province) $this->removeProvince($_province);
                
                
		// Delete zone/country connection
		static 
				$delete_handle,  
                                $delete_handle_param_instance_id, 
                                $delete_handle_param_zone_id;
                
                
               $delete_handle_param_instance_id = $_country->getID();
               $delete_handle_param_zone_id = $this->getID();
		
		if (empty($delete_handle))
		{
                    $delete_handle = $this->dbh->prepare('
                                                            DELETE FROM zones_to_countries 
                                                            WHERE country_code = :instance_id 
                                                            AND zone_id = :zone_id 
                                                            ');
                    $delete_handle->bindParam(':instance_id', $delete_handle_param_instance_id, \PDO::PARAM_STR);
                    $delete_handle->bindParam(':zone_id', $delete_handle_param_zone_id, \PDO::PARAM_INT);

                }
                

                if (!$delete_handle->execute()) $this->e->triggerException('
                                                                                Error in class (' . __CLASS__ . '): [ ' . 
                                                                                implode(', ', $delete_handle->errorInfo()) . 
                                                                                ' ] on line ' . __LINE__);
		
                // Remove id from cache...
                foreach ($this->_countries as $k=>$p) 
                {
                    if ((int)$p === (int)$delete_handle_param_instance_id) 
                    {
                        unset($this->_countries[$k]);
                        break;
                    }
                }
                
                return $this;
		
		
	}
        
        
        
        
        

        /*
         *
         * [ getListType ] 
         *_____________________________________________________
         *
         *
         */

        public function getListType()
        {
                return $this->_data['list_type'];
        }



        /*
         *
         * [ setListType ] 
         *_____________________________________________________
         *
         *
         */

        public function setListType($value = NULL)
        {
                $_enum = array('exclude', 'normal'); // as in table column...
                if ($value===NULL || !in_array(strtolower($value), $_enum)) $this->e->triggerException('Error in class (' . __CLASS__ . '): [ Invalid argument for ->setListType() ] on line ' . __LINE__);

                $this->_data['list_type'] = strtolower($value);
                $this->_modified[] = 'list_type';

                return $this;
        }


        

        /*
         *
         * [ getZoneId ] 
         *_____________________________________________________
         *
         * Alias for ->getID()
         *
         */

        public function getZoneId()
        {
                return $this->_data['zone_id'];
        }






        /*
         *
         * [ getZoneName ] 
         *_____________________________________________________
         *
         *
         */

        public function getZoneName()
        {
                return $this->_data['zone_name'];
        }



        /*
         *
         * [ setZoneName ] 
         *_____________________________________________________
         *
         *
         */

        public function setZoneName($value = NULL)
        {
                if ($value===NULL || !is_string($value)) return FALSE;

                $this->_data['zone_name'] = $value;
                $this->_modified[] = 'zone_name';

                return $this;
        }





        /*
         *
         * [ getDateAdded ] 
         *_____________________________________________________
         *
         *
         */

        public function getDateAdded()
        {
                return $this->_data['date_added'];
        }






        /*
         *
         * [ getLastModified ] 
         *_____________________________________________________
         *
         *
         */

        public function getLastModified()
        {
                return $this->_data['last_modified'];
        }

        
        
        /*
         * 
         * [ getProvincePopulation ]
         * _____________________________________________________
         * 
         * 
         */
        
        
        public function getProvincePopulation () 
        {
            return count($this->_provinces);
        }

        
        
        /*
         * 
         * [ getCountryPopulation ]
         * _____________________________________________________
         * 
         * 
         */
        
        
        public function getCountryPopulation () 
        {
            return count($this->_countries);
        }








}
	
	
	
	
