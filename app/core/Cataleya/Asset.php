<?php


namespace Cataleya;




/*
 *
 *	@Package: Cataleya
 *	@Interface: \Cataleya\Asset
 *
 *	(c) 2012 Fancy Paper Planes
 *
 *
 *
 */


interface Asset  

{
	
	/*
	 *
	 * [ load ]
	 * ________________________________________________________________
	 * 
	 * Load instance of object from existing data (database)
	 *
	 *
	 *
	 */
	 
	static public function load ($load_id);





	/*
	 *
	 * [ create ]
	 * ________________________________________________________________
	 * 
	 * Create new attribute
	 *
	 *
	 */
	 
	static public function create ();





	/*
	 * [ getID ]
	 * ________________________________________________________________
	 * 
	 * Get id of object (database table row ID)
	 *
	 *
	 */
	 
	public function getID ();




	/*
	 *
	 * [ getClassName ]
	 * ________________________________________________________________
	 * 
	 * Get object class name
	 *
	 *
	 */
	 
	public function getClassName ();



}