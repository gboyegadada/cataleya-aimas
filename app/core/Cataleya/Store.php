<?php


namespace Cataleya;



/*

CLASS STORE

*/



class Store implements \Cataleya\System\Event\Observable
{
	
	

    private $_data = array();
	
	private 
				$_description, 
				$_display_picture, 
				$_currency, 
				$_url_rewrite, 
                                $_default_contact,
                                $_contacts, 
                                $_notification_new_order, 
                                $_notification_unconfirmed_delivery, 
                                $_shop_landing;
				

	private $_modified = array();
    private $_payment_types = array (); // payment_type IDs only!!

    protected $_root_cat_id = NULL;


    private static $_label_colors = array (
        'blue' => 'Blue', 
        'red' => 'Red', 
        'grey' => 'Grey', 
        'green' => 'Green', 
        'yellow' => 'Yellow'
    );


    protected $dbh, $e;




    protected 
            $_table = 'products_to_stores', 
            $_pk = 'store_id';


	
    protected static $_events = [
            'store/created',
            'store/updated', 
            'store/deleted', 
        
            'store/opened', 
            'store/closed'
        ];



	/*
	 *
	 * [ __construct ]
	 * ________________________________________________________________
	 *
	 *
	 *
	 */
	 
	 
	public function __construct ()
	{
		
		// Get database handle...
                $this->dbh = \Cataleya\Helper\DBH::getInstance();
		
		// Get error handler
		$this->e = \Cataleya\Helper\ErrorHandler::getInstance();
		
	}
	




	/*
	 *
	 * [ __destruct ]
	 * ________________________________________________________________
	 *
	 *
	 *
	 */

    public function __destruct()
    {
		$this->saveData();
    }
	




    /**
     *
     * Store Contructor. Always use instead of 'new' !!
     *
     * @params integer $id Store ID
     * @param array $filters An array of objects that will be passed to 
     * \Cataleya\Catalog when loading products in this STORE. Accepted objects are [Sale, Coupon, Tag, Attribute]
     * @param array $sort 
     * @param integer $page_size For pagination of products.
     * @param integer $page_num For pagination of products.
     * 
     * @return \Cataleya\Store (an instance of store)
     *
     *
     */
    public static function load ($id = 0)
    {

            if ($id == 0 || filter_var($id, FILTER_VALIDATE_INT) === FALSE) return NULL;

            $_objct = __CLASS__;
            $instance = new $_objct;

            // LOAD: STORE INFO
            static $instance_select, $param_id;


            if (!isset($instance_select)) {
                    // PREPARE SELECT STATEMENT...
                    $instance_select = $instance->dbh->prepare('SELECT * FROM stores WHERE store_id = :store_id LIMIT 1');
                    $instance_select->bindParam(':store_id', $param_id, \PDO::PARAM_INT);
            }


            $param_id = $id;

            if (!$instance_select->execute()) $instance->e->triggerException('
                                                                                            Error in class (' . __CLASS__ . '): [ ' . 
                                                                                            implode(', ', $instance_select->errorInfo()) . 
                                                                                            ' ] on line ' . __LINE__);

            $instance->_data = $instance_select->fetch(\PDO::FETCH_ASSOC);
            if (empty($instance->_data))
            {
                unset ($instance);
                return NULL;
            }



            // LOAD: DESCRIPTION
            $instance->_description = \Cataleya\Asset\Description::load($instance->_data['description_id']);	

            // LOAD: DISPLAY PICTURE
            $instance->_display_picture = \Cataleya\Asset\Image::load($instance->_data['image_id']);




            return $instance;	


    }



    /**
     * 
     * @param \Cataleya\Asset\Contact $_Contact
     * @param string $description_title
     * @param string $description_text
     * @param string $language_code
     * @param string $currency_code
     * @return \Cataleya\Store
     * 
     */
    static public function create (\Cataleya\Asset\Contact $_Contact, $description_title = '', $description_text = '', $language_code = 'EN', $currency_code = 'NGN') 
    {



            // //// BEGIN TRANSACTION /////
            \Cataleya\Helper\DBH::getInstance()->beginTransaction();


            /*
             * Handle description Attribute 
             *
             */

             $description = \Cataleya\Asset\Description::create($description_title, $description_text, $language_code);


            /*
             * Handle url_rewrite Attribute 
             *
             */

             $url_rewrite = \Cataleya\Asset\URLRewrite::create(NULL, $description_title, 'index.php', 0);





             /*
              * 
              * Notification Profile: New Orders
              * 
              */


            $_new_order_profile = \Cataleya\Admin\Notification::create(array(
                'keyword'   => \Cataleya\Helper::uid(), 
                'scope'   => 'order/created', 
                'language_code' =>  'EN', 
                'title' =>  'New Orders (' . $description_title . ')', 
                'description'   =>  'You have new orders (at ' . $description_title . ').', 
                'entity_name'   =>  'New Order', 
                'entity_name_plural'    =>  'New Orders', 
                'send_after_period'   =>  1, 
                'send_after_interval'   =>  'hour', 
                'send_after_increments'   =>  10, 
                'notification_text' =>  'waiting to be processed.',  // "1 new order waiting to be processed."
                'icon'   =>  'basket-50x50.png', 
                'request_uri'   =>  BASE_URL.'landing.orders.php?show=pending', 

            ));



             /*
              * 
              * Notification Profile: Confirm Delivery
              * 
              */


            $_delivery_reminder_profile = \Cataleya\Admin\Notification::create(array(
                'keyword'   =>  \Cataleya\Helper::uid(),  
                'scope'   => 'delivery/unconfirmed', 
                'language_code' =>  'EN', 
                'title' =>  'Unconfirmed Deliveries (' . $description_title . ')', 
                'description'   =>  'You have unconfirmed deliveries (at ' . $description_title . ').', 
                'entity_name'   =>  'Unconfirmed Delivery', 
                'entity_name_plural'    =>  'Unconfirmed Deliveries', 
                'send_after_period'   =>  1, 
                'send_after_interval'   =>  'hour', 
                'send_after_increments'   =>  10, 
                'notification_text' =>  'waiting to be processed.',  // "1 Unconfirmed delivery waiting to be processed."
                'icon'   =>  'flight-50x50.png', 
                'request_uri'   =>  BASE_URL.'landing.orders.php?show=shipped', 

            ));



            // Get database handle...
            $dbh = \Cataleya\Helper\DBH::getInstance();

            // Get error handler
            $e = \Cataleya\Helper\ErrorHandler::getInstance();

            // construct
            static 
                            $store_insert, 
                            $store_insert_param_description_id, 
                            $store_insert_param_language_code, 
                            $store_insert_param_currency_code, 
                            $store_insert_param_contact_id, 
                            $store_insert_param_url_rewrite_id, 

                            $store_insert_param_order_alert_id, 
                            $store_insert_param_delivery_alert_id;



            $store_insert_param_description_id = $description->getID();
            $store_insert_param_url_rewrite_id = $url_rewrite->getID();
            $store_insert_param_language_code = $language_code;
            $store_insert_param_currency_code = $currency_code;
            $store_insert_param_contact_id = $_Contact->getID();

            $store_insert_param_order_alert_id = $_new_order_profile->getKeyword();
            $store_insert_param_delivery_alert_id = $_delivery_reminder_profile->getKeyword();


            if (empty($store_insert))
            {
                    $store_insert = $dbh->prepare('
                                                    INSERT INTO stores 
                                                    (description_id, language_code, currency_code, default_contact_id, url_rewrite_id, notification_profile_new_order, notification_profile_confirm_delivery, date_added, date_modified, active, hours, telephone, email) 
                                                    VALUES (:description_id, :language_code, :currency_code, :contact_id, :url_rewrite_id, :notification_profile_new_order, :notification_profile_confirm_delivery, NOW(), NOW(), 0, "8AM to 5PM", "07010000000", "example@email.com")
                                                    ');

                    $store_insert->bindParam(':description_id', $store_insert_param_description_id, \PDO::PARAM_INT);
                    $store_insert->bindParam(':url_rewrite_id', $store_insert_param_url_rewrite_id, \PDO::PARAM_INT);
                    $store_insert->bindParam(':contact_id', $store_insert_param_contact_id, \PDO::PARAM_INT);

                    $store_insert->bindParam(':notification_profile_new_order', $store_insert_param_order_alert_id, \PDO::PARAM_STR);
                    $store_insert->bindParam(':notification_profile_confirm_delivery', $store_insert_param_delivery_alert_id, \PDO::PARAM_STR);

                    $store_insert->bindParam(':language_code', $store_insert_param_language_code, \PDO::PARAM_STR);
                    $store_insert->bindParam(':currency_code', $store_insert_param_currency_code, \PDO::PARAM_STR);
            }


            if (!$store_insert->execute()) $e->triggerException('
                                                                            Error in class (' . __CLASS__ . '): [ ' . 
                                                                            implode(', ', $store_insert->errorInfo()) . 
                                                                            ' ] on line ' . __LINE__);


            // AUTOLOAD NEW PRODUCT AND RETURN IT
            $store = self::load($dbh->lastInsertId());

            // Handle contact
            $store->addContact($_Contact);
            
            
            // Home Page
            $_DefaultPage = \Cataleya\Front\Shop\Page::create($store, 'Home Page', 'Welcome...', $store->getLanguageCode(), TRUE);
            
            $store->setDefaultPage($_DefaultPage);
            
            
            // Create ROOT CATEGORY...
            $_RootCategory = \Cataleya\Catalog\Tag::create($store, $description_title, 'Root category for '.$description_title, $language_code);
            $store->_root_cat_id = $_RootCategory->getID();

            // //// COMMIT TRANSACTION /////
            \Cataleya\Helper\DBH::getInstance()->commit();


            \Cataleya\System\Event::notify('store/created', $this, array ());
            return $store;


    }



    /**
     * getEvents
     * 
     * @return array
     */
    public static function getEvents() {

        return self::$_events;
    }




    /*
     *
     * [ getID ] 
     *_____________________________________________________
     *
     *
     */

    public function getID()
    {
            return $this->_data['store_id'];
    }
    
    
    
    
    

    

        


    /**
     *
     * PAYMENT TYPES REBOOT
     * ------------------------------------------------------------
     * New Plugin Based implementation of Payment Types. More like 
     * Accepted Payment Types.
     * 
     *
     * USAGE
     * -----------------------------------------------------------
     *
     * On Shop Settings page in the "Accepted Payment Types" section, the 
     * list on there is generated by calling \Cataleya\Plugins\Event::trigger('payment.payment-options');
     * An array of payment plugins will be returned like this:
     *
     * array (
     *
     *          [ 
     *              'name' => 'Naira Master Card', 
     *              'plugin_handle' => 'webpay-plugin'
     *          ],
     *          [
     *              'name' => 'PayPal Express', 
     *              'plugin_handle' => 'paypal-express-plugin'
     *          ]
     *       );
     *
     * Then you go ahead and check what is or isn't accepted at the Shop by doing: 
     *
     *      if ( $Store->acceptsPaymentType($_plugin_handle) ) {
     *
     *          // do something...
     *
     *      }
     *
     *
     * ------------------- THE END: Credits Roll ------------------------------
     *
     */






    /**
     * getPaymentProcessors
     *
     * @return array
     */
    public function getPaymentProcessors() 
    {
        return  explode(' ', $this->_data['payment_processors']);
    }




    /**
     * addPaymentProcessor
     *
     * @param string $_plugin_handle   You are allowed to specified more than one plugin key here.
     *                                 Just do like this: $Store->doNotAcceptPaymentType('key1', 'key2' ...);
     *
     * @return \Cataleya\Store
     */
    public function addPaymentProcessor($_plugin_handle) 
    {

        $_args = func_get_args(); 
        $_keys = $this->getPaymentProcessors();


        foreach ($_args as $_key) {
            $_key = strtolower(trim($_key));
            if (!\Cataleya\Plugins\Package::exists($_key)) continue;

            $_keys[] = $_key;
        }

        $this->_data['payment_processors'] = implode(' ', array_unique($_keys));
        $this->_modified[] = 'payment_processors';

        return $this;
    }



    /**
     * removePaymentProcessor
     *
     * @param string $_plugin_handle   You are allowed to specified more than one plugin key here.
     *                                 Just do like this: $Store->doNotAcceptPaymentType('key1', 'key2' ...);
     *
     * @return \Cataleya\Store
     */
    public function removePaymentProcessor($_plugin_handle) 
    {
        $_to_remove = func_get_args(); 
        $_keys = $this->getPaymentProcessors();

        $this->_data['payment_processors'] = implode(' ', array_diff($_keys, $_to_remove));
        $this->_modified[] = 'payment_processors';

        return $this;
    }


    /**
     * hasPaymentProcessor
     *
     * @param string $_plugin_handle
     * @return bool
     */
    public function hasPaymentProcessor ($_plugin_handle) 
    {

        return in_array($_plugin_handle, $this->getPaymentProcessors());

    }
        



    /**
     * getPaymentOptions
     *
     * @return array
     */
    public function getPaymentOptions() 
    {

        $_list = [];
        $_payment_options = \Cataleya\Plugins\Action::trigger(
            'payment.get-options', 
            [ 'params' => [ 'currency_codes' => [ $this->getCurrencyCode() ] ] ], 
            $this->getPaymentProcessors()
        );

        if (!empty($_payment_options)) {
            foreach ($_payment_options as $_payment_opt) 
            {
                if (isset($_payment_opt['active']) && $_payment_opt['active'] === false) continue;

                $_list[] = $_payment_opt;
            }
        }
        
        

        return $_list;

    }






	
	/**
	 * saveData
	 *
	 * @return void
	 */
	public function saveData () 
	{
		

		if (empty($this->_modified)) return;
                
                $this->_modified = array_unique($this->_modified);
                
		$_update_params = array();
		$key_val_pairs = array();
		
		foreach ($this->_modified as $key) $key_val_pairs[$key] = $key . ' = :' . $key;
		
	
		// Do prepared statement for 'INSERT'...
		$update_handle = $this->dbh->prepare('
                                                        UPDATE stores 
                                                        SET date_modified = NOW(), ' . implode (', ', $key_val_pairs) . '      
                                                        WHERE store_id = :store_id 
                                                        ');
                
		$_update_params['store_id'] = $this->_data['store_id'];                
		$update_handle->bindParam(':store_id', $_update_params['store_id'], \PDO::PARAM_INT);
                
		foreach ($this->_modified as $key) {
                    
                        $_update_params[$key] = $this->_data[$key];                        
			$update_handle->bindParam(':'.$key, $_update_params[$key], \Cataleya\Helper\DBH::getTypeConst($_update_params[$key]));

		}
		
		if (!$update_handle->execute()) $this->e->triggerException('Error in class (' . __CLASS__ . '): [ ' . implode(', ', $update_handle->errorInfo()) . ' ] on line ' . __LINE__);
		
         \Cataleya\System\Event::notify('store/update', $this, array ());
	}
	
	
       
 
        
        

	/*
	 *
	 *  [ delete ]
	 * ________________________________________________________________
	 * 
	 * 
	 *
	 *
	 *
	 */
	 
	public function delete () 
	{
                        
                        
                        
                        // Delete SALES
                        $_Sales = \Cataleya\Sales\Sales::load($this);
                        foreach ($_Sales as $_Sale) $_Sale->delete();
                        
                        // Delete COUPONS
                        $_Coupons = \Cataleya\Sales\Coupons::load($this);
                        foreach ($_Coupons as $_Coupon) $_Coupon->delete();
                        
                        $this->getNewOrderNotification()->delete();
                        $this->getUnconfirmedDeliveryNotice()->delete();
                        
                        $this->_notification_unconfirmed_delivery = NULL;
                        $this->_notification_new_order = NULL;
                        
                        
                        // //// BEGIN TRANSACTION /////
                        // \Cataleya\Helper\DBH::getInstance()->beginTransaction();
                        
                        // Set 'store_id' fk column in any specified tables to NULL
                        \Cataleya\Helper\DBH::unhook(array('orders'), 'store_id', $this->getID());
                        
                        

                        
                        // First get contacts...
                        $this->getContacts();
                        
                        // Delete rows containing 'store_id' in specified tables
                        \Cataleya\Helper\DBH::sanitize(array('store_contacts', 'stores'), 'store_id', $this->getID());
            
											
			
			// CLEAN UP...
                        if (!empty($this->_description)) $this->_description->delete();                                               
                        foreach ($this->_contacts as $_Contact)
                        {
                           $_Contact->delete();
                        }
                        
                        
                        // //// COMMIT TRANSACTION /////
                        // \Cataleya\Helper\DBH::getInstance()->commit();

                        
 
        \Cataleya\System\Event::notify('store/deleted', null, array ('id' => $this->getID()));
		
		// $this = NULL;
		return TRUE;
		
		
	}
        
        
        
        
        
        
        
        
        
        
        
        
        
        


	/*
	 *
	 *  [ setTheme ]
	 * ________________________________________________________________
	 * 
	 * 
	 *
	 *
	 *
	 */
	 

	public function setTheme (\Cataleya\Front\Shop\Theme $_Theme)
	{
		if (!in_array('theme_id', $this->_modified)) $this->_modified[] = 'theme_id';
                
		$this->_data['theme_id'] = $_Theme->getID();

 
		
		return $this;              
                
		
	}





	/*
	 *
	 *  [ resetTheme ]
	 * ________________________________________________________________
	 * 
	 * 
	 *
	 *
	 *
	 */
	 

	public function resetTheme ()
	{
		if (!in_array('theme_id', $this->_modified)) $this->_modified[] = 'theme_id';
                
		$this->_data['theme_id'] = 'default';

 
		
		return $this;  
	}







        /**
         *
         * @return \Cataleya\Front\Shop\Theme
         *
         *
         */
	public function getTheme ()
	{

		
		return \Cataleya\Front\Shop\Theme::load($this->_data['theme_id']);
		
	}




        /*
         *
         * [ getThemeId ] 
         *_____________________________________________________
         *
         *
         */

        public function getThemeID()
        {
                if (empty($this->_data['theme_id'])) $this->resetTheme ();
                return $this->_data['theme_id'];
        }

        
        
        
        
        
        
        
        
        
        
        
        
        





        /**
         *
         * @param \Cataleya\Front\Shop\Page $_Page
         * @return \Cataleya\Store
         *
         */
	public function setDefaultPage (\Cataleya\Front\Shop\Page $_Page)
	{
		if (!in_array('default_page_id', $this->_modified)) $this->_modified[] = 'default_page_id';
                
		$this->_data['default_page_id'] = $_Page->getID();

 
		
		return $this;              
                
		
	}









        /**
         *
         * @return \Cataleya\Front\Shop\Page
         *
         *
         */
	public function getDefaultPage ()
	{

		
		return \Cataleya\Front\Shop\Page::load($this->getDefaultPageID());
		
	}




        /*
         *
         * [ getDefaultPageId ] 
         *_____________________________________________________
         *
         *
         */

        public function getDefaultPageID()
        {
                
                return $this->_data['default_page_id'];
        }

        
        
        
        
        
        


	/*
	 *
         * @param string Page keyword 
	 * @return \Cataleya\Front\Shop\Page 
	 *
	 */
	public function getCMSPage ($slug)
	{

		if (!is_string($slug)) return NULL;
                
		return \Cataleya\Front\Shop\Page::load($slug, $this);
                
		
	}


	/*
	 *
	 * @return \Cataleya\Front\Shop\Pages 
	 *
	 */
	public function getCMSPages ()
	{

		return \Cataleya\Front\Shop\Pages::load($this);
		
	}

        
        



	/*
	 *
	 *  [ setDisplayPicture ]
	 * ________________________________________________________________
	 * 
	 * 
	 *
	 *
	 *
	 */
	 

	public function setDisplayPicture (\Cataleya\Asset\Image $image)
	{
		if (!in_array('image_id', $this->_modified)) $this->_modified[] = 'image_id';
                
                $this->getDisplayPicture();
		$this->_data['image_id'] = $image->getID();

                // Remove old image
		if ($this->_display_picture->getID() != 0) 
                {
                    $this->saveData();
                    $this->_display_picture->destroy();
                }		
                
		$this->_display_picture = $image; 
                
                
                
 
		
		return TRUE;              
                
		
	}





	/*
	 *
	 *  [ unsetDisplayPicture ]
	 * ________________________________________________________________
	 * 
	 * 
	 *
	 *
	 *
	 */
	 

	public function unsetDisplayPicture ()
	{
		$this->_data['image_id'] = NULL;
		
		
		if (!in_array('image_id', $this->_modified)) $this->_modified[] = 'image_id';
                $this->saveData();
                $this->_display_picture->destroy();
		$this->_display_picture = NULL;
	}





	/*
	 *
	 *  [ getDisplayPicture ]
	 * ________________________________________________________________
	 * 
	 * Returns image object
	 *
	 *
	 *
	 */
	 

	public function getDisplayPicture ()
	{
		if (empty($this->_display_picture) )
		{
			$this->_display_picture = \Cataleya\Asset\Image::load($this->_data['image_id']);
		}
		
		return $this->_display_picture;
		
	}




        /*
         *
         * [ getImageId ] 
         *_____________________________________________________
         *
         *
         */

        public function getImageId()
        {
                return $this->_data['image_id'];
        }

        
        
        
        
        




        

	/**
	 *
	 *  
	 * Will return store description as object
	 * 
         * @return \Cataleya\Asset\Description Description object..
	 *
	 */
	public function getDescription () 
	{
		
		return $this->_description;
		
	}
        
        
        
        
        
  

	/**
	 *
         * Alias for child object [Description] 's setText method.
         * 
	 * @param string $_text Description text...
         * @param string $_language_code (optional)
	 *
	 *
	 */
	public function setDescription ($_text = NULL, $_language_code = NULL) 
	{
		$_language_code = (empty($_language_code)) 
                                    ? $this->getLanguageCode() 
                                    : $_language_code;
		
                return $this->_description->setText($_text, $_language_code);
		
	}
        
        
        
        


	/**
	 *
         * Alias for child object [Description] 's setTitle method.
         * 
	 * @param string $_text Title text...
         * @param string $_language_code (optional)
	 *
	 *
	 */
	public function setTitle ($_text = NULL, $_language_code = NULL) 
	{
		$_language_code = (empty($_language_code)) 
                                    ? $this->getLanguageCode() 
                                    : $_language_code;
		return $this->_description->setTitle($_text, $_language_code);
		
	}
        
        
        
        
        
        


	/**
	 *
         * Alias for child object [Description] 's getText method.
         * 
	 * @param string  $_language_code
         * @return string Store title.
	 *
	 *
	 */
	public function getDescriptionText ($_language_code = NULL) 
	{
		$_language_code = (empty($_language_code)) 
                                    ? $this->getLanguageCode() 
                                    : $_language_code;
		
                return $this->_description->getText($_language_code);
		
	}
        
        
        
        


	/**
	 *
         * Alias for child object [Description] 's getTitle method.
         * 
	 * @param string  $_language_code
         * @return string Store description.
	 *
	 */
	public function getTitleText ($_language_code = NULL) 
	{
		$_language_code = (empty($_language_code)) 
                                    ? $this->getLanguageCode() 
                                    : $_language_code;
                
		return $this->_description->getTitle($_language_code);
		
	}
        
        
    

	public function getName ($_language_code = NULL) 
	{

		return $this->getTitleText ($_language_code);
		
	}
        
        
        
        
        

	

	/**
	 *
	 *  
	 * Will return New Order Notification (object)
	 * 
         * @return \Cataleya\Admin\Notification Notification object..
	 *
	 */
	public function getNewOrderNotification ()
	{
		if (empty($this->_notification_new_order) )
		{
			$this->_notification_new_order = \Cataleya\Admin\Notification::load($this->_data['notification_profile_new_order']);
		}
		
		return $this->_notification_new_order;
		
	}
        
        
        
        
	

	/**
	 *
	 *  
	 * Will return Unconfirmed Delivery Notice (object)
	 * 
         * @return \Cataleya\Admin\Notification Notification object..
	 *
	 */
	public function getUnconfirmedDeliveryNotice ()
	{
		if (empty($this->_notification_unconfirmed_delivery) )
		{
			$this->_notification_unconfirmed_delivery = \Cataleya\Admin\Notification::load($this->_data['notification_profile_confirm_delivery']);
		}
		
		return $this->_notification_unconfirmed_delivery;
		
	}
        
        
        
        






    /**
     *
     *  
     * Will return store ID
     * 
     * @return integer
     *
     */
    public function getStoreId()
    {
            return $this->_data['store_id'];
    }

    
    

  

    /**
     *
     *  
     * Will return shop landing url (full url)
     * 
     * @return string
     *
     */
    public function getShopLanding()
    {
            if (empty($this->_shop_landing)) {
                $this->_shop_landing = SHOP_ROOT . $this->getURLRewrite()->getKeyword() . '/';
            }
            
            return $this->_shop_landing;
    }




    /*
     *
     * [ getLabelColor ] 
     *_____________________________________________________
     *
     *
     */

    public function getLabelColor()
    {
            return $this->_data['label_color'];
    }

    /*
     *
     * [ setHours ] 
     *_____________________________________________________
     *
     *
     */

    public function setLabelColor($value='blue')
    {
        
            if (!array_key_exists($value, self::$_label_colors) ) return FALSE;
            
            $this->_data['label_color'] = $value;
            $this->_modified[] = 'label_color';

            return TRUE;
    }
    
    
    
    
    /**
     * 
     * @return array
     * 
     */
    public static function getLabelColors()
    {
        
            return self::$_label_colors;
    }

    


    /*
     *
     * [ getLanguageCode ] 
     *_____________________________________________________
     *
     *
     */

    public function getLanguageCode()
    {
            return $this->_data['language_code'];
    }

    /*
     *
     * [ setLanguageCode ] 
     *_____________________________________________________
     *
     *
     */

    public function setLanguageCode($value)
    {
            $this->_data['language_code'] = $value;
            $this->_modified[] = 'language_code';

            return TRUE;
    }



    /*
     *
     * [ getCurrencyCode ] 
     *_____________________________________________________
     *
     *
     */

    public function getCurrencyCode()
    {
            return $this->_data['currency_code'];
    }



    /*
     *
     * [ getCurrency ] 
     *_____________________________________________________
     *
     *
     */

    public function getCurrency()
    {

            if (empty($this->_currency)) $this->_currency = \Cataleya\Locale\Currency::load($this->_data['currency_code']);
            return $this->_currency;
    }




    /*
     *
     * [ setCurrencyCode ] 
     *_____________________________________________________
     *
     *
     */

    public function setCurrencyCode($value)
    {
            $this->_data['currency_code'] = $value;
            $this->_modified[] = 'currency_code';

            return TRUE;
    }







    /*
     *
     * [ getHours ] 
     *_____________________________________________________
     *
     *
     */

    public function getHours()
    {
            return $this->_data['hours'];
    }

    /*
     *
     * [ setHours ] 
     *_____________________________________________________
     *
     *
     */

    public function setHours($value)
    {
            $this->_data['hours'] = $value;
            $this->_modified[] = 'hours';

            return TRUE;
    }






    /*
     *
     * [ getTelephone ] 
     *_____________________________________________________
     *
     *
     */

    public function getTelephone()
    {
            return $this->getDefaultContact()->getEntryWorkPhone();
    }




    /*
     *
     * [ setTelephone ] 
     *_____________________________________________________
     *
     *
     */

    public function setTelephone($value)
    {
            return $this->getDefaultContact()->setEntryWorkPhone($value);
    }





    /*
     *
     * [ getEmail ] 
     *_____________________________________________________
     *
     *
     */

    public function getEmail()
    {
            return $this->getDefaultContact()->getEntryEmail();
    }






    /*
     *
     * [ setEmail ] 
     *_____________________________________________________
     *
     *
     */

    public function setEmail($value)
    {
            return $this->getDefaultContact()->setEntryEmail($value);
    }





    /*
     *
     * [ getContactName ] 
     *_____________________________________________________
     *
     *
     */

    public function getContactName()
    {
            return $this->getDefaultContact()->getEntryName();
    }






    /*
     *
     * [ setContactName ] 
     *_____________________________________________________
     *
     *
     */

    public function setContactName($value)
    {
            return $this->getDefaultContact()->setEntryName($value);
    }




    /*
     *
     * [ getDateAdded ] 
     *_____________________________________________________
     *
     *
     */

    public function getDateAdded($_GMT = TRUE)
    {
        if (!is_bool($_GMT)) $this->e->triggerException('Error in class (' . 
                __CLASS__ . '): [ invalid argument for method: ' . 
                __FUNCTION__ . ' ] on line ' . __LINE__
                );

        $_DateTime = new \DateTime($this->_data['date_added']);
        if (!$_GMT) $_DateTime->setTimezone($this->getStore()->getTimezone());

        return $_DateTime;
    }







    /*
     *
     * [ getDateModified ] 
     *_____________________________________________________
     *
     *
     */

    public function getDateModified($_GMT = TRUE)
    {
        if (!is_bool($_GMT)) $this->e->triggerException(
                'Error in class (' . __CLASS__ . '): [ invalid argument for method: ' . 
                __FUNCTION__ . ' ] on line ' . __LINE__
                );

        $_DateTime = new \DateTime($this->_data['date_modified']);
        if (!$_GMT) $_DateTime->setTimezone($this->getStore()->getTimezone());

        return $_DateTime;
    }




    /*
     *
     * [ getTimezone ] 
     *_____________________________________________________
     *
     *
     */

    public function getTimezone()
    {
            return new \DateTimeZone($this->_data['timezone']);
    }



    /*
     *
     * [ setTimezone ] 
     *_____________________________________________________
     *
     *
     */

    public function setTimezone(\DateTimeZone $_TimeZone)
    {
            $this->_data['timezone'] = $_TimeZone->getName();
            $this->_modified[] = 'timezone';
            return $this;
    }



    /*
     *
     * [ isActive ] 
     *_____________________________________________________
     *
     *
     */

    public function isActive()
    {
            return ((int)$this->_data['active'] === 1) ? TRUE : FALSE;
    }





    /*
     * 
     * [ enable ]
     * ______________________________________________________
     * 
     * 
     */


    public function enable () 
    {

        if ((int)$this->_data['active'] === 0) 
        {
            $this->_data['active'] = 1;
            $this->_modified[] = 'active';
        }
            

		\Cataleya\System\Event::notify('store/opened', $this, array ());
        return TRUE;
    }





    /*
     * 
     * [ disable ]
     * ______________________________________________________
     * 
     * 
     */


    public function disable () 
    {
        if ((int)$this->_data['active'] === 1) 
        {
            $this->_data['active'] = 0;
            $this->_modified[] = 'active';
        }

		\Cataleya\System\Event::notify('store/closed', $this, array ());
        return TRUE;
    }







    /*
     *  [ getStaffMembers ]
     * ____________________________________________________________
     * 
     * 
     */


    public function getStaffMembers () 
    {
        return \Cataleya\Store\Staff::load($this);
    }










    /**
     * 
     * @param boolean $_FORCE_RELOAD
     * @return boolean
     */
    public function hasRootCategory ($_FORCE_RELOAD = FALSE)
    {

        $_id = $this->getRootCategoryID($_FORCE_RELOAD);
        return ($_id !== NULL);
        

    }
    
    
    
    


    /**
     * 
     * @param boolean $_FORCE_RELOAD
     * @return \Cataleya\Catalog\Tag
     */
    public function getRootCategory ($_FORCE_RELOAD = FALSE)
    {

        $_id = $this->getRootCategoryID($_FORCE_RELOAD);
        return (!empty($_id)) ? \Cataleya\Catalog\Tag::load($_id) : NULL;
        

    }



    
    
    /**
     * 
     * @param boolean $_FORCE_RELOAD
     * @return int|null Root Category ID
     * 
     */
    public function getRootCategoryID ($_FORCE_RELOAD = FALSE)
    {
            if (empty($this->_root_cat_id) || $_FORCE_RELOAD === TRUE )
            {



                    static $select_handle, $param_id;


                    if (!isset($select_handle)) {
                            // PREPARE SELECT STATEMENT...
                            $select_handle = $this->dbh->prepare('
                                                SELECT tag_id, is_root FROM tags 
                                                WHERE store_id = :store_id 
                                                AND is_root = 1 
                                                LIMIT 1 
                                                ');

                            $select_handle->bindParam(
                                    ':store_id', 
                                    $param_id, \PDO::PARAM_INT
                                    );
                    }


                    $param_id = $this->getID();

                    if (!$select_handle->execute())  $this->e->triggerException('
                                    Error in class (' . __CLASS__ . '): [ ' . 
                                    implode(', ', $select_handle->errorInfo()) . 
                                    ' ] on line ' . __LINE__);

                    $row = $select_handle->fetch(\PDO::FETCH_ASSOC);
                    $this->_root_cat_id = (!empty($row)) ? $row['tag_id'] : NULL;


            }

            return $this->_root_cat_id;

    }




    /**
     * 
     * @param \Cataleya\Catalog\Product $_Product
     * @return boolean
     * 
     */
    public function hasProduct (\Cataleya\Catalog\Product $_Product) 
    {

        static $select_handle, $param_id, $param_needle_id;


        if (!isset($select_handle)) {
                // PREPARE SELECT STATEMENT...
                $select_handle = $this->dbh->prepare(''
                        . 'SELECT pc.product_id '
                        . 'FROM products_to_collections pc '
                        . 'INNER JOIN tags t '
                        . 'ON pc.collection_id = t.collection_id '
                        . 'WHERE t.store_id = :store_id '
                        . 'AND pc.product_id = :needle_id '
                        . 'LIMIT 1'
                        . '');

                $select_handle->bindParam(':store_id', $param_id, \PDO::PARAM_INT);
                $select_handle->bindParam(':needle_id', $param_needle_id, \PDO::PARAM_INT);
        }


        $param_id = $this->getID();
        $param_needle_id = $_Product->getID();

        if (!$select_handle->execute())  $this->e->triggerException('
                        Error in class (' . __CLASS__ . '): [ ' . 
                        implode(', ', $select_handle->errorInfo()) . 
                        ' ] on line ' . __LINE__);

        $row = $select_handle->fetch(\PDO::FETCH_ASSOC);
        
        return (!empty($row));


    }





    /**
     * 
     * @param int $_increment
     * @return \Cataleya\Store
     * 
     */
    public function updateProductCount($_increment=0)
    {
            if ($_increment === 0) { return $this; }
            

            $this->_data['product_count'] += $_increment;
            $this->_modified[] = 'product_count';

            $this->saveData();

            return $this;
    }



    /**
     * 
     * @return int
     * 
     */
    public function getPopulation() {
        return $this->_data['product_count'];
    }

    
    

    /**
     * 
     * @param \Cataleya\Catalog\Tag $_Tag
     * @return boolean
     * 
     */
    public function hasCategory (\Cataleya\Catalog\Tag $_Category) 
    {

        static $select_handle, $param_id, $param_needle_id;


        if (!isset($select_handle)) {
                // PREPARE SELECT STATEMENT...
                $select_handle = $this->dbh->prepare(''
                        . 'SELECT JOIN tags '
                        . 'WHERE store_id = :store_id '
                        . 'AND tag_id = :needle_id '
                        . 'LIMIT 1'
                        . '');

                $select_handle->bindParam(':store_id', $param_id, \PDO::PARAM_INT);
                $select_handle->bindParam(':needle_id', $param_needle_id, \PDO::PARAM_INT);
        }


        $param_id = $this->getID();
        $param_needle_id = $_Category->getID();

        if (!$select_handle->execute())  $this->e->triggerException('
                        Error in class (' . __CLASS__ . '): [ ' . 
                        implode(', ', $select_handle->errorInfo()) . 
                        ' ] on line ' . __LINE__);

        $row = $select_handle->fetch(\PDO::FETCH_ASSOC);
        
        return (!empty($row));


    }








    /**
     *
     * @param \Cataleya\Asset\Contact $_Contact
     *
     *
     */
    public function addContact (\Cataleya\Asset\Contact $_Contact)
    {

        static 
                $contact_insert, 
                $param_id, 
                $param_contact_id;


        if (!isset($contact_insert)) {
                // PREPARE SELECT STATEMENT...
                $contact_insert = $this->dbh->prepare('
                    INSERT INTO store_contacts (store_id, contact_id)   
                    VALUES (:store_id, :contact_id)
                    ');

                $contact_insert->bindParam(':store_id', $param_id, \PDO::PARAM_INT);
                $contact_insert->bindParam(':contact_id', $param_contact_id, \PDO::PARAM_INT);
        }


        $param_id = $this->getID();
        $param_contact_id = $_Contact->getID();

        if (!$contact_insert->execute())  $this->e->triggerException('
                                Error in class (' . __CLASS__ . '): [ ' . 
                                implode(', ', $contact_insert->errorInfo()) . 
                                ' ] on line ' . __LINE__);


        // Reset base contact cache (it will be repopulated the next time [getBaseImages] is called.
        $this->_contacts = array ();

    }






    /**
     *
     * @param \Cataleya\Asset\Contact $_Contact
     *
     *
     */
    public function removeContact (\Cataleya\Asset\Contact $_Contact)
    {


                    // Reset contact cache (it will be repopulated the next time [getContacts] is called.
                    $this->_contacts = array ();





                    // Destroy contact...
                    $_Contact->delete();


                    return true;

    }







    /*
     *
     *  [ getContacts ]
     * ________________________________________________________________
     * 
     * Returns array of contact objects
     *
     *
     *
     */


    public function getContacts ($_FORCE_RELOAD = FALSE)
    {
            if (empty($this->_contacts) || $_FORCE_RELOAD === TRUE )
            {



                    static $contacts_select, $param_id;


                    if (!isset($contacts_select)) {
                            // PREPARE SELECT STATEMENT...
                            $contacts_select = $this->dbh->prepare('
                                                SELECT * FROM store_contacts 
                                                WHERE store_id = :store_id
                                                ');

                            $contacts_select->bindParam(
                                    ':store_id', 
                                    $param_id, \PDO::PARAM_INT
                                    );
                    }


                    $param_id = $this->getID();

                    if (!$contacts_select->execute())  $this->e->triggerException('
                                    Error in class (' . __CLASS__ . '): [ ' . 
                                    implode(', ', $contacts_select->errorInfo()) . 
                                    ' ] on line ' . __LINE__);

                    while ($row = $contacts_select->fetch(\PDO::FETCH_ASSOC)) 
                    {
                            $this->_contacts[] = \Cataleya\Asset\Contact::load($row['contact_id']);
                    }


            }

            return $this->_contacts;

    }











    /**
     *
     * @return \Cataleya\Asset\Contact
     *
     *
     */
    public function getDefaultContact ()
    {
            if (empty($this->_default_contact) )
            {
                    $this->_default_contact = \Cataleya\Asset\Contact::load(
                            $this->_data['default_contact_id']
                            );
            }

            return $this->_default_contact;

    }










    /**
     *
     * @param \Cataleya\Asset\Contact $_Contact
     *
     *
     */
    public function setDefaultContact (\Cataleya\Asset\Contact $_Contact)
    {
        
            $this->getContacts();
            if (!key_exists($_Contact->getID(), $this->_contacts))  $this->addContact ($_Contact);
            
            $this->_data['default_contact_id'] = $_Contact->getID();
            $this->_default_contact = $_Contact;

            if (!in_array('default_contact_id', $this->_modified)) $this->_modified[] = 'default_contact_id';

    }





    /*
     *
     *  [ usetDefaultContact ]
     * ________________________________________________________________
     * 
     * 
     *
     *
     *
     */


    public function unsetDefaultContact ()
    {
            $this->_data['default_contact_id'] = NULL;
            $this->_default_contact = NULL;

            if (!in_array('default_contact_id', $this->_modified)) $this->_modified[] = 'default_contact_id';

    }






 
 
        /**
         *
         *  
         * Will return product URL Rewrite (object)
         * 
         * @return \Cataleya\Asset\URLRewrite
         *
         */
	public function getURLRewrite () 
	{
		if (empty($this->_url_rewrite)) $this->_url_rewrite = \Cataleya\Asset\URLRewrite::load ($this->_data['url_rewrite_id']);
		return $this->_url_rewrite;
		
	}
        
        
        
        
 	/**
         * 
         * @return string
         * 
         */
	public function getHandle () 
	{
		
		return $this->getURLRewrite()->getKeyword();
		
	}

    

	/**
	 *
	 * @param string $keyword
	 *
	 *
	 *
	 */
	public function setHandle ($keyword = '') 
	{
		return $this->getURLRewrite()->setKeyword($keyword);
                
		
	}


        
    
    

    /*
     *
     *  [ addPage ]
     * ________________________________________________________________
     * 
     * 
     *
     *
     *
     */


    public function addPage (\Cataleya\Front\Shop\Page $_Page)
    {

        static 
                $insert_handle, 
                $param_id, 
                $param_page_id;


        if (!isset($insert_handle)) {
                // PREPARE SELECT STATEMENT...
                $insert_handle = $this->dbh->prepare('
                    INSERT INTO store_pages (store_id, page_id)   
                    VALUES (:store_id, :page_id)
                    ');

                $insert_handle->bindParam(':store_id', $param_id, \PDO::PARAM_INT);
                $insert_handle->bindParam(':page_id', $param_page_id, \PDO::PARAM_INT);
        }


        $param_id = $this->getID();
        $param_page_id = $_Page->getID();

        if (!$insert_handle->execute())  $this->e->triggerException('
                                Error in class (' . __CLASS__ . '): [ ' . 
                                implode(', ', $insert_handle->errorInfo()) . 
                                ' ] on line ' . __LINE__);


        

    }


    
    
    
    






        



}
	
	
	
	
