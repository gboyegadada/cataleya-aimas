<?php



namespace Cataleya\Helper\Importer\Reader;


/**
 * 
 * This is used to import products from other 
 * eCommerce platforms (Shopify, BigCommerce...)
 *
 * @author Gboyega Dada
 */
class ShopifyCSV 
extends \Cataleya\Helper\Importer\Reader {
    
    


    /**
     * 
     * @param string $_path Path to CSV file.
     * @return \Cataleya\Helper\Importer\Reader\ShopifyCSV
     * 
     */
    public function __construct($_path) {
        

        $_CSV_DATA = \Cataleya\Helper::getCSV($_path);
        $_catalog_info = array ();
        



        // sort products to categories
        foreach ($_CSV_DATA as $_row) {
            // Category
            if (!empty($_row['Type']) && !isset($_catalog_info[$_row['Type']])) { 
                
            /*
             * Notice: I am taking the liberty of using the Shopify 'Type' as 'Category' in Cataleya 
             * since they don't seem to support a proper Product Category implementation.
             * I don't know if this is the best way right now but I'm going with it.
             * F*ck Shopify!
             */
                $_category = (!empty($_row['Type']) ? $_row['Type'] : '...');
                $_catalog_info[$_category] = array ();
                
                $this->_categories[] = $_category;

                
            }

            // Product 
            if (!empty($_row['Handle']) && !isset($_catalog_info[$_category][$_row['Handle']])) { 
                
                
                $_option_nvps = $_option_attributes = array ();
                
                if (!empty($_row['Option1 Name'])) { 
                    $_option_nvps['Option1 Name'] = 'Option1 Value';
                    $_option_attributes[$_row['Option1 Name']] = $_row['Option1 Value'];
                    }
                if (!empty($_row['Option2 Name'])) { 
                    $_option_nvps['Option2 Name'] = 'Option2 Value';
                    $_option_attributes[$_row['Option2 Name']] = $_row['Option2 Value'];
                    }
                if (!empty($_row['Option3 Name'])) { 
                    $_option_nvps['Option3 Name'] = 'Option3 Value';
                    $_option_attributes[$_row['Option3 Name']] = $_row['Option3 Value'];
                    }
                
                
                $_row['Body (HTML)'] = \Cataleya\Helper\Validator::html($_row['Body (HTML)'], 0, 2000);
                
                $_catalog_info[$_category][$_row['Handle']] = array (
                    'handle' => $_row['Handle'], 
                    'category' => $_category, 
                    'title' => (!empty($_row['Title']) ? $_row['Title'] : '...'), 
                    'description' => (!empty($_row['Body (HTML)']) ? $_row['Body (HTML)'] : '...'), 
                    'meta_title' => (!empty($_row['SEO Title']) ? $_row['SEO Title'] : '...'), 
                    'meta_description' => (!empty($_row['SEO Description']) ? \Cataleya\Helper\Validator::html($_row['SEO Description']) : '...'), 
                    'tags' => $_row['Tags'], 
                    'visible' => \Cataleya\Helper::boolval($_row['Published']), 
                    'supplier' => $_row['Vendor'], 
                    'gift_card' => \Cataleya\Helper::boolval($_row['Gift Card']), 
                    'options' => array (), 
                    'image' =>  array( 
                            'src' => trim($_row['Image Src']), 
                            'alt_text' => $_row['Image Alt Text']
                        ), 
                    'product_type' => $this->getProductType(array_keys($_option_attributes)) 
                );
            }


            
            $_a = array ();
            $_conversion_table = array (
                'kg' => 1000, 
                'g' => 1, 
                'lb' => 453.592, 
                'oz' => 28.3495
            );
            
            foreach ($_option_attributes as $n=>$v) { 
                if (empty($this->_attribute_values[$n])) { $this->_attribute_values[$n] = array(); }
                $this->_attribute_values[$n][] = $v;
            }
            
            
            // Option/Variant
            $_catalog_info[$_category][$_row['Handle']]['options'][] = array (
                'SKU' => $_row['Variant SKU'], 
                'UPC' => $_row['Variant Barcode'], 
                'stock' => ((float)$_row['Variant Inventory Qty']<0) ? 0 : $_row['Variant Inventory Qty'], 
                'price' => $_row['Variant Price'], 
                'compare_at_price' => $_row['Variant Compare At Price'], 
                'requires_shipping' => \Cataleya\Helper::boolval($_row['Variant Requires Shipping']), 
                'taxable' => \Cataleya\Helper::boolval($_row['Variant Taxable']), 
                'grams' => $_row['Variant Grams'], 
                'weight' => ((!empty($_row['Variant Grams']) && is_numeric($_row['Variant Grams'])) 
                        ? ((float)$_row['Variant Grams']/$_conversion_table[$_row['Variant Weight Unit']]) 
                        : 0
                    ), 
                'weight_unit' => $_row['Variant Weight Unit'], 
                'attributes' => $_option_attributes, 
                'image' =>  array( 
                        'src' => trim($_row['Variant Image']), 
                        'alt_text' => $_row['Image Alt Text']
                    )
                    
            );

        }
        
        foreach ($this->_product_types as $attribute_list) {
            
            foreach ($attribute_list as $label) {
                if (!in_array($label, $this->_attribute_types)) { $this->_attribute_types[] = $label; }
            }
            
        }
        
        
        foreach ($this->_attribute_values as $n=>$v) {
            $this->_attribute_values[$n] = array_unique($v);
        }

        file_put_contents(ROOT_PATH.'var/logs/attributes_debug.txt', __LINE__.': '.__CLASS__ . ' PROD TYPES:' . print_r($this->_product_types, true) . "\n ", FILE_APPEND);
        file_put_contents(ROOT_PATH.'var/logs/attributes_debug.txt', __LINE__.': '.__CLASS__ . ' ATTR TYPES:' . print_r($this->_attribute_types, true) . "\n ", FILE_APPEND);
        file_put_contents(ROOT_PATH.'var/logs/attributes_debug.txt', __LINE__.': '.__CLASS__ . ' ATTR VALS:' . print_r($this->_attribute_values, true) . "\n ", FILE_APPEND);
        file_put_contents(ROOT_PATH.'var/logs/attributes_debug.txt', __LINE__.': '.__CLASS__ . ' OPT ATTRS:' . print_r($_option_attributes, true) . "\n ", FILE_APPEND);

        $this->_DATA = $_catalog_info;


        
    }
    
    
    
	
    public function getProductType (array $_attribute_names) {
        
        foreach ($this->_product_types as $key=>$pt) {
            $match = $this->_comp_array($pt, $_attribute_names);
            if ($match) { return $key; }
        }

        // No match...
        $new_key = 'Shopify Product ' . (count($this->_product_types)+1);
        $this->_product_types[$new_key] = $_attribute_names;

        return $new_key;

    }

    
    
}
