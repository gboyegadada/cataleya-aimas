<?php



namespace Cataleya\Helper\Importer;



/**
 *
 * @author Gboyega Dada <gboyega@fpplabs.com>
 */
abstract class Reader 
{
	
        protected 
                
                $_categories = array (), 
                $_products = array (), 
                $_product_types = array (), 
                $_attribute_types = array (), // Color, Size, Blah...
                $_attribute_values = array (), // Color => [Red, Green, Blue], Size => [S, M, L]
                $_DATA = array ();
        

    
        /* ------------- Supported File Types ---------------- */
        
        const FILE_TYPE_CATALEYA_CSV = 0;
        const FILE_TYPE_CATALEYA_JSON = 1;
        const FILE_TYPE_CATALEYA_XML = 2;

        const FILE_TYPE_SHOPIFY_CSV = 10;
        
        
        

	
        public function getProductType (array $attributes_array) {
            
            foreach ($this->_product_types as $key=>$pt) {
                $match = $this->_comp_array(array_keys($pt), $attributes_array);
                if ($match) { return $key; }
            }
            
            // No match...
            $new_key = count ($this->_product_types);
            $this->_product_types[$new_key] = $attributes_array;
            
            return $new_key;
            
        }
        
        
        /**
         * 
         * @param array $a1
         * @param array $a2
         * @return boolean
         * 
         */
        protected function _comp_array (array $a1, array $a2) {
            
            if (count($a1) === count($a2)) {
            foreach ($a1 as $v) {
                if (!in_array($v, $a2)) { return false; }
            }
            } else {
                return false;
            }
            
            return true;
        }
        
        
        /**
         * 
         * @param boolean $encode
         * @return array|string JSON
         * 
         */
        public function getJSON ($encode = false) {
            
            return (is_bool($encode) && $encode === true) 
                    ? json_encode($this->_DATA, JSON_PRETTY_PRINT) 
                    : $this->_DATA;
            
        }
        
        
        
        /**
         * 
         * @return array List of categories (names not handles)
         * 
         */
        public function getCategories() {
            return $this->_categories;
        }
        
        
        /**
         * 
         * @return array 
         * 
         * Each member is an array of Field Labels (Color, Size, Blah)
         * 
         */
        public function getProductTypes() {
            return $this->_product_types;
        }
        
        

        /**
         * 
         * @return array 
         * 
         * UNIQUE list of field labels (Color, Size, Blah)
         * 
         */
        public function getAttributeTypes() {
            return $this->_attribute_types;
        }
        
        
        


        /**
         * 
         * @return array 
         * 
         * UNIQUE list of field labels + values (Color => [Red, Green, Blue], Size => [S, M, L])
         * 
         */
        public function getAttributeValues() {
            return $this->_attribute_values;
        }
        
        
        
        /**
         * 
         * @return array Array of products arranged by category.
         * 
         */
        public function getProducts() {
            return $this->_DATA;
        }
	
	
}
	
	
	

