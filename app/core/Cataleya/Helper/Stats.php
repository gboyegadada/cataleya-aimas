<?php



namespace Cataleya\Helper;



/*
 *
 * 	@Package: Cataleya
 * 	@Class: Helper
 *
 * 	(c) 2012 Fancy Paper Planes
 *
 *
 *
 */

class Stats {
    //put your code here
    
    
         
    
        
/*
 * These constants will be public so you can do something like: 
 * \Cataleya\Helper\Stats::getStats(\Cataleya\Helper\Stats::INTERVAL_DAY)
 *
 */


	const INTERVAL_DAY = 1;
	const INTERVAL_WEEK = 2;
	const INTERVAL_MONTH = 3;
	const INTERVAL_YEAR = 4; 
	const INTERVAL_MONTH_3D = 5;
	const INTERVAL_HOUR = 6;
	

         
          /*
           * 
           * [ updateTally ]
           * ___________________________________________________________
           * 
           * 
           */
          
          public static function updateProductTally (\Cataleya\Store $_Store, \Cataleya\Catalog\Product $_Product, array $_tally = array ()) 
          {

                
                if (empty($_tally)) return FALSE;

                
                $_table_prefix = 'cat_stats';
                $_filter = array ();
                $_filter[] = array('col'=>'store_id', 'id'=>$_Store->getID());
                $_filter[] = array('col'=>'product_id', 'id'=>$_Product->getID());
                
                
                
                $_tally_cols = array (
                    'tally_views'=>(!empty($_tally['views']) ? $_tally['views'] : 0), 
                    'tally_wishlists'=>(!empty($_tally['wishlists']) ? $_tally['wishlists'] : 0), 
                    'tally_orders'=>(!empty($_tally['orders']) ? $_tally['orders'] : 0), 
                    'order_subtotal'=>(!empty($_tally['subtotal']) ? (float)$_tally['subtotal'] : 0)
                );
                return self::updateTally($_table_prefix, $_filter, $_tally_cols);
                
                

                
          }


  
          
          
          
          
          /*
           * 
           * [ getProductTally ]
           * ___________________________________________________________
           * 
           * 
           */
          
          public static function getProductTally (\Cataleya\Store $_Store = NULL, \Cataleya\Catalog\Product $_Product = NULL, $_duration = 1, $_interval = self::INTERVAL_MONTH, $_from_now = FALSE) 
          {

                $_table_prefix = 'cat_stats';

                $_filter = array ();
                if (!empty($_Store)) $_filter[] = array('col'=>'store_id', 'id'=>$_Store->getID());
                if (!empty($_Product)) $_filter[] = array('col'=>'product_id', 'id'=>$_Product->getID());
                
                $_tally_cols = array (
                    'tally_views', 
                    'tally_wishlists', 
                    'tally_orders', 
                    'order_subtotal'
                );
                return self::getTally($_table_prefix, $_filter, $_tally_cols, $_duration, $_interval, $_from_now);
                
                
                
          }


          
          
   

          
          
        
          
          /*
           * 
           * [ getCatalogStats ]
           * ___________________________________________________________
           * 
           * 
           */
          
          public static function getCatalogStats (\Cataleya\Store $_Store = NULL, \Cataleya\Catalog\Product $_Product = NULL, $_offset = 1, $_interval = self::INTERVAL_MONTH, $_from_now = FALSE) 
          {

                
                $_table_prefix = 'cat_stats';
                
                $_filter = array ();
                if (!empty($_Store)) $_filter[] = array('col'=>'store_id', 'id'=>$_Store->getID());
                if (!empty($_Product)) $_filter[] = array('col'=>'product_id', 'id'=>$_Product->getID());
                
                $_tally_cols = array (
                    'tally_views', 
                    'tally_wishlists', 
                    'tally_orders', 
                    'order_subtotal'
                );
                return self::getStats($_table_prefix, $_filter, $_tally_cols,  $_offset, $_interval, $_from_now);
                             
                
          }
          
          
          
          
          
          
          
          
         
          /*
           * 
           * [ updateFailedLogins ]
           * ___________________________________________________________
           * 
           * 
           */
          
          public static function updateFailedLogins ($_is_admin = FALSE) 
          {

                if (!is_bool($_is_admin)) \Cataleya\Helper\ErrorHandler::getInstance()->triggerException('Error in class (' . __CLASS__ . '): [ invalid argument for method: ' . __FUNCTION__ . ' ] on line ' . __LINE__);

                
                $_table_prefix = 'failed_logins';
                
                $_filter = array();
                $_tally_cols = $_is_admin ? array ('tally_admin'=>1) : array('tally_customers'=>1);
                
                return self::updateTally($_table_prefix, $_filter, $_tally_cols);
                
                

                
          }


  
          
          
          
          
          /*
           * 
           * [ getFailedLogins ]
           * ___________________________________________________________
           * 
           * 
           */
          
          public static function getFailedLogins ($_duration = 1, $_interval = self::INTERVAL_MONTH, $_from_now = FALSE) 
          {

                $_table_prefix = 'failed_logins';
                $_filter = array();
                $_tally_cols = array ('tally_admin', 'tally_customers');
                
                return self::getTally($_table_prefix, $_filter, $_tally_cols, $_duration, $_interval, $_from_now);
                
                
                
          }


          
          
   

          
          
        
          
          /*
           * 
           * [ getFailedLoginStats ]
           * ___________________________________________________________
           * 
           * 
           */
          
          public static function getFailedLoginStats ($_offset = 1, $_interval = self::INTERVAL_MONTH, $_from_now = FALSE) 
          {

                
                $_table_prefix = 'failed_logins';
                $_filter = array();
                $_tally_cols = array ('tally_admin', 'tally_customers');
                
                return self::getStats($_table_prefix, $_filter, $_tally_cols,  $_offset, $_interval, $_from_now);
                             
                
          }
          

          
          
          
          
          
          
          
          
 
          
          
          
          
          /*
           * 
           * [ updateFailedLogins ]
           * ___________________________________________________________
           * 
           * 
           */
          
          private static function updateTally ($_table_prefix = NULL, array $_filter = array(), array $_tally_cols = array ()) 
          {


                if (empty($_tally_cols) || empty($_table_prefix)) return FALSE;
                
                
                switch ($_table_prefix) 
                {
                    case 'failed_logins':
                    $_params_date = date('Y-m-d H') . ":59:59";
                    $_tables = array (
                        $_table_prefix.'_hourly'=>'1 HOUR', 
                        $_table_prefix.'_x3month'=>'3 DAY'
                        );
                    break;
                
                    case 'cat_stats':
                    $_params_date = date('Y-m-d') . " 23:59:59";
                    $_tables = array (
                        $_table_prefix.'_daily'=>'1 DAY', 
                        $_table_prefix.'_monthly'=>'1 MONTH', 
                        $_table_prefix.'_x3month'=>'3 DAY'
                        );
                    break;
                
                    default:
                    $_tables = array ();
                    $_params_date = date('Y-m-d') . " 23:59:59";
                    break;
                }
                
                
                $_params_prev_month_end = self::getPrevDayDateTime();
                
                

                $key_val_pairs = array ();
                $key_param_pairs = array ();
                foreach ($_tally_cols as $key=>$v) 
                {
                    $_col = $key;
                    $key_val_pairs[$_col] = $_col . ' = ' . $_col . ' + ' . ((is_float($v)) ? (float)$v : (int)$v);
                    $key_val_insert_pairs[$_col] = (is_float($v)) ? (float)$v : (int)$v;
                    $key_param_pairs[$_col] = ':' . $_col;
                    
                }
                
                $_filter_str = '';
                foreach ($_filter as $_pair) 
                {
                    $key_param_pairs[$_pair['col']] = ':' . $_pair['col'];
                    $_filter_str .= ' AND ' . $_pair['col'] .' = '. (int)$_pair['id'];
                }
                
                
                if (empty($key_val_pairs)) return FALSE;
                
                foreach ($_tables as $_table=>$_interval) 
                {


                        // Do prepared statement for 'INSERT'...
                        $update_handle = \Cataleya\Helper\DBH::getInstance()->prepare('
                                                            UPDATE ' . $_table . ' 
                                                            SET ' . implode (', ', $key_val_pairs) . '
                                                            WHERE date_added > DATE_SUB(:today, INTERVAL ' . $_interval . ')  
                                                            AND date_added > :prev_month_end 
                                                            ' . $_filter_str
                                                            );

                        $update_handle->bindParam(':today', $_params_date, \PDO::PARAM_STR);
                        $update_handle->bindParam(':prev_month_end', $_params_prev_month_end, \PDO::PARAM_STR);

                                
                        // Execute...
                        if (!$update_handle->execute()) \Cataleya\Helper\ErrorHandler::getInstance()->triggerException('Error in class (' . __CLASS__ . '): [ ' . implode(', ', $update_handle->errorInfo()) . ' ] on line ' . __LINE__);

                        
                        /// Do insert if no rows are affected (i.e. all previous inserts older than interval)
                        if ($update_handle->rowCount() < 1) 
                        {

                                // Do prepared statement for 'INSERT'...
                                $insert_handle = \Cataleya\Helper\DBH::getInstance()->prepare('
                                                                    INSERT INTO ' . $_table . ' (' . implode (', ', array_keys($key_param_pairs)) . ', date_added)       
                                                                    VALUES (' . implode (', ', $key_param_pairs) . ', NOW()) 
                                                                    ');
                                
                                $_params = array ();
                                foreach ($key_val_insert_pairs as $k=>$v) 
                                {
                                $_params[$k] = (is_float($v)) ? (float)$v : (int)$v;
                                
                                $insert_handle->bindParam(':'.$k, $_params[$k], \Cataleya\Helper\DBH::getTypeConst($_params[$k]));
                                
                                }
                                
                                foreach ($_filter as $_pair) 
                                {
                                    $_params[$_pair['col']] = $_pair['id'];
                                    $insert_handle->bindParam(':'.$_pair['col'], $_params[$_pair['col']], \Cataleya\Helper\DBH::getTypeConst($_params[$_pair['col']]));
                                    
                                }



                                // Execute...
                                if (!$insert_handle->execute()) \Cataleya\Helper\ErrorHandler::getInstance()->triggerException('Error in class (' . __CLASS__ . '): [ ' . implode(', ', $insert_handle->errorInfo()) . ' ] on line ' . __LINE__);

                        }


                }
                
                
                return TRUE;
                
          }


  
          
          
          
          
          /*
           * 
           * [ getTally ]
           * ___________________________________________________________
           * 
           * 
           */
          
          private static function getTally ($_table_prefix = NULL, array $_filter = array(), array $_tally_cols = array (), $_duration = 1, $_interval = self::INTERVAL_MONTH, $_from_now = FALSE) 
          {

                
              
                $_duration = filter_var($_duration, FILTER_VALIDATE_INT, FILTER_NULL_ON_FAILURE);
                if ($_duration === NULL || empty($_tally_cols) || empty($_table_prefix)) return FALSE;
                

                $_table = $_period = '';
                
                switch ($_interval) 
                {
                    case self::INTERVAL_HOUR:
                        $_table = $_table_prefix .  '_hourly';
                        $_period = $_duration . ' HOUR';
                    break;
                
                    case self::INTERVAL_DAY:
                        $_table = $_table_prefix .  '_daily';
                        $_period = $_duration . ' DAY';
                    break;
                
                    case self::INTERVAL_WEEK:
                        $_table = $_table_prefix .  '_daily';
                        $_period = ($_duration*7) . ' DAY';
                    break;
                
                    case self::INTERVAL_MONTH:
                        $_table = $_table_prefix .  '_monthly';
                        $_period = $_duration . ' MONTH';
                    break;
                
                    case self::INTERVAL_YEAR:
                        $_table = $_table_prefix .  '_yearly';
                        $_period = $_duration . ' YEAR';
                    break;
                
                    default:
                        $_table = $_table_prefix .  '_daily';
                        $_period = $_duration . ' DAY';
                    break;
                }
                


                $_filter_str = '';
                foreach ($_filter as $_pair) 
                {
                    $_filter_str .= ' AND ' . $_pair['col'] .' = '. (int)$_pair['id'];
                }
                
                $_params_date = ($_from_now === TRUE) ? date('Y-m-d H:i:s') : (date('Y-m-d') . " 23:59:59");
                

                $key_val_pairs = array ();
                foreach ($_tally_cols as $k) 
                {
                    $_col = $k;
                    $key_val_pairs[$_col] = 'SUM(' . $_col . ') as ' . $k;
                }

                // Do prepared statement for 'INSERT'...
                $update_handle = \Cataleya\Helper\DBH::getInstance()->prepare('
                                                    SELECT ' . implode (', ', $key_val_pairs) . ' 
                                                    FROM ' . $_table . ' 
                                                    WHERE date_added > DATE_SUB(:today, INTERVAL ' . $_period . ') 
                                                    ' . $_filter_str
                                                    );

                $update_handle->bindParam(':today', $_params_date, \PDO::PARAM_STR);


                // Execute...
                if (!$update_handle->execute()) \Cataleya\Helper\ErrorHandler::getInstance()->triggerException('Error in class (' . __CLASS__ . '): [ ' . implode(', ', $update_handle->errorInfo()) . ' ] on line ' . __LINE__);

                $_row = $update_handle->fetch(\PDO::FETCH_ASSOC);
                
                foreach ($_row as $k=>$v) 
                {
                    if (empty($v)) $_row[$k] = 0;
                }



                return $_row;
                
                
          }


          
          
   

          
          
        
          
          /*
           * 
           * [ getStats ]
           * ___________________________________________________________
           * 
           * 
           */
          
          private static function getStats ($_table_prefix = NULL, array $_filter = array(), array $_tally_cols = array (), $_offset = 1, $_interval = self::INTERVAL_MONTH, $_from_now = FALSE) 
          {

                
              
                $_offset = filter_var($_offset, FILTER_VALIDATE_INT, FILTER_NULL_ON_FAILURE);
                
                if (!is_string($_table_prefix) || $_offset === NULL || empty($_tally_cols)) return FALSE;

                $_table = $_interval_exp = '';

                $_secs = 0;
                $_limit = 1;
                $_date_format = '';
                $_days_in_this_month = cal_days_in_month(CAL_GREGORIAN, date('m'), date('Y'));
                
                switch ($_interval) 
                {
                    case self::INTERVAL_HOUR:
                        $_table = $_table_prefix .  '_hourly';
                        $_interval_exp = ' HOUR';
                        $_secs = 60*60;
                        $_date_format = 'M d, h:i';
                        $_params_date = ($_from_now === TRUE) ? date('Y-m-d H:i:s') : (date('Y-m-d H') . ":59:59");
                    break;
                
                    case self::INTERVAL_DAY:
                        $_table = $_table_prefix . '_daily';
                        $_interval_exp = ' DAY';
                        $_secs = 24*60*60;
                        $_date_format = 'd M';
                        $_params_date = ($_from_now === TRUE) ? date('Y-m-d H:i:s') : (date('Y-m-d') . " 23:59:59");
                    break;
                
//                    case self::INTERVAL_WEEK:
//                        $_table = $_table_prefix .  '_daily';
//                        $_interval_exp = (7) . ' DAY';
//                    break;
                
                    case self::INTERVAL_MONTH:
                        $_table = $_table_prefix . '_monthly';
                        $_interval_exp = ' MONTH';
                        $_secs = 24*60*60; // we'll convert to months later...
                        $_date_format = 'Y M';
                        $_params_date = ($_from_now === TRUE) ? date('Y-m-d H:i:s') : (date('Y-m-') . $_days_in_this_month . " 23:59:59");
                    
                    break;
                
                    case self::INTERVAL_MONTH_3D:
                        $_table = $_table_prefix . '_x3month';
                        $_interval_exp = ' DAY';
                        $_offset = self::calculateDaysInXMonths(-$_offset);
                        $_limit = 3;
                        $_secs = 24*60*60; // we'll convert to months later...
                        $_date_format = 'd M';
                        $_params_date = ($_from_now === TRUE) ? date('Y-m-d H:i:s') : (date('Y-m-') . $_days_in_this_month . " 23:59:59");
                    break;
                
                    case self::INTERVAL_YEAR:
                        $_table = $_table_prefix . '_yearly';
                        $_interval_exp = ' YEAR';
                        $_secs = 365*24*60*60;
                        $_date_format = 'Y';
                        $_params_date = ($_from_now === TRUE) ? date('Y-m-d H:i:s') : (date('Y-') . '12-' . cal_days_in_month(CAL_GREGORIAN, 12, date('Y')) . " 23:59:59");
                    break;
                
                    default:
                        $_table = $_table_prefix . '_daily';
                        $_interval_exp = ' DAY';
                        $_date_format = 'd M';
                        $_params_date = ($_from_now === TRUE) ? date('Y-m-d H:i:s') : (date('Y-m-d') . " 23:59:59");
                    break;
                }
                
                
                
                // Find LAST DAY of LAST MONTH
                $_params_prev_month_end = self::getPrevDayDateTime();
                

                $_filter_str = '';
                foreach ($_filter as $_pair) 
                {
                    $_filter_str .= ' AND ' . $_pair['col'] .' = '. (int)$_pair['id'];
                }
                
                
                $key_val_pairs = array ();
                foreach ($_tally_cols as $k) 
                {
                    $_col = $k;
                    $key_val_pairs[$_col] = 'SUM(' . $_col . ') as ' . $k;
                }
                
                // Do prepared statement 
                $select_handle = \Cataleya\Helper\DBH::getInstance()->prepare('
                                                    SELECT ' . implode (', ', $key_val_pairs) . '  
                                                    FROM ' . $_table . ' 
                                                    WHERE date_added > DATE_SUB(:today, INTERVAL :offset ' . $_interval_exp . ')  
                                                    AND date_added < DATE_SUB(:today, INTERVAL :offset-' . $_limit . $_interval_exp . ') 
                                                    AND date_added > :prev_month_end 
                                                    ' . $_filter_str
                                                );
                
                $_param_offset = 0;
                $_stats = array ();
                $select_handle->bindParam(':offset', $_param_offset, \PDO::PARAM_INT);
                $select_handle->bindParam(':today', $_params_date, \PDO::PARAM_STR);
                $select_handle->bindParam(':prev_month_end', $_params_prev_month_end, \PDO::PARAM_STR);
                $_index = $_sub_devision = 0;
                $_ts = strtotime($_params_date);
                    
                for ($i = $_offset; $i > 0; $i-=$_limit) 
                {

                    $_param_offset = $i;
                    $_index++;
                    
                    
                    // Execute...
                    if (!$select_handle->execute()) \Cataleya\Helper\ErrorHandler::getInstance()->triggerException('Error in class (' . __CLASS__ . '): [ ' . implode(', ', $select_handle->errorInfo()) . ' ] on line ' . __LINE__);

                    $_row = $select_handle->fetch(\PDO::FETCH_ASSOC);
                    

                    if ($_interval === self::INTERVAL_MONTH) 
                    {
                         $_date_label = date($_date_format, ($_ts-(self::calculateDaysInXMonths(-($i-1))*$_secs)));  
                    }
                    
                    else {
                        
                        $_date_label = date($_date_format, ($_ts-(($i-1)*$_secs)));
                    }
                    
                    // $_stats['ticks'][] = array ($_index, $_date_label); 
                    
                    
                    foreach ($_row as $k=>$v) 
                    {
                        if (empty($v)) $_row[$k] = 0;
                        if (!isset($_stats[$k])) $_stats[$k] = array ();
                        
                        $_stats[$k][] = array ($_date_label, ((is_float($v)) ? (float)$v : (int)$v));
                    }
                
                }
                
                return $_stats;
                
                
          }
          
          
          
          
          /*
           * 
           * [ detectBruteForce ]
           * __________________________________________________
           * 
           * 
           */
          
          static public function detectBruteForce ($_is_admin = FALSE) 
          {
              
              $_key = ($_is_admin === TRUE) ? 'tally_admin' : 'tally_customers';
              
              $_raw_data = self::getFailedLoginStats(10, self::INTERVAL_HOUR);
              $_data = array ();
              foreach ($_raw_data[$_key] as $k=>$_set) $_data[$k] = $_set[1];
              
              $_threshold = 40;
              $_max = (int)max($_data);
              $_counter = 0;
              
              // return FALSE if we've NOT had up to 20 failed logins in 
              // any of the last 10 hours.
              if ($_max < $_threshold) return FALSE;
              
              $_target = ($_max/2);
              
              // check most recent failed logins tally to make sure attack is presently on-going.
              if ((int)end($_data) < ($_target/2)) return FALSE;
              
              
              foreach ($_data as $_num) if ((int)$_num < $_target) $_counter++;
              
              return ($_counter >= 2 && $_target > $_threshold);
              
              
          }












          /*
         * 
         * [ getPrevDayDateTime ]
         * 
         */
        

          private static function getPrevDayDateTime () 
          {
                // Find LAST DAY of LAST MONTH
                $_last_month = (int)date('m');
                $_last_month_year = (int)date('Y');
                
                if ($_last_month > 1) { $_last_month--; } 
                else { $_last_month_year--; $_last_month = 12; }
                
                $_last_month_day = (int)cal_days_in_month(CAL_GREGORIAN, $_last_month, $_last_month_year);
                
                return ($_last_month_year . '-' . $_last_month . '-' . $_last_month_day . ' 23:59:59');
          }
          
          
          


        /*
         * 
         * [ calculateDaysInXMonths ]
         * 
         */
        

        static private function calculateDaysInXMonths($_months = -1) 
        {


            $_past = FALSE;
            $_days = 0;
            $_year = (int)date('Y');
            $_base12_counter = (int)date('m');


            if ($_months < 0) 
            {

                $_past = TRUE;
                $_months *= -1;

            }


            for ($i = 0; $i < $_months; $i++) 
            {
                

                $_days += cal_days_in_month(CAL_GREGORIAN, $_base12_counter, $_year);


                if ($_past === TRUE):

                    if ($_base12_counter === 1) { $_base12_counter = 12; $_year--; } 
                    else { $_base12_counter--; }

                    else:

                    if ($_base12_counter === 12) { $_base12_counter = 1; $_year++; } 
                    else { $_base12_counter++; }

                endif;


            } // end for loop

            return $_days;


        }
        
        
          
          
     
       

        

 
}


