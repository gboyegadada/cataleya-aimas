<?php


namespace Cataleya\Helper;

use \Cataleya\Error;

/*

CLASS

*/



class Validator   
{
	

        
	private $dbh, $e;
	

	


	/*
	 *
	 * [ __construct ]
	 * ________________________________________________________________
	 *
	 *
	 *
	 */
	 
	 
	private function __construct ()
	{
		
		// Get database handle...
        $this->dbh = \Cataleya\Helper\DBH::getInstance();
		
		// Get error handler
		$this->e = \Cataleya\Helper\ErrorHandler::getInstance();
		
	}
	




	/*
	 *
	 * [ __destruct ]
	 * ________________________________________________________________
	 *
	 *
	 *
	 */

        public function __destruct()
        {
                    
        }
        
        
        
        
        
        
        

        

        /**
         * 
         * @param array $_expected_keys
         * @param array $_params
         * 
         */
        public static function params (array $_expected_keys, array $_params) 
        {
            
            $_result = array ();
            
            foreach ($_expected_keys as $k=>$v) {

                // list($_type, $_format) = @explode('_', $v);
                $_a = explode('_', $v);
                $_type = $_a[0]; 
                $_format = isset($_a[1]) ? $_a[1] : null;
                $_is_array = ($_format === 'array') ? true : false;
                
                // if: 1
                // I know it looks like I'm repeating myself in this 'if' block but I'm not.
                // This is here for when the parameter value has to be an [array] of something (string, int.. etc).
                // ie. This is being enforced.
                if (isset($_params[$k]) && $_is_array && is_array($_params[$k]) && method_exists(__CLASS__, $_type)) {
                    $_result[$k] = call_user_func_array([__CLASS__, 'array_of'], [ $_type, $_params[$k] ]);
                    $_result[$k] = ($_result[$k] === FALSE && !in_array($_type, array('bool', 'boolean'))) ? NULL : $_result[$k];
                }

                // if: 2
                // Here, the parameter value just happens to be an [array] of something and will be 
                // treated as such. But the array part is not a requirement.
                else if (isset($_params[$k]) && is_array($_params[$k]) && method_exists(__CLASS__, $v)) {
                    $_result[$k] = call_user_func_array([__CLASS__, 'array_of'], [ $v, $_params[$k] ]);
                    // $_result[$k] = call_user_func_array(array(__CLASS__, $v), array($_params[$k]));
                }
                

                // if: 3
                // You can specify a regex like this [ 'shop_handle' => 'match:/^[a-zA-Z]+$/' ]
                // TODO: array version of this!!
                else if (isset($_params[$k]) && is_scalar($_params[$k]) && 0 === strpos($v, 'match:')) {
                    $_result[$k] = call_user_func_array([__CLASS__, 'match'], [$_params[$k], substr($v, 6)]);
                }
                
                // if: 4
                else if (isset($_params[$k]) && is_scalar($_params[$k]) && method_exists(__CLASS__, $v)) {
                    $_result[$k] = call_user_func_array(array(__CLASS__, $v), array($_params[$k]));
                    $_result[$k] = ($_result[$k] === FALSE && !in_array($v, array('bool', 'boolean'))) ? NULL : $_result[$k];
                }
                
                // if: 5
                else if (isset($_params[$k]) && is_object($_params[$k]) && get_class($_params[$k]) === $v) {
                    $_result[$k] = $_params[$k]->getID();
                }

                // if: 6
                else if (!isset($_params[$k]) && in_array($v, array('bool', 'boolean'))) {
                    $_result[$k] = FALSE;
                }

                // if: 7
                else {
                    $_result[$k] = NULL;
                }
            }
            
            
            if (isset($_params['auto_confirm'])) $_result['auto_confirm'] = self::bool($_params['auto_confirm']);
            

            return $_result;
            

        }


        
        
        
        

        /**
         * 
         * @param string $_filter
         * @param mixed $_param
         * 
         */
        public static function param ($_param, $_filter) 
        {
            if (!is_string($_filter)) throw new Error ('Invalid argument: argument 1 should be a string.');
            
            $_result = null;
            

            if (isset($_param) && is_scalar($_param) && method_exists(__CLASS__, $_filter)) {
                $_result = call_user_func_array(array(__CLASS__, $_filter), array($_param));
                $_result = ($_result === FALSE && $_filter !== 'bool') ? NULL : $_result;
            }


            // if: 3
            // You can specify a regex like this [ 'shop_handle' => 'match:/^[a-zA-Z]+$/' ]
            // TODO: array version of this!!
            else if (isset($_param) && is_scalar($_param) && 0 === strpos($_filter, 'match:')) {
                $_result = call_user_func_array([__CLASS__, 'match'], [$_param, substr($_filter, 6)]);
            }

            else if (isset($_param) && is_object($_param) && get_class($_param) === $_filter) {
                $_result = $_param->getID();
            }

            else {
                $_result = false;
            }

            return $_result;
            

        }

        
        
        
        
        

        /**
         * 
         * @param int $_INPUT_TYPE
         * @param string $_field
         * 
         */
        public static function INPUT ($_INPUT_TYPE = INPUT_POST, array $_expected_keys = null) 
        {

            $_params = [];
            $_INPUT = ($_INPUT_TYPE === INPUT_POST) ? $_POST : $_GET;

            foreach ($_INPUT as $k=>$v) {
                list ($_name, $_token) = explode('-', $k);
                if (!empty($_name) && !empty($_token) && $_token === $_SESSION[SESSION_PREFIX.'FORM_TOKEN']) {
                    $_params[$_name] = $v;
                }
            }

            if (!isset($_params['token']) || $_params['token'] !== $_SESSION[SESSION_PREFIX.'FORM_TOKEN']) {
                // invalidate everything...
                $_params = [];
            }

            return self::params($_expected_keys, $_params); 

        }

        
        


        /**
         * 
         * @param int $_INPUT_TYPE
         * @param string $_field
         * 
         */
        public static function INPUT2 ($_INPUT_TYPE = INPUT_POST, $_field = '') 
        {
            
            list ($_label, $_type, $_digest, $_required) = explode('-', $_field);
            
            if (isset($_type) && $_type === 'confirm') {
                return self::confirmINPUT ($_INPUT_TYPE, $_field);
            }



            if (
                    !isset($_label, $_type, $_digest) 
                    || !in_array($_INPUT_TYPE, [INPUT_GET, INPUT_POST]) 
                    || $_digest !== $_SESSION[SESSION_PREFIX.'FORM_TOKEN']
                    ) return NULL;

            $_INPUT = ($_INPUT_TYPE === INPUT_POST) ? $_POST : $_GET;
            
            
            $_result = NULL;
            

            if (method_exists(__CLASS__, $_type)) {
                $_result = call_user_func_array(array(__CLASS__, $_type), array($_INPUT[$_field]));
                $_result = ($_result === FALSE && $_type !== 'bool') ? NULL : $_result;
            }


            else {
                $_result = NULL;
            }

            return $_result;
            

        }

        
        
        
        
        
        


        /**
         * 
         * @param int $_INPUT_TYPE
         * @param string $_field
         * 
         */
        public static function confirmINPUT ($_INPUT_TYPE = INPUT_POST, $_field = '') 
        {
            
            list ($_label, $_type, $_digest, $_required) = explode('-', $_field);


            if (
                    !isset($_label, $_type, $_digest) 
                    || !in_array($_INPUT_TYPE, [INPUT_GET, INPUT_POST]) 
                    || $_digest !== $_SESSION[SESSION_PREFIX.'SHOP_FORM_TOKEN']
                    ) return NULL;

            $_INPUT = ($_INPUT_TYPE === INPUT_POST) ? $_POST : $_GET;

            $_reference_value = NULL;
            foreach ($_INPUT as $k=>$v) {
                if (preg_match('/^('.$_label.')-(?!confirm-)/', $k) > 0 && $_INPUT[$_field] === $v) { 

                    list ($_clabel, $_ctype, $x, $y) = explode('-', $k);
                    $_reference_value = \Cataleya\Helper\Validator::param($v, $_ctype); 
                    break; 

                }
            }

            $_result = (FALSE !== $_reference_value) ? $_reference_value : NULL;


            return $_result;
            

        }

        
        
        

        public static function array_of ($_type = NULL, array $_values = array (), $_min = NULL, $_max = NULL) 
        {
            
            if (!is_string($_type) || !method_exists(__CLASS__, $_type)) {
                return FALSE;
            }
            
            $results = array ();
            
            foreach ($_values as $_value) {
                $r = call_user_func_array(array(__CLASS__, $_type), array($_value, $_min, $_max));
                $r = ($r === FALSE && $_type !== 'bool') ? NULL : $r;
                
                if ($r ===  NULL) { return FALSE; }
                
                $results[] = $r;
            }
            return $results;
        }
        

        
        
        

        /**
         * 
         * @param string $_arg
         * @param boolean $_validate_extension
         * @return boolean
         * 
         */
        public static function isFilename ($_arg, $_validate_extension=TRUE) 
        {
            // $_test = filter_var($_arg, FILTER_VALIDATE_REGEXP, array('options'=>array('regexp' => '/^(([A-Za-z0-9\-]+\.?){1,10})\.(?<extension>[A-Za-z]{3,4})$/')));
            
            static $_file_exts = array('jpg', 'jpeg', 'gif', 'tiff', 'png', 'php', 'dat', 'inc');
            $_matches = array();
            
            

            if (!is_string($_arg) || preg_match('/^(([A-Za-z0-9\-_]+\.?){1,20})\.(?<extension>[A-Za-z]{3,15})$/', $_arg, $_matches) == 0) return FALSE;
            
            return (in_array($_matches['extension'], $_file_exts) || $_validate_extension === FALSE) ? TRUE : FALSE;
            
            
        }
	

        

        /**
         * 
         * @param string $_arg
         * @param boolean $_validate_extension
         * @return boolean
         * 
         */
        public static function filename ($_arg, $_validate_extension=FALSE) 
        {
            return (self::isFilename($_arg, $_validate_extension)) ? $_arg : FALSE;
        }
        
        

        
        
        
        
        
        
        /*
         * 
         * public: [ string ]
         * 
         * 
         */
        
        
        public static function string ($_value, $_min = NULL, $_max = NULL) 
        {
            
            $_min = (is_int($_min)) ? (int)$_min : 1;
            $_max = (is_int($_max)) ? (int)$_max : 500;
            
            $_regexp = '/^[\w\s\-_]{' . $_min . ',' . $_max . '}$/';
            
            return filter_var($_value, FILTER_VALIDATE_REGEXP, array('options'=>array('regexp' => $_regexp)));
            
        }
        
        
        
        
        
        /*
         * 
         * public: [ html ]
         * 
         * 
         */
        
        
        public static function html ($_value, $_min = NULL, $_max = NULL) 
        {
            
            if (!is_string($_value)) return FALSE;
            else if (strlen($_value) == 0) return '';
            
            $_min = (is_int($_min)) ? (int)$_min : 1;
            $_max = (is_int($_max)) ? (int)$_max : 4000;
            $_len = strlen($_value);
            
            if ($_len < $_min || $_len > $_max) return FALSE;
            
            
            static $config = NULL, $def = NULL, $purifier = NULL;
            
            if (empty($purifier)) 
            {
                $config = \HTMLPurifier_Config::createDefault();
                $config->set('HTML.Doctype', 'XHTML 1.0 Transitional');
                $config->set('HTML.TidyLevel', 'medium');
                
                $def = $config->getHTMLDefinition(true);

                
                // DIV
                $def->addElement(
                    'div',   // name
                    'Block',  // content set
                    'Flow', // allowed children
                    'Common' // attribute collection
                )
                ->excludes = array('form' => true);
                

                
                // A (anchor)
                $def->addElement(
                    'div',   // name
                    'Block',  // content set
                    'Flow', // allowed children
                    'Common',  // attribute collection
                    array( // attributes
                      'href' => 'URI',
                      'target' => 'Enum#_blank|_self|_target|_top'
                    )
                )
                ->excludes = array('form' => true);
                
                // IMG
                $def->addElement(
                    'img',   // name
                    'Inline',  // content set
                    'Empty', // allowed children
                    'Common', // attribute collection
                    array( // attributes
                      'src' => 'URI'
                    )
                );
                
                

                
                $purifier = new \HTMLPurifier($config);
            }
            
            return $purifier->purify($_value);
            
            
        }
        
        
        
        
        
        
        /*
         * 
         * public: [ text ]
         * 
         * 
         */
        
        
        public static function text ($_value, $_min = NULL, $_max = NULL) 
        {
            
            return self::plain_text($_value, $_min, $_max);
            
        }
        
        
                
        public static function longtext ($_value, $_min = 1, $_max = 65000) 
        {
            
            return self::plain_text($_value, $_min, $_max);
            
        }
        
        
        
        /**
         * plain_text
         *
         * @param string $_value
         * @param int $_min
         * @param int $_max
         * @return boolean | string
         */
        public static function plain_text ($_value, $_min = NULL, $_max = NULL) 
        {
            
            if (!is_string($_value)) return false;
            
            $_value = strip_tags($_value);
            
            $_min = (is_int($_min)) ? (int)$_min : 1;
            $_max = (is_int($_max)) ? (int)$_max : 500;
            $_len = strlen($_value);
            
            return ($_len < $_min || $_len > $_max) ? false : $_value;
            
        }
        
        
        
        
        
        
        
        public static function date ($_value = NULL) 
        {   
            //throw new Exception('Na so we see am: ' . $_value);
            $_value = filter_var($_value, FILTER_VALIDATE_REGEXP, array('options'=>array('regexp' => '#^(19|20)\d{2}([\/|\-|\s]\d{1,2})([\/|\-|\s]\d{1,2})$#')));
            return ($_value !== FALSE) ? preg_replace('#[\/|\s|,|\:]#', '-', $_value) : $_value;
            
        }
	

        
        public static function datetime ($_value = NULL) 
        {
            $_value = filter_var($_value, FILTER_VALIDATE_REGEXP, array('options'=>array('regexp' => '#^(19|20)\d{2}[\/|\-|\s|,]([0]?[1-9]|[10-12])[\/|\-|\s|,]([0]?[1-9]|[10-31])\s+(\d{2}[\:]){2}(\d{2})$#')));
            return ($_value !== FALSE) ? preg_replace('#[\/|,]#', '-', $_value) : $_value;
        }
        

        public static function timezone ($_value = NULL) 
        {

            return (in_array($_value, \Cataleya\Helper::getTimeZones())) ? $_value : false;

        }
        

        public static function int ($_value = NULL, $_min = NULL, $_max = NULL) 
        {
            $_options = array ();
            $_int = (is_numeric($_value)) ? (int)$_value : NULL;
            if ($_int === NULL) return FALSE;
            
            if (is_int($_min)) $_options['min_range'] = (int)$_min;
            if (is_int($_max)) $_options['max_range'] = (int)$_max;
            
            $_int = filter_var($_int, FILTER_VALIDATE_INT, array('options'=>$_options));
            
            if($_int !== FALSE) return (preg_match('/^[0]\d/', strval($_value))) ? str_pad(strval($_int), 2, '0', STR_PAD_LEFT) : $_int;
            return FALSE;
        }
        
        
        
        public static function integer ($_value = NULL, $_min = NULL, $_max = NULL) 
        {
            return self::int($_value, $_min, $_max);
        }
        
        
       
 
        public static function float ($_value = NULL, $_min = NULL, $_max = NULL) 
        {
            
            $_options = array ();
            
            if (is_float($_min)) $_options['min_range'] = (float)$_min;
            if (is_float($_max)) $_options['max_range'] = (float)$_max;
            
            
            
            return filter_var($_value, FILTER_VALIDATE_FLOAT, array('options'=>$_options, 'flags'=>FILTER_FLAG_ALLOW_FRACTION | FILTER_FLAG_ALLOW_THOUSAND));
        }
        

        public static function percent ($_value = NULL) 
        {
            $_min = 0;
            $_max = 100;

            return self::float($_value, $_min, $_max);
        }

        
        public static function angle ($_value = NULL) 
        {
            $_min = 0;
            $_max = 360;

            return self::float($_value, $_min, $_max);
        }

        
        public static function bool ($_value = NULL) 
        {
            if (is_bool($_value)) return (bool)$_value;

            return filter_var($_value, FILTER_VALIDATE_BOOLEAN, FILTER_NULL_ON_FAILURE);
        }
        
        
        public static function boolean ($_value = NULL) 
        {
            return self::bool($_value);
        }
        
        
        public static function couponcode ($_value = NULL, $_min = 1, $_max = 40) 
        {
            
            $_min = (is_int($_min)) ? (int)$_min : 1;
            $_max = (is_int($_max)) ? (int)$_max : 20;
            
            $_regexp = '/^[A-Za-z0-9]{' . $_min . ',' . $_max . '}$/';
            
            return filter_var($_value, FILTER_VALIDATE_REGEXP, array('options'=>array('regexp' => $_regexp)));
        }
        
        
        /*
        public static function couponcode ($_value = NULL, $_min = 1, $_max = 40) 
        {
            return self::couponCode($_value, $_min, $_max);
        }
        */
        
        public static function referencecode ($_value = NULL, $_min = 1, $_max = 40) 
        {
            
            $_min = (is_int($_min)) ? (int)$_min : 1;
            $_max = (is_int($_max)) ? (int)$_max : 40;
            
            $_regexp = '/^[A-Za-z0-9\-_]{' . $_min . ',' . $_max . '}$/';
            
            return filter_var($_value, FILTER_VALIDATE_REGEXP, array('options'=>array('regexp' => $_regexp)));
        }
        
        
        public static function postalcode ($_value = NULL, $_min = 1, $_max = 40) 
        {
            
            $_min = (is_int($_min)) ? (int)$_min : 1;
            $_max = (is_int($_max)) ? (int)$_max : 20;
            
            $_regexp = '/^[A-Za-z0-9]{' . $_min . ',' . $_max . '}$/';
            
            return filter_var($_value, FILTER_VALIDATE_REGEXP, array('options'=>array('regexp' => $_regexp)));
        }
        
        
        
        public static function postcode ($_value = NULL, $_min = 1, $_max = 40) 
        {
            return self::postalcode($_value, $_min, $_max);
        }
         
         
        

        
        public static function iso ($_value = NULL, $_min = 1, $_max = 3) 
        {
            
            $_min = (is_int($_min)) ? (int)$_min : 1;
            $_max = (is_int($_max)) ? (int)$_max : 3;
            
            $_regexp = '/^[A-Za-z]{' . $_min . ',' . $_max . '}$/';
            
            $_value = filter_var($_value, FILTER_VALIDATE_REGEXP, array('options'=>array('regexp' => $_regexp)));
            
            if ($_value !== FALSE) return strtoupper ($_value);
            else return $_value;
        }
        
        
        
        
        
        

        
        public static function iso2 ($_value = NULL) 
        {
            
            return self::iso($_value, 2, 2);
            
        }
        
        
        
        

        
        public static function iso3 ($_value = NULL) 
        {
            
            return self::iso($_value, 3, 3);
            
        }
        
        

        
        public static function submit () 
        {
            
            return TRUE;
            
        }

        
        /**
         * 
         * @param string $_value
         * @param int $_min
         * @param int $_max
         * @return string
         * 
         */
        public static function name ($_value, $_min = NULL, $_max = NULL) 
        {
            
            $_min = (is_int($_min)) ? (int)$_min : 1;
            $_max = (is_int($_max)) ? (int)$_max : 100;
            
            $_regexp = '/^[-\s\.A-Za-z0-9]{' . $_min . ',' . $_max . '}$/';
            
            return filter_var($_value, FILTER_VALIDATE_REGEXP, array('options'=>array('regexp' => $_regexp)));
            
        }
        
        
        
        /**
         * 
         * @param string $_value
         * @param int $_min
         * @param int $_max
         * @return string
         * 
         */
        public static function firstname ($_value, $_min = NULL, $_max = NULL) 
        {
            
            
            return self::name($_value, $_min, $_max);
            
        }
        
        
        
        

        /**
         * 
         * @param string $_value
         * @param int $_min
         * @param int $_max
         * @return string
         * 
         */
        public static function lastname ($_value, $_min = NULL, $_max = NULL) 
        {
            
            
            return self::name($_value, $_min, $_max);
            
        }
        
        
        
        
        
        
        /*
         * 
         * public: [ folderName ]
         * 
         * 
         */
        
        
        public static function foldername ($_value, $_min = NULL, $_max = NULL) 
        {
            
            $_min = (is_int($_min)) ? (int)$_min : 1;
            $_max = (is_int($_max)) ? (int)$_max : 100;
            
            $_regexp = '/^[-\.A-Za-z0-9_]{' . $_min . ',' . $_max . '}$/';
            
            return filter_var($_value, FILTER_VALIDATE_REGEXP, array('options'=>array('regexp' => $_regexp)));
            
        }
        
        
        
        
        
        public static function alphanum ($_value, $_min = NULL, $_max = NULL) 
        {
            
            $_min = (is_int($_min)) ? (int)$_min : 1;
            $_max = (is_int($_max)) ? (int)$_max : 255;
            
            $_regexp = '/^[A-Za-z0-9]{' . $_min . ',' . $_max . '}$/';
            
            return filter_var($_value, FILTER_VALIDATE_REGEXP, array('options'=>array('regexp' => $_regexp)));
            
        }
        
        
        
        
        
        /*
         * 
         * public: [ alpha ]
         * 
         * 
         */
        
        
        public static function alpha ($_value, $_min = NULL, $_max = NULL) 
        {
            
            $_min = (is_int($_min)) ? (int)$_min : 1;
            $_max = (is_int($_max)) ? (int)$_max : 255;
            
            $_regexp = '/^[A-Za-z]{' . $_min . ',' . $_max . '}$/';
            
            return filter_var($_value, FILTER_VALIDATE_REGEXP, array('options'=>array('regexp' => $_regexp)));
            
        }
        
        
        
        
        public static function num ($_value, $_min = NULL, $_max = NULL) 
        {
            
            $_min = (is_int($_min)) ? (int)$_min : 1;
            $_max = (is_int($_max)) ? (int)$_max : 100;
            
            $_regexp = '/^[0-9]{' . $_min . ',' . $_max . '}$/';
            
            return filter_var($_value, FILTER_VALIDATE_REGEXP, array('options'=>array('regexp' => $_regexp)));
            
        }
        
        
        
        
        
        public static function token ($_value = NULL, $_min = 5, $_max = 100) 
        {
            
            $_min = (is_int($_min)) ? (int)$_min : 5;
            $_max = (is_int($_max)) ? (int)$_max : 100;
            
            $_regexp = '/^[A-Za-z0-9\-]{' . $_min . ',' . $_max . '}$/';
            
            return filter_var($_value, FILTER_VALIDATE_REGEXP, array('options'=>array('regexp' => $_regexp)));
        }
        
        
        
        
        public static function scope ($_value = NULL) 
        {
            
            
            $_regexp = '/^(([A-Za-z0-9\-]+\/?){1,30}\*?)$/';
            
            return filter_var($_value, FILTER_VALIDATE_REGEXP, array('options'=>array('regexp' => $_regexp)));
        }
        
        
        
        public static function handle ($_value = NULL) 
        {
            
            
            $_regexp = '/^(([A-Za-z0-9]+)(-[A-Za-z0-9]+){0,10})$/';
            
            return filter_var($_value, FILTER_VALIDATE_REGEXP, array('options'=>array('regexp' => $_regexp)));
        }

        
        /*
         * 
         * public: [ email ]
         * 
         * 
         */
        
        
        public static function email ($_value, $_SKIP_ON_FAILED = FALSE) 
        {
            if (is_array($_value)) 
            {
                foreach ($_value as $k=>$v) 
                {
                    if (is_string($v)) $v = trim ($v);
                    $v = filter_var($v, FILTER_VALIDATE_EMAIL);
                    
                    if ($v === FALSE && $_SKIP_ON_FAILED) continue;
                        
                    $_value[$k] = $v;
                }
            }
            
            else {
                if (is_string($_value)) $_value = trim ($_value);
                $_value = filter_var($_value, FILTER_VALIDATE_EMAIL);
            }
            
            return (empty($_value) || $_value === FALSE) ? FALSE : $_value;
            
        }
        
        
        
        public static function myemail ($_value, $_SKIP_ON_FAILED = FALSE) 
        {
            return self::email($_value, $_SKIP_ON_FAILED);
        }
        
        
        
        
        /*
         * 
         * public: [ password ]
         * 
         * 
         */
        
        
        public static function password ($_value, $_min = NULL, $_max = NULL) 
        {
            
            $_min = (is_int($_min)) ? (int)$_min : 5;
            $_max = (is_int($_max)) ? (int)$_max : 100;
            
            $_regexp = '/^[-_\W\s\.A-Za-z0-9]{' . $_min . ',' . $_max . '}$/';
            
            return filter_var($_value, FILTER_VALIDATE_REGEXP, array('options'=>array('regexp' => $_regexp)));
            
        }
        
        
        
        
        public static function mypassword ($_value, $_min = NULL, $_max = NULL) 
        {
            return self::password($_value, $_min, $_max);
            
        }
        
        
        
        
        
        /**
         * 
         * @param string $_value
         * @param integer $_min Minimum number of digits
         * @param integer $_max Maximum number of digits
         * @return boolean
         */
        public static function phone ($_value, $_min = NULL, $_max = NULL) 
        {
            
            $_min = (is_int($_min)) ? (int)$_min : 5;
            $_max = (is_int($_max)) ? (int)$_max : 20;
            
            $_regexp = '/^[+]?[0-9]{' . $_min . ',' . $_max . '}$/';
            
            return filter_var($_value, FILTER_VALIDATE_REGEXP, array('options'=>array('regexp' => $_regexp)));
            
        }
        
        
        
        
        

        
        public static function address ($_value, $_min = NULL, $_max = NULL) 
        {
            $_value = trim($_value);
            
            $_min = (is_int($_min)) ? (int)$_min : 1;
            $_max = (is_int($_max)) ? (int)$_max : 200;
            
            $_regexp = '/^[-\s\.A-Za-z0-9#,]{' . $_min . ($_min===$_max ? '' : ',' . $_max) . '}$/';
            
            return filter_var($_value, FILTER_VALIDATE_REGEXP, array('options'=>array('regexp' => $_regexp)));
            
        }
        
        

        
        public static function match ($_value, $_expression = '/.*/') 
        {
            if (!is_string($_value)) return false;

            if (!is_string($_expression)) throw new Error ("Argument 2 must be a string.");

            
            return filter_var($_value, FILTER_VALIDATE_REGEXP, array('options'=>array('regexp' => $_expression)));
            
        }
        
        
        
        
        /*
         * 
         * public: [ url ]
         * 
         * 
         */
        
        
        public static function url ($_value, $_path_required = FALSE, $_query_required = FALSE) 
        {
            
            $_path_required = is_bool($_path_required) ? $_path_required : FALSE;
            $_query_required = is_bool($_query_required) ? $_query_required : FALSE;
            
            $_options = array ('flags'=>array());
            
            if ($_path_required) $_options['flags'][] = FILTER_FLAG_PATH_REQUIRED;
            if ($_query_required) $_options['flags'][] = FILTER_FLAG_QUERY_REQUIRED;
            
            return filter_var($_value, FILTER_VALIDATE_URL, $_options);
            
        }
        
        
        
        
        





}
	
	
	
	
