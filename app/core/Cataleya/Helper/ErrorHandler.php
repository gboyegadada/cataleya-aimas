<?php


namespace Cataleya\Helper;



/*

CLASS ERROR_HANDLER



* Native PHP Error Logger *

	bool error_log ( string $message [, int $message_type = 0 [, string $destination [, string $extra_headers ]]] )
	message: The error message that should be logged.
	message_type:
					[0] -  message is sent to PHP's system logger, using the Operating System's 
					system logging mechanism or a file, depending on what the error_log configuration 
					directive is set to. This is the default option.
					
					[1] -  message is sent by email to the address in the destination parameter. 
					This is the only message type where the fourth parameter, extra_headers is used.
					
					[2]	-	No longer an option.
					
					[3]	-	message is appended to the file destination. A newline is not automatically 
					added to the end of the message string.
					
					[4]	-	message is sent directly to the SAPI logging handler.

*/



class ErrorHandler implements \Cataleya\System\Event\Observable  
{
	
	//////////// ERROR HANDLER /// LOGS ERRORS QUIETLY //////////
	
	private static $_instance;
	protected $_errors = array();
	private $_listeners = array();
        private $_logger;


    protected static $_events = [
            //'system/error'
        ];


        private function __construct () 
	{
		
		
		
	}
	
	
	public function _destruct () 
	{
		
		
		
	}
	
	
	public static function getInstance()
	{
		
		if (!isset(self::$_instance))
		{
			
			// Create instance...
			$object =  __CLASS__;
			self::$_instance = new $object ();
		
		}
		
		return self::$_instance;
		
	}
	

        
    
    /**
     * getEvents
     * 
     * @return array
     */
    public static function getEvents() {

        return self::$_events;
    }
        
	
	
	
	public function triggerError ($error='Error not specified') 
	{
		
		
		$error_text = (is_array($error)) ? implode(', ', $error) : $error;
		$error_text = str_replace("\n", "", $error_text);
		$error_text = 'Error: //' . str_replace("\r", "", $error_text);
		
                
                \Cataleya\System\Event::notify(
                        'system/error', 
                        $this, 
                        array(
                            'scope'=>'system/error', 
                            'logger_id'=>'error.log', 
                            'blob'=>$error_text
                        ));
		
		// another way to call error_log():
		//error_log("You messed up!", 3, "/var/tmp/my-errors.log");
		
		$this->_errors[] = $error_text;
		return FALSE;
		
	}

	
	public static function triggerException ($error='Error not specified') 
	{
		
                // Roll back DB tranx
                \Cataleya\Helper\DBH::getInstance()->rollback();
                
		$error_text = (is_array($error)) ? implode(', ', $error) : $error;
		$error_text = str_replace("\n", "", $error_text);
		$error_text = 'Error: //' . str_replace("\r", "", $error_text);
		
                
                \Cataleya\System\Event::notify(
                        'system/error', 
                        $this, 
                            array(
                                'scope'=>'system/error',  
                                'logger_id'=>'error.log', 
                                'blob'=>$error_text, 
                                'error_arg'=>$error
                            )
                        );
		
		// another way to call error_log():
		//error_log("You messed up!", 3, "/var/tmp/my-errors.log");
		
		if (defined('DEBUG_MODE') && DEBUG_MODE === TRUE) die('Houston! We have a problem -- ' . $error_text); // throw new Exception ($error_text, 0);
		else throw new Exception ('', 0);
		
	}
        
        
        
/*
        // UNIVERSAL ERROR HANDLER (JUST ERROR MESSAGES)
        function _catch_error($message, $line = 0, $die = true) {

                $msg_str = (is_array($message)) ? ('@ ' . implode(', ', $message) . ' @') : $message;

                // Will decide what to do with error messages later...
                if (OFOFO) {
                        switch (OUTPUT) {
                                case 'JSON':
                                        _json_error($msg_str, $line, $die);
                                break;
                                case 'PLAIN_TEXT':
                                        _text_error($msg_str, $line, $die);
                                break;
                                default:
                                        _html_error($msg_str, $line, $die);
                                break;
                        }


                } else if (isset($die) && $die === true) {

                exit(1); // exit with error code
                }

        }



        // OUTPUT ERROR AS HTML...
        function _html_error($message, $line, $die) {
                ?>

                <h2><?=$line?></h2>
            <p>
            <?=$message?>

            </p>

            <?php

        if (isset($die) && $die != true) return;
                else exit(1); // exit with error code

        }


        // OUTPUT ERROR AS PLAIN TEXT...
        function _text_error($message, $line, $die) {


        if (isset($die) && $die != true) return;
                else exit(1); // exit with error code
        }



        // SEND ERROR MESSAGE TO CLIENT AS JSON...
        function _json_error ($message, $line = 0, $die = true) {
        $msg_str = (is_array($message)) ? ('@ ' . implode(', ', $message) . ' @') : $message;

        $line = (OFOFO && isset($line)) ? $line : '';
        $msg_str = (OFOFO) ? $msg_str : '';

        $json_reply = array (
                        "status"=>'error', 
                        "line"=>$line, 
                        "message"=>$msg_str . $line
                        );

        echo (json_encode($json_reply));
        if (isset($die) && $die != true) return;
                else exit(1); // exit with error code


        }


        // USE AFTER decode_json ...
        function get_json_last_error () {

        $_json_last_error = '';

        switch (json_last_error()) {
                case JSON_ERROR_NONE:
                    $_json_last_error = ' - No errors';
                break;
                case JSON_ERROR_DEPTH:
                    $_json_last_error = ' - Maximum stack depth exceeded';
                break;
                case JSON_ERROR_STATE_MISMATCH:
                    $_json_last_error = ' - Underflow or the modes mismatch';
                break;
                case JSON_ERROR_CTRL_CHAR:
                    $_json_last_error = ' - Unexpected control character found';
                break;
                case JSON_ERROR_SYNTAX:
                    $_json_last_error = ' - Syntax error, malformed JSON';
                break;
                case JSON_ERROR_UTF8:
                    $_json_last_error = ' - Malformed UTF-8 characters, possibly incorrectly encoded';
                break;
                default:
                    $_json_last_error = ' - Unknown error';
                break;
            }

        return $_json_last_error;

        }



 */       
        

	
	
	
}
	
	
	
	
