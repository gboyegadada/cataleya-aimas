<?php

namespace Cataleya\Helper;


class SimpleList implements \Iterator {

    protected $_data = [];
    protected $_collection = [];
    protected $_position = 0;
    protected $_population = 0;





    public function __construct() {

    }

    public function rewind() {

        $this->_position = 0;
    }

    public function current() 
    {

    }

    public function key() {
        return $this->_position;
    }

    public function next() {
        ++$this->_position;
    }

    public function valid() {
        
        return (isset($this->_collection[$this->_position]) && $this->_position < $this->_page_stop) ? TRUE : FALSE;
    }




    /**
     * make
     *
     * @param array $_items
     * @param string $_type product | customer | plain | null
     * @return \Cataleya\Helper\SimpleList
     */
    public static function make (array $_items, $_type = 'plain') 
    {

        $dbh = \Cataleya\Helper\DBH::getInstance();

        static $insert_handle;

        if (empty($insert_handle))
        {
                $insert_handle = $this->prepare(''
                        . 'INSERT INTO collections (count) '
                        . 'VALUES (0)'
                        . '');
        }
        
        
        if (!$insert_handle->execute()) throw new \Cataleya\Error('DB Error: '.implode(', ',$insert_handle->errorInfo()));

        return self::get($dbh->lastInsertId(), $_type);
    }


    
    public static function get ($_list_id, $_type) 
    {

    }


    public function destroy () 
    {

    }


    public function has ($_item) 
    {

    }


    public function add ($_item) 
    {

    }


    public function remove ($_item) 
    {

    }



    public function getLength () 
    {

    }


    public function isEmpty () 
    {

    }




}
