<?php



namespace Cataleya\Helper;




/*
 *
 * 	@Package: Cataleya
 * 	@Class: \Cataleya\Helper\NVPParser
 *
 * 	(c) 2012 Fancy Paper Planes
 *
 *
 *
 */

class NVPParser {
    


    static protected $_last_curl_error = '';




    /*
     * 
     * [ toString ]
     * ________________________________________________
     * 
     * @args:
     * 
     * Array $NVP_Array is an associative array to be converted to a URI string
     * Returns data as URI string (with values properly encoded)
     * 
     */
    
    
    static public function toString ($NVP_Array = array(), $encode_vals = TRUE)
    {
        $pairs = array();
        
       
        // Encode values ??
        if ($encode_vals) foreach ($NVP_Array as $name => $value) $pairs[] = strtoupper($name) . '=' . urlencode($value);
        
        // Skip encode...
        else foreach ($NVP_Array as $name => $value) $pairs[] = strtoupper($name) . '=' . $value;
        
        return implode('&', $pairs);
        
        
    }
    
   
    
    
    
    /*
     * 
     * [ toArray ]
     * ________________________________________________
     * 
     * @args:
     * 
     * String $NVP_String is a URI string
     * Returns data as associative array (with values decoded)
     * 
     */
    
    
    static public function toArray ($NVP_String = '')
    {
        if ($NVP_String === '') return array ();
        
	// Extract the response details.
        $NVP_Array = array ();
        parse_str($NVP_String, $NVP_Array);
        
        
        return $NVP_Array;
        
        
        
    }
    
    
    
    
    
    /*
     * 
     * [ toHTML ]
     * ________________________________________________
     * 
     * @args:
     * 
     * $NVP_String_or_Array is an associative array OR URI string
     * Returns data as HTML INPUT FORM ELEMENTS (as string)
     * 
     * Note: You can set second arg to TRUE to get elements as array.
     * 
     */
    static public function toHTML ($NVP_String_or_Array = NULL, $return_array = FALSE)
    {
        if ($NVP_String_or_Array === NULL) return '';
        
       
        // Name-Value Pairs..
        $NVP = (is_string($NVP_String_or_Array)) ? self::toArray ($NVP_String_or_Array) : $NVP_String_or_Array;
        
        // Form elements...
        $form_elements = array();
        
        
        foreach ($NVP as $key=>$val)
        {
            $form_elements[] = '<input type="hidden" name="' . $key . '" value="' . str_replace('"', '\"', $val) . '" />';
        }
        
        
        return ($return_array) ? $form_elements : implode(" \n", $form_elements);
        
        
        
    }
    
    
    
    

    /*
     * 
     * [ curlRequest ]
     * ________________________________________________
     * 
     * @args:
     * 
     * $URI_String should be url_string (exactly as you would type/paste it in a browser address bar)
     * 
     * Note: Parses response by default. Pass FALSE as second argument to get raw string.
     * 
     */
    public static function curlRequest ($URI_String='', $parse_response=true, $REQUEST_METHOD='POST', array $_headers = NULL)
    {
        if ($URI_String === '') return FALSE;

        list ($API_Endpoint, $nvpreq) = explode('?', $URI_String);

        if (empty($API_Endpoint) || empty($nvpreq)) return FALSE;

        //cURL settings

        switch ($REQUEST_METHOD)
        {
            
        case 'POST':
            $curlOptions = [

               CURLOPT_URL => $API_Endpoint,
               CURLOPT_VERBOSE => 1,
               CURLOPT_USERAGENT => $_SERVER['HTTP_USER_AGENT'], 

               CURLOPT_SSL_VERIFYPEER => false, // true,
               CURLOPT_SSL_VERIFYHOST => 2,
               CURLOPT_CAINFO => ROOT_PATH . 'app/etc/cacert.pem', //CA cert file
               CURLOPT_RETURNTRANSFER => 1, 
               CURLOPT_RETURNTRANSFER => 1, 
               CURLOPT_POST => 1,
               CURLOPT_POSTFIELDS => $nvpreq

            ];
            break;
        
        case 'GET':
            $curlOptions = [

               CURLOPT_URL => $URI_String, 
               CURLOPT_HTTPGET => TRUE, 
               CURLOPT_CUSTOMREQUEST => 'GET', 

               CURLOPT_VERBOSE => 1,
               CURLOPT_SSL_VERIFYPEER => true,
               CURLOPT_SSL_VERIFYHOST => 2,
               CURLOPT_CAINFO => ROOT_PATH . 'app/etc/cacert.pem', //CA cert file
               CURLOPT_RETURNTRANSFER => 1

            ];
            break;
        
        default:
            return false;
            break;
        
        }
        
        
        if (!empty($_headers) && is_array($curlOptions)) {
            $curlOptions[ CURLOPT_HTTPHEADER ] = $_headers;
        }



        $ch = curl_init();

        curl_setopt_array($ch,$curlOptions);

           

        // Get response from the server.
        $httpResponse = curl_exec($ch);

        if($httpResponse) 
        {
            curl_close($ch);
            return ($parse_response) ? self::toArray($httpResponse) : $httpResponse;
        }

        else 
        {
            self::$_last_curl_error = "Error: ".curl_error($ch).' (code: '.curl_errno($ch).')';
            
            curl_close($ch);
            
            return FALSE;
        }
        



    }
 
 
    
    
    /*
     * 
     * 
     * [ getLastCurlError ]
     * 
     * 
     * 
     */
    
    static public function getLastCurlError ()
    {
        return self::$_last_curl_error;
    }
    
    
    
    
}




