<?php


namespace Cataleya\Helper;
use \Cataleya\Error;

/**
 * Path
 *
 * @author Gboyega Dada <gboyega@fpplabs.com>
 */
class Path {



    static $_registry = [

        'dash.*' => 'Helper\Path::_system', 
        'shop.*' => 'Helper\Path::_system', 
        'app.*' => 'Helper\Path::_system', 
        'root' => 'Helper\Path::_system', 
        'logs' => 'Helper\Path::_system', 
        'lib' => 'Helper\Path::_system', 
        'var' => 'Helper\Path::_system', 
        'etc' => 'Helper\Path::_system', 
        'uploads' => 'Helper\Path::_system', 
        'cache' => 'Helper\Path::_system', 
        'packages' => 'Helper\Path::_system', 
        'skin' => 'Helper\Path::_system', 
        'inc' => 'Helper\Path::_system', 
        'includes' => 'Helper\Path::_system', 
        
        
        /* ---------- PLUGINS ----------------------- */
        
        'this.*' => 'Helper\Path::_this', 

    ];

    /**
     * load       This is to help decouple plugin apps from the core.
     *
     *               DO NOT: \Cataleya\Catalog\Product::load($_id) 
     *               DO NOT: \Cataleya\Helper\Model::load('product', $_id) 
     *
     *               DO: __('product', $_id, $_arg2, $_arg3...) instead.
     *
     *
     * @param string $_model
     * @access protected
     * @return void
     */
    public static function load ($_model = '') 
    {


        $_route = null;
        foreach (self::$_registry as $_scope=>$_controller) 
        {

            if (self::inScope($_model, $_scope)) 
            {
                $_route = $_controller; break;
            }

        }

        if (empty($_route)) return null;

        preg_match ('/^(?<c>[_A-Za-z\\\]+)(\:\:(?<m>[_A-Za-z]+))?$/', $_route, $_);
        $c = '\Cataleya\\'.$_['c']; 
        if (!class_exists($c)) throw new Error ('Model class [' . $c . '] not found.');

        if (isset($_['m']) && method_exists($c, $_['m'])) $m = $_['m'];
        else if (method_exists($c, 'load')) $m = 'load';
        else throw new Error ('No "load" method in model class [' . $c . '].');


        return forward_static_call_array([$c, $m], func_get_args()); 


    }




    /**
     * inScope  Returns true if a model (ex: 'catalog.product') is part 
     *          of a scope (ex: 'catalog.*')
     *
     * @param string $_model
     * @param string $_scope
     * @return boolean
     */
    private static function inScope($_model, $_scope) 
    {

        if (!is_string($_model)) throw new Error ('Argument 1 must be a string.');
        
        if (
            !is_string($_scope) || 
            preg_match('!^(?<sc>([a-zA-Z\-_]+)(\.[a-zA-Z\-_]+){0,20})(\.\*)?$!', $_scope, $_) < 1
        ) throw new Error ('Invalid scope.');

        return (strpos($_model, $_['sc'].'.') === 0 || $_model === $_['sc']) ? true : false;


    }






    /**
     * _system
     *
     * @return mixed
     */
    protected static function _system () 
    {

        $_args = func_get_args();
        $_model = array_shift($_args);

        
        if (self::inScope($_model, 'shop.*')) 
        {
            $_Shop = __('shop');
            $_shop_theme_id = (!empty($_Shop)) 
                ? $_Shop->getThemeID() 
                : __('sys')->getDefaultShopFrontThemeID();
        }

        $_dash_theme_id = __('sys')->getDashboardThemeID();

        
        switch ($_model) 
        {

            case 'root': 
                return ROOT_PATH;
            case 'var': return  ROOT_PATH . 'var';
            case 'logs': 
                if (!file_exists(ROOT_PATH . 'var/logs')) mkdir (ROOT_PATH . 'var/logs', 0777);
                return ROOT_PATH . 'var/logs';
            case 'uploads': 
                if (!file_exists(ROOT_PATH . 'var/uploads')) mkdir (ROOT_PATH . 'var/uploads', 0777);
                return ROOT_PATH . 'var/uploads';
            case 'cache': 
                if (!file_exists(ROOT_PATH . 'var/cache')) mkdir (ROOT_PATH . 'var/cache', 0777);
                return  ROOT_PATH . 'var/cache';
            case 'lib': return  ROOT_PATH . 'library';
            case 'app.plugins': return  ROOT_PATH . 'app/plugins';
            case 'app.core': return  ROOT_PATH . 'app/core';
            case 'etc': return  ROOT_PATH . 'app/etc';
            case 'packages': 
                if (!file_exists(ROOT_PATH . 'var/packages')) mkdir (ROOT_PATH . 'var/packages', 0777);
                return  ROOT_PATH . 'var/packages';
            case 'skin': return  ROOT_PATH . 'skin';
            case 'shop.skin': return  ROOT_PATH . 'skin/ShopFront/'.$_shop_theme_id;
            case 'shop.skin.assets': return  ROOT_PATH . 'skin/ShopFront/'.$_shop_theme_id.'/assets';
            case 'dash.skin': return  ROOT_PATH . 'skin/Dashboard/'.$_dash_theme_id;
            case 'dash.skin.assets': return  ROOT_PATH . 'skin/Dashboard/'.$_dash_theme_id.'/assets';
            case 'includes': return  ROOT_PATH . 'includes';
            case 'inc': return  ROOT_PATH . 'includes';
            case 'lib': return  ROOT_PATH . 'library';
            default: return null;

        }

    }





    /**
     * _this
     *
     * @return mixed
     */
    protected static function _this () 
    {

        $_args = func_get_args();
        $_model = array_shift($_args);
        $_app_handle = \Cataleya\Plugins\Package::getCurrentAppHandle();

        if (empty($_app_handle)) throw new Error ('Unable to retrieve current App. You must call this method within your plugin code.');

        switch ($_model) 
        {

            case 'this': return __path('root').'/plugins/'. \Cataleya\Plugins\Package::getAppNameSpace($_app_handle);
            case 'this.logs': 
                $_path = __path('logs');
                if (!file_exists($_path.$_app_handle)) mkdir ($_path.$_app_handle, 0777);
                return $_path.'/'.$_app_handle;
            case 'this.cache': 
                $_path = __path('cache').'/';
                if (!file_exists($_path.$_app_handle)) mkdir ($_path.$_app_handle, 0777);
                return $_path.$_app_handle;
            case 'this.etc': 
                $_path = __path('etc').'/plugins/';
                if (!file_exists($_path)) mkdir ($_path, 0777);
                if (!file_exists($_path.$_app_handle)) mkdir ($_path.$_app_handle, 0777);
                
                return $_path.$_app_handle;
                
            default: return null;

        }

    }
    

    
    




}

