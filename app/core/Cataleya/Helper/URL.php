<?php

namespace Cataleya\Helper;
use \Cataleya\Error;

/**
 * URL
 *
 * @author Gboyega Dada <gboyega@fpplabs.com>
 */
class URL {
    
    

    static $_registry = [
        'catalog.*' => 'Helper\URL::_catalog', 
        'customer.*' => 'Helper\URL::_customer', 

        /* ------------ FRONT -------------------- */
        'shop.*' => 'Helper\URL::_shop_front', 
        'checkout.*' => 'Helper\URL::_checkout', 

        'dash.*' => 'Helper\URL::_dash', 
        
        
        /* ---------- PLUGINS ----------------------- */
        
        'this.*' => 'Helper\URL::_this', 

    ];

    /**
     * load       This is to help decouple plugin apps from the core.
     *
     *               DO NOT: \Cataleya\Catalog\Product::load($_id) 
     *               DO NOT: \Cataleya\Helper\Model::load('product', $_id) 
     *
     *               DO: __('product', $_id, $_arg2, $_arg3...) instead.
     *
     *
     * @param string $_model
     * @access protected
     * @return void
     */
    public static function load ($_model = '') 
    {


        $_route = null;
        foreach (self::$_registry as $_scope=>$_controller) 
        {

            if (self::inScope($_model, $_scope)) 
            {
                $_route = $_controller; break;
            }

        }

        if (empty($_route)) return null;

        preg_match ('/^(?<c>[_A-Za-z\\\]+)(\:\:(?<m>[_A-Za-z]+))?$/', $_route, $_);
        $c = '\Cataleya\\'.$_['c']; 
        if (!class_exists($c)) throw new Error ('Model class [' . $c . '] not found.');

        if (isset($_['m']) && method_exists($c, $_['m'])) $m = $_['m'];
        else if (method_exists($c, 'load')) $m = 'load';
        else throw new Error ('No "load" method in model class [' . $c . '].');


        return forward_static_call_array([$c, $m], func_get_args()); 


    }




    /**
     * inScope  Returns true if a model (ex: 'catalog.product') is part 
     *          of a scope (ex: 'catalog.*')
     *
     * @param string $_model
     * @param string $_scope
     * @return boolean
     */
    private static function inScope($_model, $_scope) 
    {

        if (!is_string($_model)) throw new Error ('Argument 1 must be a string.');
        
        if (
            !is_string($_scope) || 
            preg_match('!^(?<sc>([a-zA-Z\-_]+)(\.[a-zA-Z\-_]+){0,20})(\.\*)?$!', $_scope, $_) < 1
        ) throw new Error ('Invalid scope.');

        return (strpos($_model, $_['sc'].'.') === 0 || $_model === $_['sc']) ? true : false;


    }




    /**
     * _this
     *
     * @return mixed
     */
    protected static function _this () 
    {

        $_args = func_get_args();
        $_model = array_shift($_args);
        $_app_handle = \Cataleya\Plugins\Package::getCurrentAppHandle();

        if (empty($_app_handle)) throw new Error ('Unable to retrieve current App. You must call this method within your plugin code.');

        switch ($_model) 
        {

            case 'this.dash': return ROOT_PATH.'skin/Apps/'.$_app_handle.'/assets';
            case 'this.shop-front': return ROOT_PATH.'skin/Apps/'.$_app_handle.'/assets';
            case 'this.skin.assets': return ROOT_PATH.'skin/Apps/'.$_app_handle.'/assets';
            case 'this.icons': return ROOT_PATH.'ui/app-icons/'.$_app_handle;
            default: return null;

        }

    }
    







    /**
     * _checkout
     *
     * @return mixed
     */
    protected static function _checkout () 
    {

        $_args = func_get_args();
        $_model = array_shift($_args);
        $_Shop = \__('shop');

        if (empty($_Shop)) throw new Error ('You can only use this from the a shop front (during an actual shopping session).');


        $_base = $_Shop->getShopLanding();
        $_model = str_replace('shop.', '', $_model);

        switch ($_model) 
        {

            case 'checkout': return $_base . 'checkout';
            case 'checkout.payment': return $_base . 'checkout/payment';
            case 'checkout.complete': return $_base . 'checkout/complete';
            default: return null;

        }

    }
    
    
    
    /**
     * _shop_front
     *
     * @return mixed
     */
    protected static function _shop_front () 
    {

        $_args = func_get_args();
        $_model = array_shift($_args);
        $_Shop = \__('shop');

        if (empty($_Shop)) throw new Error ('You can only use this from the a shop front (during an actual shopping session).');


        $_landing = $_Shop->getShopLanding();

        if (self::inScope($_model, 'shop.page.*')) 
        {
            $_pg = ($_model === 'shop.page' && isset($_args[0])) 
                ? $_args[0] 
                : str_replace('shop.page.', '', $_model);
            return $_base . (!empty($_pg) ? '/page/'.$_pg : '');

        } 
        else if (self::inScope($_model, 'shop.checkout.*')) 
        {
            return forward_static_call_array([__CLASS__, '_checkout'], func_get_args()); 
        }

        switch ($_model) 
        {

            case 'shop': return $_landing;
            case 'shop.cart': return $_base . '/cart';
            case 'shop.wishlist': return $_base . '/wishlist';
            default: return null;

        }

    }
    
    
    
    
    
    
    
    

    /**
     * _dash
     *
     * @return mixed
     */
    protected static function _dash () 
    {

        $_args = func_get_args();
        $_model = array_shift($_args);

        

        $_dash_theme_id = __('sys')->getDashboardThemeID();
        $_root = (SSL_REQUIRED ? 'https://' : 'http://').HOST.'/'.ROOT_URI;

        
        switch ($_model) 
        {

            case 'dash': return  $_root.'dashboard/';
            case 'dash.skin.assets': return  $_root . 'skin/Dashboard/'.$_dash_theme_id.'/assets';
            default: return null;

        }

    }

    
    
}
