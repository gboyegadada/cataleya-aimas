<?php


namespace Cataleya\Helper;



/*


CLASS BCRYPT


You may use this code as such:
_________________________________________

$bcrypt = \Cataleya\Helper\Bcrypt::getInstance(15);

$hash = $bcrypt->hash('password');
$isGood = $bcrypt->verify('password', $hash);


*/




class Bcrypt {
	
	private $rounds;
	private static  $_instance;


	private function __construct($rounds = 12) {
		
		if(CRYPT_BLOWFISH != 1) {
		throw new Exception("bcrypt not supported in this installation. See http://php.net/crypt");
		}
		
		$this->rounds = $rounds;
	}
	
	
	
	public static function getInstance ($rounds = 12)
	{
		if (empty(self::$_instance)) {
			
			$object = __CLASS__;
			self::$_instance = new $object ($rounds);
		}
		
		return self::$_instance;
	}
	
	

	public function hash($input) {
		$hash = crypt($input, $this->getSalt());

		if(strlen($hash) > 13)
			return $hash;

		return false;
	}

	public function verify($input, $existingHash) {
		$hash = crypt($input, $existingHash);

		return $hash === $existingHash;
	}

	private function getSalt() {
		$salt = sprintf('$2a$%02d$', $this->rounds);

		$bytes = $this->getRandomBytes(16);

		$salt .= $this->encodeBytes($bytes);

		return $salt;
	}

	private $randomState;
	private function getRandomBytes($count) {
		$bytes = '';

		if(function_exists('openssl_random_pseudo_bytes') &&
				(strtoupper(substr(PHP_OS, 0, 3)) !== 'WIN')) { // OpenSSL slow on Win
			$bytes = openssl_random_pseudo_bytes($count);
		}

		if($bytes === '' && is_readable('/dev/urandom') &&
			 ($hRand = @fopen('/dev/urandom', 'rb')) !== FALSE) {
			$bytes = fread($hRand, $count);
			fclose($hRand);
		}

		if(strlen($bytes) < $count) {
			$bytes = '';

			if($this->randomState === null) {
				$this->randomState = microtime();
				if(function_exists('getmypid')) {
					$this->randomState .= getmypid();
				}
			}

			for($i = 0; $i < $count; $i += 16) {
				$this->randomState = md5(microtime() . $this->randomState);

				if (PHP_VERSION >= '5') {
					$bytes .= md5($this->randomState, true);
				} else {
					$bytes .= pack('H*', md5($this->randomState));
				}
			}

			$bytes = substr($bytes, 0, $count);
		}

		return $bytes;
	}

	private function encodeBytes($input) {
		// The following is code from the PHP Password Hashing Framework
		$itoa64 = './ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';

		$output = '';
		$i = 0;
		do {
			$c1 = ord($input[$i++]);
			$output .= $itoa64[$c1 >> 2];
			$c1 = ($c1 & 0x03) << 4;
			if ($i >= 16) {
				$output .= $itoa64[$c1];
				break;
			}

			$c2 = ord($input[$i++]);
			$c1 |= $c2 >> 4;
			$output .= $itoa64[$c1];
			$c1 = ($c2 & 0x0f) << 2;

			$c2 = ord($input[$i++]);
			$c1 |= $c2 >> 6;
			$output .= $itoa64[$c1];
			$output .= $itoa64[$c2 & 0x3f];
		} while (1);

		return $output;
	}
        
        
        
        
        
        
        /*
         * 
         * 
         * This bit is for two encryption...
         * 
         * 
         */
        
        const CYPHER = MCRYPT_RIJNDAEL_256;
        const MODE   = MCRYPT_MODE_CBC;
        const KEY    = 'youdoncrazeunevaknow';
        private static $_key;

        public static function encrypt($plaintext)
        {
            $plaintext = strval($plaintext);
            if ($plaintext === '') return '';
            
            if (empty(self::$_key)) self::$_key = md5(self::KEY);
            
            $td = mcrypt_module_open(self::CYPHER, '', self::MODE, '');
            $iv = mcrypt_create_iv(mcrypt_enc_get_iv_size($td), MCRYPT_RAND);
            mcrypt_generic_init($td, self::$_key, $iv);
            $crypttext = mcrypt_generic($td, $plaintext);
            mcrypt_generic_deinit($td);
            return base64_encode($iv.$crypttext);
        }

        public static function decrypt($crypttext)
        {
            if ($crypttext === '') return '';
            
            if (empty(self::$_key)) self::$_key = md5(self::KEY);
            
            $crypttext = base64_decode($crypttext);
            $plaintext = '';
            $td        = mcrypt_module_open(self::CYPHER, '', self::MODE, '');
            $ivsize    = mcrypt_enc_get_iv_size($td);
            $iv        = substr($crypttext, 0, $ivsize);
            $crypttext = substr($crypttext, $ivsize);
            if ($iv)
            {
                mcrypt_generic_init($td, self::$_key, $iv);
                $plaintext = mdecrypt_generic($td, $crypttext);
            }
            return trim($plaintext);
        }

	
	
	
	
	
}




