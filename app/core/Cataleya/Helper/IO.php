<?php



namespace Cataleya\Helper;



/*
 *
 * 	@Package: Cataleya
 * 	@Class: Helper_IO
 *
 * 	(c) 2012 Fancy Paper Planes
 *
 *
 *
 */

class IO {
    
    
        
    const FIELD_NAME = 'name';
    const FIELD_EMAIL = 'email';
    const FIELD_PHONE = 'phone';
    const FIELD_DATE = 'date';
    const FIELD_PASSWORD = 'password';
    const FIELD_FLOAT = 'float';
    const FIELD_INT = 'int';

    




    /*
     * 
     * \Cataleya\Helper\IO::parseRequest()
     * 
     * @return: array
     * 
     */
    
    static public function parseRequest () 
    {
        switch ($_SERVER['REQUEST_METHOD']) {
            
            case 'GET':
                    
                break;
            
            case 'POST':
                
                break;
        }
    }
    
    
    
    
    
    /*
     * 
     * \Cataleya\Helper\IO::parseQueryString()
     * 
     * @return: array
     * 
     */
    
    static public function parseQueryString () 
    {
        $params = array();
        
        if (isset($_GET) && !empty($_GET)) {
           
            foreach ($_GET as $k => $v) {
                $params[$k] = $v;
            }
            
        }
        
        
        return $params;
    }


    
    
    
    
    
    
    /*
     * 
     * \Cataleya\Helper\IO::parsePostData()
     * 
     * @return: array
     * 
     */
    
    static public function parsePostData() 
    {
        $params = array();
        if (isset($_SESSION[SESSION_PREFIX.'REQUEST_TOKEN'])) define ('REQUEST_TOKEN', $_SESSION[SESSION_PREFIX.'REQUEST_TOKEN']);
        else return $params; // bad params
        
        if (isset($_POST) && !empty($_POST)) {
           
            foreach ($_POST as $k => $v) {
                list ($field, $type, $key) = explode('_', $k);
                if (!isset($field, $type, $key) || $key !== REQUEST_TOKEN) continue;
                
                switch ($type) {
                    
                    case self::FIELD_INT :
                        $v = \Cataleya\Helper\Validator::int($v);
                        break;
                    
                    case self::FIELD_FLOAT :
                        $v = \Cataleya\Helper\Validator::float($v);
                        break;
                    
                    case self::FIELD_NAME :
                        $v = \Cataleya\Helper\Validator::name($v, 1, 200);
                        break;
                    
                    case self::FIELD_PHONE :
                        $v = \Cataleya\Helper\Validator::phone($v, 1, 35);
                        break;
                    
                    case self::FIELD_DATE :
                        $v = \Cataleya\Helper\Validator::date($v);
                        break;
                    
                    case self::FIELD_PASSWORD :
                        $v = \Cataleya\Helper\Validator::password($v, 4, 50);
                        break;
                    
                    default :
                        $v = FALSE;
                        break;
                }
                
                
                if (!empty($v)) $params[$k] = $v;
            }
            
        }
        
        
        return $params;
    }












    /*
         * 
         * [ getFingerPrint  ]
         * 
         * 
         */
        
        static public function getFingerPrint () 
        {
            static $_fp = NULL;
            if (empty($_fp) && !is_string($_fp)) 
            {
                 $_suffix = defined('LOGIN_TOKEN') ? '-'.LOGIN_TOKEN : '';
                 $_fp = isset($_POST['fp'.$_suffix]) ? filter_var($_POST['fp'.$_suffix], FILTER_VALIDATE_REGEXP, array('options'=>array('regexp' => '/^[A-Za-z0-9]{1,100}$/'))) : '';
            }

            return $_fp;
        }
        
        
        
        
        
        /*
         * 
         * [ getRequestToken  ]
         * 
         * 
         */
        
        static public function getRequestToken () 
        {
            static $_t = NULL;
            if (empty($_t) && !is_string($_t)) 
            {
                 $_t = isset($_POST['rt']) ? filter_var($_POST['rt'], FILTER_VALIDATE_REGEXP, array('options'=>array('regexp' => '/^[A-Za-z0-9,\-]{20}$/'))) : '';
            }

            return $_t;
        }
        
        
        
        
        /*
         * 
         * [ getConfirmToken  ]
         * 
         * 
         */
        
        static public function getConfirmToken () 
        {
            static $_t = NULL;
            if (empty($_t) && !is_string($_t)) 
            {
                 $_t = isset($_POST['confirm_token']) ? filter_var($_POST['confirm_token'], FILTER_SANITIZE_STRIPPED) : '';
            }

            return $_t;
        }
        
      
    
        
        
       
        /*
         * 
         * [ getWhisper  ]
         * 
         * 
         */
        
        static public function getWhisper () 
        {
            static $_t = NULL;
            if (empty($_t) && !is_string($_t)) 
            {
                 $_t = isset($_POST['whisper']) ? filter_var($_POST['whisper'], FILTER_SANITIZE_STRIPPED) : '';
            }

            return $_t;
        }
        
        
        

        /*
         * 
         * [ generateToken ]
         * 
         * 
         */

        static public function generateToken ()
        {
            return (md5( uniqid(rand(), TRUE) ) . '-' . CURRENT_DT);
        }


      
    

        
        /*
         * [ digest ]
         * 
         */


        static public function digest ($salt = '', $timestamp = FALSE) 
        {
            if ($salt === '') 
            {
                $_keys = array ('hushpuppys', 'sagiterrian', 'olodo', 'agbaya', 'dondeymad', 'goshitinyourpants', 'megalomeniac', 'nacciruomoalata');        
                $salt = $_keys[rand(0, count($_keys)-1)];

            }
            return (sha1(strval(rand(0,microtime(true))) . $salt . strval(microtime(true)))) . (($timestamp) ? '-' . CURRENT_DT : '');
        }
        
        
        
        /*
         * [ uid ]
         * 
         */


        static public function uid () 
        {
            return sha1( uniqid(rand(), TRUE));
        }
        
        
        

 
}


