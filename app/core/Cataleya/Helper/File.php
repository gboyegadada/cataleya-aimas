<?php

namespace Cataleya\Helper;



/**
 * File
 *
 * @package Cataleya
 * @author Gboyega Dada <gboyega@fpplabs.com>
 * @version $Id$
 */
class File {


    /**
     * remove    Remove folder (or file) recursively.
     *
     * @param string $dir
     * @return void
     */
    public static function remove($dir) {

        $_path_info = pathinfo($dir);

        // 1. * : Remove folder content...
        if (preg_match('/\*/', $_path_info['basename']) > 0) {
            $p = '/^('.preg_replace('/([\.\-])/', "\\\\$1", $_path_info['basename']).')$/'; 
            $p = preg_replace('/(\*)/', ".\$1", $p); 
            $dir = preg_replace('~/[^/]$~', '', $dir); 
            $iterator = new \DirectoryIterator($dir);
            foreach ($iterator as $_f) {
                if ($_f->isDot() || preg_match($p, $_f->getFilename()) === 0) continue;
                self::remove($_f->getPathname());
            }
            return;
        }

        if (is_dir($dir)) {
            $files = scandir($dir);
            foreach ($files as $file) {
                if ($file != "." && $file != "..") self::remove("$dir/$file");
            }
            rmdir($dir);
        }
        else if (file_exists($dir)) unlink($dir);
    }





    /**
     * copy     Copy folder recursively
     *
     * @param string $src Source...
     * @param string $dst Destination folder...
     * @param bool $replace Replace if destination already exists ? true : false
     * @return void
     */
    public static function copy ($src, $dst, $replace=false) {

        if (!file_exists(preg_replace('~\*$~', "", $src))) throw new \Cataleya\Error('Source file/directory does not exist: '.$src);
        $_path_info = pathinfo($src);
    	$_dst_path_info = pathinfo($dst);

        // 1. * : Copy folder content...
        if (preg_match('/\*/', $_path_info['basename']) > 0 && !isset($_dst_path_info['extension'])) {
            $p = '/^('.preg_replace('/([\.\-])/', "\\\\$1", $_path_info['basename']).')$/'; 
            $p = preg_replace('/(\*)/', ".\$1", $p); 
            $dir = preg_replace('~/[^/]$~', '', $src); 
	        $iterator = new \DirectoryIterator($dir);
            foreach ($iterator as $_f) {
                if ($_f->isDot() || preg_match($p, $_f->getFilename()) === 0) continue; // 
                self::copy($_f->getPathname(), $dst);
            }
            return;
        }

        // 2. Regular Filename : Copy folder/file...
        $dst = (is_dir($dst)) ? $dst.DIRECTORY_SEPARATOR.$_path_info['basename'] : $dst; 

        if ( file_exists( $dst ) && $replace === true ) self::remove ( $dst );
        else if ( file_exists( $dst ) ) throw new \Cataleya\Error ('"'.$_path_info['basename'].'" could not be copied. It already exists in destination.');


        self::cp($src, $dst); 

    }



    /**
     * move Take $src and put in $dst if destination ($dst) is a folder.
     *      Otherwise replace $dst with $src and delete $src..
     *
     * @param mixed $src
     * @param mixed $dst
     * @param bool $replace
     * @return void
     */
    public static function move ($src, $dst, $replace=false) {

        self::copy($src, $dst, $replace);
        self::remove($src);
    }



    /**
     * cp       Copy folder recursively. I'm confining this to private use as I feel it 
     *          could easily lead to accidental replacement and a ton of swearing!
     *
     * @param string $src Source...
     * @param string $dst Destination folder...
     * @param bool $replace Replace if destination already exists ? true : false
     * @return void
     */
    private static function cp ($src, $dst, $replace=false) {


        if (is_dir ( $src )) {
            if (!mkdir ( $dst )) throw new \Cataleya\Error ('Unable to create folder. Please check path: '.$dst);
            $files = scandir ( $src );
            foreach ( $files as $file ) {
                if ($file != "." && $file != "..") {
                    self::cp ( "$src/$file", "$dst/$file" );
        		}
            }
    	} else if (file_exists ( $src )) copy ( $src, $dst ); 

    }




    /**
     * mv   Renaming basically. Works a little like unix "mv" command 
     *      but much more limited. Won't work with wild cards.
     *
     *      This can be used to replace one folder with another. But then 
     *      you may as well just use PHP rename().
     *
     * @param mixed $src
     * @param mixed $dst
     * @param bool $replace
     * @return void
     */
    public static function mv ($src, $dst, $replace=false) {

        self::cp($src, $dst, $replace);
        self::remove($src);
    }


}
