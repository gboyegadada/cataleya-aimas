<?php


namespace Cataleya\Helper;
use \Cataleya\Error;

/**
 * Model
 *
 * @author Gboyega Dada <gboyega@fpplabs.com>
 */
class Model {
    


    static $_registry = [
        'sys' => 'System::load', 
        'system' => 'System::load', 

        'catalog.product' => 'Catalog\Product', 
        'catalog.product.new' => 'Catalog\Product::create', 

        'catalog.category' => 'Catalog\Tag', 
        'catalog.category.new' => 'Catalog\Tag::create', 

        'customer' => 'Customer', 
        'customer.cart' => 'Customer\Cart', 

        'store' => 'Store', 
        'order' => 'Store\Order', 

        'currency' => 'Locale\Currency', 
        'currencies' => 'Locale\Currencies', 
        'language' => 'Locale\Language', 
        'languages' => 'Locale\Languages', 

        'country' => 'Geo\Country', 
        'countries' => 'Geo\Countries', 
        'province' => 'Geo\Province', 
        'provinces' => 'Geo\Provinces', 

        /* ------------ FRONT -------------------- */
        'form' => 'Front\Form', 
        'shop.*' => 'Helper\Model::_shop_front', 
        'dash.*' => 'Helper\Model::_dash', 
        
        
        /* ---------- PLUGINS ----------------------- */
        
        'this.*' => 'Helper\Model::_this', 

    ];

    /**
     * load       This is to help decouple plugin apps from the core.
     *
     *               DO NOT: \Cataleya\Catalog\Product::load($_id) 
     *               DO NOT: \Cataleya\Helper\Model::load('product', $_id) 
     *
     *               DO: __('product', $_id, $_arg2, $_arg3...) instead.
     *
     *
     * @param string $_model
     * @access protected
     * @return void
     */
    public static function load ($_model = '') 
    {


        $_route = null;
        foreach (self::$_registry as $_scope=>$_controller) 
        {

            if (self::inScope($_model, $_scope)) 
            {
                $_route = $_controller; break;
            }

        }

        if (empty($_route)) return null;

        preg_match ('/^(?<c>[_A-Za-z\\\]+)(\:\:(?<m>[_A-Za-z]+))?$/', $_route, $_);
        $c = '\Cataleya\\'.$_['c']; 
        if (!class_exists($c)) throw new Error ('Model class [' . $c . '] not found.');

        if (isset($_['m']) && method_exists($c, $_['m'])) $m = $_['m'];
        else if (method_exists($c, 'load')) $m = 'load';
        else if (method_exists($c, 'getInstance')) $m = 'getInstance';
        else throw new Error ('No "load" or "getInstance" method in model class [' . $c . '].');


        $_args = (substr($c,1) === __CLASS__) ? func_get_args() : array_slice(func_get_args(), 1);
        return forward_static_call_array([$c, $m], $_args); 


    }




    /**
     * inScope  Returns true if a model (ex: 'catalog.product') is part 
     *          of a scope (ex: 'catalog.*')
     *
     * @param string $_model
     * @param string $_scope
     * @return boolean
     */
    private static function inScope($_model, $_scope) 
    {

        if (!is_string($_model)) throw new Error ('Argument 1 must be a string.');
        
        if (
            !is_string($_scope) || 
            preg_match('!^(?<sc>([a-zA-Z\-_]+)(\.[a-zA-Z\-_]+){0,20})(\.\*)?$!', $_scope, $_) < 1
        ) throw new Error ('Invalid scope.');

        return (strpos($_model, $_['sc'].'.') === 0 || $_model === $_['sc']) ? true : false;


    }




    /**
     * _this
     *
     * @return mixed
     */
    protected static function _this () 
    {

        $_args = func_get_args();
        $_model = array_shift($_args);
        $_app_handle = \Cataleya\Plugins\Package::getCurrentAppHandle();

        if (empty($_app_handle)) throw new Error ('Unable to retrieve current App. You must call this method within your plugin code.');

        switch ($_model) 
        {

            case 'this': return \Cataleya\Plugins\Package::getCurrentApp();
            case 'this.skin': return \Cataleya\Plugins\Skin::load($_app_handle);
            case 'this.config': return \Cataleya\Plugins\Config::load($_app_handle);
            case 'this.config.init': return \Cataleya\Plugins\Config::init($_app_handle, $_args[0]);
            default: return null;

        }

    }
    
    
    
    
    /**
     * _shop_front
     *
     * @return mixed
     */
    protected static function _shop_front () 
    {

        $_args = func_get_args();
        $_model = array_shift($_args);

        switch ($_model) 
        {

            case 'shop': return \Cataleya\Front\Shop::getInstance();
            case 'shop.customer': return \Cataleya\Front\Shop\Customer::getInstance();
            case 'shop.cart': return \Cataleya\Front\Shop\MyCart::getInstance();
            case 'shop.skin': return \Cataleya\Front\Shop\Skin::load();
            default: return null;

        }

    }
    
    
    
    
    
    /**
     * _dash
     *
     * @return mixed
     */
    protected static function _dash () 
    {

        $_args = func_get_args();
        $_model = array_shift($_args);

        switch ($_model) 
        {

            case 'dash.user': return \Cataleya\Front\Dashboard\User::getInstance();
            case 'dash.skin': return \Cataleya\Front\Dashboard\Skin::load();
            default: return null;

        }

    }
    
    
    
}
