<?php


namespace Cataleya\Helper;



/*  

CLASS LOGGER 

*/




class Logger implements \Cataleya\System\Event\Listener 
{
	protected $_data = array();
	protected $dbh;
        protected $_save_to_db = FALSE;
        protected $_scope;



        private function __construct($_scope)
        {
            register_shutdown_function(array($this, '__destruct'));
            
            // check if we have our logger table setup
            $this->dbh = \Cataleya\Helper\DBH::getInstance();
            
            if ($this->dbh !== NULL) 
            {
                $_stmnt = $this->dbh->prepare('SELECT 1 FROM logger LIMIT 1');
                if ($_stmnt->execute()) $this->_save_to_db = TRUE;
            }
            
            unset($_stmnt);
            
            $this->_scope = $_scope;

            
                
        }
        
        
        public static function create($_scope) {
            
            if (\Cataleya\System\Event::exists($_scope)) {

                // Create instance...
                $_objct = __CLASS__;
                return new $_objct($_scope);
            
            } else {
                return NULL;
            }

                
        }

        public function __destruct()
        {
            $this->save();
            self::garbageCollect();
        }
	
	
	
	/*
	
		IMPORTANT!!
		________________________________________________________________
		
		in [ method: onChanged ], [ arg: $data ] must an array in the form (0=>'logger_id', 1=>'text_content')
		
		As you were!
	
	
	*/
	
	
	public function notify($_event, array $data = array ())
	{
        $_params = $data['params'];
		if ($this->_scope != $_event) return FALSE;
		
                $_save_now = (isset($_params['save_now']) && is_bool($_params['save_now'])) ? $_params['save_now'] : FALSE;
                
		$this->log($_params['logger_id'], $_params['blob'], $_save_now);
		return TRUE;
		
	}
        

        


        /**
         * 
         * @return string
         */
        public function getScope()
        {
                return $this->_scope;
        }
        
        
        

        
        

        public function log($logger_id, $data, $save_now = FALSE)
        {

                    if (!isset($this->_data[$logger_id])) {
                            $this->_data[$logger_id] = array($data);

                    } else {
                            $this->_data[$logger_id][] = $data;
                    } 

                    if ($save_now) $this->save ();
        }

        public function save()
        {
                    if (empty($this->_data) || !$this->_save_to_db) return TRUE;
            
                    // Get database handle...
                     $this->dbh = \Cataleya\Helper\DBH::getInstance();

                    ///////////// The insert new rows ////////////////
                    static  $logger_insert, 
                            $insert_param_logger_id, 
                            $insert_param_value;

                    // Do prepared statement for 'INSERT'...
                    if (empty($logger_insert)) {
                    $logger_insert = $this->dbh->prepare('
                                                            INSERT INTO logger (logger_id, value, logged_at)   
                                                            VALUES (:logger_id, :value, NOW())  
                                                            ');
                    $logger_insert->bindParam(':logger_id', $insert_param_logger_id, \PDO::PARAM_STR);
                    $logger_insert->bindParam(':value', $insert_param_value, \PDO::PARAM_STR);
                    }

                    foreach ($this->_data as $k=>$v) 
                    {	


                            foreach ($v as $v2) {
                                            $insert_param_logger_id = $k;
                                            $insert_param_value = $v2;

                                            // We can't call our ErrorHandler if not we might set off a loop...
                                            if (!$logger_insert->execute()) die ('error making log inserts');

                            }

                    }

                    // reset logger cache...
                    $this->_data = array();

        }
        
        
        
        
        

        
        
        // Garbage Collection
        public static function garbageCollect () 
        {
            

                // check if we have our logger table setup
                if (\Cataleya\Helper\DBH::getInstance() === NULL) return TRUE;

            
                static $delete_handle = NULL;
                
		// Do prepared statement for 'UPDATE'...
                
                if (empty($delete_handle)) 
                {
                    $delete_handle = \Cataleya\Helper\DBH::getInstance()->prepare('
                                                                        DELETE FROM logger        
                                                                        WHERE logged_at < DATE_SUB( NOW(), INTERVAL 3 month )
                                                                        ');
                }
                
                // if (!$delete_handle->execute()) \Cataleya\Helper\ErrorHandler::getInstance()->triggerException('Error in class (' . __CLASS__ . '): [ ' . implode(', ', $delete_handle->errorInfo()) . ' ] on line ' . __LINE__);
		
                
                return TRUE;
        }






    
    
    
}



