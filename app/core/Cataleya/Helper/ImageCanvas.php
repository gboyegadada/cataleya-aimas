<?php


namespace Cataleya\Helper;




/*
 *
 *	@Package: Cataleya
 *	@Class: \Cataleya\Helper\ImageCanvas
 *
 *	(c) 2012 Fancy Paper Planes
 *
 *
 *
 */





class ImageCanvas {
 
   private $_image;
   private $_image_type;






	/*
	 *
	 * [ __construct ]
	 * ________________________________________________________________
	 * 
	 * 
	 *
	 *
	 *
	 */
	 
	private function __construct () 
	{
		
		// Get error handler
		$this->e = \Cataleya\Helper\ErrorHandler::getInstance();
		
	}





	/*
	 *
	 * private: [ load ]
	 * ________________________________________________________________
	 * 
	 *
	 * ARGS: 
	 *			$filename: image path
	 *			
	 *
	 */
	 
 
   public static function load($filename) 
   {
	   
        $image_obj = new \Cataleya\Helper\ImageCanvas ();
 
 
      $image_info = getimagesize($filename);
      $image_obj->_image_type = $image_info[2];
	  
	  
		switch ($image_obj->_image_type) {
			case IMAGETYPE_JPEG:
			$image_obj->_image = imagecreatefromjpeg($filename);
			break;
			
			case IMAGETYPE_JPEG2000:
			$image_obj->_image = imagecreatefromjpeg($filename);
			break;
			
			case IMAGETYPE_PNG:
			$image_obj->_image = imagecreatefrompng($filename);
			break;
			
			case IMAGETYPE_GIF:
			$image_obj->_image = imagecreatefromgif($filename);
			break;
			
			default:
			$image_obj->_image = NULL;
			break;
		}
		
		if ($image_obj->_image === NULL) return NULL;
		
		
	return $image_obj;
	
		
   }
   
   
   
	/*
	 *
	 * private: [ save ]
	 * ________________________________________________________________
	 * 
	 *
	 * ARGS: 
	 *			$save_as: save to path (path + file name)
	 *			$compression: JPEG Compression if image is JPEG.
	 *			$permissions: file permissions
	 */
	 
   
   public function save ($save_as, $compression=80, $permissions=null) 
   {
 	  
	  
		switch ($this->_image_type) {
			case IMAGETYPE_JPEG:
			imagejpeg($this->_image, $save_as, $compression);
			break;
			
			case IMAGETYPE_JPEG2000:
			imagejpeg($this->_image, $save_as, $compression);
			break;
			
			case IMAGETYPE_PNG:
			imagepng($this->_image, $save_as);
			break;
			
			case IMAGETYPE_GIF:
			imagegif($this->_image, $save_as);
			break;
			
			default:
			$this->e->triggerException('Error in class (' . __CLASS__ . '): [ Image could not be saved to ' . $save_as . ' ] on line ' . __LINE__);
			break;
			
		}
	  
      if( $permissions != null) {
 
         chmod($save_as,$permissions);
      }
	  
	  return TRUE;
   }
   
   
   
   
/*
 *
 * private: [ flush ]
 * ________________________________________________________________
 * 
 * 
 *
 * ARGS: 
 *			none
 *			
 *			
 */


   
   public function flush() 
   {
 
       imagedestroy($this->_image);
       $this->_image_type = NULL;
 	  
	  
   }
   



   
   	/*
	 *
	 * [ rotate ]
	 * ________________________________________________________________
	 * 
	 * ARGS: 
	 *			:
	 *
	 *
	 */
	 
	public function rotate ($angle = 0) 
	{
		$angle = intval($angle);
		
		// Rotate ?
		if ($angle != 0 && $angle != 360) {
			
			$this->_image = imagerotate($this->_image, (0-$angle), 0);
					
		}
		
		
	}







	/*
	 *
	 * public: [ getWidth ]
	 * ________________________________________________________________
	 * 
	 *
	 * ARGS: 
	 *			none
	 *			
	 *			
	 */
	 
   
   
   public function getWidth() 
   {
 
      return imagesx($this->_image);
   }
   
   



	/*
	 *
	 * public: [ getHeight ]
	 * ________________________________________________________________
	 * 
	 *
	 * ARGS: 
	 *			none
	 *			
	 *			
	 */
	 
   
   
   public function getHeight() 
   {
 
      return imagesy($this->_image);
   }
   
   


	/*
	 *
	 * public: [ resizeToHeight ]
	 * ________________________________________________________________
	 * 
	 *
	 * ARGS: 
	 *			$height: target pixel height. will be used to define bounding box.
	 *			i.e. $height x $height
	 *			
	 */
	 
   
   
   public function resizeToHeight($height) 
   {
 
      $ratio = $height / $this->getHeight();
      $width = $this->getWidth() * $ratio;
      $this->resize($width,$height);
	  
   }
   
   




	/*
	 *
	 * public: [ resizeToWidth ]
	 * ________________________________________________________________
	 * 
	 *
	 * ARGS: 
	 *			$width: target pixel width. will be used to define bounding box.
	 *			i.e. $width x $width
	 *			
	 */
	 
   
 
   public function resizeToWidth($width) 
   {
	   
      $ratio = $width / $this->getWidth();
      $height = $this->getheight() * $ratio;
      $this->resize($width,$height);
	  
   }





	/*
	 *
	 * public: [ scale ]
	 * ________________________________________________________________
	 * 
	 *
	 * ARGS: 
	 *			$scale: Magnification i.e. [ 1.5 ] = [ x1.5 ] 
	 *			
	 *			
	 */
	 
 
   public function scale($scale) 
   {
	   
      $width = $this->getWidth() * $scale/100;
	  
      $height = $this->getheight() * $scale/100;
	  
      $this->resize($width,$height);
	  
   }

 
 
 
 
 
	/*
	 *
	 * public: [ resize ]
	 * ________________________________________________________________
	 * 
	 *
	 * ARGS: 
	 *			$width
	 *			$height
	 *			
	 */
	 
 
   public function resize($width,$height) {
	   
      $new_image = imagecreatetruecolor($width, $height);
	  
      imagecopyresampled($new_image, $this->_image, 0, 0, 0, 0, $width, $height, $this->getWidth(), $this->getHeight());
      $this->_image = $new_image;
	  
   }  
   



	/*
	 *
	 * public: [ smartResize ]
	 * ________________________________________________________________
	 * 
	 *
	 * ARGS: 
	 *			$width
	 *			$height
	 *			
	 */
	 
 
   public function smartResize($width,$height) {
	   
	  $ratio_orig = $this->getWidth() / $this->getHeight();
	  $ratio_targ = $width / $height;
	  
	  if ($ratio_orig > $ratio_targ) $this->resizeToWidth($width);
	  else $this->resizeToHeight($height);
	   
	  
   }  
   

  





	/*
	 *
	 * [ crop ]
	 * ________________________________________________________________
	 * 
	 * ARGS: 
	 *			:
	 *
	 *
	 */
	 
	public function crop ($w = 0, $h = 0, $x = 0, $y = 0) 
	{
		if ($w == 0 || $h == 0) return FALSE;
		
		
		$new_image = imagecreatetruecolor($w, $h);
		imagecopyresampled($new_image, $this->_image, 0, 0, $x, $y, $w, $h, $w, $h);
		
		
		$this->_image = $new_image;
		
				
	}
        
        



 
}




