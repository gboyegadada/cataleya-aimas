<?php



namespace Cataleya\Helper;


/**
 * 
 * This is used to import products from other 
 * eCommerce platforms (Shopify, BigCommerce...)
 *
 * @author Gboyega Dada
 */
class Importer {
    
    
    protected 
            
            $_data = array (), 
            $_Store, 
            $_Reader;
    




    public function __construct(\Cataleya\Store $_Store, $_FILE_TYPE = \Cataleya\Helper\Importer\Reader::FILE_TYPE_CATALEYA_CSV, $_FILE_PATH) {
        
        switch ($_FILE_TYPE) {
            
            case \Cataleya\Helper\Importer\Reader::FILE_TYPE_CATALEYA_CSV:
                $this->_Reader = new \Cataleya\Helper\Importer\Reader\CataleyaCSV($_FILE_PATH);
                break;
            
            case \Cataleya\Helper\Importer\Reader::FILE_TYPE_SHOPIFY_CSV:
                $this->_Reader = new \Cataleya\Helper\Importer\Reader\ShopifyCSV($_FILE_PATH);
                break;
            
            default:
                $this->_Reader = NULL;
                break;
        }
        
        if (empty($this->_Reader)) {
            \Cataleya\Helper\ErrorHandler::getInstance()->triggerException('Error in class (' . 
                    __CLASS__ . '): [ invalid argument for method: ' . 
                    __FUNCTION__ . ' ] on line ' . __LINE__
                    );
        }
        
        
        $this->_Store = $_Store;
        $this->_data = $this->_Reader->getJSON();
        
        
        
        /*
        $path =  ROOT_PATH . 'var/uploads/test.json';
        file_put_contents ($path, $this->_Reader->getJSON(true));
        
        
         */
        
        
    }
    



    public function doImport() {
        
        $_Categories = $_Attributes = $_AttributeTypes = $_ProductTypes = array ();
        
        $_CategoryList = $this->_Reader->getCategories();
        $_ProductTypeList = $this->_Reader->getProductTypes();
        $_AttributeTypeList = $this->_Reader->getAttributeTypes();
        $_AttributeValues = $this->_Reader->getAttributeValues();
        
        
        // Create Categories...
        foreach ($_CategoryList as $n) {
            $_Categories[$n] = \Cataleya\Catalog\Tag::create($this->_Store, $n, $n, 'EN');
        }
        
        //file_put_contents(ROOT_PATH.'var/logs/setup_debug.txt', __LINE__.': '.__CLASS__, FILE_APPEND);
        
        // Attribute Types (or Attribute Groups e.g. Color, Size, Blah)
        foreach ($_AttributeTypeList as $label) {
            $_AttributeTypes[$label] = \Cataleya\Catalog\Product\Attribute\Type::create(array ('name'=>$label, 'name_plural'=>$label, 'sort_order'=>0, 'active'=>TRUE));
            
            // Create Attributes..
            $_Attributes[$label] = array ();
            foreach ($_AttributeValues[$label] as $v) { 
                $_Attributes[$label][$v] = \Cataleya\Catalog\Product\Attribute::create($_AttributeTypes[$label], $v);
            }
            
        }
        
        // Product Types...
        foreach ($_ProductTypeList as $name=>$attributes) {
            $_ProductTypes[$name] = \Cataleya\Catalog\Product\Type::create(array('name'=>$name, 'name_plural'=>$name, 'icon'=>'tee.png', 'active'=>TRUE));
            
            // Add Attribute Types...
            foreach ($attributes as $label) {
                $_ProductTypes[$name]->addAttributeType($_AttributeTypes[$label]);
            }
            
        }
        
        $_Man = \Cataleya\Agent::load(1);
        $_Supp = \Cataleya\Agent::load(2);
        
        
        // Create products...
        foreach ($this->_data as $c=>$products) {
            
            foreach ($products as $p) {
                
                // Product Info...
                $_Product = \Cataleya\Catalog\Product::create($_ProductTypes[$p['product_type']], $_Man, $_Supp, $p['title'], $p['description'], 'EN');
                $_Product->setHandle($p['handle']);
                $_Product->setMetaTitle($p['meta_title'],'EN');
                $_Product->setMetaDescription($p['meta_description'],'EN');
                $_Product->setMetaKeywords($p['tags'], 'EN');
                
                (!isset($p['visibility']) || $p['visibility'] === true) ? $_Product->show() : $_Product->hide();
                
                // $_Product->setSupplier($p['supplier']);
                // $_Product->setGiftCard($p['gif_card']);
                
                // Product Image...
                $_Image = \Cataleya\Asset\Image::create(\Cataleya\Asset\Image::SRC_URL, $p['image']['src']);
                if ($_Image !== NULL) { $_Image->bake(array(), true); $_Product->setPrimaryImage ($_Image); }
                
                
                $this->_buildProductOptions($_Product, $p['options'], $_Attributes);
                
                $_Categories[$c]->addProduct($_Product);
                
            }
            
        }
        
        //file_put_contents(ROOT_PATH.'var/logs/setup_debug.txt', __LINE__.': '.__CLASS__, FILE_APPEND);
        
    }
    
    
    
    protected function _buildProductOptions (\Cataleya\Catalog\Product $_Product, array $options, array $_Attributes) {

        
                // Product Options..
                foreach ($options as $o) {
                    
                    // $option['attributes'] = [Color=>Red, Size=>L]
                    
                    $_attrObjects = array ();
                    foreach ($o['attributes'] as $n=>$v) { $_attrObjects[] = $_Attributes[$n][$v]; }
                    
                    $_Option = \Cataleya\Catalog\Product\Option::create ($_Product, $_attrObjects);
                    
                    
                    if (!empty($o['SKU'])) { $_Option->setSKU($o['SKU']); }
                    if (!empty($o['UPC'])) { $_Option->setUPC($o['UPC']); }
                    $_Option->getStock()->setValue($this->_Store, floatval($o['stock']));
                    $_Option->getPrice()->setValue($this->_Store, floatval($o['price']));
                    
                    ($o['taxable']) ? $_Option->makeTaxable() : $_Option->makeNonTaxable();
                    
                    $_Option->setWeightUnit(empty($o['weight_unit']) ? 'kg' : $o['weight_unit']);
                    $_Option->setWeight(!is_numeric($o['weight']) ? 0 : floatval($o['weight']));
                    

                    // Product Image...
                    $_Image = \Cataleya\Asset\Image::create(\Cataleya\Asset\Image::SRC_URL, $o['image']['src']);
                    if ($_Image !== NULL) { $_Image->bake(array(), true); $_Option->setPreviewImage ($_Image); }

                    
                }
                
    }
    
    
    
    
    
    
}

