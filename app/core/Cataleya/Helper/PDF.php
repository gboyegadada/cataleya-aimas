<?php

namespace Cataleya\Helper;



/**
 * File
 *
 * @package Cataleya
 * @author Gboyega Dada <gboyega@fpplabs.com>
 * @version $Id$
 */
class PDF extends \Cataleya\Helper\FPDF {

    
    
    
    /** 
    * MultiCell with alignment as in Cell. 
    * @param float $w 
    * @param float $h 
    * @param string $text 
    * @param mixed $border 
    * @param int $ln 
    * @param string $align 
    * @param boolean $fill 
    */ 
    public function MultiAlignCell($w,$h,$text,$border=0,$ln=0,$align='L',$fill=false) 
    { 
    // Store reset values for (x,y) positions 
    $x = $this->GetX() + $w; $y = $this->GetY(); 


    // Make a call to FPDF's MultiCell 
    $this->MultiCell($w,$h,$text,$border,$align,$fill); 

    // Reset the line position to the right, like in Cell 
    if( $ln==0 ) { $this->SetXY($x,$y); } 
    
    return $this;

    }
    
    /**
     * Footer
     *
     * @param string $dir
     * @return void
     */
    function Header () {

        
    }

    
    /**
     * Footer
     *
     * @param string $dir
     * @return void
     */
    function Footer () {

        // Go to 1.5 cm from bottom
        $this->SetY(-15);
        
        // Select Arial italic 8
        $this->SetFont('Arial', 'I', 8);
        $this->SetTextColor(100, 100, 100);
        
        // Page number
        $this->Cell(0,10,'Page '.$this->PageNo().'/{nb}',0,0,'C');
 
        
        
    }




}
