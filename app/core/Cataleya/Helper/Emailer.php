<?php


namespace Cataleya\Helper;




/*
 * CLASS EMAILER
 *
 * Usage
 * _____________________________________
 *
 * $emailer = \Cataleya\Helper\Emailer::getInstance($template_path);
 * 
 * $emailer->To = array('user@email.com', 'user2@email.com'=>'Bola Kalejaye');
 * $emailer->From = array('user2@email.com'=>'Bola Kalejaye');
 * $emailer->Sender = array('user2@email.com'=>'Bola Kalejaye');
 * $emailer->ReplyTo = array('user2@email.com'=>'Bola Kalejaye');
 *
 * $emailer->Subject = 'My Super Subject';
 * $emailer->TextBody = 'Plain text body';
 *
 * $emailer->newParam ('Firstname', 'Bisi'); // For use in template
 * $emailer->newParam ('Lastname', 'Olateyinwa'); // For use in template
 *
 * $emailer->Priority = 1: Highest Priority, 2: High Priority, 3: Normal, 4: Low, 5: Lowest
 *
 * $emailer->send();
 * 
 * 
 */


class Emailer 
implements \Cataleya\System\Event\Observable {

	private $e, $_config, $dbh;
	private $_num_sent = array();
    private $_attachments = array ();
	static $_instance;
	

	
    protected static $_events = [
            'mail/error'
        ];

	protected $_data = array (
										'To'	=>	'', 
										'From'	=>	'', 
										'Sender'	=>	'', 
										'Firstname'	=>	'', 
										'Lastname'	=>	'', 
										'ReplyTo'	=>	'', 
										'Bcc'	=>	'', 
										'Subject'	=>	'', 
										'TextBody'	=>	'', 
										'HTMLBody'	=>	'', 
										'Priority'	=>	3, 
										'Module'	=>	'unknown'
									);

	private function __construct()
	{
		
		// Get error handler
		$this->e = \Cataleya\Helper\ErrorHandler::getInstance();
		
		
		// Mailer Config
		$this->_config = \Cataleya\System\Config::load('mailer.config');
		
		// Get database handle...
                $this->dbh = \Cataleya\Helper\DBH::getInstance();
		
		
		
		
	}
	
	
	public static function getInstance($_module = 'unknown')
	{
		
		if (!isset(self::$_instance))
		{
			
			// Create instance...
			$object =  __CLASS__;
			self::$_instance = new $object ();
		
		}
		
		// Reset params...
                self::$_instance->resetParams();
		
		// Set module name
		self::$_instance->_data['Module'] = $_module;
		
		
		return self::$_instance;
		
	}
    
    
            
    
    /**
     * getEvents
     * 
     * @return array
     */
    public static function getEvents() {

        return self::$_events;
    }
        
	


	// GETTER ////////////
	
	public function __get ($name)
	{
		if (array_key_exists($name, $this->_data)) return $this->_data[$name];
		else return NULL;
		
	}
	
	
	
	
	// SETTER ////////////
	
	public function __set ($name, $value)
	{
		if (array_key_exists($name, $this->_data)) 
		{
			$this->_data[$name] = $value;
			return TRUE;
		}
		
	}
	


	// NEW PARAM ////////////
	
	public function newParam ($name, $default_value='')
	{
		if (!array_key_exists($name, $this->_data)) {
			$this->_data[$name] = $default_value;
			
			return TRUE;
		} else {
			
			if (defined('DEBUG_MODE') && DEBUG_MODE === TRUE)  $this->e->triggerError('Error in class (' . __CLASS__ . '): [ Entry already exists ] on line ' . __LINE__);
			return FALSE;
		}
		
	}
	
	
	public function resetParams ()
	{
		
		$this->_num_sent = array();
                $this->_attachments = array ();
		$this->_data = array (
										'To'	=>	'', 
										'From'	=>	'', 
										'Sender'	=>	'no-reply@'.HOST, 
										'Firstname'	=>	'', 
										'Lastname'	=>	'', 
										'ReplyTo'	=>	'no-reply@'.HOST, 
										'Bcc'	=>	array (), 
										'Subject'	=>	'', 
										'TextBody'	=>	'', 
										'HTMLBody'	=>	'', 
										'Priority'	=>	3, 
										'Module'	=>	'unknown'
									);
		
		
		
	}






/*
 *
 *  [ getFailures ] Returns failed deliveries
 *
 */	
	
	
	public function getFailures () 
	{
		return $this->_num_sent;
		
	}
	
	
	
	
	
	
	public function send ()
	{
		
		
		
		
		// Validate 'To' field(s)...

		// if there are multiple address entries...
		if (is_array($this->_data['To'])) 
		{
			foreach ($this->_data['To'] as $key=>$val) {
				
				// if address entry is like this: [ 0=>email_address ]
				if ( is_numeric($key) ) 
				{
						if (filter_var($val, FILTER_VALIDATE_EMAIL) === FALSE) $this->e->triggerError(
																		'Error in class (' . __CLASS__ . 
																		'): [ One or more invalid email addresses in \'To\' field. ] 
																		on line ' . __LINE__
																		);
					
				} 
				
				// if address entry is like this: [ email=>full_name ] 
				else if (filter_var($key, FILTER_VALIDATE_EMAIL) === FALSE) 
				{
					$this->e->triggerError(
											'Error in class (' . __CLASS__ . '): [ One or more invalid email addresses 
											in \'To\' field. ] on line ' . __LINE__
											);
				}
				
			}
			
		// if address entry is just one as a [ string ]
		} else if (filter_var($this->_data['To'], FILTER_VALIDATE_EMAIL) === FALSE) 
				{
					$this->e->triggerError(
											'Error in class (' . __CLASS__ . '): [ One or more invalid email addresses 
											in \'To\' field. ] on line ' . __LINE__
											);
				}
			
		

		

		// Check email subject...
		if ($this->_data['Subject'] == '') $this->_data['Subject'] = '[ No Subject ]';

                
                
                
                //// USE MAILER /////
                if (empty($this->_config) || empty($this->_config->smtpHost)):
                        // Setup transport
                        $transport = \Swift_MailTransport::newInstance();
                        
                
                //// OR SMTP /////
                else:
                    
                        // Setup transport
                        $transport = \Swift_SmtpTransport::newInstance()
                                                                        ->setHost($this->_config->smtpHost)
                                                                        ->setUsername($this->_config->user)
                                                                        ->setPassword($this->_config->pass);



                        // available transports
                        $_transports = stream_get_transports();

                        if (!empty($this->_config->encryption) && in_array($this->_config->encryption, $_transports)) 
                        {
                            $transport->setPort($this->_config->port);
                            $transport->setEncryption($this->_config->encryption);
                        }

                        else 
                        {
                            $transport->setPort(25);
                        }
                
                endif;

                

                
                
		
		// Get mailer													
		$mailer = \Swift_Mailer::newInstance($transport);
		
		
		
		
		
		$message = \Swift_Message::newInstance();
		
		$message->setSubject($this->_data['Subject']);
		$message->setFrom($this->_data['From']);
		if ($this->_data['Sender'] != '') $message->setSender($this->_data['Sender']);
		$message->setTo($this->_data['To']);
		if ($this->_data['ReplyTo'] != '') $message->setReplyTo($this->_data['ReplyTo']);
		if (!empty($this->_data['Bcc'])) $message->setBcc($this->_data['Bcc']);
		$message->setBody($this->_data['TextBody']);
		$message->addPart($this->_data['HTMLBody'], 'text/html');
		$message->setPriority($this->_data['Priority']);
		// $message->setReturnPath('failed.deliveries@aimas.com.ng');
                
                
                // Attachments
                if (!empty($this->_attachments)) foreach ($this->_attachments as $_path) $message->attach(\Swift_Attachment::fromPath ($_path));
		
		
		
                
                
		try {
                    $result = $mailer->send($message);
                    if ($result === 0) { 
                        
                        \Cataleya\System\Event::notify('mail/error', $this, array (
                            'logger_id'=>'emailer.log', 
                            'blob'=>'One or more emails could not be sent in class: ' . __CLASS__ . ', on line ' . __LINE__
                                ));
                    }
                
                } catch (\Swift_SwiftException $e) {
                    $result = 0;
                    \Cataleya\System\Event::notify('mail/error', $this, array (
                        'logger_id'=>'emailer.log', 
                        'blob'=>'One or more emails could not be sent because: [ ' . $e->getMessage() . ' ] in class: ' . __CLASS__ . ', on line ' . __LINE__
                            ));
                }

		
		$this->_num_sent =  $result; // integer representing number of successful sends
		


		
	/*
	 *	[ ARCHIVE ]
	 *
	 *
	 *
	 */
		
		
		static 
				$archive_insert, 
				$insert_param_message_id, 
				$insert_param_to_name, 
				$insert_param_to_address, 
				$insert_param_from_name, 
                                $insert_param_sender, 
				$insert_param_from_address, 
				$insert_param_subject, 
				$insert_param_html, 
				$insert_param_text, 
				$insert_param_module;
		
		// if (isset ($messages_saved) && $messages_saved === TRUE) return;
		
		// Do prepared statement for 'INSERT'...
		if (empty($archive_insert)) {
		$archive_insert = $this->dbh->prepare('
		
		INSERT INTO email_archive (message_id, sender, to_name, to_address, from_name, from_address, subject, html, text, module, date_sent)   
		VALUES (:message_id, :sender, :to_name, :to_address, :from_name, :from_address, :subject, :html, :text, :module, NOW())  
		
														');
														
		$archive_insert->bindParam(':message_id', $insert_param_message_id, \PDO::PARAM_INT);
		$archive_insert->bindParam(':to_name', $insert_param_to_name, \PDO::PARAM_STR);
		$archive_insert->bindParam(':to_address', $insert_param_to_address, \PDO::PARAM_STR);
		$archive_insert->bindParam(':from_name', $insert_param_from_name, \PDO::PARAM_STR);
		$archive_insert->bindParam(':from_address', $insert_param_from_address, \PDO::PARAM_STR);
		$archive_insert->bindParam(':sender', $insert_param_sender, \PDO::PARAM_STR);
		$archive_insert->bindParam(':subject', $insert_param_subject, \PDO::PARAM_STR);
		$archive_insert->bindParam(':html', $insert_param_html, \PDO::PARAM_STR);
		$archive_insert->bindParam(':text', $insert_param_text, \PDO::PARAM_STR);
		$archive_insert->bindParam(':module', $insert_param_module, \PDO::PARAM_STR);
		//$archive_insert->bindParam(':attachment', $insert_param_attachment, \PDO::PARAM_STR);
                
                
		}
		
		
		
		$insert_param_message_id = $message->getID(); 
		
		foreach ($this->_data['To'] as $key=>$val) 
		{
			
			// if address entry is like this: [ 0=>email_address ]
			$insert_param_to_name = ( is_numeric($key) ) ? '' : $val; 
			$insert_param_to_address = ( is_numeric($key) ) ? $val : $val . '<' . $key . '>';
			
		}
		
		
		foreach ($this->_data['From'] as $key=>$val) 
		{
			
			// if address entry is like this: [ 0=>email_address ]
			$insert_param_from_name = ( is_numeric($key) ) ? '' : $val; 
			$insert_param_from_address = ( is_numeric($key) ) ? $val : $val . '<' . $key . '>';
			
		}
		
		$insert_param_sender = $this->_data['Sender']; 
		$insert_param_subject = $this->_data['Subject']; 
		$insert_param_html = $this->_data['HTMLBody']; 
		$insert_param_text = $this->_data['TextBody']; 
		$insert_param_module = $this->_data['Module'];
		
		
		
		if (!$archive_insert->execute()) return $this->e->triggerException('Error in class (' . __CLASS__ . '): [ ' . implode(', ', $archive_insert->errorInfo()) . ' ] on line ' . __LINE__);
		
		return $result;
		
		
	}
        
        
        
        
        
        
        
        
        

        /*
         *
         * [ getTo ] 
         *_____________________________________________________
         *
         *
         */

        public function getTo()
        {
                return $this->_data['To'];
        }



        /*
         *
         * [ setTo ] 
         *_____________________________________________________
         *
         *
         */

        public function setTo(array $value)
        {
                $this->_data['To'] = $value;

                return $this;
        }

   
        
        
        

        /*
         *
         * [ getTo ] 
         *_____________________________________________________
         *
         *
         */

        public function getFrom()
        {
                return $this->_data['From'];
        }



        /*
         *
         * [ setTo ] 
         *_____________________________________________________
         *
         *
         */

        public function setFrom(array $value)
        {
                $this->_data['From'] = $value;

                return $this;
        }

   
        
        
 
        /*
         *
         * [ getTo ] 
         *_____________________________________________________
         *
         *
         */

        public function getSender()
        {
                return $this->_data['Sender'];
        }



        /*
         *
         * [ setTo ] 
         *_____________________________________________________
         *
         *
         */

        public function setSender($value = NULL)
        {
                $value = \Cataleya\Helper\Validator::email($value);
                if ($value === FALSE) \Cataleya\Helper\ErrorHandler::getInstance()->triggerException('Error in class (' . __CLASS__ . '): [ invalid argument (valid email address required) for method: ' . __FUNCTION__ . ' ] on line ' . __LINE__);
            
                $this->_data['Sender'] = $value;

                return $this;
        }
        
        
        


        /*
         *
         * [ getFirstname ] 
         *_____________________________________________________
         *
         *
         */

        public function getFirstname()
        {
                return $this->_data['Firstname'];
        }



        /*
         *
         * [ setFirstname ] 
         *_____________________________________________________
         *
         *
         */

        public function setFirstname($value = NULL)
        {
                $value = \Cataleya\Helper\Validator::name($value, 0, 200);
                if ($value === FALSE) \Cataleya\Helper\ErrorHandler::getInstance()->triggerException('Error in class (' . __CLASS__ . '): [ invalid argument (no special chracters allowed in names) for method: ' . __FUNCTION__ . ' ] on line ' . __LINE__);
            
                $this->_data['Firstname'] = $value;

                return $this;
        }

   
        
        
        
        

        /*
         *
         * [ getLastname ] 
         *_____________________________________________________
         *
         *
         */

        public function getLastname()
        {
                return $this->_data['Lastname'];
        }



        /*
         *
         * [ setLastname ] 
         *_____________________________________________________
         *
         *
         */

        public function setLastname($value = NULL)
        {
                $value = \Cataleya\Helper\Validator::name($value, 0, 200);
                if ($value === FALSE) \Cataleya\Helper\ErrorHandler::getInstance()->triggerException('Error in class (' . __CLASS__ . '): [ invalid argument (no special chracters allowed in names) for method: ' . __FUNCTION__ . ' ] on line ' . __LINE__);
            
                $this->_data['Lastname'] = $value;

                return $this;
        }

        
        
   
        
        
        
        
        /*
         *
         * [ getReplyTo ] 
         *_____________________________________________________
         *
         *
         */

        public function getReplyTo()
        {
                return $this->_data['ReplyTo'];
        }



        /*
         *
         * [ setReplyTo ] 
         *_____________________________________________________
         *
         *
         */

        public function setReplyTo($value = NULL)
        {
                $value = \Cataleya\Helper\Validator::email($value);
                if ($value === FALSE) \Cataleya\Helper\ErrorHandler::getInstance()->triggerException('Error in class (' . __CLASS__ . '): [ invalid argument (valid email address required) for method: ' . __FUNCTION__ . ' ] on line ' . __LINE__);
            
                $this->_data['ReplyTo'] = $value;

                return $this;
        }

        
        
        
        /*
         *
         * [ getBcc ] 
         *_____________________________________________________
         *
         *
         */

        public function getBcc()
        {
                return $this->_data['Bcc'];
        }



        /*
         *
         * [ setBcc ] 
         *_____________________________________________________
         *
         *
         */

        public function setBcc(array $value)
        {
 
                $this->_data['Bcc'] = $value;

                return $this;
        }

        
        
        
        /*
         *
         * [ addBcc ] 
         *_____________________________________________________
         *
         *
         */

        public function addBcc($email = NULL, $name = NULL)
        {
                $email = \Cataleya\Helper\Validator::email($email);
                if ($email === FALSE) \Cataleya\Helper\ErrorHandler::getInstance()->triggerException('Error in class (' . __CLASS__ . '): [ invalid argument (valid email required) for method: ' . __FUNCTION__ . ' ] on line ' . __LINE__);
                
                $name = \Cataleya\Helper\Validator::name($name, 1, 300);
                if ($name === FALSE) $this->_data['Bcc'][$email] = $name;
                else $this->_data['Bcc'][] = $email;

                return $this;
        }
        
        
        
        
        
        
        /*
         *
         * [ getReplyTo ] 
         *_____________________________________________________
         *
         *
         */

        public function getSubject()
        {
                return $this->_data['Subject'];
        }



        /*
         *
         * [ setSubject ] 
         *_____________________________________________________
         *
         *
         */

        public function setSubject($value = NULL)
        {
                $value = \Cataleya\Helper\Validator::text($value, 0, 400);
                if ($value === FALSE) \Cataleya\Helper\ErrorHandler::getInstance()->triggerException('Error in class (' . __CLASS__ . '): [ invalid argument (keep length of subject under 400 chars) for method: ' . __FUNCTION__ . ' ] on line ' . __LINE__);
            
                $this->_data['Subject'] = $value;

                return $this;
        }

   
        
        /*
         * 
         * [ attach ]
         * _______________________________________________
         * 
         * 
         */
        
        
        public function attach ($_path = NULL) 
        {
            if (!is_string ($_path) || !file_exists($_path)) \Cataleya\Helper\ErrorHandler::getInstance()->triggerException('Error in class (' . __CLASS__ . '): [ invalid argument (valid file path required) for method: ' . __FUNCTION__ . ' ] on line ' . __LINE__);
            
            $_search = array_search($_path, $this->_attachments);
            if ($_search === FALSE) 
            {
                $this->_attachments[] = $_path;
                $_search = count($this->_attachments)-1;
            }
            
            
            return $_search;
        }
        
        
        
        
        /*
         * 
         * [ detach ]
         * _______________________________________________
         * 
         * 
         */
        
        
        public function detach ($_path_or_identifier = NULL) 
        {
            if (is_numeric($_path_or_identifier)) $_path_or_identifier = (int)$_path_or_identifier;
            
            foreach ($this->_attachments as $k=>$v) 
            {
                if ($k === $_path_or_identifier || $v === $_path_or_identifier) 
                {
                    unset($this->_attachments[$k]);
                    break;
                }
            }
            
            return $this;
        }












        /*
         *
         * [ getTextBody ] 
         *_____________________________________________________
         *
         *
         */

        public function getTextBody()
        {
                return $this->_data['TextBody'];
        }



        /*
         *
         * [ setTextBody ] 
         *_____________________________________________________
         *
         *
         */

        public function setTextBody($value = NULL)
        {
                $value = \Cataleya\Helper\Validator::text($value, 0, 5000);
                if ($value === FALSE) \Cataleya\Helper\ErrorHandler::getInstance()->triggerException('Error in class (' . __CLASS__ . '): [ invalid argument (keep length of TEXT body under 5000 chars) for method: ' . __FUNCTION__ . ' ] on line ' . __LINE__);
            
                $this->_data['TextBody'] = $value;

                return $this;
        }
        
        
        
        
        
        /*
         *
         * [ getReplyTo ] 
         *_____________________________________________________
         *
         *
         */

        public function getHTMLBody()
        {
                return $this->_data['HTMLBody'];
        }



        /*
         *
         * [ setHTMLBody ] 
         *_____________________________________________________
         *
         *
         */

        public function setHTMLBody($value = NULL)
        {
                $value = \Cataleya\Helper\Validator::html($value, 0, 90000);
                if ($value === FALSE) \Cataleya\Helper\ErrorHandler::getInstance()->triggerException('Error in class (' . __CLASS__ . '): [ invalid argument (keep length of HTML body under 5000 chars) for method: ' . __FUNCTION__ . ' ] on line ' . __LINE__);
            
                $this->_data['HTMLBody'] = $value;

                return $this;
        }

   

   
        
        /*
         *
         * [ getPriority ] 
         *_____________________________________________________
         *
         *
         */

        public function getPriority()
        {
                return $this->_data['Priority'];
        }



        /*
         *
         * [ setPriority ] 
         *_____________________________________________________
         *
         *
         */

        public function setPriority($value = NULL)
        {
                $value = \Cataleya\Helper\Validator::int($value, 1, 5);
                if ($value === FALSE) \Cataleya\Helper\ErrorHandler::getInstance()->triggerException('Error in class (' . __CLASS__ . '): [ invalid argument (must be integer between 1 and 5) for method: ' . __FUNCTION__ . ' ] on line ' . __LINE__);
            
                $this->_data['Priority'] = $value;

                return $this;
        }
        
        
        
        
        
       /*
         *
         * [ getModules ] 
         *_____________________________________________________
         *
         *
         */

        public function getModule()
        {
                return $this->_data['Module'];
        }



        /*
         *
         * [ setModule ] 
         *_____________________________________________________
         *
         *
         */

        public function setModule($value = NULL)
        {
                if (!is_string($value)) return FALSE;

                $this->_data['Module'] = $value;

                return $this;
        }
        
        
        
       /*
         *
         * [ getNumSent ] 
         *_____________________________________________________
         *
         *
         */

        public function getNumSent()
        {
                return $this->_num_sent;
        }
        
        
        


        
   
	
	
}