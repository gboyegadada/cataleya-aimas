<?php


namespace Cataleya\Helper;



/*

CLASS DBH 
 ____________________________________________________
 
 Note: first we extend PDO for some extra features...

*/



class DBH extends \PDO 
{
	
	private static $_dbh_instance, $_config;
        protected $_has_active_transaction = FALSE;
        protected static $_clock = 0;



        /**
         * 
         * @return \Cataleya\Helper\DBH
         * 
         */
        public static function getInstance () 
	{
		
		if (!isset(self::$_dbh_instance))
		{
                    
                        // LOAD CONFIG
                        self::$_config = \Cataleya\Helper::loadConfig('rdc');
                        if (self::$_config === NULL) return NULL;
                        

			$_dsn = 'mysql:host=' . self::$_config->host .
				   ';dbname='    . self::$_config->dbname .
				   ';connect_timeout=15';
				   
			$_options = array(
				// self::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8',
			); 
                        
                        $_objct = __CLASS__;
                        
                        
                        
                        // Prevent spilt guts on the pavement...
                        try {
                            
                            self::$_dbh_instance = new $_objct($_dsn, self::$_config->user, self::$_config->pass, $_options);
                            self::$_dbh_instance->exec("SET time_zone ='" . date('P') ."'");
                        
                        } catch (\PDOException $e) {

                            return null; // die('db failure');

                        }
		}
		
		return self::$_dbh_instance;
		
	}
        
        
        
        

        public function beginTransaction () 
        {
           if ( $this->_has_active_transaction ) 
           {
              return FALSE;
           } else {
              $this->_has_active_transaction = parent::beginTransaction ();
              return $this->_has_active_transaction;
           }
        }
        
        
        public function hasTransaction () 
        {
           return $this->_has_active_transaction;
        }
        
        

        public function commit () 
        {
            
           if ( $this->_has_active_transaction === FALSE ) return FALSE;
           
           parent::commit ();
           $this->_has_active_transaction = FALSE;
        }
        
        

        public function rollback () 
        {
            
           if ( $this->_has_active_transaction === FALSE ) return FALSE;
           
           parent::rollback ();
           $this->_has_active_transaction = FALSE;
        }
        
        
        public static function getTypeConst ($_var) 
        {
            if (is_int($_var)) return \PDO::PARAM_INT;
            if (is_bool($_var)) return \PDO::PARAM_BOOL;
            if (is_null($_var)) return \PDO::PARAM_NULL;
            
            
            // default
            return \PDO::PARAM_STR;
        }
        
        
        
        /**
         * 
         * @param \PDOStatement $_PDOStatement
         * @param array $_params
         * 
         */
        public static function bindParamsArray (\PDOStatement &$_PDOStatement, array &$_params) 
        {
            foreach ($_params as $key=>$val) {

                    $_PDOStatement->bindParam(
                            ':'.$key, 
                            $_params[$key], 
                            self::getTypeConst($val)
                            );
            }
        }
        
        
        
        
        /**
         * 
         * @param array $_expected_keys
         * @param array $_config
         * 
         */
        public static function buildParamsArray (array $_expected_keys, array $_config) 
        {

            $_params = array ();
            
            foreach ($_expected_keys as $k=>$v) {
                
                if (isset($_config[$k]) && (is_scalar($_config[$k] && gettype($_config[$k]) === $v) || (is_object($_config[$k]) && get_class($_config[$k]) === $v))) {
                    $_params[$k] = (
                                is_object($_config[$k]) && get_class($_config[$k]) === $v
                                ) 
                                ? $_config[$k]->getID() 
                                : $_config[$k];
                }
            }


            $_cols_str = $_vals_str = [];

            foreach ($_params as $key=>$val) {

                 If (!is_numeric($val) && !is_string($val) && !is_null($val) && !is_bool($val)) { $_params[$key] = $val= ''; }

                 $_cols_str[] = $key;
                 $_vals_str[$key] = ':'.$key;
            }
            
            return array (
                'columns' => $_cols_str, 
                'placeholders' => $_vals_str, 
                'params' => $_params
            );
            

        }






        /*
         * 
         * [ runScript ]
         * _________________________________________________________
         * 
         * Executes SQL Script file.
         * 
         */
        
        
        public static function runScript_old ($_filename) 
        {
            // Validate file path
            if (file_exists(SHARED_INC_PATH.'data/'.$_filename)) $_path = SHARED_INC_PATH.'data/'.$_filename;
            else if (file_exists(INC_PATH.'data/'.$_filename)) $_path = INC_PATH.'data/'.$_filename;
            else if (file_exists(ROOT_PATH.'setup/sql/'.$_filename)) $_path = ROOT_PATH.'setup/sql/'.$_filename;
            else \Cataleya\Helper\ErrorHandler::getInstance()->triggerException('Error in class (' . __CLASS__ . '): [ File does not exist. ] on line ' . __LINE__);
            
            $_dbh = self::getInstance();
            $_dbh->beginTransaction();
            
            
            
            //load file
            $commands = file_get_contents($_path);
            
            // deal with inline/multiline comments first...
            // $commands = preg_replace('!/\*.*?\*/!s', "", $commands);
            // $commands = preg_replace('!/\n\s*\n/!', "\n", $commands);

            //delete other comments # and --
            $lines = explode("\n",$commands);
            $commands = '';
            foreach($lines as $line){
                $line = trim($line);
                if( $line && preg_match('/^\s*(--|#).*/', $line) === 0 ){
                    $commands .= $line . "\n";
                }
            }
            
            

            //convert to array
            $commands = explode(";", $commands);

            //run commands
            $total = $success = 0;
            foreach($commands as $command){
                if(trim($command) === '') continue;
                
                if (!$_dbh->query($command)) \Cataleya\Helper\ErrorHandler::getInstance ()->triggerException('
                                                                                                            Error in class (' . __CLASS__ . '): [ ' . 
                                                                                                            implode(', ', $_dbh->errorInfo()) . 
                                                                                                            ' ] on line ' . __LINE__);
                $success += 1;
                $total += 1;
            }

            
            $_dbh->commit();
            
            //return number of successful queries and total number of queries found
            return array(
                "success" => $success,
                "total" => $total
            );
            
        }

        
        
        
        
        
        
        /*
         * 
         * [ runScript ]
         * _________________________________________________________
         * 
         * Executes SQL Script file.
         * 
         */
        
        
        public static function runScript ($_filename) 
        {
            // Validate file path
            if (file_exists(SHARED_INC_PATH.'data/'.$_filename)) $_path = SHARED_INC_PATH.'data/'.$_filename;
            else if (file_exists(INC_PATH.'data/'.$_filename)) $_path = INC_PATH.'data/'.$_filename;
            else if (file_exists(ROOT_PATH.'setup/etc/sql/'.$_filename)) $_path = ROOT_PATH.'setup/etc/sql/'.$_filename;
            else if (file_exists($_filename)) $_path = $_filename;
            else throw new \Cataleya\Error('File does not exist: ' . $_filename);
            
            
            
            //load file
            $commands = array ();
            
            $lines = file($_path);
            $_hold = FALSE;
            $_delimiter = FALSE;
            $m = array ();
                       
            
            for ($i=0, $cl=count($lines); $i < $cl; $i++) {
                
                $line = $lines[$i];
                $tline = trim($line);
                
                if( empty ($tline) || preg_match('/^\s*(--|#).*/', $tline) > 0 ){
                    continue;
                }
                
                
                
                
                // ===== DELIMITERS (for TRIGGERS etc) ====== //
                
                if (preg_match('/^DELIMITER\s+([\$]+)/i', $tline, $m) > 0) {
                    $_delimiter = TRUE;
                    $commands[] = $lines[++$i];
                    continue;
                    
                } else if (preg_match('/^DELIMITER\s*/i', $tline, $m) > 0) {
                    // $commands[count($commands)-1] .= $line;
                    $_delimiter = FALSE;
                    continue;
                    
                } else if ($_delimiter === TRUE) {
                    
                    $commands[count($commands)-1] .= preg_replace('/\s*\$\$\s*/i', ';', $line);
                    continue;
                }
                
                   
                
                
            
                $_buffer2 = explode(";", $line);
                
                
                $c = count($_buffer2);
                
                if ($c === 1 && !$_hold) {
                    $commands[] = $line;
                    $_hold = TRUE;
                    continue;
                } else if ($c === 1 && $_hold) {
                    $commands[count($commands)-1] .= $line;
                    continue;
                }  else if ($c === 2 && $_hold && trim($_buffer2[1]) === '') {
                    $commands[count($commands)-1] .= $line;
                    $_hold = FALSE; 
                    continue;
                }  else if ($c === 2 && !$_hold) {
                    $commands[] = $line;
                    continue;
                }  else if ($c === 2 && $_hold && trim($_buffer2[1]) !== '') {
                    $commands[count($commands)-1] .= $_buffer2[0] . ';';
                    $commands[] = $_buffer2[1]; 
                    $_hold = TRUE;
                    continue;
                }
                
                
                
            }
                
             
            
            
            //file_put_contents(ROOT_PATH.'sql_test_script.sql', implode("\n\n-- ++++++++++++++++++++++ --\n\n", $commands));
            //die('DEBUG in ' . __CLASS__ . ' line: ' . __LINE__);

            
            
            
            $_dbh = self::getInstance();
            $_dbh->beginTransaction();
            
            //convert to array
            //$commands = explode(";", $commands);

            //run commands
            $total = $success = 0;
            foreach($commands as $command){
                if(trim($command) === '') continue;
                
                if (!$_dbh->query($command)) throw new \Cataleya\Error (implode(', ', $_dbh->errorInfo()).' -- { '.$command.' }');
                $success += 1;
                $total += 1;
            }

            
            $_dbh->commit();
            
            //return number of successful queries and total number of queries found
            return array(
                "success" => $success,
                "total" => $total
            );
            
        }

        
        
        /**
         * ping
         *
         * @param string $_table
         * @param string $_col
         * @param mixed $_val
         * @return boolean
         */
        public static function ping ($_table, $_col, $_val) 
        {

            $_expected = [
                    'table' => 'match:/^[a-zA-Z0-9_]{0,60}$/', 
                    'col' => 'match:/^[a-zA-Z0-9_]{0,60}$/'
            ];

            $_params = \Cataleya\Helper\Validator::params(
                $_expected, 
                    
                // Supplied params (arguments)...
                [
                    'table' => $_table, 
                    'col' => $_col
                ]
            );

            foreach ($_params as $k=>$v) if ($v === false) throw new \Cataleya\Error('Invalid argument: ' . $k . ' should be : ' . $_expected[$k]);

            $select_handle = self::getInstance()->prepare('
                                            SELECT ' . $_col . '
                                            FROM ' . $_table . ' 
                                            WHERE ' . $_col . ' = :instance_id 
                                            LIMIT 1 
                                            ');

            $select_handle_param_instance_id = $_val;
            $select_handle->bindParam(':instance_id', $select_handle_param_instance_id, self::getTypeConst($_val));

            if (!$select_handle->execute()) throw new Error ('DB Error: '.implode(', ', $select_handle->errorInfo())); 
            
            $_row = $select_handle->fetch(\PDO::FETCH_ASSOC);
            
            return !empty($_row);
                        
        }
        
        
        
        /*
         * 
         * 
         * [ sanitize ]
         * _________________________________________________
         * 
         * 
         */
        
        
        public static function sanitize (array $_tables, $_pk_column, $_pk) 
        {
            
                foreach ($_tables as $_table) 
                {

                    $delete_handle = self::getInstance()->prepare('
                                                            DELETE FROM ' . $_table . '  
                                                            WHERE ' . $_pk_column . ' = :instance_id
                                                            ');

                    $delete_handle_param_instance_id = $_pk;
                    $delete_handle->bindParam(':instance_id', $delete_handle_param_instance_id, self::getTypeConst($_pk));

                    if (!$delete_handle->execute()) \Cataleya\Helper\ErrorHandler::getInstance()->triggerException('
                                                                                    Error in class (' . __CLASS__ . '): [ ' . 
                                                                                    implode(', ', $delete_handle->errorInfo()) . 
                                                                                    ' ] on line ' . __LINE__);


                }
                
                return;
                        
        }
        
        
        
        
        /*
         * 
         * 
         * [ unhook ]
         * _________________________________________________
         * 
         * 
         */
        
        
        public static function unhook (array $_tables, $_pk_column, $_pk) 
        {
            
                foreach ($_tables as $_table) 
                {

                    $update_handle = self::getInstance()->prepare('
                                                            UPDATE ' . $_table . ' 
                                                            SET ' . $_pk_column . ' = NULL 
                                                            WHERE ' . $_pk_column . ' = :instance_id
                                                            ');

                    $update_handle_param_instance_id = $_pk;
                    $update_handle->bindParam(':instance_id', $update_handle_param_instance_id, self::getTypeConst($_pk));

                    if (!$update_handle->execute()) \Cataleya\Helper\ErrorHandler::getInstance()->triggerException('
                                                                                    Error in class (' . __CLASS__ . '): [ ' . 
                                                                                    implode(', ', $update_handle->errorInfo()) . 
                                                                                    ' ] on line ' . __LINE__);


                }
                
                return;
                        
        }
        
        
        
        
        
        
        
        
        
        /*
         * 
         * 
         * [ getChildren ]
         * _________________________________________________
         * 
         * 
         */
        
        
        public static function getChildren ($_table, $_parent_pk_column, $_parent_pk, $_select_column) 
        {
            

                $select_handle = self::getInstance()->prepare('
                                                        SELECT ' . $_select_column . '
                                                        FROM ' . $_table . ' 
                                                        WHERE ' . $_parent_pk_column . ' = :instance_id
                                                        ');

                $select_handle_param_instance_id = $_parent_pk;
                $select_handle->bindParam(':instance_id', $select_handle_param_instance_id, self::getTypeConst($_parent_pk));

                if (!$select_handle->execute()) \Cataleya\Helper\ErrorHandler::getInstance()->triggerException('
                                                                                Error in class (' . __CLASS__ . '): [ ' . 
                                                                                implode(', ', $select_handle->errorInfo()) . 
                                                                                ' ] on line ' . __LINE__);
                
                $_results = array ();

                while ($_row = $select_handle->fetch(\PDO::FETCH_ASSOC)) $_results[] = $_row[$_select_column];
                
                return $_results;
                        
        }
        
        
        
        
        
        


        
        /*
	 *
	 *  [ loadClassData ]
	 * ________________________________________________________________
	 * 
	 * 
	 *
	 *
	 
	public static function loadClassData ($_tables, $_pk_column, $_select_column, $_pk) 
	{

               
               static $select_handle;
               
               $_table_str = '';
               $_aliases = 'abcdefghijklmnopqrstuvwxyz';
               
               if (count ($_tables) > 1) {
                   
                   
                   for ($i=0; $i<=count($_tables); $i+=2) {
                       $a = substr($_aliases, $i, 1);
                       $b = substr($_aliases, $i+1, 1);
                       
                       $_table_str .= (!empty($_table_str)) ? ' AND ' : '';
                       
                       $_table_str .= $_tables[$i] . ' ' . $a . ' INNER JOIN ' . $_tables[$i] . ' ' . $b 
                               . ' ON ' . $a . '.' . $_pk_column . ' = ' . $b . '.' . $_pk_column;
                   }
               } else {
                   $_table_str = $_tables[0];
               }


                $select_handle = self::getInstance()->prepare('
                                                        SELECT ' . $_select_column . '
                                                        FROM ' . $_table_str . ' 
                                                        WHERE ' . $_parent_pk_column . ' = :instance_id
                                                        ');

               $param_instance_id = $_pk;
               $select_handle->bindParam(':instance_id', $param_instance_id, \PDO::PARAM_INT);



               if (!$select_handle->execute()) $this->e->triggerException('
                                                                            Error in class (' . __CLASS__ . '): [ ' . 
                                                                            implode(', ', $select_handle->errorInfo()) . 
                                                                            ' ] on line ' . __LINE__);

               $data = array ();
               while ($row = $select_handle->fetch(\PDO::FETCH_ASSOC))
               {
                   $data = $row;
               }   
                
                return $data;
	}
        
        
	 */
        


        
        
        
        
        
        
        
        /*
         * 
         * 
         * [ backup ]
         * _________________________________________________
         * 
         * 
         */
        
        
        public static function backup () 
        {
            



                /*
                Define the filename for the sql file
                If you plan to upload the  file to Amazon's S3 service , use only lower-case letters 
                */
                $fileName = 'mysqlbackup--' . date('d-m-Y') . '@'.date('h.i.s').'.sql' ; 
                $_BACKUP_DIR = ROOT_PATH . 'db';
                
                // Set execution time limit
                if(function_exists('max_execution_time')) {
                    if( ini_get('max_execution_time') > 0 ) set_time_limit(0) ;
                }


                // Check if directory is already created and has the proper permissions
                if (!file_exists($_BACKUP_DIR)) mkdir($_BACKUP_DIR , 0700) ;
                // if (!is_writable($_BACKUP_DIR)) chmod($_BACKUP_DIR , 0700) ; 


                $_dbh = self::getInstance();
                

                // Introduction information
                $return .= "--\n";
                $return .= "-- A Mysql Backup System \n";
                $return .= "--\n";
                $return .= '-- Export created: ' . date("Y/m/d") . ' on ' . date("h:i") . "\n\n\n";
                $return = "--\n";
                $return .= "-- Database : " . self::$_config->dbname . "\n";
                $return .= "--\n";
                $return .= "-- --------------------------------------------------\n";
                $return .= "-- ---------------------------------------------------\n";
                $return .= 'SET AUTOCOMMIT = 0 ;' ."\n" ;
                $return .= 'SET FOREIGN_KEY_CHECKS=0 ;' ."\n" ;
                $tables = array (); 
                
                
                
                // Exploring what tables this database has
                $result = $_dbh->query('SHOW TABLES' ) ;
                
                // Cycle through "$result" and put content into an array
                while ($row = $result->fetch(\PDO::FETCH_NUM)) $tables[] = $row[0];
                
                // Cycle through each  table
                 foreach($tables as $table)
                 { 
                        // Get content of each table
                        $result = $_dbh->query('SELECT * FROM '. $table) ; 
                        
                        
                        // Get number of fields (columns) of each table
                        $num_fields = $result->columnCount();
                        
                        
                        // Add table information
                        $return .= "--\n" ;
                        $return .= '-- Table structure for table `' . $table . '`' . "\n" ;
                        $return .= "--\n" ;
                        $return.= 'DROP TABLE  IF EXISTS `'.$table.'`;' . "\n" ; 
                        
                        // Get the table-shema
                        $shema = $_dbh->query('SHOW CREATE TABLE '.$table) ;
                        
                        // Extract table shema 
                        $tableshema = $shema->fetch(\PDO::FETCH_NUM) ;
                        
                        // Append table-shema into code
                        $return.= $tableshema[1].";" . "\n\n" ;
                        
                        // Cycle through each table-row
                        while($rowdata = $result->fetch(\PDO::FETCH_NUM)) 
                        { 
                            // Prepare code that will insert data into table 
                            $return .= 'INSERT INTO `'.$table .'`  VALUES ( '  ;
                            
                            // Extract data of each row 
                            for($i=0; $i<$num_fields; $i++) $return .= '"'.$rowdata[$i] . "\"," ;
                            
                             // Let's remove the last comma 
                             $return = substr("$return", 0, -1) ; 
                             $return .= ");" ."\n" ;
                         } 
                         $return .= "\n\n" ; 
                }
                
                
                
                $return .= 'SET FOREIGN_KEY_CHECKS = 1 ; '  . "\n" ; 
                $return .= 'COMMIT ; '  . "\n" ;
                $return .= 'SET AUTOCOMMIT = 1 ; ' . "\n"  ; 
                
                
                //$file = file_put_contents($fileName , $return) ; 
                $zip = new ZipArchive() ;
                $resOpen = $zip->open($_BACKUP_DIR. '/' .$fileName.".zip" , ZIPARCHIVE::CREATE) ;
                if( $resOpen ) $zip->addFromString( $fileName , "$return" ) ; 
                $zip->close() ;
                $fileSize = \Cataleya\Helper::getFileSizeUnit(filesize($_BACKUP_DIR. "/". $fileName . '.zip')) ; 
                $message = "done"; 

                

                
                return array (
                    'success' => TRUE, 
                    'file_size' => $fileSize, 
                    'path' => $_BACKUP_DIR. '/' .$fileName.".zip", 
                    'message' => $message
                );
                        
        }
                
        
        
        
        
        
        
        
        
        
	
	
}






