<?php


namespace Cataleya;







/*
 *
 * [ Cataleya \ Customer ]
 * ________________________________________________________________________
 *
 * (c) 2012 Fancy Paper Planes
 *
 *
*/





//if (!defined ('IS_ADMIN_FLAG')) die("Illegal Access");




class Customer implements \Cataleya\System\Event\Observable {
    
    
    
        protected static $_events = [
            'customer/created',
            'customer/updated',  
            'customer/deleted', 
            
            'customer/enabled',
            'customer/disabled', 
            
            'customer/logged-in',
            'customer/logged-out',  
            'customer/login-failed' 
            
            
        ];
            
        // Garbage Collection [ FIRST ]
        public static function garbageCollect () 
        {
                static $update_handle = NULL;
                
		// Do prepared statement for 'UPDATE'...
                
                if (empty($update_handle)) 
                {
                    $update_handle = \Cataleya\Helper\DBH::getInstance()->prepare('
                                                                        UPDATE customers SET is_online=0, last_modified=NOW()       
                                                                        WHERE last_modified < DATE_SUB( NOW(), INTERVAL 1440 second )
                                                                        ');
                }
                
                if (!$update_handle->execute()) \Cataleya\Helper\ErrorHandler::getInstance()->triggerException('Error in class (' . __CLASS__ . '): [ ' . implode(', ', $update_handle->errorInfo()) . ' ] on line ' . __LINE__);
		
                
                return TRUE;
        }



	
			
			
	private $dbh, $e, $bcrypt;
	private $_data = array();
	private $_modified = array();
        private $_wishlist;
        private $_default_contact;
        private $_contacts = array ();
	
	private function __construct($_id) 
	{
		// Get error handler
		$this->e = \Cataleya\Helper\ErrorHandler::getInstance();
		
		$_email = filter_var($_id, FILTER_VALIDATE_EMAIL);
                $_confirm_digest = filter_var($_id, FILTER_SANITIZE_STRIPPED);
		$_id = filter_var($_id, FILTER_VALIDATE_INT);
		
		if ($_email === FALSE && $_id === FALSE && $_confirm_digest === FALSE) 
		{
			if (defined('DEBUG_MODE') && DEBUG_MODE === TRUE) 
			{
			$this->e->triggerException('Error in class (' . __CLASS__ . '): [ Invalid username ] on line ' . __LINE__);
			}
			
			$this->_data = array();
			return NULL;
		}
		
		// Get database handle...
		$this->dbh = \Cataleya\Helper\DBH::getInstance();
		
		// Get encryption class...
		$this->bcrypt = \Cataleya\Helper\Bcrypt::getInstance();
		
		
		// ATTEMPT TO LOAD CUSTOMER DATA
		
		static $select_handle, $param_id;
		
		
		// Do prepared statement...
		if (empty($select_handle) ) {
		$select_handle = $this->dbh->prepare('
                                                        SELECT * FROM customers 
                                                        WHERE customer_id = :customer_id 
                                                        OR email_address = :customer_id 
                                                        OR confirm_digest = :customer_id 
                                                        LIMIT 1
                                                        ');
		$select_handle->bindParam(':customer_id', $param_id, \PDO::PARAM_INT);
		}
		
                if ($_id !== FALSE) $param_id = $_id;
                else if ($_email !== FALSE) $param_id = $_email;
                else $param_id = $_confirm_digest;
                
                
        
		// Execute...
		if (!$select_handle->execute()) $this->e->triggerError('Error in class (' . __CLASS__ . '): [ ' . implode(', ', $select_handle->errorInfo()) . ' ] on line ' . __LINE__);
		
		// Check for results...
		if ( ($this->_data = $select_handle->fetch(\PDO::FETCH_ASSOC)) === FALSE ) 
		{
			$this->_data = array();
		} else {
                    // Load user permissions...
                    
                }
                
                
	}
	
	
	function __destruct () 
	{
		
            $this->saveData();
		
	}
	



 


	/**
         * 
         * @param int $_id
         * @return \Cataleya\Customer
         * 
         */
	static public function load ($_id = FALSE) 
	{
		if ($_id === FALSE) return NULL;

		// Validate User ID
		$user = new \Cataleya\Customer ($_id);
		if (empty($user->_data))
		{
			unset($user);
			return NULL;
		}			
		
		
		////////////////// RETURN USER ////////////////////
		
		return $user;
                
         }



        
 




	/**
         * 
         * @param string $username
         * @param string $password
         * @param array $options
         * @return \Cataleya\Customer
         * 
         */
	static public function create ($username = '', $password = '', $options = array('firstname'=>'Firstname', 'lastname'=>'Lastname')) 
	{
		
		if (filter_var($username, FILTER_VALIDATE_EMAIL) === FALSE || $password === '') return NULL;

		 

		
		// Get database handle...
                $dbh = \Cataleya\Helper\DBH::getInstance();
		
		// Get error handler
		$e = \Cataleya\Helper\ErrorHandler::getInstance();

		// Get encryption class...
		$bcrypt = \Cataleya\Helper\Bcrypt::getInstance();
                
                
		// construct
		static 
				$insert_handle, 
                                $insert_handle_param_email, 
                                $insert_handle_param_pass, 
                                $insert_handle_param_firstname, 
                                $insert_handle_param_lastname, 
                                $insert_handle_param_confirm_digest, 
                                $insert_handle_param_confirm_code;
		
		if (empty($insert_handle))
		{
			$insert_handle = $dbh->prepare('
                                                        INSERT INTO customers 
                                                        (email_address, password, firstname, lastname, confirm_digest, confirm_num_code, date_created, last_modified) 
                                                        VALUES (:email, :pass, :firstname, :lastname, :confirm_digest, :confirm_num_code, NOW(), NOW())
                                                        ');
															
			$insert_handle->bindParam(':email', $insert_handle_param_email, \PDO::PARAM_STR);
			$insert_handle->bindParam(':pass', $insert_handle_param_pass, \PDO::PARAM_STR);
            $insert_handle->bindParam(':firstname', $insert_handle_param_firstname, \PDO::PARAM_STR);
            $insert_handle->bindParam(':lastname', $insert_handle_param_lastname, \PDO::PARAM_STR);
            $insert_handle->bindParam(':confirm_digest', $insert_handle_param_confirm_digest, \PDO::PARAM_STR);
            $insert_handle->bindParam(':confirm_num_code', $insert_handle_param_confirm_code, \PDO::PARAM_STR);
		}
		
		$insert_handle_param_email = $username;
		$insert_handle_param_pass = $bcrypt->hash($password);
                
        $insert_handle_param_firstname = isset($options['firstname']) ? ucwords($options['firstname']) : 'Firstname';
        $insert_handle_param_lastname = isset($options['lastname']) ? ucwords($options['lastname']) : 'Lastname';
        
        // Generate confirm digest...
        $insert_handle_param_confirm_digest = sha1(strval(rand(0,microtime(true))) + $username + strval(microtime(true)));
        $insert_handle_param_confirm_code = strval(rand(100000, 999999));
		
		if (!$insert_handle->execute()) $e->triggerException('
										Error in class (' . __CLASS__ . '): [ ' . 
										implode(', ', $insert_handle->errorInfo()) . 
										' ] on line ' . __LINE__);
										
		
		// AUTOLOAD NEW USER AND RETURN IT
		$customer = self::load($dbh->lastInsertId());
                
                
                \Cataleya\System::load()->updateCustomerCount(1);
		\Cataleya\System\Event::notify('customer/created', $customer, array ());
		
		return $customer;
		
		
	}





        
    
    /**
     * getEvents
     * 
     * @return array
     */
    public static function getEvents() {

        return self::$_events;
    }
        




/*
 *
 * [ AUTHENTICATE ] 
 * __________________________________________________________________________________
 *
 *
 *
 */	
	
	
	





	///// LOGIN /////////////////

	public static function login ($_id = FALSE, $_pass = FALSE)
	{
		if ($_id === FALSE || $_pass === FALSE) return NULL;
		
		// Validate User ID
		$user = new \Cataleya\Customer ($_id);
		if (empty($user->_data))
		{
			unset($user);        
                        \Cataleya\Helper\Stats::updateFailedLogins(FALSE);
                        if (\Cataleya\Helper\Stats::detectBruteForce(FALSE)) sleep(4);
                        
			return NULL;
		}
		
		
		// Validate Password
		if (FALSE === $user->bcrypt->verify($_pass, $user->_data['password']))
		{
			unset($user);       
                        \Cataleya\Helper\Stats::updateFailedLogins(FALSE);
                        if (\Cataleya\Helper\Stats::detectBruteForce(FALSE)) sleep(4);
                        
			return NULL;
		}
		
		
		
		///////////// UPDATE LOGIN STATUS ///////////////////////////
		
		$_update_params = array();

		// Do prepared statement for 'UPDATE'...
		$update_handle = $user->dbh->prepare('
												UPDATE customers 
												SET is_online = :is_online, last_logon = NOW(), last_modified = NOW()      
												WHERE customer_id = :instance_id 
												');
		$update_handle->bindParam(':instance_id', $_update_params['customer_id'], \PDO::PARAM_INT);
		$update_handle->bindParam(':is_online', $_update_params['is_online'], \PDO::PARAM_STR);
		
		
		$_update_params['customer_id'] = $user->_data['customer_id'];
		$_update_params['is_online'] = $user->_data['is_online'] = md5( uniqid(rand(), TRUE) ) . ':' . (CURRENT_DT+LOGIN_EXPIRES_AFTER);

		
		if (!$update_handle->execute()) $user->e->triggerException('Error in class (' . __CLASS__ . '): [ ' . implode(', ', $update_handle->errorInfo()) . ' ] on line ' . __LINE__);
		
		
		////////////////// RETURN USER ////////////////////
		
		\Cataleya\System\Event::notify('customer/logged-in', $user, array ('mode'=>'standard login'));
		return $user;
		
	}
	



        

	///// LOGOUT /////////////////

	public function logout ()
	{
		
		
		///////////// UPDATE LOGIN STATUS ///////////////////////////
		
		$_update_params = array();

		// Do prepared statement for 'UPDATE'...
		$update_handle = $this->dbh->prepare('
                                                        UPDATE customers 
                                                        SET is_online = 0  
                                                        WHERE customer_id = :instance_id 
                                                        ');
		$update_handle->bindParam(':instance_id', $_update_params['customer_id'], \PDO::PARAM_INT);
		
		
		$_update_params['customer_id'] = $this->_data['customer_id'];
		
		if (!$update_handle->execute()) $this->e->triggerException('Error in class (' . __CLASS__ . '): [ ' . implode(', ', $update_handle->errorInfo()) . ' ] on line ' . __LINE__);
		
		
		\Cataleya\System\Event::notify('customer/logged-out', $this, array ());
                
		return $this;
		
	}
	


        



	///// AUTH // Returns $USER instance if online or NULL otherwise /////////////////

	public static function authenticate ($_id = FALSE, $_is_online = FALSE, $_auto_logout = TRUE)
	{
		if ($_id === FALSE || $_is_online === FALSE) return NULL;
		
		
		$_update_params = array();
        
		// 1. User exists..
		$user = new \Cataleya\Customer ($_id);
		if (empty($user->_data))
		{
			unset($user);
			return null;
		}
        
        // 2. Token is set...
		if ($user->_data['is_online'] === '0' || $user->_data['is_online'] === 0) 
        {
            // No: not online
			unset($user);
            return null;
        }
        
        
        // 3. Validate token...
		list($token, $timestamp) = explode(':', $user->_data['is_online']);
		if ($token == $_is_online )
		{
			
			if ( (int)$timestamp < (int)CURRENT_DT )
			{
				$this->logout();
                return null;
			}
            
            // Set new timestamp
            $_update_params['is_online'] = ($token . ':' . (CURRENT_DT+LOGIN_EXPIRES_AFTER));
			
		} else {
			// $this->logout();
            unset($user);
            return null;
        }
        
        
        
        
        
        
        
		
		
		
		///////////// UPDATE LOGIN STATUS ///////////////////////////
		

		// Do prepared statement for 'UPDATE'...
		$update_handle = $user->dbh->prepare('
												UPDATE customers 
												SET is_online = :is_online, last_modified = NOW()    
												WHERE customer_id = :customer_id 
												');
		$update_handle->bindParam(':customer_id', $_update_params['customer_id'], \PDO::PARAM_INT);
		$update_handle->bindParam(':is_online', $_update_params['is_online'], \PDO::PARAM_STR);
		
		$_update_params['customer_id'] = $user->_data['customer_id'];
		
        $user->_data['is_online'] = $_update_params['is_online'];



		
		if (!$update_handle->execute()) $user->e->triggerException('Error in class (' . __CLASS__ . '): [ ' . implode(', ', $update_handle->errorInfo()) . ' ] on line ' . __LINE__);
		
		
		
			
		// Return $USER (or NULL!!) ...
		return $user;
		
		
		
	}



        
        
        




        
	///// CONFIRM PASSWORD /////////////////

	public function confirmPassword ($_pass = FALSE)
	{
		if ($_pass === FALSE) return FALSE;
		
		// Validate Password
		return $this->bcrypt->verify($_pass, $this->_data['password']);
		
	}
	


        



/*
 *
 * [GETTERS] and [SETTERS] 
 * __________________________________________________________________________________
 *
 *
 *
 */	
	



	
	

        
        
        


	/*
	 *
	 *  [ saveData ]
	 * ________________________________________________________________
	 * 
	 * 
	 *
	 *
	 *
	 */	


	public function saveData () 
	{
		

		if (empty($this->_modified)) return $this;
                
                $this->_modified = array_unique($this->_modified);
                
		$_update_params = array();
		$key_val_pairs = array();
		
		foreach ($this->_modified as $key) $key_val_pairs[$key] = $key . ' = :' . $key;
		
	
		// Do prepared statement for 'UPDATE'...
		$update_handle = $this->dbh->prepare('
												UPDATE customers 
												SET last_modified = NOW(), ' . implode (', ', $key_val_pairs) . ', last_modified=now()       
												WHERE customer_id = :customer_id 
												');
		$_update_params['customer_id'] = $this->_data['customer_id'];
		$update_handle->bindParam(':customer_id', $_update_params['customer_id'], \PDO::PARAM_INT);
                
		foreach ($this->_modified as $key) {
                    
                        $_update_params[$key] = $this->_data[$key];                        
			$update_handle->bindParam(':'.$key, $_update_params[$key], \Cataleya\Helper\DBH::getTypeConst($_update_params[$key]));

		}
		
		

		
		if (!$update_handle->execute()) $this->e->triggerException('Error in class (' . __CLASS__ . '): [ ' . implode(', ', $update_handle->errorInfo()) . ' ] on line ' . __LINE__);
		
                \Cataleya\System\Event::notify('customer/updated', $this, array ('fields'=>$this->_modified));
                
                return $this;
	}
	
	

        
        
        
   
	
	// Full name...

	public function getName () 
	{
		return ($this->_data['firstname'] . ' ' . $this->_data['lastname']);
		
	}
	






	// Login Token

	public function getLoginToken () 
	{
            
            $token = 0;
            
            if (strlen(strval($this->_data['is_online'])) > 1)
            {
                list($token, $timestamp) = explode(':', $this->_data['is_online']);

                if ( (int)$timestamp < CURRENT_DT )
                {
                        $this->_data['is_online'] = $token = 0;
                        $this->_modified[] = 'is_online';
                }
            }
                
            return $token;		
		
	}
	




        /*
         *
         * [ getCustomerId ] 
         *_____________________________________________________
         *
         *
         */

        public function getCustomerId()
        {
                return (int)$this->_data['customer_id'];
        }
        
        
        

        /**
         * 
         * @return int
         * 
         */
        public function getID()
        {
                return (int)$this->_data['customer_id'];
        }
        
        
        
        
         /**
         *
         * [ getWishlist ] 
         *_____________________________________________________
         *
         *
         */

        public function getWishlist()
        {
                if (empty($this->_wishlist)) 
                {
                    $_key = \Cataleya\Helper\DBH::getChildren('wishlists', 'customer_id', $this->getCustomerId(), 'secure_key');
                    
                    if (empty($_key)) $this->_wishlist = \Cataleya\Customer\Wishlist::create ($this);
                    else $this->_wishlist = \Cataleya\Customer\Wishlist::load ($_key[0]);
                }
                
                return $this->_wishlist;
        }





        /*
         *
         * [ getTitle ] 
         *_____________________________________________________
         *
         *
         */

        public function getTitle()
        {
                return $this->_data['title'];
        }



        /*
         *
         * [ setTitle ] 
         *_____________________________________________________
         *
         *
         */

        public function setTitle($value = NULL)
        {
                if ($value===NULL) return FALSE;

                $this->_data['title'] = $value;
                $this->_modified[] = 'title';

                return $this;
        }



        /*
         *
         * [ getFirstname ] 
         *_____________________________________________________
         *
         *
         */

        public function getFirstname()
        {
                return $this->_data['firstname'];
        }



        /*
         *
         * [ setFirstname ] 
         *_____________________________________________________
         *
         *
         */

        public function setFirstname($value = NULL)
        {
                if ($value===NULL) return FALSE;

                $this->_data['firstname'] = $value;
                $this->_modified[] = 'firstname';

                return $this;
        }



        /*
         *
         * [ getLastname ] 
         *_____________________________________________________
         *
         *
         */

        public function getLastname()
        {
                return $this->_data['lastname'];
        }



        /*
         *
         * [ setLastname ] 
         *_____________________________________________________
         *
         *
         */

        public function setLastname($value = NULL)
        {
                if ($value===NULL) return FALSE;

                $this->_data['lastname'] = $value;
                $this->_modified[] = 'lastname';

                return $this;
        }



        /*
         *
         * [ getNickname ] 
         *_____________________________________________________
         *
         *
         */

        public function getNickname()
        {
                return $this->_data['nickname'];
        }



        /*
         *
         * [ setNickname ] 
         *_____________________________________________________
         *
         *
         */

        public function setNickname($value = NULL)
        {
                if ($value===NULL) return FALSE;

                $this->_data['nickname'] = $value;
                $this->_modified[] = 'nickname';

                return $this;
        }



        /*
         *
         * [ getGender ] 
         *_____________________________________________________
         *
         *
         */

        public function getGender()
        {
                return $this->_data['gender'];
        }



        /*
         *
         * [ setGender ] 
         *_____________________________________________________
         *
         *
         */

        public function setGender($value = NULL)
        {
                if ($value===NULL) return FALSE;

                $this->_data['gender'] = $value;
                $this->_modified[] = 'gender';

                return $this;
        }



        /*
         *
         * [ getDob ] 
         *_____________________________________________________
         *
         *
         */

        public function getDob()
        {
                return $this->_data['dob'];
        }



        /*
         *
         * [ setDob ] 
         *_____________________________________________________
         * 
         * note: make sure date is in format [mm/dd/yyyy]...
         *
         *
         */

        public function setDob($value = NULL)
        {
                if ($value===NULL || !is_string($value) || preg_match('#^(\d{1,2}[\/|-|\s|,|\:]){2}(?:19|20)\d{2}$#', $value) === 0) return FALSE;
                
                $date = new \DateTime(preg_replace('!^(\d{1,2})[\/-\s,\:]{1}(\d{1,2})[\/-\s,\:]{1}(\d{2,4})$!', '${2}/${1}/${3}', $value)); 

                $this->_data['dob'] = date_format($date, 'Y-m-d');
                $this->_modified[] = 'dob';

                return $this;
        }



        /*
         *
         * [ getEmailAddress ] 
         *_____________________________________________________
         *
         *
         */

        public function getEmailAddress()
        {
                return $this->_data['email_address'];
        }



        /*
         *
         * [ setEmailAddress ] 
         *_____________________________________________________
         *
         *
         */

        public function setEmailAddress($value = NULL, $_require_confirm = TRUE)
        {
                if (filter_var($value, FILTER_VALIDATE_EMAIL) === FALSE) return FALSE;

                $this->_data['email_address'] = $value;
                $this->_modified[] = 'email_address';
                
                if ($_require_confirm) 
                {
                    $this->setConfirmDigest();
                    $this->setConfirmCode();
                }

                $this->saveData();
                
                return $this;
        }


        




        /**
         *
         * @param \Cataleya\Asset\Contact $_Contact
         *
         *
         */
        public function addContact (\Cataleya\Asset\Contact $_Contact)
        {

            static 
                    $contact_insert, 
                    $param_id, 
                    $param_contact_id;


            if (!isset($contact_insert)) {
                    // PREPARE SELECT STATEMENT...
                    $contact_insert = $this->dbh->prepare('
                        INSERT INTO customer_contacts (customer_id, contact_id)   
                        VALUES (:customer_id, :contact_id)
                        ');

                    $contact_insert->bindParam(':customer_id', $param_id, \PDO::PARAM_INT);
                    $contact_insert->bindParam(':contact_id', $param_contact_id, \PDO::PARAM_INT);
            }


            $param_id = $this->getID();
            $param_contact_id = $_Contact->getID();

            if (!$contact_insert->execute())  $this->e->triggerException('
                                    Error in class (' . __CLASS__ . '): [ ' . 
                                    implode(', ', $contact_insert->errorInfo()) . 
                                    ' ] on line ' . __LINE__);


            // Reset base contact cache (it will be repopulated the next time [getBaseImages] is called.
            $this->_contacts = array ();

        }






        /**
         *
         * @param \Cataleya\Asset\Contact $_Contact
         *
         *
         */
        public function removeContact (\Cataleya\Asset\Contact $_Contact)
        {


                        // Reset contact cache (it will be repopulated the next time [getContacts] is called.
                        $this->_contacts = array ();





                        // Destroy contact...
                        $_Contact->delete();


                        return true;

        }







        /*
         *
         *  [ getContacts ]
         * ________________________________________________________________
         * 
         * Returns array of contact objects
         *
         *
         *
         */


        public function getContacts ($_FORCE_RELOAD = FALSE)
        {
                if (empty($this->_contacts) || $_FORCE_RELOAD === TRUE )
                {



                        static $contacts_select, $param_id;


                        if (!isset($contacts_select)) {
                                // PREPARE SELECT STATEMENT...
                                $contacts_select = $this->dbh->prepare('
                                                    SELECT * FROM customer_contacts 
                                                    WHERE customer_id = :customer_id
                                                    ');

                                $contacts_select->bindParam(
                                        ':customer_id', 
                                        $param_id, \PDO::PARAM_INT
                                        );
                        }


                        $param_id = $this->getID();

                        if (!$contacts_select->execute())  $this->e->triggerException('
                                        Error in class (' . __CLASS__ . '): [ ' . 
                                        implode(', ', $contacts_select->errorInfo()) . 
                                        ' ] on line ' . __LINE__);

                        while ($row = $contacts_select->fetch(\PDO::FETCH_ASSOC)) 
                        {
                                $this->_contacts[$row['contact_id']] = \Cataleya\Asset\Contact::load($row['contact_id']);
                        }


                }

                return $this->_contacts;

        }











        /**
         *
         * @return \Cataleya\Asset\Contact
         *
         *
         */
        public function getDefaultContact ()
        {
                if (empty($this->_default_contact) )
                {
                        $this->_default_contact = \Cataleya\Asset\Contact::load(
                                $this->_data['default_contact_id']
                                );
                }

                return $this->_default_contact;

        }










        /**
         *
         * @param \Cataleya\Asset\Contact $_Contact
         *
         *
         */
        public function setDefaultContact (\Cataleya\Asset\Contact $_Contact)
        {

                $this->getContacts();
                if (!key_exists($_Contact->getID(), $this->_contacts)) $this->addContact ($_Contact);

                $this->_data['default_contact_id'] = $_Contact->getID();
                $this->_default_contact = $_Contact;

                if (!in_array('default_contact_id', $this->_modified)) $this->_modified[] = 'default_contact_id';

        }





        /*
         *
         *  [ usetDefaultContact ]
         * ________________________________________________________________
         * 
         * 
         *
         *
         *
         */


        public function unsetDefaultContact ()
        {
                $this->_data['default_contact_id'] = NULL;
                $this->_default_contact = NULL;

                if (!in_array('default_contact_id', $this->_modified)) $this->_modified[] = 'default_contact_id';

        }


        



        /*
         *
         * [ getTelephone ] 
         *_____________________________________________________
         *
         *
         */

        public function getTelephone()
        {
                return empty($this->_data['telephone']) ? 'no number' : $this->_data['telephone'];
        }



        /*
         *
         * [ setTelephone ] 
         *_____________________________________________________
         *
         *
         */

        public function setTelephone($value = NULL)
        {
                if ($value===NULL) return FALSE;

                $this->_data['telephone'] = $value;
                $this->_modified[] = 'telephone';

                return $this;
        }



        /*
         *
         * [ getFax ] 
         *_____________________________________________________
         *
         *
         */

        public function getFax()
        {
                return $this->_data['fax'];
        }



        /*
         *
         * [ setFax ] 
         *_____________________________________________________
         *
         *
         */

        public function setFax($value = NULL)
        {
                if ($value===NULL) return FALSE;

                $this->_data['fax'] = $value;
                $this->_modified[] = 'fax';

                return $this;
        }





        /*
         *
         * [ setPassword ] 
         *_____________________________________________________
         *
         *
         */

        public function setPassword($value = NULL)
        {
                if ($value===NULL) return FALSE;

                $this->_data['password'] = $this->bcrypt->hash($value);
                $this->_modified[] = 'password';

                
                return $this;
        }



        /*
         *
         * [ getConfirmCode ] 
         *_____________________________________________________
         *
         *
         */

        public function getConfirmCode()
        {
                return $this->_data['confirm_num_code'];
        }
        
        
        
        
        /*
         *
         * [ setConfirmCode ] 
         *_____________________________________________________
         *
         *
         */

        private function setConfirmCode()
        {
                $this->_data['confirm_num_code'] = strval(rand(100000, 999999));
                $this->_modified[] = 'confirm_num_code';

                return $this;
        }

        
        
        
        /*
         *
         * [ getConfirmDigest ] 
         *_____________________________________________________
         *
         *
         */

        public function getConfirmDigest()
        {
                return $this->_data['confirm_digest'];
        }



        /*
         *
         * [ setConfirmDigest ] 
         *_____________________________________________________
         *
         *
         */

        private function setConfirmDigest()
        {

                $this->_data['confirm_digest'] = sha1(strval(rand(0,microtime(true))) . $this->_data['email_address'] . strval(microtime(true)));
                $this->_modified[] = 'confirm_digest';

                return $this;
        }


        
        
        
        /*
         *
         * [ isConfirmed ] 
         *_____________________________________________________
         *
         *
         */

        public function isConfirmed()
        {
            return ($this->_data['confirm_num_code'] === '0') ? TRUE : FALSE;
        }



        /*
         *
         * [ confirm ] 
         *_____________________________________________________
         *
         *
         */

        public function confirm($token = NULL, $num_code = NULL)
        {
                if (!is_numeric($num_code) || !is_string($token)) return FALSE;
                
                if ($token === $this->_data['confirm_digest'] && (strval($num_code) === $this->_data['confirm_num_code'] || $this->_data['confirm_num_code'] === '0'))
                {
                    $this->_data['confirm_num_code'] = '0';
                    $this->_modified[] = 'confirm_num_code';
                    
                    
                    \Cataleya\System\Event::notify('customer/email-confirmed', $this, array ());
                    return TRUE;
                } else {
                    return FALSE;
                }

        }

        

      
        

        /*
         * 
         * [ enable ]
         * ______________________________________________________
         * 
         * 
         */


        public function enable () 
        {

            if ((int)$this->_data['active'] === 0) 
            {
                $this->_data['active'] = 1;
                $this->_modified[] = 'active';
            }

            
            \Cataleya\System\Event::notify('customer/enabled', $this, array ());
            return $this;
        }





        /*
         * 
         * [ disable ]
         * ______________________________________________________
         * 
         * 
         */


        public function disable () 
        {
            if ((int)$this->_data['active'] === 1) 
            {
                $this->_data['active'] = 0;
                $this->_modified[] = 'active';
            }

            
            \Cataleya\System\Event::notify('customer/disabled', $this, array ());
            return $this;
        }
        
        
        
        
        
        

        /*
         *
         * [ getNewsletter ] 
         *_____________________________________________________
         *
         *
         */

        public function getNewsletter()
        {
                return $this->_data['newsletter'];
        }



        /*
         *
         * [ setNewsletter ] 
         *_____________________________________________________
         *
         *
         */

        public function setNewsletter($value = NULL)
        {
                if ($value===NULL || !is_bool($value)) return FALSE;

                $this->_data['newsletter'] = $value;
                $this->_modified[] = 'newsletter';

                return $this;
        }
        
        
        
        /*
         *
         * [ isSubscribed ] 
         *_____________________________________________________
         *
         *
         */

        public function isSubscribed()
        {
                return ((int)$this->_data['newsletter'] === 1) ? TRUE : FALSE;
        }
        
        



        /*
         *
         * [ getGroupPricing ] 
         *_____________________________________________________
         *
         *
         */

        public function getGroupPricing()
        {
                return $this->_data['group_pricing'];
        }



        /*
         *
         * [ setGroupPricing ] 
         *_____________________________________________________
         *
         *
         */

        public function setGroupPricing($value = NULL)
        {
                if ($value===NULL) return FALSE;

                $this->_data['group_pricing'] = $value;
                $this->_modified[] = 'group_pricing';

                return $this;
        }



        /*
         *
         * [ getEmailFormat ] 
         *_____________________________________________________
         *
         *
         */

        public function getEmailFormat()
        {
                return $this->_data['email_format'];
        }



        /*
         *
         * [ setEmailFormat ] 
         *_____________________________________________________
         *
         *
         */

        public function setEmailFormat($value = NULL)
        {
                if ($value===NULL) return FALSE;

                $this->_data['email_format'] = $value;
                $this->_modified[] = 'email_format';

                return $this;
        }



        /*
         *
         * [ getAuthorization ] 
         *_____________________________________________________
         *
         *
         */

        public function getAuthorization()
        {
                return $this->_data['authorization'];
        }



        /*
         *
         * [ setAuthorization ] 
         *_____________________________________________________
         *
         *
         */

        public function setAuthorization($value = NULL)
        {
                if ($value===NULL || !is_bool($value)) return FALSE;

                $this->_data['authorization'] = $value;
                $this->_modified[] = 'authorization';

                return $this;
        }



        /*
         *
         * [ getCustomerReferral ] 
         *_____________________________________________________
         *
         *
         */

        public function getCustomerReferral()
        {
                return $this->_data['customer_referral'];
        }



        /*
         *
         * [ setCustomerReferral ] 
         *_____________________________________________________
         *
         *
         */

        public function setCustomerReferral($value = NULL)
        {
                if ($value===NULL) return FALSE;

                $this->_data['customer_referral'] = $value;
                $this->_modified[] = 'customer_referral';

                return $this;
        }



        /*
         *
         * [ isOnline ] 
         *_____________________________________________________
         *
         *
         */

        public function isOnline()
        {
                return (strval($this->getLoginToken()) === '0') ? FALSE : TRUE;
        }




        /*
         *
         * [ quickLogin ] 
         *_____________________________________________________
         *
         *
         */

        public function quickLogin()
        {

            $this->_data['is_online'] = md5( uniqid(rand(), TRUE) ) . ':' . (CURRENT_DT+LOGIN_EXPIRES_AFTER);
            $this->_modified[] = 'is_online';

            \Cataleya\System\Event::notify('customer/logged-in', $this, array ('mode'=>'quick login'));
            return $this;
            
        }




	
}






// Do a chrone job everytime this class file is loaded
\Cataleya\Customer::garbageCollect();


