<?php


namespace Cataleya;



/*

CLASS APPLICATION SETTINGS

*/



class System   
implements \Cataleya\System\Event\Observable 
{
	
	
	private $_data = array();
	private $_modified = array();
    private static $_instance = NULL;


    private $dbh, $e;
	


    
        protected static $_events = [
            'system/initialized',
            'system/on',
            'system/off',
            'system/settings/updated'
            
            
        ];


	/*
	 *
	 * [ __construct ]
	 * ________________________________________________________________
	 *
	 *
	 *
	 */
	 
	 
	private function __construct ()
	{
		
		// Get database handle...
                $this->dbh = \Cataleya\Helper\DBH::getInstance();
		
		// Get error handler
		$this->e = \Cataleya\Helper\ErrorHandler::getInstance();
		
	}
	




	/*
	 *
	 * [ __destruct ]
	 * ________________________________________________________________
	 *
	 *
	 *
	 */

        public function __destruct()
        {
                    $this->saveData();
        }
	




	/**
         * 
         * @return \Cataleya\System
         * 
         */
	public static function load ()
	{
            
                if (!empty(self::$_instance)) return self::$_instance;
                
                

		$instance = new \Cataleya\System ();
		
		// LOAD DATA
		static $select_handle = NULL;
		
			
		if (empty($select_handle)) {
			// PREPARE SELECT STATEMENT...
			$select_handle = $instance->dbh->prepare('SELECT * FROM application_preferences WHERE pk = 0 LIMIT 1');
		}

		
		if (!$select_handle->execute()) throw new \Cataleya\Error('DB Error: ' . implode(', ', $select_handle->errorInfo()));
		
		$instance->_data = $select_handle->fetch(\PDO::FETCH_ASSOC);
                
		if (empty($instance->_data))
		{
			unset($instance);
			return NULL;
		}


        self::$_instance = $instance;
		return self::$_instance;	
		
		
	}



	/**
         * 
         * @param array $_options
         * @return \Cataleya\System
         * 
         */
	static public function create (array $_options = array()) 
	{
		
		
				
               // Check if app settings table is already instantiated
		$_instance = self::load();
                if (!empty($_instance)) return $_instance;


               ////////////// APP SETTINGS ABOUT TO BE SETUP FOR THE FIRST & LAST TIME /////////////////////
                
                
		
		// Get database handle...
                $dbh = \Cataleya\Helper\DBH::getInstance();
		
		// Get error handler
		$e = \Cataleya\Helper\ErrorHandler::getInstance();
                
                
                // Validate params
                $_options['app_version'] = filter_var($_options['app_version'], FILTER_VALIDATE_REGEXP, array('options'=>array('regexp' => '/^([0-9]+\.?){1,5}$/')));
                $_options['client'] = filter_var($_options['client'], FILTER_VALIDATE_REGEXP, array('options'=>array('regexp' => '/^[-\s\.A-Za-z0-9\(\)]{1,150}$/')));
                $_options['developer'] = filter_var($_options['developer'], FILTER_VALIDATE_REGEXP, array('options'=>array('regexp' => '/^[-\s\.A-Za-z0-9\(\)]{1,150}$/')));
                $_options['credits'] = filter_var($_options['credits'], FILTER_VALIDATE_REGEXP, array('options'=>array('regexp' => '/^[-\s\.A-Za-z0-9\'_\W]{1,150}$/')));
                // $_options['license'] = filter_var($_options['license'], FILTER_SANITIZE_STRIPPED);
                $_options['active'] = filter_var($_options['active'], FILTER_VALIDATE_BOOLEAN, FILTER_NULL_ON_FAILURE);
		
                
                
		// construct
		
                $insert_handle_param_app_version = (is_string($_options['app_version'])) ? $_options['app_version'] : '1.0';
                $insert_handle_param_developer = (is_string($_options['developer'])) ? $_options['developer'] : 'unknown';
                $insert_handle_param_client = (is_string($_options['client'])) ? $_options['client'] : 'unknown';
                $insert_handle_active = (isset($_options['active']) && is_bool($_options['active'])) ? $_options['active'] : FALSE;
                $insert_handle_param_credits = (is_string($_options['credits'])) ? $_options['credits'] : 'Team? Just me for now :). -Gboyega for FPP.';
		$insert_handle_param_license = (is_string($_options['license'])) ? $_options['license'] : 'FPP Labs (c)2015.';
		
		$dbh->beginTransaction();

                $insert_handle = $dbh->prepare('
                                                INSERT INTO application_preferences  
                                                (app_version, developer, credits, client, license, active, last_modified, last_shutdown, last_Activated) 
                                                VALUES (:app_version, :developer, :credits, :client, :license, :active, NOW(), NOW(), NOW())
                                                ');

                $insert_handle->bindParam(':app_version', $insert_handle_param_app_version, \PDO::PARAM_STR);
                $insert_handle->bindParam(':developer', $insert_handle_param_developer, \PDO::PARAM_STR);
                $insert_handle->bindParam(':client', $insert_handle_param_client, \PDO::PARAM_STR);
                $insert_handle->bindParam(':credits', $insert_handle_param_credits, \PDO::PARAM_STR);
                $insert_handle->bindParam(':license', $insert_handle_param_license, \PDO::PARAM_STR);
                $insert_handle->bindParam(':active', $insert_handle_active, \PDO::PARAM_BOOL);

		if (!$insert_handle->execute()) $e->triggerException('
										Error in class (' . __CLASS__ . '): [ ' . 
										implode(', ', $insert_handle->errorInfo()) . 
										' ] on line ' . __LINE__);
										
		
                // Insert a dummy row to prevent any more insertions
                if ($dbh->query('' .
                    'INSERT INTO application_preferences (' . 
                    'pk, app_version, credits, license, active, last_modified, ' . 
                    'last_shutdown, last_Activated) ' . 
                    'VALUES (1, "0", "FPP", "FPP", 0, NOW(), NOW(), NOW())'
                     ) === FALSE) throw new \Cataleya\Error ('DB Error: '.implode(', ', $dbh->errorInfo()) );

                else $dbh->commit();
                
                \Cataleya\System\Event::notify('system/initialized', null, array('scope'=>'system/initialiazed'));
                
		// AUTOLOAD AND RETURN
		return self::load();
		
		
	}


	 


    
    /**
     * getEvents
     * 
     * @return array
     */
    public static function getEvents() {

        return self::$_events;
    }





        /**
         * 
         * @return int
         * 
         */
        public function getID()
        {
                return $this->_data['currency_code'];
        }




	
	/**
         * 
         * @return \Cataleya\System
         * 
         */
	public function saveData () 
	{
		

		if (empty($this->_modified)) return;
                $this->_modified = array_unique($this->_modified);
                
		$_update_params = array();
		$key_val_pairs = array();
		
		foreach ($this->_modified as $key) { 
                    $key_val_pairs[$key] = $key . ' = :' . $key;
                }
		
	
		// Do prepared statement for 'INSERT'...
		$update_handle = $this->dbh->prepare('
                                                        UPDATE application_preferences  
                                                        SET ' . implode (', ', $key_val_pairs) . '    
                                                        WHERE pk = 0  
                                                        ');
                
		foreach ($this->_modified as $key) {
                        
			$update_handle->bindParam(':'.$key, $_update_params[$key], \Cataleya\Helper\DBH::getTypeConst($_update_params[$key]));
                        $_update_params[$key] = $this->_data[$key];
                        
		}
		
		

		
		if (!$update_handle->execute()) $this->e->triggerException('Error in class (' . __CLASS__ . '): [ ' . implode(', ', $update_handle->errorInfo()) . ' ] on line ' . __LINE__);
                
                \Cataleya\System\Event::notify('system/settings/updated', $this, array('scope'=>'system/updated'));
                
                return $this;
		
	}
	
	
       
 
 
        
        
        
        
        
        
        

        


        /**
         * 
         * @return string
         * 
         */
        public function getAppName()
        {
                return $this->_data['app_name'];
        }



        /*
         *
         * [ setAppName ] 
         *_____________________________________________________
         *
         *
         *

        public function setAppName($value = NULL)
        {

                
                if ($value===NULL) $this->e->triggerException('Error in class (' . __CLASS__ . '): [ invalid argument for method: ' . __FUNCTION__ . ' ] on line ' . __LINE__);

                $this->_data['app_name'] = $value;
                $this->_modified[] = 'app_name';

                return $this;
        }
        
         */
        
        
        
        

        /**
         * 
         * @return string
         * 
         */
        public function getAppCredits()
        {
                return $this->_data['credits'];
        }



        /*
         *
         * [ setAppCredits ] 
         *_____________________________________________________
         *
         *
         *

        public function setAppCredits($value = NULL)
        {

                
                if ($value===NULL) $this->e->triggerException('Error in class (' . __CLASS__ . '): [ invalid argument for method: ' . __FUNCTION__ . ' ] on line ' . __LINE__);

                $this->_data['credits'] = $value;
                $this->_modified[] = 'app_name';

                return $this;
        }
        
         */
        
        
        



        /**
         * 
         * @return string
         * 
         */
        public function getAppVersion()
        {
                return $this->_data['app_version'];
        }



        /*
         *
         * [ setAppVersion ] 
         *_____________________________________________________
         *
         *
         

        public function setAppVersion($value = NULL)
        {


                if ($value===NULL) $this->e->triggerException('Error in class (' . __CLASS__ . '): [ invalid argument for method: ' . __FUNCTION__ . ' ] on line ' . __LINE__);

                $this->_data['app_version'] = $value;
                $this->_modified[] = 'app_version';

                return $this;
        }
         
        */

        
        
        

        /**
         * 
         * @return string
         * 
         */
        public function getCreatedBy()
        {
                return $this->_data['created_by'];
        }



        /*
         *
         * [ setCreatedBy ] 
         *_____________________________________________________
         * 
         *


        public function setCreatedBy($value = NULL)
        {


                if ($value===NULL) $this->e->triggerException('Error in class (' . __CLASS__ . '): [ invalid argument for method: ' . __FUNCTION__ . ' ] on line ' . __LINE__);

                $this->_data['created_by'] = $value;
                $this->_modified[] = 'created_by';

                return $this;
        }
         */
        
        


        /**
         * 
         * @return string
         * 
         */
        public function getDeveloper()
        {
                return $this->_data['developer'];
        }



        /*
         *
         * [ setDeveloper ] 
         *_____________________________________________________
         *
         *


        public function setDeveloper($value = NULL)
        {


                if ($value===NULL) $this->e->triggerException('Error in class (' . __CLASS__ . '): [ invalid argument for method: ' . __FUNCTION__ . ' ] on line ' . __LINE__);

                $this->_data['developer'] = $value;
                $this->_modified[] = 'developer';

                return $this;
        }

         
         */



        /**
         * 
         * @return string
         * 
         */
        public function getClient()
        {
                return $this->_data['client'];
        }



        /*
         *
         * [ setClient ] 
         *_____________________________________________________
         *
         *
         *

        public function setClient($value = NULL)
        {


                if ($value===NULL) $this->e->triggerException('Error in class (' . __CLASS__ . '): [ invalid argument for method: ' . __FUNCTION__ . ' ] on line ' . __LINE__);

                $this->_data['client'] = $value;
                $this->_modified[] = 'client';

                return $this;
        }
         
         
         */






        /**
         * 
         * @param boolean $_format
         * @return string|int
         * 
         */
        public function getMaxCatalogSize($_format = TRUE)
        {
                return ($_format) ? number_format($this->_data['max_catalog_population']) : $this->_data['max_catalog_population'];
        }



        /**
         * 
         * @param int $value
         * @return \Cataleya\System
         * 
         */
        public function setMaxCatalogSize($value = NULL)
        {
                $value = filter_var($value, FILTER_VALIDATE_INT, FILTER_NULL_ON_FAILURE);

                if ($value===NULL) $this->e->triggerException('Error in class (' . __CLASS__ . '): [ invalid argument for method: ' . __FUNCTION__ . ' ] on line ' . __LINE__);

                $this->_data['max_catalog_population'] = $value;
                $this->_modified[] = 'max_catalog_population';

                return $this;
        }



        /**
         * 
         * @param boolean $_format
         * @return int|string
         * 
         */
        public function getMaxAdminAccounts($_format = TRUE)
        {
                return ($_format) ? number_format($this->_data['max_admin_accounts']) : $this->_data['max_admin_accounts'];
        }



        /**
         * 
         * @param int $value
         * @return \Cataleya\System
         * 
         */
        public function setMaxAdminAccounts($value = NULL)
        {
                $value = filter_var($value, FILTER_VALIDATE_INT, FILTER_NULL_ON_FAILURE);

                if ($value===NULL) $this->e->triggerException('Error in class (' . __CLASS__ . '): [ invalid argument for method: ' . __FUNCTION__ . ' ] on line ' . __LINE__);

                $this->_data['max_admin_accounts'] = $value;
                $this->_modified[] = 'max_admin_accounts';

                return $this;
        }



        /**
         * 
         * @param boolean $_format
         * @return string|int
         * 
         */
        public function getMaxCustomerAccounts($_format = TRUE)
        {
                return ($_format) ? number_format($this->_data['max_customer_accounts']) : $this->_data['max_customer_accounts'];
        }



        /**
         * 
         * @param int $value
         * @return \Cataleya\System
         * 
         */
        public function setMaxCustomerAccounts($value = NULL)
        {
                $value = filter_var($value, FILTER_VALIDATE_INT, FILTER_NULL_ON_FAILURE);

                if ($value===NULL) $this->e->triggerException('Error in class (' . __CLASS__ . '): [ invalid argument for method: ' . __FUNCTION__ . ' ] on line ' . __LINE__);

                $this->_data['max_customer_accounts'] = $value;
                $this->_modified[] = 'max_customer_accounts';

                return $this;
        }



        /**
         * 
         * @return string
         * 
         */
        public function getLastModified()
        {
                return $this->_data['last_modified'];
        }





        /**
         * 
         * @return boolean
         * 
         */
        public function isActive()
        {
                return ((int)$this->_data['active'] === 1);
        }



        /**
         * 
         * @return \Cataleya\System
         * 
         */
        public function activate()
        {

                $this->_data['active'] = TRUE;
                $this->_modified[] = 'active';
                
                $this->_data['last_activated'] = date('Y-m-d H:i:s');
                $this->_modified[] = 'last_activated';

                \Cataleya\System\Event::notify('system/on', $this, array('scope'=>'system/on'));
                
                return $this;
        }
        
        
        
        
        
        
        
        /**
         * 
         * @return \Cataleya\System
         * 
         */
        public function shutdown()
        {

                $this->_data['active'] = FALSE;
                $this->_modified[] = 'active';
                

                $this->_data['last_shutdown'] = date('Y-m-d H:i:s');
                $this->_modified[] = 'last_shutdown';


                \Cataleya\System\Event::notify('system/off', $this, array('scope'=>'system/off'));
                
                return $this;
        }
        
        
        



        /**
         * 
         * @return string
         * 
         */
        public function getLastShutdown()
        {
                return $this->_data['last_shutdown'];
        }




        /**
         * 
         * @return string
         * 
         */
        public function getLastActivated()
        {
                return $this->_data['last_activated'];
        }








        
        /**
         * 
         * @param int $_increment
         * @return \Cataleya\System
         * 
         */
        public function updateProductCount($_increment=0)
        {
                if ($_increment === 0) { return $this; }
                
                $this->_data['product_count'] += $_increment;
                $this->_modified[] = 'product_count';

                
                return $this;
        }
        
        
        
        
        /**
         * 
         * @return int
         * 
         */
        public function getProductCount()
        {
                return $this->_data['product_count'];
        }
 
        

        
        /**
         * 
         * @param int $_increment
         * @return \Cataleya\System
         * 
         */
        public function updateCustomerCount($_increment=0)
        {
                if ($_increment === 0) { return $this; }
                
                $this->_data['customer_count'] += $_increment;
                $this->_modified[] = 'customer_count';

                
                return $this;
        }
        
        
        

        /**
         * 
         * @return int
         * 
         */
        public function getCustomerCount()
        {
                return $this->_data['customer_count'];
        }
        
        

        
        /**
         * 
         * @param int $_increment
         * @return \Cataleya\System
         * 
         */
        public function updateAdminUserCount($_increment=0)
        {
                if ($_increment === 0) { return $this; }
                
                $this->_data['admin_count'] += $_increment;
                $this->_modified[] = 'admin_count';

                
                return $this;
        }
        

        /**
         * 
         * @return int
         * 
         */
        public function getAdminUserCount()
        {
                return $this->_data['admin_count'];
        }
        
        
        
        
        
        
	/*
	 *
	 *  [ setDashboardLanguageCode ]
	 * ________________________________________________________________
	 * 
	 * 
	 *
	 *
	 *
	 */
	 

	public function setDashboardLanguageCode ($language_code = 'EN')
	{
            
                $language_code = filter_var($language_code, FILTER_VALIDATE_REGEXP, array('options'=>array('regexp' => '/^([A-Z]{2})$/')));
                
                if ($language_code === FALSE) $this->e->triggerException('Error in class (' . __CLASS__ . '): [ invalid argument for method: ' . __FUNCTION__ . ' ] on line ' . __LINE__);
                    
                    
		if (!in_array('dashboard_language_code', $this->_modified)) $this->_modified[] = 'dashboard_language_code';
                
		$this->_data['dashboard_language_code'] = $language_code;

 
		
		return $this;              
                
		
	}





	/*
	 *
	 *  [ getShopFrontLanguageCode ]
	 * ________________________________________________________________
	 * 
	 * Returns DashboardLanguageCode object
	 *
	 *
	 *
	 */
	 

	public function getShopFrontLanguageCode ()
	{

		
		return $this->_data['shopfront_language_code'];
		
	}

        
        

        
        
        
	/*
	 *
	 *  [ setShopFrontLanguageCode ]
	 * ________________________________________________________________
	 * 
	 * 
	 *
	 *
	 *
	 */
	 

	public function setDefaultShopFrontLanguageCode ($language_code = 'EN')
	{
            
                $language_code = filter_var($language_code, FILTER_VALIDATE_REGEXP, array('options'=>array('regexp' => '/^([A-Z]{2})$/')));
                
                if ($language_code === FALSE) $this->e->triggerException('Error in class (' . __CLASS__ . '): [ invalid argument for method: ' . __FUNCTION__ . ' ] on line ' . __LINE__);
                    
                    
		if (!in_array('shopfront_language_code', $this->_modified)) $this->_modified[] = 'shopfront_language_code';
                
		$this->_data['shopfront_language_code'] = $language_code;

 
		
		return $this;              
                
		
	}





	/*
	 *
	 *  [ getDashboardLanguageCode ]
	 * ________________________________________________________________
	 * 
	 * Returns DashboardLanguageCode object
	 *
	 *
	 *
	 */
	 

	public function getDashboardLanguageCode ()
	{

		
		return $this->_data['dashboard_language_code'];
		
	}


        
        

        
        
        
        
        
        
        


	/*
	 *
	 *  [ setDashboardTheme ]
	 * ________________________________________________________________
	 * 
	 * 
	 *
	 *
	 *
	 */
	 

	public function setDashboardTheme (\Cataleya\Front\Dashboard\Theme $_Theme)
	{
		if (!in_array('dashboard_theme_id', $this->_modified)) $this->_modified[] = 'dashboard_theme_id';
                
		$this->_data['dashboard_theme_id'] = $_Theme->getID();

 
		
		return $this;              
                
		
	}





	/*
	 *
	 *  [ resetDashboardTheme ]
	 * ________________________________________________________________
	 * 
	 * 
	 *
	 *
	 *
	 */
	 

	public function resetDashboardTheme ()
	{
		if (!in_array('dashboard_theme_id', $this->_modified)) $this->_modified[] = 'dashboard_theme_id';
                
		$this->_data['dashboard_theme_id'] = 'default';

 
		
		return $this;  
	}





	/*
	 *
	 *  [ getDashboardTheme ]
	 * ________________________________________________________________
	 * 
	 * Returns DashboardTheme object
	 *
	 *
	 *
	 */
	 

	public function getDashboardTheme ()
	{

		
		return \Cataleya\Front\Dashboard\Theme::load($this->_data['dashboard_theme_id']);
		
	}




        /*
         *
         * [ getDashboardThemeId ] 
         *_____________________________________________________
         *
         *
         */

        public function getDashboardThemeID()
        {
                if (empty($this->_data['dashboard_theme_id'])) $this->resetDashboardTheme ();
                return $this->_data['dashboard_theme_id'];
        }

        
        
        
        
        
        
        
        
        
        
        

        


	/*
	 *
	 *  [ setDefaultShopFrontTheme ]
	 * ________________________________________________________________
	 * 
	 * 
	 *
	 *
	 *
	 */
	 

	public function setDefaultShopFrontTheme (\Cataleya\Front\Shop\Theme $_Theme)
	{
		if (!in_array('shopfront_theme_id', $this->_modified)) $this->_modified[] = 'shopfront_theme_id';
                
		$this->_data['shopfront_theme_id'] = $_Theme->getID();

 
		
		return $this;              
                
		
	}





	/*
	 *
	 *  [ resetDefaultShopFrontTheme ]
	 * ________________________________________________________________
	 * 
	 * 
	 *
	 *
	 *
	 */
	 

	public function resetDefaultShopFrontTheme ()
	{
		if (!in_array('shopfront_theme_id', $this->_modified)) $this->_modified[] = 'shopfront_theme_id';
                
		$this->_data['shopfront_theme_id'] = 'default';

 
		
		return $this;  
	}





	/*
	 *
	 *  [ getDefaultShopFrontTheme ]
	 * ________________________________________________________________
	 * 
	 * Returns DefaultShopFrontTheme object
	 *
	 *
	 *
	 */
	 

	public function getDefaultShopFrontTheme ()
	{

		
		return \Cataleya\Front\Shop\Theme::load($this->_data['shopfront_theme_id']);
		
	}




        /*
         *
         * [ getDefaultShopFrontThemeId ] 
         *_____________________________________________________
         *
         *
         */

        public function getDefaultShopFrontThemeID()
        {
                if (empty($this->_data['shopfront_theme_id'])) $this->resetDefaultShopFrontTheme ();
                return $this->_data['shopfront_theme_id'];
        }

        
        
        
        
        
        
        
        
        
        /* ================= CATALOG PREFERENCES ==================== */
        
        
        
        
        /**
         *
         * @param void
         * @return boolean
         *
         */
        public function upcEnabled()
        {
                return ((int)$this->_data['allow_upc'] === 1);
        }



        /**
         *
         * @param void
         * @return \Cataleya\System $this
         *
         */
        public function enableUPC()
        {

                $this->_data['allow_upc'] = TRUE;
                $this->_modified[] = 'allow_upc';

                return $this;
        }
        
        
        
        

        /**
         *
         * @param void
         * @return \Cataleya\System $this
         *
         */
        public function disableUPC()
        {

                $this->_data['allow_upc'] = FALSE;
                $this->_modified[] = 'allow_upc';

                return $this;
        }
        
        
        
        
        
        

        
        /**
         *
         * @param void
         * @return boolean
         *
         */
        public function eanEnabled()
        {
                return ((int)$this->_data['allow_ean'] === 1);
        }



        /**
         *
         * @param void
         * @return \Cataleya\System $this
         *
         */
        public function enableEAN()
        {

                $this->_data['allow_ean'] = TRUE;
                $this->_modified[] = 'allow_ean';

                return $this;
        }
        
        
        
        

        /**
         *
         * @param void
         * @return \Cataleya\System $this
         *
         */
        public function disableEAN()
        {

                $this->_data['allow_ean'] = FALSE;
                $this->_modified[] = 'allow_ean';

                return $this;
        }
        
        
        
        
        
        

        
        /**
         *
         * @param void
         * @return boolean
         *
         */
        public function isbnEnabled()
        {
                return ((int)$this->_data['allow_isbn'] === 1);
        }



        /**
         *
         * @param void
         * @return \Cataleya\System $this
         *
         */
        public function enableISBN()
        {

                $this->_data['allow_isbn'] = TRUE;
                $this->_modified[] = 'allow_isbn';

                return $this;
        }
        
        
        
        

        /**
         *
         * @param void
         * @return \Cataleya\System $this
         *
         */
        public function disableISBN()
        {

                $this->_data['allow_isbn'] = FALSE;
                $this->_modified[] = 'allow_isbn';

                return $this;
        }
        
        
        
        
        

        
        /**
         *
         * @param void
         * @return boolean
         *
         */
        public function skuEnabled()
        {
                return ((int)$this->_data['allow_sku'] === 1);
        }



        /**
         *
         * @param void
         * @return \Cataleya\System $this
         *
         */
        public function enableSKU()
        {

                $this->_data['allow_sku'] = TRUE;
                $this->_modified[] = 'allow_sku';

                return $this;
        }
        
        
        
        

        /**
         *
         * @param void
         * @return \Cataleya\System $this
         *
         */
        public function disableSKU()
        {

                $this->_data['allow_sku'] = FALSE;
                $this->_modified[] = 'allow_sku';

                return $this;
        }
        
        










        /**
         *
         * @param void
         * @return \Cataleya\System $this
         *
         */
        public function enableGeneratedSKU()
        {

                $this->_data['auto_generate_skus'] = true;
                $this->_modified[] = 'auto_generate_skus';

                return $this;
        }
        
        
        
        

        /**
         *
         * @param void
         * @return \Cataleya\System $this
         *
         */
        public function disableGeneratedSKU()
        {

                $this->_data['auto_generate_skus'] = false;
                $this->_modified[] = 'auto_generate_skus';

                return $this;
        }
        





        /**
         *
         * @param void
         * @return \Cataleya\System $this
         *
         */
        public function generatedSKUEnabled()
        {
            return ((int)$this->_data['auto_generate_skus'] === 1);
        }
        
        




        /**
         * getSKUFormat
         *
         * @return string
         */
        public function getSKUFormat() 
        {
            return $this->_data['sku_autogen_format'];

        }


        /**
         * setSKUFormat
         *
         * @param string $_format
         * @return \Cataleya\System 
         */
        public function setSKUFormat ($_format = '') 
        {
            static $_accepted = [
                'PT' => 'Product Type', 
                'SN' => 'Serial Number', 
                'ID' => 'Product ID', 
                'A' => 'Attributes', 
                'M' => 'Manufacturer', 
                'S' => 'Supplier'
            ];

            $_format = \Cataleya\Helper\Validator::param($_format, 'match:/^[a-zA-Z0-9]{1,10)\}(_[a-zA-Z0-9]{1,10}){0,10}$/');
            if ($_format === false) throw new Error ('SKU format should only have letters, numbers and underscores | dashes!');

            $_format = strtoupper($_format);
            $_partials = explode($_format, ['-', '_']);
            $_diff = array_diff($_partials, $_accepted);

            if (!empty($_diff)) throw new Error ('SKU partials can only include: '.implode(', ',array_keys($_accepted)));




            $this->_data['sku_autogen_format'] = str_replace('_', '-', $_format);
            $this->_modified[] = 'sku_autogen_format';

            return $this;

                


        }
        
        
        
        /**
         * getSKUSerialNumber
         *
         * @return string
         */
        public function getSKUSerialNumber($_reset = null) 
        {
            $this->_data['sku_serial_num'] = is_int($_reset) ? $_reset+1 : (int)$this->_data['sku_serial_num'] + 1;
            $this->_modified[] ='sku_serial_num';

            return str_pad($this->_data['sku_serial_num'], 4, 0, STR_PAD_LEFT);

        }

        
        
        
        
        /**
         * resetSKUSerialNumber
         *
         * @return string
         */
        public function resetSKUSerialNumber($_init = 0) 
        {
            if (!is_int($_init)) throw new Error ('Argument 1 should be an integer.');

            $this->_data['sku_serial_num'] = $_init;
            $this->_modified[] ='sku_serial_num';

            return $this;

        }

        
        
             





}
	
	
	
	
