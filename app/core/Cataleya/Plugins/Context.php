<?php


namespace Cataleya\Plugins;

/**
 * This
 *
 * @author Gboyega Dada <gboyega@fpplabs.com>
 */
class Context {
    
    private $_handle;


    public function __construct($_app_handle) {
        $this->_handle = $_app_handle;
    }
    
    
    
    public function getAppHandle() 
    {
        return $this->_handle;
    }

    





    public function getAppIcons () {

        return \Cataleya\Plugins\Package::getAppIcons($this->getAppHandle());

    }



    /**
     * getAppController
     *
     * @param string $_plugin_handle
     * @return string
     */
    public static function getAppController () 
    {
        return \Cataleya\Plugins\Package::getAppController($this->getAppHandle());
    }


    /**
     * getAppHooks List of events our plugin is listening to.
     *
     * @param string $_plugin_handle
     * @return string
     */
    public static function getAppHooks () 
    {

        return \Cataleya\Plugins\Package::getAppHooks($this->getAppHandle());

    }



    /**
     * getAppEvents List of plugin events that others can observe / listen to.
     *
     * @param string $_plugin_handle
     * @return string
     */
    public static function getAppEvents () 
    {

        return \Cataleya\Plugins\Package::getAppEvents($this->getAppHandle());

    }






    /**
     * getAppName
     *
     * @param string $_plugin_handle
     * @return string
     */
    public static function getAppName () 
    {

        return \Cataleya\Plugins\Package::getAppName($this->getAppHandle());

    }
 


    /**
     * getAppDescription
     *
     * @param string $_plugin_handle
     * @return string
     */
    public static function getAppDescription () 
    {

        return \Cataleya\Plugins\Package::getAppDescription($this->getAppHandle());

    }



    /**
     * getAppNameSpace
     *
     * @param string $_plugin_handle
     * @return string
     */
    public static function getAppNameSpace () 
    {

        return \Cataleya\Plugins\Package::getAppNameSpace($this->getAppHandle());

    }



    public function getSkin () 
    {
        return \Cataleya\Plugins\Skin::load(self::getHandle());

    }

    
    
}
