<?php


namespace Cataleya\Plugins;
use \Cataleya\Error;
use Cataleya\Helper\DBH;




/**
 *
 * 	@Package Cataleya
 * 	@Class \Cataleya\Plugins\Hook
 *
 * 	(c) 2015 FPP Labs
 *
 * ----------------------------------------------------------------------------
 *
 * This is our gateway to through which plugin apps can listen to system events. 
 * Every event goes through here.
 * When a product is deleted, in \Cataleya\System\Hook, we will have something like:
 *
 *  \Cataleya\Plugins\Hook::notify('product/deleted', [  'product_id' => 1 ]);
 *
 * Syntax: [scope] and [event] keywords are separated by a forward slash like: 'scope/event'
 *
 * ..then it is passed to every plugin that has registered this as a hook. 
 * It works like a typical [Events/Hooks/Observables/Listeners/Mediator] pattern 
 *
 *
 */
class Hook 
{
    
    


    private static $_registry = [];
    protected static $_is_ready = false;

    
    

    /**
     * __construct
     *
     * @return void
     */
    public function __construct() {
       
        
    }
    
    
    

    /**
     * initRegistry
     *
     * @param bool $_reload
     * @return void
     */
    private static function initRegistry ($_reload=false) {
        
        if (!empty(self::$_registry) && $_reload !== true) return;

        static $_select_handle;
        self::$_registry = [];

        if (empty($_select_handle)) {
            $_select_handle = DBH::getInstance()->prepare(
                'SELECT * FROM plugin_hooks'
            );
        }

        if (!$_select_handle->execute()) {
            return;
            // throw new \Cataleya\Error(implode(', ', $_select_handle->errorInfo()), $_select_handle->errorCode()); 
        }


        while ($row = $_select_handle->fetch(\PDO::FETCH_ASSOC)) {
            if (!isset(self::$_registry[$row['scope']])) self::$_registry[$row['scope']] = [];
            self::$_registry[$row['scope']][] = $row['listener'];
        }
    
        self::$_is_ready = true;
       

    }


    
    /**
     * 
     * @param string $_event    IMPORTANT: This is NOT a scope!! It's a single, finite event like
     *                          "product/deleted". "product/*" not allowed!
     * 
     * @param array $_params    Will be supplied this way:
     *                          $_params = [
     *                              // 1. The event that happened..
     *                              'event' => 'store/deleted', 
     *                              // 2. information needed to process data...
     *                              'params' => [
     *                                  'store_id' => 1, 
     *                                  'some_other_item' => [object], 
     *                                  'etc' => 'etc' ...
     *                                  ]
     *                          ]
     *
     *
     * @return boolean
     * 
     */
    public static function notify ($_event, array $_params = []) {
        
        if (!self::isEvent($_event)) return false; 

        self::initRegistry();

        foreach (self::$_registry as $_sc => $_ls) {
            if (self::inScope($_event, $_sc)) {
                foreach ($_ls as $_c) \forward_static_call([$_c, "notify"], $_event, $_params);
            }

        }
        

        
        return;
       
    }
  

    /**
     * add
     *
     * @param string $_listener App Controller class name
     * @param string|array $_scope  An event (ex: 'product/delete') or an array of events.
     * @return void
     */
    public static function add($_listener, $_scope) 
    {
        $_scopes = (is_array($_scope)) ? $_scope : [ $_scope ];

        foreach ($_scopes as $_sc) 
        {
            if (!is_string($_sc))  throw new Error ('Invalid argument (2).');
            else if (!self::isEvent($_sc)) throw new Error ('"' . (string)$_sc . '" is not a valid event scope.');
        }

        self::initRegistry();
            

        static $_insert_handle;

        // Register hooks.
        if (empty($_insert_handle)) $_insert_handle = DBH::getInstance()->prepare( 
                    'INSERT INTO plugin_hooks ' . 
                    '(scope, listener, date_added) ' . 
                    'VALUES (:scope, :listener, NOW()) '
                );


        foreach ($_scopes as $_sc) 
        {
            if (
                    !self::isEvent($_sc) || 
                    !\Cataleya\System\Event::exists($_sc) || 
                    self::hasListener($_sc, $_listener) 
                    ) continue;
            

                /**
                 *
                 * 1. event:        System event that your plugin app wants to listen to.
                 * 2. listener:     Main controller class name (your "MyApp/Main" class).
                 *
                 */
                $_params = [
                    'scope' => $_sc, 
                    'listener' => $_listener
                ];

                DBH::bindParamsArray($_insert_handle, $_params);
                        
                        
                if (!$_insert_handle->execute()) {
                    throw new Error('DB Error: ' . implode(', ', $_insert_handle->errorInfo()), $_insert_handle->errorCode()); 
                }

                if (!isset(self::$_registry[$_sc])) self::$_registry[$_sc] = [];
                self::$_registry[$_sc][] = $_listener;

            
        }




    }
    
    


    /**
     * 
     * @param string $_listener
     * @param $_scope string | array
     * 
     */
    public static function remove ($_listener, $_scope = []) 
    {
        
        $_scopes = (is_array($_scope)) ? $_scope : [ $_scope ];
        self::initRegistry();


        static $_delete_handle;

        // Remove hooks.
        if (empty($_delete_handle)) $_delete_handle = DBH::getInstance()->prepare( 
                            'DELETE FROM plugin_hooks ' . 
                            'WHERE scope = :scope AND listener = :listener '
                            );
        foreach ($_scopes as $_sc) 
        {
            foreach (self::getEvents($_sc) as $_e=>$_ls) 
            {

                $k = array_search($_listener, $_ls, true);
                if ($k === false) continue;

                /**
                 *
                 * 1. event:        System event that your plugin app is to listen to.
                 * 2. listener:     Main controller class name (your "MyApp/Main" class).
                 *
                 */
                $_params = [
                    'scope' => $_e, 
                    'listener' => $_listener
                ];

                DBH::bindParamsArray($_delete_handle, $_params);
                        
                        
                if (!$_delete_handle->execute()) {
                    throw new Error('DB Error: ' . implode(', ', $_insert_handle->errorInfo()), $_insert_handle->errorCode()); 
                }

                
                array_slice (self::$_registry[$_e], $k, 1);
                
            }
        }
        
        return true;
        
    }
    




    public static function getEvents($_scope = '*') 
    {

        
        self::initRegistry();
        if ($_scope === '*') return self::$_registry;
        
        if (!self::isEvent($_scope)) throw new Error ("Invalid event scope");



        $_list = [];

        foreach (self::$_registry as $_event=>$_listeners) 
        {
            if (self::inScope($_event, $_scope)) $_list[$_event] = $_listeners;
        }

        return $_list;

    }


    
    /**
     * 
     * @param string $_event
     * @return boolean
     * 
     */
    public static function verifyEvent($_event) {
        return self::isEvent($_event);
    }




    /**
     * 
     * @param string $_str
     * @return boolean
     * 
     */
    public static function isEvent($_str) {

        return \Cataleya\System\Event::isEvent($_str);

    }
    



    /**
     * hasListeners
     *
     * @param string $_scope
     * @return boolean
     */
    public static function hasListeners ($_scope) 
    {
        self::initRegistry(); 
        return (self::isEvent($_scope) && array_key_exists($_scope, self::$_registry));
    }


    /**
     * hasListener
     *
     * @param string $_scope    ex: "product/delete"
     * @param string $_listener Plugin Controller class name..
     * @return boolean
     */
    public static function hasListener ($_scope, $_listener) 
    {
            return (self::hasListeners($_scope) && in_array($_listener, self::$_registry[$_scope]));
    }




    /**
     * inScope  Returns true if an event (ex: 'catalog/product/delete') is part 
     *          of a scope (ex: 'catalog/*')
     *
     * @param string $_event
     * @param string $_scope
     * @return boolean
     */
    private static function inScope($_event, $_scope = '*') 
    {

        return \Cataleya\System\Event::inScope($_event, $_scope);



    }




 
}


