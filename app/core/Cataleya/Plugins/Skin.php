<?php


namespace Cataleya\Plugins;




/**
 * Skin
 *
 * @package Cataleya
 * @author Gboyega Dada <gboyega@fpplabs.com>
 * @version $Id$
 */
class Skin 
{
	
    private static $_path = NULL;

    private $_assets_path = '';
    private $_twig_loader;
    private $_twig_instance;

    private $_templates = [];


    private function __construct ($_app_handle) 
    {

        $this->_twig_loader = new \Twig_Loader_Filesystem(self::$_path.'/'.$_app_handle.'/layouts');
        $this->_twig_instance = new \Twig_Environment($this->_twig_loader, array('cache'=>FALSE, 'autoescape'=>FALSE));
        $this->_twig_instance->addExtension(new \Twig_Extensions_Extension_Text());

        $this->_assets_path = self::$_path.'/'.$_app_handle.'/assets';

    }


        
	
	/**
	 * load
	 *
	 * @param string $_app_handle
	 * @return void
	 */
	public static function load ($_app_handle = NULL) 
	{

        if (!is_string($_app_handle) || !\Cataleya\Plugins\Package::exists($_app_handle)) {
            return null;
        }

        if (empty(self::$_path)) self::$_path = PATH_SKIN.'/Apps';


		
		return new static ($_app_handle);
		
	}	




    /**
     * getAssetsPath
     *
     * @return string
     */
    public function getAssetsPath() 
    {
        return $this->_assets_path;
    }


    /**
     * render
     *
     * @param string $_template
     * @param array|stdClass $_vars
     * @return string
     */
    public function render ($_template, $_vars) 
    {
        if (!isset($this->_templates[$_template])) {
            try {
                $this->_templates[$_template] = $this->_twig_instance->loadTemplate($_template);
            } catch (\Exception $e) {
                throw new Error ('App template missing!', $e);
            }
        } 

        return $this->_templates[$_template]->render($_vars);

    }

        
        
	
	
}






