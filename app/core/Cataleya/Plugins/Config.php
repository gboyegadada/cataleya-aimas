<?php


namespace Cataleya\Plugins;
use \Cataleya\Helper\ErrorHandler;

/**
 * 
 * Once again, I come in peace, I mean you no harm, and you're all 
 * going to die. - Galaxar.
 *
 * @author Gboyega Dada <gboyega@fpplabs.com>
 */
class Config {
    


        protected 
                
                $_handle, 
                $_instance; 


        /**
         * 
         * WARNING: Do NOT use "new Config (id)". It is not allowed. C'est finit.
         * 
         * @param string $_handle
         * @return void
         * 
         */
        protected function __construct($_handle, $_new = false, array $_params = []) {

            $this->_instance = ($_new) 
                ? \Cataleya\System\Config::init('plugins.'.$_handle, $_params) 
                : \Cataleya\System\Config::load('plugins.'.$_handle) ;
           
        }

        
        
        
        /**
         * 
         * @param array $_config
         * @param type $_handle
         * @return boolean
         * 
         */
        public static function init ($_handle, array $_params = []) 
        {
            
            static $_obj = __CLASS__;

            return new $_obj ($_handle, true, $_params);



        }
        
        


        /**
         * 
         * 
         * @param string $_handle Ex: "main.db"
         *                     This works like an autoloader DOTS === DIRECTORY_SEPARATOR 
         *                     So "main.db" === CONFIG_PATH / [main] / [db] . json
         * 
         * @return \Em\Sys\Config
         * 
         */
        public static function load($_handle = null) 
        {
            static $_obj = __CLASS__;

            $_this = new $_obj ($_handle);
            return (!empty($_this->_instance)) ? $_this : null;

        }
        
        
        

        /**
         * 
         * @return string Ex: "main.db"
         * 
         */
        public function getHandle() {
            return $this->_handle;
        }
        

        
        
        /**
         * 
         * @param string $_key Read config variable. Ex: "username"
         * @return mixed
         * 
         */
        public function getParam($_key) {
            
            return $this->_instance->getParam($_key);
            
        }
        
        
        
        /**
         * 
         * @param string $_key
         * @param mixed $_value
         * 
         */
        public function setParam($_key, $_value) {
            
            $this->_instance->setParam($_key, $_value);

            return $this;
            
        }
        
        
        
        /**
         * 
         * @param boolean $_echo
         * @return array|void
         * 
         */
        public function dump($_echo = false) {
            return $this->_intance->dump($_echo); 
        }
        
        
        public function drop () {

            if (!empty($this->_instance)) {
                $this->_instance->drop();
                $this->_instance = null;
            }

        }
        


        /**
         * 
         * Save config variables to file.
         * 
         * @return boolean
         * 
         */
        public function save() 
        {
               
            $this->_instance->save();
            return true;


        }
        
        
        
    
    
}
