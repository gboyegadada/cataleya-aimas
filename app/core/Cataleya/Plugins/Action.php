<?php


namespace Cataleya\Plugins;




/**
 *
 * 	@Package Cataleya
 * 	@Class \Cataleya\Plugins\Action
 *
 * 	(c) 2015 FPP Labs
 *
 * ----------------------------------------------------------------------------
 *
 * This is our gateway to plugin methods/actions. Every plugin action goes through here.
 * At various entry points accross Cataleya, we will have something like:
 *
 *  \Cataleya\Plugins\Action::trigger('payment.get-form-controls');
 *
 * Syntax: [scope] and [action] keywords are separated by dots like: 'scope.action'
 * You can also use a wild card like: '*.action' BUT:  
 *
 * 1.   You're not allowed to do: '*.sub-scope.action'. 
 * 
 * 2.   You cannot begin a scope def with '*' unless it is used alone 
 *      by itself like this: '*.action'.
 *
 * 3.   You cannot use an asterix anywhere inside of a scope def except
 *      at the end like this: 'scope.subscope.*' (you can add '.action' after this)
 *
 * ..then it is passed to every plugin that has registered this as one of it's 
 * actions. It works like a typical [Events/Hooks/Observables/Listeners/Mediator] pattern 
 * but I'd like think of it as 'triggering' actions. In the other patterns you are 
 * merely an observer. In this model you (the plugins) are the ones changing something. 
 * You are not an observer; you are a tool, a function, a sub-routine, a command!
 *
 * This was partially inspired by WordPress filters and it works like filters too if 
 * you use like this:
 *
 * $_menu = [ 'Home', 'Contact' ]; // Menu before action...
 * $_menu = \Cataleya\Plugins\Action::trigger('frontend.get-main-menu', [ 'data' => $_menu ]);
 * $_menu === [ 'Home', 'Contact', 'Instagram Feed', 'Gellery' ]; // Menu after action...
 *
 * We have used the 'get-main-menu' plugin action to add more items to the menu a la WordPress 
 * style.
 *
 * \Cataleya\Plugins\Action::trigger(
 *
 *          // 1. scope + action/method
 *          'frontend.get-main-menu', 
 *
 *          // 2. Arguments: -data- is what is to be manipulated. -params- has info we need in other  
 *                           manipulate -data- (like customer_id, cart_id e.t.c) and -params- cannot 
 *                           be altered.
 *          [ 
 *              'data' => $_menu, 
 *              'params' => [] 
 *          ], 
 *
 *          // 3. Filter by app_handle: We can use this array of app handles to trigger actions in 
 *                                      specific apps only. If empty, all apps covered by -scope- in #1 
 *                                      will be triggered.
 *          [ 'app_handle_1', 'app_handle_2' ... ]
 *
 *       );
 *
 *
 */
class Action
{
    
    


    

    /**
     * __construct
     *
     * @return void
     */
    public function __construct() {
       
        
    }
    
    
    
    
    
    /**
     * 
     * @param string $_action
     * @return boolean
     * 
     */
    public static function verifyAction($_action) {
        return self::isAction($_action);
    }


    /**
     * 
     * @param string $_action
     * @return boolean
     * 
     */
    public static function isAction($_action) {
        $_action = is_string($_action) ? trim($_action) : null;

        return (
            empty($_action) || 
            preg_match('/[a-zA-Z\*\-_]+(\.[a-zA-Z\*\-_]+)+/', $_action) == false  
        ) ? false : true;

    }


    
    
    /**
     * 
     * @param string $_action
     * @param array $_args      Must be supplied this way:
     *                          $_args = [
     *                              // 1. information needed to process data...
     *                              'params' => [
     *                                  'store_id' => 1, 
     *                                  'some_other_item' => [object], 
     *                                  'etc' => 'etc' ...
     *                                  ],
     *
     *                              // 2. Data to be processed and passed on to next plugin in the chain.. 
     *                              'data' => [ array | string | int | mixed ]
     *                          ]
     *
     * @param array $_filter    We can use this to target 1 or more specified 
     *                          plugins. Eg. If I want to trigger an action from the "Social App" plugin   
     *                          I can just put it's handle "social-app" in the filter array and ALL OTHER plugins 
     *                          will be ignored.
     *
     * @return boolean
     * 
     */
    public static function trigger($_action, array $_args = [], array $_filter = []) {
        
        if (!self::isAction($_action)) return false; 

        if (!isset($_args['params'])) $_args['params'] = [];
        if (!isset($_args['data'])) $_args['data'] = []; 

        $_partials = explode('.', $_action);
        
        // Extract [action]...
        $_method = trim(array_pop($_partials));
        if (empty($_method) || preg_match('/^[a-zA-Z\-_]+$/', $_method) == false) return false;

        // Extract [scope]...
        $_scope = implode('.', $_partials);
        


        $_app_list = \Cataleya\Plugins\Package::getAppList($_scope);
        $_first_run = true;
        $_result;
            
        // Trigger registered plugin actions...
        foreach ($_app_list as $_handle) { 
            
            if (!empty($_filter) && !in_array($_handle, $_filter)) continue;
            
            $c = \Cataleya\Plugins\Package::getAppController($_handle);
            \Cataleya\Plugins\Package::rememberApp($_handle);
            $_result = forward_static_call(
                [$c, "run"], $_method, 
                $_args['params'], ($_first_run ? $_args['data'] : $_result)
            );

            /*
            $_result = forward_static_call([$c, "run"], $_method, ($_first_run ? $_data : $_return));

            if (!is_array($_result)) { $_result = [ $_result ]; }
            $_return = $_return + $_result;
            */

            if ($_first_run) {  $_first_run = false; }

        }
        
        return $_result;
       
    }
    
    

        

 
}


