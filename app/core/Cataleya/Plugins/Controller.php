<?php


namespace Cataleya\Plugins;





/**
 * Controller             This is an interface (yes, we noticed) that 
 *                        must be implemented by every Plugin. A plugin 
 *                        behaves like a controller. It's got a bunch of 
 *                        'actions' it responds to when triggered.
 *
 * @package Cataleya
 * @author Gboyega Dada <gboyega@fpplabs.com>
 * @version $Id$
 */
interface Controller {


    /**
     * init
     *
     * @return void
     */
    public static function init ();


     /**
     * run    This is like the regular 'exec' method that a controller has 
     *        basically functioning as a gateway.
     *
     * @param string $_action
     * @param array $_params
     * @param array $_data
     * @return mixed
     */
    public static function run ($_action, array $_params, $_data);



    /**
     * notify   This works like '::run'. It will be called whenever an event 
     *           your app is listening to is triggered. You have to specify these 
     *           using scopes in your app.json file just like your plugin scopes.
     *
     * @param string $_event
     * @param array $_params
     * @return void
     */
    public static function notify ($_event, array $_params);



   /**
     * getHandle    Return plugin handle (alphanumeric + dashes && underscores). 
     *
     *              The plugin handle must be unique and must be registered 
     *              at the App Store. After registration, you have to hard-code it   
     *              into your plugin's Controller class.
     *
     * @return string
     */
    public static function getHandle();





    /**
     * getActions            Get list of |actions| that Plugin Controller can carry out.
     *
     * @return array         An array of |actions| (strings), that our plugin 
     *                       will respond to. Eg. 'payments.start-transaction' and so on.
     *
     */
    public static function getActions ();



    /**
     * install
     *
     * @return void
     */
    public static function install ();


    /**
     * uninstall
     *
     * @return void
     */
    public static function uninstall ();





}
