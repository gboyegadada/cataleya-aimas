<?php

namespace Cataleya\Plugins;


use \Cataleya\Helper\DBH;
use \Cataleya\Error;



/**
 * Package
 *
 * @package Cataleya
 * @author Gboyega Dada <gboyega@fpplabs.com>
 * @version $Id$
 */
class Package {


    private static $_registry = [];
    private static $_modified = [];

    private static $_installer_log = [];
    private static $_installer_logs_path;
    private static $_app_icons_path;
    private static $_app_templates_path;
    
    private static $_current_app = null;

        /**
     * install
     *
     * @param string $_plugin_handle
     * @return void
     */
    public static function install ($_plugin_handle) 
    {

        $_package_archive = PLUGIN_PACKAGES_PATH . '/' . $_plugin_handle . '/package.zip';
        $_package_folder = PLUGIN_PACKAGES_PATH . '/' . $_plugin_handle;

        if (file_exists($_package_folder)) {
            // 1.1 Package already on disc.
            $_plugin_info = json_decode(file_get_contents($_package_folder.DIRECTORY_SEPARATOR.'app.json'));
            self::$_installer_log['is_bundle'] = true;
        } else {

            self::$_installer_log['is_bundle'] = false;
            
            // 1.2 Download from repo.
            $_plugin_info = file_get_contents(APP_STORE_ENDPOINT.'/i/'.$_plugin_handle);
            if (empty($_plugin_info)) { throw new Error ("App info missing (app.json)"); }
            $_plugin_info = json_decode($_plugin_info);

            $fh = fopen (APP_STORE_ENDPOINT . '/d/' . $_plugin_handle);
            if (empty($fh)) { return null; }
            $fhs = fopen ($_package_archive);

            while ($buffer = fread($fh, 1024)) {
                fwrite($fhs, $buffer); 
            }

            fclose($fhs); fclose($fh);



            // 2. Expand.
            mkdir ($_package_folder, 0755);
            $_Package = new ZipArchive ($_package_archive);
            $_Package->extractTo($_package_folder); 
            
        }
        
        

        if (empty($_plugin_info)) { throw new Error ("app.json could not be decoded. Hint: check for JSON syntax errors."); }
        if (
            !isset($_plugin_info->{'scope'}) || 
            empty($_plugin_info->{'scope'})
        ) { throw new Error ("Your App has no scope!"); }

        // Inspect { hooks }
        if (!isset($_plugin_info->{'hooks'})) $_plugin_info->{'hooks'} = []; // e.g [ "product/delete" ]
        else if (is_string($_plugin_info->{'hooks'})) { 
            $_plugin_info->{'hooks'} = preg_split('/[\s,]+/', $_plugin_info->{'hooks'}); 
        } else if (!is_array($_plugin_info->{'hooks'})) {

            throw new Error ('JSON Error: { hooks } must be an array or even a string with entries separated by spaces or commas.');
        }

        // Inspect { events }
        if (!isset($_plugin_info->{'events'})) $_plugin_info->{'events'} = []; // e.g [ "my-app-handle/unistalled" ]
        else if (is_string($_plugin_info->{'events'})) { 
            $_plugin_info->{'events'} = preg_split('/[\s,]+/', $_plugin_info->{'events'}); 
        } else if (!is_array($_plugin_info->{'events'})) {

            throw new Error ('JSON Error: { events } must be an array or even a string with entries separated by spaces or commas.');
        }

        // Put plugin events in a sandbox (ie: "apps/app-handle/event")...
        $_base_scp = 'apps/' . $_plugin_handle . '/';
        foreach ($_plugin_info->events as $k=>$v) $_plugin_info->events[$k] = $_base_scp.$v;



        self::$_installer_log['plugin_handle'] = $_plugin_handle;
        self::$_installer_log['$PATH']['package_archive'] = $_package_archive;
        self::$_installer_log['$PATH']['package_folder'] = $_package_folder;

        // 3. Copy class files to plugin dir.
        $_code_path = $_package_folder.DIRECTORY_SEPARATOR.'code';
        $_namespace_path = $_code_path.DIRECTORY_SEPARATOR.$_plugin_info->namespace;

        // Find main plugin class..
        $_main_class_path = []; $_main_class_name = null;
        if (file_exists($_namespace_path.'.php')) { 
            $_main_class_path[] = $_namespace_path.'.php';
            $_main_class_name = '\Cataleya\Apps\\'.$_plugin_info->namespace;
        }
        if (file_exists($_namespace_path.DIRECTORY_SEPARATOR.'Main.php')) {
            $_main_class_path[] = $_namespace_path.DIRECTORY_SEPARATOR.'Main.php';
            $_main_class_name = '\Cataleya\Apps\\'.$_plugin_info->namespace.'\Main';
        }

        if (!empty($_main_class_name)) self::$_installer_log['plugin_controller'] = $_main_class_name;


        // Make sure there's only 1 controller class... 
        if (empty($_main_class_path)) {
            self::rollback();
            throw new Error ('Main plugin class MIA in package.'); 
        } else if (count($_main_class_path) > 1) {
            self::rollback();
            throw new Error ('More than 1 controller (main) class found in package. There can only be one *Jet-Li face*'); 
        }

        $_class_files = new \DirectoryIterator($_code_path);
        $_code_black_list = glob(CATALEYA_PLUGINS_PATH."/*");
        $_code_white_list = [$_plugin_info->namespace, $_plugin_info->namespace.'.php']; 
        self::$_installer_log['$PATH']['code_files'] = [];

        foreach ($_class_files as $f) {

            if ($f->isDot()) continue;

            $_fname = $f->getFilename();
            // If name clash: abort
            if (in_array($_fname, $_code_black_list) || !in_array($_fname, $_code_white_list) ) {
                self::$_installer_log['$PATH']['code_files'] = []; // No files written yet...
                self::rollback();
                throw new Error ('App files could not be copied: Name clash -OR- isn\'t in correct namespace!');
            }
            self::$_installer_log['$PATH']['code_files'][] = CATALEYA_PLUGINS_PATH.DIRECTORY_SEPARATOR.$_fname;
        }


        // Copy class files...
        \Cataleya\Helper\File::copy($_code_path.'/*', CATALEYA_PLUGINS_PATH);



        // Copy App Template Files...
        $_new_templates_path = self::getTemplatesPath().'/'.$_plugin_handle;
        if (!file_exists($_new_templates_path)) {
            mkdir($_new_templates_path);
            if (file_exists($_package_folder.'/skin/')) {
                \Cataleya\Helper\File::copy($_package_folder.'/skin/*', $_new_templates_path);
            } 
            
            if (!file_exists($_new_templates_path.'/layouts')) mkdir($_new_templates_path.'/layouts');

            self::$_installer_log['$PATH']['templates_folder'] = $_new_templates_path;
        } else {
            self::rollback();
            throw new Error ('Your app handle @'.$_plugin_handle.' is already in use by another app!');
        }


        // Copy App icons...
        $_new_icons_path = $_new_templates_path.'/assets/images/icons';
        if (!file_exists($_new_templates_path.'/assets')) mkdir($_new_templates_path.'/assets');
        if (!file_exists($_new_templates_path.'/assets/images')) mkdir($_new_templates_path.'/assets/images');

        if (!file_exists($_new_icons_path)) {
            mkdir($_new_icons_path);
            if (file_exists($_package_folder.'/icons/')) {
                \Cataleya\Helper\File::copy($_package_folder.'/icons/*', $_new_icons_path);
            } else {
                \Cataleya\Helper\File::copy(ROOT_PATH.'ui/images/app-icons/default/*', $_new_icons_path);
            }
            // self::$_installer_log['$PATH']['icons_folder'] = $_new_icons_path;
        } else {
            self::rollback();
            throw new Error ('Your app handle @'.$_plugin_handle.' is already in use by another app!');
        }


        if (!class_exists($_main_class_name)) { 
            self::rollback();
            throw new Error ('Main class is not declared. Remember to do "namespace \Cataleya\Apps\\'.$_plugin_info->namespace.';" then "class Main {}".');
        }

        
        $rc = new \ReflectionClass($_main_class_name);
        if (!$rc->implementsInterface("\Cataleya\Plugins\Controller")) {
            self::rollback();
            throw new Error ('Main class found -- "'.$_main_class_name.'" but it must implement "\Cataleya\Plugins\Controller". Gorrit?');
        }


        // 4. sql setup script: If one exists, we will attempt to run it.
        $_sql_setup_script =  $_package_folder.DIRECTORY_SEPARATOR.'sql/install.sql';
        if (file_exists($_sql_setup_script)) {
            try {
                DBH::runScript($_sql_setup_script);
            } catch (Error $e) {
                self::rollback();
            }
            self::$_installer_log['sql_setup_script_executed'] = true;
        }



        // 5. Register plugin.
        $_insert_handle = DBH::getInstance()->prepare( 
            'INSERT INTO plugin_registry ' . 
            '(handle, scope, name, description, vendor, version, type, namespace, ' .  
            'controller_class, hooks, events, status, date_added, last_updated) ' . 
            'VALUES (:handle, :scope, :name, :description, :vendor, :version, :type, :namespace, ' . 
            ':controller_class, :hooks, :events, :status, NOW(), NOW()) '
                                        );

        /**
         *
         * 1. handle:       App Handle
         * 2. scope:        App Scope. ex: [ 'payament', 'dashboard' ]
         * 3. hooks:        System events that your plugin app wants to listen to.
         * 4. events:       Observable plugin events ex: [ 'my-app-handle/installed' ]
         * 5. name:         Display name
         * 6. description:  Need a description?
         * 7. vendor:       ex: "Apple inc"
         * 8. version:      ex: 1.0.1
         * 9. type:         "local" ~ our app is running on the same server as Cataleya
         *                  "web" ~ our app is hosted on a remote server (like Shopify apps)
         * 10. namespace:   Base namespace for app code ex: "MyApp" (which becomes \Cataleya\Apps\MyApp)
         * 11. controller_class: Main controller class name (your "MyApp/Main" class.
         * 12. status:      "1" ~ enabled, "0" ~ disabled
         *
         */
        $_params = [
            'handle' => $_plugin_handle, 
            'scope' => (is_array($_plugin_info->scope) ? implode(' ', $_plugin_info->scope) : $_plugin_info->scope), 
            'hooks' => implode(' ', $_plugin_info->hooks), 
            'events' => implode(' ', $_plugin_info->events),  
            'name' => $_plugin_info->name, 
            'description' => $_plugin_info->description, 
            'vendor' => $_plugin_info->vendor, 
            'version' => $_plugin_info->version, 
            'type' => 'local',  
            'namespace' => $_plugin_info->namespace, 
            'controller_class' => $_main_class_name, 
            'status' => 0
        ];

        DBH::bindParamsArray($_insert_handle, $_params);
                
                
        if (!$_insert_handle->execute()) {
            self::rollback();
            throw new Error(implode(', ', $_insert_handle->errorInfo()), $_insert_handle->errorCode()); 
        }





        // 6. Register hooks...
        try {
            \Cataleya\Plugins\Hook::add($_main_class_name, $_plugin_info->hooks);
        } catch (Error $ex) {
            self::rollback();
            throw new Error ("Error registering App hooks.", $ex, 0);
        }



        // 7. Register plugin events...
        try {
            \Cataleya\System\Event::add($_plugin_info->events, $_main_class_name);
        } catch (Error $ex) {
            self::rollback();
            throw new Error ("Error registering App events.", $ex, 0);
        }

        // Reload registry...
        self::initRegistry(true);
        self::rememberApp($_plugin_handle);

        /**
         *
         * Run plugin install method...
         *
         * This is useful for setting up db tables and initializing 
         * Plugin settings.
         *
         */
        try {
            forward_static_call([ $_main_class_name, 'install' ]);
        } catch (\Exception $e) {
            self::rollback();
            throw new Error ('Something went wrong in plugin "install" method: ' . $e->getMessage(), 0, $e);
            
        }
        

        // Save and clear log.
        file_put_contents(
            self::getLogsPath().DIRECTORY_SEPARATOR.self::$_installer_log['plugin_handle'].'.log', 
            json_encode(self::$_installer_log)
        );
        self::$_installer_log = [];
        

        // Switch Plugin ON...
        self::switchOn($_plugin_handle);

        return true;

    }


    /**
     * uninstall
     *
     * @param string $_plugin_handle
     * @return void
     */
    public static function uninstall ($_plugin_handle) 
    {

        if (!self::exists($_plugin_handle)) throw new Error("Could not remove @".$_plugin_handle.". Plugin not found.");

        // 1. Run plugin "unistall" action..
        
        self::rememberApp($_plugin_handle);
        forward_static_call([self::getAppController($_plugin_handle), 'uninstall']);

        // 2. Cleanup.
        $_log_path = self::getLogsPath().DIRECTORY_SEPARATOR.$_plugin_handle.'.log';
        if (file_exists($_log_path)) {
            self::$_installer_log = json_decode(file_get_contents($_log_path), true);
            self::rollback();
            \Cataleya\Helper\File::remove($_log_path);
        }


        // 3. Reload registry...
        self::initRegistry(true);

    }





    /**
     * rollback  This will attempt a roll-back.
     *
     * @return void
     */
    private static function rollback () {

            // 1. sql unistall script: If one exists, we will attempt to run it.
            if (isset(self::$_installer_log['sql_setup_script_executed'])) {
                $_sql_script = self::$_installer_log['$PATH']['package_folder'].DIRECTORY_SEPARATOR.'sql/uninstall.sql';
                if (file_exists($_sql_script)) DBH::runScript($_sql_script);
            }
            
            if (self::$_installer_log['is_bundle'] === true) unset (self::$_installer_log['$PATH']['package_folder']);
 
            // 2. Remove package (downloaded) files and copied files...
            foreach (self::$_installer_log['$PATH'] as $_path) 
            {
                if (is_array($_path)) {

                foreach ($_path as $_p) { if (file_exists($_p)) \Cataleya\Helper\File::remove($_p); }

                } else if (file_exists($_path)) \Cataleya\Helper\File::remove($_path);

            }

            $_handle = self::$_installer_log['plugin_handle'];
            if (!empty(self::$_installer_log['plugin_controller'])) 
            {
                $_controller = self::$_installer_log['plugin_controller'];

                // 3. Remove hooks...
                $_hooks = self::getAppHooks($_handle);
                if (!empty($_hooks)) \Cataleya\Plugins\Hook::remove($_controller, $_hooks);


                // 4. Remove plugin events...
                $_events = self::getAppEvents($_handle);
                if (!empty($_events)) \Cataleya\System\Event::remove($_events);
            }

            // 5. Remove from registry...
            $_delete_handle = DBH::getInstance()->prepare( 
            'DELETE FROM plugin_registry WHERE handle = :handle'
                                        );

            $_params = [ 'handle' => self::$_installer_log['plugin_handle'] ];
            DBH::bindParamsArray($_delete_handle, $_params);
                    
                    
            if (!$_delete_handle->execute()) {
                throw new Error(implode(', ', $_delete_handle->errorInfo()), $_delete_handle->errorCode()); 
            }

            // 6. Remove plugin config if any...
            $_Config = \Cataleya\Plugins\Config::load(self::$_installer_log['plugin_handle']);
            if (!empty($_Config)) $_Config->drop();

            // Clear log..
            self::$_installer_log = [];

    }
    
    
    
    public static function rememberApp($_plugin_handle) 
    {
        if (self::exists($_plugin_handle)) self::$_current_app = $_plugin_handle;
        
        return;
        
    }
    
    
    
    
    
    public static function forgetApp($_plugin_handle = null) 
    {
        if (empty($_plugin_handle) || self::exists($_plugin_handle)) self::$_current_app = null;
        
        return;
        
    }
    
    
    
    
    
    /**
     * getCurrentApp
     *
     * @return \Cataleya\Plugins\Context
     */
    public static function getCurrentApp() 
    {
        return (!empty(self::$_current_app)) 
                        ? new \Cataleya\Plugins\Context (self::$_current_app) : 
                        null;
    }

    
    /**
     * getCurrentAppHandle
     *
     * @return string
     */
    public static function getCurrentAppHandle() 
    {
        return self::$_current_app;
    }



    /**
     * getLogsPath
     *
     * @return void
     */
    private static function getLogsPath () {

        if (empty(self::$_installer_logs_path)) 
            self::$_installer_logs_path = CATALEYA_LOGS_PATH.DIRECTORY_SEPARATOR.'app-installer';

        if (!file_exists(CATALEYA_LOGS_PATH)) mkdir (CATALEYA_LOGS_PATH, 0777);
        if (!file_exists(self::$_installer_logs_path)) mkdir(self::$_installer_logs_path, 0777);
        
        return self::$_installer_logs_path;
    }



    /**
     * getIconsPath
     *
     * @return string
     */
    private static function getIconsPath ($_app_handle) {

        return (self::exists($_app_handle)) 
            ? self::getTemplatesPath().'/'.$_app_handle.'/assets/images/icons' : null;
    }




    /**
     * getTemplatesPath
     *
     * @return string
     */
    private static function getTemplatesPath () {

        if (empty(self::$_app_templates_path)) self::$_app_templates_path = PATH_SKIN . '/Apps';

        if (!file_exists(self::$_app_templates_path)) mkdir(self::$_app_templates_path, 0777);

        return self::$_app_templates_path;
    }
    /**
     * isInstalled
     *
     * @param string $_plugin_handle
     * @return bool
     */
    public static function isInstalled ($_plugin_handle) 
    {

        self::initRegistry();
        return isset(self::$_registry[$_plugin_handle]);
    }



    /**
     * exists
     *
     * @param string $_plugin_handle
     * @return bool
     */
    public static function exists ($_plugin_handle=null)
    {

        return  self::isInstalled($_plugin_handle);

    }



    /**
     * initRegistry
     *
     * @param bool $_reload
     * @return void
     */
    private static function initRegistry ($_reload=false) {
        
        if (!empty(self::$_registry) && $_reload !== true) return;

        static $_select_handle;

        if (empty($_select_handle)) {
            $_select_handle = DBH::getInstance()->prepare(
                'SELECT * FROM plugin_registry'
            );
        }

        if (!$_select_handle->execute()) {
            throw new Error(implode(', ', $_select_handle->errorInfo()), $_select_handle->errorCode()); 
        }

        self::$_registry = [];

        while ($row = $_select_handle->fetch(\PDO::FETCH_ASSOC)) {
            self::$_registry[$row['handle']] = $row;
        }
    
       

    }


    /**
     * isOn
     *
     * @param string $_plugin_handle
     * @return bool
     */
    public static function isOn ($_plugin_handle) 
    {

        if (!self::isInstalled($_plugin_handle)) throw new Error('Plugin "' . $_plugin_handle . '" not found.');

        return ((int)self::$_registry[$_plugin_handle]['status'] === 1);

    }



    /**
     * switchOn
     *
     * @param string $_plugin_handle
     * @return void
     */
    public static function switchOn ($_plugin_handle) 
    {
    
        self::updateRegistry($_plugin_handle, 'status', true);
        self::saveRegistryEntry($_plugin_handle);

    }


    /**
     * switchOff
     *
     * @param string $_plugin_handle
     * @return void
     */
    public static function switchOff ($_plugin_handle) 
    {

        self::updateRegistry($_plugin_handle, 'status', false);
        self::saveRegistryEntry($_plugin_handle);

    }



    /**
     * updateRegistry
     *
     * @param string $_plugin_handle
     * @param string $_key
     * @param mixed $_val
     * @return void
     */
    private static function updateRegistry ($_plugin_handle, $_key, $_val) {

        static $_allowed_keys = ['name', 'description', 'status'];
        if (!in_array($_key, $_allowed_keys)) throw new Error('Invalid parameter!');

        self::initRegistry();
        if (!self::isInstalled($_plugin_handle)) throw new Error('Plugin "' . $_plugin_handle . '" not found.');

        if (!isset( self::$_modified[$_plugin_handle] )) self::$_modified[$_plugin_handle] = [ $_key => $_val ];
        else  self::$_modified[$_plugin_handle][$_key] = $_val;

    }




    /**
     * saveRegistryEntry
     *
     * @param string $_plugin_handle
     * @return void
     */
    private static function saveRegistryEntry ($_plugin_handle) {

        if (!isset(self::$_modified[$_plugin_handle])) return;

        static $_allowed_keys = ['name'=>'string', 'description'=>'string', 'status'=>'boolean'];

        $_params = DBH::buildParamsArray($_allowed_keys, self::$_modified[$_plugin_handle]);
        $_set_str = [];
        foreach ($_params['placeholders'] as $c=>$h) { $_set_str[] = $c . '=' . $h; }


        if (empty($_set_str)) throw new Error('Missing PARAMS on ' . __LINE__);
        
        $_update_handle = DBH::getInstance()->prepare(
            'UPDATE plugin_registry SET '. 
            implode(', ', $_set_str) . 
            ' WHERE handle = :handle'
        );

        $_params['params']['handle'] = $_plugin_handle;
        DBH::bindParamsArray($_update_handle, $_params['params']);


        if (!$_update_handle->execute()) {
            throw new Error(implode(', ', $_update_handle->errorInfo()), $_update_handle->errorCode()); 
        }

        unset(self::$_modified[$_plugin_handle]);


    }




    /**
     * saveRegistry
     *
     * @return void
     */
    public static function saveRegistry () {

        if (empty(self::$_modified)) return;

        foreach (self::$_modified as $h=>$p) self::saveRegistryEntry($h);

        self::$_modified = [];

    }



    /**
     * getPluginList    Get list of Plugin Handles (installed plugins only).
     *
     *
     * Syntax is: Scope tokens are separated by dots like: 'scope.sub-scope'
     * You can also use a wild card like: '*' BUT:  
     *
     * 1.   You're not allowed to do: '*.sub-scope'. 
     * 
     * 2.   You cannot begin a scope def with '*' unless it is used alone 
     *      by itself like this: '*'.
     *
     * 3.   You cannot use an asterix anywhere inside of a scope def except
     *      at the end like this: 'scope.subscope.*'
     *
     * @param string $_scope 
     * @param boolean $_all False: return only enabled apps, True: Include disabled apps.
     * @return array Plugin handles.
     */
    public static function getPluginList ($_scope='*', $_all = false) 
    {

        if (
            $_scope !== '*' && 
            preg_match('/[a-zA-Z\.\-_]+(\.\*)?$/', $_scope) == false 
        ) throw new Error ('Invalid scope!');

        self::initRegistry();

        $_list = [];
        if ($_scope === '*') { 
            foreach (self::$_registry as $_entry) { 
                if ((int)$_entry['status'] === 1 || $_all === true) $_list[] = $_entry['handle'];
            }
        } else {

            $p = '/^('.preg_replace('/([\.\-])/', "\\\\$1", $_scope).')$/'; 
            $p = preg_replace('/(\*)/', "([a-zA-Z\-_]+)", $p); 
            foreach (self::$_registry as $_entry) {
                foreach (explode(' ', $_entry['scope']) as $s) {
                    if (
                        ((int)$_entry['status'] === 1 || $_all === true) && 
                        preg_match($p, $s) > 0) { $_list[] = $_entry['handle']; break; }
                }
            }

        }
        return $_list;

    }
        





    /**
     * getPluginController
     *
     * @param string $_plugin_handle
     * @return void
     */
    public static function getPluginController ($_plugin_handle) 
    {

        return (self::exists($_plugin_handle)) 
            ? self::$_registry[$_plugin_handle]['controller_class']
            : null;

    }




    /**
     * isPluginController
     *
     * @param string $_class
     * @param bool $_return_reflection_obj
     * @return \ReflectionClass|bool
     */
    public static function isPluginController($_class, $_return_reflection_obj = false) {
        if (!is_string($_class)) throw Error ('Argument must be a string!');;

        self::initRegistry();
        $_match = false;

        foreach (self::$_registry as $h=>$i) {
            if ($i['controller_class'] === $_class) { $_match = true; break; }
        }

        return ($_match !== false && $_return_reflection_obj === true) 
            ? new \ReflectionClass($_class) 
            : $_match;
        
    } 




    /**
     * getAppList    Get list of Plugin Handles (installed plugins only).
     *
     *
     * Syntax is: Scope tokens are separated by dots like: 'scope.sub-scope'
     * You can also use a wild card like: '*' BUT:  
     *
     * 1.   You're not allowed to do: '*.sub-scope'. 
     * 
     * 2.   You cannot begin a scope def with '*' unless it is used alone 
     *      by itself like this: '*'.
     *
     * 3.   You cannot use an asterix anywhere inside of a scope def except
     *      at the end like this: 'scope.subscope.*'
     *
     *
     * @param string $_scope 
     * @param boolean $_all False: return only enabled apps, True: Include disabled apps.
     * @return array Plugin handles.
     */
    public static function getAppList ($_scope='*', $_all = false) 
    {

        return self::getPluginList($_scope);

    }


    /**
     * getAppInfo
     *
     * @return array [ plugin_controller, name, handle, description ]
     */
    public static function getAppInfo ($_plugin_handle) 
    {

        static $_base_url = null;
        if (empty($_base_url)) $_base_url = (SSL_REQUIRED ? 'https://' : 'http://') . HOST . '/' . ROOT_URI . 'ui/images/app-icons';

        return (self::exists($_plugin_handle)) 
            ? [
                'handle' => $_plugin_handle, 
                'name' => self::$_registry[$_plugin_handle]['name'],
                'description' => self::$_registry[$_plugin_handle]['description'],
                'vendor' => self::$_registry[$_plugin_handle]['vendor'],
                'version' => self::$_registry[$_plugin_handle]['version'],
                'active' => self::isOn($_plugin_handle), 
                'icon' => [
                    'hrefs' => self::getAppIcons($_plugin_handle)
                    ]

                ]

            : null;

    }




    private static function getAppIcons ($_app_handle) {

        $_base_url = (SSL_REQUIRED ? 'https://' : 'http://') . 
            HOST . '/' . ROOT_URI . 
            'skin/Apps/'.$_app_handle.'/assets/images/icons';

        
        return self::exists($_app_handle) 
            ? [
                'large' => $_base_url.'/large.png', 
                'thumb' => $_base_url.'/small.png', 
            ] : null;

    }




    /**
     * getAppController
     *
     * @param string $_plugin_handle
     * @return string
     */
    public static function getAppController ($_plugin_handle=null) 
    {
        return self::getPluginController($_plugin_handle);
    }


    /**
     * getAppHooks List of events our plugin is listening to.
     *
     * @param string $_plugin_handle
     * @return string
     */
    public static function getAppHooks ($_plugin_handle) 
    {

        if (!self::exists($_plugin_handle)) return null;

        if (is_string(self::$_registry[$_plugin_handle]['hooks'])) 
        {
            self::$_registry[$_plugin_handle]['hooks'] = preg_split(
                '/[\s,]+/', self::$_registry[$_plugin_handle]['hooks']
            ); 
        } 
        
        
        if (!is_array(self::$_registry[$_plugin_handle]['hooks'])) 
        {
            self::$_registry[$_plugin_handle]['hooks'] = [];
        }

        return self::$_registry[$_plugin_handle]['hooks'];

    }



    /**
     * getAppListenerEvents List of events our plugin is listening to.
     *
     * @param string $_plugin_handle
     * @return string
     */
    public static function getAppListenerEvents ($_plugin_handle) 
    {

        return self::getAppHooks($_plugin_handle);

    }


    /**
     * getAppEvents List of plugin events that others can observe / listen to.
     *
     * @param string $_plugin_handle
     * @return string
     */
    public static function getAppEvents ($_plugin_handle) 
    {


        if (!self::exists($_plugin_handle)) return null;

        if (is_string(self::$_registry[$_plugin_handle]['events'])) 
        {
            self::$_registry[$_plugin_handle]['events'] = preg_split(
                '/[\s,]+/', self::$_registry[$_plugin_handle]['events']
            ); 
        }
        
        
        if (!is_array(self::$_registry[$_plugin_handle]['events'])) 
        {
            self::$_registry[$_plugin_handle]['events'] = [];
        }

        return self::$_registry[$_plugin_handle]['events'];


    }




    /**
     * getAppName
     *
     * @param string $_plugin_handle
     * @return string
     */
    public static function getAppName ($_plugin_handle) 
    {

        return (self::exists($_plugin_handle)) 
            ? self::$_registry[$_plugin_handle]['name']
            : null;

    }
 


    /**
     * getAppDescription
     *
     * @param string $_plugin_handle
     * @return string
     */
    public static function getAppDescription ($_plugin_handle) 
    {

        return (self::exists($_plugin_handle)) 
            ? self::$_registry[$_plugin_handle]['description']
            : null;

    }



    /**
     * getAppNameSpace
     *
     * @param string $_plugin_handle
     * @return string
     */
    public static function getAppNameSpace ($_plugin_handle) 
    {

        return (self::exists($_plugin_handle)) 
            ? self::$_registry[$_plugin_handle]['namespace']
            : null;

    }







}
