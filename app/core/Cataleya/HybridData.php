<?php


namespace Cataleya;



/*

ABSTRACT HYBRID DATA
 
 Note: Use this if you need both [ _Abstract_Data ] and [ _Abstract_Collection ]. 

*/



abstract class HybridData extends \Cataleya\Collection  {
    



	/*
	 *
	 * [ __destruct ]
	 * ________________________________________________________________
	 *
	 *
	 *
	 */

        public function __destruct()
        {
                    $this->saveData();
        }
        
        
        
        
        
        /*
         * 
         * public [ saveData ]
         * ______________________________________________________________________
         * 
         * 
         */





        public function saveData () 
	{
		

		if (empty($this->_modified)) return;
                $this->_modified = array_unique($this->_modified);
                
		$_update_params = array();
		$key_val_pairs = array();
		
		
		foreach ($this->_modified as $key) { 
                    $key_val_pairs[$key] = $key . ' = :' . $key;
                }
	
		// Do prepared statement for 'INSERT'...
		$update_handle = $this->dbh->prepare('
                                                        UPDATE ' . static::TABLE . '  
                                                        SET ' . implode (', ', $key_val_pairs) . ', last_modified=now()       
                                                        WHERE ' . static::PK . ' = :instance_id 
                                                        ');
                
                $_update_params['id'] = $this->getID();               
		$update_handle->bindParam(':instance_id', $_update_params['id'], \Cataleya\Helper\DBH::getTypeConst($_update_params['id']));

                
		foreach ($this->_modified as $key) {
                    
                        $_update_params[$key] = $this->_data[$key];                        
			$update_handle->bindParam(':'.$key, $_update_params[$key], \Cataleya\Helper\DBH::getTypeConst($_update_params[$key]));

		}
		

		
		if (!$update_handle->execute()) $this->e->triggerException('Error in class (' . __CLASS__ . '): [ ' . implode(', ', $update_handle->errorInfo()) . ' ] on line ' . __LINE__);
		
                $this->_modified = array ();
	}
	
	
       
 
 





	 
	 /*
	 *  [ delete ]
	 * ________________________________________________________________
	 * 
	 * 
	 *
	 *
	 */

	 
	public function delete () 
	{
                        
                        

			// DELETE 
			$delete_handle = $this->dbh->prepare('
													DELETE FROM ' . static::TABLE . ' 
													WHERE ' . static::PK . ' = :instance_id
													');
                        
 			$delete_handle_param_instance_id = $this->getID();                       
			$delete_handle->bindParam(':instance_id', $delete_handle_param_instance_id, \Cataleya\Helper\DBH::getTypeConst($delete_handle_param_instance_id));
                        
	
			if (!$delete_handle->execute()) $this->e->triggerException('
											Error in class (' . __CLASS__ . '): [ ' . 
											implode(', ', $delete_handle->errorInfo()) . 
											' ] on line ' . __LINE__);
			

                        return TRUE;
		
		
	}

        
        
        





        /*
         *
         * [ getID ] 
         *_____________________________________________________
         *
         *
         */

        abstract public function getID();
        
        
        


        
        

        

        
        



        





}
	
	
	
	
?>