<?php


namespace Cataleya\Shipping;



/*

CLASS SHIPPING OPTION

*/



class ShippingOption   
{
	
	
	private $_data = array();
	private $_modified = array();
        
	private $dbh, $e;
        
        private $_carrier, $_zone, $_description, $_cost;
	

	


	/*
	 *
	 * [ __construct ]
	 * ________________________________________________________________
	 *
	 *
	 *
	 */
	 
	 
	private function __construct ()
	{
		
		// Get database handle...
                $this->dbh = \Cataleya\Helper\DBH::getInstance();
		
		// Get error handler
		$this->e = \Cataleya\Helper\ErrorHandler::getInstance();
		
	}
	




	/*
	 *
	 * [ __destruct ]
	 * ________________________________________________________________
	 *
	 *
	 *
	 */

        public function __destruct()
        {
                    $this->saveData();
        }
	




	/*
	 *
	 * [ load ]
	 * ________________________________________________________________
	 *
	 *
	 *
	 */
	 
	 
	public static function load ($id = 0)
	{
		
                if ($id == 0 || filter_var($id, FILTER_VALIDATE_INT) == FALSE) return NULL;
		
		$instance = new \Cataleya\Shipping\ShippingOption ();
		
		// LOAD: CURRENCY
		static $select_handle, $param_instance_id;
		
			
		if (empty($select_handle)) {
			// PREPARE SELECT STATEMENT...
			$select_handle = $instance->dbh->prepare('SELECT * FROM shipping_options WHERE shipping_option_id = :instance_id LIMIT 1');
			$select_handle->bindParam(':instance_id', $param_instance_id, \PDO::PARAM_INT);
		}
		
		
		$param_instance_id = $id;
		
		if (!$select_handle->execute()) $instance->e->triggerException('
												Error in class (' . __CLASS__ . '): [ ' . 
												implode(', ', $select_handle->errorInfo()) . 
												' ] on line ' . __LINE__);
		
		$instance->_data = $select_handle->fetch(\PDO::FETCH_ASSOC);
                
               
		if (empty($instance->_data))
		{
			unset($instance);
			return NULL;
		}


		return $instance;	
		
		
	}



	/*
	 *
	 *  [ create ]
	 * ________________________________________________________________
	 * 
	 * 
	 *
	 *
	 */

	 
	static public function create (\Cataleya\Store $_store, \Cataleya\Agent $_carrier, \Cataleya\Geo\Zone $_zone, \Cataleya\Asset\Description $_description) 
	{
            
            
            
                /*
                 * Handle price attribute
                 * 
                 */

                $_price = \Cataleya\Catalog\Price::create ();
                
                
		
		// Get database handle...
                $dbh = \Cataleya\Helper\DBH::getInstance();
		
		// Get error handler
		$e = \Cataleya\Helper\ErrorHandler::getInstance();
		
		// construct
		static 
				$insert_handle,  
                                $insert_handle_param_store_id, 
                                $insert_handle_param_carrier_id, 
                                $insert_handle_param_zone_id, 
                                $insert_handle_param_description_id, 
                                $insert_handle_param_price_id;
		
		if (empty($insert_handle))
		{
			$insert_handle = $dbh->prepare('
                                                        INSERT INTO shipping_options 
                                                        (store_id, carrier_id, zone_id, description_id, price_id, last_modified, date_added) 
                                                        VALUES (:store_id, :carrier_id, :zone_id, :description_id, :price_id, NOW(), NOW())
                                                        ');
															
			$insert_handle->bindParam(':store_id', $insert_handle_param_store_id, \PDO::PARAM_INT);
                        $insert_handle->bindParam(':carrier_id', $insert_handle_param_carrier_id, \PDO::PARAM_INT);	
			$insert_handle->bindParam(':zone_id', $insert_handle_param_zone_id, \PDO::PARAM_INT);	
			$insert_handle->bindParam(':description_id', $insert_handle_param_description_id, \PDO::PARAM_INT);
			$insert_handle->bindParam(':price_id', $insert_handle_param_price_id, \PDO::PARAM_INT);
		}
		
                $insert_handle_param_store_id = $_store->getStoreId();
                $insert_handle_param_carrier_id = $_carrier->getID();
                $insert_handle_param_zone_id = $_zone->getID();
                $insert_handle_param_description_id = $_description->getID();
                $insert_handle_param_price_id = $_price->getID();
		
		if (!$insert_handle->execute()) $e->triggerException('
										Error in class (' . __CLASS__ . '): [ ' . 
										implode(', ', $insert_handle->errorInfo()) . 
										' ] on line ' . __LINE__);
										
		
		// AUTOLOAD NEW PRODUCT AND RETURN IT
		$instance = self::load($dbh->lastInsertId());
		
		return $instance;
		
		
	}


	 
	 /*
	 *  [ delete ]
	 * ________________________________________________________________
	 * 
	 * 
	 *
	 *
	 */

	 
	public function delete () 
	{
		
			// DELETE 
			$delete_handle = $this->dbh->prepare('
													DELETE FROM shipping_options 
													WHERE shipping_option_id = :instance_id
													');
			$delete_handle->bindParam(':instance_id', $delete_handle_param_instance_id, \PDO::PARAM_INT);
			
			$delete_handle_param_instance_id = $this->_data['shipping_option_id'];
	
			if (!$delete_handle->execute()) $this->e->triggerException('
											Error in class (' . __CLASS__ . '): [ ' . 
											implode(', ', $delete_handle->errorInfo()) . 
											' ] on line ' . __LINE__);
											
			
                        $this->getCost()->delete();
                        $this->getDescription()->delete();

		
		// $this = NULL;
		return TRUE;
		
		
	}





        /*
         *
         * [ getID ] 
         *_____________________________________________________
         *
         *
         */

        public function getID()
        {
                return $this->_data['shipping_option_id'];
        }




	
	
	public function saveData () 
	{
		

		if (empty($this->_modified)) return;
                $this->_modified = array_unique($this->_modified);
                
		$_update_params = array();
		$key_val_pairs = array();
		
		foreach ($this->_modified as $key) $key_val_pairs[$key] = $key . ' = :' . $key;
		
	
		// Do prepared statement for 'INSERT'...
		$update_handle = $this->dbh->prepare('
                                                        UPDATE shipping_options 
                                                        SET ' . implode (', ', $key_val_pairs) . ', last_modified=now()       
                                                        WHERE shipping_option_id = :instance_id 
                                                        ');
                
		$update_handle->bindParam(':instance_id', $_update_params['shipping_option_id'], \PDO::PARAM_INT);
                $_update_params['shipping_option_id'] = $this->_data['shipping_option_id'];
                
		foreach ($this->_modified as $key) {
                    
                        if ($this->_data[$key] === NULL) $param_type = \PDO::PARAM_NULL;
                        else if (is_int($this->_data[$key])) $param_type = \PDO::PARAM_INT;
                        else $param_type = \PDO::PARAM_STR;
                        
			$update_handle->bindParam(':'.$key, $_update_params[$key], $param_type);
                        $_update_params[$key] = $this->_data[$key];
                        
		}
		
		

		
		if (!$update_handle->execute()) $this->e->triggerException('Error in class (' . __CLASS__ . '): [ ' . implode(', ', $update_handle->errorInfo()) . ' ] on line ' . __LINE__);
		
	}
	
	
       
 
 
        
        
        


        /**
         * 
         * @return \Cataleya\Agent
         *
         */
        public function getCarrier () 
        {
                                
             if (empty($this->_carrier)) $this->_carrier = \Cataleya\Agent::load($this->_data['carrier_id']);
            
            return $this->_carrier;
        }

        
        


        /**
         * 
         * @param \Cataleya\Agent $_carrier
         * @return boolean
         */
        public function setCarrier (\Cataleya\Agent $_carrier) 
        {
            if ((int)$this->_data['carrier_id'] == (int)$_carrier->getID()) return TRUE;
                                
            $this->_data['carrier_id'] = $_carrier->getID();
            $this->_modified[] = 'carrier_id';
            
            $this->_carrier = $_carrier;
            
            return TRUE;
        }

        
        
        



        /*
         * 
         * [ getShippingZone ]
         * ________________________________________________
         * 
         * 
         */

        public function getShippingZone () 
        {
                                
             if (empty($this->_zone)) $this->_zone = \Cataleya\Geo\Zone::load($this->_data['zone_id']);
            
            return $this->_zone;
        }

        
        

        /*
         * 
         * [ setShippingZone ]
         * ________________________________________________
         * 
         * 
         */

        public function setShippingZone (\Cataleya\Geo\Zone $_zone) 
        {
            if ((int)$this->_data['zone_id'] == (int)$_zone->getID()) return TRUE;
                                
            $this->_data['zone_id'] = $_zone->getID();
            $this->_modified[] = 'zone_id';
            
            $this->_zone = $_zone;
            
            return TRUE;
        }
        
        
        
        


        /*
         * 
         * [ getStore ]
         * ________________________________________________
         * 
         * 
         */

        public function getStore () 
        {
             static $_store = NULL;
             
             if (empty($_store)) $_store = \Cataleya\Store::load($this->_data['store_id']);
            
             return $_store;
        }

        
        
        

        /*
         * 
         * [ getStoreId ]
         * ________________________________________________
         * 
         * 
         */

        public function getStoreId () 
        {
             return $this->_data['store_id'];
        }

        
      
        
        
        
        /*
         * 
         * [ getDescription ]
         * 
         * 
         */
        
        
        public function getDescription () 
        {
            if (empty($this->_description)) $this->_description = \Cataleya\Asset\Description::load($this->_data['description_id']);
            
            return $this->_description;
        }
        
        
        
        
        
        /*
         * 
         * [ getCost ]
         * 
         * 
         */
        
        
        public function getCost () 
        {
            return $this->getRate();
        }
        
        
        /*
         * 
         * [ setCost ]
         * 
         * 
         */
        
        
        public function setCost ($value) 
        {
            return $this->setRate($value);
        }
        

        
        /*
         * 
         * [ getRate ]
         * 
         * 
         */
        
        
        public function getRate () 
        {
            if (empty($this->_cost)) $this->_cost = \Cataleya\Catalog\Price::load($this->_data['price_id']);
            
            return $this->_cost->getValue($this->getStoreId());
        }
        
        
        
        
        
        /*
         *
         * [ setRate ] 
         *_____________________________________________________
         *
         *
         */

        public function setRate($value = NULL)
        {
                $value = filter_var($value, FILTER_VALIDATE_FLOAT, FILTER_NULL_ON_FAILURE);

                if ($value===NULL) $this->e->triggerException('Error in class (' . __CLASS__ . '): [ invalid argument for method: ' . __FUNCTION__ . ' ] on line ' . __LINE__);
                
                if (empty($this->_cost)) $this->_cost = \Cataleya\Catalog\Price::load($this->_data['price_id']);
                $this->_cost->setValue($this->getStoreId(), $value);

                return $this;
        }

        


        
        



        /*
         * 
         * [ enable ]
         * ______________________________________________________
         * 
         * 
         */


        public function enable () 
        {
            if ((int)$this->_data['active'] === 0) 
            {
                $this->_data['active'] = 1;
                $this->_modified[] = 'active';
            }

            return TRUE;
        }





        /*
         * 
         * [ disable ]
         * ______________________________________________________
         * 
         * 
         */


        public function disable () 
        {
            if ((int)$this->_data['active'] === 1) 
            {
                $this->_data['active'] = 0;
                $this->_modified[] = 'active';
            }

            return TRUE;
        }
        
        
        
        
        
        

        /*
         *
         * [ getMaxDeliveryDays ] 
         *_____________________________________________________
         *
         *
         */

        public function getMaxDeliveryDays()
        {
                return (int)$this->_data['max_delivery_days'];
        }



        /*
         *
         * [ setMaxDeliveryDays ] 
         *_____________________________________________________
         *
         *
         */

        public function setMaxDeliveryDays($value = NULL)
        {
                if ($value===NULL || filter_var($value, FILTER_VALIDATE_INT) === FALSE) return FALSE;

                $this->_data['max_delivery_days'] = (int)$value;
                $this->_modified[] = 'max_delivery_days';

                return TRUE;
        }
        
        
        
        
        /*
         *
         * [ getRateType ] 
         *_____________________________________________________
         *
         *
         */

        public function getRateType()
        {

                return $this->_data['rate_is_per'];
        }
        
        
        
        /*
         *
         * [ setRateType ] 
         *_____________________________________________________
         *
         *
         */

        public function setRateType($value = NULL)
        {
                if (!is_string($value) || !in_array($value, array('weight', 'unit', 'volume', 'flat'))) return FALSE;

                $this->_data['rate_is_per'] = $value;
                $this->_modified[] = 'rate_is_per';

                return TRUE;
        }
        
        
        
        
        
        
        

        /*
         *
         * [ getDateAdded ] 
         *_____________________________________________________
         *
         *
         */

        public function getDateAdded()
        {
                return $this->_data['date_added'];
        }
        
        
        
        
        
        
       
        
        /*
         *
         * [ isTaxable ] 
         *_____________________________________________________
         *
         *
         */

        public function isTaxable()
        {
                return ((int)$this->_data['is_taxable'] === 1) ? TRUE : FALSE;
        }



        
            

        /*
         * 
         * [ makeTaxable ]
         * ______________________________________________________
         * 
         * 
         */


        public function makeTaxable () 
        {

            if ((int)$this->_data['is_taxable'] === 0) 
            {
                $this->_data['is_taxable'] = 1;
                $this->_modified[] = 'is_taxable';
            }

            return $this;
        }





        /*
         * 
         * [ makeNonTaxable ]
         * ______________________________________________________
         * 
         * 
         */


        public function makeNonTaxable () 
        {
            if ((int)$this->_data['is_taxable'] === 1) 
            {
                $this->_data['is_taxable'] = 0;
                $this->_modified[] = 'is_taxable';
            }

            return $this;
        }
        
        
        
  



        



}
	
	
	
	
