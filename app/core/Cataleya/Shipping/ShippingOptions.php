<?php



namespace Cataleya\Shipping;




/*
 *
 * 	@Package: Cataleya
 * 	@Class: \Cataleya\Shipping\ShippingOptions
 *
 * 	(c) 2012 Fancy Paper Planes
 *
 *
 *
 */

class ShippingOptions extends \Cataleya\Collection {
   
    // To be used by [\Cataleya\Collection]
    protected $_collection = array ();
    protected $_position = 0;
    
    // CONST VALUES 1 - 15 RESERVED FOR [ _Abstract_Collection ]
    
    const ORDER_BY_NAME = 16;
    const ORDER_BY_ID = 17;


    public function __construct() {
        
        // Get database handle...
        $this->dbh = \Cataleya\Helper\DBH::getInstance();
		
        // Get error handler
        $this->e = \Cataleya\Helper\ErrorHandler::getInstance();
        

        
        parent::__construct();

        
    }
    
    
    
    

	/*
	 *
	 * [ load ]
	 * ________________________________________________________________
	 *
	 *
	 *
	 */
	 
	 
	public static function load (\Cataleya\Store $_store, $sort = array('order_by'=>'', 'order'=>'', 'index'=>''), $page_size = 100, $page_num = 1)
	{
            
            
                $_objct = __CLASS__;
                $instance = new $_objct;
                
                

                // This is used to determine which column is to be used in the index...
                // $_index_with = 'title';

                if (is_array($sort) && !empty($sort['order_by']) && !empty($sort['order']) && is_int($sort['order_by']) && is_int($sort['order']) )
                {


                    // ASC | DESC
                    switch ($sort['order']) {

                        case self::ORDER_ASC:
                            $param_order = 'ASC';
                            break;

                        case self::ORDER_DESC:
                            $param_order = 'DESC';
                            break;

                        default :
                            $param_order = 'ASC';
                            break;
                    }  


                    // ORDER BY
                    switch ($sort['order_by']) {

                        case self::ORDER_BY_NAME:
                            $param_order_by = "title $param_order";
                            $_index_with = 'title';
                            break;

                        case self::ORDER_BY_ID:
                            $param_order_by = "shipping_option_id $param_order";
                            $_index_with = 'shipping_option_id';
                            break;
                        
                        case self::ORDER_BY_CREATED:
                            $param_order_by = "date_added $param_order";
                            $_index_with = 'date_added';
                            break;
                        
                        case self::ORDER_BY_MODIFIED:
                            $param_order_by = "last_modified $param_order";
                            $_index_with = 'last_modified';
                            break;

                        default :
                            $param_order_by = "title $param_order";
                            $_index_with = 'title';
                            break;
                    }





                }

                // Defaults                
                else {

                        $param_order = 'ASC';
                        $param_order_by = "title $param_order";
                        $_index_with = 'title';

                }    



                // Check if index is specified
                if (is_array($sort) && isset($sort['index']) && is_string($sort['index']))
                {
                    $param_regex = (preg_match('/^[a-zA-Z]{1}$/', $sort['index']) > 0) ? '^[' . strtolower($sort['index']) . '].*' : '.*';
                }

                else {
                    $param_regex = '.*';
                }




                $param_offset = 0;
                $param_limit = 99999999;
                $param_store_id = $_store->getStoreId();
        
        


                // PREPARE SELECT STATEMENT...
                $select_handle = $instance->dbh->prepare('
                                                SELECT shipping_option_id  
                                                FROM shipping_options a 
                                                INNER JOIN content_languages b 
                                                ON a.description_id = b.content_id 
                                                WHERE ' . $_index_with . ' REGEXP :regex 
                                                AND store_id = :store_id 
                                                ORDER BY '. $param_order_by . '   
                                                LIMIT :offset, :limit
                                                ');

                $select_handle->bindParam(':offset', $param_offset, \PDO::PARAM_INT);
                $select_handle->bindParam(':limit', $param_limit, \PDO::PARAM_INT);
                $select_handle->bindParam(':regex', $param_regex, \PDO::PARAM_STR);
                $select_handle->bindParam(':store_id', $param_store_id, \PDO::PARAM_INT);
                

                if (!$select_handle->execute()) $instance->e->triggerException('
                                                                                                Error in class (' . __CLASS__ . '): [ ' . 
                                                                                                implode(', ', $select_handle->errorInfo()) . 
                                                                                                ' ] on line ' . __LINE__);
                while ($row = $select_handle->fetch(\PDO::FETCH_ASSOC))
                {
                    $instance->_collection[] = $row['shipping_option_id'];
                }

                
                $instance->pageSetup($page_size, $page_num);
                
                return $instance;

            
        }
    
    
    
    
        public function current()
        {
            return \Cataleya\Shipping\ShippingOption::load($this->_collection[$this->_position]);

        }
        
        
        
        
    
    /*
     * 
     * [ search ]
     * 
     */
    
    public static function search ($_term = '', $sort = array('order_by'=>'', 'order'=>'', 'index'=>''), $page_size = 100, $page_num = 1) 
    {
        

                // This is used to determine which column is to be used in the index...
                // $_index_with = 'title';

                if (is_array($sort) && !empty($sort['order_by']) && !empty($sort['order']) && is_int($sort['order_by']) && is_int($sort['order']) )
                {


                    // ASC | DESC
                    switch ($sort['order']) {

                        case self::ORDER_ASC:
                            $param_order = 'ASC';
                            break;

                        case self::ORDER_DESC:
                            $param_order = 'DESC';
                            break;

                        default :
                            $param_order = 'ASC';
                            break;
                    }  


                    // ORDER BY
                    switch ($sort['order_by']) {

                        case self::ORDER_BY_NAME:
                            $param_order_by = "title $param_order";
                            $_index_with = 'title';
                            break;

                        case self::ORDER_BY_ID:
                            $param_order_by = "shipping_option_id $param_order";
                            $_index_with = 'shipping_option_id';
                            break;
                        
                        case self::ORDER_BY_CREATED:
                            $param_order_by = "date_added $param_order";
                            $_index_with = 'date_added';
                            break;
                        
                        case self::ORDER_BY_MODIFIED:
                            $param_order_by = "last_modified $param_order";
                            $_index_with = 'last_modified';
                            break;

                        default :
                            $param_order_by = "title $param_order";
                            $_index_with = 'title';
                            break;
                    }




                }

                // Defaults                
                else {

                        $param_order = 'ASC';
                        $param_order_by = "title $param_order";
                        $_index_with = 'title';

                }    



            // First, take out funny looking characters...
            $_term = preg_replace('/[^-\s\.A-Za-z0-9@*#+]+/', '', $_term);

            // Trim then split terms by [:space:]
            $terms = explode(' ', trim($_term, ' '));



            // SORT SEARCH TERMS
            $regex = array();
            $regex['title'] = array();


            foreach ($terms as $key=>$val) {

                    // Loose any search terms shorter than 3 chars...
                    if (strlen($val) < 2) unset($terms[$key]);
                    
                    // Also make dots safe...
                    else if (preg_match('/[-\s\.A-Za-z0-9]{1,100}/', $val))  $regex['title'][] = preg_replace('/[\.]/', '\.', $val); 
            }

            
            // Build REGEX
            $regex_concat = '';
            foreach ($regex as $k => $v) {
                    if (count($v) == 0) {$regex[$k] = ''; continue; } // no search terms...
                    elseif (count($v) > 3) $v = array_slice($v, 0, 3); // truncate if there are too many search terms...

                    $regex[$k] = "^.*(" . implode('|', $v) . ").*$";
                    $regex_concat .= ($regex_concat != '') ? ' OR ' : '';
                    $regex_concat .= $k . ' REGEXP :regex_' . $k;


            }

            
            
            // Create instance...
            $_objct = __CLASS__;
            $instance = new $_objct;


            // If nothing made it through the filtering...
            // [ _collection ] will be empty a.k.a no results..
            if ($regex_concat == '') return $instance;




            $param_offset = 0;
            $param_limit = 99999999;
        
            
            
            // DO SEARCH...
            $select_handle = \Cataleya\Helper\DBH::getInstance()->prepare('
                                            SELECT shipping_option_id 
                                            FROM shipping_options tc 
                                            INNER JOIN content_languages dc 
                                            ON tc.description_id = dc.content_id 
                                            WHERE ' . $regex_concat . '  
                                            ORDER BY '. $param_order_by . '   
                                            LIMIT :offset, :limit
                                            ');

            $select_handle->bindParam(':offset', $param_offset, \PDO::PARAM_INT);
            $select_handle->bindParam(':limit', $param_limit, \PDO::PARAM_INT);
            
            // bind search search params
            foreach ($regex as $k=>$v) $select_handle->bindParam(':regex_'.$k, $regex[$k], \PDO::PARAM_STR);

            if (!$select_handle->execute()) \Cataleya\Helper\ErrorHandler::getInstance ()->triggerException('
                                                                        Error in class (' . __CLASS__ . '): [ ' . 
                                                                        implode(', ', $select_handle->errorInfo()) . 
                                                                        ' ] on line ' . __LINE__);





            while ($row = $select_handle->fetch(\PDO::FETCH_ASSOC))$instance->_collection[] = $row['shipping_option_id'];

            $instance->pageSetup($page_size, $page_num);

            return $instance;
        
    }
    
    
    


      
    
    
    
}


