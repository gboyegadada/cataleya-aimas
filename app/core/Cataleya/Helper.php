<?php



namespace Cataleya;
use \Cataleya\Error;


/*
 *
 * 	@Package: Cataleya
 * 	@Class: Helper
 *
 * 	(c) 2012 Fancy Paper Planes
 *
 *
 *
 */

class Helper {
    //put your code here
    
    
    static public function countInEnglish ($_count = 0, $_singular = 'item', $_plural = 'items') 
        {
                switch ($_count) {
                    case 0:
                        return 'No '.$_plural;
                        break;

                    case 1:
                        return '1 '.$_singular;
                        break;

                    default:
                        return $_count . ' ' . $_plural;
                        break;
                }
        }
        
        
    static public function getVerb ($_count = 0, $_pad = FALSE) 
        {
                
                $p = (filter_var($_pad, FILTER_VALIDATE_BOOLEAN)) ? ' ' : '';
                
                switch ($_count) {
                    case 0:
                        return $p.'have'.$p;
                        break;

                    case 1:
                        return $p.'has'.$p;
                        break;

                    default:
                        return $p.'have'.$p;
                        break;
                }
        }
        
        
        
    /**
     * codify
     *
     * @param string $_str
     * @param int $_len
     * @return string
     */
    public static function codify($_str = '', $_len = 2)
    {
       if (!is_string($_str) || empty($_str)) throw new Error ('Argument 1 should be a string and not be empty.');
       if (!is_int($_len)) throw new Error ('Argument 2 should be an integer.');

       $code = ''; // empty string
       $vowels = ['a', 'e', 'i', 'o', 'u', 'y']; // vowels
       preg_match_all('/[A-Z][a-z]*/', ucfirst($_str), $m); // Match every word that begins with a capital letter, added ucfirst() in case there is no uppercase letter

       foreach($m[0] as $substring){

           $substring = str_replace($vowels, '', strtoupper($substring)); // String to upper case and remove all vowels
           $code .= preg_replace('/([a-zA-Z]{'.$_len.'})(.*)/', '$1', $substring); // Extract the first N letters.

       }

       return $code;
    } 




    /**
     * makeFormControl
     *
     * @param array $_params
     * @return string
     */
    public static function makeFormControl(array $_params, $_echo = false)
    {

        $_control = isset($_params['control']) ? $_params['control'] : 'text';
        $_html = '';

        $_id = isset($_params['id']) ? $_params['id'] : '';
        $_class_list = isset($_params['classes']) ? $_params['classes'] : '';

        switch ($_control) 
        {
        case 'text':
            $_html = '<input type="text" id="'.$_id.'"  class="'.$_class_list.'"  name="' . $_params['name'] . '" value="' . $_params['value'] . '" />';
            break;

        case 'textarea':
            $_html = '<textarea id="'.$_id.'"  class="'.$_class_list.'"  name="' . $_params['name'] . '">' . $_params['value'] . '</textarea>';
            break;

        case 'select':
            $_html = '<select id="'.$_id.'"  class="'.$_class_list.'"  name="' . $_params['name'] . '">';
            if (isset($_params['options']) && is_array($_params['options'])) {
                foreach ($_params['options'] as $_opt) { 
                    $_html .= '<option value="'.$_opt['value'].'">'.$_opt['label'].'</option>';
                }
            }
            $_html .= '</select>';
            break;

        case 'radio':
            if (isset($_params['options']) && is_array($_params['options'])) {
                foreach ($_params['options'] as $_opt) { 
                    $_html .= '<label for="'.$_params['name'].'"><input class="'.$_class_list.'"  type="radio" id="' . 
                            $_params['name'].'" name="'.$_params['name'].'" value="' . 
                            $_opt['value'].'" /></label>';
                }
            }
            break;

        case 'checkbox':
            if (isset($_params['options']) && is_array($_params['options'])) {
                foreach ($_params['options'] as $_opt) { 
                    $_html .= '<label for="'.$_params['name'].'"><input class="'.$_class_list.'" type="checkbox" id="' . 
                            $_params['name'].'" name="'.$_params['name'].'" value="' . 
                            $_opt['value'].'" /></label>';
                }
            }
            break;
        }

        if ($_echo===true) echo $_html;

        return $_html;


    }

        
        
        /*
         * 
         * [ getFingerPrint  ]
         * 
         * 
         */
        
        static public function getFingerPrint () 
        {
            static $_fp = NULL;
            if (empty($_fp) && !is_string($_fp)) 
            {
                 //$_suffix = defined('LOGIN_TOKEN') ? '-'.LOGIN_TOKEN : '';
                 $_fp = isset($_SERVER['HTTP_X_CATALEYA_FINGER_PRINT']) ? filter_var($_SERVER['HTTP_X_CATALEYA_FINGER_PRINT'], FILTER_VALIDATE_REGEXP, array('options'=>array('regexp' => '/^[A-Za-z0-9]{1,100}$/'))) : '';
            }

            return $_fp;
        }
        
        
        
        
        
        /*
         * 
         * [ getRequestToken  ]
         * 
         * 
         */
        
        static public function getRequestToken () 
        {
            
            static $_t = NULL;
            if (empty($_t) && !is_string($_t)) 
            {
                 $_t = isset($_SERVER['HTTP_X_CATALEYA_REQUEST_TOKEN']) ? filter_var($_SERVER['HTTP_X_CATALEYA_REQUEST_TOKEN'], FILTER_VALIDATE_REGEXP, array('options'=>array('regexp' => '/^[A-Za-z0-9,\-]{20}$/'))) : '';
            }

            return $_t;
        }
        
        
        
        

        
        /*
         * 
         * [ getAuthToken  ]
         * 
         * 
         */
        
        static public function getAuthToken () 
        {
            
            static $_t = NULL;
            if (empty($_t) && !is_string($_t)) 
            {
                 $_t = isset($_SERVER['HTTP_X_CATALEYA_AUTH_TOKEN']) ? filter_var($_SERVER['HTTP_X_CATALEYA_AUTH_TOKEN'], FILTER_VALIDATE_REGEXP, array('options'=>array('regexp' => '/^[A-Za-z0-9,\-]{1,100}$/'))) : '';
            }

            return $_t;
        }
        
        
        
        
        
        
        /*
         * 
         * [ getConfirmToken  ]
         * 
         * 
         */
        
        static public function getConfirmToken () 
        {
            static $_t = NULL;
            if (empty($_t) && !is_string($_t)) 
            {
                 $_t = isset($_POST['confirm_token']) ? filter_var($_POST['confirm_token'], FILTER_SANITIZE_STRIPPED) : '';
            }

            return $_t;
        }
        
      
    
        
        
       
        /*
         * 
         * [ getWhisper  ]
         * 
         * 
         */
        
        static public function getWhisper () 
        {
            static $_t = NULL;
            if (empty($_t) && !is_string($_t)) 
            {
                 $_t = isset($_POST['whisper']) ? filter_var($_POST['whisper'], FILTER_SANITIZE_STRIPPED) : '';
            }

            return $_t;
        }
        
        
        

        /*
         * 
         * [ generateToken ]
         * 
         * 
         */

        static public function generateToken ($delimiter = '-')
        {
            return (md5( uniqid(rand(), TRUE) ) . $delimiter . CURRENT_DT);
        }
        
        
        
    

        
        /*
         * [ digest ]
         * 
         */


        static public function digest ($salt = '', $timestamp = FALSE) 
        {
            if ($salt === '') 
            {
                $_keys = array ('hushpuppys', 'sagiterrian', 'olodo', 'agbaya', 'dondeymad', 'goshitinyourpants', 'megalomeniac', 'nacciruomoalata');        
                $salt = $_keys[rand(0, count($_keys)-1)];

            }
            return (sha1(strval(rand(0,microtime(true))) . $salt . strval(microtime(true)))) . (($timestamp) ? '-' . CURRENT_DT : '');
        }
        
        
        
        /*
         * [ uid ]
         * 
         */


        static public function uid () 
        {
            return sha1( uniqid(rand(), TRUE));
        }
        
        
        static public function jsrand () 
        {
            
            return mt_rand()/(mt_getrandmax()+1);
        }

        



        /**
       * uuid
       *
       * @param integer $len
       * @param integer $radix
       * @return string
       */
      static public function uuid ($len, $radix = null) {
        // Private array of chars to use
        $chars = str_split('0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz');
        $uuid = []; $i;
        $radix = !empty($radix) ? $radix : count($chars);

        if ($len) {
          // Compact form
          for ($i = 0; $i < $len; $i++) $uuid[$i] = $chars[0 | self::jsrand()*$radix]; 
          
        } else {
          // rfc4122, version 4 form
          $r;

          // rfc4122 requires these characters
          $uuid[8] = $uuid[13] = $uuid[18] = $uuid[23] = '-';
          $uuid[14] = '4';

          // Fill in random data.  At i==19 set the high bits of clock sequence as
          // per rfc4122, sec. 4.1.5
          for ($i = 0; $i < 36; $i++) {
            if (!$uuid[$i]) {
              $r = 0 | self::jsrand()*16;
              $uuid[$i] = $chars[($i == 19) ? ($r & 0x3) | 0x8 : $r];
            }
          }
        }

        return implode('', $uuid);
      }





        
        /*
         * 
         * [ generatePassword ]
         * 
         */
        
        static public function generatePassword ($length = 8)
         {

           // start with a blank password
           $password = "";

           // define possible characters - any character in this string can be
           // picked for use in the password, so if you want to put vowels back in
           // or add special characters such as exclamation marks, this is where
           // you should do it
           $possible = "2346789bcdfghjkmnpqrtvwxyzBCDFGHJKLMNPQRTVWXYZ";

           // we refer to the length of $possible a few times, so let's grab it now
           $maxlength = strlen($possible);

           // check for length overflow and truncate if necessary
           if ($length > $maxlength) {
             $length = $maxlength;
           }

           // set up a counter for how many characters are in the password so far
           $i = 0; 

           // add random characters to $password until $length is reached
           while ($i < $length) { 

             // pick a random character from the possible ones
             $char = substr($possible, mt_rand(0, $maxlength-1), 1);

             // have we already used this character in $password?
             if (!strstr($password, $char)) { 
               // no, so it's OK to add it onto the end of whatever we've already got...
               $password .= $char;
               // ... and increase the counter by one
               $i++;
             }

           }

           // done!
           return $password;

         }
         
         
         
         


	/**
	 * 
	 * Sanitizes title, replacing whitespace and a few other characters with dashes.
	 *
	 * Limits the output to alphanumeric characters, underscore (_) and dash (-).
	 * Whitespace becomes a dash.
	 *
	 *
	 * @param string $title The title to be sanitized.
	 * @param string $raw_title Optional. Not used.
	 * @param string $context Optional. The operation for which the string is sanitized.
	 * @return string The sanitized title.
	 */
	static public function sanitizeTitleWithDashes($title, $raw_title = '', $context = 'display') {
		$title = strip_tags($title);
		// Preserve escaped octets.
		$title = preg_replace('|%([a-fA-F0-9][a-fA-F0-9])|', '---$1---', $title);
		// Remove percent signs that are not part of an octet.
		$title = str_replace('%', '', $title);
		// Restore octets.
		$title = preg_replace('|---([a-fA-F0-9][a-fA-F0-9])---|', '%$1', $title);
	
		if (seems_utf8($title)) {
			if (function_exists('mb_strtolower')) {
				$title = mb_strtolower($title, 'UTF-8');
			}
			$title = utf8_uri_encode($title, 200);
		}
	
		$title = strtolower($title);
		$title = preg_replace('/&.+?;/', '', $title); // kill entities
		$title = str_replace('.', '-', $title);
	
		if ( 'save' == $context ) {
			// Convert nbsp, ndash and mdash to hyphens
			$title = str_replace( array( '%c2%a0', '%e2%80%93', '%e2%80%94' ), '-', $title );
	
			// Strip these characters entirely
			$title = str_replace( array(
				// iexcl and iquest
				'%c2%a1', '%c2%bf',
				// angle quotes
				'%c2%ab', '%c2%bb', '%e2%80%b9', '%e2%80%ba',
				// curly quotes
				'%e2%80%98', '%e2%80%99', '%e2%80%9c', '%e2%80%9d',
				'%e2%80%9a', '%e2%80%9b', '%e2%80%9e', '%e2%80%9f',
				// copy, reg, deg, hellip and trade
				'%c2%a9', '%c2%ae', '%c2%b0', '%e2%80%a6', '%e2%84%a2',
			), '', $title );
	
			// Convert times to x
			$title = str_replace( '%c3%97', 'x', $title );
		}
	
		$title = preg_replace('/[^%a-z0-9 _-]/', '', $title);
		$title = preg_replace('/\s+/', '-', $title);
		$title = preg_replace('|-+|', '-', $title);
		$title = trim($title, '-');
	
		return $title;
	}
        
        
         
 

        
        
        /*
         * 
         * [ getDateTime ]
         * 
         */
        
        
        static public function getDateTime () 
        {
            return date('Y-m-d H:i:s');
        }
        
        
        
        /*
         * 
         * [ toReadableDateTime ]
         * 
         */
        
        
        static public function toReadableDateTime ($_dateTimeString = '') 
        {
            if (empty($_dateTimeString) || !is_string($_dateTimeString)) return '';

            return date('D, d M Y H:i', strtotime($_dateTimeString));
        }
        
        
        
        
        
        /*
         * 
         * [ getOrdinal ]
         * 
         */
        
        
        static public function getOrdinal ($_num = '', $_make = FALSE) 
        {
            if (filter_var($_num, FILTER_VALIDATE_FLOAT) === FALSE);
            
            $_num_str = strval((int)$_num);
            $_last_digit = intval(substr($_num_str, -1));
            
            $_ordinals = array ('th', 'st', 'nd', 'rd', 'th', 'th', 'th', 'th', 'th', 'th');

            return ($_make === TRUE) ? $_num_str.$_ordinals[$_last_digit] : $_ordinals[$_last_digit];
        }
        
        
        
        /*
         * 
         * [ getNth ]
         * _________________________________________________________
         * 
         * Alias for [getOrdinal]
         * 
         */
        
        
        static public function getNth ($_num = '', $_make = FALSE) 
        {
            return getOrdinal($_num, $_make);
        }
        
        
        
        
        
        /*
         * 
         * [ getFileSizeUnit ]
         * _________________________________________________________
         * 
         * Function to append proper Unit after file-size
         * 
         */
        
        static public function getFileSizeUnit($file_size)
        {
            
            switch (true) {
                case ($file_size/1024 < 1) :
                    return intval($file_size ) ." Bytes" ;
                    break;
                case ($file_size/1024 >= 1 && $file_size/(1024*1024) < 1)  :
                    return intval($file_size/1024) ." KB" ;
                    break;
                    default:
                    return intval($file_size/(1024*1024)) ." MB" ;
            }
        }
        
        
        
        
        

        static public function CartesianProduct($arrays = array (), $i = 0) {
            if (!isset($arrays[$i])) {
                return array();
            }
            if ($i == count($arrays) - 1) {
                // return $arrays[$i];
                
                $result = array ();
                foreach ($arrays[$i] as $v) $result[] = [$v];
                
                return $result;
            }

            // get combinations from subsequent arrays
            $tmp = self::CartesianProduct($arrays, $i + 1);

            $result = array();

            // concat each array from tmp with each element from $arrays[$i]
            foreach ($arrays[$i] as $v) {
                foreach ($tmp as $t) {
                    $result[] = is_array($t) ? 
                        array_merge(array($v), $t) :
                        array($v, $t);
                }
            }

            return $result;
        }





        /*
         * 
         * 
         * [ getIP ]
         * 
         * 
         */

        public static function getIP() {
            if (isset($_SERVER)) {
                if (isset($_SERVER["HTTP_X_FORWARDED_FOR"])) {
                    $realip = $_SERVER["HTTP_X_FORWARDED_FOR"];
                } elseif (isset($_SERVER["HTTP_CLIENT_IP"])) {
                    $realip = $_SERVER["HTTP_CLIENT_IP"];
                } else {
                    $realip = $_SERVER["REMOTE_ADDR"];
                }
            } else {
                if ( getenv( 'HTTP_X_FORWARDED_FOR' ) ) {
                    $realip = getenv( 'HTTP_X_FORWARDED_FOR' );
                } elseif ( getenv( 'HTTP_CLIENT_IP' ) ) {
                    $realip = getenv( 'HTTP_CLIENT_IP' );
                } else {
                    $realip = getenv( 'REMOTE_ADDR' );
                }
            }
            return $realip;
        }
        
        
        
        /*
         * 
         * STOP WATCH
         * 
         */
        
        

        private static $_clock = 0;
        private static $_clocks = array();

        public static function startTimer($_id = NULL) 
        {

            $_id = filter_var($_id, FILTER_VALIDATE_REGEXP, array('options'=>array('regexp' => '/^([A-Za-z0-9\-_]{1,50})$/')));
            
            if ( $_id === FALSE ) 
            {

                self::$_clock = microtime(TRUE);

            } else {

                $_id = trim($_id);
                self::$_clocks[$_id] = microtime(TRUE);

            }

            return 0;

        }





        public static function stopTimer($_id = NULL, $_seconds = TRUE)
        {

            $_id = filter_var($_id, FILTER_VALIDATE_REGEXP, array('options'=>array('regexp' => '/^([A-Za-z0-9\-_]{1,50})$/')));

            if ( $_id !== FALSE  && isset(self::$_clocks[$_id])) 
            {

                $_duration = microtime(TRUE) - self::$_clocks[$_id];
                unset(self::$_clocks[$_id]);

            } else if (self::$_clock > 0) {

                $_duration = microtime(TRUE) - self::$_clock;
                self::$_clock = 0;

            } else {

            return 0;

            }
            
            
            
            return round($_duration, 7); // ($_seconds) ? round(($_duration/1000), 7) : round($_duration, 7);


        }
        
        
        public static function getClassList ($_implements = null) 
        {

            static $_list = [];
            
            $_result =[];
            
            if (empty($_list)) 
            {
                

                $_list = [];
                $path = ROOT_PATH.'/app/core/';
                $iterator = new \RecursiveIteratorIterator(
                    new \RecursiveDirectoryIterator($path, \RecursiveDirectoryIterator::SKIP_DOTS),
                    \RecursiveIteratorIterator::SELF_FIRST
                        );


                foreach ($iterator as $item) {


                    // Note SELF_FIRST, so array keys are in place before values are pushed.

                        $subPath = $iterator->getSubPathName();

                        if (!$item->isDir() && preg_match('/[^(rename_bot)]*(.php)$/i', $subPath) > 0) {

                            // Add a new element to the array of the current file name.
                            $subPath = str_replace("/", '\\', $subPath);
                            $subPath = preg_replace("/.php$/e", '', $subPath);

                            $classname = '\\'.$subPath;
                            if (class_exists($classname)) $_list[] = $classname;

                            
                        }

                }
                
                
            }
            
            if ($_implements !== null) {

                foreach ($_list as $_c) 
                {

                    $interfaces = class_implements($_c);
                    if($interfaces && in_array($_implements, $interfaces)) {
                        $_result[] = $_c;
                    }

                }
            } else { $_result = $_list; }

            return $_result;

        }










        /*
         * 
         *  CONFIG HANDLERS (file config)
         * 
         * 
         */
        
        

        // LOAD CONFIG
        
        public static function loadConfig($_id = NULL) 
        {


            $_id = filter_var($_id, FILTER_VALIDATE_REGEXP, array('options'=>array('regexp' => '/^([A-Za-z0-9\-_]{1,50})$/')));

            if ( $_id === FALSE ) return NULL;

            $_path =  ROOT_PATH . 'app/etc/' . $_id . '.dat';

            // check if file exists
            if (file_exists($_path)) 
            {

                // read and decrypt
                $_data = \Cataleya\Helper\Bcrypt::decrypt(file_get_contents($_path));

                // decode
                $_config = json_decode($_data);
                if ($_config === FALSE) return NULL;
                
                return $_config;

            } else { 

                return NULL;

            }


        }




        // WRITE CONFIG
        
        public static function writeConfig(array $_config, $_id = NULL) 
        {

                $_id = filter_var($_id, FILTER_VALIDATE_REGEXP, array('options'=>array('regexp' => '/^([A-Za-z0-9\-_]{1,50})$/')));

                if ( $_id === FALSE ) throw new Error ('Invalid config id');

                if (  empty($_config) ) throw new Error ('Empty config array');


                // encode and encrypt
                $_data = \Cataleya\Helper\Bcrypt::encrypt(json_encode($_config));

                // write
                $_path =  ROOT_PATH . 'app/etc/' . $_id . '.dat';
                

                if( @file_put_contents($_path, $_data) === FALSE) throw new Error ('Config data could not be written to file');

                return TRUE;


        }
        
        
        
        
        
        /*
         * 
         *  CSV READER
         * 
         * 
         */
        
        

        // GET CSV
        
        public static function getCSV($_id = NULL, $_delimiter = ',', $_enclosure = '"', $_escape = '\\') 
        {

            if (file_exists($_id)) {
                $_path = $_id;
            } else {
                
                $_id = filter_var($_id, FILTER_VALIDATE_REGEXP, array('options'=>array('regexp' => '/^([A-Za-z0-9\-_]{1,50})(\.csv|\.CSV)?$/')));
                if ( $_id === FALSE ) { return NULL; }
                
                $_path = ROOT_PATH . 'app/etc/' . $_id . '.csv';
            
            }

            
            

            $_DATA = array ();
            
            

            // check if file exists
            if (file_exists($_path)) 
            {

                $CSV_HANDLE = fopen($_path, "r");
                $CSV_COL_NAMES = fgetcsv($CSV_HANDLE, 1024, $_delimiter, $_enclosure, $_escape);

                $row_count = 0;
                while ($CSV_ROW = fgetcsv($CSV_HANDLE, 1024, $_delimiter, $_enclosure, $_escape)) {
                        $_DATA[$row_count] = array();
                        
                        for ($i=0; $i < count($CSV_ROW); $i++) {
                                $_DATA[$row_count][$CSV_COL_NAMES[$i]] = $CSV_ROW[$i];
                        }

                        $row_count++;

                }
                
                
                fclose($CSV_HANDLE);

            } 
                
            return $_DATA;


        }
        
        
        
        
        


        // PUT CSV
        
        public static function putCSV($_id = NULL, array $_data = array(), $_delimiter = ',', $_enclosure = '"') 
        {

                $_id = filter_var($_id, FILTER_VALIDATE_REGEXP, array('options'=>array('regexp' => '/^([A-Za-z0-9\-_]{1,50})$/')));

                if ( $_id === FALSE ) throw new Error ('Invalid csv id');

                if (  empty($_data) ) throw new Error ('Empty array');


                // write
                $_path =  ROOT_PATH . 'app/etc/' . $_id . '.csv';
                $CSV_HANDLE = fopen($_path, "w");
                
                // column names
                fputcsv($CSV_HANDLE, array_keys($_data[0]), $_delimiter, $_enclosure);
                
                // rows
                foreach ($_data as $_row) fputcsv($CSV_HANDLE, array_values($_row), $_delimiter, $_enclosure);
                
                
                fclose($CSV_HANDLE);
                
                return TRUE;


        }
        
        
        



        /*
         * [ readFolder ]
         * ___________________________________________
         * 
         * Read contents of a folder and return filenames in an array.
         * 
         * 
         */


        public static function readFolder ($_PATH = NULL, $_FILTER = '*') 
        {
            if (!is_string($_FILTER) || empty($_PATH) || !is_string($_PATH) || !file_exists($_PATH)) return array ();


            $_FILTER = preg_replace('/([^a-zA-Z0-9-_\|]+)/i', '', $_FILTER);
            if (strlen($_FILTER) > 20) return array ();

            if (empty($_FILTER)) $_FILTER = '*';



            $_files = array ();
            $dh  = scandir($_PATH, 0);

            foreach ($dh as $filename) {
                if (preg_match('/^([a-zA-Z0-9-_]+)\.(' . $_FILTER . ')$/i', $filename)) $_files[] = $filename;
            }


            return  empty ($_files) ? array () : $_files;
        }


        
        
        
        

        
        
        
        
        
        static function getTimeZones () 
        {
            static $_timezones = 
            
                    array (
                      '(GMT-12:00) International Date Line West' => 'Pacific/Wake',
                      '(GMT-11:00) Midway Island' => 'Pacific/Apia',
                      '(GMT-11:00) Samoa' => 'Pacific/Apia',
                      '(GMT-10:00) Hawaii' => 'Pacific/Honolulu',
                      '(GMT-09:00) Alaska' => 'America/Anchorage',
                      '(GMT-08:00) Pacific Time (US &amp; Canada); Tijuana' => 'America/Los_Angeles',
                      '(GMT-07:00) Arizona' => 'America/Phoenix',
                      '(GMT-07:00) Chihuahua' => 'America/Chihuahua',
                      '(GMT-07:00) La Paz' => 'America/Chihuahua',
                      '(GMT-07:00) Mazatlan' => 'America/Chihuahua',
                      '(GMT-07:00) Mountain Time (US &amp; Canada)' => 'America/Denver',
                      '(GMT-06:00) Central America' => 'America/Managua',
                      '(GMT-06:00) Central Time (US &amp; Canada)' => 'America/Chicago',
                      '(GMT-06:00) Guadalajara' => 'America/Mexico_City',
                      '(GMT-06:00) Mexico City' => 'America/Mexico_City',
                      '(GMT-06:00) Monterrey' => 'America/Mexico_City',
                      '(GMT-06:00) Saskatchewan' => 'America/Regina',
                      '(GMT-05:00) Bogota' => 'America/Bogota',
                      '(GMT-05:00) Eastern Time (US &amp; Canada)' => 'America/New_York',
                      '(GMT-05:00) Indiana (East)' => 'America/Indiana/Indianapolis',
                      '(GMT-05:00) Lima' => 'America/Bogota',
                      '(GMT-05:00) Quito' => 'America/Bogota',
                      '(GMT-04:00) Atlantic Time (Canada)' => 'America/Halifax',
                      '(GMT-04:00) Caracas' => 'America/Caracas',
                      '(GMT-04:00) La Paz' => 'America/Caracas',
                      '(GMT-04:00) Santiago' => 'America/Santiago',
                      '(GMT-03:30) Newfoundland' => 'America/St_Johns',
                      '(GMT-03:00) Brasilia' => 'America/Sao_Paulo',
                      '(GMT-03:00) Buenos Aires' => 'America/Argentina/Buenos_Aires',
                      '(GMT-03:00) Georgetown' => 'America/Argentina/Buenos_Aires',
                      '(GMT-03:00) Greenland' => 'America/Godthab',
                      '(GMT-02:00) Mid-Atlantic' => 'America/Noronha',
                      '(GMT-01:00) Azores' => 'Atlantic/Azores',
                      '(GMT-01:00) Cape Verde Is.' => 'Atlantic/Cape_Verde',
                      '(GMT) Casablanca' => 'Africa/Casablanca',
                      '(GMT) Edinburgh' => 'Europe/London',
                      '(GMT) Greenwich Mean Time : Dublin' => 'Europe/London',
                      '(GMT) Lisbon' => 'Europe/London',
                      '(GMT) London' => 'Europe/London',
                      '(GMT) Monrovia' => 'Africa/Casablanca',
                      '(GMT+01:00) Amsterdam' => 'Europe/Berlin',
                      '(GMT+01:00) Belgrade' => 'Europe/Belgrade',
                      '(GMT+01:00) Berlin' => 'Europe/Berlin',
                      '(GMT+01:00) Bern' => 'Europe/Berlin',
                      '(GMT+01:00) Bratislava' => 'Europe/Belgrade',
                      '(GMT+01:00) Brussels' => 'Europe/Paris',
                      '(GMT+01:00) Budapest' => 'Europe/Belgrade',
                      '(GMT+01:00) Copenhagen' => 'Europe/Paris',
                      '(GMT+01:00) Ljubljana' => 'Europe/Belgrade',
                      '(GMT+01:00) Madrid' => 'Europe/Paris',
                      '(GMT+01:00) Paris' => 'Europe/Paris',
                      '(GMT+01:00) Prague' => 'Europe/Belgrade',
                      '(GMT+01:00) Rome' => 'Europe/Berlin',
                      '(GMT+01:00) Sarajevo' => 'Europe/Sarajevo',
                      '(GMT+01:00) Skopje' => 'Europe/Sarajevo',
                      '(GMT+01:00) Stockholm' => 'Europe/Berlin',
                      '(GMT+01:00) Vienna' => 'Europe/Berlin',
                      '(GMT+01:00) Warsaw' => 'Europe/Sarajevo',
                      '(GMT+01:00) West Central Africa' => 'Africa/Lagos',
                      '(GMT+01:00) Zagreb' => 'Europe/Sarajevo',
                      '(GMT+02:00) Athens' => 'Europe/Istanbul',
                      '(GMT+02:00) Bucharest' => 'Europe/Bucharest',
                      '(GMT+02:00) Cairo' => 'Africa/Cairo',
                      '(GMT+02:00) Harare' => 'Africa/Johannesburg',
                      '(GMT+02:00) Helsinki' => 'Europe/Helsinki',
                      '(GMT+02:00) Istanbul' => 'Europe/Istanbul',
                      '(GMT+02:00) Jerusalem' => 'Asia/Jerusalem',
                      '(GMT+02:00) Kyiv' => 'Europe/Helsinki',
                      '(GMT+02:00) Minsk' => 'Europe/Istanbul',
                      '(GMT+02:00) Pretoria' => 'Africa/Johannesburg',
                      '(GMT+02:00) Riga' => 'Europe/Helsinki',
                      '(GMT+02:00) Sofia' => 'Europe/Helsinki',
                      '(GMT+02:00) Tallinn' => 'Europe/Helsinki',
                      '(GMT+02:00) Vilnius' => 'Europe/Helsinki',
                      '(GMT+03:00) Baghdad' => 'Asia/Baghdad',
                      '(GMT+03:00) Kuwait' => 'Asia/Riyadh',
                      '(GMT+03:00) Moscow' => 'Europe/Moscow',
                      '(GMT+03:00) Nairobi' => 'Africa/Nairobi',
                      '(GMT+03:00) Riyadh' => 'Asia/Riyadh',
                      '(GMT+03:00) St. Petersburg' => 'Europe/Moscow',
                      '(GMT+03:00) Volgograd' => 'Europe/Moscow',
                      '(GMT+03:30) Tehran' => 'Asia/Tehran',
                      '(GMT+04:00) Abu Dhabi' => 'Asia/Muscat',
                      '(GMT+04:00) Baku' => 'Asia/Tbilisi',
                      '(GMT+04:00) Muscat' => 'Asia/Muscat',
                      '(GMT+04:00) Tbilisi' => 'Asia/Tbilisi',
                      '(GMT+04:00) Yerevan' => 'Asia/Tbilisi',
                      '(GMT+04:30) Kabul' => 'Asia/Kabul',
                      '(GMT+05:00) Ekaterinburg' => 'Asia/Yekaterinburg',
                      '(GMT+05:00) Islamabad' => 'Asia/Karachi',
                      '(GMT+05:00) Karachi' => 'Asia/Karachi',
                      '(GMT+05:00) Tashkent' => 'Asia/Karachi',
                      '(GMT+05:30) Chennai' => 'Asia/Calcutta',
                      '(GMT+05:30) Kolkata' => 'Asia/Calcutta',
                      '(GMT+05:30) Mumbai' => 'Asia/Calcutta',
                      '(GMT+05:30) New Delhi' => 'Asia/Calcutta',
                      '(GMT+05:45) Kathmandu' => 'Asia/Katmandu',
                      '(GMT+06:00) Almaty' => 'Asia/Novosibirsk',
                      '(GMT+06:00) Astana' => 'Asia/Dhaka',
                      '(GMT+06:00) Dhaka' => 'Asia/Dhaka',
                      '(GMT+06:00) Novosibirsk' => 'Asia/Novosibirsk',
                      '(GMT+06:00) Sri Jayawardenepura' => 'Asia/Colombo',
                      '(GMT+06:30) Rangoon' => 'Asia/Rangoon',
                      '(GMT+07:00) Bangkok' => 'Asia/Bangkok',
                      '(GMT+07:00) Hanoi' => 'Asia/Bangkok',
                      '(GMT+07:00) Jakarta' => 'Asia/Bangkok',
                      '(GMT+07:00) Krasnoyarsk' => 'Asia/Krasnoyarsk',
                      '(GMT+08:00) Beijing' => 'Asia/Hong_Kong',
                      '(GMT+08:00) Chongqing' => 'Asia/Hong_Kong',
                      '(GMT+08:00) Hong Kong' => 'Asia/Hong_Kong',
                      '(GMT+08:00) Irkutsk' => 'Asia/Irkutsk',
                      '(GMT+08:00) Kuala Lumpur' => 'Asia/Singapore',
                      '(GMT+08:00) Perth' => 'Australia/Perth',
                      '(GMT+08:00) Singapore' => 'Asia/Singapore',
                      '(GMT+08:00) Taipei' => 'Asia/Taipei',
                      '(GMT+08:00) Ulaan Bataar' => 'Asia/Irkutsk',
                      '(GMT+08:00) Urumqi' => 'Asia/Hong_Kong',
                      '(GMT+09:00) Osaka' => 'Asia/Tokyo',
                      '(GMT+09:00) Sapporo' => 'Asia/Tokyo',
                      '(GMT+09:00) Seoul' => 'Asia/Seoul',
                      '(GMT+09:00) Tokyo' => 'Asia/Tokyo',
                      '(GMT+09:00) Yakutsk' => 'Asia/Yakutsk',
                      '(GMT+09:30) Adelaide' => 'Australia/Adelaide',
                      '(GMT+09:30) Darwin' => 'Australia/Darwin',
                      '(GMT+10:00) Brisbane' => 'Australia/Brisbane',
                      '(GMT+10:00) Canberra' => 'Australia/Sydney',
                      '(GMT+10:00) Guam' => 'Pacific/Guam',
                      '(GMT+10:00) Hobart' => 'Australia/Hobart',
                      '(GMT+10:00) Melbourne' => 'Australia/Sydney',
                      '(GMT+10:00) Port Moresby' => 'Pacific/Guam',
                      '(GMT+10:00) Sydney' => 'Australia/Sydney',
                      '(GMT+10:00) Vladivostok' => 'Asia/Vladivostok',
                      '(GMT+11:00) Magadan' => 'Asia/Magadan',
                      '(GMT+11:00) New Caledonia' => 'Asia/Magadan',
                      '(GMT+11:00) Solomon Is.' => 'Asia/Magadan',
                      '(GMT+12:00) Auckland' => 'Pacific/Auckland',
                      '(GMT+12:00) Fiji' => 'Pacific/Fiji',
                      '(GMT+12:00) Kamchatka' => 'Pacific/Fiji',
                      '(GMT+12:00) Marshall Is.' => 'Pacific/Fiji',
                      '(GMT+12:00) Wellington' => 'Pacific/Auckland',
                      '(GMT+13:00) Nuku\'alofa' => 'Pacific/Tongatapu',
                    );
            
            return $_timezones;

        }
        
        




        
        /*
         * 
         * [ echoPDF ]
         * ______________________________________________________________
         * 
         */
        
        
        public static function echoPDF ($_path, $_DOWNLOAD = FALSE) 
        {
            $_matches = array();
            
            if (is_string ($_path) && preg_match('!\/(?<filename>[^\\\|/]+)\.(?<ext>pdf)$!', $_path, $_matches) > 0 && file_exists($_path)) 
            {
                $_filename = $_matches['filename'] . '.' . $_matches['ext'];
                
                if ($_DOWNLOAD) 
                {
			header('Content-Type: application/x-download');
			header('Content-Disposition: attachment; filename="'.$_filename.'"');
			header('Cache-Control: private, max-age=0, must-revalidate');
			header('Pragma: public');
                }
                
                else {
                        header('Content-Type: application/pdf');
                        header('Content-Disposition: inline; filename="'.$_filename.'"');
                        header('Cache-Control: private, max-age=0, must-revalidate');
                        header('Pragma: public');
                }
                
                readfile($_path);
            }
        }
        
        
        
        
        
        
        
        
        
        
        // SCALAR VALUES
        
        public static function boolval ($val = NULL) {
            static 
            
            $true_array = array ('true', 'yes');
            $t = gettype($val);
            $v = false;
            
            switch ($t) {
                case 'string':
                    $val = trim($val);
                    $v = (in_array(strtolower($val), $true_array)) ? true : false;
                    break;
                
                case 'integer':
                    $v = (1 === (int)$val) ? true : false;
                    break;
                
                case 'boolean':
                    $v = (bool)$val;
                    break;
                
                default:
                    $v = NULL;
                    break;
            }
            
            return $v;
            
        } 


        

 
}


