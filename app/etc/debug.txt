
                    SELECT p.product_id, dc.title  
                    FROM products p 
                    INNER JOIN content_languages dc 
                    ON p.description_id = dc.content_id 
                    WHERE dc.title REGEXP :regex   
                    AND p.display_status   
                    AND p.featured 
                      
                    GROUP BY p.product_id  
                    ORDER BY p.product_id DESC LIMIT :limit