// Global Storefront Javascript Doc

window.cartMessengerTimeout = null;
window.pageMenuTimeout = null;
window.cartMessengerHoverTimeout = null;




$('document').ready(function () {
		 var grid = new hashgrid({
			 id: 'grid',            // id for the grid container
			 modifierKey: 'null',      // optional 'ctrl', 'alt' or 'shift'
			 showGridKey: 'g',        // key to show the grid
			 holdGridKey: 'h',    // key to hold the grid in place
			 foregroundKey: 'f',      // key to toggle foreground/background
			 jumpGridsKey: 'j',       // key to cycle through the grid classes
			 numberOfGrids: 2,        // number of grid classes used
			 classPrefix: 'hgrid-',    // prefix for the grid classes
			 cookiePrefix: 'aimas-hashgrid'   // prefix for the cookie name
		 });
                 
                 
                 

                // Initialize cart messenger
                $('#shopper-dashboard-bag')
                .mouseover(function () {
                    clearTimeout (window.cartMessengerTimeout, window.cartMessengerHoverTimeout);
                    window.cartMessengerHoverTimeout = setTimeout ("showMenu('#cart-messenger', '#shopper-dashboard-bag')", 300);
                    $("#cart-messenger").load($("#add-to-cart-form").prop('action'));
                })
                .mouseout(function () {
                       clearTimeout(window.cartMessengerHoverTimeout);
                       window.cartMessengerTimeout = setTimeout(function () {
                       hideMenu('#cart-messenger', '#shopper-dashboard-bag');

                    }, 300);
                });

                $('#cart-messenger').mouseover(function () {
                    clearTimeout (window.cartMessengerTimeout);
                })
                .mouseout(function () {
                   window.cartMessengerTimeout = setTimeout(function () {
                       hideMenu('#cart-messenger', '#shopper-dashboard-bag');
                   }, 300);
                });



		 
 
});



