
 
// ----------------------------------------------------------------------------------------- //


// Class: [ makeSlider ]
;(function (main) {   
    
    
    
    main.Node = function (selector, type) {

        
        type = (typeof type === 'string' && ['all', 'one', 'dom'].indexOf(type) > -1) ? type : 'all';
        
        
        
        // env...
        var 

        
        self = this, 
        available = {
            classList:  ("classList" in document.createElement("_"))
        }, 
        type_of = function (obj) {

        return ({}).toString.call(obj).match(/\s([a-zA-Z]+)/)[1].toLowerCase();
        }, 
        fade_css = {  
                        transition: 'all 0.5s ease-in-out', 
                        WebkitTransition: 'all 0.5s ease-in-out', 
                        MozTransition: 'all 0.5s ease-in-out'
                    }; 
        self.listeners = {};
        self.elements = [];
        
        
        
        
        
        // Select...
        if (selector !== null && typeof selector === 'object' && selector.nodeType) {
            self.elements.push(selector);
        }
        
        else if (typeof selector === 'string') {

            selector = selector.trim();

            if (type === 'dom' || (selector.charAt(0) === "<" && selector.charAt( selector.length - 1 ) === ">" && selector.length >= 3) ) {
                self.elements = toDOM(selector);
            }

            else if (type === 'all') {
                selector = document.querySelectorAll(selector);

                for (var _i=0, _l=selector.length; _i < _l; _i++) { self.elements.push(selector[_i]); }
            }
            else if (type === 'one') { 
                var element = document.querySelector(selector);
                if (element) self.elements.push(element);
            }

        }
        
        else {

            try {
                if (selector.length > 0 && typeof selector[0] === 'object' && selector[0].nodeType) {

                    for (var _i=0, _l=selector.length; _i < _l; _i++) { 
                        if (typeof selector[_i] === 'object' && selector[_i].nodeType) self.elements.push(selector[_i]); 
                    }

                }
            } catch (e) {
                // do nothing
            }

        }



        var 

        rspace = /\s+/, 
        rline = /[\t\r\n]/g;


            function esc (s) {
             return s.replace(/([.?*+^$[\]\\(){}|-])/g, "\\$1");
            }

            function toDOM (html, return_fragment) {
                
                return_fragment = (typeof return_fragment === 'boolean' && return_fragment === true);
              
                var 

                d=document, 
                i, 
                wrap = d.createElement("div");

                wrap.innerHTML = html;

                // This is sufficient for "<a href="#">link</a>"... (single element)
                //if (!return_fragment) return wrap.firstChild;
                
                
                // Use doc fragment in multiple children scenerio like: "<li>a</li><li>b</li>"...
                var b = d.createDocumentFragment(), c = [];
                while(i = wrap.firstChild) c.push(b.appendChild(i));

                return return_fragment ? b : c;
                
            }




            function _hasClass (el, c) {

                    return (available.classList) 
                    ? el.classList.contains(c)
                    : ((" " + el.className + " ").replace(rline, " ").indexOf(c) > -1);

                }

            function _removeClass (el, c) {
                    var 

                    cStr = (" " + el.className + " ").replace(rline, " "), 
                    cList = (c || "").split(rspace);


                    for (var i2=0, l2=cList.length; i2 < l2; i2++) {

                        while ((" " + cStr + " ").indexOf(cList[i2]) > -1) {
                            cStr = (" " + cStr + " ").replace(cList[i2], " ");
                        }
                    }

                    el.className = cStr.trim().replace("  ", " ");
            }


            function _addClass (el, c) {
                    if (!_hasClass(el, c)) el.className += ' ' + c; 
            }






            //self.toDOM = toDOM;


            self.forEach = function(cb) {
                if (typeof cb !== 'function') throw new TypeError ('Argument must be a function.');

                this.elements.forEach(function(obj, i) {
                  cb.apply(obj, [obj, i]);
                });

            },

            self.item = function (i) {
                return this.elements[i];
            }, 


            /*=========  CLASS LIST ==================== */

            self.addClass = function (c) {

                if ((available.classList) ) this.forEach(function () { this.classList.add(c); });
                else this.forEach(function () { _addClass(this, c); });

                return this;

            },

            self.removeClass = function (c) {

                if ((available.classList) ) this.forEach(function () { this.classList.remove(c); });
                else this.forEach(function () { _removeClass(this, c); });

                return this;

            },

            self.toggleClass = function (c) {
                if ((available.classList) ) this.forEach(function () { this.classList.toggle(c); });
                else this.forEach(function () { _hasClass(this, c) ? _removeClass(this, c) : _addClass(this, c); });
                return this;
            },

            self.hasClass = function (c) {

               for (var i=0, l=this.elements.length; i < l; i++) {
                        if (_hasClass(this.elements[i], c)) { 
                            return true;
                            break;
                        }
                }

                return false;

            }, 



            /* ============= HTML ========= */

            self.html = function (html) {

                this.elements.forEach(function(obj) { obj.innerHTML = html; });
                
                return this;

            },
            self.appendChild = function (n, _return_child) {

                if (typeof n === 'string') n = toDOM(n);
                // if (typeof n === 'function' && n instanceof self) n = n.item(0);
                //if ((typeof n === 'object' || typeof n === 'function') && typeof n.html === 'function') n = n.item(0);
                if (n.nodeType) n = [n];
                
                

                var child = [];

                for (var i=0, l=this.elements.length; i < l; i++) {
                    n.forEach(function (o) {
                        child.push(self.elements[i].appendChild(o));
                    });
                    
                }
                return (typeof _return_child === 'boolean' && _return_child === true) ? new self.constructor(child) : this;
            },

            self.prependChild = function (n, _return_child) {

                if (typeof n === 'string') n = toDOM(n, true);
                var child;

                for (var i=0, l=this.elements.length; i < l; i++) {
                    child = this.elements[i].insertBefore(n, this.elements[i].firstChild);
                }

                return (typeof _return_child === 'boolean' && _return_child === true) ? new self.constructor(child) : this;
            },

            self.remove = function () {
                for (var i=0, l=this.elements.length; i < l; i++) {
                    this.elements[i].parentNode.removeChild(this.elements[i]);
                }

                return this;
            },


            self.querySelectorAll = function (q) {
                var list = [];

                for (var i=0, l=this.elements.length; i < l; i++) {
                    var nodes = this.elements[i].querySelectorAll(q);

                    for (var k in nodes) list.push(nodes[k]);
                }


                return list;
            },


            self.querySelector = function (q) {


                return new self.constructor(this.item(0).querySelector(q));

            },


            /* ======== CSS ============= */

            self.css = function (css) {
                for (var i=0, l=this.elements.length; i < l; i++) {

                    for (var k in css) {
                        this.elements[i].style[k] = css[k];

                    }

                    //console.log(this.elements[i]);

                }

                return this;

            }, 


            self.hide = function () {
                for (var i=0, l=this.elements.length; i < l; i++) {

                    this.elements[i].style.display = 'none';

                }

                return this;

            }, 

             self.show = function (display) {
                display = display || "block";

                self.css({ opacity: '1', display: display, visibility: 'visible' });

                return this;

            }, 
                    
            self.isVisible = function () {
                for (var i=0, l=this.elements.length; i < l; i++) {
                    var s = window.getComputedStyle(this.elements[i]);
                    
                    if (
                        s.display !== 'none' && 
                        s.visibility !== 'hidden' && 
                        parseFloat(s.opacity) > 0 
                        ) { return true; break; }

                }

                return false;

            }, 


           /* ============= EVENT LISTENERS ============ */


           self.addEventListener = function (_event_type, fn, useCapture) {

                if (typeof useCapture !== 'boolean') useCapture = false; 

                for (var i=0, l=this.elements.length; i < l; i++) {
                    this.elements[i].addEventListener(_event_type, fn, useCapture);

                }

                if (_event_type in this.listeners) { 

                    var _in = false;

                    for (var i3=0, l3=this.listeners[_event_type].length; i3<l3; i3++) {
                        if (this.listeners[_event_type][l3] === fn) { _in = true; break; }
                    }

                    if (!_in) this.listeners[_event_type].push(fn);
                }
                else this.listeners[_event_type] = [fn];

                return this;
           },

           self.removeEventListener = function (_event_type, fn, useCapture) {

                if (_event_type in this.listeners && typeof this.listeners[_event_type] !== 'undefined') {

                    if (typeof useCapture !== 'boolean') useCapture = false;

                    for (var i=0, l=this.elements.length; i < l; i++) { this.elements[i].removeEventListener( _event_type, fn, useCapture); }

                    for (var i3=0, l3=this.listeners[_event_type].length; i3<l3; i3++) {
                        if (this.listeners[_event_type][l3] === fn) { this.listeners[_event_type].splice(l3, 1); }
                    }


                }
                return this;
           },





           self.clearEventListeners = function (_event_type) {

                if (_event_type in this.listeners && typeof this.listeners[_event_type] !== 'undefined') {

                    var useCapture = false;


                    for (var i=0, l=this.elements.length; i < l; i++) {
                        for (var i2=0, l2=this.listeners[_event_type].length; i2<l2; i2++) {
                            this.elements[i].removeEventListener( _event_type, this.listeners[_event_type][l2], useCapture);
                        }
                    }

                    this.listeners[_event_type] = undefined;

                }

                return this;
           },


           // Aliases...

           self.on = function (_event_type, fn, useCapture) {

                return self.addEventListener(_event_type, fn, useCapture);
           },

           self.off = function (_event_type, fn, useCapture) {

                return self.removeEventListener( _event_type, fn, useCapture);
           },




            /* =========== EFX ========== */

            self.fadeIn = function (display, callBack){

                /*
                this.css({
                        opacity: '0', 
                        transition: 'all 0.2s ease-in-out', 
                        WebkitTransition: 'all 0.2s ease-in-out', 
                        MozTransition: 'all 0.2s ease-in-out'
                    })
                .css({opacity: '1' });

                setTimeout(function () { 
                    self.css({ display: display });
                    if (typeof callBack === 'function') callBack(self);

                }, 700);

                return this;
                */

                var elements = this.elements;

                elements[0].style.opacity = 0;
                elements[0].style.display = display || "block";
                elements[0].style.visibility = "visible";



                (function fade() {
                  var val = parseFloat(elements[0].style.opacity);
                  if (!((val += .1) > .9)) {
                    elements[0].style.opacity = val;
                    (typeof requestAnimationFrame !== 'undefined') ? requestAnimationFrame(fade) : setTimeout(fade, 50);
                  } else {
                      elements[0].style.opacity = 1;
                      if (typeof callBack === 'function') setTimeout(function () { callBack(self); }, 70);
                  }
                })();

                return this;
            }, 


            self.fadeOut = function (display, callBack){
                /* this.css({
                        //opacity: '0', 
                        transition: 'all 0.2s ease-in-out', 
                        WebkitTransition: 'all 0.2s ease-in-out', 
                        MozTransition: 'all 0.2s ease-in-out'
                    })
                .css({opacity: '0' });

                setTimeout(function () { 
                    self.css({display: "none" });
                    if (typeof callBack === 'function') callBack(self);

                }, 700);

                return this;
                */

                var elements = this.elements;

                elements[0].style.opacity = 1;
                elements[0].style.display = display || "block";
                //elements[0].style.visibility = "visible";

                (function fade() {
                  var val = parseFloat(elements[0].style.opacity);
                  if (!((val -= .1) < 0.1)) {
                    elements[0].style.opacity = val;
                    (typeof requestAnimationFrame !== 'undefined') ? requestAnimationFrame(fade) : setTimeout(fade, 50);
                  } else {
                      elements[0].style.opacity = 0;
                      elements[0].style.display = 'none';
                      if (typeof callBack === 'function') setTimeout(function () { callBack(self); }, 70);
                  }
                })();

                return this;
            };



        return this;

    };
    
    
    main.all = function (element) {
        return new main.Node (element, 'all');
    };
    
    main.one = function (element) {
        return new main.Node (element, 'one');
    };
    
    main.dom = function (element) {
        return new main.Node (element, 'dom');
    };
 
    return main;
 
}(Utils));
