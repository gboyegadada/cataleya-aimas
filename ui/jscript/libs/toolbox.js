var init = Array ();
var errorCallBackCue = Array ();
window.confirmToken = '';
window.whisper = '';



/*
 *
 * APPEND DIALOG BOXES
 * 
 */
 
 
 $('document').ready(function (){
    
    // Alert Bubble
    $('body')
    .prepend('<div id="alert-box" class="alert-box" style="">&nbsp;</div>')    
    
    // Tip Bubble (used in form validation)
    .prepend('<div id="bubble-1" onclick="hideTip()" class="message-bubble message-bubble-above"><span class="bubble-arrow">&nbsp;</span><p></p>&nbsp;</div>')
   
    // Confirm Bubble
    .prepend('<div id="confirm-box" class="alert-box" style="display:none; position:fixed; top:140px; left:490px; width:300px; height:auto; background-color:#111; color:#bbb; z-index:10001;"><h2 class="bubble-title">Hi,</h2><p class="bubble-text"></p><a class="button-1" id="confirm-button-ok" href="#" onClick="return false;" >confirm</a>&nbsp;&nbsp;<a class="button-1" id="confirm-button-cancel" href="#" onClick="return false;" >cancel</a><small class="bubble-tiny-text">click anywhere to close X</small></div>')

    // Tranlucent background
    .prepend('<div id="fullscreen-bg2">');

    window.alert = prettyAlert;
    window.confirm = prettyConfirm;
    window.originalTipBoxZIndex = $('#bubble-1').css('z-index');
    


});
 





function hideMenu (menu, menuButton)
{
    
    $(menu).fadeOut(300); 
    
    return false;
}


function showMenu (menu, menuButton)
{
    $(menu).slideDown('fast'); 
    
    return false;
}
 
 
 

/*

CUSTOM ALERT ...


*/




function prettyAlert (title, message) {
    
        release(); // In case user was previoulsy on hold...
        
	$('#alert-box, #fullscreen-bg2').unbind('click').click(function () {
                
		$('#alert-box, #fullscreen-bg2').fadeOut(300);
		$('#alert-box, #fullscreen-bg2').unbind('click');
                $(document).unbind('scroll'); 
 		$('body').css({'overflow':'visible'});	

	});
        

	
        message = sanitizeDialogText(message);
        title = sanitizeDialogText(title);
        if (message == '' || message == 'undefined') {
            message = title;
            title = 'Alert';
        }
	
	
	$('#alert-box').html('<h2>'+title+'</h2><p>'+message+'</p><small>click anywhere to close X</small>');
	$('#alert-box, #fullscreen-bg2').show();
	
	// lock scrollbars temporarily...
	//$('body').css({'overflow':'hidden'});
	$(document).bind('scroll',function () { 
	   window.scrollTo(0,0); 
	});	
	  
	  
	  return false;
	
	
}




// HOLD (use if there's server request that shouldn't be interrupted)
// This box cannot be removed by a user event 
// It has to be removed by the application itself...
function hold (message) {
        
        release();
        
	message = sanitizeDialogText(message);
	
	
	$('#alert-box').html('<p>'+message+'</p>');
	$('#alert-box, #fullscreen-bg2').show();
	
	// lock scrollbars temporarily...
	//$('body').css({'overflow':'hidden'});
	$(document).bind('scroll',function () { 
	   window.scrollTo(0,0); 
	});	
	  
	  
	return false;
	
	
}


// RELEASE (remove 'hold' bubble...)
function release () {


	
	$('#alert-box').html('<p></p>');
        $('#alert-box, #fullscreen-bg2').unbind('click');
	$('#alert-box, #fullscreen-bg2').hide();
	
	
        $(document).unbind('scroll'); 
        $('body').css({'overflow':'visible'});	
	  
	  
	return false;
	
	
}




// CONFIRM BUBBLE SETUP

function prettyConfirm (message, callBack)
{
    
    release(); // In case user was previoulsy on hold...
    
    // Remove previous click events...
    $('#confirm-box #confirm-button-ok, #confirm-box #confirm-button-ok, #fullscreen-bg2').unbind('click');
    

        
    // Add message...
    message = sanitizeDialogText(message);   
    $('#confirm-box .bubble-text').html(message);
    
    // Set title
    $('#confirm-box .bubble-title').html('Confirm');
    
    
    // Init confirm dialog 'confirm' button...
    $('#confirm-box #confirm-button-ok').one('click', function () {
        $('#confirm-box, #fullscreen-bg2').fadeOut(300);
        callBack();
        return false;
    });

    
    // Init confirm dialog 'cancel' button...
    $('#confirm-box #confirm-button-cancel, #fullscreen-bg2').click(function () {
            $('#confirm-box, #fullscreen-bg2').fadeOut(300);
            window.confirmToken = '';
            return false;
    });


    // Show confirm bubble...
    $('#confirm-box, #fullscreen-bg2').show();
    
    return false;
}




// REQUIRE PASSWORD BUBBLE 

function requirePassword (message, callBack)
{
    
    release(); // In case user was previoulsy on hold...
    
    // Remove previous click events...
    $('#confirm-box #confirm-button-ok, #confirm-box #confirm-button-ok, #fullscreen-bg2').unbind('click');
    
    // message...
    message = sanitizeDialogText(message); 
    
    // Password box
    var passwordControl = '<input id="confirm-password" style="display:block; width:200px; margin:10px 0px;"  name="password" type="password" value="Password..." tip="Please enter your password <strong>password</strong> <small>Your password is required to complete this operation.</small>" onblur="resetTextbox(this);"  onfocus="resetTextbox(this); hideTip();" size=24 /><br/>';
    
    // Set meessage
    $('#confirm-box .bubble-text').html(message + passwordControl);
    
    // Set title
    $('#confirm-box .bubble-title').html('Password Required');
    
    
    // Init confirm dialog 'confirm' button...
    $('#confirm-box #confirm-button-ok').bind('click', function () {
        if ($('#confirm-box #confirm-password').prop('value') == $('#confirm-box #confirm-password').prop('defaultValue')) 
            {
                showTip('#confirm-box #confirm-password');
                $('#bubble-1').css('z-index', 10002); // A small fix to bring tipBubble on top of confirm box
                return false;
            } else {
        
                $('#confirm-box, #fullscreen-bg2').fadeOut(300);

                window.whisper = $('#confirm-box #confirm-password').val();
                callBack();
                return false;
            }
    });

    
    // Init confirm dialog 'cancel' button...
    $('#confirm-box #confirm-button-cancel, #fullscreen-bg2').click(function () {
            $('#confirm-box, #fullscreen-bg2').fadeOut(300);
            window.confirmToken = '';
            return false;
    });


    // Show confirm bubble...
    $('#confirm-box, #fullscreen-bg2').show();
    
    return false;
}





function sanitizeDialogText (message)
{
  	try {
	if (typeof message == 'string') {

	message = message
				.replace(/[^\s]{30,40}/g, '$& ')
				.replace(/&/g, '&amp;')
				.replace(/</g, '&lt;')
				.replace(/>/g, '&gt;')
				.replace(/"/g, '&quot;')
                                .replace(/\n/g, '<br/>');

	} else if (typeof(message) !== 'number' && typeof(message) !== 'boolean') {
		message = typeof(message);
		
	}
	
	} catch (e) {
		
                message = '';
	} 
        
        return message;
    
}



// TOOL TIP BUBBLE SETUP

init[init.length] = function () {
	
	// any element with 'tip' attribute...
	$("#new-customer-form input[tip]").blur(function () {
		
		$('#bubble-1').hide();
		
	})

}





function showTip (target, warning) {

        
        
        $('#bubble-1 > p').html($(target).attr('tip'));
		if (typeof warning === 'string') {
			$('#bubble-1 > p > small').html(warning);
		}
	
		$('#bubble-1').show().position({
				my: "left bottom", // hor-vert
				at: "left top", // hor-vert
				offset: "-20 -25", // left-top
				of: $(target)
		});
	
}


function hideTip () {
    $('#bubble-1')
    .fadeOut(300)
    .delay(400)
    .css('z-index', window.originalTipBoxZIndex); // Reset z-index (in case 'confirmPassword()' messed with it... 
}




function talkToServer (form) {

	
		$('#dialog').html("");	
		$('.sub_pane').css('display', 'none');
		$('div#dialog_container').css('display', 'block');
		$('#loader').css('display', 'block');

		
	tries++;
	
	$(form).ajaxSubmit({
		dataType: 'json', 
		success: function(data, statusText, xhr, formObjct) {
			$('div#dialog').html('<p>'+data.message+'</p>');
			$('div#dialog').css('display', 'block');
			$('#loader').css('display', 'none');
			
			return false;
			},
			
		error: function () {
			if (tries >= 5) {
				tries = 0;
				$('div#dialog').html(connection_error);
				$('div#dialog').css('display', 'block');
				$('#loader').css('display', 'none');
			
				return false;
				} else {
				setTimeout ('talkToServer("' + form + '")', 2000*tries);
				return false;
				}
			}
	
	});
	

	return false;
}





function resetTextbox (box) {
if (box.value == box.defaultValue) box.value = "";
else if (box.value == "") box.value = box.defaultValue;

}



// Password checker...

function scorePassword(pass) {
    var score = 0;
    if (!pass)
        return score;

    // award every unique letter until 5 repetitions
    var letters = new Object();
    for (var i=0; i<pass.length; i++) {
        letters[pass[i]] = (letters[pass[i]] || 0) + 1;
        score += 5.0 / letters[pass[i]];
    }

    // bonus points for mixing it up
    var variations = {
        digits: /\d/.test(pass),
        lower: /[a-z]/.test(pass),
        upper: /[A-Z]/.test(pass),
        nonWords: /\W/.test(pass)
    }

    variationCount = 0;
    for (var check in variations) {
        variationCount += (variations[check] == true) ? 1 : 0;
    }
    score += (variationCount -1) * 10;

    return parseInt(score);
}




// ESCAPE UNSAFE CHARACTERS FOR JSON...
function babyJSON (text) {
	
		if (typeof text != 'string') return text;
                
		return text
				.replace(/\\/g, '\\\\')	
				.replace(/\//g, '\\/')
				.replace(/[\b]/g, '\\b')
				.replace(/\f/g, '\\f')
				.replace(/\n/g, '\\n')
				.replace(/\r/g, '\\r')
				.replace(/\t/g, '\\t')	
				.replace(/"/g, '\"')
				.replace(/\u[0-9a-fA-F]{4}/g, '\\u[0-9a-fA-F]{4}');

	
}



// PROPER STRINGIFY FUNCTION

function encodeJSON (jsonObjct) {
    
}




// FORMAT FLOAT PRICES

/**
*   Usage:  numberFormat(123456.789, 2, '.', ',');
*   result: 123,456.79
**/


function numberFormat (number, decimals, dec_point, thousands_sep) {
    
        var dec = (typeof dec_point === 'undefined') ? '.' : dec_point;    
        var sep = (typeof thousands_sep === 'undefined') ? ',' : thousands_sep;
        decimals = (typeof decimals === 'undefined') ? 2 : decimals;
    
    
 
        // Convert to string temporarily
        var numStr = ''+number;
        
        // Remove commas if any
        numStr = numStr.replace(/,/g, '');
        
        // Convert to float temporarily
        numStr = parseFloat(numStr, 10).toFixed(decimals);
        
        // Separate integer from decimal
        var num = (numStr+'').split('.');
        
        // Reverse integer string for a sec
        num[0] = num[0].split('').reverse().join('');
        
        // Return or add commas to integer
        if (num[0].length > 3) num[0] = num[0].replace(/([\d]{3})/g, '$&'+sep);
        
        // Bring it back
        num[0] = num[0].split('').reverse().join('')
        
        // Fix stray comma
        .replace(/^([,]+)/, '');
        
        // All together now
        return num.join(dec);
}
 




// FORM VALIDATOR
function validateForm (form) {
	var NAME_REGEXP = /[^-\s\.A-Za-z0-9]/;
	var PASSPHRASE_REGEXP = /([-_a-zA-Z0-9]{2,})*\s([-_a-zA-Z0-9]{2,})*\s([-_a-zA-Z0-9]{2,})/;
	var EMAIL_REGEXP = /^[^@\.]?[\w\._-]+@([\w]+)\.([A-Za-z]{2,4})$/;

	for (var i = 0; i < form.elements.length; i++) {
		var field = form.elements[i];
		
		switch (field.name) {
			case 'name':
				if (field.value == '' || NAME_REGEXP.test(field.value)) {
				field.focus(); 
				return false;
				}
				
				break;
                                
			case 'fname':
				if (field.value == '' || NAME_REGEXP.test(field.value)) {
				field.focus(); 
				return false;
				}
				
				break;

			case 'lname':
				if (field.value == '' || NAME_REGEXP.test(field.value)) {
				field.focus();
				return false;
				}
				break;
				
			case 'email':
				if (field.value == '' || !(EMAIL_REGEXP.test(field.value))) {
				showTip(field, 'Please enter a valid email address.');
				return false;
				}
				break;
				
			case 'password':
				if (field.value == '') {
				field.focus();
				return false;
				}
				if ((scorePassword(field.value) < 60) && !PASSPHRASE_REGEXP.test(field.value)) {
				field.focus();
				return false;
				}
				break;

				default:
				
				// do nothing
				
				break;
		}
				 
		
	}
	
	return true;
	
}






// CHECK PHONE NUMBER ENTRY
function checkPhoneDigit (e, box) {
	
	var chrTyped, chrCode=0, evt= e ? e : event;
	 if (evt.charCode!=null)     chrCode = evt.charCode;
	 else if (evt.which!=null)   chrCode = evt.which;
	 else if (evt.keyCode!=null) chrCode = evt.keyCode;
	
	 if (chrCode==0) chrTyped = 'SPECIAL KEY';
	 else chrTyped = String.fromCharCode(chrCode);
		
	 //Digits, special keys & backspace [\b] work as usual:
	 if (chrTyped.match(/\d|[\b]|SPECIAL/)) return true;
	 if (evt.altKey || evt.ctrlKey || chrCode<28) return true;




	if (/[^+#*\d]/.test(box.value.substr(-1, 1))) {
			box.value = box.value.replace(/[^+#*\d]/, '');
			if (evt.preventDefault) {evt.preventDefault(); } else {return false; }
			evt.returnValue=false;

	}

}


function recheckPhoneDigit (e, box) {
	evt= e ? e : event;
	if (/[^+#*\d]/.test(box.value.substr(-1, 1))) {
			box.value = box.value.replace(/[^+#*\d]/, '');
			if (evt.preventDefault) {evt.preventDefault(); } else {return false; }
			evt.returnValue=false;
	}

}





// CHECK FLOAT NUMBER ENTRY
function validateFloatEntry (e, box) {
	
	var chrTyped, chrCode=0, evt= e ? e : event;
	 if (evt.charCode!=null)     chrCode = evt.charCode;
	 else if (evt.which!=null)   chrCode = evt.which;
	 else if (evt.keyCode!=null) chrCode = evt.keyCode;
	
	 if (chrCode==0) chrTyped = 'SPECIAL KEY';
	 else chrTyped = String.fromCharCode(chrCode);
		
	 //Digits, special keys & backspace [\b] work as usual:
	 if (chrTyped.match(/\d|[\b]|\.|\-|\+|,|SPECIAL/)) return true;
	 if (evt.altKey || evt.ctrlKey || chrCode<28) return true;




	revalidateFloatEntry (e, box);

}


function revalidateFloatEntry (e, box) {
	evt= e ? e : event;
	if (/[^-+\.,\d]/.test(box.value)) {
			box.value = box.value.replace(/[^-+\.,\d]/, '');
			if (evt.preventDefault) {evt.preventDefault(); } else {return false; }
			evt.returnValue=false;
	}

}




// find a string 'needle' in an array 'haystack' and return the array key if found...
 
function findNeedle (needle, haystack) {
	for (key in haystack) { if (haystack[key] == needle) return key;	}
	return '';
}
	





/*
 * 
 * function: [ countInEnglish ]
 * 
 */

function countInEnglish (count, singular, plural) {
    
            switch (count) {
                case 0:
                    return 'No ' + plural;
                    break;

                case 1:
                    return '1 ' + singular;
                    break;

                default:
                    return count + ' ' + plural;
                    break;
            }
                      
}







/*

CURSOR FIX

*/
		
init[init.length] = function () {
	
		var curBrowser = $.browser;
		// IE doesn't support co-ordinates
		var cursCoords = (curBrowser == "msie") ? "" : " 4 4"; 
		closedHandCursor = (curBrowser == "mozilla") ? "-moz-grabbing" : "url(ui/images/closedhand.cur)" + cursCoords + ", move";
		// Opera doesn't support url cursors and doesn't fall back well...
		if (curBrowser == "opera") openHandCursor = "pointer";
		openHandCursor = (curBrowser == "mozilla") ? "-moz-grab" : "url(ui/images/openhand.cur)" + cursCoords + ", move";
			 
}
			


// Passprase REGEXP...

var PASSPHRASE_REGEXP2 = /([-_a-zA-Z0-9]+)*\s([-_a-zA-Z0-9]+)/;
var PASSPHRASE_REGEXP3 = /([-_a-zA-Z0-9]+)*\s([-_a-zA-Z0-9]+)*\s([-_a-zA-Z0-9]+)/;
var EMAIL_REGEXP = /^[^@\.]?[\w\._-]+@([\w\.]+)\.([A-Za-z]{2,4})$/;





/*
 * 
 * UNIVERSAL RESPONSE HANDLER
 * 
 */


function responseHandler (jsonData, tinyStatusSelector, callBack, onError)
{
        jsonData.status = jsonData.status.toLowerCase();
        
        if (jsonData.status == 'ok') {
            
            window.confirmToken = '';
            window.whisper = '';
            
            if (tinyStatusSelector != '') $(tinyStatusSelector).removeClass('error').toggleClass('blink').html('Saved.').delay(2000).html('&nbsp;');
            // else alert('Ok', jsonData.message);
            callBack (jsonData);
            
        }
    
        else if (jsonData.status == 'error') {
                window.confirmToken = '';
                window.whisper = '';
                
                if (tinyStatusSelector != '') $(tinyStatusSelector).removeClass('blink').addClass('error').html('Error!');
                else alert ('Oops!', jsonData.message);
                
                if (typeof onError == 'function') onError(jsonData);
                
                
                return false;
        } 
        
        else if (jsonData.status == 'bomb') {
                        window.confirmToken = '';
                        window.whisper = '';
                        
                        alert('Session Expired', jsonData.message);
                        window.location = 'login.php';
                        return false;
        }
        
        else if (jsonData.status == 'confirm') {
                        
                        window.confirmToken = jsonData.confirm_token;
                        confirm(jsonData.message, callBack);
                        
                        return false;
        }
        
        else if (jsonData.status == 'require_password') {
                        
                        window.confirmToken = jsonData.confirm_token;
                        requirePassword(jsonData.message, callBack);
                        
                        return false;
        }        


        window.confirmToken = '';
        window.whisper = '';
        return false;
    
}









/*
 * 
 * UNIVERSAL AJAX ERROR HANDLER
 * 
 */


function errorHandler (xhr, tinyStatusSelector, callBack)
{
    
    
        if (tinyStatusSelector != '' && xhr.status == 0) $(tinyStatusSelector).toggleClass('blink').html('Connection failed.');
        else alert('Error: ' + xhr.responseText);
        callBack (xhr);    

        return false;
    
}