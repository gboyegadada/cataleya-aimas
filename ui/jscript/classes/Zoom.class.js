




 
 // ------------------------------------------------------------------------------------ //


/*
// Class: [ Dom Element Utility Class ]
__el.prototype.makeZoom = function (options) {
    
    
    var defaults = {	
            id: 'zoom_helper',
            parent: 'body',
            append: true,
            preload: 'Loading...',
            error: 'There has been a problem with loading the image.'
    }; 
    
    if (typeof options !== 'object') options = {};
    
    for (var i in defaults) {
        if (typeof options[i] === 'undefined') options[i] = defaults[i];
    }



    var     obj
    ,       img = new Image()
    ,       loaded = false
    ,       found = true
    ,       timeout
    ,       w1,w2,h1,h2,rw,rh
    ,       over = false
    ,       helper;
    
    this
            .css({ cursor: 'crosshair' })
            .on('click', function(e){ e.preventDefault(); })
            .on('mouseover', function(e){ start(e); })
            .on('mouseout', function(){ hide(); })		
            .on('mousemove', function(e){ move(e); });	
    
    
    for (var i=0, l=this.elements.length; i < l; i++) {
            obj = this.elements[i];	
            // works only for anchors
            var tagName = obj.tagName.toLowerCase();
            if(tagName == 'a'){			   

                    var href = ""+obj.getAttribute('data-src');			
                    img.src = href + '?' + (new Date()).getTime() + ' =' + (new Date()).getTime();
                    //$(img).error(function(){ found = false; });
                    img.onload = function(){ 									
                            loaded = true;	
                            img.onload=function(){};
                    };	
			
            };
    }
    
    
    




        function start(e){
                hide();			
                helper = this.toDOM('<div id="'+ options.id +'">'+ options.preload +'</div>');
                if(options.append) { document.querySelector('body').appendChild(helper); }
                
                if(!found){
                        error();
                } else {
                        if(loaded){
                                show(e);
                        } else {
                                loop(e);
                        };				
                };			
        };

        function loop(e){
                if(loaded){
                        show(e);
                        clearTimeout(timeout);
                } else {
                        timeout = setTimeout(function(){loop(e)},200);
                };
        };

        function show(e){
                over = true;
                __el(img).css({'position':'absolute','top':'0','left':'0'});
                helper.appendChild(img);			
                w1 = parseFloat(window.getComputedStyle(obj).getPropertyValue("width"));
                h1 = parseFloat(window.getComputedStyle(obj).getPropertyValue("height"));
                w2 = parseFloat(window.getComputedStyle(helper).getPropertyValue("width"));
                h2 = parseFloat(window.getComputedStyle(helper).getPropertyValue("height"));
                w3 = img.getAttribute('width');
                h3 = img.getAttribute('height');	
                w4 = w3 - w2;
                h4 = h3 - h2;	
                rw = w4/w1;
                rh = h4/h1;
                move(e);
        };
        

        function hide(){
                over = false;
                document.querySelector('body').removeChild(helper);
        };

        function error(){
                helper.innerHTML = options.error;
        };

        function move(e){
                if(over){
                        // target image movement
                        var p = $('img',obj).offset();
                        var pl = e.pageX - p.left;
                        var pt = e.pageY - p.top;	
                        var xl = pl*rw;
                        var xt = pt*rh;
                        xl = (xl>w4) ? w4 : xl;
                        xt = (xt>h4) ? h4 : xt;	
                        __el(helper.querySelect('img')).css({'left':xl*(-1),'top':xt*(-1)});
                };
        };


    
    

};

*/


