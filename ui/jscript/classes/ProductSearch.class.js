
 
 
// ----------------------------------------------------------------------------------------- //


// Class: [ loadSearch ]
Cataleya.loadSearch = function () {
    

        var _last_req = new Date ();

        // Load moustache templates
        ajax.loadTemplates(['s-product-tile'], true);




        // method: [ makeProductTile ]
        function makeProductTile (_data) {
           return $(Mustache.render(ajax.getTemplate('s-product-tile'), _data)); 
        } 





        function quickSearch (options) {

                        if($.trim($('#product-search-term').val()).length > 2) {


                            var _new_req = new Date ();

                            if (_new_req.getTime() < (_last_req.getTime()+1000)) return;
                            else _last_req = new Date ();

                            ajax.doAPICall ({
                                'api': 'product', 
                                'method': 'search', 
                                params: { text: $('#product-search-term').val() }, 
                                beforeSend: function () {
                                                //$('#product-search-form-wrapper .search-icon-inset').toggleClass('blink-2');	
                                                //setStatus(true); // busy

                                },
                                success: quickSearchCallBack,

                                error: function (xhr) {
                                        if (typeof window.noResult == 'function') window.noResult();
                                        return false;
                                        }

                            });

                        } else {

                                $('#quick-search-results').hide();
                                if (typeof window.noResult == 'function') window.noResult();

                        }

                }
                

    
    
    
    //Return public methods
    return {

        quickSearch: quickSearch, 
        makeProductTile: makeProductTile
        
    };
    
    
    
 }
 
 
 

/*
 * 
 * QUICK SEARCH INTI
 * 
 * 
 */




// JavaScript Document

$('document').ready(function() {
	
	var 
        Search = Cataleya.loadSearch (), 
        GenUtility = Cataleya.getUtility();
                    
        $('.page-title .exit-button-1').click(function () {
            $('.search-results-wrapper').hide();
            $('.content').show();
        });
        
	$(this).keyup(function (e) {
            if (e.keyCode == 13) {
                // enter
            } else if (e.keyCode == 27) {
                $('.search-results-wrapper').hide();
                $('.content').show();
            }
        });
        
	$('#product-search-term').keyup(function (e) {
		if (e.keyCode == 27) return;
                var _this = $(this);
                    
		if ($.trim(_this.val()) == '') {
                    // _this.val(_this.prop('defaultValue'));
                    $('.search-results-wrapper').hide();
                    $('.content').show();
                    return;
                }
                
		Search.quickSearch();
		
	}).focus(function (e) {
            
                var _this = $(this);
                
		if (_this.prop('defaultValue') === _this.val()) _this.val('');
                
		$('#product-search-term').data('status', true);
		if ($.trim(_this.val()).length > 2 && $('#quick-search-results > li').length > 0) $('#quick-search-results').show();
                
	}).blur(function () {
                var _this = $(this);
                    
		if ($.trim(_this.val()) == '') {
                    _this.val(_this.prop('defaultValue'));
                    $('.search-results-wrapper').hide();
                    $('.content').show();
                }
                
                $('#product-search-term').data('status', false);
		
	});
	

	$('body, document, window').click(function (e) {
			if ($('#product-search-term').data('status') || $('#quick-search-results').css('display') == 'none') return;

	});
	
	


	
	// QUICK SEARCH SUBMIT BIND
	$('#product-search-form').submit(function () {
		
		Search.quickSearch();
		return false;
		
	});
        
        
        


        window.quickSearchCallBack = function (data) 
        {

                var _grid = $('.search-item-grid');


                $('.content').hide();	
                $('.search-results-wrapper').show();
                $('.search-item-count').html('Please wait...');

                if (data.count > 0) {

                    _grid.html('');
                    for (i in data.Products) {
                            var _tile = Search.makeProductTile({ store: data.Store, product: data.Products[i] }); 
                            _grid.append(_tile);
                    }
                    
                    $('.search-item-count').html('Showing ' + countInEnglish (data.count, 'item', 'items'));


                } else {

                    _grid.html('');

                    $('.search-item-count').html('No items found.');


                }
        }





        window.noResult = function (data) 
        {
                    var _grid = $('.search-item-grid');
                    _grid.html('');

                    $('.search-item-count').html('No items found.');
        }




        
	


});