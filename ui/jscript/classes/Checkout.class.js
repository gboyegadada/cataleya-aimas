
 
 
// ----------------------------------------------------------------------------------------- //


// Class: [ loadCheckout ]
Cataleya.loadCheckout = function () {
    
        // Private properties

        var 

        Validator = Cataleya.newValidator(), 
        Utility = Cataleya.getUtility(), 
        _grand_total_cache = $('.cart-grand-total span.field-value').html();


        var 
        
        _shipping_charge_tile = '<li class="cart-shipping-charge"> <span class="fltlft field-label"><strong>Shipping ({{ name }}):</strong></span> <span class="fltrt field-value">{{{ amount }}}</span></li>', 
        _shipping_tax_tile = '<li class="cart-shipping-tax"> <span class="fltlft field-label"><strong>{{ name }}:</strong></span> <span class="fltrt field-value">{{{ amount }}}</span></li>';

        // Load moustache templates
        ajax.loadTemplates(['s-shipping-options-panel', 's-province-select-control'], true);


        function prepForm () {

            Validator.prepForm('#checkout-form', {

                 fields: {
                   // reference the field you're talking about, probably by `id`
                   // but you could certainly do $('[name=name]') as well.



                   // SHIPPING ADDRESS FORM

                   '[name=shipping-firstname]': {
                     required: true,
                     message: 'Please enter your first name (only A-Z, a-z, 0-9, spaces, dots, commas or dashes allowed!).', 
                     test: Validator.name
                   },
                   '[name=shipping-lastname]': {
                     required: true,
                     message: 'Please enter your last name (only A-Z, a-z, 0-9, spaces, dots, commas or dashes allowed!).', 
                     test: Validator.name
                   },

                   '[name=shipping-address-1]': {
                     required: true,
                     message: 'Please enter an address.',
                     test: Validator.description
                   }, 
                   '[name=shipping-country]': {
                     required: true,
                     message: 'Please choose a country.',
                     test: Validator.select
                   }, 
                   '[name=shipping-city]': {
                     required: true,
                     message: 'Please specify a city (only A-Z, a-z, 0-9, spaces, dots, commas or dashes allowed!).',
                     test: Validator.name
                   }, 
                   '[name=shipping-province-id]': {
                     required: true,
                     message: 'Please choose a state.',
                     test: Validator.select
                   }, 
                   '[name=shipping-province]': {
                     required: true,
                     message: 'Please specify a province (only A-Z, a-z, 0-9, spaces, dots, commas or dashes allowed!).',
                     test: Validator.name
                   }, 


                   // BILLING ADDRESS FORM

                   '[name=billing-firstname]': {
                     required: true,
                     message: 'Please enter your first name (only A-Z, a-z, 0-9, spaces, dots, commas or dashes allowed!).', 
                     test: Validator.name
                   },
                   '[name=billing-lastname]': {
                     required: true,
                     message: 'Please enter your last name (only A-Z, a-z, 0-9, spaces, dots, commas or dashes allowed!).', 
                     test: Validator.name
                   },

                   '[name=billing-address-1]': {
                     required: true,
                     message: 'Please enter an address.',
                     test: Validator.description
                   }, 
                   '[name=billing-country]': {
                     required: true,
                     message: 'Please choose a country.',
                     test: Validator.select
                   }, 
                   '[name=billing-city]': {
                     required: true,
                     message: 'Please specify a city (only A-Z, a-z, 0-9, spaces, dots, commas or dashes allowed!).',
                     test: Validator.name
                   }, 
                   '[name=billing-province]': {
                     required: true,
                     message: 'Please specify a province (only A-Z, a-z, 0-9, spaces, dots, commas or dashes allowed!).',
                     test: Validator.name
                   }

                 }

             });
        
        }
        

        
        
        
        
        
        
        // method: [ renderProvinceField ]
        function renderProvinceField (_field) {
            
            _field = $(_field);
            var _has_provinces = ($('option:selected', _field).attr('data_has_provinces') == 1) ? true : false;
            
            $('#shipping-options').html('');
            $('#shipping-province-field').replaceWith($('<input id="shipping-province-field" name="state_loading" type="text" size="20" value="Please wait..." maxlength="100" DISABLED />'));
               
            
            // selected country has provinces
            
            if (_has_provinces && Validator.iso2(_field.val())) {
                
                // get provinces
                ajax.doAPICall ({
                    'api': 'geo', 
                    'method': 'getProvinces', 
                    params: {country_code: _field.val() },
                    success: function (data) {
                        $('#shipping-province-field').replaceWith(Mustache.render(ajax.getTemplate('s-province-select-control'), { provinces: data.provinces, Country: { iso2: _field.val()} }));
                        $('#shipping-options').html('Please choose a country and state/province to see available delivery options here.');
                    },

                    error: function (xhr) {


                            }

                });
            } else {
                
                // selected country has NO provinces
                $('#shipping-province-field').replaceWith($('<input id="shipping-province-field" name="shipping-province" type="text" size="20" maxlength="100" />'));
                refreshShippingOptions (_field.val());

            }
           
        } 



        // method: [ refreshShippingOptions ]
        function refreshShippingOptions (_country_code, _province_field) {

            if (!Validator.iso2(_country_code) || (typeof _province_field !== 'undefined' && !Validator.select($(_province_field).val()))) {
                $('#shipping-options').html('Please choose a country and state/province to see available delivery options here.');
                $('#mini-shipping-charges').html('Shipping cost: Unavailable.');                   
                $('#mini-delivery-info').hide().html('');
                $('.cart-shipping-tax, .cart-shipping-charge').remove();
                $('.cart-grand-total span.field-value').html('Unavailable');
                return;
            }
            
            
            $('#shipping-options').html('Loading delivery options...');
            $('#mini-shipping-charges').html('Shipping cost: Unavailable.');                  
            $('#mini-delivery-info').hide().html('');
            
            var _province_id = (typeof _province_field !== 'undefined') ? $(_province_field).val() : 0;
            

            ajax.doAPICall ({
                'api': 'shipping', 
                'method': 'getOptions', 
                params: {country_code: _country_code, province_id: _province_id }, 
                dataType: 'json', 
                success: function (data) {
                    
                    if (data.ShippingOptions.length > 0) $('#shipping-options').html(Mustache.render(ajax.getTemplate('s-shipping-options-panel'), { ShippingOptions: data.ShippingOptions }));
                    else $('#shipping-options').html('There are no shipping options available for the selected location. Please select a different country and/or province.');
                },

                error: function (xhr) {
                     $('#shipping-options').html('Please choose a country and state/province to see available delivery options here.');
                 }

            });

           
        } 
        
        
        

        // method: [ refreshShippingCharges ]
        function refreshShippingCharges (_field) {
            
            _field = $(_field);
            if (typeof _field === 'undefined' || !_field.prop('checked')) return;
            
            var _OrderForm = $('#checkout-form');
            var _country_code = _OrderForm.find('select[name^=shipping_country]').val();
            var _province_id = _OrderForm.find('select[name^=shipping_province_id]').val();
            
            
            if (!Validator.iso2(_country_code) || typeof _province_id === 'undefined') return; 
            
            $('#mini-shipping-charges').html('Calculating shipping cost...');
            
            ajax.doAPICall ({
                'api': 'shipping', 
                'method': 'getCharges', 
                'params': {shipping_option_id: _field.val(), country_code: _country_code, province_id: _province_id }, 
                success: function (data) {
                    
                    $('#mini-shipping-charges').html('Shipping cost: ' + data.ShippingCharges.FancyAmount);                    
                    $('#mini-delivery-info').html(data.ShippingCharges.description).fadeIn(200);
                    
                    
                    $('.cart-shipping-tax, .cart-shipping-charge').remove();
                    $('li.cart-grand-total').before(Mustache.render(_shipping_charge_tile, { name: data.ShippingCharges.name, amount: data.ShippingCharges.FancyAmount }));
                    
                    for (var i in data.ShippingCharges.Taxes) {
                        $('li.cart-grand-total').before(Mustache.render(_shipping_tax_tile, { name: data.ShippingCharges.Taxes[i].description, amount: data.ShippingCharges.symbol + data.ShippingCharges.Taxes[i].pretty_amount }));
                    }

                    $('.cart-grand-total span.field-value').html(data.ShippingCharges.symbol + data.ShippingCharges.grandTotal);
                    
                },

                error: function (xhr) {
                    
                    $('.cart-shipping-tax, .cart-shipping-charge').remove();
                    $('#mini-shipping-charges').html('Shipping cost: Unavailable.');                   
                    $('#mini-delivery-info').hide().html('');
                    $('.cart-grand-total span.field-value').html('Unavailable');
                 }

            });

           
        } 
        
   
        
        


        /*
         * 
         * [ FUNCTION ] updateBillingInfoFrame 
         * ____________________________________________________________________________
         * 
         * 
         * 
         */


        function updateBillingInfoFrame (address_required) {
            if (typeof (address_required) !== 'boolean') return false;
            $('#default-billing-info-message').hide();

            if (address_required) {
                $('#credit-card-billing-address').fadeIn(200);
                $('#billing-address-not-required').hide();
            } else {
                $('#billing-address-not-required').fadeIn(200);
                $('#credit-card-billing-address').hide();
            }

            return true;
        }




        function submit () {

                prepForm();
                var _OrderForm = $('#checkout-form');
                
                _OrderForm.find('input[name=do]').val('checkout');

//                if (Validator.validateForm(_OrderForm).length > 0) return; 
//                else _OrderForm.submit();
//                
//                console.log(Utility.getFormData(_OrderForm));
                _OrderForm.submit();
                
        }
        
        

        function applyCoupon () {


                prepForm();
                var _OrderForm = $('#checkout-form');
                
                //_OrderForm.find('input[name=do]').val('apply-coupon');

                if (Validator.couponCode(_OrderForm.find('input[name=coupon-code]').val()) === false) { showTip('#coupon-code-field', 'Please enter a valid coupon code.'); return; }
                else _OrderForm.submit();


        }

    
    //Return public methods
    return {

        refreshShippingCharges: refreshShippingCharges, 
        refreshShippingOptions: refreshShippingOptions, 
        renderProvinceField: renderProvinceField, 
        updateBillingInfoFrame: updateBillingInfoFrame, 
        submit: submit, 
        applyCoupon: applyCoupon
        
    };
    
    
    
 }
 
 
 