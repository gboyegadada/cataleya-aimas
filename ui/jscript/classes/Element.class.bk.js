




 
 // ------------------------------------------------------------------------------------ //



// Class: [ Dom Element Utility Class ]
var __el = function (selector) {
    
    var self = this.prototype;
    
    if (typeof selector === 'object' && selector.nodeType) {
        self.elements = [selector];
    }
    
    try {
        if (selector.length > 0 && typeof selector[0] === 'object' && selector[0].nodeType) {
            self.elements = selector;
            
        }
    } catch (e) {
        // do nothing
    }
    
    if (typeof selector === 'string') {
        

        if ( selector.charAt(0) === "<" && selector.charAt( selector.length - 1 ) === ">" && selector.length >= 3 ) {
            self.elements = [toDOM(selector)];
        }

        else if (selector.match(/^[#\.]/)) {
            self.elements = document.querySelectorAll(selector);
        }
        // else if (typeof selector === 'string' && selector.match(/^[\da-z\-_]{1,100}/gi)) element = document.querySelector(selector);
    
    }
    
    
    
    var 
    
    rspace = /\s+/, 
    rline = /[\t\r\n]/g;
    

    function esc (s) {
     return s.replace(/([.?*+^$[\]\\(){}|-])/g, "\\$1");
    }

    function toDOM (html) {
      var 

        d=document, 
        i, 
        wrap = d.createElement("div"), 
        b = d.createDocumentFragment();

        wrap.innerHTML = html;

        while(i = wrap.firstChild) b.appendChild(i);

      return b;
    }
    
    
    
    
    function _hasClass (el, c) {
            
            return ((" " + el.className + " ").replace(rline, " ").indexOf(c) > -1);
           
        }



        /*=========  CLASS LIST ==================== */

        self.addClass = function (c) {

            for (var i=0, l=this.elements.length; i < l; i++) {
                if (!_hasClass(this.elements[i], c)) this.elements[i].className += ' ' + c;
                
                
            }

            return this;

        },

        self.removeClass = function (c) {
            
            for (var i=0, l=this.elements.length; i < l; i++) {
                var 

                cStr = (" " + this.elements[i].className + " ").replace(rline, " "), 
                cList = (c || "").split(rspace);
        

                for (var i2=0, l2=cList.length; i2 < l2; i2++) {

                    while ((" " + cStr + " ").indexOf(cList[i2]) > -1) {
                        cStr = (" " + cStr + " ").replace(cList[i2], " ");
                    }
                }

                this.elements[i].className = cStr.trim();
                
            }

            return this;

        },

        self.toggleClass = function (c) {

            this.hasClass(c) ? this.removeClass(c) : this.addClass(c);

            return this;
        },

        self.hasClass = function (c) {
            
           for (var i=0, l=this.elements.length; i < l; i++) {
                    if (_hasClass(this.elements[i], c)) { 
                        return true;
                        break;
                    }
            }
            
            return false;

        }, 



        /* ============= HTML ========= */

        self.appendChild = function (n) {
            for (var i=0, l=this.elements.length; i < l; i++) {
                if (typeof n === 'string') n = toDOM(n);
                this.elements[i].appendChild(n);
            }
            return self;
        },
        
        
        /* ======== CSS ============= */

        self.css = function (css) {
            for (var i=0, l=this.elements.length; i < l; i++) {
                
                for (var k in css) {
                    this.elements[i].style[k] = css[k];
                    
                }
                
                //console.log(this.elements[i]);
                
            }
            
            return this;

        }, 
        
        
        
        
        /* =========== EFX ========== */
        
        self.fadeIn = function (display, callBack){
            var elements = this.elements;
            
            elements[0].style.opacity = 0;
            elements[0].style.display = display || "block";

            (function fade() {
              var val = parseFloat(elements[0].style.opacity);
              if (!((val += .1) > .9)) {
                elements[0].style.opacity = val;
                (typeof requestAnimationFrame !== 'undefined') ? requestAnimationFrame(fade) : setTimeout(fade, 50);
              } else {
                  elements[0].style.opacity = 1;
                  if (typeof callBack === 'function') setTimeout(function () { callBack(self); }, 70);
              }
            })();
        }, 
        
        
        self.fadeOut = function (display, callBack){
            var elements = this.elements;
            
            elements[0].style.opacity = 1;
            elements[0].style.display = display || "block";

            (function fade() {
              var val = parseFloat(elements[0].style.opacity);
              if (!((val -= .1) < 0.1)) {
                elements[0].style.opacity = val;
                (typeof requestAnimationFrame !== 'undefined') ? requestAnimationFrame(fade) : setTimeout(fade, 50);
              } else {
                  elements[0].style.opacity = 0;
                  if (typeof callBack === 'function') setTimeout(function () { callBack(self); }, 70);
              }
            })();
        };



    return this;

};




