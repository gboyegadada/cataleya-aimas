

var cart_default_status = '<ul id="cart-messenger-content"><li>Please wait while we update your cart.<br /><br /><img src="ui/images/ui/loading.gif" /></li></ul>';

$('document').ready(function () {
    
    $("#add-to-cart-form").submit(function (){
            addToCart();

            return false;

    });
    
    

    
});




/*
 * 
 * [ FUNCTION ] addToCart
 * ____________________________________________________________________________
 * 
 * 
 * 
 */




function addToCart () {
	

    var option = getSelection();
    
    // no selection: check if any option is available.
    if (option == false && !product.out_of_stock) 
        
            {
                alert ('Hint', 'Please select an option.');
                return false;
            }

	// Show cart loading...
        else $('#cart-messenger').html(cart_default_status);
        
        $('#cart-messenger').show();
        
        $("#add-to-cart-form").find('input[name="option_id"]').val(option.id);
        $("#add-to-cart-form").find('input[name="add"]').val(1);
        
        
        // Do query...


        $.ajax ({
        type: 'POST', 
        url: $("#add-to-cart-form").find('#shop_landing').val()+'cart', 
        data: $("#add-to-cart-form").serialize(), 
        dataType: 'html', 
        success: function (data) {

                $("#cart-messenger").html(data);

                return false;
                }, 
        error: function (xhr) {
                        $("#cart-messenger").hide();
                        alert('Error', "Your item could not be added because there's been an error. Please try again at another time.");
                }
        });



	return false;
	
}








/*
 * 
 * [ FUNCTION ] addToCartQuick
 * ____________________________________________________________________________
 * 
 * 
 * 
 */




function addToCartQuick (product_id, color_id, size_id, shop_landing) {
	

	// Show cart loading...
        $('#cart-messenger').html(cart_default_status);
        
        $('#cart-messenger').show();
        
        var query = {
            product_id: product_id, 
            size_id: size_id, 
            color_id: color_id, 
            add: 1, 
            shop_landing: shop_landing
        }
        
        
        
        // Do query...


        $.ajax ({
        type: 'POST', 
        url: 'cart', 
        data: query, 
        dataType: 'html', 
        success: function (data) {

                $("#cart-messenger").html(data);

                return false;
                }, 
        error: function (xhr) {
                        $("#cart-messenger").hide();
                        alert('Error', "Your item could not be added because there's been an error. Please try again at another time.");
                }
        });



	return false;
	
}






function cartQuickView (shop_landing) {
	

	// Show cart loading...
        $('#cart-messenger').html(cart_default_status);
        
        $('#cart-messenger').show();
        
        var query = {
            shop_landing: shop_landing
        }
        
        
        
        // Do query...


        $.ajax ({
        type: 'POST', 
        url: 'cart', 
        data: query, 
        dataType: 'html', 
        success: function (data) {

                $("#cart-messenger").html(data);

                return false;
                }, 
        error: function (xhr) {
                        $("#cart-messenger").hide();
                        alert('Error', "Your item could not be added because there's been an error. Please try again at another time.");
                }
        });



	return false;
	
}







/*
 * 
 * [ FUNCTION ] addToWishlist
 * ____________________________________________________________________________
 * 
 * 
 * 
 */




function addToWishlist () {
	

        if ($('#product-color-select').val() == '0' || $('#product-size-select').val() == '0') 
            {
                showTip ('#add-to-wishlist-button', 'Please select a color and a size.');
                return false;
            }


        
        
        // Do query...


        $.ajax ({
        type: 'POST', 
        url: $("#add-to-cart-form").find('#shop_landing').val()+'wishlist', 
        data: $("#add-to-cart-form").serialize(), 
        dataType: 'json', 
        success: function (data) {

                $('#wishlist-item-count').html(data.Wishlist.items.length);

                return false;
                }, 
        error: function (xhr) {
            
                        // alert(xhr.responseText);
                        showTip ('#add-to-wishlist-button', "The item could not be added to your wishlist. Please make sure you are signed in and then try again at another time.");
                }
        });



	return false;
	
}

