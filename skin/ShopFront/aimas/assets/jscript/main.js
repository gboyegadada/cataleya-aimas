// Global Storefront Javascript Doc

window.cartMessengerTimeout = null;
window.pageMenuTimeout = null;
window.cartMessengerHoverTimeout = null;



$('document').ready(function () {


        // Initialize cart messenger
        $('#shopper-dashboard-bag')
        .mouseover(function () {
            clearTimeout (window.cartMessengerTimeout, window.cartMessengerHoverTimeout);
            window.cartMessengerHoverTimeout = setTimeout ("showMenu('#cart-messenger', '#shopper-dashboard-bag')", 300);
            $("#cart-messenger").load($('#ENV_SHOP_LANDING').val()+'cart/view/');
        })
        .mouseout(function () {
               clearTimeout(window.cartMessengerHoverTimeout);
               window.cartMessengerTimeout = setTimeout(function () {
               hideMenu('#cart-messenger', '#shopper-dashboard-bag');

            }, 300);
        });

        $('#cart-messenger').mouseover(function () {
            clearTimeout (window.cartMessengerTimeout);
        })
        .mouseout(function () {
           window.cartMessengerTimeout = setTimeout(function () {
               hideMenu('#cart-messenger', '#shopper-dashboard-bag');
           }, 300);
        });





/* ------------------- MENU ----------------------------- */



        var
        t, t2, on, visible = false, active = false, activeAnchor = false,
        crumb='slider';


        function aMouseOutHandler () {
            crumb = 'anchor';
            h();
        }


        function sMouseOutHandler () {
            crumb = 'slider';
            h();
        }

        function h () {

            on = false;

            clearTimeout(t);
            t = setTimeout(function () {
                if (on === false) {
                   activeAnchor.removeClass('active');
                   visible = false;
                   if (active !== false) {
                       active.fadeOut(200); // .removeClass('active');
                       active = activeAnchor = false;
                   }
                }
            }, 10);
        }


        function aMouseOverHandler () {


            on = true;

            var submenu_id = this.getAttribute('data-submenu');

            if (typeof submenu_id === 'string') {

                var submenu = document.querySelector(submenu_id);

                active = $(submenu).fadeIn(200); // .addClass('active');
                activeAnchor = $(this).addClass('active');
            } else {
                active.fadeOut(200); // removeClass('active');
                activeAnchor.removeClass('active');
                active = activeAnchor = false;
            }


            visible = true;
        }




        function aClickHandler (e) {

            var submenu_id = this.getAttribute('data-submenu');

            if (typeof submenu_id === 'string') {
                e = e || window.event;
                e.preventDefault ? e.preventDefault() : e.returnValue = false;
            }

            if (visible === false) {
                aMouseOverHandler.apply(this, e);


            }
            else {
                active.fadeOut(200); // .removeClass('active');
                active = visible = false;
            }

        }




        function sMouseOverHandler () {
            on = true;

        }




        $('.header-links > .menu-item-wrap > a')
        .mouseover(aClickHandler)
        .mouseout(aMouseOutHandler);


        $('.header-links > .menu-item-wrap > .submenu, #shop-submenu')
        .mouseover(sMouseOverHandler)
        .mouseout(sMouseOutHandler);




});




function hint (target, text) {


    $(target).grumble({
    		text: text,
    		angle: 150,
    		distance: 3,
    		showAfter: false,
    		hideAfter: false,
    		hideOnClick: true,
    		type: 'alt-',
    		hasHideButton: true,
    		buttonHideText: 'Pop!'
    	});


}
