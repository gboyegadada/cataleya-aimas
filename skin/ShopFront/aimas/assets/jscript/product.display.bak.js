




/*
 *
 * Initialize easyZoom for main product photo...
 *
 */
	
$('document').ready(function($){
    
    initZoom ();

});



/*
 * 
 * [ FUNCTION ] tabFocus 
 * ____________________________________________________________________________
 * 
 * For tabs in product details section.
 * 
 */


function tabFocus(tabId) {
	
	switch (tabId) {
		case 'tab-1':
			$('#tab-1, #tab-1-header').addClass('tab-state-front');
			$('#tab-2, #tab-2-header, #tab-3-header, #tab-3').removeClass('tab-state-front');
			break;

		case 'tab-2':
			$('#tab-2, #tab-2-header').addClass('tab-state-front');
			$('#tab-1, #tab-1-header, #tab-3, #tab-3-header').removeClass('tab-state-front');
			break;

		case 'tab-3':
			$('#tab-3, #tab-3-header').addClass('tab-state-front');
			$('#tab-1, #tab-1-header, #tab-2, #tab-2-header').removeClass('tab-state-front');
			break;
		default:
			$('#tab-1, #tab-1-header').addClass('tab-state-front');
			$('#tab-2, #tab-2-header, #tab-3, #tab-3-header').removeClass('tab-state-front');
			break;
			
	}
	return false;
	
}



/*
 * 
 * [ FUNCTION ] getStock 
 * ____________________________________________________________________________
 * 
 * Returns avaialable stock for [color + size] argument passed to it.
 * 
 */



function getStock () {
    
    var color_id = $('#product-color-select').val();
    var size_id = $('#product-size-select').val();
    
    // no selection: check if any option is available.
    if (color_id == '0' && size_id == '0' && !product.out_of_stock)
        {
            return 1;
        }
        
    // color and size selected: check if selected size is available in selected color.
    else if (color_id != '0' && size_id != '0')
        {
        for (var i = 0; i < product.options.length; i++)
            {
                if (product.options[i].color.id == color_id && product.options[i].size.id == size_id ) return product.options[i].stock;
            }
            
        } 
     
     // color selected, size not selected: check if selected color is available in any size.
     else if (color_id != '0') 
         {
            for (var i = 0; i < product.options.length; i++)
                {
                    if (product.options[i].color.id == color_id && product.options[i].stock > 0) return product.options[i].stock;
                } 
                
                return 0;
                
         }     
     
     // size selected, color not selected: check if selected size is available in any color.
     else if (size_id != '0') 
         {
            for (var i = 0; i < product.options.length; i++)
                {
                    if (product.options[i].size.id == size_id && product.options[i].stock > 0) return product.options[i].stock;
                }  
                
                return 0;
         }
         
    else return 0;
            
    
}






/*
 * 
 * [ FUNCTION ] getPrice 
 * ____________________________________________________________________________
 * 
 * Returns price for [color + size] option based on user's selection
 * 
 */



function getPrice () {
    
    var color_id = $('#product-color-select').val();
    var size_id = $('#product-size-select').val();
    
  
        
        
    if ( color_id != '0' && size_id != '0' ) {
        
        for (var i = 0; i < product.options.length; i++)
            {
                if (product.options[i].color.id == color_id && product.options[i].size.id == size_id ) return product.options[i].price;
            }
            
       return 'Unavailable';
      
    } else {
        return product.base_price;
    }    
    
}







/*
 * 
 * [ FUNCTION ] getBasePrice 
 * ____________________________________________________________________________
 * 
 * Returns product base price.
 * 
 */



function getBasePrice () {
    
    try {
        
    return product.basePrice;
    
    } catch (e) {
        return 0;
    }
}





/*
 * 
 * [ FUNCTION ] onSale 
 * ____________________________________________________________________________
 * 
 * .
 * 
 */



function isOnSale () {
    
    try {
        
    return product.isOnSale;
    
    } catch (e) {
        return false;
    }
}







/*
 * 
 * [ FUNCTION ] getBasePrice 
 * ____________________________________________________________________________
 * 
 * Returns product option sale price.
 * 
 */



function getSalePrice () {
    
    var color_id = $('#product-color-select').val();
    var size_id = $('#product-size-select').val();
    
  
        
        
    if ( color_id != '0' && size_id != '0' ) {
        
        for (var i = 0; i < product.options.length; i++)
            {
                if (product.options[i].color.id == color_id && product.options[i].size.id == size_id ) return product.options[i].salePrice;
            }
            
       return 'Unavailable';
      
    } else {
        return product.salePrice;
    }  
    
}




/*
 * 
 * [ FUNCTION ] toggleAddToCartButton 
 * ____________________________________________________________________________
 * 
 * Disables of enables 'add to cart' button.
 * 
 */



function toggleAddToCartButton (on_or_off) {
    
    if (on_or_off) 
        {
            $('#add-to-cart-button').show();
            $('#sold-out-button').hide();
        } else {
            $('#add-to-cart-button').hide();
            $('#sold-out-button').show();            
        }
        
    return false;
    
}






/*
 * 
 * [ FUNCTION ] checkAvailability 
 * ____________________________________________________________________________
 * 
 * 
 * 
 */



function checkAvailability () {
    
    
    // Check availability...
    var stock = getStock();
    
    // Show or hide 'add to cart' button...
    if (stock == 0) toggleAddToCartButton(false);
    else toggleAddToCartButton(true);
    
    // Update product price...
    $('#product-price-value').html(numberFormat(getPrice(), 2));
    
    // Update display picture
    enlargeColorPreviewImage ();
        
    return false;
    
}





/*
 * 
 * [ FUNCTION ] enlargeProductPhoto 
 * ____________________________________________________________________________
 * 
 * 
 * 
 */


function enlargeProductPhoto (zoom_anchor) {
    
    $("#main-product-photo-zoom-anchor").prop('href', $(zoom_anchor).prop('href'));
    $("#main-product-photo").prop('src', $(zoom_anchor).attr('data_new_src'));
    
    initZoom ();
    
    return false;
}





/*
 * 
 * [ FUNCTION ] enlargeProductPhoto 
 * ____________________________________________________________________________
 * 
 * 
 * 
 */


function enlargeColorPreviewImage () {
    
    var color_id = $('#product-color-select').val();
    if (color_id == '0') {
        
        $("#main-product-photo-zoom-anchor").prop('href', product.primaryImage.zoom);
        $("#main-product-photo").prop('src', product.primaryImage.large);
        initZoom ();
        return false; 
    }

    for (i in product.colors)
        {
            if (product.colors[i].id == color_id) {
                
                if (!product.colors[i].image_available) break;
                
                $("#main-product-photo-zoom-anchor").prop('href', product.colors[i].image.zoom);
                $("#main-product-photo").prop('src', product.colors[i].image.large);
                initZoom ();
                break;
            }
        }
    
    return false;
}






/*
 * 
 * [ FUNCTION ] initZoom 
 * ____________________________________________________________________________
 * 
 * 
 * 
 */


function initZoom () {
    
	
	$('a.product-zoom').easyZoom({
		
			id: 'easy_zoom',
			parent: '#product-info-wrapper',
			append: true
	});
	
	
	$('#main-product-photo-wrapper').mouseenter(function () {
		$('#photo-zoom-icon').fadeOut(200);
		
	}).mouseleave(function () {
		$('#photo-zoom-icon').fadeIn(200);
	});
        
     return false;
}