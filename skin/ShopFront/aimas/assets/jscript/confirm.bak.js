


/*
 *
 * RESEND CONFIRM CODE VIA AJAX
 * 
 * 
 * 
 */


$('document').ready(function () {
    
    $('#confirm-form-code').focus();
    
    $('#resend-confirm-form').submit(function ()
        {

                $('#resend-confirm-box').html('<span class="blink">Sending...</span>');

                // Do request...
                $.ajax ({
                        type: 'POST', 
                        url: $("#resend-confirm-form").prop('action'), 
                        data: $("#resend-confirm-form").serialize(), 
                        dataType: 'json', 
                        success: function(data) {

                                    if (data.status == 'error') {
                                            $('#resend-confirm-box').html(sanitizeDialogText(data.message));
                                            return false;
                                    } else if (data.status == 'bomb') {
                                            bombSession(data.message);
                                            return false;
                                    }
                                  
                                  $('#resend-confirm-box').html(sanitizeDialogText(data.message));
                                  return false;


                        }, 
                        error: function (data) {

                        $('#resend-confirm-box').html('Error');
                        return false;
                        }

                });
                
                return false;

        });

});





