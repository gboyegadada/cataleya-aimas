

var cart_default_status = '<ul id="cart-messenger-content"><li>Please wait while we update your cart.<br /><br /><img src="ui/images/ui/loading.gif" /></li></ul>';

$('document').ready(function () {
    
    $("#add-to-cart-form").submit(function (){
            addToCart();

            return false;

    });
   
    

    
});




/*
 * 
 * [ FUNCTION ] addToCart
 * ____________________________________________________________________________
 * 
 * 
 * 
 */




function addToCart () {
	

        if ($('#product-color-select').val() == '0' || $('#product-size-select').val() == '0') 
            {
                showTip ('#product-size-select', 'Please select a size.');
                return false;
            }

	// Show cart loading...
        else $('#cart-messenger').html(cart_default_status);
        
        $('#cart-messenger').show();
        
        
        
        // Do query...


        $.ajax ({
        type: 'POST', 
        url: $("#add-to-cart-form").find('#shop_landing').val()+'cart', 
        data: $("#add-to-cart-form").serialize(), 
        dataType: 'html', 
        success: function (data) {

                $("#cart-messenger").html(data);

                return false;
                }, 
        error: function (xhr) {
                        $("#cart-messenger").hide();
                        alert('Error', "Your item could not be added because there's been an error. Please try again at another time.");
                }
        });



	return false;
	
}







/*
 * 
 * [ FUNCTION ] addToCartQuick
 * ____________________________________________________________________________
 * 
 * 
 * 
 */




function addToCartQuick (product_id, color_id, size_id, shop_landing) {
	

	// Show cart loading...
        $('#cart-messenger').html(cart_default_status);
        
        $('#cart-messenger').show();
        
        var query = {
            product_id: product_id, 
            size_id: size_id, 
            color_id: color_id, 
            add: 1, 
            shop_landing: shop_landing
        }
        
        
        
        // Do query...


        $.ajax ({
        type: 'POST', 
        url: shop_landing+'cart', 
        data: query, 
        dataType: 'html', 
        success: function (data) {

                $("#cart-messenger").html(data);

                return false;
                }, 
        error: function (xhr) {
                        $("#cart-messenger").hide();
                        alert('Error', "Your item could not be added because there's been an error. Please try again at another time.");
                }
        });



	return false;
	
}








/*
 * 
 * [ FUNCTION ] addToWishlist
 * ____________________________________________________________________________
 * 
 * 
 * 
 */




function addToWishlist () {
	

        if ($('#product-color-select').val() == '0' || $('#product-size-select').val() == '0') 
            {
                showTip ('#product-size-select', 'Please select a size.');
                return false;
            }


        
        
        // Do query...


        $.ajax ({
        type: 'POST', 
        url: $("#add-to-cart-form").find('#shop_landing').val()+'wishlist', 
        data: $("#add-to-cart-form").serialize(), 
        dataType: 'json', 
        success: function (data) {
                var _count = classes.getUtility().countInEnglish (data.Wishlist.items.length, 'item', 'items');
                $('#wishlist-item-count').html(_count);
                alert('One item has been added to your wishlist! You now have ' + _count + ' in your list.');

                return false;
                }, 
        error: function (xhr) {
            
                         // alert(xhr.responseText);
                        alert ("This item could not be added to your wishlist. Please make sure you are signed in and then try again.");
                }
        });



	return false;
	
}

