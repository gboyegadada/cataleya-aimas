
var Lookbook, initPhotoSwipeFromDOM;

$('document').ready(function () {

            Lookbook = makeSlideshow($('#lookbook-wrapper'), $('#lookbook-thumbs-wrapper'));
            //setInterval(function () {Lookbook.nextSlide();}, 1000);



                    /*
                     *
                     * GALLERY INIT
                     *
                     *
                     */
                    initPhotoSwipeFromDOM = function(gallerySelector) {

                        // parse slide data (url, title, size ...) from DOM elements
                        // (children of gallerySelector)
                        var parseThumbnailElements = function(el) {
                            var thumbElements = el.childNodes,
                                numNodes = thumbElements.length,
                                items = [],
                                figureEl,
                                linkEl,
                                size,
                                item;

                            for(var i = 0; i < numNodes; i++) {

                                figureEl = thumbElements[i]; // <figure> element

                                // include only element nodes
                                if(figureEl.nodeType !== 1) {
                                    continue;
                                }

                                linkEl = figureEl.children[0]; // <a> element

                                size = linkEl.getAttribute('data-size').split('x');

                                // create slide object
                                item = {
                                    src: linkEl.getAttribute('href'),
                                    w: parseInt(size[0], 10),
                                    h: parseInt(size[1], 10)
                                };



                                if(figureEl.children.length > 1) {
                                    // <figcaption> content
                                    item.title = figureEl.children[1].innerHTML;
                                }

                                if(linkEl.children.length > 0) {
                                    // <img> thumbnail element, retrieving thumbnail url
                                    item.msrc = linkEl.children[0].getAttribute('src');
                                }

                                item.el = figureEl; // save link to element for getThumbBoundsFn
                                items.push(item);
                            }

                            return items;
                        };

                        // find nearest parent element
                        var closest = function closest(el, fn) {
                            return el && ( fn(el) ? el : closest(el.parentNode, fn) );
                        };

                        // triggers when user clicks on thumbnail
                        var onThumbnailsClick = function(e) {
                            e = e || window.event;
                            e.preventDefault ? e.preventDefault() : e.returnValue = false;

                            var eTarget = e.target || e.srcElement;

                            // find root element of slide
                            var clickedListItem = closest(eTarget, function(el) {
                                return el.tagName === 'FIGURE';
                            });

                            if(!clickedListItem) {
                                return;
                            }

                            // find index of clicked item by looping through all child nodes
                            // alternatively, you may define index via data- attribute
                            var clickedGallery = clickedListItem.parentNode,
                                childNodes = clickedListItem.parentNode.childNodes,
                                numChildNodes = childNodes.length,
                                nodeIndex = 0,
                                index;

                            for (var i = 0; i < numChildNodes; i++) {
                                if(childNodes[i].nodeType !== 1) {
                                    continue;
                                }

                                if(childNodes[i] === clickedListItem) {
                                    index = nodeIndex;
                                    break;
                                }
                                nodeIndex++;
                            }



                            if(index >= 0) {
                                // open PhotoSwipe if valid index found
                                openPhotoSwipe( index, clickedGallery );
                            }
                            return false;
                        };

                        // parse picture index and gallery index from URL (#&pid=1&gid=2)
                        var photoswipeParseHash = function() {
                            var hash = window.location.hash.substring(1),
                            params = {};

                            if(hash.length < 5) {
                                return params;
                            }

                            var vars = hash.split('&');
                            for (var i = 0; i < vars.length; i++) {
                                if(!vars[i]) {
                                    continue;
                                }
                                var pair = vars[i].split('=');
                                if(pair.length < 2) {
                                    continue;
                                }
                                params[pair[0]] = pair[1];
                            }

                            if(params.gid) {
                                params.gid = parseInt(params.gid, 10);
                            }

                            if(!params.hasOwnProperty('pid')) {
                                return params;
                            }
                            params.pid = parseInt(params.pid, 10);
                            return params;
                        };

                        var openPhotoSwipe = function(index, galleryElement, disableAnimation) {
                            var pswpElement = document.querySelectorAll('.pswp')[0],
                                gallery,
                                options,
                                items;

                            items = parseThumbnailElements(galleryElement);

                            // define options (if needed)
                            options = {
                                index: index,
                                shareEl: false,

                                // define gallery index (for URL)
                                galleryUID: galleryElement.getAttribute('data-pswp-uid'),

                                getThumbBoundsFn: function(index) {
                                    // See Options -> getThumbBoundsFn section of documentation for more info
                                    var thumbnail = items[index].el.getElementsByTagName('img')[0], // find thumbnail
                                        pageYScroll = window.pageYOffset || document.documentElement.scrollTop,
                                        rect = thumbnail.getBoundingClientRect();

                                    return {x:rect.left, y:rect.top + pageYScroll, w:rect.width};
                                }

                            };

                            if(disableAnimation) {
                                options.showAnimationDuration = 0;
                            }

                            // Pass data to PhotoSwipe and initialize it
                            gallery = new PhotoSwipe( pswpElement, PhotoSwipeUI_Default, items, options);
                            gallery.init();
                        };

                        // loop through all gallery elements and bind events
                        var galleryElements = document.querySelectorAll( gallerySelector );

                        for(var i = 0, l = galleryElements.length; i < l; i++) {
                            galleryElements[i].setAttribute('data-pswp-uid', i+1);
                            galleryElements[i].onclick = onThumbnailsClick;
                        }

                        // Parse URL and open gallery if it contains #&pid=3&gid=1
                        var hashData = photoswipeParseHash();
                        if(hashData.pid > 0 && hashData.gid > 0) {
                            openPhotoSwipe( hashData.pid - 1 ,  galleryElements[ hashData.gid - 1 ], true );
                        }
                    };


});






// Class: [ makeSlideshow ]
makeSlideshow = function (_target, _thumbs) {

    // Private properties
    _target = $(_target);

    var

    _slides = _target.find('li.slide'),
    _strip = _target.find('.slide-strip'),
    _thumb_slides = _thumbs.find('li.slide'),
    _population = _target.find('li.slide').length;
    if (_population < 2) return {};

//    _options = (_options != null && typeof _options === 'object') ? _options : {};
//
//    _options.interval = (typeof _options.interval !== 'number' || parseInt(_options.interval) < 3000) ? (Math.random()*(6000-4000)+4000) : (Math.random()*(2000)+_options.interval);
//    if (typeof _options.duration !== 'number') _options.duration = 200;
//    if (typeof _options.transition !== 'string' || !$.inArray(_options.transition, ['fade', 'slide', 'push', 'slideUp'])) _options.transition = 'fade';
//
    var

        _adjusted_pixel_width = _slides.css('width'),
        _trans = function () {},
        _ghost_slide_css = {
        position: 'absolute',
        top: '0px',
        left: '0px',
        display: 'block',
        'z-index': 5
        };

    _target.find('.slide-strip').css({width: (_population * parseInt(_adjusted_pixel_width))});
    _thumbs.find('.slide-strip').css({width: (_population * 90)});


    var

    _thumb_slide_count = -1,
    _thumb_on_css = {opacity: 1},
    _thumb_off_css = {opacity: .8};

    _thumb_slides.each(function (i, obj) {
        _thumb_slide_count++;

        $(this)
        .css({opacity: .8, outline: 'none'})
        .data('slide_num', _thumb_slide_count)
        .mouseenter(function () {
            _thumb_slides.stop().css(_thumb_off_css);
            $(this).css(_thumb_on_css);
            Lookbook.gotoSlide($(this).data('slide_num'));
        });

      });




    /*
     *
     * [ FUNCTION ] gotoSlide
     * ____________________________________________________________________________
     *
     *
     *
     */


     function gotoSlide(slidePos) {

            if (typeof slidePos !== 'number') { throw new Exception ('Invalid slide position or tile id!'); return; }

            var

            pixelPos = 0;


            slidePos = parseInt(slidePos);
            if (slidePos > parseInt(_population)-1) return;

            if (slidePos == 0) slidePos = 1;

            pixelPos = (0 - slidePos*parseInt(_adjusted_pixel_width));
            _strip.stop().animate({left: pixelPos}, 300);
            _target.data('slidePos', slidePos);



            return;
     }








    /*
     *
     * [ FUNCTION ] nextSlide
     * ____________________________________________________________________________
     *
     *
     *
     */


     function nextSlide() {

            var

            slidePos = _target.data('slidePos'),
            pixelPos = 0;

            if (typeof slidePos !== 'number') slidePos = 0;

            if (slidePos >= parseInt(_population)-2) return;
            slidePos++;

            // if (slidePos == 0) slidePos = 1;

            pixelPos = slidePos*parseInt(_adjusted_pixel_width);
            _strip.stop().animate({left: -pixelPos}, 200);
            _target.data('slidePos', slidePos);

            return;
     }









    /*
     *
     * [ FUNCTION ] prevSlide
     * ____________________________________________________________________________
     *
     *
     *
     */


     function prevSlide() {

            var

            slidePos = _target.data('slidePos'),
            pixelPos = 0;

            if (typeof slidePos === 'number') {
                if (slidePos == 0) return;

                slidePos--;
            }

            else return;


            if (slidePos == 0) slidePos = 1;

            pixelPos = slidePos*parseInt(_adjusted_pixel_width);
            _strip.stop().animate({left: -pixelPos}, 200);
            _target.data('slidePos', slidePos);


            return;
     }









    //Return public methods
    return {
        gotoSlide: gotoSlide,
        nextSlide: nextSlide,
        prevSlide: prevSlide
    };



 }
