

var cart_default_status = '<ul id="cart-messenger-content"><li>Please wait while we update your cart.<br /><br /><img src="ui/images/ui/loading.gif" /></li></ul>';

$('document').ready(function () {
    
    $("#add-to-cart-form").submit(function (){
            addToCart();

            return false;

    });
    
    

    
});




/*
 * 
 * [ FUNCTION ] addToCart
 * ____________________________________________________________________________
 * 
 * 
 * 
 */




function addToCart () {
	

    var option = getSelection();
    
    // no selection: check if any option is available.
    if (option == null && !product.out_of_stock) 
        
            {
                showTip ('#add-to-cart-button', 'Please choose something then try again.');
                return false;
            }

	// Show cart loading...
        else $('#cart-messenger').html(cart_default_status);
        
        $('#cart-messenger').show();
        
        $("#add-to-cart-form").find('input[name="option_id"]').val(option.id);
        $("#add-to-cart-form").find('input[name="add"]').val(1);
        
        
        // Do query...


        $.ajax ({
        type: 'POST', 
        url: $('#ENV_SHOP_LANDING').val()+'cart/add/', 
        data: $("#add-to-cart-form").serialize(), 
        dataType: 'html', 
        success: function (data) {

                $("#cart-messenger").html(data);

                return false;
                }, 
        error: function (xhr) {
                        $("#cart-messenger").hide();
                        alert('Error', "Your item could not be added because there's been an error. Please try again at another time.");
                }
        });



	return false;
	
}









/*
 * 
 * [ FUNCTION ] removeFromCart
 * ____________________________________________________________________________
 * 
 * 
 * 
 */




function removeFromCart (cart_item_id) {
	

    
	// Show cart loading...
        //$('#cart-messenger').html(cart_default_status);
        
        //$('#cart-messenger').show();
        
        $('#cart-messenger-item-' + cart_item_id).fadeOut(200, function () {
        
            // Do query...

            $.ajax ({
            type: 'GET', 
            url: $('#ENV_SHOP_LANDING').val()+'cart/remove/', 
            data: { 'id': cart_item_id }, 
            dataType: 'html', 
            success: function (data) {

                    $("#cart-messenger").html(data);

                    return false;
                    }, 
            error: function (xhr) {
                            $('#cart-messenger-item-' + cart_item_id).fadeIn(200);
                            $("#cart-messenger").hide();
                            alert('Error', "This item could not be removed because there's been an error. Please try again at another time.");
                    }
            });
            
            
        });
        
        
        
        




	return false;
	
}







/*
 * 
 * [ FUNCTION ] addToCartQuick
 * ____________________________________________________________________________
 * 
 * 
 * 
 */




function addToCartQuick (option_id) {
	

	// Show cart loading...
        $('#cart-messenger').html(cart_default_status);
        
        $('#cart-messenger').show();
        
        var query = {
            a: 'add', 
            option_id: option_id, 
            add: 1
        };
        
        
        
        // Do query...


        $.ajax ({
        type: 'POST', 
        url: $('#ENV_SHOP_LANDING').val()+'cart/add/', 
        data: query, 
        dataType: 'html', 
        success: function (data) {

                $("#cart-messenger").html(data);

                return false;
                }, 
        error: function (xhr) {
                        $("#cart-messenger").hide();
                        alert('Error', "Your item could not be added because there's been an error. Please try again at another time.");
                }
        });



	return false;
	
}











/*
 * 
 * [ FUNCTION ] addToWishlist
 * ____________________________________________________________________________
 * 
 * 
 * 
 */




function addToWishlist () {
	


    var option = getSelection();
    
    // no selection: check if any option is available.
    if (option == null) 
        
            {
                showTip ('#add-to-wishlist-button', 'Please choose something then try again.');
                return false;
            }

        
        $("#add-to-cart-form").find('input[name="option_id"]').val(option.id);
        $("#add-to-cart-form").find('input[name="add"]').val(1);
        
        

        
        
        // Do query...


        $.ajax ({
        type: 'POST', 
        url: $('#ENV_SHOP_LANDING').val()+'wishlist/add.json', 
        data: $("#add-to-cart-form").serialize(), 
        dataType: 'json', 
        success: function (data) {
                console.log(data);
                $('#wishlist-item-count').html(countInEnglish(data.wishlist.items.length, 'item', 'items'));

                return false;
                }, 
        error: function (xhr) {
            
                        console.log(xhr.responseText);
                        showTip ('#add-to-wishlist-button', "The item could not be added to your wishlist. Please make sure you are signed in and then try again at another time.");
                }
        });



	return false;
	
}







/*
 * 
 * [ FUNCTION ] addWishlistItemToCart
 * ____________________________________________________________________________
 * 
 * 
 * 
 */




function addWishlistItemToCart (item_id, move) {
	

	// Show cart loading...
        $('#cart-messenger').html(cart_default_status);
        var item_row = $('#item-row-'+item_id).hide();
        
        $('#cart-messenger').show();
        
        var query = {
            a: (typeof move === 'boolean' && move === true) ? 'move-to-cart' : 'add-to-cart', 
            items: [item_id]
        }
        
        
        
        // Do query...


        $.ajax ({
        type: 'POST', 
        url: $('#ENV_SHOP_LANDING').val()+'wishlist', 
        data: query, 
        dataType: 'html', 
        success: function (data) {

                item_row.remove();
                $("#cart-messenger").html(data);

                return false;
                }, 
        error: function (xhr) {
                        $("#cart-messenger").hide();
                        item_row.show();
                        alert('Error', "Your item could not be added because there's been an error. Please try again at another time.");
                }
        });



	return false;
	
}

