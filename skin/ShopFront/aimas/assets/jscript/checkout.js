
var Checkout;

$('document').ready(function () {
    //$('select[name=country]').selectToAutocomplete();
    
    Checkout = classes.loadCheckout();
    
    $('#shipping-country-field').change(function () {
        Checkout.renderProvinceField(this);
    });
    
    
    
    
    

    $('#resend-confirm-form').submit(function ()
        {
                var form_backup = $('#resend-confirm-box').html();
                
                $('#resend-confirm-box').html('<span class="blink">Sending...</span>');

                // Do request...
                $.ajax ({
                        type: 'POST', 
                        url: $("#resend-confirm-form").prop('action'), 
                        data: $("#resend-confirm-form").serialize(), 
                        dataType: 'json', 
                        success: function(data) {

                                    if (data.status == 'error') {
                                            $('#resend-confirm-box').html(sanitizeDialogText(data.message));
                                            return false;
                                    } else if (data.status == 'bomb') {
                                            bombSession(data.message);
                                            return false;
                                    }
                                  
                                  $('#resend-confirm-box').html(sanitizeDialogText(data.message));
                                  return false;


                        }, 
                        error: function (xhr) {
                        
                        console.log(xhr.responseText);
                        
                        $('#resend-confirm-box').html(form_backup);
                        return false;
                        }

                });
                
                return false;

        });
    
    
    
});








/*
 * 
 * [ FUNCTION ] updateBillingInfoFrame 
 * ____________________________________________________________________________
 * 
 * 
 * 
 */


function updateBillingInfoFrame (address_required) {
    if (typeof (address_required) !== 'boolean') return false;
    $('#default-billing-info-message').hide();
    
    if (address_required) {
        $('#credit-card-billing-address').fadeIn(200);
        $('#billing-address-not-required').hide();
    } else {
        $('#billing-address-not-required').fadeIn(200);
        $('#credit-card-billing-address').hide();
    }
    
    return true;
}