


$('document').ready(function () {
    
            $('#layout-tile-grid-wrapper > li.layout-tile').each(function (i, obj) {

                makeSlideshow($(this).find('.slide-strip'), {duration: 300, interval: 3000, transition: 'push'});

              });
              
              
    
});






// Class: [ makeSlideshow ]
makeSlideshow = function (_target, _options) {
    
    // Private properties
    if ($(_target).find('li.slide').length < 2) return;

    _options = (_options != null && typeof _options === 'object') ? _options : {};
    
    _options.interval = (typeof _options.interval !== 'number' || parseInt(_options.interval) < 3000) ? (Math.random()*(6000-4000)+4000) : (Math.random()*(2000)+_options.interval);
    if (typeof _options.duration !== 'number') _options.duration = 200;
    if (typeof _options.transition !== 'string' || !$.inArray(_options.transition, ['fade', 'slide', 'push', 'slideUp'])) _options.transition = 'fade';
        
    var 
    
        _trans = function () {}, 
        _ghost_slide_css = {
        position: 'absolute',
        top: '0px', 
        left: '0px', 
        display: 'block', 
        'z-index': 5
        };

    switch (_options.transition) {
        case 'fade':
            _trans = _efx_fade;
            break;
        case 'slide':
            _trans = _efx_slide;
            break; 
        case 'slideUp':
            _trans = _efx_slide;
            break; 
        case 'push':
            _trans = _efx_push;
            break; 
        default:
            _trans = _efx_fade;
            break; 
    }
    
    
    function nextSlide() {

        _trans($(_target).find('li.slide').first()); // do transition
        
    }
    
    
    
    function _efx_fade (_obj) {
        
        var _next = _obj.next('li.slide').find('.slide-content-wrapper').clone().css(_ghost_slide_css);
        _obj.parent().after(_next);
        _obj.fadeOut(_options.duration, function () {
            $(_target).find('li.slide').last().after(_obj.detach()); // send slide 1 to back
            _obj.show();
            _next.remove();
        });
    }
    
    
    function _efx_slide_up (_obj) {
        
        var _next = _obj.next('li.slide').find('.slide-content-wrapper').clone().css(_ghost_slide_css);
        _obj.parent().after(_next);
        _obj.slideUp(_options.duration, function () {
            $(_target).find('li.slide').last().after(_obj.detach()); // send slide 1 to back
            _obj.show();
            _next.remove();
        });
    }
    
    
    
    function _efx_push (_obj) {
        
        var 
        
        _wrapper = $('<div></div>').css({
            
                            width: _obj.css('width'), 
                            height: _obj.css('height'), 
                            position: 'absolute', 
                            top: '0px', 
                            left: '0px', 
                            overflow: 'hidden', 
                            'z-index': 100
                        }), 
                        
        _strip = _wrapper.clone().css({width: parseInt(_obj.css('width'))*2, left: '-'+_obj.css('width')});
        _strip.append(_obj.next('li.slide').find('.slide-content-wrapper').clone().css({'float': 'left'}));
        _strip.append(_obj.find('.slide-content-wrapper').clone().css({'float': 'left'}));

        $(_target).find('li.slide').last().after(_obj.detach()); // send slide 1 to back
        
        _wrapper.prepend(_strip);
        _obj.parent().after(_wrapper);
        
        _strip.animate({left: '0px'}, _options.duration, function () {
            _wrapper.remove();
        });
        
        
        
    }
    
    
    
    function _efx_slide (_obj) {
        
        var 
        
        _wrapper = $('<div></div>').css({
            
                            width: _obj.css('width'), 
                            height: _obj.css('height'), 
                            position: 'absolute', 
                            top: '0px', 
                            left: '0px', 
                            overflow: 'hidden', 
                            'z-index': 100
                        }), 
                        
        _strip = _wrapper.clone().css({width: parseInt(_obj.css('width'))*2});
        _strip.append(_obj.find('.slide-content-wrapper').clone().css({'float': 'left'}));
        
        $(_target).find('li.slide').last().after(_obj.detach()); // send slide 1 to back
        
        _wrapper.prepend(_strip);
        _obj.parent().after(_wrapper);
        
        _strip.animate({left: '-'+_obj.css('width')}, _options.duration, function () {
            _wrapper.remove();
        });
        
        
        
    }
    
    
    setInterval(nextSlide, _options.interval);
    
    return;
    
    
    
    
    
    
    
 
    
    
    //Return public methods
    return {

        
    };
    
    
    
 }
 
 
 