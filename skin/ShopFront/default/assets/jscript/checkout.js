

$('document').ready(function () {
    $('select[name=country]').selectToAutocomplete();
});


/*
 * 
 * [ FUNCTION ] updateBillingInfoFrame 
 * ____________________________________________________________________________
 * 
 * 
 * 
 */


function updateBillingInfoFrame (address_required) {
    if (typeof (address_required) !== 'boolean') return false;
    $('#default-billing-info-message').hide();
    
    if (address_required) {
        $('#credit-card-billing-address').fadeIn(200);
        $('#billing-address-not-required').hide();
    } else {
        $('#billing-address-not-required').fadeIn(200);
        $('#credit-card-billing-address').hide();
    }
    
    return true;
}