




/*
 *
 * Initialize easyZoom for main product photo...
 *
 */
	
$('document').ready(function($){
    
    initZoom ();

});



/*
 * 
 * [ FUNCTION ] tabFocus 
 * ____________________________________________________________________________
 * 
 * For tabs in product details section.
 * 
 */


function tabFocus(tabId) {
	
	switch (tabId) {
		case 'tab-1':
			$('#tab-1').addClass('tab-state-front');
			$('#tab-2, #tab-3').removeClass('tab-state-front');
			break;

		case 'tab-2':
			$('#tab-2').addClass('tab-state-front');
			$('#tab-1, #tab-3').removeClass('tab-state-front');
			break;

		case 'tab-3':
			$('#tab-3').addClass('tab-state-front');
			$('#tab-1, #tab-2').removeClass('tab-state-front');
			break;
		default:
			$('#tab-1').addClass('tab-state-front');
			$('#tab-2, #tab-3').removeClass('tab-state-front');
			break;
			
	}
	return false;
	
}





/*
 * 
 * [ FUNCTION ] getStock 
 * ____________________________________________________________________________
 * 
 * Returns avaialable stock for [color + size] argument passed to it.
 * 
 */



function getSelection () {
    
    var option_pid = '';
    var item_not_selected = false;
    
    $('.product-attribute-control').each (function () {
        
        var val = $(this).val();
        if (parseInt(val) == 0 && !product.out_of_stock) item_not_selected = true; // break
        else option_pid += val;
        
    });
    
    // no selection: check if any option is available.
    if (parseInt(option_pid) != 0 && item_not_selected == false)
        {
            return product.options[product.attributes_to_options[option_pid]];
        }
        
    // color and size selected: check if selected size is available in selected color.
    else return null;
     
     
            
    
}








/*
 * 
 * [ FUNCTION ] getStock 
 * ____________________________________________________________________________
 * 
 * Returns avaialable stock for [color + size] argument passed to it.
 * 
 */



function getStock () {
    
    var option = getSelection();
    
    // no selection: check if any option is available.
    if (option != null && !product.out_of_stock)
        {
            return option.stock;
        }
        
    // color and size selected: check if selected size is available in selected color.
    else if (!product.out_of_stock) {
        
        return 1;
        
        } 
     
     
         
    return 0;
            
    
}






/*
 * 
 * [ FUNCTION ] getPrice 
 * ____________________________________________________________________________
 * 
 * Returns price for [color + size] option based on user's selection
 * 
 */



function getPrice (sale) {
    
    sale = (typeof sale === 'boolean') ? sale : false;
    
    var option = getSelection();
    
    // no selection: check if any option is available.
    if (option != null) { 
        return (sale) 
        ? option.salePrice 
        : option.price ;
    }
    else return product.base_price;
    
}







/*
 * 
 * [ FUNCTION ] getBasePrice 
 * ____________________________________________________________________________
 * 
 * Returns product base price.
 * 
 */



function getBasePrice () {
    
    try {
        
    return product.basePrice;
    
    } catch (e) {
        return 0;
    }
}





/*
 * 
 * [ FUNCTION ] onSale 
 * ____________________________________________________________________________
 * 
 * .
 * 
 */



function isOnSale () {
    
    try {
        
    return product.isOnSale;
    
    } catch (e) {
        return false;
    }
}







/*
 * 
 * [ FUNCTION ] getBasePrice 
 * ____________________________________________________________________________
 * 
 * Returns product option sale price.
 * 
 */



function getSalePrice () {
    

        return getPrice(true);
    
}




/*
 * 
 * [ FUNCTION ] toggleAddToCartButton 
 * ____________________________________________________________________________
 * 
 * Disables of enables 'add to cart' button.
 * 
 */



function toggleAddToCartButton (on_or_off) {
    
    if (on_or_off) 
        {
            $('#add-to-cart-button').show();
            $('#sold-out-button').hide();
        } else {
            $('#add-to-cart-button').hide();
            $('#sold-out-button').show();            
        }
        
    return false;
    
}






/*
 * 
 * [ FUNCTION ] checkAvailability 
 * ____________________________________________________________________________
 * 
 * 
 * 
 */



function checkAvailability () {
    
    
    // Check availability...
    var stock = getStock();
    
    // Show or hide 'add to cart' button...
    if (stock == 0) toggleAddToCartButton(false);
    else toggleAddToCartButton(true);
    
    // Update product price...
    $('#product-price-value').html(numberFormat(getPrice(), 2));
    
    // Update display picture
    enlargeOptionPreviewImage ();
        
    return false;
    
}





/*
 * 
 * [ FUNCTION ] enlargeProductPhoto 
 * ____________________________________________________________________________
 * 
 * 
 * 
 */


function enlargeProductPhoto (zoom_anchor) {
    
    $("#main-product-photo-zoom-anchor").prop('href', $(zoom_anchor).prop('href'));
    $("#main-product-photo").prop('src', $(zoom_anchor).attr('data_new_src'));
    
    initZoom ();
    
    return false;
}





/*
 * 
 * [ FUNCTION ] enlargeProductPhoto 
 * ____________________________________________________________________________
 * 
 * 
 * 
 */


function enlargeOptionPreviewImage () {
    
    var option = getSelection();
    
    // no selection: check if any option is available.
    if (option == null || option.image == false) {
        
        $("#main-product-photo-zoom-anchor").prop('href', product.primaryImage.zoom);
        $("#main-product-photo").prop('src', product.primaryImage.large);
        initZoom ();
        return false; 
    }



    $("#main-product-photo-zoom-anchor").prop('href', option.image.zoom);
    $("#main-product-photo").prop('src', option.image.large);
    initZoom ();
    
    return false;
}






/*
 * 
 * [ FUNCTION ] initZoom 
 * ____________________________________________________________________________
 * 
 * 
 * 
 */


function initZoom () {
    
	
	$('a.product-zoom').easyZoom({
		
			id: 'easy_zoom',
			parent: '#product-info-wrapper',
			append: true
	});
	
	
	$('#main-product-photo-wrapper').mouseenter(function () {
		$('#photo-zoom-icon').fadeOut(200);
		
	}).mouseleave(function () {
		$('#photo-zoom-icon').fadeIn(200);
	});
        
     return false;
}