/*
 *
 * CLASSES
 * ____________________________________________
 * 
 * PHP-STYLE CLASSES MUAAHHH!!!!
 *
 * REMINDER: Whenever a 'contructor' is wrapped in brackets like: "(function () {})()", 
 * it's a singuleton i.e. we only want / need one instance in the whole app!
 *
 */

var Cataleya = new Cataleya ();

function Cataleya () {
    
    
    var self = this;
    
    self.confirm = function (options)
        {

            release(); // In case user was previoulsy on hold...
            
            var default_options = {
               title: 'Confirm', 
               message: 'Please confirm...', 
               label_confirm: 'confirm', 
               label_cancel: 'cancel',
               callBack: function () {}
            };
            
            options = this.extend(default_options, options);
            

            // Remove previous click events...
            //__el(document.querySelectorAll('#confirm-box #confirm-button-ok, #confirm-box #confirm-button-cancel, #fullscreen-hold-bg')).off('click', cancel);


            // Add message...
            //message = sanitizeDialogText(message);   
            document.querySelector('#confirm-box .bubble-text').innerHTML = options.message;

            // Set title
            document.querySelector('#confirm-box .bubble-title').innerHTML = options.title;


            var confirm = function confirm () {
                __el(document.querySelectorAll('#confirm-box, #fullscreen-hold-bg')).css({ display: 'none' });
                hold('Please wait...');
                options.callBack(true); // .apply(this, arguments);
                
                return done(true);
            };
            
            var cancel = function cancel () {
                    
                    __el(document.querySelectorAll('#confirm-box, #fullscreen-hold-bg')).css({ display: 'none' });
                    
                    options.callBack(false); // .apply(this, arguments);
                    
                    return done(false);
            };
            
            
            


            // CONFIRM button init...
            var confirm_button = document.querySelector('#confirm-box #confirm-button-ok');
            confirm_button.addEventListener('click', confirm);
            confirm_button.innerHTML = options.label_confirm;
            
            // CANCEL button init...
            var cancel_button = document.querySelector('#confirm-box #confirm-button-cancel');
            cancel_button.innerHTML = options.label_cancel;
            __el(document.querySelectorAll('#confirm-box #confirm-button-cancel, #fullscreen-hold-bg')).addEventListener('click', cancel);



            
            
            var done = function done (ok) {
                    MyShortcuts.clear('cancel', cancel);
                    MyShortcuts.clear('return', confirm);
                    
                    __el(document.querySelectorAll('#confirm-box #confirm-button-cancel, #fullscreen-hold-bg')).removeEventListener('click', cancel);
                    document.getElementById('confirm-button-ok').removeEventListener('click', confirm);
                    
                    return ok;
            };



            // KEYBOARD shortcuts...
            MyShortcuts.register('return', confirm);
            MyShortcuts.register('cancel', cancel);
            


            // Show confirm bubble...
            __el(document.querySelectorAll('#confirm-box, #fullscreen-hold-bg')).css({ display: 'block' });

            return false;
        };
        
        
        
        
        
        
        self.extend = function (target, source) {
            target = target || {};
            
            for (var prop in source) {
                if (typeof prop === 'object') {
                    target[prop] = this.extend(target[prop], source[prop]);
                } else {
                    target[prop] = source[prop];
                }
            }
            
            return target;
        };
        
        
        return this;
        
        
}



function Exception (message) {
    this.message = message;
    this.name = "Exception";
}

var type_of = function (obj) {
    
    return ({}).toString.call(obj).match(/\s([a-zA-Z]+)/)[1].toLowerCase();
};



// ---------------------- POLYFILLS ------------------------ //


if (!Array.isArray) {
  Array.isArray = function(arg) {
    return Object.prototype.toString.call(arg) === '[object Array]';
  };
}


// Production steps of ECMA-262, Edition 5, 15.4.4.18
// Reference: http://es5.github.io/#x15.4.4.18
if (!Array.prototype.forEach) {

  Array.prototype.forEach = function(callback, thisArg) {

    var T, k;

    if (this === null) {
      throw new TypeError(' this is null or not defined');
    }

    // 1. Let O be the result of calling ToObject passing the |this| value as the argument.
    var O = Object(this);

    // 2. Let lenValue be the result of calling the Get internal method of O with the argument "length".
    // 3. Let len be ToUint32(lenValue).
    var len = O.length >>> 0;

    // 4. If IsCallable(callback) is false, throw a TypeError exception.
    // See: http://es5.github.com/#x9.11
    if (typeof callback !== "function") {
      throw new TypeError(callback + ' is not a function');
    }

    // 5. If thisArg was supplied, let T be thisArg; else let T be undefined.
    if (arguments.length > 1) {
      T = thisArg;
    }

    // 6. Let k be 0
    k = 0;

    // 7. Repeat, while k < len
    while (k < len) {

      var kValue;

      // a. Let Pk be ToString(k).
      //   This is implicit for LHS operands of the in operator
      // b. Let kPresent be the result of calling the HasProperty internal method of O with argument Pk.
      //   This step can be combined with c
      // c. If kPresent is true, then
      if (k in O) {

        // i. Let kValue be the result of calling the Get internal method of O with argument Pk.
        kValue = O[k];

        // ii. Call the Call internal method of callback with T as the this value and
        // argument list containing kValue, k, and O.
        callback.call(T, kValue, k, O);
      }
      // d. Increase k by 1.
      k++;
    }
    // 8. return undefined
  };
}



/* --------------- addEventListener -------------*/

if (!Element.prototype.addEventListener) {
  var oListeners = {};
  function runListeners(oEvent) {
    if (!oEvent) { oEvent = window.event; }
    for (var iLstId = 0, iElId = 0, oEvtListeners = oListeners[oEvent.type]; iElId < oEvtListeners.aEls.length; iElId++) {
      if (oEvtListeners.aEls[iElId] === this) {
        for (iLstId; iLstId < oEvtListeners.aEvts[iElId].length; iLstId++) { oEvtListeners.aEvts[iElId][iLstId].call(this, oEvent); }
        break;
      }
    }
  }
  Element.prototype.addEventListener = function (sEventType, fListener /*, useCapture (will be ignored!) */) {
    if (oListeners.hasOwnProperty(sEventType)) {
      var oEvtListeners = oListeners[sEventType];
      for (var nElIdx = -1, iElId = 0; iElId < oEvtListeners.aEls.length; iElId++) {
        if (oEvtListeners.aEls[iElId] === this) { nElIdx = iElId; break; }
      }
      if (nElIdx === -1) {
        oEvtListeners.aEls.push(this);
        oEvtListeners.aEvts.push([fListener]);
        this["on" + sEventType] = runListeners;
      } else {
        var aElListeners = oEvtListeners.aEvts[nElIdx];
        if (this["on" + sEventType] !== runListeners) {
          aElListeners.splice(0);
          this["on" + sEventType] = runListeners;
        }
        for (var iLstId = 0; iLstId < aElListeners.length; iLstId++) {
          if (aElListeners[iLstId] === fListener) { return; }
        }     
        aElListeners.push(fListener);
      }
    } else {
      oListeners[sEventType] = { aEls: [this], aEvts: [ [fListener] ] };
      this["on" + sEventType] = runListeners;
    }
  };
  Element.prototype.removeEventListener = function (sEventType, fListener /*, useCapture (will be ignored!) */) {
    if (!oListeners.hasOwnProperty(sEventType)) { return; }
    var oEvtListeners = oListeners[sEventType];
    for (var nElIdx = -1, iElId = 0; iElId < oEvtListeners.aEls.length; iElId++) {
      if (oEvtListeners.aEls[iElId] === this) { nElIdx = iElId; break; }
    }
    if (nElIdx === -1) { return; }
    for (var iLstId = 0, aElListeners = oEvtListeners.aEvts[nElIdx]; iLstId < aElListeners.length; iLstId++) {
      if (aElListeners[iLstId] === fListener) { aElListeners.splice(iLstId, 1); }
    }
  };
}








Cataleya.arrayFuncs = function () {
    
    function remove (needle, _a) {
        
            var key = _a.indexOf(needle);
            if (key > -1) _a.splice(key, 1);
            
            return _a;
    }


    function has(needle, _a) {
            for (var key in _a) { if (_a[key] == needle) return true;	}
            return false;
    }


    function search(needle, _a) {
            return _a.indexOf(needle);
    }
    
    function keys (_a) {
        var _keys = [];
        
        for (var key in _a) {
            if (key === 'length' || _a.hasOwnProperty(key)) continue;
            _keys.push(key);
        }
        
        return _keys;
    }
    
    
    
    function values (_a) {
        var _values = [];
        
        for (var key in _a) {
            if (key === 'length' || _a.hasOwnProperty(key)) continue;
            _values.push(_a[key]);
        }
        
        return _values;
    }
    
    
    function toArray (_a) {
        return values(_a);
    }
    
    
    return {
        remove: remove, 
        has: has, 
        search: search, 
        keys: keys, 
        values: values, 
        toArray: toArray
    };
 
 
};






 

Cataleya.getUtility = (function () {
    
        var 
        _auth_token, 
        _picker_data;

        $('document').ready (function () {
                if ($('#meta_token').length < 1) return;
                _auth_token = $('#meta_token').val();

                // Load picker data
                setTimeout (refreshPickerData, 1000); 

        });
        
        
        
        function refreshPickerData () {
                // Load picker data
                ajax.jsonRequest ({
                        method: 'POST', 
                        url: 'get-generic-picker-info.ajax.php', 
                        data: {auth_token: _auth_token },
                        success: function (response) {
                                    var data = response.json();
                                    
                                    _picker_data = data.payload;

                                    if (typeof callBack == 'function') callBack();
                                    return false;

                                },

                        error: function (xhr) {
                            return false;
                        }
                });
        }



        // [ getPickerData ]

        function getPickerData (_id) {
            if (typeof _picker_data[_id] !== 'undefined') {
                var _items = _picker_data[_id];
                for (var i in _items) _items[i].selected = '';

                return _items;
            }
            else throw new Exception ('Picker data for "' + _id + '" not found.');

            return {};
        }


    function getRange (_min, _max, _select, _ordinal) {
            if (typeof _min !== 'number' || typeof _max !== 'number') return {};
            
            var _array = [];
            var _ordinals = ['th', 'st', 'nd', 'rd', 'th', 'th', 'th', 'th', 'th', 'th'];
            
            _select = (typeof _select !== 'number') ? 0 : parseInt(_select);
            _min = parseInt(_min);
            _max = parseInt(_max);

            
            if (typeof _ordinal === 'boolean' && _ordinal === true) {
                for (var i=_min; i <= _max; i++) {
                   var 
                   _num_str = i+'', 
                   _len = parseInt(_num_str.length), 
                   _last_digit = parseInt(_num_str.substring(_len-1, 1)), 
                   _ord = (_len == 2) ? 'th' : _ordinals[_last_digit], 
                   _sel = (_select == i) ? 'selected=selected' : '';

                   _array.push({numeric: i, name: (_num_str+''+_ord), selected: _sel});
               }
            }
            else for (var i2=_min; i2 <= _max; i2++) {
                   var _sel2 = (_select == i2) ? 'selected=selected' : '';

                   _array.push({numeric: i2, name: i2, selected: _sel2});
            }
            
            return _array;
    }
    
    
    function getMonths (_select) {
        
        _select = (typeof _select !== 'number') ? 0 : parseInt(_select);
        var _array = [
            {name: 'Jan', numeric: '01'}, 
            {name: 'Feb', numeric: '02'},  
            {name: 'Mar', numeric: '03'},  
            {name: 'Apr', numeric: '04'},  
            {name: 'May', numeric: '05'},  
            {name: 'Jun', numeric: '06'},  
            {name: 'Jul', numeric: '07'},  
            {name: 'Aug', numeric: '08'},  
            {name: 'Sep', numeric: '09'},  
            {name: 'Oct', numeric: '10'},  
            {name: 'Nov', numeric: '11'},  
            {name: 'Dec', numeric: '12'}
        ];
        
        for (var i=0; i < _array.length; i++) {
            _array[i].selected = (_select == i) ? 'selected=selected' : '';
        }
        
        
        return _array;
        
    }
    
    
    
    
    function getFormData (_form) {
        var 
        form = $(_form).get(0), 
        _objct = {};
        
        
        for (var i = 0; i < form.elements.length; i++) {
                var field = $(form.elements[i]);
                var fieldName = field.prop('name').replace(/\-/g, '_');
                
                if (
                (field.prop('type') == 'checkbox' && field.prop('checked') == false) 
                || (field.prop('type') == 'radio' && field.prop('checked') == false)
                ) {
                    _objct[fieldName] = false;
                } else {
                    _objct[fieldName] = field.val();
                }
        }
        
        return _objct;
    }
    
    var _sing = {
                getRange: getRange, 
                getMonths: getMonths, 
                getFormData: getFormData, 
                getPickerData: getPickerData, 
                refreshPickerData: refreshPickerData
            };
    
    return function () {
    
            return _sing;
    
    };
    
    
})();







// ----------------------------------------------------------------------------------------------- ///


/*
 * 
 * [ ajax ]
 * ___________________________________________________________________
 * 
 * This is a wrapper for $.ajax()
 * 
 * 
 */


var ajax = (function () {
            
        var 
        
        self = this, 
        
        _dejavu = {}, 
        _template_cache = {}, 
        _auth_token, 
        _fp = $.jFingerprint.getHash(), 
        _rpc_id = 1;
            
            
        $('document').ready (function () {
            
                _auth_token = $('#meta_token').val();
        });
        
        
        
        
        self.fetch = function (url, options, keychain) {
            
            var 
            
            fetch = this, // this.fetch, 
            callBack = {
            success: function (response) {}, 
            error: function (xhr) {
                throw new Exception ('Connection failed: ' + xhr.responseText());

                }
            }, 
            headers = [], 
            defaultOptions = {
              method: 'GET', 
              async: true, 
              withCredentials: true, 
              headers: [] , 
              dataType: 'html', 
              contentType: 'application/x-www-form-urlencoded',
              data: null
            };


            if (typeof options.type === 'string' && typeof options.method === 'undefined') {
                options.method = options.type;
            }
            
            

            options = Cataleya.extend(defaultOptions, options);
            
            options.headers.forEach(function (h) {
                
                headers.push(h);
            
            }); 

            // url = _admin_root + url; // document.getElementById('meta_admin_root').value + url; // options.url;

            options.method = options.method.toLowerCase();
            if (typeof options.data !== 'string' && options.data !== null) {
                if (
                        (typeof options.data.whisper === 'undefined' || options.data.whisper === '') 
                        && typeof keychain !== 'undefined' 
                        && typeof keychain.whisper === 'string') options.data.whisper = keychain.whisper;
                options.data = this.param(options.data);
            } 
            
            else if (
                    typeof options.data === 'string'
                    && typeof keychain !== 'undefined' 
                    && typeof keychain.whisper === 'string') 
            {
                options.data += '&whisper=' + encodeURIComponent(keychain.whisper);
            }

            if (options.method === 'get' && typeof options.data === 'string')  {
                url += '?' + options.data;
                options.data = null;
            }

            if (options.method === 'post')  {
                headers.push({'name': 'Content-type', 'value': 'application/x-www-form-urlencoded;charset=UTF-8' });
            }

            headers.push({'name': 'X-Requested-With', 'value':  'XMLHttpRequest'});
            headers.push({'name': 'X-Cataleya-Auth-Token', 'value':  _auth_token});
            headers.push({'name': 'X-Cataleya-Finger-Print', 'value':  _fp});
            if (typeof keychain !== 'undefined') headers.push({'name': 'X-Cataleya-Request-Token', 'value':  keychain.rt});
 


            var core = {

                ajax: function (url, options, cb) {
                    

                                   var 

                                   xhr = getXHR(),  



                                   // Event Listeners...

                                   transferComplete = function transferComplete (e) {

                                       var status = (xhr.status === 1223) ? 204 : xhr.status;
                                       if (status < 100 || status > 599) {
                                         if (typeof options.error === 'function') cb.error(xhr);
                                         return;
                                       }

                                       if (status === 200) {


                                            var responseObj = {
                                                
                                                json: function () {
                                                    
                                                    try {
                                                        var data = JSON.parse(xhr.responseText);
                                                        return data;
                                                    } catch (e) {
                                                        throw new Error (xhr.responseText);
                                                    }
                                                    
                                                }, 
                                                responseText: function () {
                                                    return xhr.responseText;
                                                }, 
                                                status: function () {
                                                    return xhr.statusText;
                                                }
                                                
                                            };


                                            if (typeof cb.success === 'function') cb.success(responseObj, xhr.status, xhr);

                                          

                                       }
                                       else if (xhr.status !== 200) {

                                           if (typeof cb.error === 'function') cb.error(xhr);

                                       }
                                   },


                                   transferFailed = function transferFailed (e) {
                                     if (typeof cb.error === 'function') cb.error(xhr);
                                   }, 



                                   transferCancelled = function transferCancelled (e) {
                                     if (typeof cb.cancelled === 'function') cb.cancelled(xhr);
                                   };





                                   xhr.addEventListener("load", transferComplete, false);
                                   xhr.addEventListener("error", transferFailed, false);
                                   xhr.addEventListener("abort", transferCancelled, false);
                                   xhr.withCredentials = options.withCredentials;



                                   // Open connection...
                                   xhr.open(options.method, url, options.async);

                                   // Set headers...
                                   headers.forEach(function (h) {
                                       xhr.setRequestHeader(h.name, h.value);
                                   });



                                   if (typeof cb.beforeSend === 'function') cb.beforeSend(xhr);
                                   
                                   
                                   xhr.send(options.data);
                           }
                    };


                

                fetch.then = function (cb) {
                        if (typeof cb === 'function') callBack.success = cb;

                        return fetch;
                    };

                fetch.catch = function (cb) {
                    if (typeof cb === 'function') callBack.error = cb;
                    
                    return fetch;
                };
                
                setTimeout(function () {
                    core.ajax(url, options, callBack);
                }, 0);
                
                return fetch;


                
                
        };
        
        
        
        
        getXHR = function () {
                var XMLHttpObject = null;


                // Firefox, Opera 8+, Safari...

                try 
                        { XMLHttpObject = new XMLHttpRequest();		}

                catch (e)
                        {

                        // Internet Explorer...
                        try
                                { XMLHttpObject = new ActiveXObject ("Msxml2.XMLHTTP"); }


                        catch (e)
                                {
                                XMLHttpObject = new ActiveXObject ("Microsoft.XMLHTTP");

                                }

                        }



              return XMLHttpObject;
        };
        
        
        
        
        
        
        
        
        
        self.param = function param(a) {
            
	var prefix,
		s = [],
		add = function( k, v ) {
			// If value is a function, invoke it and return its value
			v = ( typeof v === 'function' ) ? v() : ( v === null ? "" : v );
			s[ s.length ] = encodeURIComponent( k ) + "=" + encodeURIComponent( v );
		};
                
                
	// If an array was passed in, assume that it is an array of form elements.
	if ( Array.isArray( a ) ) {
		// Serialize the form elements
		a.forEach(function(f) {
			add( f.name, f.value );
		});

	} else {
		// otherwise encode params recursively.
		for ( prefix in a ) {
			buildParams( prefix, a[ prefix ], add );
		}
	}
                

	// Return the resulting serialization
	return s.join( "&" ).replace( /%20/g, "+" );   
                
        };
        
        
        


        function buildParams( prefix, obj, add ) {
                var name;

                if ( Array.isArray( obj ) ) {
                        // Serialize array item.
                        obj.forEach(function( v, i ) {
                                if ( /\[\]$/.test( prefix ) ) {
                                        // Treat each array item as a scalar.
                                        add( prefix, v );

                                } else {
                                        // If array item is non-scalar (array or object), encode its
                                        // numeric index to resolve deserialization ambiguity issues.
                                        buildParams( prefix + "[" + ( typeof v === "object" ? i : "" ) + "]", v, add );
                                }
                        });

                } else if ( typeof obj === "object" ) {
                        // Serialize object item.
                        for ( name in obj ) {
                                buildParams( prefix + "[" + name + "]", obj[ name ], add );
                        }

                } else {
                        // Serialize scalar item.
                        add( prefix, obj );
                }
        }
        
        
        
        
        
        
        
        function handleData (dataType, data) {
            
            if (typeof data === 'string') return data;
            else return self.param(data);
            
            switch (dataType) {
                case 'form': 
                    
                    return self.param(data);
                    
                break;
                
                case 'json': 
                    
                    return JSON.stringify(data);
                    
                break;
                
                case 'query': 
                    
                    return self.param(data);
                    
                break;
                
                case 'text': 
                    
                    return encodeURI(data);
                    
                break;
                
                case 'plain': 
                    
                    return encodeURI(data);
                    
                break;
                
                case 'plain-text': 
                    
                    return encodeURI(data);
                    
                break;
                
                default:
                    
                    return self.param(data);
                    
                break;
                
            }
            
            
        };
        
        

            
            
        self.jsonRequest = function (options) {

            if (
                    // Required...
                    typeof options != 'object'
                    || typeof options.url != 'string' 
                    || typeof options.data == 'undefined' 

                ) { alert('AJAX error: Invalid parameters!'); return false; }
        
        
            var _options = {
               headers: [
                   { name: 'Accept', value: 'application/json' }
               ] 
            };
            
            
            Cataleya.extend(_options, options);
            


            _options._keychain = { 
                rt: Math.uuid(20),
                fp: _fp, 
                confirm_token: '', 
                whisper: ''
            };
            

            
            _options._isFresh = true;
            
            

            
            var freshOptions = bakeQuery(_options);
            
            
            // Send Request
            self.fetch(freshOptions.url, freshOptions.options, freshOptions.keychain).then(freshOptions.success).catch(freshOptions.error);
            //$.ajax (freshOptions);


            return true;


        };
        

        
        function bakeQuery (_options) {
            
            
            if (_options._isFresh) {
                
            
                    // Optional params
                    var tinyStatusSelector = (typeof _options.tinyStatus == 'string') ? _options.tinyStatus : '';
                    var callBack = (typeof _options.success == 'function') ? _options.success : function () {};
                    var errorCallback = (typeof _options.error == 'function') ? _options.error : function () {};




                    _dejavu[_options._keychain.rt] = {
                        
                        _isFresh: false,
                        _data: _options.data, 
                        _keychain: _options._keychain, 
                        _connectionSettings: {
                                options: {
                                async: ((typeof _options.async == 'boolean') ? _options.async : true),
                                method: ((typeof _options.type == 'string') ? _options.type : 'POST'), 
                                headers: ((typeof _options.headers !== 'undefined') ? _options.headers : []), 
                                dataType: 'json', 
                                contentType: ((typeof _options.contentType == 'string') ? _options.contentType : "application/x-www-form-urlencoded"),
                                    
                                }, 
                                url: _options.url, // ((/^http\:\/\//.test(_options.url)) ? '' : $('#meta_admin_root').val() ) + _options.url, 
                                beforeSend: ((typeof _options.beforeSend == 'function') ? _options.beforeSend : function () {}), 
                                success: function (response) {
                                                var jsonData = response.json();
                                                
                                                //console.log(jsonData);
                                                
                                                var _keychain;

                                                 jsonData.status = (typeof jsonData.status == 'string') ? jsonData.status.toLowerCase() : 'ok';

                                                 if (jsonData.status == 'ok') {

                                                     if (tinyStatusSelector != '') $(tinyStatusSelector).removeClass('error').toggleClass('blink').html('Saved.').delay(2000).html('&nbsp;');
                                                     // else alert('Ok', jsonData.message);
                                                     release();
                                                     callBack (response);

                                                 }

                                                 else if (jsonData.status == 'error') {
                                                     
                                                         
                                                         if (tinyStatusSelector != '' && false) $(tinyStatusSelector).removeClass('blink').addClass('error').html('Error!');
                                                         else alert ('Oops!', jsonData.message);

                                                         if (typeof errorCallback == 'function') errorCallback(response);


                                                         return false;
                                                 } 

                                                 else if (jsonData.status == 'bomb') {

                                                                 hold(jsonData.message);  
                                                                 window.location = 'login.php'; 
                                                                 return false;
                                                 }

                                                 else if (jsonData.status == 'confirm') {

                                                                 // _keychain.confirm_token = jsonData.confirm_token;

                                                                 _keychain = {
                                                                     confirm_token: jsonData.confirm_token, 
                                                                     rt: jsonData.rt, 
                                                                     whisper: ''
                                                                 };
                                                                 
                                                                 
                                                                 Cataleya.confirm({
                                                                     message: jsonData.message, 
                                                                     callBack: function (ok) {

                                                                     if (ok) {
                                                                         // [ OK ]
                                                                        var cache = _dejavu[_keychain.rt];
                                                                        
                                                                        // Flush cache
                                                                        _dejavu[_keychain.rt] = null;

                                                                        // Regenerate request token (rt)
                                                                        _keychain.rt = Math.uuid(20);
                                                                        
                                                                        // Replace keychain
                                                                        cache._keychain = _keychain;
                                                                        
                                                                        // back into the fridge
                                                                        _dejavu[_keychain.rt] = cache;

                                                                        // Re-bake opetions
                                                                        var replayOptions = bakeQuery(cache);

                                                                        // Send request
                                                                        //$.ajax(replayOptions);
                                                                        self.fetch(replayOptions.url, replayOptions.options, _keychain).then(replayOptions.success).catch(replayOptions.error);
                                                                        
                                                                    } else {
                                                                        // [ CANCEL ] // Flush cache
                                                                       _dejavu[_keychain.rt] = null;
                                                                    }

                                                                     }
                                                                 });
                                                                 

                                                                 return false;
                                                 }

                                                 else if (jsonData.status == 'require_password') {

                                                                 // _keychain.confirm_token = jsonData.confirm_token;

                                                                 _keychain = {
                                                                     confirm_token: jsonData.confirm_token, 
                                                                     rt: jsonData.rt, 
                                                                     whisper: ''
                                                                 };

                                                                 requirePassword(jsonData.message, _keychain, function (keychain, ok) {

                                                                     if (ok) {
                                                                         // [ OK ]
                                                                        var cache = _dejavu[keychain.rt];

                                                                        // Flush cache
                                                                        _dejavu[keychain.rt] = null;

                                                                        // Regenerate request token (rt)
                                                                        keychain.rt = Math.uuid(20);
                                                                        
                                                                        // Replace keychain
                                                                        cache._keychain = keychain;
                                                                        
                                                                        // back into the fridge
                                                                        _dejavu[keychain.rt] = cache;

                                                                        // Re-bake opetions
                                                                        var replayOptions = bakeQuery(cache);

                                                                        // Send request
                                                                        //$.ajax(replayOptions);
                                                                        self.fetch(replayOptions.url, replayOptions.options, keychain).then(replayOptions.success).catch(replayOptions.error);

                                                                        } else {
                                                                            // [ CANCEL ] // Flush cache
                                                                           _dejavu[keychain.rt] = null;
                                                                        }

                                                                     });


                                                                 return false;
                                                 }  

                                                 return false;

                                         },


                                  error: function (xhr) {
                                                 
                                                 throw new Exception ('Connection Error: ' + xhr.responseText);
                                                 
                                                 if (tinyStatusSelector != '' && xhr.status == 0) $(tinyStatusSelector).toggleClass('blink').html('Connection failed.');
                                                 else alert('Error: ' + xhr.responseText);
                                                 errorCallback (xhr);    

                                                 return false;

                                         }
                            }

                    };
            
            
            }
            
            var rt = _options._keychain.rt;
            _options = null; // Free up memory
            
            var _baked = _dejavu[rt]._connectionSettings;
            var _data = _dejavu[rt]._data;

            // Append request token
            if (typeof _data === 'string') _data += '&confirm_token=' + _dejavu[rt]._keychain.confirm_token;
            else if (typeof _data === 'object') _data['confirm_token'] = _dejavu[rt]._keychain.confirm_token;
            


            
            // Check data format
            _data = (typeof _baked.contentType == 'string' && /(application\/json)/.test(_baked.contentType)) ? JSON.stringify(_data) : _data;
            
            // Append data
            _baked.options.data = _data;
            _baked.keychain = _dejavu[rt]._keychain;
            
            return _baked;
            
        }
        
        
        
        
        
        


        // REQUIRE PASSWORD BUBBLE 

        function requirePassword (message, keychain, callBack)
        {

            release(); // In case user was previoulsy on hold...

            // Remove previous click events...
            $('#confirm-box #confirm-button-ok, #confirm-box #confirm-button-ok, #fullscreen-bg2').unbind('click');

            // message...
            message = sanitizeDialogText(message); 

            // Password box
            var passwordControl = '<input id="confirm-password" style="display:block; width:200px; margin:10px 0px;"  name="password" type="password" value="" tip="Please enter your password <strong>password</strong> <small>Your password is required to complete this operation.</small>" onblur="resetTextbox(this);"  onfocus="resetTextbox(this); hideTip();" size=24 /><br/>';

            // Set meessage
            $('#confirm-box .bubble-text').html(message + passwordControl);

            // Set title
            $('#confirm-box .bubble-title').html('Password Required');


            // Init confirm dialog 'confirm' button...
            
            // Init confirm dialog 'confirm' button...
            var confirmPasswordDialog = function confirmPasswordDialog () {
                    if ($('#confirm-box #confirm-password').prop('value') == $('#confirm-box #confirm-password').prop('defaultValue')) 
                    {
                        showTip('#confirm-box #confirm-password');
                        $('#bubble-1').css('z-index', 10002); // A small fix to bring tipBubble on top of confirm box
                        return false;
                    } else {

                        $('#confirm-box, #fullscreen-bg2').hide();
                        hold('Please wait...');

                        keychain.whisper = $('#confirm-box #confirm-password').val();
                        
                        callBack(keychain, true);
                        return false;
                    }
            };
            
            $('#confirm-box #confirm-button-ok').bind('click', confirmPasswordDialog);
            MyShortcuts.register('return', confirmPasswordDialog, {'disable_in_input':false});


            // Init confirm dialog 'cancel' button...
            var cancelPasswordDialog = function cancelPasswordDialog () {
                    $('#confirm-box, #fullscreen-bg2').fadeOut(300);
                    callBack(keychain, false);
                    MyShortcuts.clear('cancel', cancelPasswordDialog);
                    MyShortcuts.clear('return', confirmPasswordDialog);
                    return false;
            };

            // Init confirm dialog 'cancel' button...
            $('#confirm-box #confirm-button-cancel, #fullscreen-bg2').click(cancelPasswordDialog);
            MyShortcuts.register('cancel', cancelPasswordDialog);


            // Show confirm bubble...
            $('#confirm-box, #fullscreen-bg2').show();
            $('#confirm-box #confirm-password').focus();

            return false;
        }









        
        
        
        
        
        


        /*
         * 
         *  Private: [ FUNCTION ] _doAPICall
         * ____________________________________________________________________________
         * 
         * 
         * 
         */



        self.doAPICall = function (_options) {
            
                
                if (typeof _options['api'] !== 'string') { throw new Exception ('Invalid api key!'); return false; }
                
                

                var query = {
                    'id': _rpc_id++
                };
                // query = Cataleya.extend(query, _api_query_temp);

                
                // if (typeof _options['target_id'] !== 'undefined') query['target_id'] = _options['target_id'];
                if (typeof _options['do'] !== 'undefined') query['method'] = _options['do'];
                if (typeof _options['data'] !== 'undefined') query['params'] = _options['data'];
                
               
                if (
                        typeof _options['target_id'] !== 'undefined' && 
                        typeof _options['data']['target_id'] === 'undefined'
                        ) query['params']['target_id'] = _options['target_id'];
               
                // For tiny status...
                var tinyStatus = (typeof _options['tinyStatusSelector'] !== 'undefined') ? $(_options['tinyStatusSelector']) : null;
                if (tinyStatus === null && typeof _options['tinyStatus'] !== 'undefined') {
                    tinyStatus = $(_options['tinyStatus']);
                }
                
                if (typeof _options['uniLabel'] === 'undefined' || typeof _options['uniLabel'].status === 'undefined') _options['uniLabel'] = null; 
                

                if (
                        (tinyStatus === null && _options['uniLabel'] !== null) || 
                        (typeof _options['hold'] !== 'undefined' && _options['hold'] === true)) hold ('Please wait...');
               

                this.jsonRequest ({
                method: 'POST', 
                url: '../dashboard/rpc/'+_options['api'], 
                data: query, 
                dataType: 'json', 
                headers: [
                   { name: 'Accept', value: 'application/cataleya.1.0+json' }
                ],  
                success: function(response) {
                                
                            var data = response.json();
                            
                            if (data.result !== null) {
                            
                                if (tinyStatus !== null) { 
                                    tinyStatus.html('Saved.').toggleClass('blink');
                                    setTimeout(function () { tinyStatus.html('&nbsp;'); }, 1000);
                                }
                                
                                if (_options['uniLabel'] !== null) { 
                                    _options['uniLabel'].status
                                            .html('<span class="saved">Saved &nbsp; &nbsp; &nbsp;</span>');

                                    setTimeout(function () {
                                            _options['uniLabel'].status.hide();
                                            $(_options['uniLabel'].field).show();
                                        }, 1000);
                                }
                                
                                if (typeof _options['success'] == 'function') _options['success'](data.result);

                            } else {
                                if (typeof _options['error'] == 'function') _options['error'](data.error);
                                else alert('Error', data.error.message);
                            }


                            return false;

                        }, 
                error: function (xhr) {
                        if (typeof _options['error'] == 'function') _options['error'](xhr);

                        return false;
                }

                });

                return false;
        };
        
        
        
        
        
        self.loadTemplates = function (_template_keys, _force_reload) {
            
            _force_reload = (typeof _force_reload === 'boolean') ? _force_reload : false;
            
            
            // Fetch moustache templates
            var _ids = [];

            _template_keys.forEach(function (k) {
                
                        if (typeof window.localStorage !== 'undefined' && window.localStorage.getItem(k) !== null && !_force_reload) { 
                            if (typeof _template_cache[k] === 'undefined') _template_cache[k] = window.localStorage.getItem(k);

                        }
                        
                        else if (typeof _template_cache[k] === 'undefined' || _force_reload) _ids.push(k);
            });

            
            if (_ids.length > 0) {

                this.fetch ('get-mustache.ajax.php', {
                        method: 'GET', 
                        async: true, 
                        data: { id: _ids },
                        dataType: 'json'
                })
                .then(function (response) {

                        var data = response.json();

                        for (var name in data.templates) {
                            var content = data.templates[name];

                            _template_cache[name] = content;

                            if (typeof window.localStorage !== 'undefined') window.localStorage.setItem(name, content);



                        }


                        return false;

                        })
                .catch(function (xhr) { throw new TypeError ('Templates could not be retrieved: ' + xhr.responseText); } );

            }

                    
            return true;
            
        };
        
        
        
        
        
        self.getTemplate = function (_key) {
            
            // Fetch moustache templates
            //_key = _key.replace(/\-/g, '_');
            return (typeof _template_cache[_key] !== 'undefined') ? _template_cache[_key] : null;
        };
        
        
        
        
        return self;
        



})(); // Immediately invoke the function to create the singleton.










// ---------------------------------------------------------------------------------- //

/*
 * 
 * class: [ validator ]
 * 
 * 
 */


Cataleya.newValidator = function () {
    
    
            
            // PASSWORD METER
            function newPasswordMeter (passwordField, meter){

            var _meter = (typeof meter == 'string') ? ge(meter) : meter;
            var _target = (typeof passwordField == 'string') ? ge(passwordField) : passwordField;
            
            $(_target).blur(function () {
                        var m = $(_meter);
                        
                        m.find('li').css({'background-color': 'rgba(60, 60, 60, .7)' });
                        m.removeClass('blink').hide();
            })
            
            .focus(function () {
                // m.show();
            });
                
                
            $(_target).keyup(function () {

                        var v = $(_target).val();
                        var m = $(_meter);

                         if (v.length == 0) {
                                        m.removeClass('blink').fadeOut(300);
                                        return;
                        } 

                        var s = scorePassword(v);


                        if (s > 0) {
                                        m.find('li').css({'background-color': 'rgba(60, 60, 60, .7)' });
                                        m.find('li.password-strength-poor').css({'background-color': 'rgba(160, 0, 0, .8)' });
                                        m.show().addClass('blink');
                        } else {return; }

                         if (s > 40 || /([-_a-zA-Z0-9]{2,})*\s([-_a-zA-Z0-9]{2,})/.test(v)) {
                                        m.find('li').css({'background-color': 'rgba(60, 60, 60, .7)' });
                                        m.find('li.password-strength-weak').css({'background-color': 'rgba(160, 160, 0, .8)' });
                                        m.removeClass('blink');
                        } else {return; }

                         if (s > 60 || /([-_a-zA-Z0-9]{2,})*\s([-_a-zA-Z0-9]{2,})*\s([-_a-zA-Z0-9]{2,})/.test(v)) { // both lower and upper case...
                                        m.find('li').css({'background-color': 'rgba(60, 60, 60, .7)' });
                                        m.find('li.password-strength-good').css({'background-color': 'rgba(60, 160, 60, .8)' });
                                        m.removeClass('blink');
                        } else {return; }

                         if (s > 80 || /([-_a-zA-Z0-9]{4,})*\s([-_a-zA-Z0-9]{4,})*\s([-_a-zA-Z0-9]{4,})/.test(v)) {
                                        m.find('li').css({'background-color': 'rgba(60, 60, 60, .7)' });
                                        m.find('li.password-strength-excellent').css({'background-color': 'rgba(60, 200, 60, .8)' });
                                        m.removeClass('blink');
                        }

            });


        }
        
        


        // Password checker...

        function scorePassword(pass) {
            var score = 0;
            if (!pass)
                return score;

            // award every unique letter until 5 repetitions
            var letters = new Object();
            for (var i=0; i<pass.length; i++) {
                letters[pass[i]] = (letters[pass[i]] || 0) + 1;
                score += 5.0 / letters[pass[i]];
            }

            // bonus points for mixing it up
            var variations = {
                digits: /\d/.test(pass),
                lower: /[a-z]/.test(pass),
                upper: /[A-Z]/.test(pass),
                nonWords: /\W/.test(pass)
            }

            variationCount = 0;
            for (var check in variations) {
                variationCount += (variations[check] == true) ? 1 : 0;
            }
            score += (variationCount -1) * 10;

            return parseInt(score);
        }



        
        
        

        // FORM VALIDATOR
        function validateForm (form) {

                if (typeof form == 'string') form = ge(form);

                try {
                if (form == null) return null;
                } catch (e) {
                    return null;
                }

                var NAME1_REGEXP = /[^-\s\.A-Za-z0-9]/;
                var NAME_REGEXP = /[-\s\.A-Za-z0-9]{1,100}/;
                var PASSPHRASE_REGEXP = /([-_a-zA-Z0-9]{2,})*\s([-_a-zA-Z0-9]{2,})*\s([-_a-zA-Z0-9]{2,})/;
                var EMAIL_REGEXP = /^[^@\.]?[\w\._-]+@([\w]+)(\.[A-Za-z]{2,4})+$/;
                var PHONE_REGEXP = /^[+]?[\d]{6,16}$/;

                var PASSPHRASE_REGEXP2 = /([-_a-zA-Z0-9]+)*\s([-_a-zA-Z0-9]+)/;
                var PASSPHRASE_REGEXP3 = /([-_a-zA-Z0-9]+)*\s([-_a-zA-Z0-9]+)*\s([-_a-zA-Z0-9]+)/;
                var ISO_REGEXP = /^([A-Za-z]{2,4})$/;
                var ISO2_REGEXP = /^([A-Za-z]{2})$/;
                var ISO3_REGEXP = /^([A-Za-z]{2})$/;
                var DESCR_REGEXP = /[-\s\.A-Za-z0-9\W]{1,1500}/;

                var failedItems = [];
                var pass1, pass2;

                for (var i = 0; i < form.elements.length; i++) {
                        var field = form.elements[i];
                        if (typeof $(field).attr('require') !== 'string') continue;

                        switch ($(field).attr('require')) {

                                case 'name':
                                        if (field.value == '' || !NAME_REGEXP.test(field.value)) failedItems.push(field); 

                                        break;
                                        
                                case 'integer':
                                        if (field.value == '' || !NAME_REGEXP.test(field.value)) failedItems.push(field); 

                                        break;
                                        
                                case 'float':
                                        if (field.value == '' || !NAME_REGEXP.test(field.value)) failedItems.push(field); 

                                        break;

                                case 'description':
                                        if (field.value == '' || DESCR_REGEXP.test(field.value)) failedItems.push(field); 

                                        break;


                                case 'email':
                                        if (field.value == '' || !(EMAIL_REGEXP.test(field.value))) failedItems.push(field); 

                                        break;

                                case 'password':
                                        if ($.trim(field.value).length < 8) failedItems.push(field); 
                                        else if ((scorePassword(field.value) < 60) && !PASSPHRASE_REGEXP.test(field.value)) failedItems.push(field); 

                                        pass1 = field;
                                        break;

                                case 'password-confirm':
                                        pass2 = field;

                                        if (typeof pass1 != 'undefined' && typeof pass2 != 'undefined') {
                                            if (pass1.value != pass2.value) failedItems.push(pass2);
                                        }
                                        break;

                                case 'telephone':
                                        if (field.value == '' || !PHONE_REGEXP.test(field.value)) failedItems.push(field); 

                                        break;

                                case 'phone':
                                        if (field.value == '' || !PHONE_REGEXP.test(field.value)) failedItems.push(field); 

                                        break;

                                case 'tel':
                                        if (field.value == '' || !PHONE_REGEXP.test(field.value)) failedItems.push(field); 

                                        break;

                                case 'select':
                                        if (typeof field.selectedIndex == 'number' && field.selectedIndex == 0) failedItems.push(field); 

                                        break;


                                case 'iso':
                                        if (field.value == '' || !ISO_REGEXP.test(field.value)) failedItems.push(field); 

                                        break;

                                case 'iso2':
                                        if (field.value == '' || !ISO2_REGEXP.test(field.value)) failedItems.push(field); 

                                        break;

                                case 'iso3':
                                        if (field.value == '' || !ISO3_REGEXP.test(field.value)) failedItems.push(field); 

                                        break;

                                        default:

                                        // data type isn't know, just check if field is empty...
                                        if (field.value == '' && $(field).attr('require') != 'password-confirm') failedItems.push(field);

                                        break;
                        }


                }


                // Return failed items. Will be empty if everything is good.
                return failedItems;

        }





        return {
            newPasswordMeter: newPasswordMeter, 
            validateForm: validateForm, 
            scorePassword: scorePassword
        };




};
