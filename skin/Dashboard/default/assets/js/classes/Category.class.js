


// ----------------------------------------------------------------------------------------- //


// Class: [ loadCategory ]
Cataleya.loadCategory = function (callBack) {

    // Private properties

    var

    _dialogBox,
    _auth_token = $('#meta_token').val(),
    _category_id = 0,
    _a = Cataleya.arrayFuncs(),
    Utility = Cataleya.getUtility(),

    _query_temp = {
        auth_token: _auth_token,
        target_id: 0,
        'do': 'void',
        data: []

    };




    // Load moustache templates
    ajax.loadTemplates([
        'category-dialog',
        'category-tile'
    ], true);






    function renderDialog (data) {


                    $('.grp-dialog-box').remove(); // remove existing dialog boxes...

                    _dialogBox = $(Mustache.render(ajax.getTemplate('category-dialog'), data));

                    // Show dialog box...
                    $('body').append(_dialogBox);

                    _dialogBox.find('#category-status-button').click(function () {
                        var _checkbox = $('#category-status-checkbox');

                        if (_checkbox.prop('checked') == true) {
                            $(this)
                            .removeClass('online')
                            .addClass('offline')
                            .html('Off (Turn On)');

                            _checkbox.prop('checked', false);
                        } else {
                            $(this)
                            .removeClass('offline')
                            .addClass('online')
                            .html('On (Turn Off)');

                            _checkbox.prop('checked', true);
                        }


                        return false;

                    })
                    .end()
                    .show();



                    $('#fullscreen-bg2, .grp-dialog-box > .cancel-button').unbind('click').click(function () {

                       _dialogBox.hide().remove();

                       $('#fullscreen-bg2').unbind('click').hide();
                       $(document).unbind('scroll');
                       $('body').css({'overflow':'visible'});

                    })
                    .show();


    }








    // method: [ newCategory ]
    function newCategory (store_id) {



            _category_id = 0;
            var data = {
                title: 'New Category',
                description: 'No Description',
                store_id: store_id,
                is_active: false
            };

            ajax.doAPICall({
                api: 'category',
                target_id: 0,
                'do': 'newCategory',
                data: data,
                success: function (data) {
                    _category_id = data.Category.id;

                    var

                        _meta = {
                            title: 'New Category',
                            buttonLabel: 'save category'
                        };

                        renderDialog({meta: _meta, Category: data.Category, Parents: data.Parents});


                        $('#fullscreen-bg2, .grp-dialog-box > .cancel-button').click(function () {

                                ajax.doAPICall({
                                    api: 'category',
                                    target_id: _category_id,
                                    'do': 'deleteCategory',
                                    data: { store_id: 0, auto_confirm: true },
                                    success: function (data) {
                                        _category_id = 0;
                                        $('li#category-tile-'+data.Category.id).remove();
                                        return false;
                                    },

                                    error: function (xhr) {
                                        return false;
                                    }
                                });

                        });

                },

                error: function (xhr) {
                    return false;
                }
            });



            return false;

    }






    // method: [ editCategory ]
    function editCategory (id) {

            if (typeof id !== 'number') {throw new Exception ('Invalid category id!'); return false; }

            _category_id = parseInt(id);

            ajax.doAPICall({
                api: 'category',
                target_id: _category_id,
                'do': 'getInfo',
                data: { store_id: 0 },
                success: function (data) {
                    var _meta = {
                        title: 'Edit Category',
                        buttonLabel: 'save category'
                    };

                    renderDialog({meta: _meta, Category: data.Category, Parents: data.Parents});
                    return false;
                },

                error: function (xhr) {
                    return false;
                }
        });

        return false;

    }




    // method: [ saveCategory ]
    function saveCategory (id) {

            if (typeof id !== 'number') {throw new Exception ('Invalid category id!'); return false; }
            _category_id = parseInt(id);

            _dialogBox.hide();
            $('#fullscreen-bg2').hide();

            hold('Saving category ...');

            var data = Utility.getFormData($('#edit-category-form'));


            ajax.doAPICall({
                api: 'category',
                target_id: _category_id,
                'do': ((_category_id == 0) ? 'newCategory' : 'saveCategory'),
                data: data,
                success: function (data) {



                    _dialogBox.remove();
                     var new_tile = $(Mustache.render(ajax.getTemplate('category-tile'), data.Category));
                     var oldTile = $('li#category-tile-'+data.Category.id);

                     if (oldTile.length > 0) oldTile.before(new_tile);
                     else $('#new-category-tile').before(new_tile);


                     // In case we're modifing an existing tile, remove from list...
                     if (oldTile.hasClass('current')) new_tile.addClass('current');
                     oldTile.remove();

                     Utility.refreshPickerData();

                },

                error: function (xhr) {
                    _dialogBox.show();
                    $('#fullscreen-bg2').show();
                    return false;
                }
        });


    }






    // method: [ deleteCategory ]
    function deleteCategory (id, skip_confirm) {

            if (typeof id !== 'number') {throw new Exception ('Invalid category id!'); return false; }


            _category_id = 0;
            ajax.doAPICall({
                api: 'category',
                target_id: parseInt(id),
                'do': 'deleteCategory',
                data: { store_id: 0, auto_confirm: (typeof skip_confirm === 'boolean') ? skip_confirm : false },
                success: function (data) {

                    $('li#category-tile-'+data.Category.id).remove();
                    return false;
                },

                error: function (xhr) {
                    return false;
                }
        });

        return false;

    }











        /*
         *
         * [ saveDisplayPicture ]
         *
         */




        function saveBanner (_options) {
            uploader_engaged = true;


                ajax.doAPICall({
                    api: 'category',
                    target_id: _category_id,
                    'do': 'saveBanner',
                    data: _options,
                    success: function(data) {

                            uploader_engaged = false;

                            release(); //alert('Saved', data.message, true);
                            $('.banner-image-picker-add-button').css({
                                                                      'background': 'url(' + data.Category.banner.hrefs.large + ')',
                                                                      'background-size': 'contain',
                                                                      'background-repeat': 'no-repeat',
                                                                      'background-position': 'center center'

                                                                  });


                    },
                    error: function (xhr) {


                    }

                });


                return false;

        }





        function clearBanner (id) {

                if (typeof id !== 'number') {throw new Exception ('Invalid category id!'); return false; }
                _category_id = parseInt(id);

                ajax.doAPICall({
                    api: 'category',
                    target_id: _category_id,
                    'do': 'clearBanner',
                    data: { void: 0 },
                    success: function(data) {


                            release(); //alert('Saved', data.message, true);
                            $('.banner-image-picker-add-button').css({
                                                                      'background': 'url(' + data.Category.banner.hrefs.large + ')',
                                                                      'background-size': 'contain',
                                                                      'background-repeat': 'no-repeat',
                                                                      'background-position': 'center center'

                                                                  });


                    },
                    error: function (xhr) {


                    }

                });


                return false;

        }






    //Return public methods
    var self = {
        newCategory: newCategory,
        editCategory: editCategory,
        saveCategory: saveCategory,
        deleteCategory: deleteCategory,

        saveBanner: saveBanner,
        clearBanner: clearBanner

    };

    return self;



 }
