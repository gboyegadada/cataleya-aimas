
// Class: [ Provinces ]
Cataleya.loadProvinces = function (iso, callBack) {
    
    var _country_code = iso;
    // Private property
    var _collection = [ /* provinces */ ];
    
    
    var query = {};



    reloadProvinces(callBack);
    
    // method: [ refresh ]
    function reloadProvinces (callBack) {
        
	ajax.jsonRequest ({
		method: 'POST',  
		url: 'get-provinces.ajax.php', 
		data: {auth_token: $('#meta_token').val(), country_code: _country_code }, 
		dataType: 'json', 
		success: function (response) {
                            
                            var data = response.json();
                            setData(data.provinces);  
                            if (typeof callBack == 'function') callBack(data.provinces);
                            return false;
			},
			
		error: function (xhr) {
                    return false;
                }
        });
    }
    
    
    
    // private method: [ setData ]
    function setData (data) {
        _collection = data;
    } 
    
    
    
    // method: [ exists ]
    function exists (isoCode) {
        isoCode = isoCode.toUpperCase();
        
        for (var i = 0; i < _collection.length; i++) {
            if (_collection[i].iso == isoCode) return true;
        }
        
        
        return false;
    } 
    
    
    
    
    // method: [ getProvince ]
    function getProvince (id) {
        for (var i = 0; i < _collection.length; i++) {
            if (_collection[i].id == id) return _collection[i];
        }
        
        return {};
    } 
    
    
    
    
    // method: [ getProvinces ]
    function getProvinces () {
        return _collection;
    } 
    
    
    // method: [ getProvinceByIso ]
    function getProvinceByIso (isoCode) {
        for (var i = 0; i < _collection.length; i++) {
            if (_collection[i].iso == isoCode) return _collection[i];
        }
        
        return {};
    } 
    
    
    // private method: [ setProvinceSupport ]
    function setProvinceSupport (provinces_status, reload_page) 
    {
            hold ('Please wait...');

            query = {
                auth_token: $('#meta_token').val(),
                country_code: _country_code, 
                set: (provinces_status) ? 'on' : 'off'
            };

                
                


            // Do request...
            ajax.jsonRequest ({
                    method: 'POST', 
                    url: 'toggle-province-support.ajax.php', 
                    data: query, 
                    dataType: 'json', 
                    success: function (response) {
                            
                            var data = response.json();
                                    query.set = ''; 
                                    if (reload_page) location.reload(); 
                                    else release();
                                return false;
                            },

                    error: function (xhr) {
                                return false;
                            }
            });
    }
    
    
    // public method: [ enableProvinces ]
    function enableProvinces (reload_page) 
    {
        reload_page = (typeof reload_page == 'undefined') ? true : reload_page;
        setProvinceSupport(true, reload_page);
    }
    
    
    // public method: [ disableProvinces ]
    function disableProvinces (reload_page) 
    {
        reload_page = (typeof reload_page == 'undefined') ? true : reload_page;
        setProvinceSupport(false, reload_page);
    }
    
    
    // method: [ removeProvince ]
    function removeProvince (province_id) {
            
            hold ('Please wait...');
            
            query = {
                    auth_token: $('#meta_token').val(),
                    province_id: (typeof query.province_id == 'undefined' || query.province_id == 0) ? province_id : query.province_id

                    };




            // Do request...
            ajax.jsonRequest ({
                    method: 'POST', 
                    url: 'delete-province.ajax.php', 
                    data: query, 
                    dataType: 'json', 
                    success: function (response) {
                            
                            var data = response.json();
                                    $('#province-'+data.province.id).detach();
                                    
                                    var provinceCount1 = (data.provinceCount == 1) ? 'is ' : 'are ';
                                    provinceCount1 += countInEnglish(data.provinceCount, 'province', 'provinces');

                                    var provinceCount2 = (data.provinceCount > 0) ? '(' + data.provinceCount + ')' : '';

                                    $('.province-count-1').html(provinceCount1);
                                    $('.province-count-2').html(provinceCount2);
                                    
                                    query.province_id = 0; // reset id
                                    
                                    // Update cache and bring it back
                                    reloadProvinces(release);
                                return false;
                            },

                    error: function (xhr) {
                                query.province_id = 0; // reset id
                                return false;
                            }
            });
        }

    
    
    
    
    //Return public methods
    return {
        
        refresh: reloadProvinces, 
        exists: exists, 
        removeProvince: removeProvince,
        enableProvinces: enableProvinces,
        disableProvinces: disableProvinces,
        getProvince: getProvince, 
        getProvinces: getProvinces, 
        getProvinceByIso: getProvinceByIso
        
    };
    
    
    
 }







