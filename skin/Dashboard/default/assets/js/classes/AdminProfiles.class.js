



// ----------------------------------------------------------------------------------------- //


// Class: [ loadAdminProfiles ]
Cataleya.loadAdminProfiles = function () {
    
    // Private property
    var _collection = [ /* admin profilees */ ];
    
    
    var query = {};



    loadData();


    // method: [ refresh ]
    function loadData (callBack) {

        ajax.jsonRequest ({
                method: 'POST', 
                url: 'get-admin-profiles.ajax.php', 
                data: {auth_token: $('#meta_token').val() }, 
                dataType: 'json', 
                success: function (response) {
                            
                            var data = response.json();
                    
                            setData(data);
                            
                            // Recalculate page content height
                            var new_height = ((data.count / 3) * 300)+520;
                            $('.content').css('height', new_height);
                            
                            if (typeof callBack == 'function') callBack();
                            return false;

                        },

                error: function (xhr) {
                    return false;
                }
        });
    }

    
    // private method: [ setData ]
    function setData (data) {
        _collection = data.AdminProfiles;
    } 
    
    
    
    /*
     *
     * private method: [ renderDialog ]
     * 
     */
    
    $('#change-admin-password').change(function () {
        if ($('#change-admin-password').prop('checked')) {
            
            $('#text-field-admin-pass1, #text-field-admin-pass2').attr('DISABLED', false);
        }
        
        else {
            $('#text-field-admin-pass1, #text-field-admin-pass2').attr('DISABLED', true);
        }
    });

    function renderDialog (_id) {
    
            // Remove previous click events...
            $('#admin-profile-dialog-box #admin-profile-button-cancel, #fullscreen-bg2').unbind('click');

            var dialogBox = $('#admin-profile-dialog-box');

            // Set up dialog box...
            if (_id == 0) {

                // NEW
                dialogBox.find('.bubble-title').html('New Admin Profile');
                $('#hidden-field-admin-id').val(0);
                $('#text-field-admin-fname, #text-field-admin-lname, #text-field-admin-email, #text-field-admin-phone').val('');
                $('#text-field-admin-pass1').val('');
                $('#text-field-admin-pass2').val('');
                $('#select-field-admin-role-id option[value=0]').attr('selected', true);
                $('#admin-profile-button-ok').val('create new admin profile');
                $('#enable-admin-profile-radio, #change-admin-password').prop('checked', true);
                
                $('#admin-profile-form').find('input, select').attr('DISABLED', false);


            } else {

                // EDIT

                var AdminProfile = getAdminProfile(_id);
                console.log(AdminProfile);
                
                dialogBox.find('.bubble-title').html('Edit Admin Profile (' + AdminProfile.name + ')');
                $('#hidden-field-admin-id').val(_id);

                $('#text-field-admin-fname').val(AdminProfile.firstname);
                $('#text-field-admin-lname').val(AdminProfile.lastname);
                $('#text-field-admin-email').val(AdminProfile.email);
                $('#text-field-admin-phone').val(AdminProfile.phone);
                $('#text-field-admin-pass1').val('');
                $('#text-field-admin-pass2').val('');
                $('#select-field-admin-role-id option[value='+AdminProfile.role.id+']').attr('selected', true);
                
                if (AdminProfile.isActive) $('#enable-admin-profile-radio').prop('checked', true);
                else $('#disable-admin-profile-radio').prop('checked', true);
                
                $('#admin-profile-button-ok').val('save changes');
                

                
                // Prevent changes if profile is a super user acc.
                if (AdminProfile.role.isAdmin) $('#admin-profile-form').find('input, select').attr('DISABLED', true);
                else $('#admin-profile-form').find('input, select').attr('DISABLED', false);
                
                // disable password change by default...
                $('#change-admin-password').prop('checked', false);
                $('#text-field-admin-pass1, #text-field-admin-pass2').attr('DISABLED', true);
                
            }



            // Init dialog 'cancel' button...
            $('#admin-profile-dialog-box #admin-profile-button-cancel, #fullscreen-bg2').click(function () {
                    $('#admin-profile-dialog-box, #fullscreen-bg2').fadeOut(300);
                    hideTip();
                    return false;
            });

            // Show dialog box...
            $('#admin-profile-dialog-box, #fullscreen-bg2').show();

    }
    
    
    // method: [ sendRequest ]
    function sendRequest () {
        
            // Validate form
            var failedItems = Validator.validateForm('admin-profile-form');
            
            if (failedItems.length > 0) {
                
                for (var i = 0; i < failedItems.length; i++) {
                    if (failedItems[i].type == 'password' && $('#change-admin-password').prop('checked') == false) continue;
                    
                    showTip(failedItems[i]);
                    $(failedItems[i])
                    .focus()
                    .blur(function () { hideTip(); });
                    return false;
                    break;
                }

            }
            
            


            $('#admin-profile-dialog-box').hide();
            hold('Adding new admin profile...');

            ajax.jsonRequest ({
                    method: 'POST', 
                    url: $('#admin-profile-form').prop('action'), 
                    data: $('#admin-profile-form').serialize(), 
                    dataType: 'json', 
                    success: function (response) {
                            
                            var data = response.json();
                              
                              if (data.failedValidation) { 
                                  // Failed validation
                                  // Show dialog box...
                                  release();
                                  $('#admin-profile-dialog-box, #fullscreen-bg2').show(); 
                                  
                                    if (data.failedItems.length > 0) {
                                        var field = $('input[name=' + data.failedItems[0] + ']') ;
                                        if (field.length == 0) field = $('select[name=' + data.failedItems[0] + ']') ;
                                        
                                        if (field.length > 0) {
                                                showTip(field);
                                                $(field)
                                                .focus()
                                                .blur(function () { hideTip(); });
                                        
                                        }
                                    }
                                    
                                  return false; 
                              }
                              
                              // if (!data.AdminProfile.isNew) {location.reload(); return true; }



                              // Admin Profile Role
                              var _info = data.AdminProfile.role.name;
                              _info += ' (' + (data.AdminProfile.isActive ? 'Enabled' : 'Disabled') + ')';
                              var online_status = (data.AdminProfile.isOnline) ? 'online' : 'offline';


                              // Then add new tile...
                              var new_tile = $('<a href="profile.admin.php?id=' + data.AdminProfile.id + '" class="admin-profile-tile" id="admin-profile-' + data.AdminProfile.id + '"  ><div class="mediumListIconTextItem user-status ' + online_status + '"><span class="icon-user-4 tile-icon-large" ></span><div class="mediumListIconTextItem-Detail"><h4>' + data.AdminProfile.name + '</h4>' + 
                                                '<h6>' + _info + '</h6></div></div></a>'
                                    );


                               
                             // replace tile...
                             var oldTile = $('#admin-profile-'+data.AdminProfile.id);

                             if (oldTile.length > 0 && oldTile.prev('.admin-profile-tile').length > 0) oldTile.prev('.admin-profile-tile').after(new_tile);
                             else new_tile.prependTo('#admin-profile-grid-wrapper');


                             // In case we're modifing an existing tile, remove from list...
                             oldTile.detach(); 
                             
                             
                             // if we are on an admin profile page do this!
                            if (data.AdminProfile.isOnline && $('#profile-name-in-header').hasClass('offline')) { 
                                $('#profile-name-in-header').removeClass('offline').addClass('online');
                                $('#store-status-toggle-button').html('close this store');
                            }
                            else if (!data.AdminProfile.isOnline && $('#profile-name-in-header').hasClass('online')) { 
                                $('#profile-name-in-header').removeClass('online').addClass('offline');
                                $('#store-status-toggle-button').html('open this store');
                            }


                            // reload data then bring it back
                            loadData(function() {alert('Saved', 'Profile saved.', true)});

                            return false;


                            },

                    error: function (xhr) {
                        return false;
                    }
            });
            return false;

    }
    
    
    
    // method: [ newAdminProfile ]
    function newAdminProfile () {
        renderDialog(0);
        
        return true;
    } 
    
    
    // method: [ editAdminProfile ]
    function editAdminProfile (id) {
        renderDialog(id);
        
        return true;
    } 
    
    
    
    
    
    
    // method: [ getAdminProfile ]
    function getAdminProfile (id) {
        for (var i = 0; i < _collection.length; i++) {
            if (_collection[i].id == id) return _collection[i];
        }
        
        return {};
    } 
    
    

    

    
    // method: [ deleteAdminProfile ]
    function deleteAdminProfile (_id) {
            
            hold ('Please wait...');
            
            query = {
                    auth_token: $('#meta_token').val(),
                    admin_id: (typeof query.admin_id == 'undefined' || query.admin_id == 0) ? _id : query.admin_id

                    };
            


            // Do request...
            ajax.jsonRequest ({
                    method: 'POST', 
                    url: 'delete-admin-profile.ajax.php', 
                    data: query, 
                    dataType: 'json', 
                    success: function (response) {
                            
                            var data = response.json();
                                query.admin_id = 0; 
                                // $('#admin-profile-'+data.AdminProfile.id).detach();
                                hold('Please wait...')
                                window.location = 'profiles.admin.php';
                                
                                return false;

                            },

                    error: function (xhr) {
                                query.admin_id = 0; // reset id
                                return false;
                            }
            });
        }

    
    
    
    
    //Return public methods
    return {
        
        refresh: loadData, 
        deleteAdminProfile: deleteAdminProfile,
        getAdminProfile: getAdminProfile, 
        newAdminProfile: newAdminProfile, 
        editAdminProfile: editAdminProfile, 
        submitForm:  sendRequest
        
    };
    
    
    
 };
 
 
 






