
 
 
// ----------------------------------------------------------------------------------------- //


// Class: [ loadTaxRule ]
Cataleya.loadTaxRule = function (_type, callBack) {
    
    // Private properties
    
    var 
    
    _dialogBox, 
    _tax_rule_id = 0, 
    _a = Cataleya.arrayFuncs(), 
    Utility = Cataleya.getUtility(); 
    

    

    
    
    // Load moustache templates
    ajax.loadTemplates(['tax-rule-dialog']);
    
    var TaxRuleTile = '<li class="editable-list-tile trashable" id="tax-rule-{{id}}" trash_type="tax-rule-tile"  data_id="{{id}}" data_name="{{name}}" data_rate="{{rate}}" >' 
                                + '{{name}} ({{rate}}%)'
                                + '<a class="button-1 editable-list-edit-button fltrt" href="javascript:void(0)" onclick="TaxRule.editTaxRule({{id}});" >edit</a>' 
                               + '<a class="button-1 editable-list-delete-button fltrt" href="javascript:void(0)" onclick="TaxRule.removeTaxRule({{id}});" >remove</a></li>'





    
    
    
    function renderDialog (data) {


                    if (typeof _dialogBox !== 'undefined') _dialogBox.remove(); // remove existing dialog boxes...

                    _dialogBox = $(Mustache.render(ajax.getTemplate('tax-rule-dialog'), data));  

                    // Show dialog box...
                    $('body').append(_dialogBox);
                    
                    _dialogBox.find('#edit-tax-rule-form').submit(function () {
                        TaxRule.saveTaxRule(parseInt(data.TaxRule.id));
                        return false;
                    }).end().show();
                    

                    $('#fullscreen-bg2, #edit-tax-rule-dialog-box > .cancel-button').unbind('click').click(function () {

                       _dialogBox.hide().remove();
                           
                       $('#fullscreen-bg2').unbind('click').hide();
                       $(document).unbind('scroll'); 
                       $('body').css({'overflow':'visible'});	

                    })
                    .show();
                    

    }
    
   

    
    
    


    // method: [ newTaxRule ]
    function newTaxRule () {
        
        
            var 

            _zones = Utility.getPickerData('Zones'), 
            _default_zone = _zones[0], 
            
            _TaxRule = {
                id: 0, 
                title: 'New Tax Rule', 
                description: 'New Tax Rule', 
                type: _type, 
                priority: 0, 
                rate: 0, 
                Zone: _default_zone
            }, 
            
            _meta = {
                title: 'New Tax Rule', 
                buttonLabel: 'save tax rule'
            };
            
            renderDialog({meta: _meta, TaxRule: _TaxRule, Zones: _zones});
            _tax_rule_id = 0;
            
            return false;
        
    }
    
    
    
    


    // method: [ editTaxRule ]
    function editTaxRule (id) {
            
            if (typeof id !== 'number') {throw new Exception ('Invalid tax rule id!'); return false; }
            
            _tax_rule_id = parseInt(id);
            
            var _zones = Utility.getPickerData('Zones'); 
            
            ajax.doAPICall({
                api: 'tax-rule', 
                target_id: _tax_rule_id, 
                'do': 'getTaxRule', 
                data: { store_id: 0 }, 
                success: function (data) {
                    var _meta = {
                        title: 'Edit Tax Rule', 
                        buttonLabel: 'save tax rule'
                    };
                    
                    for (var i in _zones) {
                        _zones[i].selected = (_zones[i].id == data.TaxRule.Zone.id) ? 'selected' : '';
                    }

                    renderDialog({meta: _meta, TaxRule: data.TaxRule, Zones: _zones});
                    return false;
                },

                error: function (xhr) {
                    return false;
                }
        });
        
        return false;
        
    }
    
    
    
    
    // method: [ saveTaxRule ]
    function saveTaxRule (id) {
        
            if (typeof id !== 'number') {throw new Exception ('Invalid tax rule id!'); return false; }
            _tax_rule_id = parseInt(id);
            
            _dialogBox.hide();
            $('#fullscreen-bg2').hide();
            
            hold('Saving tax rule ...');
            
            var data = Utility.getFormData($('#edit-tax-rule-form'));

            
            ajax.doAPICall({
                api: 'tax-rule', 
                target_id: _tax_rule_id, 
                'do': ((_tax_rule_id == 0) ? 'newTaxRule' : 'saveTaxRule'), 
                data: data, 
                success: function (data) {
                    
            

                    _dialogBox.remove();
                    
                     var new_tile = $(Mustache.render(TaxRuleTile, data.TaxRule));
                     
                    new_tile.mouseenter(function () {
                                 $(this).find('.button-1').fadeIn('fast');
                             })
                             .mouseleave(function () {
                                 $(this).find('.button-1').hide();
                             });
                                        
                     var oldTile = $('li#tax-rule-tile-'+data.TaxRule.id);
                     
                    if (oldTile.length > 0) oldTile.after(new_tile);
                    else $('#tax-rule-list > li').eq(2).after(new_tile); 


                    $('#tax-rule-list > li.editable-list-tile.last-child').hide();


                     // In case we're modifing an existing tile, remove from list...
                     oldTile.remove(); 
                     
                     
//                    var population1 = (data.TaxRules.population > 0) ? '(' + data.TaxRules.population + ')' : '';
//
//                    $('.population-count-2').html(population1);
                    
                },

                error: function (xhr) {
                    _dialogBox.show();
                    $('#fullscreen-bg2').show();
                    return false;
                }
        }); 
        
        
    }
    
    
    
    
    

    // method: [ deleteTaxRule ]
    function deleteTaxRule (id) {
            
            if (typeof id !== 'number') {throw new Exception ('Invalid tax rule id!'); return false; }
            
            _tax_rule_id = 0;
            ajax.doAPICall({
                api: 'tax-rule', 
                target_id: parseInt(id), 
                'do': 'deleteTaxRule', 
                data: { store_id: 0 }, 
                success: function (data) {

                    $('li#tax-rule-tile-'+data.TaxRule.id).remove();
                    return false;
                },

                error: function (xhr) {
                    return false;
                }
        });
        
        return false;
        
    }
    
    
    
    
    


    
    
    
    
    //Return public methods
    return {
        newTaxRule: newTaxRule, 
        editTaxRule: editTaxRule, 
        saveTaxRule: saveTaxRule, 
        removeTaxRule: deleteTaxRule
        
    };
    
    
    
 }
 
 
 