
 
 
// ----------------------------------------------------------------------------------------- //


// Class: [ loadCatalog ]
Cataleya.loadCatalog = function () {
    
    // Private properties
    
    var 
    
    env = {
        
        mainChecklist: undefined, 
        searchChecklist: undefined, 
        mainGridActive: true
        
    }, 
    _auth_token = $('#meta_token').val(), 
    _selected = [],  
    _a = Cataleya.arrayFuncs(), 
    _utility = Cataleya.getUtility(), 
    _menu_status = false,
    
    _picker_data = [], 
    
    _query_temp = {
        auth_token: _auth_token,
        product_id: [], 
        'do': 'void', 
        target_id: 0
    }, 
            
    GRID_TYPE_MAIN = 0, 
    GRID_TYPE_SEARCH = 1;
    
    

    
    // Load moustache templates
    ajax.loadTemplates(['product-tile', 'generic-picker', 'generic-picker-multi', 'import-dialog'], true);
    
    loadData();
    
    
    
    $(document).ready(function () {

        initProductGrid(GRID_TYPE_MAIN);

        // Set shortcut...
        MyShortcuts.register('newItem', Catalog.newProduct.bind(Catalog));



    });
    
    
    
    

                            /*

                            SET DRAGGABLES

                            */
                           
                           
                           makeDraggableTile ($( '.product-tile-wrapper' ));
                           
                           
                           
                           

                            /*

                            CATEGORY DROPPABLE

                            */

                            $( ".droppable-tile" ).droppable({
                                    accept: '.product-tile-wrapper', 
                                    tolerance: 'pointer', // 'fit', 'intersect' (default), 'touch'...
                                    hoverClass: '.droppable-tile-drag-over-state', 
                                    activate: function ( event, ui ) {
                                        
                                                    
                                                    // $('.category-section-wrap').css({background:'#222'});

                                                    ui.helper
                                                    .css('cursor', closedHandCursor)
                                                    .find('.header').css('cursor', closedHandCursor).end()
                                                    .addClass('drag-state')
                                                    .animate({height: '60px'}, 600);
                                                    // .addClass('blink-2');
                                                    
                                                    var _id = ui.draggable.attr('data-tile-id');
                                                    env.mainGridActive 
                                                            ? env.mainChecklist.deselectAll().select(_id, false, false)
                                                            : env.searchChecklist.deselectAll().select(_id, false, false);

                                                    // if (ui.helper.hasClass('product-tile-wrapper')) ui.draggable.hide();

                                    }, 
                                    deactivate: function ( event, ui ) {

                                                    if (ui.helper.hasClass('product-tile-wrapper'))
                                                    {
                                                        // ui.draggable.css('cursor', openHandCursor);	 // .css('cursor', 'pointer')
                                                        // .show();
                                                        
                                                        ui.helper
                                                        .css('cursor', openHandCursor)
                                                        .find('.header').css('cursor', openHandCursor);
                                                    } 
                                                    
                                                    $('.category-section-wrap').css({background:'none'});
                                                    
                                                    


                                                    // ui.helper.css('cursor', openHandCursor).toggleClass('blink-2');
                                                    
                                                    


                                    }, 
                                    over: function ( event, ui ) {
                                        
                                        
                                                    $(event.target).css({background: '#047'});
                                        
                                        
                                    },
                                    out: function ( event, ui ) {
                                        
                                        
                                                    $(event.target).css({background: 'none'});
                                        
                                        
                                    },
                                    drop: function( event, ui ) {
                                            
                                            switch ($(event.target).attr('data_add_to'))
                                            {
                                                case 'category':
                                                    toggleCategory ($(event.target).attr('data_target_id'), true);
                                                    break;
                                                    
                                                case 'store':
                                                    toggleStore ($(event.target).attr('data_target_id'), true);
                                                    break;
                                                    
                                                case 'sale':
                                                    toggleSale ($(event.target).attr('data_target_id'), true);
                                                    break;
                                                    
                                                case 'coupon':
                                                    toggleCoupon ($(event.target).attr('data_target_id'), true);
                                                    break;
                                                    
                                                    
                                            }
                                            
                                            
                                            
                                            
                                            $(event.target)
                                            .css({background: '#330'})
                                            .addClass('blink-fast');
                                            
                                            setTimeout(function () {
                                                
                                                $(event.target)
                                                .removeClass('blink-fast')
                                                .css({background: 'none'})
                                            }, 650);
                                            
                                        }

                                    });
                                    
                                    
                                    
                                    
                                    
                                    
                                    
                                    


                            /*

                            RECYCLE BIN


                            $( "#main-recycle-bin" ).droppable({
                                    accept: '.trashable', 
                                    hoverClass: 'recycle-bin-hover-state', 
                                    activate: function ( event, ui ) {
                                        

                                                    $('#main-recycle-bin-wrapper').slideDown(300);

                                                    $( "#main-recycle-bin" ).fadeTo(300, 1);

                                    }, 
                                    deactivate: function ( event, ui ) {

                                                    $('#main-recycle-bin-wrapper').slideUp(300);
                                                    $( "#main-recycle-bin" ).fadeTo(300, .4);



                                    },
                                    drop: function( event, ui ) {

                                        switch (ui.draggable.attr('trash_type'))
                                        {

                                            case 'product-tile':
                                                deleteProducts ([ $(event.target).attr('data_target_id') ]);
                                                break;
                                                
                                            default:
                                                // do nothing
                                                ui.draggable.show();
                                                break;

                                        }

                                    }
                            });



                            */
                                                                
                                    
                                    
                                    


    
    
    
    
    
    
    
    
    
    
    function loadData (callBack) {
        
    


    }
    
    
    
    function makeDraggableTile (tile) {

     tile = $( tile );
     
     tile
     .css('cursor', openHandCursor)
     .find('.header').css('cursor', openHandCursor).end()
     .draggable({
                                             revert: 'invalid', 
                                             revertDuration: 200,
                                             appendTo: "body", 
                                             containment: 'document',  
                                             opacity: 0.5, 
                                             zIndex: 12700, 
                                             helper: "clone"
                                             });  
                                             
     return tile;

    }
    
    
    // [ getPickerData ]
    
    function getPickerData (_id) {
        return _utility.getPickerData(_id);
    }





    // Context Menu
    function toggleContextMenu (showOrHide) {
        var _menu = $('#catalog-context-menu');
        
        if (typeof showOrHide !== 'boolean') showOrHide = !_menu_status;
        
        
        
        function handler_showContextMenu () { 

             $('#fullscreen-bg2').unbind('click').click(function () {
                toggleContextMenu(false);
             })
             .show();
             
            _menu.fadeIn(200); _menu_status = true; 
            
            MyShortcuts.register('cancel', handler_hideContextMenu);
        }
        
        
        function handler_hideContextMenu () { 
            _menu.fadeOut(200); _menu_status = false;
            
            $('#fullscreen-bg2').unbind('click').hide();
            $(document).unbind('scroll'); 
            $('body').css({'overflow':'visible'});
            
            MyShortcuts.clear('cancel', handler_hideContextMenu);
        }
        
        
        
        if (showOrHide === true && showOrHide !== _menu_status) { 
            
            handler_showContextMenu ();
        
        }
        else if (showOrHide === false && showOrHide !== _menu_status) { 
            
            handler_hideContextMenu ();
        
        }
        
    }
    
    
    
    function toggleSideBarSection(_header) {
        _header = $(_header);
        
        var status = ($.inArray(_header.next('li').css('display'), ['inline', 'block']) > -1);
            
        $('li.sb-section-header').parent('a').next('li').slideUp(200);
        
        $('li.sb-section-header > span')
        .removeClass('icon-minus-3')
        .removeClass('icon-plus-3')
        .addClass('icon-plus-3');
        


        var _symbol = _header.find('.sb-section-header > span');
        
        if (status) {
            _header.next('li').slideUp(200);
            _symbol.removeClass('icon-minus-3').addClass('icon-plus-3');
        }
        else {
            _header.next('li').slideDown(200);
            _symbol.removeClass('icon-plus-3').addClass('icon-minus-3');
        }
    }
    
    
    
    
    
    
    
    
    /*
     * 
     * [ FUNCTION ] initProducts
     * ____________________________________________________________________________
     * 
     * Self explanatory.
     * 
     */


    function initProductGrid (grid_type)
    {


            
            if ((env.mainGridActive ? env.mainChecklist : env.searchChecklist) !== undefined) deselectAll(true);
                
            // INIT PRODUCT OPTIONS CHECKLIST
            
            env.mainGridActive = (grid_type === GRID_TYPE_MAIN);
            
            var 
            
                checklistContainer = env.mainGridActive 
                                    ? document.querySelector('#main-tile-grid > .tiles-wrapper') 
                                    : document.querySelector('#quick-search-tile-grid > .tiles-wrapper'), 
                                            
                button_SelectAll = __el(document.getElementById('global-select-button')), 
                button_deleteSelected = __el(document.getElementById('global-delete-button')), 
                button_hideSelected = __el(document.getElementById('global-hide-button')), 
                button_showSelected = __el(document.getElementById('global-show-button')), 
                theList = (env.mainGridActive ? env.mainChecklist : env.searchChecklist);
            
            if (typeof theList !== 'undefined') { 
                
                
                theList.deselectAll();
                theList.destroy();
                theList = null;
            }
            
            theList = new Cataleya.CheckList(checklistContainer);
            var deselectAllHandle = theList.deselectAll.bind(theList);
            
            
            theList
            
            .on('onChange', function () {
                document.querySelector('#task-bar-menu-button > .count').innerHTML = theList.count();
            })
            .on('onFirstCheck', function () {
                MyShortcuts.register('cancel', deselectAllHandle);
                MyShortcuts.register('deleteItem', deleteProductHandle);
                MyShortcuts.register('open', editProductHandle);
                
                MyShortcuts.register('shift', shiftKeyHandler);
                
                $('li.list-table-header').hide(); 
                $('li#task-bar').slideDown(200); //fadeIn(200);
                
            })
            .on('onAllUnchecked', function () {
                button_SelectAll.removeClass('active');
                MyShortcuts.clear('cancel', deselectAllHandle);
                MyShortcuts.clear('deleteItem', deleteProductHandle);
                MyShortcuts.clear('open', editProductHandle);
                MyShortcuts.clear('shift', shiftKeyHandler);
                
                button_SelectAll.item(0).querySelector('.button-label').innerHTML = 'Select All';
                
                $('li#task-bar').slideUp(200, function () {
                   $('li.list-table-header').slideDown(200); 
                });
                
            })
            .on('onAllChecked', function () {
                
                button_SelectAll.querySelector('.icon-checkmark').addClass('active');
                button_SelectAll.item(0).querySelector('.button-label').innerHTML = 'Deselect All';
                
            })
            .on('onLastUnchecked', function () {
                
                
                button_SelectAll.querySelector('.icon-checkmark').removeClass('active');
                button_SelectAll.item(0).querySelector('.button-label').innerHTML = 'Select All';
                
            
            });
            
            




            function deleteProductHandle () {

                MyShortcuts.clear('deleteItem', deleteProductHandle);
                Catalog.deleteProducts();

            };
            
            function editProductHandle () {
                return;

                MyShortcuts.clear('open', editProductHandle);
                if (theList.count() > 1) { 
                    Catalog.editProduct(theList.getSelected());
                    theList.deselectAll();
                } else  { 
                    productWidget.editProduct(theList.getLastSelected());
                    theList.deselectAll();
                }

            };
                
                
            function shiftKeyHandler (e) {
                if (e.shiftKey) {
                    // Keydown...

                    //button_SelectAll.querySelector('.icon-checkmark').addClass('active');
                    button_SelectAll.item(0).querySelector('.button-label').innerHTML = 'Deselect All';


                } else {
                    // Keyup...
                    button_SelectAll.item(0).querySelector('.button-label').innerHTML = 'Select All';
                }
            };
            
            
            
            
            function hideSelectedHandler () {
                if (theList.count() > 0) self.toggleProductDisplay(false);
            };
            
            
            
            
            function showSelectedHandler () {
                
                if (theList.count() > 0) self.toggleProductDisplay(true);

            };
                
                
                
            button_SelectAll.addEventListener('click', toggleAllSelections);
            button_deleteSelected.addEventListener('click', deleteProductHandle);
            button_hideSelected.addEventListener('click', hideSelectedHandler);
            button_showSelected.addEventListener('click', showSelectedHandler);
            //toolboxContainer.querySelector('a.edit-selected-product-options-button').addEventListener('click', editProductHandle);



            // INIT: Task Buttons...
            theList.forEach(function (obj) {
                
                obj.querySelector('.select-button').addEventListener('click', function toggleSelectionHadler () {
                        
                        theList.toggleSelection(obj.getAttribute('data-tile-id'));
                });

                obj.querySelector('.delete-button').addEventListener('click', function deleteSelectionHadler () {
                        Catalog.deleteProducts(obj.getAttribute('data-tile-id'), false, false);
                });

                var vb = obj.querySelector('.toggle-visibility-button');
                vb.addEventListener('click', function editSelectionHadler () {
                    
                        var hidden = (parseInt(vb.getAttribute('data-hidden')) === 1);
                        Catalog.toggleProductDisplay(hidden, parseInt(obj.getAttribute('data-tile-id')));
                        
                });
                
                obj.querySelector('.edit-button').addEventListener('click', function editSelectionHadler () {
                        productWidget.editProduct(obj.getAttribute('data-tile-id'), theList.deselectAll);
                });

            });
            
            
            env.mainGridActive ? env.mainChecklist = theList : env.searchChecklist = theList;
            
            return theList;
    }





    
    
    // Select
    function toggleSelection() {
        
        
        (env.mainGridActive ? env.mainChecklist : env.searchChecklist).toggleSelection.apply(arguments);
        
    }
    
    
    // isSelect
    function isSelected() {

        return (env.mainGridActive ? env.mainChecklist : env.searchChecklist).isSelected.apply(arguments); // _a.has($(_tile).attr('data_product_id'), _selected);
        
    }
    
    
    
    // toggleAllSelections
    function toggleAllSelections() {
        
        (env.mainGridActive ? env.mainChecklist : env.searchChecklist).toggleAllSelections.apply(arguments);
    }
    
    
    
    /**
     * 
     * @param {Boolean} _render
     * @param {Boolean} triggerEvent
     * @returns {Boolean}
     * 
     */
    function deselectAll(_render, triggerEvent) {
        
        
          
        (env.mainGridActive ? env.mainChecklist : env.searchChecklist).deselectAll.apply(arguments);
          
        return false;
        
    }
    
    
    
    // deselectAll
    function selectAll() {
        
        
        (env.mainGridActive ? env.mainChecklist : env.searchChecklist).selectAll.apply(arguments);
        
       
        
    }
    
    
    
    


    /*
     * 
     * [ FUNCTION ] deleteProducts 
     * ____________________________________________________________________________
     * 
     * Self explanatory.
     * 
     */



    function deleteProducts (_id) {

        var _selected;

        if (typeof _id !== 'undefined') { 


            _selected = env.mainGridActive 
                    ? env.mainChecklist.deselectAll(false, false).select(_id, false, false).getSelected() 
                    : env.searchChecklist.deselectAll(false, false).select(_id, false, false).getSelected();

        } else {

            _selected = env.mainGridActive 
                    ? env.mainChecklist.getSelected() 
                    : env.searchChecklist.getSelected();

        }
        
        
        _doAPICall({
            'do': 'deleteProduct', 
            'success': function (data) {
                
                for (var id in data.Categories) $('#category-population-'+data.Categories[id].id).html(data.Categories[id].population);

                var _item_num = parseInt($('#main-tile-grid > .tiles-wrapper > div.product-tile-wrapper').attr('item_number'));
                var _item_num2 = parseInt($('#quick-search-tile-grid > .tiles-wrapper > div.product-tile-wrapper').attr('item_number'));
                
                for (var i in _selected) {

                        var _id = _selected[i];

                        // Main Tile Wrapper

                        $('#main-tile-grid > .tiles-wrapper').find('#product-tile-'+_id).hide(200, function () { 
                            $(this).remove();
                        });


                        // Quick Search Tile Wrapper
                        $('#quick-search-tile-grid > .tiles-wrapper').find('#product-tile-'+_id).hide(200, function () { 
                            $(this).remove();
                        });

                 }
                 
                 deselectAll();
                     
                     
                setTimeout (function () {
                    $('#main-tile-grid > .tiles-wrapper > div.product-tile-wrapper').each(function (i, obj) {
                          $(this).attr('item_number', _item_num++);
                      });

                    $('#quick-search-tile-grid > .tiles-wrapper > div.product-tile-wrapper').each(function (i, obj) {
                          $(this).attr('item_number', _item_num2++);
                      });
                }, 205);


                }
         });

            return false; 

    }







        





 
        /*
         * 
         * [ FUNCTION ] newProduct 
         * ____________________________________________________________________________
         * 
         * Self explanatory.
         * 
         */



        function newProduct () {
            

            
            var createProduct = function (data) {
                
                    env.mainGridActive 
                            ? env.mainChecklist.deselectAll() 
                            : env.searchChecklist.deselectAll();


                    ajax.doAPICall({
                        api: 'catalog', 
                        'do': 'newProduct',
                        'data': data, 
                        'success': function (data) {

                            //var data = response.json();

                            for (var id in data.Categories) $('#category-population-'+data.Categories[id].id).html(data.Categories[id].population);

                            var new_tile = productWidget.makeTile(data.Product);                             
                            $('.product-tile-add-button').after(new_tile); 

                            
                            makeDraggableTile(new_tile);


                            var _item_num = parseInt($('#main-tile-grid > .tiles-wrapper > div.product-tile-wrapper').attr('item_number'));
                            setTimeout (function () {
                                $('#main-tile-grid > .tiles-wrapper > div.product-tile-wrapper').each(function (i, obj) {
                                      $(this).attr('item_number', _item_num++);
                                  });

                            }, 205);
                            
                            initProductGrid(GRID_TYPE_MAIN);


                            productWidget.editProduct (data.Product.id);

                        }
                    });
                
                };
                
                

                renderMultiPickerDialog ({
                    _do: true, 
                    callBack: createProduct,

                    payload: {
                        title: 'New Product', 
                        description: 'What kind of product would you like to create?', 
                        entityName: 'product', 
                        entities: [ 
                            { 
                                input_name: 'product_type_id', 
                                entity_name: 'Product Type', 
                                options: getPickerData('ProductTypes')
                            },  
                            { 
                                input_name: 'manufacturer_id', 
                                entity_name: 'Manufacturer', 
                                options: getPickerData('Manufacturers')
                            },  
                            { 
                                input_name: 'supplier_id', 
                                entity_name: 'Supplier', 
                                options: getPickerData('Suppliers')
                            } 
                       ], 

                        buttonLabel: 'Add New Product'
                    }

                });


                return false; 

        }
        
     



     






 
 
        /*
         * 
         * [ FUNCTION ] deleteProduct 
         * ____________________________________________________________________________
         * 
         * Self explanatory.
         * 
         */



        function toggleProductDisplay (_status, _id) {
                var _selected, _render = false;
                
                if (typeof _id !== 'undefined') { 
                    
                
                    _selected = env.mainGridActive 
                            ? env.mainChecklist.deselectAll(false, false).select(_id, false, false).getSelected() 
                            : env.searchChecklist.deselectAll(false, false).select(_id, false, false).getSelected();
                            

                } else {

                    _selected = env.mainGridActive 
                            ? env.mainChecklist.getSelected() 
                            : env.searchChecklist.getSelected();
                            
                    _render = true;
                            
                }
                
                _doAPICall({
                    'do': ((_status === true) ? 'showProduct' : 'hideProduct'), 
                    'success': function (data) {


                            // INIT: Task Buttons...
                            (env.mainGridActive ? env.mainChecklist : env.searchChecklist).forEach(function (obj) {
                                    var vb = obj.querySelector('.toggle-visibility-button');
                                    
                                    vb.setAttribute('data-hidden', _status ? 0 : 1);

                                    if (_status) { 
                                    __el(vb.querySelector('.hide-icon')).removeClass('icon-eye-4').addClass('icon-minus-5');
                                    vb.querySelector('.button-label').innerHTML = 'Hide';

                                    } else {
                                    __el(vb.querySelector('.hide-icon')).removeClass('icon-minus-5').addClass('icon-eye-4');
                                    vb.querySelector('.button-label').innerHTML = 'Show';
                                    }
                                    

                            }, true);
                            
                            deselectAll(_render, false);
                            
                            
                            
                            
                            

                        }
                });
                
                


                return false; 

        }
        
        
        
        
        
        
        
        
        
        

        /**
         * 
         * @param {Boolean} _status
         * @param {Integer|Array} _id
         * @returns {Boolean}
         * 
         */
        function toggleFeaturedStatus (_status, _id) {
                var _selected, _render = false;
                
                if (typeof _id !== 'undefined') { 
                    
                
                    _selected = env.mainGridActive 
                            ? env.mainChecklist.deselectAll(false, false).select(_id, false, false).getSelected() 
                            : env.searchChecklist.deselectAll(false, false).select(_id, false, false).getSelected();

                } else {

                    _selected = env.mainGridActive 
                            ? env.mainChecklist.getSelected() 
                            : env.searchChecklist.getSelected();
                            
                    _render = true;
                            
                }
                
                _doAPICall({
                    'do': ((_status === true) ? 'feature' : 'unFeature'), 
                    'success': function (data) {


                            // INIT: Task Buttons...
                            (env.mainGridActive ? env.mainChecklist : env.searchChecklist).forEach(function (obj) {
                                    
                                    obj.setAttribute('data-featured', _status ? 0 : 1);

                                    if (_status) { 
                                    obj.classList.add('featured');
                                    //vb.querySelector('.button-label').innerHTML = 'Hide';

                                    } else {
                                    obj.classList.remove('featured');
                                    //vb.querySelector('.button-label').innerHTML = 'Show';
                                    }
                                    

                            }, true);
                            
                            deselectAll(_render, false);
                            
                            
                            
                            
                            

                        }
                });
                
                


                return false; 

        }
        
        
        
        
        
        
        
        
        
    /*
     * 
     * [ FUNCTION ] newCategory 
     * ____________________________________________________________________________
     * 
     * New product tag.
     * 
     */



    // method: [ newCategory ]
    function newCategory () {
        
        var payload = {
                title: 'New Category', 
                description: 'Please choose a store...', 
                entityName: 'store', 
                options: getPickerData('Stores'), 
                buttonLabel: 'Create New Category'
            };
           
        // if there is only one store, skip dialog...
        if (payload.options.length < 2) {
            Category.newCategory(payload.options[0].id);
            return false;
        }

        renderPickerDialog ({
            _do: true, 
            callBack: Category.newCategory,

            payload: payload

        });



        return false;
        
    }
    
        
        
        
        
        
        
        

        


        /*
         * 
         * [ FUNCTION ] addToCategory 
         * ____________________________________________________________________________
         * 
         * Adds or removes a product tag.
         * 
         */


         function addToCategory () {
             
            var payload = {
                title: 'Category', 
                description: 'Choose something', 
                entityName: 'category', 
                options: getPickerData('Categories'), 
                buttonLabel: 'Add to Category'
            };
            
            
            renderPickerDialog ({
                _do: true, 
                callBack: toggleCategory,
                
                payload: payload

            });

             return false;
             
         }
         
         
         
         


        /*
         * 
         * [ FUNCTION ] removeFromCategory 
         * ____________________________________________________________________________
         * 
         * Removes a product tag.
         * 
         */
        
        
         function removeFromCategory () {
             
            var payload = {
                title: 'Category', 
                description: 'Choose something', 
                entityName: 'category', 
                options: getPickerData('Categories'), 
                buttonLabel: 'Remove from Category'
            };
            
            
            
            renderPickerDialog ({
                _do: false, 
                callBack: toggleCategory,
                
                payload: payload

            });
            

             return false;
             
         }
        
        
        
        

        




        /*
         * 
         * [ FUNCTION ] toggleCategory 
         * ____________________________________________________________________________
         * 
         * Adds or removes a product tag.
         * 
         */



        function toggleCategory (tag_id, _do) {


                _doAPICall({
                    'do': ((_do === true) ? 'addToCategory' : 'removeFromCategory'), 
                    'target_id':  tag_id, 
                    'success': function (data) {
                        $('#category-population-'+data.Category.id).html(data.Category.population);
                        $('#store-population-'+data.Category.Store.id).html(data.Category.Store.population);
                        
                    }
                });


                return false;
        }






        
        
        
        


        /*
         * 
         * [ FUNCTION ] addToStore
         * ____________________________________________________________________________
         * 
         * Adds or removes a product tag.
         * 
         */


         function addToStore() {
             
            var payload = {
                title: 'Store', 
                description: 'Choose something', 
                entityName: 'store', 
                options: getPickerData('Stores'), 
                buttonLabel: 'Add to Store'
            };
            
            
            renderPickerDialog ({
                _do: true, 
                callBack: toggleStore,
                
                payload: payload

            });

             return false;
             
         }
         
         
         
         


        /*
         * 
         * [ FUNCTION ] removeFromStore
         * ____________________________________________________________________________
         * 
         * Removes a product tag.
         * 
         */
        
        
         function removeFromStore() {
             
            var payload = {
                title: 'Store', 
                description: 'Choose something', 
                entityName: 'store', 
                options: getPickerData('Stores'), 
                buttonLabel: 'Remove from Store'
            };
            
            
            
            renderPickerDialog ({
                _do: false, 
                callBack: toggleStore,
                
                payload: payload

            });
            

             return false;
             
         }
        
        
        
       




        /*
         * 
         * [ FUNCTION ] toggleStore
         * ____________________________________________________________________________
         * 
         * Adds or removes a product tag.
         * 
         */



        function toggleStore (_id, _do) {


                _doAPICall({
                    'do': ((_do === true) ? 'addToStore' : 'removeFromStore'), 
                    'target_id':  _id, 
                    'success': function (data) {
                        $('#store-population-'+data.Store.id).html(data.Store.population);
                    }
                });


                return false;
        }







        
        
 


        /*
         * 
         * [ FUNCTION ] addToSale
         * ____________________________________________________________________________
         * 
         * 
         * 
         */


         function addToSale() {
             
            renderPickerDialog ({
                _do: true, 
                callBack: toggleSale,
                
                payload: {
                    title: 'Sale', 
                    description: 'Choose something', 
                    entityName: 'sale', 
                    options: getPickerData('Sales'), 
                    buttonLabel: 'Add to Sale'
                }

            });

             return false;
             
         }
         
         
         
         


        /*
         * 
         * [ FUNCTION ] removeFromSale
         * ____________________________________________________________________________
         * 
         * Removes a product tag.
         * 
         */
        
        
         function removeFromSale() {
             
            renderPickerDialog ({
                _do: false, 
                callBack: toggleSale,
                
                payload: {
                    title: 'Sale', 
                    description: 'Choose something', 
                    entityName: 'sale', 
                    options: getPickerData('Sales'), 
                    buttonLabel: 'Remove from Sale'
                }

            });

            return false;
             
         }
        
        




        /*
         * 
         * [ FUNCTION ] toggleSale
         * ____________________________________________________________________________
         * 
         * Adds or removes a product tag.
         * 
         */



        function toggleSale(sale_id, _do) {

                _doAPICall({
                    'do': ((_do === true) ? 'addToSale' : 'removeFromSale'), 
                    'target_id':  sale_id, 
                    'success': function (data) {
                        $('#sale-population-'+data.Sale.id).html(data.Sale.population);
                    }
                });
                          


                return false;
        }
        
        
 


        /*
         * 
         * [ FUNCTION ] applyCoupon
         * ____________________________________________________________________________
         * 
         * 
         * 
         */


         function applyCoupon() {
             
            renderPickerDialog ({
                _do: true, 
                callBack: toggleCoupon,
                
                payload: {
                    title: 'Apply Coupon Code', 
                    description: 'Choose something', 
                    entityName: 'coupon code', 
                    options: getPickerData('Coupons'), 
                    buttonLabel: 'Appy Coupon Coupon'
                }

            });

             return false;
             
         }
         
         
         
         


        /*
         * 
         * [ FUNCTION ] removeCoupon
         * ____________________________________________________________________________
         * 
         * Removes a product tag.
         * 
         */
        
        
         function removeCoupon() {
             
            renderPickerDialog ({
                _do: false, 
                callBack: toggleCoupon,
                
                payload: {
                    title: 'Remove Coupon Code', 
                    description: 'Choose something', 
                    entityName: 'coupon code', 
                    options: getPickerData('Coupons'), 
                    buttonLabel: 'Remove Coupon Code'
                }

            });

            return false;
             
         }
        
        




        /*
         * 
         * [ FUNCTION ] toggleCoupon
         * ____________________________________________________________________________
         * 
         * Adds or removes a product tag.
         * 
         */



        function toggleCoupon(_id, _do) {

                _doAPICall({
                    'do': ((_do === true) ? 'applyCoupon' : 'removeCoupon'), 
                    'target_id':  _id, 
                    'success': function (data) {
                        $('#coupon-population-'+data.Coupon.id).html(data.Coupon.population);
                    }
                });
                          


                return false;
        }
        
     
      
      
      
      
      
      
        
        


        /*
         * 
         * [ FUNCTION ] setTaxClass 
         * ____________________________________________________________________________
         * 
         * 
         * 
         */
        
        
         function setTaxClass () {
             
            var payload = {
                title: 'Apply Tax Class', 
                description: 'Choose something', 
                entityName: 'tax class', 
                options: getPickerData('TaxClasses'), 
                buttonLabel: 'apply tax class'
            };
            
            
            
            renderPickerDialog ({
                _do: 'setTaxClass', 
                callBack: function (target_id, _do) {
                    _doAPICall({
                        'do': _do, 
                        'target_id':  target_id
                    });
                    
                    return false;
                },
                
                payload: payload

            });
            

             return false;
             
         }
         
         
         
         

        /*
         * 
         * [ FUNCTION ] setTaxClass 
         * ____________________________________________________________________________
         * 
         * 
         * 
         */
        
        
         function newImport () {
             
            var payload = {
                meta: {
                title: 'Import Products', 
                description: 'Choose something',  
                buttonLabel: 'Start Import'
                }, 
                fields: [
                    {
                    entityName: 'Store', 
                    options: getPickerData('Stores')
                    }, 
                    {
                    entityName: 'File Type', 
                    options: getPickerData('ImporterFileTypes')
                    }
                
                ]
            };
            
            
            
            renderPickerDialog ({
                _do: 'setTaxClass', 
                callBack: function (target_id, _do) {
                    _doAPICall({
                        'do': _do, 
                        'target_id':  target_id
                    });
                    
                    return false;
                },
                
                template: 'import-dialog', 
                payload: payload

            });
            

             return false;
             
         }
        
         
        
        




  
        
        
        
        
        
        

        /*
         * 
         * [ FUNCTION ] renderPickerDialog 
         * ____________________________________________________________________________
         * 
         * 
         * 
         */


         function renderPickerDialog(_options) {
             
            if (typeof _options.template === 'undefined') _options.template = 'generic-picker';
            
            
            Catalog.toggleContextMenu(false); 
            var dialogBox = $(Mustache.render(ajax.getTemplate(_options.template), _options.payload));  

            // Show dialog box...
            $('body').append(dialogBox);
            dialogBox.show();
            
            dialogBox.find('#select-target-id').unbind('focus').focus (function () {hideTip(true); });
             

             $('#fullscreen-bg2, .picker-dialog-box > .cancel-button').unbind('click').click(function () {
                
                hideTip(true);

                dialogBox.fadeOut(200, function () {
                    $('.picker-dialog-box').remove();
                });
                $('#fullscreen-bg2').unbind('click').hide();
                $(document).unbind('scroll'); 
 		$('body').css({'overflow':'visible'});	

             })
             .show();
             
            $('#add-to-button').click(function () {
                
                    var _target_id = $('#select-target-id').val();
                    if (_target_id == -1) { 
                        showTip ($('#select-target-id'), 'Please choose a ' + _options.payload.entityName + '.');
                        return false;
                    }
                    
                    hideTip(true);
                    
                    dialogBox.fadeOut(200, function () {
                        $('.picker-dialog-box').remove();
                    });
                    $('#fullscreen-bg2').unbind('click').hide();
                    $(document).unbind('scroll'); 
                    $('body').css({'overflow':'visible'});
                    

                    _options.callBack (_target_id, _options._do);
                    return false;
            });

             return false;
             
         }
         
  






     function renderMultiPickerDialog(_options) {
         
        if (typeof _options.template === 'undefined') _options.template = 'generic-picker-multi';
        
        
        var dialogBox = $(Mustache.render(ajax.getTemplate(_options.template), _options.payload));  

        // Show dialog box...
        $('body').append(dialogBox);
        dialogBox.show();
        
        dialogBox.find('#select-target-id').unbind('focus').focus (function () {hideTip(true); });
         

         $('#fullscreen-bg2, .picker-dialog-box > .cancel-button').unbind('click').click(function () {
            
            hideTip(true);

            dialogBox.fadeOut(200, function () {
                $('.picker-dialog-box').remove();
            });
            $('#fullscreen-bg2').unbind('click').hide();
            $(document).unbind('scroll'); 
    $('body').css({'overflow':'visible'});	

         })
         .show();
         
        var data = {};

        $('#add-to-button').click(function () {
            
            data = {};

            for (var i=0,l=_options.payload.entities.length; i<l; i++) {
                var opt = _options.payload.entities[i];

                var id = '#select-'+opt.input_name;
                data[opt.input_name] = $(id).val();
                if (data[opt.input_name] == -1) { 
                    showTip ($(id), 'Please choose a ' + opt.entity_name + '.');
                    return false;
                }
            }


            hideTip(true);
            
            dialogBox.fadeOut(200, function () {
                $('.picker-dialog-box').remove();
            });
            $('#fullscreen-bg2').unbind('click').hide();
            $(document).unbind('scroll'); 
            $('body').css({'overflow':'visible'});
            

            _options.callBack (data, _options._do);
            return false;
        });

         return false;
         
     }
     










        /*
         * 
         *  Private: [ FUNCTION ] _doAPICall
         * ____________________________________________________________________________
         * 
         * 
         * 
         */



        function _doAPICall(_options) {
                
                Catalog.toggleContextMenu(false); 
                hold ('Please wait...');
                
                var _ids = env.mainGridActive ? env.mainChecklist.getSelected() : env.searchChecklist.getSelected();
                
                // if no ids
                if (_ids.length == 0) { alert('No selections made.'); return false; }

                var query = _query_temp;
                
                //query['product_id'] =  _ids;
                if (typeof _options['do'] !== 'undefined') query['do'] = _options['do'];
                if (typeof _options['target_id'] !== 'undefined') query['target_id'] = _options['target_id'];


                ajax.doAPICall ({
                api: 'catalog', 
                'do': query['do'], 
                target_id: query['target_id'], 
                data: { product_id: _ids  }, 
                success: function(data) {
                            
                            //var data = response.json();
                            
                            if (typeof _options['success'] === 'function') _options['success'](data);
                            
                            deselectAll();
                            
                            return false;

                        }, 
                error: function (xhr) {
                        if (typeof _options['error'] === 'function') _options['error'](xhr);
                        return false;
                }

                });

                return false;
        }
        
        
        
        
        
        

        quickSearchCallBack = function (response) 
        {

                var data = response.json();

                var _grid = document.querySelector('#quick-search-tile-grid > .tiles-wrapper');

                

                __el('#main-tile-grid').hide();	
                __el('#quick-search-tile-grid').show();

                if (data.count > 0) {

                    _grid.innerHTML = '';
                    for (var i in data.Products) {
                            __el(_grid).appendChild(Mustache.render(ajax.getTemplate('product-tile'), data.Products[i]));
                    }
                    
                    
                    initProductGrid(GRID_TYPE_SEARCH);

    //                $('#main-tile-grid').hide();
    //                $('#quick-search-tile-grid').show();



                } else {

                    _grid.innerHTML = '';
                    
                    var _exit_button = __el(_grid).appendChild('<span class="no-results">No Results (click here to exit search)</span>', true);
                    
                    _exit_button.addEventListener('click', (function hideSearchResultsHandler () {
                        this.remove();
                        __el('#quick-search-tile-grid').hide();
                        __el('#main-tile-grid').show();
                        
                        env.mainGridActive  = true;
                    }).bind(_exit_button));

                    


                }
        }





        noResult = function (data) 
        {
                    var _grid = $('#quick-search-tile-grid > .tiles-wrapper');
                    _grid.html('');
                    var _exit_button = $('<span class="no-results">No Results (click here to exit search)</span>');
                    _exit_button.click(function () {
                        $(this).detach();
                        $('#quick-search-tile-grid').hide();
                        $('#main-tile-grid').show();
                        
                        env.mainGridActive  = true;
                    });

                    _grid.append(_exit_button);
        }



    
    
    

    
    
    
    
    //Return public methods
    var self = {
        toggleContextMenu: toggleContextMenu, 
        toggleSideBarSection: toggleSideBarSection, 
        toggleSelection: toggleSelection, 
        toggleAllSelections: toggleAllSelections, 
        deselectAll: deselectAll, 
        selectAll: selectAll,
        isSelected: isSelected, 
        newProduct: newProduct, 
        deleteProduct: deleteProducts, 
        deleteProducts: deleteProducts, 
        
        toggleProductDisplay: toggleProductDisplay, 
        toggleFeaturedStatus: toggleFeaturedStatus, 
        
        newCategory: newCategory, 
        addToCategory: addToCategory, 
        removeFromCategory: removeFromCategory, 
        addToStore: addToStore, 
        removeFromStore: removeFromStore, 
        addToSale: addToSale, 
        removeFromSale: removeFromSale, 
        applyCoupon: applyCoupon, 
        removeCoupon: removeCoupon, 
        setTaxClass: setTaxClass, 
        getPickerData: getPickerData, 
        newImport: newImport, 
        
        quickSearchCallBack: quickSearchCallBack, 
        noResult: noResult
        
    };
    
    
    return self;
    
    
    
    
 };
 
 
 
