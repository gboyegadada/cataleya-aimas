
Cataleya.makeCheckList = function (c) {
    return new Cataleya.CheckList(c);
}


Cataleya.CheckList = function CheckList (_container) {
    
    var  
    
    
    self = this,
    _list_id = Math.uuid(5),
    _tiles = _container.querySelectorAll('.checklist-tile'), 
    _list_length = _tiles.length,
    _selected = [], 
    _last_selected, 
    _is_a_virgin = true, 
    _all_selected = false, 
    _shift_key_down = false, 
    _set_label = (_container.querySelector('.select-button .button-label') !== null), 
    _a = Cataleya.arrayFuncs(), 
    _eventsRegister = {
        'onChange': [], 
        'onCheck': [], 
        'onFirstCheck': [], 
        'onLastUnchecked': [], 
        'onAllChecked': [], 
        'onUncheck': [], 
        'onAllUnchecked': []
    }, 
    
    onChangeEvent = new CustomEvent('onChange' + _list_id, { 
                                            detail: { listID: _list_id, selected: _selected, lastSelected: _last_selected },
                                            bubbles: true,
                                            cancelable: false 
                                        }),
                                        
            
    onCheckEvent = new CustomEvent('onCheck' + _list_id, { 
                                            detail: { listID: _list_id, selected: _selected, lastSelected: _last_selected },
                                            bubbles: true,
                                            cancelable: false 
                                        }),
                                        
    onFirstCheckEvent = new CustomEvent('onFirstCheck' + _list_id, { 
                                            detail: { list_id: _list_id, selected: _selected },
                                            bubbles: true,
                                            cancelable: false 
                                        }),
                                        
    onLastUncheckedEvent = new CustomEvent('onLastUnchecked' + _list_id, { 
                                            detail: { list_id: _list_id, selected: _selected },
                                            bubbles: true,
                                            cancelable: false 
                                        }),
                                        
    onAllCheckedEvent = new CustomEvent('onAllChecked' + _list_id, { 
                                            detail: { list_id: _list_id, selected: _selected },
                                            bubbles: true,
                                            cancelable: false 
                                        }),
                                        
    onUncheckEvent = new CustomEvent('onUncheck' + _list_id, { 
                                            detail: { list_id: _list_id, selected: _selected },
                                            bubbles: true,
                                            cancelable: false 
                                        }),
                                        
    onAllUncheckedEvent = new CustomEvent('onAllUnchecked' + _list_id, { 
                                            detail: { list_id: _list_id, selected: _selected },
                                            bubbles: true,
                                            cancelable: false 
                                        });
                                        
                                        
    document.addEventListener('keydown', _shift_key_handler, false);
    document.addEventListener('keyup', _shift_key_handler, false);
    
    
    function _shift_key_handler (e) {
    
            _shift_key_down = e.shiftKey;
            
    }
                                        
             
    
    self.getListID = function () {

        return _list_id;
        
    };       
    
    
    self.forEach = function(cb, filter) {
        if (typeof cb !== 'function') throw new TypeError ('Argument must be a function.');
        filter  = (typeof filter === 'boolean') ? filter : false;
        
        for (var _i=0, _l=_tiles.length; _i < _l; _i++) { 
            if (filter && !this.isSelected(parseInt(_tiles[_i].getAttribute('data-tile-id')))) continue;
            
            cb.apply(_tiles[_i], [_tiles[_i], _i]); 
        }
        

    },

    self.item = function (i) {
        return _tiles[i];
    }, 
    
    
    
    
    
    
    /**
     * 
     * @param {string} event
     * @param {function} func
     * @returns {CheckList}
     * 
     */
    self.on = function (event, func) {

        if (typeof func !== 'function') throw new TypeError ('Argument 2 must be a function.');
        
        if (typeof _eventsRegister[event] !== 'undefined') {
            document.addEventListener(event+_list_id, func);
            _eventsRegister[event].push(func);
        }
        
        return this;
    };    
    
    
    
    
    
    /**
     * 
     * @param {string} event
     * @param {function} func
     * @returns {CheckList}
     * 
     */
    self.off = function (event, func) {

        if (typeof func !== 'function') throw new TypeError ('Argument 2 must be a function.');
        
        if (typeof _eventsRegister[event] !== 'undefined') {
            document.removeEventListener(event+_list_id, func);

            _eventsRegister[event].forEach(function (f, i) {
                if(f.name === func.name) _eventsRegister[event].slice(i, 1);
            });
        }
        
        return this;
    }; 
    
    
    
    
    /**
     * 
     * @param {int} _id
     * @param {boolean} _render
     * @param {boolean} triggerEvent
     * @returns {CheckList}
     * 
     */
    self.toggleSelection = function (_id, _render, triggerEvent) {
        _id = parseInt(_id);
        
        return (!this.isSelected(_id)) 
                ? this.select(_id, _render, triggerEvent) 
                : this.deselect(_id, _render, triggerEvent);
        
        
    };
    
    
    
    /**
     * 
     * @param {int} _id
     * @param {boolean} _render
     * @param {boolean} triggerEvent
     * @returns {CheckList}
     * 
     */
    self.select = function (_id, _render, triggerEvent) {

        _render = (typeof _render === 'boolean' && _render === false) ? false : true;
        triggerEvent  = (typeof triggerEvent === 'boolean') ? triggerEvent : true;
        
        _id = parseInt(_id);
        
        if (!this.isSelected(_id)) {
        

            var _tile = _container.querySelector('.checklist-tile[data-tile-id="'+_id+'"]');
            _last_selected = _id;

            if (_render) {
                __el(_tile).addClass('selected');
                if (_set_label) _tile.querySelector('.select-button .button-label').innerHTML = 'Remove';
            }

            _is_a_virgin = (_selected.length === 0) ? true : false;
            _selected.push(_id);

            if (triggerEvent) {
                if (_is_a_virgin) document.dispatchEvent(onFirstCheckEvent);
                document.dispatchEvent(onCheckEvent);
                document.dispatchEvent(onChangeEvent);
            }

            if (_selected.length === _list_length) {
                _all_selected = true;
                if (triggerEvent) document.dispatchEvent(onAllCheckedEvent);
            }
            
        }
        
        return this;
    };
    
    
    
    
    
    
    /**
     * 
     * @param {int} _id
     * @param {boolean} _render
     * @param {boolean} triggerEvent
     * @returns {CheckList}
     * 
     */
    self.deselect = function (_id, _render, triggerEvent) {

        _render = (typeof _render === 'boolean' && _render === false) ? false : true;
        triggerEvent  = (typeof triggerEvent === 'boolean') ? triggerEvent : true;
        _id = parseInt(_id);
        

        if (this.isSelected(_id)) {
        
            var _tile = _container.querySelector('.checklist-tile[data-tile-id="'+_id+'"]');
            _last_selected = _id;


            if (_render) { 
                __el(_tile).removeClass('selected');
                if (_set_label) _tile.querySelector('.select-button .button-label').innerHTML = 'Select';
            }
            _selected = _a.remove(_id, _selected);

            if (triggerEvent) {
                document.dispatchEvent(onUncheckEvent);
                document.dispatchEvent(onChangeEvent);
            }
        
            if (_selected.length === 0 && triggerEvent) document.dispatchEvent(onAllUncheckedEvent);
            
            if (_selected.length < _list_length) {
                if (_all_selected && triggerEvent) document.dispatchEvent(onLastUncheckedEvent); 
                _all_selected = false;
            }
        
        }
        
        return this;
        
    };
    
    
    // isSelect
    self.isSelected = function (_id) {
        
        return (_selected.indexOf(parseInt(_id)) > -1);
        
    };
    
    

    // allSelect
    self.allSelected = function () {

        return _all_selected;
        
    };
    
    
    
    /**
     * 
     * @param {Boolean} _render
     * @param {Boolean} triggerEvent
     * @returns {CheckList}
     * 
     */
    self.deselectAll = function (_render, triggerEvent) {
        
        if (_selected.length === 0) return this;
        
       var _all_were_selected = _all_selected;
       _all_selected = false;
       _is_a_virgin = true;
       _selected = [];
       
       _render = (typeof _render === 'boolean' && _render === false) ? false : true;
        
       
            
        // Trigger onAllUncheckedEvent... 
        if (typeof triggerEvent !== 'boolean' || triggerEvent ) { 
            
            document.dispatchEvent(onAllUncheckedEvent); 
            document.dispatchEvent(onChangeEvent);
            
 
           if (_all_were_selected) document.dispatchEvent(onLastUncheckedEvent);
       
        }
             


        if (_render) {
            
            __el(_container.querySelectorAll('.checklist-tile.selected'))
            .removeClass('selected')
            .forEach(function (obj) {

                if (_set_label) obj.querySelector('.select-button .button-label').innerHTML = 'Select';

              });
        }
          
          
            
          return this;
        
    };
    
    
    
    // selectAll
    self.selectAll = function (_render) {

        _render = (typeof _render === 'boolean' && _render === false) ? false : true;
        
        var _change = (_selected.length < _list_length);
        
        this.deselectAll(false, false);
        
        _selected = [];
       
       
       if (_render) {
           
            __el(_tiles)
                .addClass('selected')
                .forEach(function (obj) {

                if (_set_label) obj.querySelector('.select-button .button-label').innerHTML = 'Remove';
                _selected.push(parseInt(obj.getAttribute('data-tile-id')));

                });
        } else {
            
            this.forEach(function (obj) {

                _selected.push(parseInt(obj.getAttribute('data-tile-id')));

              });
              
        }
       
       if (_selected.length > 0) {
            _all_selected = true;
    
            // Trigger onFirstCheckEvent... 
            if (_is_a_virgin) document.dispatchEvent(onFirstCheckEvent);
            
            // Trigger onAllCheckedEvent...
            document.dispatchEvent(onAllCheckedEvent);
            if (_change) document.dispatchEvent(onChangeEvent);
            
       } 
       
        else {
           
           //if (_all_selected) document.dispatchEvent(onLastUncheckedEvent); 
           _all_selected = false;
       }
       
       
       
       return this;
        
    };
    
    
    
    
    // toggleAllSelections
    self.toggleAllSelections = function () {
        
        if (_shift_key_down) {
        // reverse...
            
        self.deselectAll();
        
            
        }
        
        else {
        // normal...
            
        if (!_all_selected) self.selectAll();
        else self.deselectAll();
        
        }
        
        return this;
    };
    
    
    
    
    
    
    self.getSelected = function  () {
        return _selected;
    };
    
    
    self.getLastSelected = function  () {
        return _last_selected;
    };
    
    self.count = function  () {
        return _selected.length;
    };
    
    self.destroy = function  () {
        
        this.deselectAll();
        
        for (var event in _eventsRegister) {
            

            _eventsRegister[event].forEach(function (f) {
                
                document.removeEventListener(event+_list_id, f);
                
            });
            
        }
        
        document.removeEventListener('keydown', _shift_key_handler, false);
        document.removeEventListener('keyup', _shift_key_handler, false);
        
        _eventsRegister = null;
    };
    
    
    
    
    
    
    return this;
    
    
    
};


