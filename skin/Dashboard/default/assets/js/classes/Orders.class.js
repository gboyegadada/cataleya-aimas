
 
 
// ----------------------------------------------------------------------------------------- //


// Class: [ loadOrders ]
Cataleya.loadOrders = function () {
    
    // Private properties
    
    var 
    
    _auth_token = $('#meta_token').val(), 
    _active_filter = $('#active-filter').val(), 
    _active_store = parseInt($('#active-store').val()), 
    _selected = [], 
    _all_selected = false, 
    _a = Cataleya.arrayFuncs(), 
    _utility = Cataleya.getUtility(), 
    _menu_status = false,
    
    _picker_data = [], 
    
    _query_temp = {
        auth_token: _auth_token,
        order_id: [], 
        data: [], 
        'do': 'void', 
        target_id: 0
    };
    
    

    
    // Load moustache templates
    ajax.loadTemplates(['order-tile', 'generic-picker']);
    
    loadData();
    
    
    function loadData (callBack) {
        
    



    }
    
    
    
    // [ getPickerData ]
    
    function getPickerData (_id) {
        return _utility.getPickerData(_id);
    }





    // Context Menu
    function toggleContextMenu (showOrHide) {
        var _menu = $('#catalog-context-menu');
        
        if (typeof showOrHide !== 'boolean') showOrHide = !_menu_status;
        
        if (showOrHide === true && showOrHide !== _menu_status) { 

             $('#fullscreen-bg2').unbind('click').click(function () {
                toggleContextMenu(false);
             })
             .show();
             
            _menu.fadeIn(200); _menu_status = true; 
            
        
        }
        else if (showOrHide === false && showOrHide !== _menu_status) { 
            _menu.fadeOut(200); _menu_status = false;
            
            $('#fullscreen-bg2').unbind('click').hide();
            $(document).unbind('scroll'); 
            $('body').css({'overflow':'visible'});	
        
        }
        
    }
    
    
    
    function toggleSideBarSection(_header) {
        _header = $(_header);
        
        var status = ($.inArray(_header.next('li').css('display'), ['inline', 'block']) > -1);
            
        $('li.sb-section-header').parent('a').next('li').slideUp(200);
        
        $('li.sb-section-header > span')
        .removeClass('icon-minus-3')
        .removeClass('icon-plus-3')
        .addClass('icon-plus-3');
        


        var _symbol = _header.find('.sb-section-header > span');
        
        if (status) {
            _header.next('li').slideUp(200);
            _symbol.removeClass('icon-minus-3').addClass('icon-plus-3');
        }
        else {
            _header.next('li').slideDown(200);
            _symbol.removeClass('icon-plus-3').addClass('icon-minus-3');
        }
    }



    
    
    // Select
    function toggleSelection(_id) {
        
        _all_selected = false;
        var _tile = $('.tiles-wrapper').find('#order-tile-'+_id);
        
        if (_a.has(_id, _selected)) {
            $(_tile).removeClass('selected')
            .find('#order-tile-select-button .button-label').html('Select');
            //.end().find('#order-tile-select-button .icon-cancel-2').removeClass('icon-cancel-2').addClass('icon-checkmark');
            _selected = _a.remove(_id, _selected);
        } else {
            $(_tile).addClass('selected')
            .find('#order-tile-select-button .button-label').html('Remove');
            //.end().find('#order-tile-select-button .icon-checkmark').removeClass('icon-checkmark').addClass('icon-cancel-2');
            _selected.push(_id);
        }
        
        if (_selected.length > 0 && !$('li#task-bar').is(':visible')) { 
            $('li.list-table-header').hide(); 
            $('li#task-bar').slideDown(200); //fadeIn(200);
            
            $('#order-status-control-x option[value=x]').attr('selected', true);

            $('li#task-bar > a#global-select-button')
            .find('.button-label').html('Select All')
            .end()
            .find('span.icon-checkmark').removeClass('active');
        }
        else if (_selected.length < 1 && $('li#task-bar').is(':visible')) { 
            $('li#task-bar').slideUp(200, function () {
               $('li.list-table-header').slideDown(200); 
            });
            
        }
        
        
    }
    
    
    // isSelect
    function isSelected(_id) {

        return _a.has($(_tile).attr('data_order_id'), _selected);
        
    }
    
    
    
    // toggleAllSelections
    function toggleAllSelections() {
        
        if (!_all_selected) selectAll();
        else deselectAll();
    }
    
    
    
    // deselectAll
    function deselectAll(hideTaskBar) {
       _all_selected = false;
       _selected = [];
       
       
        if (!$('li#task-bar').is(":visible")) return false;

        $('div.order-tile').each(function (i, obj) {
            
            var _tile = $(this);
            
            if (_tile.hasClass('selected')) {
                
                _tile.removeClass('selected')
                .find('.order-tile-dashboard-wrapper')
                .css({bottom: '-50px'})
                .find('#order-tile-select-button .button-label').html('Select');
            
            }
            


          });
          
          if (typeof hideTaskBar === 'boolean' && hideTaskBar === false) return false;

          $('li#task-bar > a#global-select-button')
          .find('.button-label').html('Select All')
          .end()
          .find('span.icon-checkmark').removeClass('active');
          
          $('li#task-bar').slideUp(200, function () {
               $('li.list-table-header').slideDown(200); 
            });

            
          return false;
        
    }
    
    
    
    // deselectAll
    function selectAll(_main_tiles) {

        if (typeof _main_tiles === 'undefined' || typeof _main_tiles !== 'boolean') _main_tiles = true;
        deselectAll(false);
        
        _selected = [];
       
       
       if (_main_tiles) {
            
            $('#main-tile-grid > .tiles-wrapper > div.order-tile').each(function (i, obj) {

                $(this).addClass('selected')
                .find('.order-tile-dashboard-wrapper')
                .animate({bottom: '+=50'}, 200)
                .find('#order-tile-select-button .button-label').html('Remove')
                .end();
                _selected.push($(this).attr('data_order_id'));

              });
              
              
       } else {
           
            $('#quick-search-tile-grid > .tiles-wrapper > div.order-tile').each(function (i, obj) {

                $(this).addClass('selected')
                .find('#order-tile-select-button .button-label').html('Remove')
                .end();
                _selected.push($(this).attr('data_order_id'));

              });
              
       }
       
       
       if (_selected.length > 0) {
            _all_selected = true;
            $('li#task-bar > a#global-select-button')
            .find('.button-label').html('Deselect All')
            .end()
            .find('span.icon-checkmark').addClass('active');
       }
        
    }
    
    
    
    


    /*
     * 
     * [ FUNCTION ] deleteOrders 
     * ____________________________________________________________________________
     * 
     * Self explanatory.
     * 
     */



    function deleteOrders (_id) {

        if (typeof _id !== 'undefined' && !_a.has(_id, _selected)) { deselectAll(); _selected.push(_id); }

        _doAPICall({
            'do': 'deleteOrder', 
            'data': { active_store: _active_store }, 
            'success': function (data) {
                
                $('#all-orders-count').html(data.Population.all); 
                $('#pending-orders-count').html(data.Population.pending); 
                $('#shipped-orders-count').html(data.Population.shipped); 
                $('#delivered-orders-count').html(data.Population.delivered); 
                $('#cancelled-orders-count').html(data.Population.cancelled);
                
                for (var i in _selected) {

                        var _id = _selected[i];

                        // Main Tile Wrapper

                        $('#main-tile-grid > .tiles-wrapper').find('#order-tile-'+_id).hide(200, function () { 
                            $(this).remove();
                        });


                        // Quick Search Tile Wrapper
                        $('#quick-search-tile-grid > .tiles-wrapper').find('#order-tile-'+_id).hide(200, function () { 
                            $(this).remove();
                        });

                 }
                 
                 deselectAll();
                     
                     
                setTimeout (function () {
                    $('#main-tile-grid > .tiles-wrapper > div.order-tile').each(function (i, obj) {
                          $(this).attr('item_number', _item_num++);
                      });

                    $('#quick-search-tile-grid > .tiles-wrapper > div.order-tile').each(function (i, obj) {
                          $(this).attr('item_number', _item_num2++);
                      });
                }, 205);


                }
         });

            return false; 

    }









    /*
     * 
     * [ FUNCTION ] setOrderStatus 
     * ____________________________________________________________________________
     * 
     * Self explanatory.
     * 
     */



    function setOrderStatus (field, _id) {

        field = $(field);
        if (field.val() == 'x') return false;
        
        if (typeof _id !== 'undefined' && !_a.has(_id, _selected)) { deselectAll(); _selected.push(_id); }

        _doAPICall({
            'do': 'setOrderStatus', 
            'data': { order_status: field.val(), active_store: _active_store }, 
            'success': function (data) {

                 
                $('#pending-orders-count').html(data.Population.pending); 
                $('#shipped-orders-count').html(data.Population.shipped); 
                $('#delivered-orders-count').html(data.Population.delivered); 
                $('#cancelled-orders-count').html(data.Population.cancelled);
                
                
                // Hide if status filter is ON e.g. we're presently viewing 'PENDING' orders and this Order is 'SHIPPED'.
                if (_active_filter != data.OrderStatus && _active_filter != 'all' && !$('.order-expanded-view-box').is(':visible')) {
                    
                    
                    for (var i in _selected) {

                        var _id = _selected[i];
                        
                        // Main Tile Wrapper

                        $('#main-tile-grid > .tiles-wrapper').find('#order-tile-'+_id).hide(200, function () { 
                            $(this).remove();
                        });


                        // Quick Search Tile Wrapper
                        $('#quick-search-tile-grid > .tiles-wrapper').find('#order-tile-'+_id).hide(200, function () { 
                            $(this).remove();
                        });


                     }

                    
                } else {
                    
                    for (var i in _selected) {

                            var _id = _selected[i];

                            // Main Tile Wrapper

                            $('#main-tile-grid > .tiles-wrapper, #quick-search-tile-grid > .tiles-wrapper')
                            .find('#order-tile-'+_id)
                            .removeClass('pending').removeClass('shipped').removeClass('delivered').removeClass('cancelled')
                            .addClass(data.OrderStatus)
                            .find('.order-status-indicator')
                            .removeClass('pending').removeClass('shipped').removeClass('delivered').removeClass('cancelled')
                            .addClass(data.OrderStatus);

                            $('.order-expanded-view-box').find('.order-status-indicator')
                            .removeClass('pending').removeClass('shipped').removeClass('delivered').removeClass('cancelled')
                            .addClass(data.OrderStatus);

                            $('#order-status-control-'+_id+' option[value='+data.OrderStatus+']').attr('selected', true);


                     }

                }
                 
                 deselectAll();
                     

                }
         });

            return false; 

    }






        
        
        
        
        

        /*
         * 
         * [ FUNCTION ] renderPickerDialog 
         * ____________________________________________________________________________
         * 
         * 
         * 
         */


         function renderPickerDialog(_options) {
             
            Orders.toggleContextMenu(false); 
            var dialogBox = $(Mustache.render(ajax.getTemplate('generic-picker'), _options.payload));  

            // Show dialog box...
            $('body').append(dialogBox);
            dialogBox.show();
            
            dialogBox.find('#select-target-id').unbind('focus').focus (function () {hideTip(true); });
             

             $('#fullscreen-bg2, .picker-dialog-box > .cancel-button').unbind('click').click(function () {
                
                hideTip(true);

                dialogBox.fadeOut(200, function () {
                    $('.picker-dialog-box').remove();
                });
                $('#fullscreen-bg2').unbind('click').hide();
                $(document).unbind('scroll'); 
 		$('body').css({'overflow':'visible'});	

             })
             .show();
             
            $('#add-to-button').click(function () {
                
                    var _target_id = $('#select-target-id').val();
                    if (_target_id == -1) { 
                        showTip ($('#select-target-id'), 'Please choose a ' + _options.payload.entityName + '.');
                        return false;
                    }
                    
                    hideTip(true);
                    
                    dialogBox.fadeOut(200, function () {
                        $('.picker-dialog-box').remove();
                    });
                    $('#fullscreen-bg2').unbind('click').hide();
                    $(document).unbind('scroll'); 
                    $('body').css({'overflow':'visible'});
                    

                    _options.callBack (_target_id, _options._do);
                    return false;
            });

             return false;
             
         }
         
  





        /*
         * 
         *  Private: [ FUNCTION ] _doAPICall
         * ____________________________________________________________________________
         * 
         * 
         * 
         */



        function _doAPICall(_options) {
                
                Orders.toggleContextMenu(false); 
                hold ('Please wait...');
                
                var _ids = _selected;

                // if no ids
                if (_ids.length == 0) { alert('No selections made.'); return false; }

                var query = _query_temp;
                
                query['order_id'] =  _ids;
                if (typeof _options['do'] !== 'undefined') query['do'] = _options['do'];
                if (typeof _options['target_id'] !== 'undefined') query['target_id'] = _options['target_id'];
                if (typeof _options['data'] !== 'undefined') query['data'] = _options['data'];

                

                ajax.jsonRequest ({
                method: 'POST', 
                url: 'orders.api.ajax.php', 
                data: query, 
                dataType: 'json', 
                success: function(response) {
                                
                            var data = response.json();
                            
                            if (typeof _options['success'] == 'function') _options['success'](data);
                            
                            deselectAll();
                            
                            return false;

                        }, 
                error: function (xhr) {
                        if (typeof _options['error'] == 'function') _options['error'](xhr);
                        return false;
                }

                });

                return false;
        }
        
        
   
    
    
    

    
    
    
    
    //Return public methods
    return {
        toggleContextMenu: toggleContextMenu, 
        toggleSideBarSection: toggleSideBarSection, 
        toggleSelection: toggleSelection, 
        toggleAllSelections: toggleAllSelections, 
        deselectAll: deselectAll, 
        selectAll: selectAll,
        isSelected: isSelected, 
        setOrderStatus: setOrderStatus, 
        deleteOrder: deleteOrders, 
        deleteOrders: deleteOrders, 
        getPickerData: getPickerData
        
    };
    
    
    
 }
 
 
 
