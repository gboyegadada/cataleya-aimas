
 
 
// ----------------------------------------------------------------------------------------- //

var MyAccount;

$(document).ready(function () {
    
    if ($('#meta_token').length > 0) MyAccount = Cataleya.loadMyAccount();
 
});

 
// Class: [ loadMyAccount ]
Cataleya.loadMyAccount = function (callBack) {
    
    // Private properties
    
    var 
    
    _dialogBox, 
    _auth_token = $('#meta_token').val(), 
    Validator = Cataleya.newValidator(), 
    _a = Cataleya.arrayFuncs(), 
    Utility = Cataleya.getUtility(), 
    
    _query_temp = {
        auth_token: _auth_token,
        'do': 'void', 
        data: []

    };
    

    
    
    // Load moustache templates
    ajax.loadTemplates(['my-account-dialog']);




    /*
     * 
     *  Private: [ FUNCTION ] _doAPICall
     * ____________________________________________________________________________
     * 
     * 
     * 
     */



    function _doAPICall(_options) {

            hold ('Please wait...');

            var query = _query_temp;

            if (typeof _options['do'] !== 'undefined') query['do'] = _options['do'];
            if (typeof _options['data'] !== 'undefined') query['data'] = _options['data'];



            ajax.jsonRequest ({
            method: 'POST', 
            url: 'my-account.api.ajax.php', 
            data: query, 
            dataType: 'json', 
            success: function(response) {

                        var data = response.json();
                        
                        if (typeof _options['success'] == 'function') _options['success'](data);

                        return false;

                    }, 
            error: function (xhr) {
                    if (typeof _options['error'] == 'function') _options['error'](xhr);

                    return false;
            }

            });

            return false;
    }


    
    
    
    function renderDialog (data) {


                    $('#my-account-dialog-box').detach(); // remove existing dialog boxes...

                    _dialogBox = $(Mustache.render(ajax.getTemplate('my-account-dialog'), data));  

                    // Show dialog box...
                    $('body').append(_dialogBox);
                    
                    _dialogBox.show();

                    // Password meter
                    Validator.newPasswordMeter('account-field-password', 'account-field-password-meter');

                    $('#fullscreen-bg2, .grp-dialog-box > .cancel-button').unbind('click').click(function () {

                       _dialogBox.fadeOut(200, function () {
                           _dialogBox.detach();
                       });
                       $('#fullscreen-bg2').unbind('click').hide();
                       $(document).unbind('scroll'); 
                       $('body').css({'overflow':'visible'});	

                    })
                    .show();
                    

    }
    
   

    
   
    


    // method: [ editProfile ]
    function editProfile () {
            

            _doAPICall({
                'do': 'getInfo', 
                data: { 'void': 0 }, 
                success: function (data) {
                    var _meta = {
                        title: 'My Account', 
                        buttonLabel: 'save changes'
                    };

                    renderDialog({meta: _meta, Profile: data.Profile});
                    return false;
                },

                error: function (xhr) {
                    return false;
                }
        });
        
        return false;
        
    }
    
    
    
    
    // method: [ saveProfile ]
    function saveProfile () {
        
        
            // Validate form
            var failedItems = Validator.validateForm('edit-my-account-form');
            
            if (failedItems.length > 0) {
                
                for (var i = 0; i < failedItems.length; i++) {
                    if (failedItems[i].type == 'password' && $('#edit-my-account-form #account-field-set-password').prop('checked') == false) continue;
                    
                    showTip(failedItems[i]);
                    $(failedItems[i])
                    .focus()
                    .blur(function () { hideTip(); });
                    return false;
                    break;
                }

            }

            _dialogBox.hide();
            $('#fullscreen-bg2').hide();
            
            hold('Saving profile ...');
            
            var data = Utility.getFormData($('#edit-my-account-form'));

            
            _doAPICall({
                'do': 'saveProfile', 
                data: data, 
                success: function (data) {
                    
                    if (data.failedValidation) {
                        
                        // Failed validation
                        // Show dialog box...
                        release();
                        $('#fullscreen-bg2').show(); 
                        _dialogBox.show();

                          if (data.failedItems.length > 0) {
                              var field = _dialogBox.find('input[name=' + data.failedItems[0].replace(/_/g, '-') + ']') ;
                              if (field.length == 0) field = _dialogBox.find('select[name=' + data.failedItems[0].replace(/_/g, '-') + ']') ;

                              if (field.length > 0) {
                                      showTip(field);
                                      $(field)
                                      .focus()
                                      .blur(function () { hideTip(); });

                              }
                          }
                          
                        return false; 
                    }
                    
                    _dialogBox.detach();
                    
                    
                },

                error: function (xhr) {
                    _dialogBox.show();
                    $('#fullscreen-bg2').show();
                    return false;
                }
        }); 
        
        
    }
    
    
    
    
    function togglePasswordTool (checkbox) {
        var box = $('#my-account-dialog-box #password-tool-wrapper');
        
        if ($(checkbox).prop('checked') == true) box.slideDown(200);
        else box.slideUp(200);
    }
 
    
    
    
        
        


        /*
         * 
         * [ saveDisplayPicture ]
         * 
         */




        function saveDisplayPicture (_options) {
            uploader_engaged = true;


                $('#canvas_image').attr({'src': '../ui/images/transparent_pixel.gif'});
                $('#outer-outer-canvas-wrapper, #photo_editor_controls, #upload_in_progress, #fullscreen_bg').hide();
                hold('Saving photo...');
                
                _doAPICall({
                    target_id: 0, 
                    'do': 'saveDisplayPicture', 
                    data: _options,  
                    success: function(data) {
                        
                            uploader_engaged = false;

                            release(); //alert('Saved', data.message, true);
                            $('.display-picture-image-picker-button').css({
                                                                      'background': 'url(' + data.Profile.DisplayPicture.large + ')',
                                                                      'background-size': 'contain', 
                                                                      'background-repeat': 'no-repeat', 
                                                                      'background-position': 'center center'

                                                                  }); 


                    }, 
                    error: function (xhr) {


                    }

                });


                return false;	

        }
        
      
    
    

    
    
    
    
    //Return public methods
    return {
        editProfile: editProfile, 
        saveProfile: saveProfile,  
        
        saveDisplayPicture: saveDisplayPicture, 
        _doAPICall: _doAPICall, 
        togglePasswordTool: togglePasswordTool
        
    };
    
    
    
 }
 
