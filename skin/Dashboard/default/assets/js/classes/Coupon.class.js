
 
 
// ----------------------------------------------------------------------------------------- //


// Class: [ loadCoupon ]
Cataleya.loadCoupon = function (callBack) {
    
    // Private properties
    
    var 
    
    _dialogBox, 
    _coupon_id = 0, 
    _a = Cataleya.arrayFuncs(), 
    Utility = Cataleya.getUtility(), 
    
    _templates = {};
    

    
    
    // Load moustache templates
    ajax.loadTemplates(['coupon-dialog']);
    
    _templates.couponTile = '<li id="coupon-tile-{{id}}" class="tile user-status {{#isActive}}online{{/isActive}} {{^isActive}}offline{{/isActive}}"><a class="fltlft main-anchor" href="{{href}}"><span class="tiny-label">{{Store.title}}</span> {{code}} ({{population}})</a>'
                    + '<a href="javascript:void(0)"  onclick="Coupon.editCoupon({{id}});" ><span class="icon-pencil-2 fltrt"></span> </a>' 
                    + '<a href="javascript:void(0)" onclick="Coupon.deleteCoupon({{id}});" ><span class="icon-close fltrt"></span></a></li>';




    
    
    
    function renderDialog (data) {


                    $('.grp-dialog-box').detach(); // remove existing dialog boxes...

                    _dialogBox = $(Mustache.render(ajax.getTemplate('coupon-dialog'), data));  

                    // Show dialog box...
                    $('body').append(_dialogBox);
                    
                    _dialogBox.find('#coupon-status-button').click(function () {
                        var _checkbox = $('#coupon-status-checkbox');
                        
                        if (_checkbox.prop('checked') == true) {
                            $(this)
                            .removeClass('online')
                            .addClass('offline')
                            .html('Off (Turn On)');
                            
                            _checkbox.prop('checked', false);
                        } else {
                            $(this)
                            .removeClass('offline')
                            .addClass('online')
                            .html('On (Turn Off)');
                            
                            _checkbox.prop('checked', true);
                        }
                        
                        
                        return false;
                        
                    })
                    .end()
                    .show();



                    $('#fullscreen-bg2, .grp-dialog-box > .cancel-button').unbind('click').click(function () {

                       _dialogBox.hide().remove();
                       
                       $('#fullscreen-bg2').unbind('click').hide();
                       $(document).unbind('scroll'); 
                       $('body').css({'overflow':'visible'});	

                    })
                    .show();
                    

    }
    
   

    
    
    


    // method: [ newCoupon ]
    function newCoupon () {

            var 
            
            _day = 24*60*60*1000, 
            _dateStart = new Date(), 
            _dateEnd = new Date(_dateStart.getTime()+(15*_day)),
            
            _stores = Catalog.getPickerData('Stores'), 
            _default_store = _stores[0], 

            _Coupon = {
                id: 0, 
                code: 'COUPONCODE', 
                discountType: 0, 
                discountAmount: 0, 
                Currency: _default_store.Currency, 
                
                startDateDay: Utility.getRange(1, 31, _dateStart.getDate(), true), 
                startDateMonth: Utility.getMonths(_dateStart.getMonth()), 
                startDateYear: Utility.getRange(_dateStart.getFullYear(), _dateStart.getFullYear()+5, _dateStart.getFullYear()), //_dateStart.getFullYear(), 
                
                endDateDay: Utility.getRange(1, 31, _dateEnd.getDate(), true), 
                endDateMonth: Utility.getMonths(_dateEnd.getMonth()), 
                endDateYear: Utility.getRange(_dateStart.getFullYear(), _dateStart.getFullYear()+5, _dateEnd.getFullYear()), 
                
                usesPerCoupon: 100, 
                isActive: false, 
                Store: _default_store
            }, 
            
            _meta = {
                title: 'New Discount Coupon', 
                buttonLabel: 'save coupon'
            };
            
            renderDialog({meta: _meta, Coupon: _Coupon, Stores: _stores});
            _coupon_id = 0;
            
            return false;
        
    }
    
    
    
    


    // method: [ editCoupon ]
    function editCoupon (id) {
            
            if (typeof id !== 'number') {throw new Exception ('Invalid coupon id!'); return false; }
            
            _coupon_id = parseInt(id);
            ajax.doAPICall({
                api: 'coupon', 
                target_id: _coupon_id, 
                'do': 'getInfo', 
                data: { store_id: 0 }, 
                success: function (data) {
                    var _stores = Catalog.getPickerData('Stores');
                    var _meta = {
                        title: 'Edit Discount Coupon', 
                        buttonLabel: 'save coupon'
                    }, 
                    _dateEnd = new Date();
                    
                    
                    for (var i in _stores) {
                        _stores[i].selected = (_stores[i].id == data.Coupon.Store.id) ? 'selected' : '';
                    }
                    
                    data.Coupon.startDateDay = Utility.getRange(1, 31, parseInt(data.Coupon.startDateDay), true); 
                    data.Coupon.startDateMonth = Utility.getMonths(parseInt(data.Coupon.startDateMonth)); 
                    
                    var startYear = parseInt(data.Coupon.startDateYear);
                    data.Coupon.startDateYear = Utility.getRange(startYear, startYear+5, startYear);

                    data.Coupon.endDateDay = Utility.getRange(1, 31, parseInt(data.Coupon.endDateDay), true); 
                    data.Coupon.endDateMonth = Utility.getMonths(parseInt(data.Coupon.endDateMonth)); 
                    
                    var endYear = parseInt(data.Coupon.endDateYear);
                    data.Coupon.endDateYear = Utility.getRange(_dateEnd.getFullYear(), _dateEnd.getFullYear()+5, endYear); 

                    renderDialog({meta: _meta, Coupon: data.Coupon, Stores: _stores});
                    return false;
                },

                error: function (xhr) {
                    return false;
                }
        });
        
        return false;
        
    }
    
    
    
    
    // method: [ saveCoupon ]
    function saveCoupon (id) {
        
            if (typeof id !== 'number') {throw new Exception ('Invalid coupon id!'); return false; }
            _coupon_id = parseInt(id);
            
            _dialogBox.hide();
            $('#fullscreen-bg2').hide();
            
            hold('Saving coupon ...');
            
            var data = Utility.getFormData($('#edit-coupon-form'));

            
            ajax.doAPICall({
                api: 'coupon', 
                target_id: _coupon_id, 
                'do': ((_coupon_id == 0) ? 'newCoupon' : 'saveCoupon'), 
                data: data, 
                success: function (data) {
                    
            

                    _dialogBox.detach();
                    
                     var new_tile = $(Mustache.render(_templates.couponTile, data.Coupon));
                     var oldTile = $('li#coupon-tile-'+data.Coupon.id);

                     if (oldTile.length > 0) oldTile.before(new_tile);
                     else $('#new-coupon-tile').before(new_tile); 


                     // In case we're modifing an existing tile, remove from list...
                     if (oldTile.hasClass('current')) new_tile.addClass('current');
                     oldTile.detach(); 
                     
                     Utility.refreshPickerData();
                    
                },

                error: function (xhr) {
                    _dialogBox.show();
                    $('#fullscreen-bg2').show();
                    return false;
                }
        }); 
        
        
    }
    
    
    
    
    
    
    

    // method: [ deleteCoupon ]
    function deleteCoupon (id) {
            
            if (typeof id !== 'number') {throw new Exception ('Invalid coupon id!'); return false; }
            
            _coupon_id = 0;
            ajax.doAPICall({
                api: 'coupon', 
                target_id: parseInt(id), 
                'do': 'deleteCoupon', 
                data: { store_id: 0 }, 
                success: function (data) {

                    $('li#coupon-tile-'+data.Coupon.id).detach();
                    return false;
                },

                error: function (xhr) {
                    return false;
                }
        });
        
        return false;
        
    }
    
    
    
    
    
    
    
    
    // method: [ resetCurrency ] 
    function resetCurrency (control) {
        var code = $(control.options[control.selectedIndex]).attr('currencyCode');
        if (typeof code === 'string' && code.length == 3) _dialogBox.find('#coupon-currency').text(code);
        
        validateCode();
    }
    
    
    // method: [ validateAmount ] 
    function validateAmount () {
        var amount = _dialogBox.find('#text-field-discount-amount');
        var type = _dialogBox.find('#select-field-discount-type').get(0);
        
        if (type.selectedIndex == 0 && parseInt(amount.val()) > 100) amount.val(0);
    }
    
    
    // method: [ validateCode ] 
    function validateCode () {
        var _coupons = Catalog.getPickerData('Coupons');
        var _field = _dialogBox.find('#text-field-coupon-code');
        var _store_id = parseInt(_dialogBox.find('#coupon-store-id').val());
        
        for (var i in _coupons) {
            if (parseInt(_coupons[i].id) != _coupon_id && _store_id == _coupons[i].store_id && _coupons[i].code == _field.val().toUpperCase()) {
                _field.addClass('invalid');
                return; break;
            }
        }
        
        // if (_field.val() == '' && !_field.hasClass('invalid')) _field.addClass('invalid');
        _field.removeClass('invalid');
        
    }


    
    
    
    
    //Return public methods
    return {
        newCoupon: newCoupon, 
        editCoupon: editCoupon, 
        saveCoupon: saveCoupon, 
        deleteCoupon: deleteCoupon, 
        
        resetCurrency: resetCurrency, 
        validateAmount: validateAmount, 
        validateCode: validateCode
        
    };
    
    
    
 }
 
 
 