




 
 // ------------------------------------------------------------------------------------ //



// Class: [ Stores ]
Cataleya.newStoreWidget = function (_store_id, callBack) {
    
    
    
    var 
    env = {
        store_id: _store_id, 
        label_color: document.getElementById('select-label-color').value, 
        dialogBox: {}, 
        
        _frame_index: [], 
        frame_width: 0, 
        max_frame_rounds: 0, 

        activeFrame: 1
    };





    // FUNCTIONS FOR SUB_PANE NAVIGATOR ....

    function nextPane () {  
        var _frame = env.activeFrame+1;
        return navigateToFrame (_frame);
    }



    function prevPane () {

        var _frame = env.activeFrame-1;
        navigateToFrame (_frame);
    }
    
    
    function seekPane (_label) {

        for (var i=0; i < env._frame_index.length; i++) {
            if (_label == env._frame_index[i]) {
                return navigateToFrame (i+1);
                break;
            }
        }

        return false;
    }




    function navigateToFrame (frame) {

        if (frame-1 >= env.max_frame_rounds || frame-1 < 0) return false;

            $('#frame-headers li:nth-child(' + env.activeFrame + ')').fadeOut(300, function () {
                            env.activeFrame = frame;
                            env.frame_rounds = frame-1;

                            var slide = -((frame-1) * env.frame_width); // 598);
                            $('#sub-pane-strip').animate({left: slide}, 400);
                            $('#frame-headers li:nth-child(' + frame + ')').fadeIn(300);

            });

            $('#bread-crumbs li.active-crumb').removeClass('active-crumb');
            $('#bread-crumbs li:nth(' + (frame-1) + ')').addClass('active-crumb');

            return false;

    }
    
    
    


    loadData();

    __el(document.querySelector('#store-edit-form-wrapper .uni-label input'))
    .on('focus', uniLabelInputFocusHandler);


    // method: [ refresh ]
    function loadData (callBack) {
        
        

        // Load moustache templates
        ajax.loadTemplates([
            'color-picker-widget'
        ]);
        
        
        //__el('.content').appendChild(Mustache.render(ajax.getTemplate('color-picker-widget'), { 'void': '' }));
        
        //__el('#color-picker-widget-wrapper').css({ display: 'block' });
        
        if (typeof callBack == 'function') callBack();
        return false;
        
        
    }
    
    
    

    
    
    // method: [ deleteStore ]
    function deleteStore (_id, _redirect) {
            
            hold ('Please wait...');
            
            
            _redirect = (typeof _redirect === 'boolean') ? _redirect : false;


            ajax.doAPICall ({ 
                    api: 'store', 
                    'do': 'deleteStore',
                    target_id: _id, 
                    data: { store_id: env.store_id }, 
                    dataType: 'json', 
                    success: function (data) {


                            if (typeof callBack === 'function') callBack(data);
                            
                            if (_redirect) window.location = '/';
                            

                            return false;

                            },

                    error: function (xhr) {
                        
                        return false;
                    }
            });
            
            
            
            
        }
        
        
        
        
    
    
    // method: [ newStore ]
    function newStore () {
        editStore('new');
        
        return true;
    } 
    
    
    // method: [ editStore ]
    function editStore (_id, _callBack) {


            hold('Please wait...');
            
            var _do, _target_id, _mode;
            

            if (_id === 'new') {
                _do = 'newStore';
                _target_id = env.store_id;
                _mode = 'new';
            } else {
                _do = 'getStoreInfo';
                _target_id = env.store_id;
                _mode = 'edit';
            }
            

            ajax.doAPICall({
                api: 'store', 
                'do': _do,
                'target_id': _target_id,  
                'data': { 'store_id': env.store_id }, 
                'success': function (data) {
                    
                    console.log(data);
                    
                    release();
                    
                    env.store_id = data.Store.id;
                    

                    renderPageDialogBox(data, _mode, _callBack);    




                    }
            });

    }
    
    
    
    
    

    /*
     * 
     * [ FUNCTION ] renderStoreDialogBox 
     * ____________________________________________________________________________
     * 
     * Self explanatory.
     * 
     */



    function renderStoreDialogBox (data, _mode, _callBack) {

    
                    env.dialogBox = $(Mustache.render(ajax.getTemplate('store-dialog-box'), data));
                     

                    // Show dialog box...
                    $('body').append(env.dialogBox);

                    var 
                    
                    closeStoreDialogBox = function closeStoreDialogBox () {

                        // storesChecklist.destroy();
                        
                        
                        $('#fullscreen-bg3').unbind('click').hide();
                        MyShortcuts.clear('close', closeStoreDialogBox);
                        MyShortcuts.clear('save', saveStoreBodyHandler);
                        
                        env.dialogBox.remove();
                    }, 
                            
                    
                    saveStoreBodyHandler = function saveStoreBodyHandler () {

                        saveContentBody(quill.getHTML());

                        
                    };
                    

                    $('#fullscreen-bg3, #new-item-pane > .cancel-button').click(closeStoreDialogBox)
                    .show();
                    env.dialogBox.show();
                    
                    
                     var quill = new Quill('.quill-editor > #editor', { 
                         modules: {
                             'toolbar': { container: '.quill-editor > #toolbar' } 
                         }
                     });


                    MyShortcuts.register('close', closeStoreDialogBox);
                    MyShortcuts.register('save', saveStoreBodyHandler, {'disable_in_input':false});
                    
                    


                    // Index the frames for the [ seekPanel() ] method.
                    env._frame_index = [];
                    env.activeFrame = 1;
                    $('#frame-headers > li').each(function (i, obj) {
                        env._frame_index[i] = $(this).attr('label');
                    });


                    // Set max number of frames
                    env.max_frame_rounds = env._frame_index.length;
                    env.frame_width = parseInt( env.dialogBox.find('.sub-pane').first().css('width') ) + 10;

                            
                    

                    if (typeof _callBack === 'function') _callBack();

    }
    
    
    
    
    
    

    /*
     * 
     * [ FUNCTION ] saveContentBody 
     * ____________________________________________________________________________
     * 
     * Self explanatory.
     * 
     */



    function saveContentBody (content, tinyStatusSelector) {


            // For tiny status...
            if (typeof tinyStatusSelector === "string") { 
                $(tinyStatusSelector).html('Saving...').toggleClass('blink');
            }





            var 

            _target_id = env.store_id, 


            data = {
                store_content: content
            },
            _do = 'saveBody', 
            _doHold = true;



            ajax.doAPICall({
                api: 'store', 
                target_id: _target_id, 
                'do': _do, 
                data: data, 
                hold: _doHold,
                tinyStatus: tinyStatusSelector, 
                success: function (data) {
                        
                        return false;

                        },

                error: function (xhr) {
                        return false;
                }
            });

            return false;


    }



    
    
    


    /*
     * 
     * [ FUNCTION ] saveDescription 
     * ____________________________________________________________________________
     * 
     * Self explanatory.
     * 
     */



    function save (field) {

            var _field = $(field);

            var _sync_id = _field.attr('data-sync-id');





            var 

            _target_id = 0, 


            data = {
                text: field.value
            },
            _do = 'void', 
            _doHold = true;




            switch (field.name) {
                case 'store-name':
                    _do = 'saveTitle';
                    _target_id = env.store_id;
                    break;
                    
                case 'store-description':
                    _do = 'saveDescription';
                    _target_id = env.store_id;
                    break;
                    
                case 'store-theme':
                    _do = 'saveTheme';
                    _target_id = env.store_id;
                    data.theme_id = field.value;
                    break;
                    
                case 'store-handle':
                    _do = 'saveHandle';
                    _target_id = env.store_id;
                    data.shop_handle = field.value;
                    break;
                    
                case 'store-label-color':
                    _do = 'saveLabelColor';
                    _target_id = env.store_id;
                    data.label_color = field.value;
                    break;
                    
                case 'store-hours':
                    _do = 'saveStoreHours';
                    _target_id = env.store_id;
                    break;
                    
                case 'store-timezone':
                    _do = 'saveTimeZone';
                    _target_id = env.store_id;
                    break;
                    
                case 'store-language':
                    _do = 'saveLanguage';
                    _target_id = env.store_id;
                    break;
                    
                case 'store-currency':
                    _do = 'saveCurrency';
                    _target_id = env.store_id;
                    break;
                    
                case 'contact-name':
                    _do = 'saveContactName';
                    _target_id = env.store_id;
                    break;
                    
                case 'contact-email':
                    
                    if ((field.value === '' || !(EMAIL_REGEXP.test(field.value))) ) {
                                            showTip(field, 'Please enter a valid email address.');
                                            setTimeout('hideTip()', 3000);
                                            return false;
                                            }
                                            
                    _do = 'saveContactEmail';
                    _target_id = env.store_id;
                    data.email = field.value
                    break;
                    
                case 'contact-phone':
                    _do = 'saveContactPhone';
                    _target_id = env.store_id;
                    break;
                    
                case 'street-address':
                    _do = 'saveContactStreetAddress';
                    _target_id = env.store_id;
                    break;
                    
                case 'street-address-2':
                    _do = 'saveContactStreetAddress2';
                    _target_id = env.store_id;
                    break;
                    
                case 'city':
                    _do = 'saveContactCity';
                    _target_id = env.store_id;
                    break;
                    
                case 'state':
                    _do = 'saveContactState';
                    _target_id = env.store_id;
                    break;
                    
                case 'country':
                    _do = 'saveContactCountry';
                    _target_id = env.store_id;
                    break;
                    
                case 'postcode':
                    _do = 'saveContactPostCode';
                    _target_id = env.store_id;
                    break;
                    
                default:
                    _do = 'void';
                    break;
            }

            
            
            


            // For tiny status...
            var tinyStatusSelector = _field.attr('tinyStatus');
            if (typeof tinyStatusSelector === "string") { 
                $(tinyStatusSelector).html('Saving...').toggleClass('blink');
                
                _doHold = false;
            }


            // For uni-label status...
            var uniLabelStatus = _field.attr('data-uni-label-status');
            
            if (typeof uniLabelStatus === "string") { 

                uniLabelStatus = $(uniLabelStatus);
                _field.attr('uni-label-title', uniLabelStatus.text());

                uniLabelStatus.html('<span class="blink">Saving...</span>');
                _field.hide();
                uniLabelStatus.show();
                
                _doHold = false;
            }
            
            

            ajax.doAPICall({
                api: 'store', 
                target_id: _target_id, 
                'do': _do, 
                data: data, 
                hold: _doHold,
                tinyStatus: tinyStatusSelector,  
                uniLabel: { status: uniLabelStatus, field: field }, 
                success: function (data) {
                    
                        
                        switch (_do) {
                            case 'saveTitle': 
                                env.dialogBox.find('.store-name').html(data.Store.title);
                            break;
                            
                            case 'saveLabelColor': 
                                var _sid = '[data-sync-id=' + _sync_id + ']';
                                
                                __el(document.querySelectorAll(_sid)).removeClass(env.label_color).addClass(data.Store.label_color);
                                env.label_color = data.Store.label_color;
                               
                                
                            break;

                        }


                        return false;

                        },

                error: function (xhr) {
                        return false;
                }
            });

            return false;


    }





    /*
     * 
     * [ FUNCTION ] uniLabelInputFocusHandler
     * ____________________________________________________________________________
     * 
     * Self explanatory.
     * 
     */


    function uniLabelInputFocusHandler (e)
    {
                e = e || window.event;
                var target = e.target || e.srcElement;
                
                //var This = $(target);
                var _uniLabelInputCache = target.value;
                
                
                var formatInput = function  (i) {
                    
                    // var This = $(i);
                    
                    switch (i.getAttribute('data-format')) {
                            case 'price':
                                i.value(numberFormat(i.value, 2));
                            break;
                            
                            case 'float':
                                i.value(parseFloat(i.value));
                            break;
                            
                            case 'integer':
                                i.value(parseInt(i.value));
                            break;
                            
                            case 'upper-case':
                                i.value(i.value.toUpperCase());
                            break;
                            
                            case 'lower-case':
                                i.value(i.value.toLowerCase());
                            break;
                            
                        }
                       
                };



                __el(target).on('change', doUniLabelInputSave);



                function cancelUniLabelInputSave () {
                    
                    __el(target).off('change', doUniLabelInputSave);

                    target.blur();
                    target.value = _uniLabelInputCache;


                    MyShortcuts.clear('return', doUniLabelInputSave);
                    MyShortcuts.clear('cancel', cancelUniLabelInputSave);
                }

                function doUniLabelInputSave () {
                    formatInput(target);

                    __el(target).off('change', doUniLabelInputSave);
                    target.blur();

                    self.save(target); 



                    MyShortcuts.clear('return', doUniLabelInputSave);
                    MyShortcuts.clear('cancel', cancelUniLabelInputSave);
                }


                MyShortcuts.register('return', doUniLabelInputSave, {'disable_in_input':false});
                MyShortcuts.register('cancel', cancelUniLabelInputSave, {'disable_in_input':false});

    }







    


    /*
     * 
     * [ FUNCTION ] toggleStoreStatus
     * ____________________________________________________________________________
     * 
     * Self explanatory.
     * 
     */



    function toggleStoreStatus () {

            hold ('Please wait...');

            var data = {
                store_id: env.store_id

            }, 
            
            _do = ($('#profile-name-in-header').hasClass('offline')) ? 'openStore' : 'closeStore', 
            _doHold = true;






            ajax.doAPICall ({
                    api: 'store', 
                    target_id: env.store_id, 
                    'do': _do, 
                    data: data, 
                    hold: _doHold, 
                    success: function (data) {
                                    alert ('Ok', data.message, true);

                                    if (data.Store.active) { 
                                        $('#profile-name-in-header').removeClass('offline').addClass('online');
                                        $('#store-status-toggle-button').html('close this store');
                                    }
                                    else { 
                                        $('#profile-name-in-header').removeClass('online').addClass('offline');
                                        $('#store-status-toggle-button').html('open this store');
                                    }
                                    return false;

                            },

                    error: function (xhr) {
                        return false;
                    }
            });

            return false; 

    }







    /*
     * 
     * [ FUNCTION ] toggleAcceptedPayment 
     * ____________________________________________________________________________
     * 
     * Adds or removes an accepeted payment type.
     * 
     */



    function toggleAcceptedPayment (_id) {
            
            
            var pt = __el('#payment-type-' + _id + '-anchor');
            
            var data = {
                payment_processor_id: _id, 
                store_id: env.store_id 
            }, 
            _do = pt.hasClass('icon-checkbox-unchecked') ? 'enablePaymentProcessor' : 'disablePaymentProcessor';

            pt.toggleClass('icon-checkbox-partial').toggleClass('icon-checkbox-unchecked');



            ajax.doAPICall ({
                    api: 'store', 
                    target_id: _id, 
                    'do': _do, 
                    data: data, 
                    hold: true, 
                    success: function(data) {
                        

                            if (data.PaymentProcessor.active) {
                                pt
                                .removeClass('icon-checkbox-unchecked')
                                .addClass('icon-checkbox-partial');
                            } else {
                                pt
                                .removeClass('icon-checkbox-partial')
                                .addClass('icon-checkbox-unchecked');
                            }
                            return false;

                            }, 
                    error: function (data) {
                        
                            pt.toggleClass('icon-checkbox-partial').toggleClass('icon-checkbox-unchecked');
                            return false;
                            
                    }

                    });

            return false;
    }









    /*
     * 
     * [ saveDisplayPicture ]
     * 
     */




    function saveDisplayPicture (_options) {
            uploader_engaged = true;



            ajax.doAPICall ({
                    api: 'store', 
                    target_id: env.store_id, 
                    'do': 'saveDisplayImage', 
                    data: _options, 
                    success: function(data) {
                            uploader_engaged = false;

                            alert('Saved', data.message, true);
                            $('.image-picker-add-button').css({
                                                               'background': 'url(' + data.Image.hrefs.thumb + ')',
                                                               'background-size': 'contain', 
                                                               'background-repeat': 'no-repeat', 
                                                               'background-position': 'center center'

                                                           });

                    }, 
                    error: function (xhr) {

                    }



                    });


            return false;	

    }




    
    
    
    
    

    
    
    
    
    //Return public methods
    var self = {
        
        save: save, 
        deleteStore: deleteStore,
        newStore: newStore, 
        editStore: editStore, 
        toggleStoreStatus: toggleStoreStatus, 
        
        toggleAcceptedPayment: toggleAcceptedPayment, 
        
        saveDisplayPicture: saveDisplayPicture, 
        
        nextPane: nextPane, 
        prevPane: prevPane, 
        seekPane: seekPane
        
    };
    
    return self;
    
    
    
 };






