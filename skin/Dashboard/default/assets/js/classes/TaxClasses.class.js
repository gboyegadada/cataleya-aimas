




// ----------------------------------------------------------------------------------------- //


// Class: [ loadTaxClasses ]
Cataleya.loadTaxClasses = function () {
    
    // Private property
    var _collection = [ /* tax classes */ ];
    
    
    var query = {};



    loadData();


    // method: [ refresh ]
    function loadData (callBack) {

        ajax.jsonRequest ({
                method: 'POST', 
                url: 'get-tax-Cataleya.ajax.php', 
                data: {auth_token: $('#meta_token').val() }, 
                dataType: 'json', 
                success: function (response) {
                            
                            var data = response.json();
                    
                            setData(data);
                            
                            // Recalculate page content height
                            var unit_height = (data.count > 1) ? data.count : 2;
                            var new_height = ((unit_height / 3) * 300)+450;
                            $('.content').css('height', new_height);
                            
                            if (typeof callBack == 'function') callBack();
                            return false;

                        },

                error: function (xhr) {
                    return false;
                }
        });
    }

    
    // private method: [ setData ]
    function setData (data) {
        _collection = data.TaxClasses;
    } 
    
    
    
    /*
     *
     * private method: [ renderDialog ]
     * 
     */

    function renderDialog (_id) {
    
            // Remove previous click events...
            $('#tax-class-dialog-box #tax-class-button-cancel, #fullscreen-bg2').unbind('click');

            var dialogBox = $('#tax-class-dialog-box');

            // Set up dialog box...
            if (_id == 0) {

                // NEW
                dialogBox.find('.bubble-title').html('New Tax Class');
                $('#tax-class-id').val(0);
                $('#text-field-tax-class-name').val('');
                $('#tax-class-button-ok').val('create new tax class');
                $('#normal-list-radio').prop('checked', true);

            } else {

                // EDIT
                dialogBox.find('.bubble-title').html('Edit Tax Class');
                $('#tax-class-id').val(_id);

                var TaxClass = getTaxClass(_id);
                $('#text-field-tax-class-name').val(TaxClass.name);
                $('#text-field-tax-class-description').val(TaxClass.description);
                $('#tax-class-button-ok').val('save changes');
            }

            // Init dialog 'cancel' button...
            $('#tax-class-dialog-box #tax-class-button-cancel, #fullscreen-bg2').click(function () {
                    $('#tax-class-dialog-box, #fullscreen-bg2').fadeOut(300);
                    hideTip();
                    return false;
            });

            // Show dialog box...
            $('#tax-class-dialog-box, #fullscreen-bg2').show();

    }
    
    
    // method: [ sendRequest ]
    function sendRequest () {
        
            // Validate form
            var nameField = $('#text-field-tax-class-name');
            var descriptionField = $('#text-field-tax-class-description');



            if (NAME_REGEXP.test(nameField.val()) || nameField.val().length < 2) {
                showTip(nameField);
                nameField
                .focus()
                .one('blur', function () { hideTip(); });

                return false;
            }
            
            if (DESCR_REGEXP.test(descriptionField.val()) || descriptionField.val().length < 2) {
                showTip(descriptionField);
                descriptionField
                .focus()
                .one('blur', function () { hideTip(); });

                return false;
            }

            $('#tax-class-dialog-box').hide();
            hold('Adding new tax class...');

            ajax.jsonRequest ({
                    method: 'POST', 
                    url: $('#tax-class-form').prop('action'), 
                    data: $('#tax-class-form').serialize(), 
                    dataType: 'json', 
                    success: function (response) {
                            
                            var data = response.json();

                              if (!data.TaxClass.isNew) {location.reload(); return true; }

                              // In case we're modifing an existing tax class, remove from list first...
                              $('#tax-class-'+data.TaxClass.id).detach();


                              // Rates in Tax Class
                              var tax_rate_info = countInEnglish(data.TaxClass.population, 'Tax Rate', 'Tax Rates');



                              // Then add new tile...
                              var new_tile = $('<a href="rates.tax.php?id=' + data.TaxClass.id + '"><div class="mediumListIconTextItem"><span class="icon-globe tile-icon-large" ></span><div class="mediumListIconTextItem-Detail"><h4>' + data.TaxClass.name + '</h4>' + 
                                                '<h6>' + tax_rate_info + '</h6></div></div></a>'
                                    );

                              // Insert new tile...
                               $('#tax-classes-grid-wrapper').prepend(new_tile);


                                // reload data then bring it back
                                loadData(release);

                                return false;


                            },

                    error: function (xhr) {
                        return false;
                    }
            });
            return false;

    }
    
    
    
    // method: [ newTaxClass ]
    function newTaxClass (id) {
        renderDialog(id);
        
        return true;
    } 
    
    
    // method: [ editTaxClass ]
    function editTaxClass (id) {
        renderDialog(id);
        
        return true;
    } 
    
    
    
    
    
    
    // method: [ getTaxClass ]
    function getTaxClass (id) {
        for (var i = 0; i < _collection.length; i++) {
            if (_collection[i].id == id) return _collection[i];
        }
        
        return {};
    } 
    
    

    

    
    // method: [ deleteTaxClass ]
    function deleteTaxClass (_id) {
            
            hold ('Please wait...');
            
            query = {
                    auth_token: $('#meta_token').val(),
                    tax_class_id: (typeof query.tax_class_id == 'undefined' || query.tax_class_id == 0) ? _id : query.tax_class_id

                    };
            


            // Do request...
            ajax.jsonRequest ({
                    method: 'POST', 
                    url: 'delete-tax-class.ajax.php', 
                    data: query, 
                    dataType: 'json', 
                    success: function (response) {
                            
                            var data = response.json();
                                query.tax_class_id = 0; 
                                window.location = 'Cataleya.tax.php'; 
                                return false;

                            },

                    error: function (xhr) {
                                query.tax_class_id = 0; // reset id
                                return false;
                            }
            });
        }

    
    
    
    
    //Return public methods
    return {
        
        refresh: loadData, 
        deleteTaxClass: deleteTaxClass,
        getTaxClass: getTaxClass, 
        newTaxClass: newTaxClass, 
        editTaxClass: editTaxClass, 
        submitForm:  sendRequest
        
    };
    
    
    
 }
 
 
 
 