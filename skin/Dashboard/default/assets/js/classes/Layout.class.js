
 
 
// ----------------------------------------------------------------------------------------- //




// Class: [ loadLayout ]
Cataleya.loadLayout = function (_store_id, callBack) {
    
    // if (typeof _store_id != 'number') {throw new Exception ('Invalid layout id!'); return false; }
    
    
    // Private properties
    
    var 
    
    _dialogBox, 
    _auth_token = $('#meta_token').val(), 
    _layout_id = parseInt(_store_id), 
    _active_slide_id = 0, 
    _a = Cataleya.arrayFuncs(), 
    Utility = Cataleya.getUtility(), 
    
    _query_temp = {
        auth_token: _auth_token,
        target_id: 0, 
        'do': 'void', 
        data: []

    };
    
    
    

    
    
    // Load moustache templates
    ajax.loadTemplates(['layout-dialog', 'layout-tile-dialog', 'layout-tile', 'layout-slide-dialog'], true);




    /*
     * 
     *  Private: [ FUNCTION ] _doAPICall
     * ____________________________________________________________________________
     * 
     * 
     * 
     */



    function _doAPICall(_options) {

            hold ('Please wait...');

            var query = _query_temp;

            if (typeof _options['target_id'] !== 'undefined') query['target_id'] = _options['target_id'];
            if (typeof _options['do'] !== 'undefined') query['do'] = _options['do'];
            if (typeof _options['data'] !== 'undefined') query['data'] = _options['data'];
            



            ajax.jsonRequest ({
            method: 'POST', 
            url: 'layout.api.ajax.php', 
            data: query, 
            dataType: 'json', 
            success: function(response) {

                        var data = response.json();
                        
                        if (typeof _options['success'] == 'function') _options['success'](data);

                        return false;

                    }, 
            error: function (xhr) {
                    if (typeof _options['error'] == 'function') _options['error'](xhr);

                    return false;
            }

            });

            return false;
    }


    
    
    
    function renderDialog (data) {


                    $('.layout-dialog-box').remove(); // remove existing dialog boxes...
                    

                    _dialogBox = $(Mustache.render(ajax.getTemplate('layout-dialog'), data));  

                    // Show dialog box...
                    $('body').append(_dialogBox);
                    
                    
                    
                    for (var i in data.Layout.Tiles) $('#layout-tile-'+data.Layout.Tiles[i].id).data('source', data.Layout.Tiles[i]);
                    

                    _dialogBox.find('li.layout-tile')
                    .mouseenter(function () {
                        $(this).find('.layout-tile-tool-box, .layout-tile-dashboard-wrapper').fadeIn(200);
                    })
                    .mouseleave(function () {
                        $(this).find('.layout-tile-tool-box, .layout-tile-dashboard-wrapper').fadeOut(200);
                    });

                    $('#fullscreen-bg3, .layout-dialog-box > .cancel-button').unbind('click').click(function () {

                       _dialogBox.hide().remove();
                       $('#layout-slide-dialog-box').remove();
                           
                       $('#fullscreen-bg3').unbind('click').hide();
                       $(document).unbind('scroll'); 
                       $('body').css({'overflow':'visible'});	

                    })
                    .show();
                    
                    
                    /*

                    SET DRAGGABLES

                    */


                    _dialogBox
                    .find('.drag-handle')
                    .css('cursor', openHandCursor)
                    .end()
                    .find('#layout-tile-grid-wrapper')
                    .sortable({
                                    //containment: '#layout-tile-grid-wrapper', 
                                    placeholder: '.layout-tile-placeholder', 
                                    scroll: true, 
                                    tolerance: 'intersect', 
                                    handle: '.drag-handle', 
                                    update: function () { $('#sort-order-tool-wrapper').slideDown(200); },
                                    forcePlaceholderSize: true, 
                                    //grid: [120, 25], 
                                    zIndex: 12700
                                    }).disableSelection();
                    
                    
                    _dialogBox.show();
                    
//                    setTimeout(function () {
//                        console.log($('#layout-tile-grid-wrapper').sortable('toArray', {attribute: 'sort_id'}));
//                    }, 3000);
                    

    }
    
    
    
    
    
  

    
    /*
     * 
     * [ FUNCTION ] saveSortable
     * ____________________________________________________________________________
     * 
     * 
     * 
     */
    
    function saveSortable() {
        
        var data = { layout_sort_order: $('#layout-tile-grid-wrapper').sortable('toArray', {attribute: 'sort_id'}) };
        
        console.log(data);
        



        _doAPICall({
            target_id: _layout_id, 
            'do': 'saveLayoutSortOrder', 
            data: data, 
            success: function (data) {
                
                
                $('#sort-order-tool-wrapper').slideUp(200);
                

                return false;
            },

            error: function (xhr) {
                return false;
            }
        });


        return false;
    }


    
    
    
    
    
   

    
    
    /*
     * 
     * [ FUNCTION ] newTile
     * ____________________________________________________________________________
     * 
     * Removes a product tag.
     * 
     */


     function editTile(_id) {
         var 
         
         Tile, 
         _do, 
         _payload = {};
         
         
        if (typeof _id == 'number') {
            
            _payload = {
                title: 'Edit Layout Tile', 
                description: 'Choose a width and a height (in pixels)', 
                buttonLabel: 'save changes', 
                width: [], 
                height: []
            };
            
            _do = 'saveTile';
            
            Tile = $('#layout-tile-'+_id).data('source');
            
        } else {
            _payload = {
                title: 'New Layout Tile', 
                description: 'Choose a width and a height (in pixels)', 
                buttonLabel: 'create new tile', 
                width: [], 
                height: []
            };
            
            _id = _layout_id;
            _do = 'newTile';
            Tile = {pixel_width: 160, pixel_height: 25}
        }
        
        
        // generate width options
         for (var w = 160; w <= (160*6); w+=160) {
             _payload.width.push ({
                 value: w, 
                 label: w + 'px', 
                 selected: (Tile.pixel_width == w) ? 'selected' : ''
             });
         }
         
         
        // generate height options
         for (var h = 25; h < (25*40); h+=25) {
             _payload.height.push ({
                 value: h, 
                 label: h + 'px', 
                 selected: (Tile.pixel_height == h) ? 'selected' : ''
             });
         }
         
         


        renderTileDialog ({
            _do: false, 
            callBack: function (_data) {
                        
                        _doAPICall({
                            target_id: _id, 
                            'do': _do, 
                            data: _data, 
                            success: function (data) {
                                
                                renderTile(data.Tile);
                                

                                return false;
                            },

                            error: function (xhr) {
                                return false;
                            }
                        });
            },

            payload: _payload

        });

        return false;

     }
     
     
     
     
    

    // method: [ deleteTile ]
    function deleteTile (id) {
            
            if (typeof id !== 'number') {throw new Exception ('Invalid layout id!'); return false; }
            
            _doAPICall({
                target_id: parseInt(id), 
                'do': 'deleteTile', 
                data: { 'void': 0 }, 
                success: function (data) {

                    _dialogBox.find('li#layout-tile-'+data.Tile.id).remove();
                    return false;
                },

                error: function (xhr) {
                    return false;
                }
        });
        
        return false;
        
    }
    
    
     
     
     
     


    
    
    
    
    
    /*
     * 
     * [ FUNCTION ] nextSlide
     * ____________________________________________________________________________
     * 
     * 
     * 
     */


     function gotoSlide(_id, slidePos, _useID) {
            
            if (typeof _id !== 'number' || typeof slidePos !== 'number') { throw new Exception ('Invalid slide position or tile id!'); return; }
            
            var 
            
            Tile = $('#layout-tile-'+_id), 
            TileData = Tile.data('source'), 
            pixelPos = 0;
            
            if (typeof _useID === 'boolean' && _useID === true) {
                for (var i in TileData.Slides) {
                    if (slidePos == TileData.Slides[i].id) { slidePos = i; break; }
                }
            }
            
            slidePos = parseInt(slidePos);
            if (slidePos > parseInt(TileData.population)-1) return;

            pixelPos = slidePos*parseInt(TileData.adjusted_pixel_width);
            Tile.find('.slide-strip').animate({left: -pixelPos}, 200);
            Tile.data('slidePos', slidePos);
            
            _active_slide_id = TileData.Slides[slidePos].id;
            
            return;
     }
    
    
    
    
    

    
    
    /*
     * 
     * [ FUNCTION ] nextSlide
     * ____________________________________________________________________________
     * 
     * 
     * 
     */


     function nextSlide(_id) {
            
            var 
            
            Tile = $('#layout-tile-'+_id), 
            TileData = Tile.data('source'), 
            slidePos = Tile.data('slidePos'), 
            pixelPos = 0;
            
            if (typeof slidePos !== 'number') slidePos = 0;
            
            if (slidePos >= parseInt(TileData.population)-1) return;
            slidePos++;

            pixelPos = slidePos*parseInt(TileData.adjusted_pixel_width);
            Tile.find('.slide-strip').animate({left: -pixelPos}, 200);
            Tile.data('slidePos', slidePos);
            
            _active_slide_id = TileData.Slides[slidePos].id;
            
            return;
     }
     
     
     
     
     
     

    
    
    /*
     * 
     * [ FUNCTION ] prevSlide
     * ____________________________________________________________________________
     * 
     * 
     * 
     */


     function prevSlide(_id) {
            
            var 
            
            Tile = $('#layout-tile-'+_id), 
            TileData = Tile.data('source'), 
            slidePos = Tile.data('slidePos'), 
            pixelPos = 0;
            
            if (typeof slidePos === 'number') {
                if (slidePos == 0) return;
                
                slidePos--;
            } 
            
            else return;

            pixelPos = slidePos*parseInt(TileData.adjusted_pixel_width);
            Tile.find('.slide-strip').animate({left: -pixelPos}, 200);
            Tile.data('slidePos', slidePos);
            
            _active_slide_id = TileData.Slides[slidePos].id;
            
            return;
     }
    
    
    




    
    
    
    

    
    /*
     * 
     * [ FUNCTION ] saveSlide
     * ____________________________________________________________________________
     * 
     * 
     * 
     */
    
    function saveSlide(_id) {

        var data = Utility.getFormData('#edit-layout-slide-form');
        
        $('#layout-slide-dialog-box').fadeOut(200, function () {
            $(this).remove();
        });
        
        _dialogBox.css('z-index', 10001);



        _doAPICall({
            target_id: _id, 
            'do': 'saveSlide', 
            data: data, 
            success: function (data) {
                
                renderTile(data.Tile);
                gotoSlide(parseInt(data.Tile.id), parseInt(data.Slide.id), true);
                

                return false;
            },

            error: function (xhr) {
                return false;
            }
        });


        return false;
    }


    
    



  
    
    /*
     * 
     * [ FUNCTION ] newSlide
     * ____________________________________________________________________________
     * 
     * 
     * 
     */


     function newSlide(_id) {

         if (typeof _id != 'number') { throw new Exception ('Invalid layout id!'); return false; }


        _doAPICall({
            target_id: _id, 
            'do': 'newSlide', 
            data: { 'void': 0 }, 
            success: function (data) {
                var _meta = {
                    title: 'New Slide', 
                    buttonLabel: 'save changes'
                };

                _active_slide_id = data.Slide.id;
                renderTile(data.Tile);
                
                renderSlideDialog({meta: _meta, Slide: data.Slide});
                return false;
            },

            error: function (xhr) {
                return false;
            }
        });

        return false;

     }
     
     
  






    
    
    /*
     * 
     * [ FUNCTION ] editSlide
     * ____________________________________________________________________________
     * 
     * 
     * 
     */


     function editSlide(_id) {

        if (typeof _id != 'number') {throw new Exception ('Invalid layout id!'); return false; }
            

        var 

        Tile = $('#layout-tile-'+_id), 
        TileData = Tile.data('source'), 
        slidePos = Tile.data('slidePos'); 
        
        if (TileData.population == 0) { throw new Exception ('Layout Slide data not found!'); return false; }

 
        var _meta = {
            title: 'Edit Slide', 
            buttonLabel: 'save changes'
        };
        
        slidePos = (typeof slidePos === 'number') ? slidePos : 0;
        _active_slide_id = TileData.Slides[slidePos].id;

        renderSlideDialog({meta: _meta, Slide: TileData.Slides[slidePos]});
        

        return false;

     }
     
     
     
     
     

    
    
    /*
     * 
     * [ FUNCTION ] deleteSlide
     * ____________________________________________________________________________
     * 
     * 
     * 
     */


     function deleteSlide(_id) {

        if (typeof _id != 'number') {throw new Exception ('Invalid tile id!'); return false; }
            

        var 

        Tile = $('#layout-tile-'+_id), 
        TileData = Tile.data('source'), 
        slidePos = Tile.data('slidePos'); 
        
        if (TileData.population == 0) { throw new Exception ('Layout Slide data not found!'); return false; }

 

        slidePos = (typeof slidePos === 'number') ? slidePos : 0;
        
        _doAPICall({
                target_id: TileData.Slides[slidePos].id, 
                'do': 'deleteSlide', 
                data: { 'void': 0 }, 
                success: function (data) {
                    
                
                    renderTile(data.Tile);
                    
                    return false;
                },

                error: function (xhr) {
                    return false;
                }
        });
        
        return false;

     }
     
     
     
     function showImageTab() {
         $('#slide-image-picker-tab, #slide-html-input-tab').removeClass('tab-selected');
         $('#slide-image-picker-tab').addClass('tab-selected');
         
         $('#layout-slide-dialog-box #field-slide-type').prop('checked', true);
         
         $('#slide-html-input-wrapper').hide();
         $('#slide-image-picker-wrapper').fadeIn(200);
     }
     
     
     
     function showHTMLTab() {
         $('#slide-image-picker-tab, #slide-html-input-tab').removeClass('tab-selected');
         $('#slide-html-input-tab').addClass('tab-selected');
         
         $('#layout-slide-dialog-box #field-slide-type').prop('checked', false);
         
         $('#slide-image-picker-wrapper').hide();
         $('#slide-html-input-wrapper').fadeIn(200);
         
     }
     
   
     
     
     
     
 
     
     
     function renderTile (Tile) {
         

            var oldTile = _dialogBox.find('#layout-tile-'+Tile.id);
            var new_tile = $(Mustache.render(ajax.getTemplate('layout-tile'), Tile)); 

            new_tile.data('source', Tile);

            // render new tile

            if (oldTile.length > 0) oldTile.replaceWith(new_tile);
            else { 
                var _grid = _dialogBox.find('#layout-tile-grid-wrapper'); 
                _grid.append(new_tile).animate({scrollTop: _grid.get(0).scrollHeight}, 300);
            }
            
            
            new_tile
            .mouseenter(function () {
                $(this).find('.layout-tile-tool-box, .layout-tile-dashboard-wrapper').fadeIn(200);
            })
            .mouseleave(function () {
                $(this).find('.layout-tile-tool-box, .layout-tile-dashboard-wrapper').fadeOut(200);
            })
            .find('.drag-handle').css('cursor', openHandCursor);
                    
     }
     




    // method: [ renderTileDialog ]
    function renderTileDialog (_options) {
        
        
            var dialogBox = $(Mustache.render(ajax.getTemplate('layout-tile-dialog'), _options.payload));  

            // Show dialog box...
            $('body').append(dialogBox);
            dialogBox.show();
            
             

             $('#layout-tile-dialog-box > .cancel-button').unbind('click').click(function () {
                
                hideTip(true);

                dialogBox.fadeOut(200, function () {
                    $(this).remove();
                });
                _dialogBox.css('z-index', 10001);
                $(document).unbind('scroll'); 
 		$('body').css({'overflow':'visible'});	

             })
             .show();
             
             _dialogBox.css('z-index', 9000);
             
            dialogBox.find('#save-button').click(function () {

                    var data = Utility.getFormData('#edit-layout-tile-form')
                    dialogBox.fadeOut(200, function () {
                        $(this).remove();
                    });
                    
                    _dialogBox.css('z-index', 10001);
                    $(document).unbind('scroll'); 
                    $('body').css({'overflow':'visible'});
                    

                    _options.callBack (data);
                    return false;
            });

             return false;
        
    }
    
    
    
    
    





    // method: [ renderSlideDialog ]
    function renderSlideDialog (_options) {
        
        
            var dialogBox = $(Mustache.render(ajax.getTemplate('layout-slide-dialog'), _options));  

            // Show dialog box...
            $('body').append(dialogBox);
            dialogBox.show();
            
            _dialogBox.css('z-index', 9000);
            
             

             $('#layout-slide-dialog-box > .cancel-button').unbind('click').click(function () {
                
                hideTip(true);

                dialogBox.fadeOut(200, function () {
                    $(this).remove();
                });
                _dialogBox.css('z-index', 10001);
                $(document).unbind('scroll'); 
 		$('body').css({'overflow':'visible'});	

             })
             .show();
             

             return false;
        
    }
    
    
    
    
    


    // method: [ editLayout ]
    function editLayout () {
            
            // if (typeof id !== 'number') {throw new Exception ('Invalid layout id!'); return false; }
            
            // _layout_id = parseInt(id);
            _doAPICall({
                target_id: _layout_id, 
                'do': 'getLayout', 
                data: { 'void': 0 }, 
                success: function (data) {
                    var _meta = {
                        title: 'Edit Layout', 
                        buttonLabel: 'save layout'
                    };

                    renderDialog({meta: _meta, Layout: data.Layout});
                    return false;
                },

                error: function (xhr) {
                    return false;
                }
        });
        
        return false;
        
    }
    
    
    

    
    
    
        
        


        /*
         * 
         * [ saveSlideImage ]
         * 
         */




        function saveSlideImage (_options) {
            uploader_engaged = true;


                _doAPICall({
                    target_id: _active_slide_id, 
                    'do': 'saveSlideImage', 
                    data: _options,  
                    success: function(data) {
                        
                            uploader_engaged = false;

                            release(); //alert('Saved', data.message, true);
                            $('.slide-image-picker-add-button').css({
                                                                      'background': 'url(' + data.Slide.image.large + ')',
                                                                      'background-size': 'contain', 
                                                                      'background-repeat': 'no-repeat', 
                                                                      'background-position': 'center center'

                                                                  }); 


                    }, 
                    error: function (xhr) {


                    }

                });


                return false;	

        }
        
      
    
    

    
    
    
    
    //Return public methods
    return {
        newTile: editTile, 
        editTile: editTile, 
        deleteTile: deleteTile, 
        editLayout: editLayout, 
        saveSortable: saveSortable, 
        newSlide: newSlide, 
        editSlide: editSlide, 
        saveSlide: saveSlide, 
        deleteSlide: deleteSlide, 
        
        nextSlide: nextSlide, 
        prevSlide: prevSlide, 
        showImageTab: showImageTab, 
        showHTMLTab: showHTMLTab, 
        
        
        saveSlideImage: saveSlideImage, 
        _doAPICall: _doAPICall
        
    };
    
    
    
 }
 
 
 