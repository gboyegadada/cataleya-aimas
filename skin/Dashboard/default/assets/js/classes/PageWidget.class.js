




 
 // ------------------------------------------------------------------------------------ //



// Class: [ Pages ]
Cataleya.newPageWidget = function (_store_id, callBack) {
    
    
    
    var 
    env = {
        store_id: _store_id, 
        page_id: 0, 
        dialogBox: {}, 
        
        _frame_index: [], 
        frame_width: 0, 
        max_frame_rounds: 0, 

        activeFrame: 1, 
        autosave_timeout: false, 
        autosave_status_timeout: false, 
        changes_made: false,   
        document_saved: false,  
        autosave_delay: (1*60*1000), // Autosave every 1 min...  
        autosave_status_delay: 30000,  
        autosave_saved_at: 0,
        slider: false
    };




    


    loadData();


    // method: [ refresh ]
    function loadData (callBack) {
        
        

        // Load moustache templates
        ajax.loadTemplates([
            'page-dialog-box', 
            'page-tile', 
            'page-app-list', 
            'page-app-list-item', 
            'app-picker-widget', 
            'app-picker-widget-thumb'
        ], true);

        if (typeof callBack == 'function') callBack();
        return false;
        
        
    }
    
    
    
    
    
    
    // method: [ getPage ]
    function getPage (id) {
        for (var i = 0; i < _collection.length; i++) {
            if (_collection[i].id == id) return _collection[i];
        }
        
        return {};
    } 
    
    
    
    
    // method: [ getPages ]
    function getPages () {
        return _collection;
    } 
    
    
    


    // FUNCTIONS FOR SUB_PANE NAVIGATOR ....

    function nextPane () {  
        //var _frame = activeFrame+1;
        //return navigateToFrame (_frame);
        
        env.slider.nextSlide();
    }



    function prevPane () {

        //var _frame = activeFrame-1;
        //navigateToFrame (_frame);
        
        env.slider.prevSlide();
    }
    
    
    function seekPane (_label) {
        

        return env.slider.gotoSlide (parseInt(_label));

        return false;
    }




    function navigateToFrame (frame) {


        env.slider.gotoSlide(frame); 

        return false;

    }
    
    
    
    
    

    
    
    // method: [ removePage ]
    function removePage (_id) {
            
            hold ('Please wait...');
            
            
            


            ajax.doAPICall ({ 
                    api: 'pages', 
                    'do': 'deletePage',
                    target_id: _id, 
                    data: { 'page_id': env.page_id, store_id: _id }, 
                    dataType: 'json', 
                    success: function (data) {

                            
                            $('#page-'+data.Page.id).remove();
                            if ($('#pages-editable-list > li').length < 3) $('#pages-editable-list > li.editable-list-tile.last-child').show();


                            if (typeof callBack === 'function') callBack();

                            // Update cache and bring it back
                            loadData(release);


                            return false;

                            },

                    error: function (xhr) {

                        return false;
                    }
            });
            
            
            
            
        }
        
        
        
        
    
    
    // method: [ newPage ]
    function newPage () {
        editPage('new');
        
        return true;
    } 
    
    
    // method: [ editPage ]
    function editPage (_id, _callBack) {


            hold('Please wait...');
            
            var _do, _target_id, _mode;
            

            if (_id === 'new') {
                _do = 'newPage';
                _target_id = env.store_id;
                _mode = 'new';
            } else {
                _do = 'getPageInfo';
                _target_id = _id;
                _mode = 'edit';
            }
            

            ajax.doAPICall({
                api: 'pages', 
                'do': _do,
                'target_id': _target_id,  
                'data': { 'page_id': _id, 'store_id': env.store_id }, 
                'success': function (data) {
                    
                    release();
                    
                    env.page_id = data.Page.id;
                    

                    renderPageDialogBox(data, _mode, _callBack);    




                    }
            });

    }
    
    
    
    
    
    
    
    
    
    
    

    /*
     * 
     * [ FUNCTION ] renderPageDialogBox 
     * ____________________________________________________________________________
     * 
     * Self explanatory.
     * 
     */



    function renderPageDialogBox (data, _mode, _callBack) {

    
                    env.dialogBox = $(Mustache.render(
                                ajax.getTemplate('page-dialog-box'), 
                                data, 
                                { 
                                    'page-app-list': ajax.getTemplate('page-app-list'), 
                                    'page-app-list-item': ajax.getTemplate('page-app-list-item'), 
                                    'app-picker-widget': ajax.getTemplate('app-picker-widget'), 
                                    'app-picker-widget-thumb': ajax.getTemplate('app-picker-widget-thumb')
                                })
                                );
                     
                    env.document_saved = (data.Page.Autosave === false);

                    // Show dialog box...
                    $('body').append(env.dialogBox);

                    var 
                    
                    closePageDialogBox = function closePageDialogBox () {

                        if (env.document_saved) done (); 
                        else Cataleya.confirm({
                           title: 'Save Changes...', 
                           message: 'Do you want to save the changes you made in the document “' + data.Page.title + '”?', // Your changes will be lost if you don’t save them.
                           label_confirm: 'Save', 
                           label_cancel: "Don't Save", 
                           callBack: function (ok) {
                               if (ok) {
                                   
                                   savePage ();
                                   
                               } else {
                        
                                   clearAutosave ();
                                
                               }
                               
                               done ();
                               
                           }
                        });
                        

                    }; 
                            

                    // Init: save button...
                    env.saveButton = __el('#new-item-pane .editor-save-button');
                    env.tinyStatus = __el('.rtf-editor-status-bar span.dot');
                    
                    savePageBodyHandler = function savePageBodyHandler () {

                            self.savePage();
                            env.saveButton.fadeOut();
                            env.tinyStatus.fadeIn();
                    }, 
                         
                    done = function done () {
                        
                            env.changes_made = false;
                            env.document_saved = true;
                               
                            // pagesChecklist.destroy();
                            window.clearTimeout(env.autosave_timeout);
                            window.clearTimeout(env.autosave_status_timeout);

                            __el('#fullscreen-bg3')
                                    .removeEventListener('click', closePageDialogBox)
                                    .css({ display: 'none' });
                            
                            MyShortcuts.clear('close', closePageDialogBox);
                            MyShortcuts.clear('save', savePageBodyHandler);
                            
                            env.quill.destroy();

                            env.dialogBox.remove(); 
                    };
                    

                    __el('#fullscreen-bg3, #new-item-pane > .cancel-button')
                            .addEventListener('click', closePageDialogBox)
                            .css({ display: 'block' });
            
                    env.dialogBox.show();
                    
                    
                     env.quill = new Quill('.quill-editor > #editor', { 
                         modules: {
                             'toolbar': { container: '.quill-editor > #toolbar' } 
                         }
                     });
                     
                     
                    // Init: save button...
                    env.saveButton.addEventListener('click', savePageBodyHandler);

                     
                     // on change: title...
                     document.getElementById('page-title-field').addEventListener('change', function () {
                         env.changes_made = true;
                         env.document_saved = false;
                         env.saveButton.show('inline-block');
                     });
                     
                     //on change: body ...
                     env.quill.on('text-change', function () {
                         env.changes_made = true;
                         env.document_saved = false;
                         env.saveButton.show('inline-block');
                     });
                     
                     if (!env.document_saved) env.saveButton.show('inline-block');
                     
                     


                    MyShortcuts.register('close', closePageDialogBox);
                    MyShortcuts.register('save', savePageBodyHandler, {'disable_in_input':false});
                    
                    
                    env.autosave_timeout = window.setTimeout(doAutosave, env.autosave_delay);
                    
                    // Skip the autosave readouts.
                    //env.autosave_status_timeout = window.setTimeout(refreshAutosaveStatus, env.autosave_status_delay);



                    // Index the frames for the [ seekPanel() ] method.


                    env.slider = Cataleya.makeSlider(document.querySelector('.pageWidgetDialog'), 'slide');
                    var updateFrameTitle = function updateFrameTitle (e) {

                            $('#frame-headers li:nth-child(' + (env.slider.getLastPos()+1) + ')').fadeOut(300, function () {

                                $('#frame-headers li:nth-child(' + (env.slider.getPos()+1) + ')').fadeIn(300);

                            });


                            $('#bread-crumbs li.active-crumb').removeClass('active-crumb');
                            $('#bread-crumbs li:nth(' + (env.slider.getPos()) + ')').addClass('active-crumb');

                    };

                    env.slider
                            .on('seekFrame', updateFrameTitle)
                            .on('nextFrame', updateFrameTitle)
                            .on('previousFrame', updateFrameTitle);



                    var bread_crumbs = env.dialogBox[0].querySelectorAll('#bread-crumbs > a');

                    for (var i=0; i<bread_crumbs.length; i++) {
                        bread_crumbs[i].onClick = function () {
                            env.slider.gotoSlide(i);
                        };
                    }


                            
                    // INIT APPS CHECKLIST
                    env.appChecklist = initAppList();


                        
                    var closeAppWidget = function closeAppWidget () {
                        env.dialogBox.find('#app-picker-widget-wrapper').fadeOut(100);
                        MyShortcuts.clear('cancel', closeAppWidget);
                    };
                    
                    env.dialogBox.find('#app-picker-widget-wrapper .cancel-button').click(closeAppWidget);
                    env.dialogBox.find('#app-add-button').click(function (e) {
                        
                        e.stopPropagation();
                        e.preventDefault();

                        //self.renderAppPickerWidget();
                        
                        $('#app-picker-widget-wrapper').fadeIn(100);
                        MyShortcuts.register('cancel', closeAppWidget);
                    });


                    __el(env.dialogBox[0].querySelector('.uni-label input'))
                    .on('focus', uniLabelInputFocusHandler);
                   

                    

                    if (typeof _callBack === 'function') _callBack();

    }
    
    
    
    
    
    

    /*
     * 
     * [ FUNCTION ] savePage 
     * ____________________________________________________________________________
     * 
     * Self explanatory.
     * 
     */



    function savePage () {



            var 

            _target_id = env.page_id, 


            data = {
                page_id: env.page_id, 
                page_title: document.getElementById('page-title-field').value, 
                page_content: env.quill.getHTML()
            },
            _do = 'save', 
            _doHold = false;



            ajax.doAPICall({
                api: 'pages', 
                target_id: _target_id, 
                'do': _do, 
                data: data, 
                hold: _doHold, 
                success: function (data) {
                        
                        env.document_saved = true;
                        
                        setTimeout(function () { env.tinyStatus.fadeOut(); }, 2000);
                        return false;

                        },

                error: function (xhr) {
                        return false;
                }
            });

            return false;


    }
    
    
    
    function formatTime (ms) {
        
        ms = parseInt(ms);
        
        
        var 
        
        secs = Math.floor(ms/1000), 
        mins = Math.floor(ms/(60*1000)), 
        hrs = Math.floor(ms/(60*60*1000)), 
        days = Math.floor(ms/(24*60*60*1000)), 
        english = [];




        if (days >= 1) {
            english.push(days + ' day'+ (days>1 ? 's' : ''));
        }
        
        if (hrs >= 1) {
            english.push(hrs + ' hour' + (hrs>1 ? 's' : ''));
        }
        
        if (mins >= 1) {
            english.push(mins + ' min' + (mins>1 ? 's' : ''));
        }
        
        if (secs >= 1) {
            english.push(secs + ' sec' + (secs>1 ? 's' : ''));
        }
        
        
        return (english.length > 0) ? english : ['0 secs'];

        
    }
    
    
    
    
    function refreshAutosaveStatus () {
        
            window.clearTimeout(env.autosave_status_timeout);
            
            if (env.autosave_saved_at !== 0) {
                var elapsed =  Date.now() - env.autosave_saved_at;
                env.dialogBox[0].querySelector('.rtf-editor-status-bar > .text').innerHTML = 'Autosaved ' + formatTime(elapsed)[0] + ' ago..';
            }
        
            env.autosave_status_timeout = window.setTimeout(refreshAutosaveStatus, env.autosave_status_delay);
    }
    
    
    

    /**
     * 
     * @param {string} tinyStatusSelector
     * @returns {Boolean|undefined}
     * 
     */
    function doAutosave (tinyStatusSelector) {

            if (!env.changes_made) {
                
                window.clearTimeout(env.autosave_timeout);
                env.autosave_timeout = window.setTimeout(doAutosave, env.autosave_delay);
                return;
                
            }
            
            // For tiny status...
            if (typeof tinyStatusSelector === "string") { 
                $(tinyStatusSelector).html('Saving...').toggleClass('blink');
            }





            var 

            _target_id = env.page_id, 


            data = {
                page_id: env.page_id, 
                page_title: document.getElementById('page-title-field').value, 
                page_content: env.quill.getHTML()
            },
            _do = 'autosave', 
            _doHold = false;



            ajax.doAPICall({
                api: 'pages', 
                target_id: _target_id, 
                'do': _do, 
                data: data, 
                hold: _doHold,
                tinyStatus: tinyStatusSelector, 
                success: function (data) {
                    
                        window.clearTimeout(env.autosave_timeout);
                        env.autosave_timeout = window.setTimeout(doAutosave, env.autosave_delay);
                        
                        env.changes_made = false;
                        env.autosave_saved_at = Date.now();
                        
                        // refreshAutosaveStatus();
                        
                        return false;

                        },

                error: function (xhr) {
                        return false;
                }
            });

            return false;


    }
    
    
    
    
    
    
    
    
    /**
     * 
     * @param {string} tinyStatusSelector
     * @returns {Boolean}
     * 
     */
    function clearAutosave (tinyStatusSelector) {

            window.clearTimeout(env.autosave_timeout);
            
            // For tiny status...
            if (typeof tinyStatusSelector === "string") { 
                $(tinyStatusSelector).html('Saving...').toggleClass('blink');
            }





            var 

            _target_id = env.page_id, 


            data = {
                page_id: env.page_id
            },
            _do = 'clearAutosave', 
            _doHold = false;



            ajax.doAPICall({
                api: 'pages', 
                target_id: _target_id, 
                'do': _do, 
                data: data, 
                hold: _doHold,
                tinyStatus: tinyStatusSelector, 
                success: function (data) {
                        return false;

                        },

                error: function (xhr) {
                        return false;
                }
            });

            return false;


    }
    
    
    
    
    
    
    

    /*
     * 
     * [ FUNCTION ] uniLabelInputFocusHandler
     * ____________________________________________________________________________
     * 
     * Self explanatory.
     * 
     */


    function uniLabelInputFocusHandler (e)
    {
                e = e || window.event;
                var target = e.target || e.srcElement;
                
                //var This = $(target);
                var _uniLabelInputCache = target.value;
                
                
                var formatInput = function  (i) {
                    
                    // var This = $(i);
                    
                    switch (i.getAttribute('data-format')) {
                            case 'price':
                                i.value(numberFormat(i.value, 2));
                            break;
                            
                            case 'float':
                                i.value(parseFloat(i.value));
                            break;
                            
                            case 'integer':
                                i.value(parseInt(i.value));
                            break;
                            
                            case 'upper-case':
                                i.value(i.value.toUpperCase());
                            break;
                            
                            case 'lower-case':
                                i.value(i.value.toLowerCase());
                            break;
                            
                        }
                       
                };



                __el(target).on('change', doUniLabelInputSave);



                function cancelUniLabelInputSave () {
                    
                    __el(target).off('change', doUniLabelInputSave);

                    target.blur();
                    target.value = _uniLabelInputCache;


                    MyShortcuts.clear('return', doUniLabelInputSave);
                    MyShortcuts.clear('cancel', cancelUniLabelInputSave);
                }

                function doUniLabelInputSave () {
                    formatInput(target);

                    __el(target).off('change', doUniLabelInputSave);
                    target.blur();

                    self.save(target); 



                    MyShortcuts.clear('return', doUniLabelInputSave);
                    MyShortcuts.clear('cancel', cancelUniLabelInputSave);
                }


                MyShortcuts.register('return', doUniLabelInputSave, {'disable_in_input':false});
                MyShortcuts.register('cancel', cancelUniLabelInputSave, {'disable_in_input':false});

    }







    /**
     * 
     * initAppList
     * ____________________________________________________________________________
     * 
     * Self explanatory.
     * 
     */
    function initAppList ()
    {


            // INIT APPS CHECKLIST
            
            var 
            
                appsChecklistContainer = document.getElementById('apps-checklist-container'), 
                button_SelectAll = __el(env.dialogBox[0].querySelector('a.select-all-apps-button'));
            
            if (typeof env.appsChecklist !== 'undefined') { 
                env.appsChecklist.destroy();
                env.appsChecklist = null;
            }
            
            env.appsChecklist = Cataleya.makeCheckList(appsChecklistContainer);
            
            
            
            env.appsChecklist
            
            .on('onFirstCheck', function () {
                MyShortcuts.register('deleteItem', removeAppHandle);
                MyShortcuts.register('shift', shiftKeyHandler);
                
            })
            .on('onAllUnchecked', function () {
                button_SelectAll.removeClass('active');
                MyShortcuts.clear('deleteItem', removeAppHandle);
                MyShortcuts.clear('shift', shiftKeyHandler);
                
                button_SelectAll.item(0).querySelector('.button-label').innerHTML = 'Select All';
            })
            .on('onAllChecked', function () {
                button_SelectAll.addClass('active');
            })
            .on('onLastUnchecked', function () {
                button_SelectAll.removeClass('active');
            });
            


   
                

            var 
            deselectAllHandler = env.appsChecklist.deselectAll.bind(env.appsChecklist); 
            
            function removeAppHandle () {

                MyShortcuts.clear('deleteItem', removeAppHandle);
                pageWidget.removeApp(env.appsChecklist.getSelected(), deselectAllHandler);

            }; 
            
                

            function shiftKeyHandler (e) {
                if (e.shiftKey) {
                    // Keydown...

                    //button_SelectAll.querySelector('.icon-checkmark').addClass('active');
                    button_SelectAll.item(0).querySelector('.button-label').innerHTML = 'Deselect All';


                } else {
                    // Keyup...
                    button_SelectAll.item(0).querySelector('.button-label').innerHTML = 'Select All';
                }
            };
                
                
                
            button_SelectAll.addEventListener('click', env.appsChecklist.toggleAllSelections.bind(env.appsChecklist));
            env.dialogBox[0].querySelector('a.remove-selected-apps-button').addEventListener('click', removeAppHandle);



            // INIT: Task Buttons...
            __el(appsChecklistContainer.querySelectorAll('.checklist-tile')).forEach(function () {
                
                var This = this;
                
                This.querySelector('.select-app-button').addEventListener('click', function () {
                        env.appsChecklist.toggleSelection(This.getAttribute('data-tile-id'));
                });

                This.querySelector('.remove-app-button').addEventListener('click', function () {
                        pageWidget.removeApp(This.getAttribute('data-tile-id'));
                });


            });
            
            
            return env.appsChecklist;
    }


    


    /*
     * 
     * [ FUNCTION ] saveDescription 
     * ____________________________________________________________________________
     * 
     * Self explanatory.
     * 
     */



    function save (field) {

            field = $(field);

            var _sync_id = field.attr('data-sync-id');


            var 

            _target_id = 0, 


            data = {
                page_id: env.page_id, 
                text: field.val()
            },
            _do = 'void', 
            _doHold = true;


            // For tiny status...
            var tinyStatusSelector = field.attr('tinyStatus');
            if (typeof tinyStatusSelector === "string") { 
                $(tinyStatusSelector).html('Saving...').toggleClass('blink');
                
                _doHold = false;
            }


            // For uni-label status...
            var uniLabelStatus = field.attr('data-uni-label-status');
            if (typeof uniLabelStatus === "string") { 

                uniLabelStatus = $(uniLabelStatus);
                field.attr('uni-label-title', uniLabelStatus.text());

                uniLabelStatus.html('<span class="blink">Saving...</span>');
                field.hide();
                uniLabelStatus.show();
                
                _doHold = false;
            }






            switch (field.attr('name')) {
                case 'title':
                    _do = 'saveTitle';
                    _target_id = env.page_id;
                    break;
                    
                case 'body':
                    _do = 'saveBody';
                    _target_id = env.page_id;
                    break;
                    
                case 'page-template':
                    _do = 'saveTemplate';
                    _target_id = env.page_id;
                    data.template_id = field.val();
                    break;
                    
                case 'page-handle':
                    _do = 'savePageHandle';
                    _target_id = env.page_id;
                    data.page_handle = field.val();
                    break;
                    
                case 'draft':
                    _do = 'saveDraft';
                    _target_id = env.page_id;
                    break;
                    
                case 'auto-save':
                    _do = 'autosave';
                    _target_id = env.page_id;
                    break;
                    
                default:
                    _do = 'void';
                    break;
            }



            ajax.doAPICall({
                api: 'pages', 
                target_id: _target_id, 
                'do': _do, 
                data: data, 
                hold: _doHold,
                tinyStatusSelector: tinyStatusSelector, 
                uniLabel: { status: uniLabelStatus, field: field }, 
                success: function (data) {
                    

                        switch (_do) {
                            case 'saveTitle': 
                                env.dialogBox.find('#page-dialog-title .item-name').html(data.Page.title);
                            break;



                        }


                        return false;

                        },

                error: function (xhr) {
                        return false;
                }
            });

            return false;


    }



    
    
    

    function addApp (_id, cb) {

        if ($('#app-picker-widget-wrapper').is(':visible')) { 
                $('#app-picker-widget-wrapper').fadeOut(200);
        }

        _id = Array.isArray(_id) ? _id : [ _id ];

        ajax.doAPICall({
            api: 'pages', 
            target_id: env.page_id, 
            'do': 'addPlugins', 
            data: { 'page_id': env.page_id, 'plugin_handles': _id }, 
            success: function(data) {
                if (typeof cb === 'function') cb();

                var list = __el('#apps-checklist-container');
                data.AppList.forEach(function (v) {

                list.prependChild(Mustache.render(
                            ajax.getTemplate('page-app-list-item'), 
                            v));
                });

                initAppList();

            }, 
            error: function (data) {

            }

            });

        return false;

    }


    
    
    function removeApp (_id, cb) {


        hold ('Please wait...');

        _id = Array.isArray(_id) ? _id : [ _id ];

        ajax.doAPICall({
            api: 'pages', 
            target_id: env.page_id, 
            'do': 'removePlugins', 
            data: { 'page_id': env.page_id, 'plugin_handles': _id }, 
            success: function(data) {
                if (typeof cb === 'function') cb();

                var toRemove = [];
                data.AppList.forEach(function (v) {
                    toRemove.push('.app-tile#app-tile-' + v);
                });

                env.dialogBox.find(toRemove.join(', ')).remove();
            }, 
            error: function (data) {

            }

            });

        return false;

    }

    
    
    
    

    
    
    
    
    //Return public methods
    var self = {
        
        save: save, 
        savePage: savePage, 
        removePage: removePage,
        getPage: getPage, 
        getPages: getPages, 
        newPage: newPage, 
        editPage: editPage, 

        addApp: addApp, 
        removeApp: removeApp, 
        
        nextPane: nextPane, 
        prevPane: prevPane, 
        seekPane: seekPane
        
    };
    
    return self;
    
    
    
 };






