
 
 
// ----------------------------------------------------------------------------------------- //


// Class: [ loadChart ]
Cataleya.loadChart = function (callBack) {
    
    // Private properties
    
    var 
    
    _auth_token = $('#meta_token').val(), 
    _store_ids = [], 
    _product_ids = [], 
    _offset = 1, 
    _a = Cataleya.arrayFuncs(), 
    _graphData = {
        skip: [], 
        data: [], 
        container: {}
    }, 
    
    _query_temp = {
        auth_token: _auth_token,
        store_id: [], 
        product_id: [], 
        'do': 'void', 
        data: []

    }, 
    _options_temp = {
        offset: 1, 
        interval: 'month-3d', 
        from_now: false
    };
    
    
    // Fetch moustache templates
    
    ajax.loadTemplates(['stats']);
    
    



    /*
     * 
     *  Private: [ FUNCTION ] _doAPICall
     * ____________________________________________________________________________
     * 
     * 
     * 
     */



    function _doAPICall(_options) {

            hold ('Please wait...');

            var query = _query_temp;

            query['product_id'] =  (_product_ids.length == 0) ? [0] : _product_ids;
            query['store_id'] =  (_store_ids.length == 0) ? [0] : _store_ids;
            if (typeof _options['do'] !== 'undefined') query['do'] = _options['do'];
            if (typeof _options['data'] !== 'undefined') query['data'] = _options['data'];



            ajax.jsonRequest ({
            method: 'POST', 
            url: 'stats-api.ajax.php', 
            data: $.param(query), 
            dataType: 'json', 
            success: function(response) {

                        var data = response.json();
                        
                        if (typeof _options['success'] == 'function') _options['success'](data);
                        _product_ids = _store_ids = [];

                        return false;

                    }, 
            error: function (xhr) {
                    if (typeof _options['error'] == 'function') _options['error'](xhr);


                    _product_ids = _store_ids = [];
                    return false;
            }

            });

            return false;
    }


    
    
    
    function renderDialog (data) {

                    _graphData.data = data;
                    
                    $('.chart-dialog-box').detach(); // remove existing dialog boxes...

                    var dialogBox = $(Mustache.render(ajax.getTemplate('stats'), data.meta));  

                    // Show dialog box...
                    $('body').append(dialogBox);
                    dialogBox.show();

                    _graphData.container = dialogBox.find('.graph-container').get(0);
                    drawGraph(_graphData.container, data.payload, []);


                    $('#fullscreen-bg2, .chart-dialog-box > .cancel-button').unbind('click').click(function () {

                       dialogBox.fadeOut(200, function () {
                           dialogBox.detach();
                       });
                       $('#fullscreen-bg2').unbind('click').hide();
                       $(document).unbind('scroll'); 
                       $('body').css({'overflow':'visible'});	

                    })
                    .show();

    }
    
   

    
    
    function drawGraph (container, data, skip) {

                    
                    
                    var 
                        ticks = [], 
                        i = 0, 
                        i2 = 0, 
                        list = data[0]['data'], 
                        max = list.length-1, 
                        sub_dev = Math.floor(max/10);

                    do {
                      if (i2 == sub_dev) {
                          ticks.push([ i, list[i][0].replace(/\s/, '<br/>').replace(/^[0]/, '') ]);
                          i2 = 0;
                        } 
                      i++;
                      i2++;
                    } while (i <= max);

                    i = list.length-1;
                    ticks.push([ i, list[i][0].replace(/\s/, '<br/>').replace(/^[0]/, '') ]);
                    
                    
                    var payload = [];
                    i2 = 0;
                    for (var k in data) {
                        if (_a.has(data[k]['key'], skip)) continue;
                        
                        payload[i2] = { 
                            data: [], 
                            color: data[k]['color'],
                            clickable: false, 
                            hoverable: true
                        }
                        var new_index = 0;
                        var graph_data = data[k]['data'];
                        
                        for (var ii in graph_data) {
                            payload[i2]['data'].push([ new_index++, graph_data[ii][1] ]);
                        }
                        
                        i2++;
                    }
                    
                    
                    console.log(data[0]['data']);
                    
                    $(container).html('');

                    // Draw Graph
                    return Flotr.draw(container, payload, {
                        xaxis: {
                          //title: 'Days',           // => axis title
                          //titleAngle: 0,         // => axis title's angle, in degrees
                          //titleAlign: 'left',
                          ticks: ticks,           // => format: either [1, 3] or [[1, 'a'], 3]
                          minorTickFreq: 4, 
                          labelsAngle: 0, 
                          min: null,             // => min. value to show, null means set automatically
                          max: max,             // => max. value to show, null means set automatically
                          mode: 'normal',        // => can be 'time' or 'normal'
                          timeFormat: null,
                          timeMode:'UTC',        // => For UTC time ('local' for local time).
                          timeUnit:'day',// => Unit for time (millisecond, second, minute, hour, day, month, year)
                          scaling: 'linear'     // => Scaling, can be 'linear' or 'logarithmic'
                        },
                        yaxis: {
                          // title: 'Views',           // => axis title
                          // titleAngle: 90,         // => axis title's angle, in degrees
                          //titleAlign: 'left',
                          minorTickFreq: 1, 
                          tickDecimals: 0,    // => no. of decimals, null means auto
                          //labelsAngle: 30, 
                          min: null,             // => min. value to show, null means set automatically
                          max: null,             // => max. value to show, null means set automatically
                          mode: 'normal'        // => can be 'time' or 'normal'
                        },
                        grid: {
                          color: '#545454',      // => primary color used for outline and labels
                          backgroundColor: null,
                          tickColor: 'rgba(50,50,50,.6)', //'#DDDDDD',  // => color used for the ticks
                          labelMargin: 3,        // => margin in pixels
                          verticalLines: false,   // => whether to show gridlines in vertical direction
                          minorVerticalLines: null, // => whether to show gridlines for minor ticks in vertical dir.
                          horizontalLines: true, // => whether to show gridlines in horizontal direction
                          minorHorizontalLines: null // => whether to show gridlines for minor ticks in horizontal dir.
                        },
                        mouse: {
                          track: true,          // => true to track the mouse, no tracking otherwise
                          relative: true,       // => next to the mouse cursor
                          margin: 5,             // => margin in pixels of the valuebox
                          lineColor: '#FF3F19',  // => line color of points that are drawn when mouse comes near a value of a series
                          trackDecimals: 1,      // => decimals for the track values
                          sensibility: 4        // => the lower this number, the more precise you have to aim to show a value
                        }
                    });
                    



    }
    
    
    
    
    
    
    
    


    // method: [ showCatalogStats ]
    function showCatalogStats (id) {
        

            _product_ids = _store_ids = [];
            var _options = _options_temp;
            
            if (typeof id !== 'undefined') _store_ids.push(id);

            _doAPICall({
                'do': 'getCatalogStats', 
                data: _options, 
                success: renderDialog,

                error: function (xhr) {
                    return false;
                }
        }); 
        
        
    }
    
    
    
    
    


    // method: [ showProductStats ]
    function showProductStats (store_id, product_id) {
        

            _product_ids = []; _store_ids = [];
            var _options = _options_temp;
            
            if (typeof store_id !== 'undefined') _store_ids.push(store_id);
            if (typeof product_id !== 'undefined') _product_ids.push(product_id);
            
            console.log(_product_ids);

            _doAPICall({
                'do': 'getProductStats', 
                data: _options, 
                success: renderDialog,

                error: function (xhr) {
                    return false;
                }
        }); 
        
        
    }
    
    
    
    
    
    

    


    // method: [ showFailedLoginStats ]
    function showFailedLoginStats () {
        

            _product_ids = _store_ids = [];
            var _options = _options_temp;
            _options.from_now = true;

            _doAPICall({
                'do': 'getFailedLoginStats', 
                data: _options, 
                success: renderDialog,

                error: function (xhr) {
                    return false;
                }
        }); 
        
        
    }
    
    
    
    
    
    
    function toggleGraph(button, key) {
        if (_a.has(key, _graphData.skip)) {
            _graphData.skip = _a.remove(key, _graphData.skip);
            $(button).removeClass('icon-checkbox-partial').addClass('icon-checkbox-unchecked');
        }
        else {
            _graphData.skip.push(key);
            $(button).removeClass('icon-checkbox-unchecked').addClass('icon-checkbox-partial');
        }
        

        drawGraph(_graphData.container, _graphData.data.payload, _graphData.skip);
        
        return false;
    }





    
    
    
    
    //Return public methods
    return {
        toggleGraph: toggleGraph, 
        //toggleOrders: toggleOrders, 
        //toggleWishlists: toggleWishlists,
        showCatalogStats: showCatalogStats, 
        showProductStats: showProductStats, 
        showFailedLoginStats: showFailedLoginStats
        
    };
    
    
    
 }
 
 
 