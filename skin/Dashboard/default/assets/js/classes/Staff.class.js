
 
 
// ----------------------------------------------------------------------------------------- //


// Class: [ loadStaff ]
Cataleya.loadStaff = function (_store_id, callBack) {
    
    
    if (typeof _store_id == 'object' || typeof _store_id == 'undefined') {throw new Exception ('Invalid store id!'); return {}; }
    else _store_id = parseInt(_store_id);
    
    // Private properties
    
    var 
    
    _dialogBox, 
    _auth_token = $('#meta_token').val(), 
    _pickerData = {}, 
    _a = Cataleya.arrayFuncs(), 
    Utility = Cataleya.getUtility(), 
    
    _templates = {}, 
    
    _query_temp = {
        auth_token: _auth_token,
        target_id: _store_id, 
        'do': 'void', 
        data: []

    };
    

    
    
    // Load moustache templates
    ajax.loadTemplates(['generic-picker']);
    
    _templates.StaffMemberTile = '<li class="editable-list-tile trashable" id="staff-member-{{id}}" trash_type="staff-member-tile"  data_admin_id="{{id}}" data_name="{{name}}" data_fname="{{firstname}}" data_lname="{{lastname}}"  >' 
                                + '<span class="tiny user-dp fltlft" ><img src="{{dp.thumb}}"></span>&nbsp;&nbsp;{{name}} ({{role.name}})'
                               + '<a class="button-1 editable-list-delete-button fltrt" href="javascript:void(0)" onclick="Staff.removeStaffMember({{id}});" >remove</a></li>'


    // Load picker data
    getPickerData ();
    


    /*
     * 
     *  Private: [ FUNCTION ] _doAPICall
     * ____________________________________________________________________________
     * 
     * 
     * 
     */



    function _doAPICall(_options, _stealth) {

            if (typeof _stealth === 'boolean' && _stealth === true) {} 
            else hold ('Please wait...');

            var query = _query_temp;

            if (typeof _options['target_id'] !== 'undefined') query['target_id'] = _options['target_id'];
            if (typeof _options['do'] !== 'undefined') query['do'] = _options['do'];
            if (typeof _options['data'] !== 'undefined') query['data'] = _options['data'];



            ajax.jsonRequest ({
            method: 'POST', 
            url: 'staff.api.ajax.php', 
            data: query, 
            dataType: 'json', 
            success: function(response) {

                        var data = response.json();
                        
                        if (typeof _options['success'] == 'function') _options['success'](data);

                        return false;

                    }, 
            error: function (xhr) {
                    if (typeof _options['error'] == 'function') _options['error'](xhr);

                    return false;
            }

            });

            return false;
    }


    
    
    
    function renderDialog () {


                    $('.picker-dialog-box').remove(); // remove existing dialog boxes...
                    
                    var payload = {
                        title: 'Add New Staff Member', 
                        description: 'Choose something', 
                        entityName: 'staff member', 
                        options: _pickerData, 
                        buttonLabel: 'Add New Staff Member'
                    };



                    _dialogBox = $(Mustache.render(ajax.getTemplate('generic-picker'), payload)); 
                    

                    // Show dialog box...
                    $('body').append(_dialogBox);
                    _dialogBox.show();

                    _dialogBox.find('#select-target-id').unbind('focus').focus (function () {hideTip(true); });


                     $('#fullscreen-bg2, .picker-dialog-box > .cancel-button').unbind('click').click(function () {

                        hideTip(true);

                        _dialogBox.fadeOut(200, function () {
                            $('.picker-dialog-box').remove();
                        });
                        $('#fullscreen-bg2').unbind('click').hide();
                        $(document).unbind('scroll'); 
                        $('body').css({'overflow':'visible'});	

                     })
                     .show();

                    $('#add-to-button').click(function () {

                            var _target_id = $('#select-target-id').val();
                            if (_target_id == -1) { 
                                showTip ($('#select-target-id'), 'Please choose a ' + _options.payload.entityName + '.');
                                return false;
                            }

                            hideTip(true);

                            _dialogBox.fadeOut(200, function () {
                                $('.picker-dialog-box').remove();
                            });
                            $('#fullscreen-bg2').unbind('click').hide();
                            $(document).unbind('scroll'); 
                            $('body').css({'overflow':'visible'});


                            addStaffMember (_target_id);
                            return false;
                    });

                     return false;


    }
    
   

    
    


    // method: [ getPickerData ]
    function getPickerData () {
        

            var data = {
                admin_id: 0
            };
            
            _doAPICall({
                target_id: _store_id, 
                'do': 'getInfo', 
                data: data, 
                success: function (data) {

                        _pickerData = data.payload;
                        return false;
                    
                },

                error: function (xhr) {
                    return false;
                }
            }, true); 
        

            
            return false;
        
    }
    
    
    
    
    
    
    
    


    // method: [ addStaffMember ]
    function addStaffMember (_id) {
        

            var data = {
                admin_id: parseInt(_id)
            };
            
            _doAPICall({
                target_id: _store_id, 
                'do': 'addStaffMember', 
                data: data, 
                success: function (data) {

                        _pickerData = data.payload;
                        
                        // Then add new tile...
                        var new_tile = $(Mustache.render(_templates.StaffMemberTile, data.StaffMember));


                         new_tile.mouseenter(function () {
                                      $(this).find('.button-1').fadeIn('fast');
                                  })
                                  .mouseleave(function () {
                                      $(this).find('.button-1').hide();
                                  });
                                  
                        $('#staff-members-editable-list > li').first().after(new_tile); 
                        $('#staff-members-editable-list > li.editable-list-tile.last-child').hide();
                    
                },

                error: function (xhr) {
                    return false;
                }
            }); 
        

            
            return false;
        
    }
    
    
    
 

    
    
    

    // method: [ removeStaffMember ]
    function removeStaffMember (id) {
            
            if (typeof id !== 'number') {throw new Exception ('Invalid staff member id!'); return false; }
            
            _doAPICall({
                target_id: _store_id, 
                'do': 'removeStaffMember', 
                data: { admin_id: parseInt(id) }, 
                success: function (data) {
                    
                    _pickerData = data.payload;
                    $('#staff-members-editable-list > li#staff-member-'+data.StaffMember.id).remove();
                    
                    if ($('#staff-members-editable-list > li').length < 3) $('#staff-members-editable-list > li.editable-list-tile.last-child').show();
                    
                    return false;
                },

                error: function (xhr) {
                    return false;
                }
        });
        
        return false;
        
    }
    
    
    
    
    
    
    
  

    
    
    
    
    //Return public methods
    return {
        addStaffMember: addStaffMember, 
        removeStaffMember: removeStaffMember, 
        
        showDialog: renderDialog, 
        _doAPICall: _doAPICall
        
    };
    
    
    
 }
 
 
 