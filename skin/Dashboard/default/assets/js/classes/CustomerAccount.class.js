
 
 
// ----------------------------------------------------------------------------------------- //

//var MyAccount;
//
//$(document).ready(function () {
//    
//    if ($('#meta_token').length > 0) MyAccount = Cataleya.loadMyAccount();
// 
//});

 
// Class: [ loadMyAccount ]
Cataleya.loadCustomerAccount = function (callBack) {
    
    // Private properties
    
    var 
    
    _dialogBox, 
    _auth_token = $('#meta_token').val(), 
    _api = 'customer-account', 
    Validator = Cataleya.newValidator(), 
    _a = Cataleya.arrayFuncs(), 
    Utility = Cataleya.getUtility(), 
    
    _query_temp = {
        auth_token: _auth_token,
        'do': 'void', 
        data: []

    };
    

    
    
    // Load moustache templates
    ajax.loadTemplates(['customer-account-dialog'], true);




    
    
    
    function renderDialog (data) {


                    $('#customer-profile-dialog-box').detach(); // remove existing dialog boxes...

                    _dialogBox = $(Mustache.render(ajax.getTemplate('customer-account-dialog'), data));  

                    // Show dialog box...
                    $('body').append(_dialogBox);
                    
                    _dialogBox.show();

                    // Password meter
                    Validator.newPasswordMeter('text-field-customer-pass1', 'customer-password-meter-1');

                    $('#fullscreen-bg2, #customer-profile-dialog-box .cancel-button').unbind('click').click(function () {

                       _dialogBox.fadeOut(200, function () {
                           _dialogBox.detach();
                       });
                       $('#fullscreen-bg2').unbind('click').hide();
                       $(document).unbind('scroll'); 
                       $('body').css({'overflow':'visible'});	

                    })
                    .show();
                    

    }
    
   

    
   
    


    // method: [ editProfile ]
    function editProfile (_id) {
            

            ajax.doAPICall({
                api: _api, 
                'do': 'getInfo', 
                target_id: _id, 
                data: { 'void': 0 }, 
                success: function (data) {
                    var _meta = {
                        title: 'Edit Customer Account: ' + data.Profile.name, 
                        saveButtonLabel: 'save changes'
                    };

                    renderDialog({meta: _meta, Customer: data.Profile});
                    return false;
                },

                error: function (xhr) {
                    return false;
                }
        });
        
        return false;
        
    }
    
    
    
    
    // method: [ saveProfile ]
    function saveProfile (_id) {
        
        
            // Validate form
            var failedItems = Validator.validateForm('customer-profile-form');
            
            if (failedItems.length > 0) {
                
                for (var i = 0; i < failedItems.length; i++) {
                    if (failedItems[i].type == 'password' && $('#customer-profile-form #change-customer-password').prop('checked') == false) continue;
                    
                    showTip(failedItems[i]);
                    $(failedItems[i])
                    .focus()
                    .blur(function () { hideTip(); });
                    return false;
                    break;
                }

            }

            _dialogBox.hide();
            $('#fullscreen-bg2').hide();
            
            hold('Saving profile ...');
            
            var data = Utility.getFormData($('#customer-profile-form'));

            
            ajax.doAPICall({
                api: _api, 
                target_id: _id, 
                'do': 'saveProfile', 
                data: data, 
                success: function (data) {
                    
                    if (data.failedValidation) {
                        
                        // Failed validation
                        // Show dialog box...
                        release();
                        $('#fullscreen-bg2').show(); 
                        _dialogBox.show();

                          if (data.failedItems.length > 0) {
                              var field = _dialogBox.find('input[name=' + data.failedItems[0].replace(/_/g, '-') + ']') ;
                              if (field.length == 0) field = _dialogBox.find('select[name=' + data.failedItems[0].replace(/_/g, '-') + ']') ;

                              if (field.length > 0) {
                                      showTip(field);
                                      $(field)
                                      .focus()
                                      .blur(function () { hideTip(); });

                              }
                          }
                          
                        return false; 
                    }
                    
                    _dialogBox.detach();
                    
                    
                },

                error: function (xhr) {
                    _dialogBox.show();
                    $('#fullscreen-bg2').show();
                    return false;
                }
        }); 
        
        
    }
    
    
    
    
    function togglePasswordTool (checkbox) {
        var box = $('#customer-profile-dialog-box #password-tool-wrapper');
        
        if ($(checkbox).prop('checked') == true) box.slideDown(200);
        else box.slideUp(200);
    }
 
    
    
    
        
        

    
    
    
    //Return public methods
    return {
        editProfile: editProfile, 
        saveProfile: saveProfile,  
        
        togglePasswordTool: togglePasswordTool
        
    };
    
    
    
 }
 
