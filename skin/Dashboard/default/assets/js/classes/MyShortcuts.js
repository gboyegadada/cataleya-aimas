
// ----------------------------------------------------------------------------------------------- ///


/*
 * 
 * [ MyShortcuts ]
 * ___________________________________________________________________
 * 
 * Keyboard Shortcuts
 * 
 * 
 */


Cataleya.MyShortcuts = function () {
    
    var default_options = {
                            'type':'keydown',
                            'propagate':false,
                            'disable_in_input':true,
                            'target':document,
                            'keycode':false
                    };
    
    var ShortCuts = {
        'deleteItem': { 
            que: [], 
            parallel: [], 
            shortcuts: ['delete', 'backspace']
        }, 
        'newItem': { 
            que: [], 
            parallel: [], 
            shortcuts: ['ctrl+n', 'meta+n']
        }, 
        'copy': { 
            que: [], 
            parallel: [], 
            shortcuts: ['ctrl+c', 'meta+c']
        }, 
        'cut': { 
            que: [], 
            parallel: [], 
            shortcuts: ['ctrl+x', 'meta+x']
        }, 
        'paste': { 
            que: [], 
            parallel: [], 
            shortcuts: ['ctrl+v', 'meta+v']
        }, 
        'undo': { 
            que: [], 
            parallel: [], 
            shortcuts: ['ctrl+z', 'meta+z']
        }, 
        'save': { 
            que: [], 
            parallel: [], 
            shortcuts: ['ctrl+s', 'meta+s']
        }, 
        'open': { 
            que: [], 
            parallel: [], 
            shortcuts: ['ctrl+o', 'meta+o']
        }, 
        'close': { 
            que: [], 
            parallel: [], 
            shortcuts: ['ctrl+w', 'meta+w']
        }, 
        'scrollToTop': { 
            que: [], 
            parallel: [],  
            shortcuts: ['t']
        }, 
        'scrollToBottom': { 
            que: [], 
            parallel: [], 
            shortcuts: ['b']
        }, 
        'cancel': { 
            que: [], 
            parallel: [], 
            shortcuts: ['esc']
        }, 
        'return': { 
            que: [], 
            parallel: [], 
            shortcuts: ['return']
        },
        shift: { 
            que: [], 
            parallel: [], 
            shortcuts: ['shift']
        },
        ctrl : { 
            que: [], 
            parallel: [], 
            shortcuts: ['ctrl']
        },
        alt  : { 
            que: [], 
            parallel: [], 
            shortcuts: ['alt']
        },
        meta : { 
            que: [],
            parallel: [],  
            shortcuts: ['meta']
        }	//Meta is Mac specific
    };
    
    
    

    
    return {
        register: function (_shortcut, _hook, _options) {
            
            _options = _options || {};
            
            for(var dfo in default_options) {
                    if(typeof _options[dfo] === 'undefined') _options[dfo] = default_options[dfo];
            }
            
            
            
            if (
                !(_shortcut in ShortCuts) 
                || typeof _hook !== 'function') { 
                    throw new TypeError ('Failed to register shortcut: Invalid arguments..');
                    return;
                }
                
            /*    
            if (_options['que'] === false) {
                
                // Add new function to que...
                ShortCuts[_shortcut]['parallel'].push({hook:_hook, options: _options});
                
                ShortCuts[_shortcut]['shortcuts'].forEach(function (v, k) {

                       shortcut.add(v, _hook, _options);

                });
                console.log(_shortcut  + ' (' + _hook.name + ') - added to parallel');
                
                return;
            }
            */  
                
            
            for (var k in ShortCuts[_shortcut]['que']) {
                if (ShortCuts[_shortcut]['que'][k].hook.name === _hook.name) {
                    console.log(_shortcut  + ' (' + _hook.name + ') - already added');
                    return;
                }
            }
            

            var l =  ShortCuts[_shortcut]['que'].length;
            
            // Reset keyboard shrotcut (replace old function with new)...
            if (l > 0) {
                ShortCuts[_shortcut]['shortcuts'].forEach(function (v, k) {
                    
                    shortcut.remove(v);
                
                });
                
                console.log(_shortcut  + ' (' + ShortCuts[_shortcut]['que'][l-1].hook.name + ') - benched');
            }
            
            
            ShortCuts[_shortcut]['shortcuts'].forEach(function (v, k) {

                   shortcut.add(v, _hook, _options);
                   
            });
            
            
            // Add new function to que...
            ShortCuts[_shortcut]['que'].push({hook:_hook, options: _options});
            console.log(_shortcut  + ' (' + _hook.name + ') - added');
            
            
        }, 
        clear: function (_shortcut, _hook) {  
            
            
            if (!(_shortcut in ShortCuts) ) { 
                    throw new TypeError ('Failed to clear shortcut: Invalid arguments..');
                    return;
                }
                
                
            
            if (typeof _hook === 'function') { 
                
                    // If a function / method is specified (i.e. _hook is a valid function)
                    // Remove only that function and keep the rest
                    for (var k in ShortCuts[_shortcut]['que']) {
                        if (ShortCuts[_shortcut]['que'][k].hook.name === _hook.name) {
                            ShortCuts[_shortcut]['que'].splice(k, 1);
                            console.log(_shortcut + ' (' + _hook.name + ') - removed');
                            break;
                        }
                    }
                    
                    
                } else {
                    // If no function / method is specified we clear the que
                    ShortCuts[_shortcut].que = Array ();
                    console.log(_shortcut + ' ( all ) - removed');
                }
                
            // Clear short combinations...
            ShortCuts[_shortcut]['shortcuts'].forEach(function (v) { shortcut.remove(v); });
                
            
            // Check if there are any players on the bench...
            var l =  ShortCuts[_shortcut]['que'].length;   
            if (l >= 1) {
                // Get player replacement...
                var _player = ShortCuts[_shortcut]['que'][l-1];
                
                
                // Add key combinations...
                ShortCuts[_shortcut]['shortcuts'].forEach(function (v) { 
                    shortcut.add(v, function (e) { _player.hook(), _player.options });  
                });
                
                console.log(_shortcut + ' (' + _player.hook.name + ') - replacement');
                
            }
                
            
            
        }

        
        
    };

    
};



$(document).ready(function () {
    window.MyShortcuts = Cataleya.MyShortcuts ();
    

});


