// Avoid `console` errors in browsers that lack a console.
(function() {
    var method;
    var noop = function () {};
    var methods = [
        'assert', 'clear', 'count', 'debug', 'dir', 'dirxml', 'error',
        'exception', 'group', 'groupCollapsed', 'groupEnd', 'info', 'log',
        'markTimeline', 'profile', 'profileEnd', 'table', 'time', 'timeEnd',
        'timeStamp', 'trace', 'warn'
    ];
    var length = methods.length;
    var console = (window.console = window.console || {});

    while (length--) {
        method = methods[length];

        // Only stub undefined methods.
        if (!console[method]) {
            console[method] = noop;
        }
    }
}());



// ---------------------- POLYFILLS ------------------------ //


if (!Array.isArray) {
  Array.isArray = function(arg) {
    return Object.prototype.toString.call(arg) === '[object Array]';
  };
}


// Production steps of ECMA-262, Edition 5, 15.4.4.18
// Reference: http://es5.github.io/#x15.4.4.18
if (!Array.prototype.forEach) {

  Array.prototype.forEach = function(callback, thisArg) {

    var T, k;

    if (this === null) {
      throw new TypeError(' this is null or not defined');
    }

    // 1. Let O be the result of calling ToObject passing the |this| value as the argument.
    var O = Object(this);

    // 2. Let lenValue be the result of calling the Get internal method of O with the argument "length".
    // 3. Let len be ToUint32(lenValue).
    var len = O.length >>> 0;

    // 4. If IsCallable(callback) is false, throw a TypeError exception.
    // See: http://es5.github.com/#x9.11
    if (typeof callback !== "function") {
      throw new TypeError(callback + ' is not a function');
    }

    // 5. If thisArg was supplied, let T be thisArg; else let T be undefined.
    if (arguments.length > 1) {
      T = thisArg;
    }

    // 6. Let k be 0
    k = 0;

    // 7. Repeat, while k < len
    while (k < len) {

      var kValue;

      // a. Let Pk be ToString(k).
      //   This is implicit for LHS operands of the in operator
      // b. Let kPresent be the result of calling the HasProperty internal method of O with argument Pk.
      //   This step can be combined with c
      // c. If kPresent is true, then
      if (k in O) {

        // i. Let kValue be the result of calling the Get internal method of O with argument Pk.
        kValue = O[k];

        // ii. Call the Call internal method of callback with T as the this value and
        // argument list containing kValue, k, and O.
        callback.call(T, kValue, k, O);
      }
      // d. Increase k by 1.
      k++;
    }
    // 8. return undefined
  };
}



/* --------------- addEventListener -------------*/

if (!Element.prototype.addEventListener) {
  var oListeners = {};
  function runListeners(oEvent) {
    if (!oEvent) { oEvent = window.event; }
    for (var iLstId = 0, iElId = 0, oEvtListeners = oListeners[oEvent.type]; iElId < oEvtListeners.aEls.length; iElId++) {
      if (oEvtListeners.aEls[iElId] === this) {
        for (iLstId; iLstId < oEvtListeners.aEvts[iElId].length; iLstId++) { oEvtListeners.aEvts[iElId][iLstId].call(this, oEvent); }
        break;
      }
    }
  }
  Element.prototype.addEventListener = function (sEventType, fListener /*, useCapture (will be ignored!) */) {
    if (oListeners.hasOwnProperty(sEventType)) {
      var oEvtListeners = oListeners[sEventType];
      for (var nElIdx = -1, iElId = 0; iElId < oEvtListeners.aEls.length; iElId++) {
        if (oEvtListeners.aEls[iElId] === this) { nElIdx = iElId; break; }
      }
      if (nElIdx === -1) {
        oEvtListeners.aEls.push(this);
        oEvtListeners.aEvts.push([fListener]);
        this["on" + sEventType] = runListeners;
      } else {
        var aElListeners = oEvtListeners.aEvts[nElIdx];
        if (this["on" + sEventType] !== runListeners) {
          aElListeners.splice(0);
          this["on" + sEventType] = runListeners;
        }
        for (var iLstId = 0; iLstId < aElListeners.length; iLstId++) {
          if (aElListeners[iLstId] === fListener) { return; }
        }     
        aElListeners.push(fListener);
      }
    } else {
      oListeners[sEventType] = { aEls: [this], aEvts: [ [fListener] ] };
      this["on" + sEventType] = runListeners;
    }
  };
  Element.prototype.removeEventListener = function (sEventType, fListener /*, useCapture (will be ignored!) */) {
    if (!oListeners.hasOwnProperty(sEventType)) { return; }
    var oEvtListeners = oListeners[sEventType];
    for (var nElIdx = -1, iElId = 0; iElId < oEvtListeners.aEls.length; iElId++) {
      if (oEvtListeners.aEls[iElId] === this) { nElIdx = iElId; break; }
    }
    if (nElIdx === -1) { return; }
    for (var iLstId = 0, aElListeners = oEvtListeners.aEvts[nElIdx]; iLstId < aElListeners.length; iLstId++) {
      if (aElListeners[iLstId] === fListener) { aElListeners.splice(iLstId, 1); }
    }
  };
}





// Place any jQuery/helper plugins in here.




var Utils = (function (m) {   
    
    var 
    
    
    _private = m._private = m._private || { modules: [], locks: 1, ready_que: [], domReady: false },  
    _seal = m._seal = m._seal || function () {
        delete m._private;
        delete m._seal;
        delete m._unseal;
    },  
    _unseal = m._unseal = m._unseal || function () {
        m._private = _private;
        m._seal = _seal;
        m._unseal = _unseal;
    };


    var DOMContentLoaded = function() {
            if (_private.domReady) return;
            
            _private.domReady = true;
            console.log('DOM ready.');
            _private._doReady();
                    
            if ( document.addEventListener ) {
                    document.removeEventListener( "DOMContentLoaded", DOMContentLoaded, false );
            } else if ( document.readyState === "complete" ) {
                    // we're here because readyState === "complete" in oldIE
                    // which is good enough for us to call the dom ready!
                    document.detachEvent( "onreadystatechange", DOMContentLoaded );
            }
        };
            
    if ( document.readyState === "complete" ) {
            // Handle it asynchronously to allow scripts the opportunity to delay ready
            setTimeout( DOMContentLoaded, 1 );

    // Standards-based browsers support DOMContentLoaded
    } else if ( document.addEventListener ) {
            // Use the handy event callback
            document.addEventListener( "DOMContentLoaded", DOMContentLoaded, false );

            // A fallback to window.onload, that will always work
            window.addEventListener( "load", DOMContentLoaded, false );

    // If IE event model is used
    } else {
			// Ensure firing before onload, maybe late but safe also for iframes
			document.attachEvent( "onreadystatechange", DOMContentLoaded );

			// A fallback to window.onload, that will always work
			window.attachEvent( "onload", DOMContentLoaded );
    }
    
    
    
    
    
    _private._doReady = function () {
        
        _private.locks < 1 || --_private.locks;
        
        if (_private.locks > 0 || !_private.domReady) { console.log('-- ', _private.locks); return; }
        
        _private.ready_que.forEach(function (cb, i) {
           if (typeof cb === 'function') cb(m); 
        });
        
        _private.ready_que = [];
        _seal();
        
        console.log('Ready.');
    };
    
    
    
    _seal();

    
    m.ready = function (fn) {
        if (typeof fn === 'function') {
            
            if ( _private.domReady &&  _private.locks === 0) { fn(m); }
            else {
                _private.ready_que.push(fn);
            }
            //
        }
        
        return m;
    };
    
    
    
    // The ready event handler and self cleanup method
    /*
    }
    */
    
    
    
    function load(filename){
        
        m._unseal || _unseal();
        
        _private.locks++;
        
        console.log('++ ', _private.locks);
        
        
        var fileref=document.createElement('script');
        fileref.setAttribute("type","text/javascript");
        fileref.setAttribute("src", filename);
        
        
        if (typeof fileref !== "undefined") document.getElementsByTagName("head")[0].appendChild(fileref);
    }
    
    
    
    

    m.use = function () {

        if (arguments.length < 1) return;
        
        
        var 

        // self = this, 
        _options = {
          base: 'utils'  
        }, 
        _end_point = 'modules.js.php?', 
        _modules, 
        _q = [], 
        a = [];
        
        
        // 1: Digest arguments...
        if (typeof arguments[arguments.length] === 'object') {
            _options = m.extend(_options, a.slice.call(arguments, -1));
            _modules = a.slice.call(arguments, 0, -1);
        } else {
            _modules = a.slice.call(arguments);
        }
        
        
        // 2: Filter out loaded modules...
        var i = _modules.length;
        if (i < 1) return;
        
        while (i--) {
            if (_private.modules.indexOf(_modules[i]) === -1) { 
                _q.push(_modules[i]);
                _private.modules.push(_modules[i]); 
            }
        }
        
        // 3: Load...
        if (_q.length > 0) load(_end_point + 'q=' + _q.join('+') + '&b=' + _options.base);
        
        
        return m;
        
    };
    
    
    
    
    
    
    
    
        
    m.extend = function (target, source) {
        target = target || {};

        for (var prop in source) {
            if (typeof prop === 'object') {
                target[prop] = this.extend(target[prop], source[prop]);
            } else {
                target[prop] = source[prop];
            }
        }

        return target;
    };
    
    
    




    m.param = function (a) {

    var prefix,
            s = [],
            add = function( k, v ) {
                    // If value is a function, invoke it and return its value
                    v = ( typeof v === 'function' ) ? v() : ( v === null ? "" : v );
                    s[ s.length ] = encodeURIComponent( k ) + "=" + encodeURIComponent( v );
            };


    // If an array was passed in, assume that it is an array of form elements.
    if ( Array.isArray( a ) ) {
            // Serialize the form elements
            a.forEach(function(f) {
                    add( f.name, f.value );
            });

    } else {
            // otherwise encode params recursively.
            for ( prefix in a ) {
                    buildParams( prefix, a[ prefix ], add );
            }
    }


    // Return the resulting serialization
    return s.join( "&" ).replace( /%20/g, "+" );   

    };
    
    
    


    function buildParams( prefix, obj, add ) {
            var name;

            if ( Array.isArray( obj ) ) {
                    // Serialize array item.
                    obj.forEach(function( v, i ) {
                            if ( /\[\]$/.test( prefix ) ) {
                                    // Treat each array item as a scalar.
                                    add( prefix, v );

                            } else {
                                    // If array item is non-scalar (array or object), encode its
                                    // numeric index to resolve deserialization ambiguity issues.
                                    buildParams( prefix + "[" + ( typeof v === "object" ? i : "" ) + "]", v, add );
                            }
                    });

            } else if ( typeof obj === "object" ) {
                    // Serialize object item.
                    for ( name in obj ) {
                            buildParams( prefix + "[" + name + "]", obj[ name ], add );
                    }

            } else {
                    // Serialize scalar item.
                    add( prefix, obj );
            }
    }



    
    return m;
    
}(Utils || {}));



