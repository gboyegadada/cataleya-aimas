var NotificationProfiles, Validator;


$('document').ready(function () {
    

        NotificationProfiles = Cataleya.loadNotificationProfiles();
        Validator = Cataleya.newValidator();
        

        $('#notification-profile-form').submit(function () {
            NotificationProfiles.submitForm();
            return false;
        });



});


window.quickSearchCallBack = function (data) 
{
    
	$('#quick-search-results').html('');
        
        if (data.count > 0) {
                for (zone_id in data.zones) {
                    
                      // Countries in zone
                      var countries_info = countInEnglish(data.zones[zone_id].countryPopulation, 'Country', 'Countries');
                      
                      // Provinces in zone
                      var provinces_info = countInEnglish(data.zones[zone_id].provincePopulation, 'Province', 'Provinces');
                      
                      
                        var details = '<small class="quick-search-result-detail">' + countries_info + '</small>';
                        details += '<small class="quick-search-result-detail">' + provinces_info + '</small>';
                        $('#quick-search-results').prepend('<a href="' + data.zones[zone_id].url + '"><li>' + data.zones[zone_id].name + '<br />' + details + '</li></a>');
                }

        $('#quick-search-results').show();

        } else {$('#quick-search-results').hide();	}
}








