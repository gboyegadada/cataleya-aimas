<?php


/**
 * Cataleya
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@cataleya.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Cataleya to newer
 * versions in the future. If you wish to customize Cataleya for your
 * needs please refer to http://www.cataleya.com for more information.
 *
 * @category   Cataleya
 * @package    Cataleya
 * @copyright  Copyright (c) 2009 FPP Labs Inc. DBA Varien (http://www.fpplabs.com)
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

if (version_compare(phpversion(), '5.3.0', '<') === true) {
    echo  '<div style="font:12px/1.35em arial, helvetica, sans-serif;">
<div style="margin:0 0 25px 0; border-bottom:1px solid #ccc;">
<h3 style="margin:0; font-size:1.7em; font-weight:normal; text-transform:none; text-align:left; color:#2f2f2f;">
Whoops, it looks like you have an invalid PHP version.</h3></div><p>Cataleya supports PHP 5.3.0 or newer.
<a href="http://www.cataleya.com/install" target="">Find out</a> how to install</a>
 Cataleya using PHP-CGI as a work-around.</p></div>';
    exit;
}




/**
 * Error reporting
 */
// error_reporting(E_ALL | E_STRICT);





if(
    isset($_SERVER['HTTP_X_REQUESTED_WITH']) && 
    strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest'
    ) define('IS_AJAX', TRUE);
else define('IS_AJAX', FALSE);




define ('ROOT_PATH', getcwd() . '/');

#ini_set('display_errors', 1);

umask(0);
 
define('SSL_REQUIRED', false);

// Include APP_HEAD
require_once (ROOT_PATH.'includes/app_top.php');

//  \Cataleya\Plugins\Package::uninstall('social-app');
//  exit('We did it !!');

$_Controller = Cataleya\Front\Controller::load();
$_Controller->execute();
 
