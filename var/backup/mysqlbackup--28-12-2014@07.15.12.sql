--
-- Database : cataleya
--
-- --------------------------------------------------
-- ---------------------------------------------------
SET AUTOCOMMIT = 0 ;
SET FOREIGN_KEY_CHECKS=0 ;
--
-- Table structure for table `accepted_payment_types`
--
DROP TABLE  IF EXISTS `accepted_payment_types`;
CREATE TABLE `accepted_payment_types` (
  `store_id` smallint(5) unsigned NOT NULL,
  `payment_type_id` smallint(5) unsigned NOT NULL,
  PRIMARY KEY (`store_id`,`payment_type_id`),
  KEY `fk_accepted_payment_modes_payment_mode1_idx` (`payment_type_id`),
  CONSTRAINT `fk_accepted_payment_modes_payment_mode1` FOREIGN KEY (`payment_type_id`) REFERENCES `payment_types` (`payment_type_id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `fk_accepted_payment_modes_stores1` FOREIGN KEY (`store_id`) REFERENCES `stores` (`store_id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



--
-- Table structure for table `address_book`
--
DROP TABLE  IF EXISTS `address_book`;
CREATE TABLE `address_book` (
  `entry_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `customer_id` int(11) unsigned NOT NULL,
  `entry_gender` enum('undisclosed','female','male') NOT NULL DEFAULT 'undisclosed',
  `entry_company` varchar(128) DEFAULT NULL,
  `entry_firstname` varchar(128) NOT NULL DEFAULT '',
  `entry_lastname` varchar(128) NOT NULL DEFAULT '',
  `location_id` bigint(20) unsigned NOT NULL,
  PRIMARY KEY (`entry_id`,`customer_id`),
  KEY `idx_address_book_customers_id` (`customer_id`),
  KEY `fk_address_book_locations1_idx` (`location_id`),
  CONSTRAINT `fk_address_book_locations1` FOREIGN KEY (`location_id`) REFERENCES `locations` (`location_id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



--
-- Table structure for table `address_format`
--
DROP TABLE  IF EXISTS `address_format`;
CREATE TABLE `address_format` (
  `address_format_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `address_format` varchar(128) NOT NULL DEFAULT '',
  `address_summary` varchar(48) NOT NULL DEFAULT '',
  PRIMARY KEY (`address_format_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;



--
-- Table structure for table `admin`
--
DROP TABLE  IF EXISTS `admin`;
CREATE TABLE `admin` (
  `admin_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `role_id` smallint(5) unsigned NOT NULL,
  `firstname` varchar(32) NOT NULL DEFAULT '',
  `lastname` varchar(32) NOT NULL,
  `image_id` bigint(20) unsigned DEFAULT NULL,
  `admin_email` varchar(128) NOT NULL DEFAULT '',
  `admin_pass` varchar(255) NOT NULL DEFAULT '',
  `prev_pass1` varchar(255) NOT NULL DEFAULT '',
  `prev_pass2` varchar(255) NOT NULL DEFAULT '',
  `prev_pass3` varchar(255) NOT NULL DEFAULT '',
  `pwd_last_change_date` datetime NOT NULL DEFAULT '1000-01-01 00:00:00',
  `reset_token` varchar(255) NOT NULL DEFAULT '',
  `date_created` datetime NOT NULL,
  `last_modified` datetime NOT NULL DEFAULT '1000-01-01 00:00:00',
  `last_login_date` datetime NOT NULL DEFAULT '1000-01-01 00:00:00',
  `last_login_ip` varchar(15) NOT NULL DEFAULT '',
  `failed_logins` smallint(4) unsigned NOT NULL DEFAULT '0',
  `lockout_expires` int(11) NOT NULL DEFAULT '0',
  `last_failed_attempt` datetime NOT NULL DEFAULT '1000-01-01 00:00:00',
  `last_failed_ip` varchar(15) NOT NULL DEFAULT '',
  `admin_phone` varchar(15) NOT NULL DEFAULT '+234',
  `is_online` varchar(255) NOT NULL DEFAULT '0',
  `shop_key` varchar(255) NOT NULL DEFAULT '',
  `last_login_geo_loc` varchar(225) NOT NULL DEFAULT 'unknown',
  `last_failed_geo_loc` varchar(255) NOT NULL DEFAULT 'unknown',
  `active` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`admin_id`),
  KEY `idx_admin_name` (`firstname`,`lastname`),
  KEY `idx_admin_email` (`admin_email`),
  KEY `fk_admin_admin_roles1_idx` (`role_id`),
  KEY `fk_admin_images1_idx` (`image_id`),
  CONSTRAINT `fk_admin_admin_roles1` FOREIGN KEY (`role_id`) REFERENCES `admin_roles` (`role_id`) ON UPDATE NO ACTION,
  CONSTRAINT `fk_admin_images1` FOREIGN KEY (`image_id`) REFERENCES `images` (`image_id`) ON DELETE SET NULL ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

INSERT INTO `admin`  VALUES ( "1","1","Gboyega","Dada","","boyega@gmail.com","$2a$12$IQDwJAMLJXWto7Ol5wSd3.m0vM4MXaxToUTr56niN/8zirdl8Rt7u","","","","1000-01-01 00:00:00","","2014-12-26 03:36:54","2014-12-27 08:47:45","2014-12-27 07:53:45","","0","0","1000-01-01 00:00:00","","07051101640","c26c1892c3157ce9c02174fad13d126b1bdb593b:1419671865","","","unknown","1");


--
-- Table structure for table `admin_activity_log`
--
DROP TABLE  IF EXISTS `admin_activity_log`;
CREATE TABLE `admin_activity_log` (
  `log_id` bigint(15) unsigned NOT NULL AUTO_INCREMENT,
  `admin_id` int(11) unsigned NOT NULL,
  `access_date` datetime NOT NULL DEFAULT '0001-01-01 00:00:00',
  `page_accessed` varchar(80) NOT NULL DEFAULT '',
  `page_parameters` text,
  `ip_address` varchar(20) NOT NULL DEFAULT '',
  `flagged` tinyint(4) NOT NULL DEFAULT '0',
  `attention` varchar(255) NOT NULL DEFAULT '',
  `gzpost` mediumblob NOT NULL,
  PRIMARY KEY (`log_id`),
  KEY `idx_page_accessed_zen` (`page_accessed`),
  KEY `idx_access_date_zen` (`access_date`),
  KEY `idx_flagged_zen` (`flagged`),
  KEY `idx_ip_zen` (`ip_address`),
  KEY `fk_admin_activity_log_admin1_idx` (`admin_id`),
  CONSTRAINT `fk_admin_activity_log_admin1` FOREIGN KEY (`admin_id`) REFERENCES `admin` (`admin_id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



--
-- Table structure for table `admin_geoloc_trend`
--
DROP TABLE  IF EXISTS `admin_geoloc_trend`;
CREATE TABLE `admin_geoloc_trend` (
  `pk` bigint(15) unsigned NOT NULL AUTO_INCREMENT,
  `admin_id` int(11) DEFAULT NULL,
  `country_iso2_code` char(2) NOT NULL,
  `city` varchar(45) DEFAULT NULL,
  `login_tally` int(11) unsigned NOT NULL DEFAULT '0',
  `last_login` datetime NOT NULL,
  `date_added` datetime NOT NULL,
  PRIMARY KEY (`pk`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



--
-- Table structure for table `admin_roles`
--
DROP TABLE  IF EXISTS `admin_roles`;
CREATE TABLE `admin_roles` (
  `role_id` smallint(5) unsigned NOT NULL AUTO_INCREMENT,
  `description_id` bigint(20) unsigned NOT NULL,
  `date_created` datetime NOT NULL,
  `last_modified` datetime NOT NULL,
  PRIMARY KEY (`role_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

INSERT INTO `admin_roles`  VALUES ( "1","1","2014-12-26 03:36:01","2014-12-26 03:36:01");
INSERT INTO `admin_roles`  VALUES ( "2","2","2014-12-26 03:36:01","2014-12-26 03:36:01");
INSERT INTO `admin_roles`  VALUES ( "3","3","2014-12-26 03:36:01","2014-12-26 03:36:01");
INSERT INTO `admin_roles`  VALUES ( "4","4","2014-12-26 03:36:01","2014-12-26 03:36:01");


--
-- Table structure for table `admin_ua_trend`
--
DROP TABLE  IF EXISTS `admin_ua_trend`;
CREATE TABLE `admin_ua_trend` (
  `pk` bigint(15) unsigned NOT NULL AUTO_INCREMENT,
  `admin_id` int(11) unsigned NOT NULL,
  `fingerprint` text NOT NULL,
  `login_tally` int(11) unsigned NOT NULL DEFAULT '0',
  `last_login` datetime NOT NULL,
  `date_added` datetime NOT NULL,
  PRIMARY KEY (`pk`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



--
-- Table structure for table `alerts`
--
DROP TABLE  IF EXISTS `alerts`;
CREATE TABLE `alerts` (
  `alert_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `admin_id` int(11) unsigned NOT NULL,
  `notification_id` smallint(4) unsigned NOT NULL,
  `to_email` tinyint(1) NOT NULL DEFAULT '0',
  `to_dashboard` tinyint(1) NOT NULL DEFAULT '0',
  `is_displayed` tinyint(1) NOT NULL DEFAULT '0',
  `is_read` tinyint(1) NOT NULL DEFAULT '0',
  `tally` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`alert_id`,`admin_id`,`notification_id`),
  KEY `notification_recipient_admin_id_idx` (`admin_id`),
  KEY `notifications_notification_id_idx` (`notification_id`),
  CONSTRAINT `notifications_notification_id0` FOREIGN KEY (`notification_id`) REFERENCES `notification_profile` (`notification_id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `notification_recipient_admin_id0` FOREIGN KEY (`admin_id`) REFERENCES `admin` (`admin_id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=utf8;

INSERT INTO `alerts`  VALUES ( "1","1","1","0","0","0","0","0");
INSERT INTO `alerts`  VALUES ( "2","1","2","0","0","0","0","0");
INSERT INTO `alerts`  VALUES ( "3","1","3","0","0","0","0","0");
INSERT INTO `alerts`  VALUES ( "4","1","4","0","0","0","0","0");
INSERT INTO `alerts`  VALUES ( "5","1","5","0","0","0","0","0");
INSERT INTO `alerts`  VALUES ( "6","1","6","0","0","0","0","0");
INSERT INTO `alerts`  VALUES ( "7","1","7","0","0","1","1","0");
INSERT INTO `alerts`  VALUES ( "8","1","8","0","0","0","0","0");
INSERT INTO `alerts`  VALUES ( "16","1","9","0","0","1","1","0");
INSERT INTO `alerts`  VALUES ( "17","1","10","0","0","0","0","0");
INSERT INTO `alerts`  VALUES ( "18","1","11","0","0","1","1","0");
INSERT INTO `alerts`  VALUES ( "19","1","12","0","0","0","0","0");


--
-- Table structure for table `application_preferences`
--
DROP TABLE  IF EXISTS `application_preferences`;
CREATE TABLE `application_preferences` (
  `pk` bit(1) NOT NULL DEFAULT b'0',
  `app_name` enum('Cataleya') NOT NULL DEFAULT 'Cataleya',
  `app_version` varchar(15) NOT NULL,
  `created_by` enum('Fancy Paper Planes') NOT NULL DEFAULT 'Fancy Paper Planes',
  `developer` varchar(160) NOT NULL DEFAULT 'unknown',
  `client` varchar(160) NOT NULL DEFAULT 'unknown',
  `default_language` char(2) NOT NULL DEFAULT 'EN',
  `allow_pricing_by_variant` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `allow_pricing_by_attribute` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `allow_flat_pricing` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `allow_product_colors` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `allow_product_sizes` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `allow_user_defined_sizes` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `max_catalog_population` int(10) unsigned NOT NULL DEFAULT '10000',
  `max_admin_accounts` int(10) unsigned NOT NULL DEFAULT '20',
  `max_customer_accounts` int(10) unsigned NOT NULL DEFAULT '50000',
  `last_modified` datetime NOT NULL,
  `active` tinyint(1) unsigned NOT NULL,
  `last_shutdown` datetime NOT NULL,
  `last_activated` datetime NOT NULL,
  `credits` text NOT NULL,
  `shopfront_template_folder` varchar(255) NOT NULL DEFAULT 'default',
  PRIMARY KEY (`pk`),
  UNIQUE KEY `pk_UNIQUE` (`pk`),
  KEY `fk_application_preferences_languages1_idx` (`default_language`),
  CONSTRAINT `fk_application_preferences_languages1` FOREIGN KEY (`default_language`) REFERENCES `languages` (`language_code`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8 MAX_ROWS=1;

INSERT INTO `application_preferences`  VALUES ( "0","Cataleya","1.0","Fancy Paper Planes","Gboyega Dada (FPP)","Tayo Shonekan (Aimas)","EN","1","0","1","0","1","1","10000","20","50000","2014-12-26 03:36:01","1","2014-12-26 03:36:01","2014-12-27 07:50:00","Team? Just me for now :). -Gboyega for FPP.","default");
INSERT INTO `application_preferences`  VALUES ( "1","Cataleya","0","Fancy Paper Planes","unknown","unknown","EN","0","0","1","0","1","1","10000","20","50000","2014-12-26 03:36:01","0","2014-12-26 03:36:01","2014-12-26 03:36:01","FPP","default");


--
-- Table structure for table `attribute_types`
--
DROP TABLE  IF EXISTS `attribute_types`;
CREATE TABLE `attribute_types` (
  `attribute_type_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT '',
  `name_plural` varchar(255) DEFAULT '',
  `sort_order` int(11) DEFAULT NULL,
  PRIMARY KEY (`attribute_type_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

INSERT INTO `attribute_types`  VALUES ( "1","Color","Colors","0");
INSERT INTO `attribute_types`  VALUES ( "2","Size","Sizes","1");


--
-- Table structure for table `attributes`
--
DROP TABLE  IF EXISTS `attributes`;
CREATE TABLE `attributes` (
  `attribute_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `attribute_type_id` int(11) unsigned NOT NULL,
  `value` varchar(255) DEFAULT ' ',
  `sort_order` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`attribute_id`),
  KEY `fk_attribute_type_id_idx` (`attribute_type_id`),
  CONSTRAINT `fk_attribute_type_id` FOREIGN KEY (`attribute_type_id`) REFERENCES `attribute_types` (`attribute_type_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

INSERT INTO `attributes`  VALUES ( "1","1","Red","0");
INSERT INTO `attributes`  VALUES ( "2","1","Yellow","0");
INSERT INTO `attributes`  VALUES ( "3","1","Green","0");
INSERT INTO `attributes`  VALUES ( "4","2","Small","0");
INSERT INTO `attributes`  VALUES ( "5","2","Medium","0");
INSERT INTO `attributes`  VALUES ( "6","2","Large","0");


--
-- Table structure for table `banners`
--
DROP TABLE  IF EXISTS `banners`;
CREATE TABLE `banners` (
  `banners_id` int(11) NOT NULL AUTO_INCREMENT,
  `banners_title` varchar(64) NOT NULL DEFAULT '',
  `banners_url` varchar(255) NOT NULL DEFAULT '',
  `banners_image` varchar(64) NOT NULL DEFAULT '',
  `banners_group` varchar(15) NOT NULL DEFAULT '',
  `banners_html_text` text,
  `expires_impressions` int(7) DEFAULT '0',
  `expires_date` datetime DEFAULT NULL,
  `date_scheduled` datetime DEFAULT NULL,
  `date_added` datetime NOT NULL DEFAULT '0001-01-01 00:00:00',
  `date_status_change` datetime DEFAULT NULL,
  `status` int(1) NOT NULL DEFAULT '1',
  `banners_open_new_windows` int(1) NOT NULL DEFAULT '1',
  `banners_on_ssl` int(1) NOT NULL DEFAULT '1',
  `banners_sort_order` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`banners_id`),
  KEY `idx_status_group_zen` (`status`,`banners_group`),
  KEY `idx_expires_date_zen` (`expires_date`),
  KEY `idx_date_scheduled_zen` (`date_scheduled`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;



--
-- Table structure for table `banners_history`
--
DROP TABLE  IF EXISTS `banners_history`;
CREATE TABLE `banners_history` (
  `banners_history_id` int(11) NOT NULL AUTO_INCREMENT,
  `banners_id` int(11) NOT NULL DEFAULT '0',
  `banners_shown` int(5) NOT NULL DEFAULT '0',
  `banners_clicked` int(5) NOT NULL DEFAULT '0',
  `banners_history_date` datetime NOT NULL DEFAULT '0001-01-01 00:00:00',
  PRIMARY KEY (`banners_history_id`),
  KEY `idx_banners_id_zen` (`banners_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;



--
-- Table structure for table `carriers`
--
DROP TABLE  IF EXISTS `carriers`;
CREATE TABLE `carriers` (
  `carrier_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `description_id` bigint(20) unsigned NOT NULL,
  `location_id` int(11) unsigned NOT NULL,
  `contact_name` varchar(255) NOT NULL,
  `telephone` varchar(16) NOT NULL,
  `email` varchar(255) NOT NULL,
  `website` varchar(300) NOT NULL,
  `logo_image_id` bigint(20) unsigned DEFAULT NULL,
  `active` tinyint(3) unsigned NOT NULL DEFAULT '1',
  `date_added` datetime NOT NULL,
  `last_modified` datetime NOT NULL,
  PRIMARY KEY (`carrier_id`),
  KEY `fk_carriers_images1_idx` (`logo_image_id`),
  CONSTRAINT `fk_carriers_images1` FOREIGN KEY (`logo_image_id`) REFERENCES `images` (`image_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

INSERT INTO `carriers`  VALUES ( "1","77","1","Contact Name","Telephone","Email","Website","","1","2014-12-26 03:36:55","2014-12-26 03:36:55");
INSERT INTO `carriers`  VALUES ( "2","78","2","Contact Name","Telephone","Email","Website","","1","2014-12-26 03:36:55","2014-12-26 03:36:55");


--
-- Table structure for table `cart`
--
DROP TABLE  IF EXISTS `cart`;
CREATE TABLE `cart` (
  `cart_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `store_id` smallint(5) unsigned NOT NULL,
  `customer_id` int(11) unsigned DEFAULT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `secure_key` varchar(255) NOT NULL,
  PRIMARY KEY (`cart_id`),
  KEY `fk_cart_products1_idx` (`customer_id`),
  KEY `fk_cart_stores1_idx` (`store_id`),
  CONSTRAINT `fk_cart_customers` FOREIGN KEY (`customer_id`) REFERENCES `customers` (`customer_id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `fk_cart_stores1` FOREIGN KEY (`store_id`) REFERENCES `stores` (`store_id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

INSERT INTO `cart`  VALUES ( "1","1","","2014-12-27 07:50:15","2014-12-27 07:50:15","1","5c81c0e2916d8b52021b152c8b2db64b-1419666615");


--
-- Table structure for table `cart_items`
--
DROP TABLE  IF EXISTS `cart_items`;
CREATE TABLE `cart_items` (
  `cart_item_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `cart_id` int(11) unsigned NOT NULL,
  `store_id` smallint(5) unsigned NOT NULL,
  `product_id` int(11) unsigned NOT NULL,
  `option_id` bigint(20) unsigned NOT NULL,
  `coupon_id` int(10) unsigned DEFAULT NULL,
  `quantity` float NOT NULL DEFAULT '1',
  `date_added` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`cart_item_id`,`cart_id`,`product_id`,`option_id`),
  KEY `fk_cart_items_products1_idx` (`product_id`),
  KEY `fk_cart_items_options1_idx` (`option_id`),
  KEY `fk_cart_items_stores1_idx` (`store_id`),
  KEY `fk_cart_items_coupons1_idx` (`coupon_id`),
  KEY `fk_cart_items_cart10` (`cart_id`),
  CONSTRAINT `fk_cart_items_cart10` FOREIGN KEY (`cart_id`) REFERENCES `cart` (`cart_id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `fk_cart_items_coupons1` FOREIGN KEY (`coupon_id`) REFERENCES `coupons` (`coupon_id`) ON DELETE SET NULL ON UPDATE NO ACTION,
  CONSTRAINT `fk_cart_items_options1` FOREIGN KEY (`option_id`) REFERENCES `product_options` (`option_id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `fk_cart_items_products1` FOREIGN KEY (`product_id`) REFERENCES `products` (`product_id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `fk_cart_items_stores1` FOREIGN KEY (`store_id`) REFERENCES `stores` (`store_id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

INSERT INTO `cart_items`  VALUES ( "1","1","1","1","1","","1","2014-12-27 16:51:07");
INSERT INTO `cart_items`  VALUES ( "2","1","1","8","34","","1","2014-12-27 17:00:57");
INSERT INTO `cart_items`  VALUES ( "3","1","1","1","2","","1","2014-12-28 05:33:59");
INSERT INTO `cart_items`  VALUES ( "4","1","1","9","38","","1","2014-12-28 05:38:39");


--
-- Table structure for table `cat_stats_daily`
--
DROP TABLE  IF EXISTS `cat_stats_daily`;
CREATE TABLE `cat_stats_daily` (
  `pk` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `product_id` int(11) unsigned NOT NULL,
  `store_id` smallint(5) unsigned NOT NULL,
  `tally_views` int(10) unsigned NOT NULL DEFAULT '0',
  `tally_wishlists` int(10) unsigned NOT NULL DEFAULT '0',
  `tally_orders` int(10) unsigned NOT NULL DEFAULT '0',
  `order_subtotal` decimal(15,4) NOT NULL DEFAULT '0.0000',
  `date_added` datetime NOT NULL,
  PRIMARY KEY (`pk`),
  KEY `fk_views_daily_products1_idx` (`product_id`),
  KEY `fk_cat_stats_daily_stores1_idx` (`store_id`),
  CONSTRAINT `fk_cat_stats_daily_stores1` FOREIGN KEY (`store_id`) REFERENCES `stores` (`store_id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `fk_views_daily_products1` FOREIGN KEY (`product_id`) REFERENCES `products` (`product_id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

INSERT INTO `cat_stats_daily`  VALUES ( "1","2","1","3","0","0","0.0000","2014-12-27 07:50:33");
INSERT INTO `cat_stats_daily`  VALUES ( "2","1","1","31","0","0","0.0000","2014-12-27 07:52:32");
INSERT INTO `cat_stats_daily`  VALUES ( "3","8","1","1","0","0","0.0000","2014-12-27 17:00:42");
INSERT INTO `cat_stats_daily`  VALUES ( "4","1","1","30","0","0","0.0000","2014-12-28 04:22:16");
INSERT INTO `cat_stats_daily`  VALUES ( "5","9","1","1","0","0","0.0000","2014-12-28 05:38:26");
INSERT INTO `cat_stats_daily`  VALUES ( "6","2","1","1","0","0","0.0000","2014-12-28 05:38:54");


--
-- Table structure for table `cat_stats_monthly`
--
DROP TABLE  IF EXISTS `cat_stats_monthly`;
CREATE TABLE `cat_stats_monthly` (
  `pk` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `product_id` int(11) unsigned NOT NULL,
  `store_id` smallint(5) unsigned NOT NULL,
  `tally_views` int(10) unsigned NOT NULL DEFAULT '0',
  `tally_wishlists` int(10) unsigned NOT NULL DEFAULT '0',
  `tally_orders` int(10) unsigned NOT NULL DEFAULT '0',
  `order_subtotal` decimal(15,4) NOT NULL DEFAULT '0.0000',
  `date_added` datetime NOT NULL,
  PRIMARY KEY (`pk`),
  KEY `fk_views_daily_products1_idx` (`product_id`),
  KEY `fk_cat_stats_monthly_stores1_idx` (`store_id`),
  CONSTRAINT `fk_cat_stats_monthly_stores1` FOREIGN KEY (`store_id`) REFERENCES `stores` (`store_id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `fk_views_daily_products10` FOREIGN KEY (`product_id`) REFERENCES `products` (`product_id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

INSERT INTO `cat_stats_monthly`  VALUES ( "1","2","1","4","0","0","0.0000","2014-12-27 07:50:33");
INSERT INTO `cat_stats_monthly`  VALUES ( "2","1","1","61","0","0","0.0000","2014-12-27 07:52:32");
INSERT INTO `cat_stats_monthly`  VALUES ( "3","8","1","1","0","0","0.0000","2014-12-27 17:00:42");
INSERT INTO `cat_stats_monthly`  VALUES ( "4","9","1","1","0","0","0.0000","2014-12-28 05:38:26");


--
-- Table structure for table `cat_stats_x3month`
--
DROP TABLE  IF EXISTS `cat_stats_x3month`;
CREATE TABLE `cat_stats_x3month` (
  `pk` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `product_id` int(11) unsigned NOT NULL,
  `store_id` smallint(5) unsigned NOT NULL,
  `tally_views` int(10) unsigned NOT NULL DEFAULT '0',
  `tally_wishlists` int(10) unsigned NOT NULL DEFAULT '0',
  `tally_orders` int(10) unsigned NOT NULL DEFAULT '0',
  `order_subtotal` decimal(15,4) NOT NULL DEFAULT '0.0000',
  `date_added` datetime NOT NULL,
  PRIMARY KEY (`pk`),
  KEY `fk_views_daily_products1_idx` (`product_id`),
  KEY `fk_cat_stats_x3month_stores1_idx` (`store_id`),
  CONSTRAINT `fk_cat_stats_x3month_stores1` FOREIGN KEY (`store_id`) REFERENCES `stores` (`store_id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `fk_views_daily_products100` FOREIGN KEY (`product_id`) REFERENCES `products` (`product_id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

INSERT INTO `cat_stats_x3month`  VALUES ( "1","2","1","4","0","0","0.0000","2014-12-27 07:50:33");
INSERT INTO `cat_stats_x3month`  VALUES ( "2","1","1","61","0","0","0.0000","2014-12-27 07:52:32");
INSERT INTO `cat_stats_x3month`  VALUES ( "3","8","1","1","0","0","0.0000","2014-12-27 17:00:42");
INSERT INTO `cat_stats_x3month`  VALUES ( "4","9","1","1","0","0","0.0000","2014-12-28 05:38:26");


--
-- Table structure for table `config`
--
DROP TABLE  IF EXISTS `config`;
CREATE TABLE `config` (
  `config_id` varchar(255) NOT NULL,
  `param_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `value` blob NOT NULL,
  `last_modified` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`param_id`,`config_id`)
) ENGINE=InnoDB AUTO_INCREMENT=41 DEFAULT CHARSET=utf8;

INSERT INTO `config`  VALUES ( "open.exchange.config","1","YXBpX2tleQ==","6tWCU0QqmGL7HIgCFMX7WzErm590TwTkM7ZGeO9eWJaSM7ZX3fZ6yVYxprtAtUC6Zhc1GcrP8GxqbjW9ERBIEQ==","2014-12-26 03:36:01");
INSERT INTO `config`  VALUES ( "open.exchange.config","2","dXJs","htg3quxgKJkb/AC4AeMv/s9pAnM31deWYYeSe1TbNdt60qhpfEIAtB/GxXmsIYlIvqUB1J2YQbRC6PdKFRXNJHIPpkdfxX2UrBmCbBetkRgQtrtKdPy2wYh7ROYglpdJ","2014-12-26 03:36:01");
INSERT INTO `config`  VALUES ( "open.exchange.config","3","c2VjdXJlX3VybA==","jHvGH/Ts50kAhiFlaiRksCKAgSOwFyE830pkSOjbtGwvPcFjWan64DdYOXgTo55ZFx6pk3LpDrCgLj2WGbg+l2xsepDvGXmX6/mJEDEjKkUeKV7YrD8M8zGJ5Gh2FB42","2014-12-26 03:36:01");
INSERT INTO `config`  VALUES ( "open.exchange.config","4","YmFzZV9jdXJyZW5jeQ==","J3crqumu48m59eFv9VpNj+4pKz0Gqbvh+KTAvMN1WHtl0WEgOhHKhHYuQJhPY+NMIoGsaKfiKVhM7b+ebtOlHQ==","2014-12-26 03:36:01");
INSERT INTO `config`  VALUES ( "twitter.config","5","YXBpX2tleQ==","lkPkExxt53CCOIy3fpXVD596wA+Ll+NNkDmQrkDr7IOfnbng2B56aYzEPtBNmzO6e0BcWQZwEIWNf7VRcNhQ0g==","2014-12-26 03:36:01");
INSERT INTO `config`  VALUES ( "twitter.config","6","dXNlcg==","TcWbei5XlBs6JzPJpPk863ulFmwGmqr1DxMRwIHWMNKFdZJXRfaZwyXDZn7fzXVpDYOOmD4eWiGQjYl7KfiHkg==","2014-12-26 03:36:01");
INSERT INTO `config`  VALUES ( "twitter.config","7","cGFzcw==","UmO0hhbu6pu/kgakWN5wjjp1UU35qjeNpJzhro3OnMuobgHwRRD3ZYNGT/nwyfuOxfFzRaHp88EONgvTw411gQ==","2014-12-26 03:36:01");
INSERT INTO `config`  VALUES ( "facebook.config","8","YXBpX2tleQ==","4OE6jo7V2Ai+6R0jKYkdhDE9E6IDnVG1oqV9FQHfM1LuwtfQVsnJH68qtRWieI5JKcnwVbga4wdJaJ9qZm0ObA==","2014-12-26 03:36:01");
INSERT INTO `config`  VALUES ( "facebook.config","9","dXNlcg==","trrSSdnwD0phOXFYeYY6VV2vONd9+cTZ9SaIQILhwm1Ls2ezkN8xx/m4uAWXXShCpRZLfAR7j+ZVFH7k67HDiw==","2014-12-26 03:36:01");
INSERT INTO `config`  VALUES ( "facebook.config","10","cGFzcw==","gBs26WN7FhmgJQpVw9DjaldcMAH+f8gmAc1hJtY6nZYjd9/s6BsTV/ymdJHm/aLtVKZyuR1JbPk8eFzkx7S+eg==","2014-12-26 03:36:01");
INSERT INTO `config`  VALUES ( "bank.transfer.config","11","dHJhbnhfdXJs","","2014-12-26 03:36:01");
INSERT INTO `config`  VALUES ( "bank.transfer.config","12","cmV0dXJuX3VybA==","n4S8ygShkxo9GAMjdt0ZIBlIO67w3n3u3v0mt/tXVtQON4K3hXUV08+a0Z1jRtVR5x28KbkxXnLPwC1J2xvVMzSy5FAxegPpfjSmXKXeC7qNq7lmgWjFJ1oHqRby1j++","2014-12-26 03:36:01");
INSERT INTO `config`  VALUES ( "cash.on.delivery.config","13","dHJhbnhfdXJs","","2014-12-26 03:36:01");
INSERT INTO `config`  VALUES ( "cash.on.delivery.config","14","cmV0dXJuX3VybA==","G3VlB9XgSQvcp3bCaV6+RefgLMbG44oY8quZA8ub3BT31piK8N2+55cHSi7fbf2+H5pwK/9RVBQZjq7aPIQPRYSjBc6FnXGI72S35wF8E3qVwiXi2T6VkYN4F/x+7Wvz","2014-12-26 03:36:01");
INSERT INTO `config`  VALUES ( "webpay.config","15","bWVyY2hhbnRfaWQ=","","2014-12-26 03:36:01");
INSERT INTO `config`  VALUES ( "webpay.config","16","dHJhbnhfdXJs","WBL7FPWGre6wzKSl7/suLZHObUvmjB8gxDqUq0fnEUOXtkKhq6HoCBES5rjqxL+rPS8SvRFgbJGcdovvJlswTVJY2sb1ZBnuVQnku4AdheLi6Gy1QTBb7+akBBL0qpVX","2014-12-26 03:36:01");
INSERT INTO `config`  VALUES ( "webpay.config","17","bm9fc2hvd19ndGJhbms=","gIFycSlfdBRo7GV5PJjUlafYZ3pGlwz5r3/aj7qf+g636IfY2ANd1B+roD/LKhrnJu3bT1SFMhArJVDO8Yx0bw==","2014-12-26 03:36:01");
INSERT INTO `config`  VALUES ( "webpay.config","18","bm9fc2hvd19jdXN0b21lcl9pZA==","ENWroVUqL+MAz27rZoCF60TsFq1EPUXI9q27/Pw7IW3wrKgdhE6V4m2sU4y7hWTHCXApYbLFLYpYQDwQmadt9w==","2014-12-26 03:36:01");
INSERT INTO `config`  VALUES ( "webpay.config","19","Z2F0ZXdheV9uYW1l","","2014-12-26 03:36:01");
INSERT INTO `config`  VALUES ( "webpay.config","20","Z2F0ZXdheV9maXJzdA==","","2014-12-26 03:36:01");
INSERT INTO `config`  VALUES ( "webpay.config","21","cmV0dXJuX3VybA==","4B30Ew0eZmvIOzvkO2t1TgX0yBiv2wECl8K7zcsNEs20/uRAem3da6Cjrl7d4oL9N1eC24GGTFi8seY2fVxKDdcTayIa9tHnWDhONO4fNo84EO4zr1pAVdOSB4AK0+yU","2014-12-26 03:36:01");
INSERT INTO `config`  VALUES ( "paypal.config","22","Y3VybF9lbmRfcG9pbnQ=","0DTOyFKw0z+itNg36pz7BmGPjxKKmkly6W1w59WsKwezD/ITbW+jGtcyazyye3VPbDiRi9jOXCKsSRIEjSKWVMcLXwwhqta/fybdyqrW/qQwuUxIQDmpp78skgF9f6KQ","2014-12-26 03:36:01");
INSERT INTO `config`  VALUES ( "paypal.config","23","c2hvcHBlcl9lbmRfcG9pbnQ=","NA8ROD7caXSJuKBJFetv3ADS4PhwUWhexc0UU+Km4fqQUFmf5u/GoJZGxS2FONqTc88Z73x6pfpQp3bng8lSszaBdP1yqOS2Fx9M7ca9eY020QT3CoemM/5vFLsk7KFu","2014-12-26 03:36:01");
INSERT INTO `config`  VALUES ( "paypal.config","24","dXNlcg==","lcSZoI+KjlZLsezVKDQ03h9IbkFIml9PKQ7lykcXtvN+7JU19EJ3pcL1wOG8eIueu3lGb7/YSGSHeEY8aEAsuw==","2014-12-26 03:36:01");
INSERT INTO `config`  VALUES ( "paypal.config","25","cGFzcw==","ctF6IDy1hZol+gvNr1jg2DCro1GMW+FLEkfqwh3c21HfWW9F8RKCGdOGpZSn29ZZPpULeJTUHzA3gbwyt37z7A==","2014-12-26 03:36:01");
INSERT INTO `config`  VALUES ( "paypal.config","26","c2lnbmF0dXJl","","2014-12-26 03:36:01");
INSERT INTO `config`  VALUES ( "paypal.config","27","dmVyc2lvbg==","fctJJll3qjg79LGgzrWUAQY7ICwLc4e55izWmhqPrlfUIz6eYjF1ck9pZCArcuCyrUOvwkNe18his6LSS+jtQg==","2014-12-26 03:36:01");
INSERT INTO `config`  VALUES ( "paypal.config","28","cmV0dXJuX3VybA==","YSVHaKd6HoEhpdXnauA2yRF/NRB4+O9M7HvDM5SXW/hnaBV1T3HyV3Xn4eFgC7zgQzYZ5QnlAV9tzkJhpAzWoACgtXBibxizeWbEG9Slgv9vHR2wKiS6FHi7z3RkTb8l","2014-12-26 03:36:01");
INSERT INTO `config`  VALUES ( "paypal.config","29","Y2FuY2VsX3VybA==","cLiNzFZnhO2/Cc895ylm/r2FyPNzSL5izsRygtb1vaMon9uSOxKImjjVmyst45YYHqpl50gp8cXIGz9bA/LjRD1f9+uP3MNWg04BXJzjAxSUZQ0jg7Bwfhi5fCS6aaVV","2014-12-26 03:36:01");
INSERT INTO `config`  VALUES ( "mailer.config","30","ZW5jcnlwdGlvbg==","J2V32fO+TCdn46F5JJep5a9/fq/B+ObMhZ4OIs5Ir0yTCD84O+Le8oDet+KetTT560h05FIHhFXjv7Xg8tfkog==","2014-12-26 03:36:01");
INSERT INTO `config`  VALUES ( "mailer.config","31","bG9jYWxob3N0","t4aYj18zGIQ+WDgX71N1di/czEktTMl8itCUvEr1ridbAIPxS+nu8bBU+PPF6n4gKHqhCt1sLXYmzYnPmGBnFw==","2014-12-26 03:36:01");
INSERT INTO `config`  VALUES ( "mailer.config","32","cG9ydA==","e8sPNBOIm4IpaGqkKRJsESEYm6nnvdyej4/TEgUDfEfuq14A/w5YonhVILXphrKnpSRPWVHCowaKU6KcD238xg==","2014-12-26 03:36:01");
INSERT INTO `config`  VALUES ( "mailer.config","33","c210cEhvc3Q=","","2014-12-26 03:36:01");
INSERT INTO `config`  VALUES ( "mailer.config","34","dXNlcg==","","2014-12-26 03:36:01");
INSERT INTO `config`  VALUES ( "mailer.config","35","cGFzcw==","","2014-12-26 03:36:01");
INSERT INTO `config`  VALUES ( "store.core","36","ZG9tYWlu","pHibGPvOCMe6fyw4VcPMsJq6RIiYLOztQy8RpJ+Az4ypvCu0+5LZQRqIepVbYZtK/6fwEE2d/W/jFtxCYuz/9Q==","2014-12-26 03:36:01");
INSERT INTO `config`  VALUES ( "store.core","37","aG9zdA==","5hO1e2083lU/EEx2VMxKHHaFTsTqk4Bi+VR0DBBE+WvCLUTD7krua2hliv5jVEox9wF0XmWEhRBret+9OkNGig==","2014-12-26 03:36:01");
INSERT INTO `config`  VALUES ( "store.core","38","YWRtaW5Sb290","Rl/6lnfm3/t5oxwjYwzW7LYKoZqSG0vML7ZMzLv3WK/GYqI9z1nDDyXTmv23HyJVaKMYyMMScM8RPsdD5TDWfA==","2014-12-26 03:36:01");
INSERT INTO `config`  VALUES ( "store.core","39","c3RvcmVSb290","usJVZ3v1bOR9hASBC2Bl5vtZDw+hBWwGclG6pCEDV1PrZF2Z+wTCc5JYjFJwh1XsXtplceOOW0ZabSb8gUOdKg==","2014-12-26 03:36:01");
INSERT INTO `config`  VALUES ( "store.core","40","c2hvcExhbmRpbmc=","4uEyNiZv4L7y6lENbMwJ6cGnhNqrKsqbiMQugVHcxqxwcYJ6oU3SYioI1yiohm43yuSkGWg9tu7QFxj3nILskA==","2014-12-26 03:36:01");


--
-- Table structure for table `countries`
--
DROP TABLE  IF EXISTS `countries`;
CREATE TABLE `countries` (
  `country_code` char(2) NOT NULL DEFAULT '',
  `name` varchar(160) NOT NULL DEFAULT '',
  `printable_name` varchar(160) NOT NULL,
  `iso3` char(3) DEFAULT '',
  `numcode` smallint(6) DEFAULT NULL,
  `has_states` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`country_code`),
  UNIQUE KEY `country_iso_code_2_UNIQUE` (`country_code`),
  UNIQUE KEY `country_iso_code_3_UNIQUE` (`iso3`),
  KEY `idx_countries_name` (`name`),
  KEY `idx_iso_2` (`country_code`),
  KEY `idx_iso_3` (`iso3`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `countries`  VALUES ( "AD","ANDORRA","Andorra","AND","20","0");
INSERT INTO `countries`  VALUES ( "AE","UNITED ARAB EMIRATES","United Arab Emirates","ARE","784","0");
INSERT INTO `countries`  VALUES ( "AF","AFGHANISTAN","Afghanistan","AFG","4","0");
INSERT INTO `countries`  VALUES ( "AG","ANTIGUA AND BARBUDA","Antigua and Barbuda","ATG","28","0");
INSERT INTO `countries`  VALUES ( "AI","ANGUILLA","Anguilla","AIA","660","0");
INSERT INTO `countries`  VALUES ( "AL","ALBANIA","Albania","ALB","8","0");
INSERT INTO `countries`  VALUES ( "AM","ARMENIA","Armenia","ARM","51","0");
INSERT INTO `countries`  VALUES ( "AN","NETHERLANDS ANTILLES","Netherlands Antilles","ANT","530","0");
INSERT INTO `countries`  VALUES ( "AO","ANGOLA","Angola","AGO","24","0");
INSERT INTO `countries`  VALUES ( "AQ","ANTARCTICA","Antarctica","","","0");
INSERT INTO `countries`  VALUES ( "AR","ARGENTINA","Argentina","ARG","32","0");
INSERT INTO `countries`  VALUES ( "AS","AMERICAN SAMOA","American Samoa","ASM","16","0");
INSERT INTO `countries`  VALUES ( "AT","AUSTRIA","Austria","AUT","40","0");
INSERT INTO `countries`  VALUES ( "AU","AUSTRALIA","Australia","AUS","36","0");
INSERT INTO `countries`  VALUES ( "AW","ARUBA","Aruba","ABW","533","0");
INSERT INTO `countries`  VALUES ( "AZ","AZERBAIJAN","Azerbaijan","AZE","31","0");
INSERT INTO `countries`  VALUES ( "BA","BOSNIA AND HERZEGOVINA","Bosnia and Herzegovina","BIH","70","0");
INSERT INTO `countries`  VALUES ( "BB","BARBADOS","Barbados","BRB","52","0");
INSERT INTO `countries`  VALUES ( "BD","BANGLADESH","Bangladesh","BGD","50","0");
INSERT INTO `countries`  VALUES ( "BE","BELGIUM","Belgium","BEL","56","0");
INSERT INTO `countries`  VALUES ( "BF","BURKINA FASO","Burkina Faso","BFA","854","0");
INSERT INTO `countries`  VALUES ( "BG","BULGARIA","Bulgaria","BGR","100","0");
INSERT INTO `countries`  VALUES ( "BH","BAHRAIN","Bahrain","BHR","48","0");
INSERT INTO `countries`  VALUES ( "BI","BURUNDI","Burundi","BDI","108","0");
INSERT INTO `countries`  VALUES ( "BJ","BENIN","Benin","BEN","204","0");
INSERT INTO `countries`  VALUES ( "BM","BERMUDA","Bermuda","BMU","60","0");
INSERT INTO `countries`  VALUES ( "BN","BRUNEI DARUSSALAM","Brunei Darussalam","BRN","96","0");
INSERT INTO `countries`  VALUES ( "BO","BOLIVIA","Bolivia","BOL","68","0");
INSERT INTO `countries`  VALUES ( "BR","BRAZIL","Brazil","BRA","76","0");
INSERT INTO `countries`  VALUES ( "BS","BAHAMAS","Bahamas","BHS","44","0");
INSERT INTO `countries`  VALUES ( "BT","BHUTAN","Bhutan","BTN","64","0");
INSERT INTO `countries`  VALUES ( "BV","BOUVET ISLAND","Bouvet Island","","","0");
INSERT INTO `countries`  VALUES ( "BW","BOTSWANA","Botswana","BWA","72","0");
INSERT INTO `countries`  VALUES ( "BY","BELARUS","Belarus","BLR","112","0");
INSERT INTO `countries`  VALUES ( "BZ","BELIZE","Belize","BLZ","84","0");
INSERT INTO `countries`  VALUES ( "CA","CANADA","Canada","CAN","124","0");
INSERT INTO `countries`  VALUES ( "CC","COCOS (KEELING) ISLANDS","Cocos (Keeling) Islands","","","0");
INSERT INTO `countries`  VALUES ( "CD","CONGO, THE DEMOCRATIC REPUBLIC OF THE","Congo, the Democratic Republic of the","COD","180","0");
INSERT INTO `countries`  VALUES ( "CF","CENTRAL AFRICAN REPUBLIC","Central African Republic","CAF","140","0");
INSERT INTO `countries`  VALUES ( "CG","CONGO","Congo","COG","178","0");
INSERT INTO `countries`  VALUES ( "CH","SWITZERLAND","Switzerland","CHE","756","0");
INSERT INTO `countries`  VALUES ( "CI","COTE D'IVOIRE","Cote D'Ivoire","CIV","384","0");
INSERT INTO `countries`  VALUES ( "CK","COOK ISLANDS","Cook Islands","COK","184","0");
INSERT INTO `countries`  VALUES ( "CL","CHILE","Chile","CHL","152","0");
INSERT INTO `countries`  VALUES ( "CM","CAMEROON","Cameroon","CMR","120","0");
INSERT INTO `countries`  VALUES ( "CN","CHINA","China","CHN","156","0");
INSERT INTO `countries`  VALUES ( "CO","COLOMBIA","Colombia","COL","170","0");
INSERT INTO `countries`  VALUES ( "CR","COSTA RICA","Costa Rica","CRI","188","0");
INSERT INTO `countries`  VALUES ( "CS","SERBIA AND MONTENEGRO","Serbia and Montenegro","","","0");
INSERT INTO `countries`  VALUES ( "CU","CUBA","Cuba","CUB","192","0");
INSERT INTO `countries`  VALUES ( "CV","CAPE VERDE","Cape Verde","CPV","132","0");
INSERT INTO `countries`  VALUES ( "CX","CHRISTMAS ISLAND","Christmas Island","","","0");
INSERT INTO `countries`  VALUES ( "CY","CYPRUS","Cyprus","CYP","196","0");
INSERT INTO `countries`  VALUES ( "CZ","CZECH REPUBLIC","Czech Republic","CZE","203","0");
INSERT INTO `countries`  VALUES ( "DE","GERMANY","Germany","DEU","276","0");
INSERT INTO `countries`  VALUES ( "DJ","DJIBOUTI","Djibouti","DJI","262","0");
INSERT INTO `countries`  VALUES ( "DK","DENMARK","Denmark","DNK","208","0");
INSERT INTO `countries`  VALUES ( "DM","DOMINICA","Dominica","DMA","212","0");
INSERT INTO `countries`  VALUES ( "DO","DOMINICAN REPUBLIC","Dominican Republic","DOM","214","0");
INSERT INTO `countries`  VALUES ( "DZ","ALGERIA","Algeria","DZA","12","0");
INSERT INTO `countries`  VALUES ( "EC","ECUADOR","Ecuador","ECU","218","0");
INSERT INTO `countries`  VALUES ( "EE","ESTONIA","Estonia","EST","233","0");
INSERT INTO `countries`  VALUES ( "EG","EGYPT","Egypt","EGY","818","0");
INSERT INTO `countries`  VALUES ( "EH","WESTERN SAHARA","Western Sahara","ESH","732","0");
INSERT INTO `countries`  VALUES ( "ER","ERITREA","Eritrea","ERI","232","0");
INSERT INTO `countries`  VALUES ( "ES","SPAIN","Spain","ESP","724","0");
INSERT INTO `countries`  VALUES ( "ET","ETHIOPIA","Ethiopia","ETH","231","0");
INSERT INTO `countries`  VALUES ( "FI","FINLAND","Finland","FIN","246","0");
INSERT INTO `countries`  VALUES ( "FJ","FIJI","Fiji","FJI","242","0");
INSERT INTO `countries`  VALUES ( "FK","FALKLAND ISLANDS (MALVINAS)","Falkland Islands (Malvinas)","FLK","238","0");
INSERT INTO `countries`  VALUES ( "FM","MICRONESIA, FEDERATED STATES OF","Micronesia, Federated States of","FSM","583","0");
INSERT INTO `countries`  VALUES ( "FO","FAROE ISLANDS","Faroe Islands","FRO","234","0");
INSERT INTO `countries`  VALUES ( "FR","FRANCE","France","FRA","250","0");
INSERT INTO `countries`  VALUES ( "GA","GABON","Gabon","GAB","266","0");
INSERT INTO `countries`  VALUES ( "GB","UNITED KINGDOM","United Kingdom","GBR","826","0");
INSERT INTO `countries`  VALUES ( "GD","GRENADA","Grenada","GRD","308","0");
INSERT INTO `countries`  VALUES ( "GE","GEORGIA","Georgia","GEO","268","0");
INSERT INTO `countries`  VALUES ( "GF","FRENCH GUIANA","French Guiana","GUF","254","0");
INSERT INTO `countries`  VALUES ( "GH","GHANA","Ghana","GHA","288","0");
INSERT INTO `countries`  VALUES ( "GI","GIBRALTAR","Gibraltar","GIB","292","0");
INSERT INTO `countries`  VALUES ( "GL","GREENLAND","Greenland","GRL","304","0");
INSERT INTO `countries`  VALUES ( "GM","GAMBIA","Gambia","GMB","270","0");
INSERT INTO `countries`  VALUES ( "GN","GUINEA","Guinea","GIN","324","0");
INSERT INTO `countries`  VALUES ( "GP","GUADELOUPE","Guadeloupe","GLP","312","0");
INSERT INTO `countries`  VALUES ( "GQ","EQUATORIAL GUINEA","Equatorial Guinea","GNQ","226","0");
INSERT INTO `countries`  VALUES ( "GR","GREECE","Greece","GRC","300","0");
INSERT INTO `countries`  VALUES ( "GS","SOUTH GEORGIA AND THE SOUTH SANDWICH ISLANDS","South Georgia and the South Sandwich Islands","","","0");
INSERT INTO `countries`  VALUES ( "GT","GUATEMALA","Guatemala","GTM","320","0");
INSERT INTO `countries`  VALUES ( "GU","GUAM","Guam","GUM","316","0");
INSERT INTO `countries`  VALUES ( "GW","GUINEA-BISSAU","Guinea-Bissau","GNB","624","0");
INSERT INTO `countries`  VALUES ( "GY","GUYANA","Guyana","GUY","328","0");
INSERT INTO `countries`  VALUES ( "HK","HONG KONG","Hong Kong","HKG","344","0");
INSERT INTO `countries`  VALUES ( "HM","HEARD ISLAND AND MCDONALD ISLANDS","Heard Island and Mcdonald Islands","","","0");
INSERT INTO `countries`  VALUES ( "HN","HONDURAS","Honduras","HND","340","0");
INSERT INTO `countries`  VALUES ( "HR","CROATIA","Croatia","HRV","191","0");
INSERT INTO `countries`  VALUES ( "HT","HAITI","Haiti","HTI","332","0");
INSERT INTO `countries`  VALUES ( "HU","HUNGARY","Hungary","HUN","348","0");
INSERT INTO `countries`  VALUES ( "ID","INDONESIA","Indonesia","IDN","360","0");
INSERT INTO `countries`  VALUES ( "IE","IRELAND","Ireland","IRL","372","0");
INSERT INTO `countries`  VALUES ( "IL","ISRAEL","Israel","ISR","376","0");
INSERT INTO `countries`  VALUES ( "IN","INDIA","India","IND","356","0");
INSERT INTO `countries`  VALUES ( "IO","BRITISH INDIAN OCEAN TERRITORY","British Indian Ocean Territory","","","0");
INSERT INTO `countries`  VALUES ( "IQ","IRAQ","Iraq","IRQ","368","0");
INSERT INTO `countries`  VALUES ( "IR","IRAN, ISLAMIC REPUBLIC OF","Iran, Islamic Republic of","IRN","364","0");
INSERT INTO `countries`  VALUES ( "IS","ICELAND","Iceland","ISL","352","0");
INSERT INTO `countries`  VALUES ( "IT","ITALY","Italy","ITA","380","0");
INSERT INTO `countries`  VALUES ( "JM","JAMAICA","Jamaica","JAM","388","0");
INSERT INTO `countries`  VALUES ( "JO","JORDAN","Jordan","JOR","400","0");
INSERT INTO `countries`  VALUES ( "JP","JAPAN","Japan","JPN","392","0");
INSERT INTO `countries`  VALUES ( "KE","KENYA","Kenya","KEN","404","0");
INSERT INTO `countries`  VALUES ( "KG","KYRGYZSTAN","Kyrgyzstan","KGZ","417","0");
INSERT INTO `countries`  VALUES ( "KH","CAMBODIA","Cambodia","KHM","116","0");
INSERT INTO `countries`  VALUES ( "KI","KIRIBATI","Kiribati","KIR","296","0");
INSERT INTO `countries`  VALUES ( "KM","COMOROS","Comoros","COM","174","0");
INSERT INTO `countries`  VALUES ( "KN","SAINT KITTS AND NEVIS","Saint Kitts and Nevis","KNA","659","0");
INSERT INTO `countries`  VALUES ( "KP","KOREA, DEMOCRATIC PEOPLE'S REPUBLIC OF","Korea, Democratic People's Republic of","PRK","408","0");
INSERT INTO `countries`  VALUES ( "KR","KOREA, REPUBLIC OF","Korea, Republic of","KOR","410","0");
INSERT INTO `countries`  VALUES ( "KW","KUWAIT","Kuwait","KWT","414","0");
INSERT INTO `countries`  VALUES ( "KY","CAYMAN ISLANDS","Cayman Islands","CYM","136","0");
INSERT INTO `countries`  VALUES ( "KZ","KAZAKHSTAN","Kazakhstan","KAZ","398","0");
INSERT INTO `countries`  VALUES ( "LA","LAO PEOPLE'S DEMOCRATIC REPUBLIC","Lao People's Democratic Republic","LAO","418","0");
INSERT INTO `countries`  VALUES ( "LB","LEBANON","Lebanon","LBN","422","0");
INSERT INTO `countries`  VALUES ( "LC","SAINT LUCIA","Saint Lucia","LCA","662","0");
INSERT INTO `countries`  VALUES ( "LI","LIECHTENSTEIN","Liechtenstein","LIE","438","0");
INSERT INTO `countries`  VALUES ( "LK","SRI LANKA","Sri Lanka","LKA","144","0");
INSERT INTO `countries`  VALUES ( "LR","LIBERIA","Liberia","LBR","430","0");
INSERT INTO `countries`  VALUES ( "LS","LESOTHO","Lesotho","LSO","426","0");
INSERT INTO `countries`  VALUES ( "LT","LITHUANIA","Lithuania","LTU","440","0");
INSERT INTO `countries`  VALUES ( "LU","LUXEMBOURG","Luxembourg","LUX","442","0");
INSERT INTO `countries`  VALUES ( "LV","LATVIA","Latvia","LVA","428","0");
INSERT INTO `countries`  VALUES ( "LY","LIBYAN ARAB JAMAHIRIYA","Libyan Arab Jamahiriya","LBY","434","0");
INSERT INTO `countries`  VALUES ( "MA","MOROCCO","Morocco","MAR","504","0");
INSERT INTO `countries`  VALUES ( "MC","MONACO","Monaco","MCO","492","0");
INSERT INTO `countries`  VALUES ( "MD","MOLDOVA, REPUBLIC OF","Moldova, Republic of","MDA","498","0");
INSERT INTO `countries`  VALUES ( "MG","MADAGASCAR","Madagascar","MDG","450","0");
INSERT INTO `countries`  VALUES ( "MH","MARSHALL ISLANDS","Marshall Islands","MHL","584","0");
INSERT INTO `countries`  VALUES ( "MK","MACEDONIA, THE FORMER YUGOSLAV REPUBLIC OF","Macedonia, the Former Yugoslav Republic of","MKD","807","0");
INSERT INTO `countries`  VALUES ( "ML","MALI","Mali","MLI","466","0");
INSERT INTO `countries`  VALUES ( "MM","MYANMAR","Myanmar","MMR","104","0");
INSERT INTO `countries`  VALUES ( "MN","MONGOLIA","Mongolia","MNG","496","0");
INSERT INTO `countries`  VALUES ( "MO","MACAO","Macao","MAC","446","0");
INSERT INTO `countries`  VALUES ( "MP","NORTHERN MARIANA ISLANDS","Northern Mariana Islands","MNP","580","0");
INSERT INTO `countries`  VALUES ( "MQ","MARTINIQUE","Martinique","MTQ","474","0");
INSERT INTO `countries`  VALUES ( "MR","MAURITANIA","Mauritania","MRT","478","0");
INSERT INTO `countries`  VALUES ( "MS","MONTSERRAT","Montserrat","MSR","500","0");
INSERT INTO `countries`  VALUES ( "MT","MALTA","Malta","MLT","470","0");
INSERT INTO `countries`  VALUES ( "MU","MAURITIUS","Mauritius","MUS","480","0");
INSERT INTO `countries`  VALUES ( "MV","MALDIVES","Maldives","MDV","462","0");
INSERT INTO `countries`  VALUES ( "MW","MALAWI","Malawi","MWI","454","0");
INSERT INTO `countries`  VALUES ( "MX","MEXICO","Mexico","MEX","484","0");
INSERT INTO `countries`  VALUES ( "MY","MALAYSIA","Malaysia","MYS","458","0");
INSERT INTO `countries`  VALUES ( "MZ","MOZAMBIQUE","Mozambique","MOZ","508","0");
INSERT INTO `countries`  VALUES ( "NA","NAMIBIA","Namibia","NAM","516","0");
INSERT INTO `countries`  VALUES ( "NC","NEW CALEDONIA","New Caledonia","NCL","540","0");
INSERT INTO `countries`  VALUES ( "NE","NIGER","Niger","NER","562","0");
INSERT INTO `countries`  VALUES ( "NF","NORFOLK ISLAND","Norfolk Island","NFK","574","0");
INSERT INTO `countries`  VALUES ( "NG","NIGERIA","Nigeria","NGA","566","1");
INSERT INTO `countries`  VALUES ( "NI","NICARAGUA","Nicaragua","NIC","558","0");
INSERT INTO `countries`  VALUES ( "NL","NETHERLANDS","Netherlands","NLD","528","0");
INSERT INTO `countries`  VALUES ( "NO","NORWAY","Norway","NOR","578","0");
INSERT INTO `countries`  VALUES ( "NP","NEPAL","Nepal","NPL","524","0");
INSERT INTO `countries`  VALUES ( "NR","NAURU","Nauru","NRU","520","0");
INSERT INTO `countries`  VALUES ( "NU","NIUE","Niue","NIU","570","0");
INSERT INTO `countries`  VALUES ( "NZ","NEW ZEALAND","New Zealand","NZL","554","0");
INSERT INTO `countries`  VALUES ( "OM","OMAN","Oman","OMN","512","0");
INSERT INTO `countries`  VALUES ( "PA","PANAMA","Panama","PAN","591","0");
INSERT INTO `countries`  VALUES ( "PE","PERU","Peru","PER","604","0");
INSERT INTO `countries`  VALUES ( "PF","FRENCH POLYNESIA","French Polynesia","PYF","258","0");
INSERT INTO `countries`  VALUES ( "PG","PAPUA NEW GUINEA","Papua New Guinea","PNG","598","0");
INSERT INTO `countries`  VALUES ( "PH","PHILIPPINES","Philippines","PHL","608","0");
INSERT INTO `countries`  VALUES ( "PK","PAKISTAN","Pakistan","PAK","586","0");
INSERT INTO `countries`  VALUES ( "PL","POLAND","Poland","POL","616","0");
INSERT INTO `countries`  VALUES ( "PM","SAINT PIERRE AND MIQUELON","Saint Pierre and Miquelon","SPM","666","0");
INSERT INTO `countries`  VALUES ( "PN","PITCAIRN","Pitcairn","PCN","612","0");
INSERT INTO `countries`  VALUES ( "PR","PUERTO RICO","Puerto Rico","PRI","630","0");
INSERT INTO `countries`  VALUES ( "PS","PALESTINIAN TERRITORY, OCCUPIED","Palestinian Territory, Occupied","","","0");
INSERT INTO `countries`  VALUES ( "PT","PORTUGAL","Portugal","PRT","620","0");
INSERT INTO `countries`  VALUES ( "PW","PALAU","Palau","PLW","585","0");
INSERT INTO `countries`  VALUES ( "PY","PARAGUAY","Paraguay","PRY","600","0");
INSERT INTO `countries`  VALUES ( "QA","QATAR","Qatar","QAT","634","0");
INSERT INTO `countries`  VALUES ( "RE","REUNION","Reunion","REU","638","0");
INSERT INTO `countries`  VALUES ( "RO","ROMANIA","Romania","ROM","642","0");
INSERT INTO `countries`  VALUES ( "RU","RUSSIAN FEDERATION","Russian Federation","RUS","643","0");
INSERT INTO `countries`  VALUES ( "RW","RWANDA","Rwanda","RWA","646","0");
INSERT INTO `countries`  VALUES ( "SA","SAUDI ARABIA","Saudi Arabia","SAU","682","0");
INSERT INTO `countries`  VALUES ( "SB","SOLOMON ISLANDS","Solomon Islands","SLB","90","0");
INSERT INTO `countries`  VALUES ( "SC","SEYCHELLES","Seychelles","SYC","690","0");
INSERT INTO `countries`  VALUES ( "SD","SUDAN","Sudan","SDN","736","0");
INSERT INTO `countries`  VALUES ( "SE","SWEDEN","Sweden","SWE","752","0");
INSERT INTO `countries`  VALUES ( "SG","SINGAPORE","Singapore","SGP","702","0");
INSERT INTO `countries`  VALUES ( "SH","SAINT HELENA","Saint Helena","SHN","654","0");
INSERT INTO `countries`  VALUES ( "SI","SLOVENIA","Slovenia","SVN","705","0");
INSERT INTO `countries`  VALUES ( "SJ","SVALBARD AND JAN MAYEN","Svalbard and Jan Mayen","SJM","744","0");
INSERT INTO `countries`  VALUES ( "SK","SLOVAKIA","Slovakia","SVK","703","0");
INSERT INTO `countries`  VALUES ( "SL","SIERRA LEONE","Sierra Leone","SLE","694","0");
INSERT INTO `countries`  VALUES ( "SM","SAN MARINO","San Marino","SMR","674","0");
INSERT INTO `countries`  VALUES ( "SN","SENEGAL","Senegal","SEN","686","0");
INSERT INTO `countries`  VALUES ( "SO","SOMALIA","Somalia","SOM","706","0");
INSERT INTO `countries`  VALUES ( "SR","SURINAME","Suriname","SUR","740","0");
INSERT INTO `countries`  VALUES ( "ST","SAO TOME AND PRINCIPE","Sao Tome and Principe","STP","678","0");
INSERT INTO `countries`  VALUES ( "SV","EL SALVADOR","El Salvador","SLV","222","0");
INSERT INTO `countries`  VALUES ( "SY","SYRIAN ARAB REPUBLIC","Syrian Arab Republic","SYR","760","0");
INSERT INTO `countries`  VALUES ( "SZ","SWAZILAND","Swaziland","SWZ","748","0");
INSERT INTO `countries`  VALUES ( "TC","TURKS AND CAICOS ISLANDS","Turks and Caicos Islands","TCA","796","0");
INSERT INTO `countries`  VALUES ( "TD","CHAD","Chad","TCD","148","0");
INSERT INTO `countries`  VALUES ( "TF","FRENCH SOUTHERN TERRITORIES","French Southern Territories","","","0");
INSERT INTO `countries`  VALUES ( "TG","TOGO","Togo","TGO","768","0");
INSERT INTO `countries`  VALUES ( "TH","THAILAND","Thailand","THA","764","0");
INSERT INTO `countries`  VALUES ( "TJ","TAJIKISTAN","Tajikistan","TJK","762","0");
INSERT INTO `countries`  VALUES ( "TK","TOKELAU","Tokelau","TKL","772","0");
INSERT INTO `countries`  VALUES ( "TL","TIMOR-LESTE","Timor-Leste","","","0");
INSERT INTO `countries`  VALUES ( "TM","TURKMENISTAN","Turkmenistan","TKM","795","0");
INSERT INTO `countries`  VALUES ( "TN","TUNISIA","Tunisia","TUN","788","0");
INSERT INTO `countries`  VALUES ( "TO","TONGA","Tonga","TON","776","0");
INSERT INTO `countries`  VALUES ( "TR","TURKEY","Turkey","TUR","792","0");
INSERT INTO `countries`  VALUES ( "TT","TRINIDAD AND TOBAGO","Trinidad and Tobago","TTO","780","0");
INSERT INTO `countries`  VALUES ( "TV","TUVALU","Tuvalu","TUV","798","0");
INSERT INTO `countries`  VALUES ( "TW","TAIWAN, PROVINCE OF CHINA","Taiwan, Province of China","TWN","158","0");
INSERT INTO `countries`  VALUES ( "TZ","TANZANIA, UNITED REPUBLIC OF","Tanzania, United Republic of","TZA","834","0");
INSERT INTO `countries`  VALUES ( "UA","UKRAINE","Ukraine","UKR","804","0");
INSERT INTO `countries`  VALUES ( "UG","UGANDA","Uganda","UGA","800","0");
INSERT INTO `countries`  VALUES ( "UM","UNITED STATES MINOR OUTLYING ISLANDS","United States Minor Outlying Islands","","","0");
INSERT INTO `countries`  VALUES ( "US","UNITED STATES","United States","USA","840","0");
INSERT INTO `countries`  VALUES ( "UY","URUGUAY","Uruguay","URY","858","0");
INSERT INTO `countries`  VALUES ( "UZ","UZBEKISTAN","Uzbekistan","UZB","860","0");
INSERT INTO `countries`  VALUES ( "VA","HOLY SEE (VATICAN CITY STATE)","Holy See (Vatican City State)","VAT","336","0");
INSERT INTO `countries`  VALUES ( "VC","SAINT VINCENT AND THE GRENADINES","Saint Vincent and the Grenadines","VCT","670","0");
INSERT INTO `countries`  VALUES ( "VE","VENEZUELA","Venezuela","VEN","862","0");
INSERT INTO `countries`  VALUES ( "VG","VIRGIN ISLANDS, BRITISH","Virgin Islands, British","VGB","92","0");
INSERT INTO `countries`  VALUES ( "VI","VIRGIN ISLANDS, U.S.","Virgin Islands, U.s.","VIR","850","0");
INSERT INTO `countries`  VALUES ( "VN","VIET NAM","Viet Nam","VNM","704","0");
INSERT INTO `countries`  VALUES ( "VU","VANUATU","Vanuatu","VUT","548","0");
INSERT INTO `countries`  VALUES ( "WF","WALLIS AND FUTUNA","Wallis and Futuna","WLF","876","0");
INSERT INTO `countries`  VALUES ( "WS","SAMOA","Samoa","WSM","882","0");
INSERT INTO `countries`  VALUES ( "YE","YEMEN","Yemen","YEM","887","0");
INSERT INTO `countries`  VALUES ( "YT","MAYOTTE","Mayotte","","","0");
INSERT INTO `countries`  VALUES ( "ZA","SOUTH AFRICA","South Africa","ZAF","710","0");
INSERT INTO `countries`  VALUES ( "ZM","ZAMBIA","Zambia","ZMB","894","0");
INSERT INTO `countries`  VALUES ( "ZW","ZIMBABWE","Zimbabwe","ZWE","716","0");


--
-- Table structure for table `coupon_email_track`
--
DROP TABLE  IF EXISTS `coupon_email_track`;
CREATE TABLE `coupon_email_track` (
  `unique_id` int(11) NOT NULL AUTO_INCREMENT,
  `coupon_id` int(11) NOT NULL DEFAULT '0',
  `customer_id_sent` int(11) NOT NULL DEFAULT '0',
  `sent_firstname` varchar(32) DEFAULT NULL,
  `sent_lastname` varchar(32) DEFAULT NULL,
  `emailed_to` varchar(32) DEFAULT NULL,
  `date_sent` datetime NOT NULL DEFAULT '0001-01-01 00:00:00',
  PRIMARY KEY (`unique_id`),
  KEY `idx_coupon_id_zen` (`coupon_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;



--
-- Table structure for table `coupon_gv_customer`
--
DROP TABLE  IF EXISTS `coupon_gv_customer`;
CREATE TABLE `coupon_gv_customer` (
  `customer_id` int(5) NOT NULL DEFAULT '0',
  `amount` decimal(15,4) NOT NULL DEFAULT '0.0000',
  PRIMARY KEY (`customer_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;



--
-- Table structure for table `coupon_gv_queue`
--
DROP TABLE  IF EXISTS `coupon_gv_queue`;
CREATE TABLE `coupon_gv_queue` (
  `unique_id` int(5) NOT NULL AUTO_INCREMENT,
  `customer_id` int(5) NOT NULL DEFAULT '0',
  `order_id` int(5) NOT NULL DEFAULT '0',
  `amount` decimal(15,4) NOT NULL DEFAULT '0.0000',
  `date_created` datetime NOT NULL DEFAULT '0001-01-01 00:00:00',
  `ipaddr` varchar(32) NOT NULL DEFAULT '',
  `release_flag` char(1) NOT NULL DEFAULT 'N',
  PRIMARY KEY (`unique_id`),
  KEY `idx_cust_id_order_id_zen` (`customer_id`,`order_id`),
  KEY `idx_release_flag_zen` (`release_flag`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;



--
-- Table structure for table `coupon_redeem_track`
--
DROP TABLE  IF EXISTS `coupon_redeem_track`;
CREATE TABLE `coupon_redeem_track` (
  `pk` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `coupon_id` int(10) unsigned NOT NULL,
  `customer_id` int(11) unsigned NOT NULL,
  `order_id` int(11) unsigned NOT NULL,
  `redeem_date` datetime NOT NULL,
  PRIMARY KEY (`pk`,`coupon_id`,`customer_id`),
  KEY `fk_coupon_redeem_track_customers1_idx` (`customer_id`),
  KEY `fk_coupon_redeem_track_coupons1_idx` (`coupon_id`),
  KEY `fk_coupon_redeem_track_orders1_idx` (`order_id`),
  CONSTRAINT `fk_coupon_redeem_track_coupons1` FOREIGN KEY (`coupon_id`) REFERENCES `coupons` (`coupon_id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `fk_coupon_redeem_track_customers1` FOREIGN KEY (`customer_id`) REFERENCES `customers` (`customer_id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `fk_coupon_redeem_track_orders1` FOREIGN KEY (`order_id`) REFERENCES `orders` (`order_id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



--
-- Table structure for table `coupon_restrict`
--
DROP TABLE  IF EXISTS `coupon_restrict`;
CREATE TABLE `coupon_restrict` (
  `restrict_id` int(11) NOT NULL AUTO_INCREMENT,
  `coupon_id` int(11) NOT NULL DEFAULT '0',
  `product_id` int(11) NOT NULL DEFAULT '0',
  `category_id` int(11) NOT NULL DEFAULT '0',
  `coupon_restrict` char(1) NOT NULL DEFAULT 'N',
  PRIMARY KEY (`restrict_id`),
  KEY `idx_coup_id_prod_id_zen` (`coupon_id`,`product_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;



--
-- Table structure for table `coupons`
--
DROP TABLE  IF EXISTS `coupons`;
CREATE TABLE `coupons` (
  `coupon_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `store_id` smallint(5) unsigned NOT NULL,
  `code` varchar(45) NOT NULL,
  `discount_type` varchar(15) NOT NULL DEFAULT '(%)',
  `amount` decimal(15,4) NOT NULL DEFAULT '0.0000',
  `minimum_order` decimal(15,4) NOT NULL DEFAULT '1.0000',
  `start_date` datetime NOT NULL,
  `expire_date` datetime NOT NULL,
  `indefinite` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `uses_per_coupon` int(11) NOT NULL DEFAULT '1',
  `uses_per_user` int(11) NOT NULL DEFAULT '1',
  `is_active` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `restrict_to_zone` int(10) unsigned DEFAULT NULL,
  `date_created` datetime NOT NULL,
  `last_modified` datetime NOT NULL,
  PRIMARY KEY (`coupon_id`,`store_id`),
  UNIQUE KEY `code_UNIQUE` (`code`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

INSERT INTO `coupons`  VALUES ( "1","1","XMAS2014","(-)(%)","20.0000","1.0000","2014-12-26 00:00:00","2015-01-10 00:00:00","0","100","1","1","","2014-12-26 16:05:26","2014-12-26 16:05:26");


--
-- Table structure for table `currencies`
--
DROP TABLE  IF EXISTS `currencies`;
CREATE TABLE `currencies` (
  `currency_code` char(3) NOT NULL,
  `currency_name` varchar(255) DEFAULT '',
  `numeric_code` char(3) NOT NULL DEFAULT '000',
  `minor_unit` varchar(5) NOT NULL DEFAULT '2',
  `utf_code` int(11) DEFAULT '0',
  `symbol_left` varchar(24) DEFAULT '',
  `symbol_right` varchar(24) DEFAULT NULL,
  `decimal_point` char(1) DEFAULT NULL,
  `thousands_point` char(1) DEFAULT NULL,
  `rate` float(13,6) DEFAULT NULL,
  `last_updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `active` tinyint(1) NOT NULL DEFAULT '0',
  `is_base_currency` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`currency_code`),
  UNIQUE KEY `currency_code_UNIQUE` (`currency_code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `currencies`  VALUES ( "CAD","Canada Dollar","124","2","36","","","","","0.999528","2012-11-01 16:33:49","1","0");
INSERT INTO `currencies`  VALUES ( "CHF","Switzerland Franc","756","2","67","","","","","0.930986","2012-11-01 16:33:49","1","0");
INSERT INTO `currencies`  VALUES ( "EUR","Euro Member Countries","978","2","8364","","","","","0.770476","2012-11-01 16:33:49","1","0");
INSERT INTO `currencies`  VALUES ( "GBP","United Kingdom Pound","826","2","163","","","","","0.620028","2012-11-01 16:33:49","1","0");
INSERT INTO `currencies`  VALUES ( "GHC","Ghana Cedi","936","2","162","","","","","0.000000","2012-10-26 17:56:38","0","0");
INSERT INTO `currencies`  VALUES ( "HKD","Hong Kong Dollar","344","2","36","","","","","7.749568","2012-11-01 16:33:49","1","0");
INSERT INTO `currencies`  VALUES ( "JPY","Japan Yen","392","2","165","","","","","79.806480","2012-11-01 16:33:49","1","0");
INSERT INTO `currencies`  VALUES ( "NGN","Nigeria Naira","566","2","8358","","","","","157.207993","2012-11-01 16:33:49","1","0");
INSERT INTO `currencies`  VALUES ( "RUB","Russia Ruble","643","2","1088","","","","","31.379187","2012-11-01 16:33:49","1","0");
INSERT INTO `currencies`  VALUES ( "SAR","Saudi Arabia Riyal","682","2","65020","","","","","3.749882","2012-11-01 16:33:50","1","0");
INSERT INTO `currencies`  VALUES ( "USD","United States Dollar","840","2","36","","","","","1.000000","2012-11-01 16:33:50","1","1");
INSERT INTO `currencies`  VALUES ( "XAF","CFA Franc BEAC","950","2","0","","","","","505.868103","2012-11-01 16:33:50","1","0");
INSERT INTO `currencies`  VALUES ( "ZAR","South Africa Rand","710","2","82","","","","","8.660274","2012-11-01 16:33:50","1","0");


--
-- Table structure for table `customer_geoloc_trend`
--
DROP TABLE  IF EXISTS `customer_geoloc_trend`;
CREATE TABLE `customer_geoloc_trend` (
  `pk` bigint(15) unsigned NOT NULL AUTO_INCREMENT,
  `customer_id` int(11) DEFAULT NULL,
  `country_iso2_code` char(2) NOT NULL,
  `city` varchar(45) DEFAULT NULL,
  `login_tally` int(11) unsigned NOT NULL DEFAULT '0',
  `last_login` datetime NOT NULL,
  `date_added` datetime NOT NULL,
  PRIMARY KEY (`pk`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



--
-- Table structure for table `customer_ua_trend`
--
DROP TABLE  IF EXISTS `customer_ua_trend`;
CREATE TABLE `customer_ua_trend` (
  `pk` bigint(15) unsigned NOT NULL AUTO_INCREMENT,
  `customer_id` int(11) unsigned NOT NULL,
  `fingerprint` text NOT NULL,
  `login_tally` int(11) unsigned NOT NULL DEFAULT '0',
  `last_login` datetime NOT NULL,
  `date_added` datetime NOT NULL,
  PRIMARY KEY (`pk`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



--
-- Table structure for table `customers`
--
DROP TABLE  IF EXISTS `customers`;
CREATE TABLE `customers` (
  `customer_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(20) DEFAULT NULL,
  `firstname` varchar(128) NOT NULL DEFAULT '',
  `lastname` varchar(128) NOT NULL DEFAULT '',
  `nickname` varchar(96) NOT NULL DEFAULT '',
  `gender` enum('female','male','undisclosed') NOT NULL DEFAULT 'undisclosed',
  `dob` date NOT NULL DEFAULT '0001-01-01',
  `email_address` varchar(255) NOT NULL DEFAULT '',
  `default_address_id` int(11) NOT NULL DEFAULT '0',
  `telephone` varchar(32) NOT NULL DEFAULT '',
  `fax` varchar(32) DEFAULT NULL,
  `password` varchar(255) NOT NULL DEFAULT '',
  `confirm_digest` varchar(255) DEFAULT NULL,
  `confirm_num_code` varchar(6) DEFAULT '0',
  `newsletter` tinyint(1) NOT NULL DEFAULT '0',
  `group_pricing` int(11) NOT NULL DEFAULT '0',
  `email_format` enum('text','html') NOT NULL DEFAULT 'text',
  `authorization` int(1) NOT NULL DEFAULT '0',
  `customer_referral` varchar(128) NOT NULL DEFAULT '',
  `is_online` varchar(255) NOT NULL DEFAULT '0',
  `number_of_logons` int(5) NOT NULL DEFAULT '0',
  `last_logon` datetime DEFAULT NULL,
  `date_created` datetime NOT NULL,
  `last_modified` datetime NOT NULL,
  PRIMARY KEY (`customer_id`),
  KEY `idx_email_address` (`email_address`),
  KEY `idx_referral` (`customer_referral`(10)),
  KEY `idx_name` (`firstname`,`lastname`),
  KEY `idx_newsletter` (`newsletter`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



--
-- Table structure for table `db_cache`
--
DROP TABLE  IF EXISTS `db_cache`;
CREATE TABLE `db_cache` (
  `cache_entry_name` varchar(64) NOT NULL DEFAULT '',
  `cache_data` mediumblob,
  `cache_entry_created` int(15) DEFAULT NULL,
  PRIMARY KEY (`cache_entry_name`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;



--
-- Table structure for table `description`
--
DROP TABLE  IF EXISTS `description`;
CREATE TABLE `description` (
  `description_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `date_added` datetime NOT NULL,
  PRIMARY KEY (`description_id`)
) ENGINE=InnoDB AUTO_INCREMENT=112 DEFAULT CHARSET=utf8;

INSERT INTO `description`  VALUES ( "1","2014-12-26 03:36:01");
INSERT INTO `description`  VALUES ( "2","2014-12-26 03:36:01");
INSERT INTO `description`  VALUES ( "3","2014-12-26 03:36:01");
INSERT INTO `description`  VALUES ( "4","2014-12-26 03:36:01");
INSERT INTO `description`  VALUES ( "5","2014-12-26 03:36:01");
INSERT INTO `description`  VALUES ( "6","2014-12-26 03:36:01");
INSERT INTO `description`  VALUES ( "7","2014-12-26 03:36:01");
INSERT INTO `description`  VALUES ( "8","2014-12-26 03:36:02");
INSERT INTO `description`  VALUES ( "9","2014-12-26 03:36:02");
INSERT INTO `description`  VALUES ( "10","2014-12-26 03:36:02");
INSERT INTO `description`  VALUES ( "11","2014-12-26 03:36:02");
INSERT INTO `description`  VALUES ( "12","2014-12-26 03:36:02");
INSERT INTO `description`  VALUES ( "13","2014-12-26 03:36:02");
INSERT INTO `description`  VALUES ( "14","2014-12-26 03:36:02");
INSERT INTO `description`  VALUES ( "15","2014-12-26 03:36:02");
INSERT INTO `description`  VALUES ( "16","2014-12-26 03:36:02");
INSERT INTO `description`  VALUES ( "17","2014-12-26 03:36:02");
INSERT INTO `description`  VALUES ( "18","2014-12-26 03:36:02");
INSERT INTO `description`  VALUES ( "19","2014-12-26 03:36:02");
INSERT INTO `description`  VALUES ( "20","2014-12-26 03:36:02");
INSERT INTO `description`  VALUES ( "21","2014-12-26 03:36:02");
INSERT INTO `description`  VALUES ( "22","2014-12-26 03:36:02");
INSERT INTO `description`  VALUES ( "23","2014-12-26 03:36:02");
INSERT INTO `description`  VALUES ( "24","2014-12-26 03:36:02");
INSERT INTO `description`  VALUES ( "25","2014-12-26 03:36:02");
INSERT INTO `description`  VALUES ( "26","2014-12-26 03:36:02");
INSERT INTO `description`  VALUES ( "27","2014-12-26 03:36:02");
INSERT INTO `description`  VALUES ( "28","2014-12-26 03:36:02");
INSERT INTO `description`  VALUES ( "29","2014-12-26 03:36:02");
INSERT INTO `description`  VALUES ( "30","2014-12-26 03:36:02");
INSERT INTO `description`  VALUES ( "31","2014-12-26 03:36:02");
INSERT INTO `description`  VALUES ( "32","2014-12-26 03:36:02");
INSERT INTO `description`  VALUES ( "33","2014-12-26 03:36:02");
INSERT INTO `description`  VALUES ( "34","2014-12-26 03:36:02");
INSERT INTO `description`  VALUES ( "35","2014-12-26 03:36:02");
INSERT INTO `description`  VALUES ( "36","2014-12-26 03:36:02");
INSERT INTO `description`  VALUES ( "37","2014-12-26 03:36:02");
INSERT INTO `description`  VALUES ( "38","2014-12-26 03:36:02");
INSERT INTO `description`  VALUES ( "39","2014-12-26 03:36:02");
INSERT INTO `description`  VALUES ( "40","2014-12-26 03:36:02");
INSERT INTO `description`  VALUES ( "41","2014-12-26 03:36:02");
INSERT INTO `description`  VALUES ( "42","2014-12-26 03:36:02");
INSERT INTO `description`  VALUES ( "43","2014-12-26 03:36:02");
INSERT INTO `description`  VALUES ( "44","2014-12-26 03:36:02");
INSERT INTO `description`  VALUES ( "45","2014-12-26 03:36:02");
INSERT INTO `description`  VALUES ( "46","2014-12-26 03:36:02");
INSERT INTO `description`  VALUES ( "47","2014-12-26 03:36:02");
INSERT INTO `description`  VALUES ( "48","2014-12-26 03:36:02");
INSERT INTO `description`  VALUES ( "49","2014-12-26 03:36:02");
INSERT INTO `description`  VALUES ( "50","2014-12-26 03:36:02");
INSERT INTO `description`  VALUES ( "51","2014-12-26 03:36:02");
INSERT INTO `description`  VALUES ( "52","2014-12-26 03:36:02");
INSERT INTO `description`  VALUES ( "53","2014-12-26 03:36:02");
INSERT INTO `description`  VALUES ( "54","2014-12-26 03:36:02");
INSERT INTO `description`  VALUES ( "55","2014-12-26 03:36:02");
INSERT INTO `description`  VALUES ( "56","2014-12-26 03:36:02");
INSERT INTO `description`  VALUES ( "57","2014-12-26 03:36:02");
INSERT INTO `description`  VALUES ( "58","2014-12-26 03:36:02");
INSERT INTO `description`  VALUES ( "59","2014-12-26 03:36:02");
INSERT INTO `description`  VALUES ( "60","2014-12-26 03:36:02");
INSERT INTO `description`  VALUES ( "61","2014-12-26 03:36:02");
INSERT INTO `description`  VALUES ( "62","2014-12-26 03:36:02");
INSERT INTO `description`  VALUES ( "63","2014-12-26 03:36:02");
INSERT INTO `description`  VALUES ( "64","2014-12-26 03:36:02");
INSERT INTO `description`  VALUES ( "65","2014-12-26 03:36:02");
INSERT INTO `description`  VALUES ( "66","2014-12-26 03:36:02");
INSERT INTO `description`  VALUES ( "67","2014-12-26 03:36:02");
INSERT INTO `description`  VALUES ( "68","2014-12-26 03:36:02");
INSERT INTO `description`  VALUES ( "69","2014-12-26 03:36:02");
INSERT INTO `description`  VALUES ( "70","2014-12-26 03:36:02");
INSERT INTO `description`  VALUES ( "71","2014-12-26 03:36:02");
INSERT INTO `description`  VALUES ( "72","2014-12-26 03:36:02");
INSERT INTO `description`  VALUES ( "73","2014-12-26 03:36:02");
INSERT INTO `description`  VALUES ( "74","2014-12-26 03:36:02");
INSERT INTO `description`  VALUES ( "75","2014-12-26 03:36:02");
INSERT INTO `description`  VALUES ( "76","2014-12-26 03:36:02");
INSERT INTO `description`  VALUES ( "77","2014-12-26 03:36:55");
INSERT INTO `description`  VALUES ( "78","2014-12-26 03:36:55");
INSERT INTO `description`  VALUES ( "79","2014-12-26 03:36:55");
INSERT INTO `description`  VALUES ( "80","2014-12-26 03:36:55");
INSERT INTO `description`  VALUES ( "81","2014-12-26 03:36:55");
INSERT INTO `description`  VALUES ( "82","2014-12-26 03:36:55");
INSERT INTO `description`  VALUES ( "83","2014-12-26 03:36:55");
INSERT INTO `description`  VALUES ( "84","2014-12-26 03:36:55");
INSERT INTO `description`  VALUES ( "85","2014-12-26 03:36:55");
INSERT INTO `description`  VALUES ( "86","2014-12-26 03:36:55");
INSERT INTO `description`  VALUES ( "87","2014-12-26 03:36:55");
INSERT INTO `description`  VALUES ( "88","2014-12-26 03:36:55");
INSERT INTO `description`  VALUES ( "89","2014-12-26 03:36:55");
INSERT INTO `description`  VALUES ( "90","2014-12-26 03:36:55");
INSERT INTO `description`  VALUES ( "91","2014-12-26 03:36:55");
INSERT INTO `description`  VALUES ( "92","2014-12-26 03:36:55");
INSERT INTO `description`  VALUES ( "93","2014-12-26 03:36:55");
INSERT INTO `description`  VALUES ( "94","2014-12-26 03:36:55");
INSERT INTO `description`  VALUES ( "95","2014-12-26 03:36:55");
INSERT INTO `description`  VALUES ( "96","2014-12-26 03:36:55");
INSERT INTO `description`  VALUES ( "97","2014-12-26 03:36:55");
INSERT INTO `description`  VALUES ( "98","2014-12-26 03:36:55");
INSERT INTO `description`  VALUES ( "99","2014-12-26 03:36:55");
INSERT INTO `description`  VALUES ( "103","2014-12-26 15:42:41");
INSERT INTO `description`  VALUES ( "105","2014-12-26 16:15:17");
INSERT INTO `description`  VALUES ( "110","2014-12-27 03:47:56");
INSERT INTO `description`  VALUES ( "111","2014-12-27 07:46:44");


--
-- Table structure for table `description_content`
--
DROP TABLE  IF EXISTS `description_content`;
CREATE TABLE `description_content` (
  `description_id` bigint(20) unsigned NOT NULL,
  `language_code` char(2) NOT NULL,
  `title` varchar(255) NOT NULL,
  `text` mediumtext,
  `last_modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`language_code`,`description_id`),
  KEY `fk_products_description_languages1_idx` (`language_code`),
  KEY `fk_description_content_description1_idx` (`description_id`),
  KEY `content_description1_title_idx` (`title`),
  CONSTRAINT `fk_description_content_description1` FOREIGN KEY (`description_id`) REFERENCES `description` (`description_id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `fk_products_description_languages10` FOREIGN KEY (`language_code`) REFERENCES `languages` (`language_code`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `description_content`  VALUES ( "1","EN","Administrator","Administrator (super user)","2014-12-26 03:36:01");
INSERT INTO `description_content`  VALUES ( "2","EN","Store Owner","Store Owner","2014-12-26 03:36:01");
INSERT INTO `description_content`  VALUES ( "3","EN","Store Manager","Store Manager","2014-12-26 03:36:01");
INSERT INTO `description_content`  VALUES ( "4","EN","Attendant","Store Attendant","2014-12-26 03:36:01");
INSERT INTO `description_content`  VALUES ( "5","EN","Super User","Super User","2014-12-26 03:36:01");
INSERT INTO `description_content`  VALUES ( "6","EN","View Customer Account","View Customer Account","2014-12-26 03:36:01");
INSERT INTO `description_content`  VALUES ( "7","EN","Edit Customer Account","Edit Customer Account","2014-12-26 03:36:02");
INSERT INTO `description_content`  VALUES ( "8","EN","Create Customer Account","Create Customer Account","2014-12-26 03:36:02");
INSERT INTO `description_content`  VALUES ( "9","EN","Delete Customer Account","Delete Customer Account","2014-12-26 03:36:02");
INSERT INTO `description_content`  VALUES ( "10","EN","View Orders","View Orders","2014-12-26 03:36:02");
INSERT INTO `description_content`  VALUES ( "11","EN","Edit Orders","Edit Orders","2014-12-26 03:36:02");
INSERT INTO `description_content`  VALUES ( "12","EN","Create Orders","Create Orders","2014-12-26 03:36:02");
INSERT INTO `description_content`  VALUES ( "13","EN","Delete Orders","Delete Orders","2014-12-26 03:36:02");
INSERT INTO `description_content`  VALUES ( "14","EN","Process Orders","Process Orders","2014-12-26 03:36:02");
INSERT INTO `description_content`  VALUES ( "15","EN","Cancel Orders","Cancel Orders","2014-12-26 03:36:02");
INSERT INTO `description_content`  VALUES ( "16","EN","Close Orders","Close Orders","2014-12-26 03:36:02");
INSERT INTO `description_content`  VALUES ( "17","EN","View Stores","View Stores","2014-12-26 03:36:02");
INSERT INTO `description_content`  VALUES ( "18","EN","View Store","View Store","2014-12-26 03:36:02");
INSERT INTO `description_content`  VALUES ( "19","EN","Edit Store","Edit Store","2014-12-26 03:36:02");
INSERT INTO `description_content`  VALUES ( "20","EN","Delete Store","Delete Store","2014-12-26 03:36:02");
INSERT INTO `description_content`  VALUES ( "21","EN","Create Store","Create Store","2014-12-26 03:36:02");
INSERT INTO `description_content`  VALUES ( "22","EN","Change Store Status","Change Store Status","2014-12-26 03:36:02");
INSERT INTO `description_content`  VALUES ( "23","EN","View Admin Profiles","View Admin Profiles","2014-12-26 03:36:02");
INSERT INTO `description_content`  VALUES ( "24","EN","Edit Admin Profiles","Edit Admin Profiles","2014-12-26 03:36:02");
INSERT INTO `description_content`  VALUES ( "25","EN","Delete Admin Profiles","Delete Admin Profiles","2014-12-26 03:36:02");
INSERT INTO `description_content`  VALUES ( "26","EN","Create Admin Profiles","Create Admin Profiles","2014-12-26 03:36:02");
INSERT INTO `description_content`  VALUES ( "27","EN","View Admininistrator Account","View Admininistrator Account","2014-12-26 03:36:02");
INSERT INTO `description_content`  VALUES ( "28","EN","Edit Admininistrator Account","Edit Admininistrator Account","2014-12-26 03:36:02");
INSERT INTO `description_content`  VALUES ( "29","EN","Delete Admininistrator Account","Delete Admininistrator Account","2014-12-26 03:36:02");
INSERT INTO `description_content`  VALUES ( "30","EN","Create Admininistrator Account","Create Admininistrator Account","2014-12-26 03:36:02");
INSERT INTO `description_content`  VALUES ( "31","EN","View Advanced Settings","View Advanced Settings","2014-12-26 03:36:02");
INSERT INTO `description_content`  VALUES ( "32","EN","Edit Advanced Settings","Edit Advanced Settings","2014-12-26 03:36:02");
INSERT INTO `description_content`  VALUES ( "33","EN","Delete Advanced Settings","Delete Advanced Settings","2014-12-26 03:36:02");
INSERT INTO `description_content`  VALUES ( "34","EN","Create Advanced Settings","Create Advanced Settings","2014-12-26 03:36:02");
INSERT INTO `description_content`  VALUES ( "35","EN","View Store Owner Account","View Store Owner Account","2014-12-26 03:36:02");
INSERT INTO `description_content`  VALUES ( "36","EN","Edit Store Owner Account","Edit Store Owner Account","2014-12-26 03:36:02");
INSERT INTO `description_content`  VALUES ( "37","EN","Delete Store Owner Account","Delete Store Owner Account","2014-12-26 03:36:02");
INSERT INTO `description_content`  VALUES ( "38","EN","Create Store Owner Account","Create Store Owner Account","2014-12-26 03:36:02");
INSERT INTO `description_content`  VALUES ( "39","EN","View Manager Account","View Manager Account","2014-12-26 03:36:02");
INSERT INTO `description_content`  VALUES ( "40","EN","Edit Manager Account","Edit Manager Account","2014-12-26 03:36:02");
INSERT INTO `description_content`  VALUES ( "41","EN","Delete Manager Account","Delete Manager Account","2014-12-26 03:36:02");
INSERT INTO `description_content`  VALUES ( "42","EN","Create Manager Account","Create Manager Account","2014-12-26 03:36:02");
INSERT INTO `description_content`  VALUES ( "43","EN","View Employee Account","View Employee Account","2014-12-26 03:36:02");
INSERT INTO `description_content`  VALUES ( "44","EN","Edit Employee Account","Edit Employee Account","2014-12-26 03:36:02");
INSERT INTO `description_content`  VALUES ( "45","EN","Delete Employee Account","Delete Employee Account","2014-12-26 03:36:02");
INSERT INTO `description_content`  VALUES ( "46","EN","Create Employee Account","Create Employee Account","2014-12-26 03:36:02");
INSERT INTO `description_content`  VALUES ( "47","EN","View Catalog","View Catalog","2014-12-26 03:36:02");
INSERT INTO `description_content`  VALUES ( "48","EN","Edit Catalog","Edit Catalog","2014-12-26 03:36:02");
INSERT INTO `description_content`  VALUES ( "49","EN","View Pproduct","View Pproduct","2014-12-26 03:36:02");
INSERT INTO `description_content`  VALUES ( "50","EN","Edit Product","Edit Product","2014-12-26 03:36:02");
INSERT INTO `description_content`  VALUES ( "51","EN","Delete Product","Delete Product","2014-12-26 03:36:02");
INSERT INTO `description_content`  VALUES ( "52","EN","Create Product","Create Product","2014-12-26 03:36:02");
INSERT INTO `description_content`  VALUES ( "53","EN","Set Product Price","Set Product Price","2014-12-26 03:36:02");
INSERT INTO `description_content`  VALUES ( "54","EN","Set Product Stock","Set Product Stock","2014-12-26 03:36:02");
INSERT INTO `description_content`  VALUES ( "55","EN","View Countries","View Countries","2014-12-26 03:36:02");
INSERT INTO `description_content`  VALUES ( "56","EN","Edit Countries","Edit Countries","2014-12-26 03:36:02");
INSERT INTO `description_content`  VALUES ( "57","EN","View Provinces","View Provinces","2014-12-26 03:36:02");
INSERT INTO `description_content`  VALUES ( "58","EN","Edit Provinces","Edit Provinces","2014-12-26 03:36:02");
INSERT INTO `description_content`  VALUES ( "59","EN","Delete Provinces","Delete Provinces","2014-12-26 03:36:02");
INSERT INTO `description_content`  VALUES ( "60","EN","Create Provinces","Create Provinces","2014-12-26 03:36:02");
INSERT INTO `description_content`  VALUES ( "61","EN","View Tax Classes","View Tax Classes","2014-12-26 03:36:02");
INSERT INTO `description_content`  VALUES ( "62","EN","Edit Tax Classes","Edit Tax Classes","2014-12-26 03:36:02");
INSERT INTO `description_content`  VALUES ( "63","EN","Create Tax Classes","Create Tax Classes","2014-12-26 03:36:02");
INSERT INTO `description_content`  VALUES ( "64","EN","Delete Tax Classes","Delete Tax Classes","2014-12-26 03:36:02");
INSERT INTO `description_content`  VALUES ( "65","EN","View Tax Rates","View Tax Rates","2014-12-26 03:36:02");
INSERT INTO `description_content`  VALUES ( "66","EN","Edit Tax Rates","Edit Tax Rates","2014-12-26 03:36:02");
INSERT INTO `description_content`  VALUES ( "67","EN","Create Tax Rates","Create Tax Rates","2014-12-26 03:36:02");
INSERT INTO `description_content`  VALUES ( "68","EN","Delete Tax Rates","Delete Tax Rates","2014-12-26 03:36:02");
INSERT INTO `description_content`  VALUES ( "69","EN","Low Stock","Product stock is low.","2014-12-26 03:36:02");
INSERT INTO `description_content`  VALUES ( "70","EN","Products out of stock.","Some products out of stock.","2014-12-26 03:36:02");
INSERT INTO `description_content`  VALUES ( "71","EN","New customers.","You have new customers.","2014-12-26 03:36:02");
INSERT INTO `description_content`  VALUES ( "72","EN","Transaction Error.","There're one or more transaction errors.","2014-12-26 03:36:02");
INSERT INTO `description_content`  VALUES ( "73","EN","Failed Logins.","There's been one or more failed logins.","2014-12-26 03:36:02");
INSERT INTO `description_content`  VALUES ( "74","EN","Account Deleted","An admin acount has just been deleted.","2014-12-26 03:36:02");
INSERT INTO `description_content`  VALUES ( "75","EN","Account Created","A new admin acount has just been created.","2014-12-26 03:36:02");
INSERT INTO `description_content`  VALUES ( "76","EN","Store Created","A new store has just been created.","2014-12-26 03:36:02");
INSERT INTO `description_content`  VALUES ( "77","EN","UK Shipping Agent","For UK and international shipping.","2014-12-26 03:36:55");
INSERT INTO `description_content`  VALUES ( "78","EN","Lagos Shipping Agent","For Lagos shipping.","2014-12-26 03:36:55");
INSERT INTO `description_content`  VALUES ( "79","EN","UK VAT","No description","2014-12-26 03:36:55");
INSERT INTO `description_content`  VALUES ( "80","EN","NG VAT","No description","2014-12-26 03:36:55");
INSERT INTO `description_content`  VALUES ( "81","EN","UK Shipping Tax","No description","2014-12-26 03:36:55");
INSERT INTO `description_content`  VALUES ( "82","EN","NG Shipping Tax","No description","2014-12-26 03:36:55");
INSERT INTO `description_content`  VALUES ( "83","EN","Aimas NG","Aimas NG Store","2014-12-26 03:36:55");
INSERT INTO `description_content`  VALUES ( "84","EN","New Orders (Aimas NG)","You have new orders (at Aimas NG).","2014-12-26 03:36:55");
INSERT INTO `description_content`  VALUES ( "85","EN","Unconfirmed Deliveries (Aimas NG)","You have unconfirmed deliveries (at Aimas NG).","2014-12-26 03:36:55");
INSERT INTO `description_content`  VALUES ( "86","EN","Aimas UK","Aimas UK Store","2014-12-26 03:36:55");
INSERT INTO `description_content`  VALUES ( "87","EN","New Orders (Aimas UK)","You have new orders (at Aimas UK).","2014-12-26 03:36:55");
INSERT INTO `description_content`  VALUES ( "88","EN","Unconfirmed Deliveries (Aimas UK)","You have unconfirmed deliveries (at Aimas UK).","2014-12-26 03:36:55");
INSERT INTO `description_content`  VALUES ( "89","EN","Standard Delivery","Delivery within 3-5 working days. Cost is N800.","2014-12-26 03:36:55");
INSERT INTO `description_content`  VALUES ( "90","EN","Next-Day Delivery","Order must be placed by 3pm. A signature is required on delivery. Cost is N1200.","2014-12-26 03:36:55");
INSERT INTO `description_content`  VALUES ( "91","EN","Same-Day Delivery","Order must be placed by 12 noon. A signature is required on delivery. Cost is N1500.","2014-12-26 03:36:55");
INSERT INTO `description_content`  VALUES ( "92","EN","UK Standard Delivery","Delivery within 1 – 3 working days. Cost is £4","2014-12-26 03:36:55");
INSERT INTO `description_content`  VALUES ( "93","EN","UK Next-Day Delivery","Order must be placed by 3pm. A signature is required on delivery. Cost is £6.","2014-12-26 03:36:55");
INSERT INTO `description_content`  VALUES ( "94","EN","Outside the UK","Delivery within 3 – 8 days. A signature is required on delivery. Cost is £10.","2014-12-26 03:36:55");
INSERT INTO `description_content`  VALUES ( "95","EN","Women","Women's Clothing","2014-12-26 03:36:55");
INSERT INTO `description_content`  VALUES ( "96","EN","Chicy Aimas T-Shirts","Chicy t-shirts for summer!","2014-12-26 03:36:55");
INSERT INTO `description_content`  VALUES ( "97","EN","Chicy Clutch Handbags","Chicy aimas leather handbags!","2014-12-26 03:36:55");
INSERT INTO `description_content`  VALUES ( "98","EN","Chicy Pink Tee","Chicy Pink Tee.","2014-12-26 03:36:55");
INSERT INTO `description_content`  VALUES ( "99","EN","Blue Summer Top","Blue Summer Top.","2014-12-26 03:36:55");
INSERT INTO `description_content`  VALUES ( "103","EN","Black Friday 2014","1","2014-12-26 15:42:41");
INSERT INTO `description_content`  VALUES ( "105","EN","Shoes","Fuck me shoes!","2014-12-26 16:16:05");
INSERT INTO `description_content`  VALUES ( "110","EN","Pink Leather Clutch","Quality leather handbag.","2014-12-27 03:48:18");
INSERT INTO `description_content`  VALUES ( "111","EN","Ankara Dress","Ankara Dress","2014-12-27 07:47:07");


--
-- Table structure for table `email_archive`
--
DROP TABLE  IF EXISTS `email_archive`;
CREATE TABLE `email_archive` (
  `archive_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `message_id` varchar(255) NOT NULL,
  `sender` varchar(255) NOT NULL,
  `to_name` varchar(255) NOT NULL DEFAULT '',
  `to_address` varchar(255) NOT NULL DEFAULT '',
  `from_name` varchar(255) NOT NULL DEFAULT '',
  `from_address` varchar(255) NOT NULL DEFAULT '',
  `subject` varchar(255) NOT NULL DEFAULT '',
  `html` text NOT NULL,
  `text` text NOT NULL,
  `date_sent` datetime NOT NULL,
  `module` varchar(64) NOT NULL DEFAULT '',
  PRIMARY KEY (`archive_id`),
  KEY `idx_email_to_address` (`to_address`),
  KEY `idx_module` (`module`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



--
-- Table structure for table `failed_logins_hourly`
--
DROP TABLE  IF EXISTS `failed_logins_hourly`;
CREATE TABLE `failed_logins_hourly` (
  `pk` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `tally_admin` int(10) unsigned NOT NULL DEFAULT '0',
  `tally_customers` int(10) unsigned NOT NULL DEFAULT '0',
  `date_added` datetime NOT NULL,
  PRIMARY KEY (`pk`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



--
-- Table structure for table `failed_logins_monthly`
--
DROP TABLE  IF EXISTS `failed_logins_monthly`;
CREATE TABLE `failed_logins_monthly` (
  `pk` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `tally_admin` int(10) unsigned NOT NULL DEFAULT '0',
  `tally_customers` int(10) unsigned NOT NULL DEFAULT '0',
  `date_added` datetime NOT NULL,
  PRIMARY KEY (`pk`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



--
-- Table structure for table `failed_logins_x3month`
--
DROP TABLE  IF EXISTS `failed_logins_x3month`;
CREATE TABLE `failed_logins_x3month` (
  `pk` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `tally_admin` int(10) unsigned NOT NULL DEFAULT '0',
  `tally_customers` int(10) unsigned NOT NULL DEFAULT '0',
  `date_added` datetime NOT NULL,
  PRIMARY KEY (`pk`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



--
-- Table structure for table `featured`
--
DROP TABLE  IF EXISTS `featured`;
CREATE TABLE `featured` (
  `featured_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `product_id` int(11) unsigned NOT NULL DEFAULT '0',
  `featured_date_added` datetime NOT NULL,
  `featured_last_modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `expires_date` date NOT NULL DEFAULT '1000-01-01',
  `date_status_change` datetime DEFAULT NULL,
  `status` int(1) NOT NULL DEFAULT '1',
  `featured_date_available` date NOT NULL DEFAULT '1000-01-01',
  PRIMARY KEY (`featured_id`,`product_id`),
  KEY `idx_status_zen` (`status`),
  KEY `idx_products_id_zen` (`product_id`),
  KEY `idx_date_avail_zen` (`featured_date_available`),
  KEY `idx_expires_date_zen` (`expires_date`),
  KEY `fk_product_id_idx` (`product_id`),
  CONSTRAINT `fk_product_id0` FOREIGN KEY (`product_id`) REFERENCES `products` (`product_id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



--
-- Table structure for table `fulltext_catalog_index_isam`
--
DROP TABLE  IF EXISTS `fulltext_catalog_index_isam`;
CREATE TABLE `fulltext_catalog_index_isam` (
  `product_id` int(11) unsigned NOT NULL,
  `keywords` text,
  PRIMARY KEY (`product_id`),
  FULLTEXT KEY `idx_data_index` (`keywords`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

INSERT INTO `fulltext_catalog_index_isam`  VALUES ( "8","Pink Leather Clutch  ");
INSERT INTO `fulltext_catalog_index_isam`  VALUES ( "9","Ankara Dress  ");


--
-- Table structure for table `fulltext_order_index_isam`
--
DROP TABLE  IF EXISTS `fulltext_order_index_isam`;
CREATE TABLE `fulltext_order_index_isam` (
  `order_id` int(11) unsigned NOT NULL,
  `keywords` text,
  PRIMARY KEY (`order_id`),
  FULLTEXT KEY `idx_data_index` (`keywords`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;



--
-- Table structure for table `group_pricing`
--
DROP TABLE  IF EXISTS `group_pricing`;
CREATE TABLE `group_pricing` (
  `group_id` int(11) NOT NULL AUTO_INCREMENT,
  `group_name` varchar(32) NOT NULL DEFAULT '',
  `group_percentage` decimal(5,2) NOT NULL DEFAULT '0.00',
  `last_modified` datetime DEFAULT NULL,
  `date_added` datetime NOT NULL DEFAULT '0001-01-01 00:00:00',
  PRIMARY KEY (`group_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;



--
-- Table structure for table `help_topics`
--
DROP TABLE  IF EXISTS `help_topics`;
CREATE TABLE `help_topics` (
  `topic_id` varchar(255) NOT NULL,
  `language_code` char(2) NOT NULL,
  `topic` varchar(255) NOT NULL,
  `short_text` text NOT NULL,
  `long_text` mediumtext NOT NULL,
  `full_text` mediumtext NOT NULL,
  `date_added` datetime NOT NULL,
  `last_modified` datetime NOT NULL,
  PRIMARY KEY (`topic_id`),
  UNIQUE KEY `topic_id_UNIQUE` (`topic_id`),
  KEY `fk_help_topics_languages1_idx` (`language_code`),
  CONSTRAINT `fk_help_topics_languages1` FOREIGN KEY (`language_code`) REFERENCES `languages` (`language_code`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



--
-- Table structure for table `images`
--
DROP TABLE  IF EXISTS `images`;
CREATE TABLE `images` (
  `image_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `description_id` bigint(20) unsigned DEFAULT NULL,
  `image_type` tinyint(1) unsigned NOT NULL DEFAULT '3',
  `image_file` varchar(60) NOT NULL DEFAULT '',
  `date_added` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `hatched` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`image_id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;

INSERT INTO `images`  VALUES ( "1","","2","img_1.jpeg","2014-12-26 05:25:22","1");
INSERT INTO `images`  VALUES ( "2","","2","img_2.jpeg","2014-12-26 05:41:25","1");
INSERT INTO `images`  VALUES ( "3","","2","img_3.jpeg","2014-12-27 03:27:26","1");
INSERT INTO `images`  VALUES ( "4","","2","img_4.jpeg","2014-12-27 03:43:13","1");
INSERT INTO `images`  VALUES ( "5","","2","img_5.jpeg","2014-12-27 03:45:52","1");
INSERT INTO `images`  VALUES ( "6","","2","img_6.jpeg","2014-12-27 03:48:23","1");
INSERT INTO `images`  VALUES ( "7","","2","img_7.jpeg","2014-12-27 07:46:02","1");
INSERT INTO `images`  VALUES ( "8","","2","img_8.jpeg","2014-12-27 07:47:10","1");
INSERT INTO `images`  VALUES ( "9","","2","img_9.jpeg","2014-12-27 07:49:21","1");


--
-- Table structure for table `languages`
--
DROP TABLE  IF EXISTS `languages`;
CREATE TABLE `languages` (
  `language_code` char(2) NOT NULL,
  `name` varchar(32) NOT NULL DEFAULT '',
  `image` varchar(64) DEFAULT NULL,
  `directory` varchar(32) DEFAULT NULL,
  `sort_order` int(3) DEFAULT NULL,
  PRIMARY KEY (`language_code`),
  UNIQUE KEY `code_UNIQUE` (`language_code`),
  KEY `idx_languages_name_zen` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `languages`  VALUES ( "AA","Afar","","","");
INSERT INTO `languages`  VALUES ( "AB","Abkhaz","","","");
INSERT INTO `languages`  VALUES ( "AE","Avestan","","","");
INSERT INTO `languages`  VALUES ( "AF","Afrikaans","","","");
INSERT INTO `languages`  VALUES ( "AK","Akan","","","");
INSERT INTO `languages`  VALUES ( "AM","Amharic","","","");
INSERT INTO `languages`  VALUES ( "AN","Aragonese","","","");
INSERT INTO `languages`  VALUES ( "AR","Arabic","","","");
INSERT INTO `languages`  VALUES ( "AS","Assamese","","","");
INSERT INTO `languages`  VALUES ( "AV","Avaric","","","");
INSERT INTO `languages`  VALUES ( "AY","Aymara","","","");
INSERT INTO `languages`  VALUES ( "AZ","Azerbaijani","","","");
INSERT INTO `languages`  VALUES ( "BA","Bashkir","","","");
INSERT INTO `languages`  VALUES ( "BE","Belarusian","","","");
INSERT INTO `languages`  VALUES ( "BG","Bulgarian","","","");
INSERT INTO `languages`  VALUES ( "BH","Bihari","","","");
INSERT INTO `languages`  VALUES ( "BI","Bislama","","","");
INSERT INTO `languages`  VALUES ( "BM","Bambara","","","");
INSERT INTO `languages`  VALUES ( "BN","Bengali","","","");
INSERT INTO `languages`  VALUES ( "BO","Tibetan Standard","","","");
INSERT INTO `languages`  VALUES ( "BR","Breton","","","");
INSERT INTO `languages`  VALUES ( "BS","Bosnian","","","");
INSERT INTO `languages`  VALUES ( "CA","Catalan","","","");
INSERT INTO `languages`  VALUES ( "CE","Chechen","","","");
INSERT INTO `languages`  VALUES ( "CH","Chamorro","","","");
INSERT INTO `languages`  VALUES ( "CO","Corsican","","","");
INSERT INTO `languages`  VALUES ( "CR","Cree","","","");
INSERT INTO `languages`  VALUES ( "CS","Czech","","","");
INSERT INTO `languages`  VALUES ( "CU","Old Church Slavonic","","","");
INSERT INTO `languages`  VALUES ( "CV","Chuvash","","","");
INSERT INTO `languages`  VALUES ( "CY","Welsh","","","");
INSERT INTO `languages`  VALUES ( "DA","Danish","","","");
INSERT INTO `languages`  VALUES ( "DE","German","","","");
INSERT INTO `languages`  VALUES ( "DV","Divehi","","","");
INSERT INTO `languages`  VALUES ( "DZ","Dzongkha","","","");
INSERT INTO `languages`  VALUES ( "EE","Ewe","","","");
INSERT INTO `languages`  VALUES ( "EL","Greek","","","");
INSERT INTO `languages`  VALUES ( "EN","English","","","");
INSERT INTO `languages`  VALUES ( "EO","Esperanto","","","");
INSERT INTO `languages`  VALUES ( "ES","Spanish","","","");
INSERT INTO `languages`  VALUES ( "ET","Estonian","","","");
INSERT INTO `languages`  VALUES ( "EU","Basque","","","");
INSERT INTO `languages`  VALUES ( "FA","Persian","","","");
INSERT INTO `languages`  VALUES ( "FF","Fula","","","");
INSERT INTO `languages`  VALUES ( "FI","Finnish","","","");
INSERT INTO `languages`  VALUES ( "FJ","Fijian","","","");
INSERT INTO `languages`  VALUES ( "FO","Faroese","","","");
INSERT INTO `languages`  VALUES ( "FR","French","","","");
INSERT INTO `languages`  VALUES ( "FY","Western Frisian","","","");
INSERT INTO `languages`  VALUES ( "GA","Irish","","","");
INSERT INTO `languages`  VALUES ( "GD","Scottish Gaelic","","","");
INSERT INTO `languages`  VALUES ( "GL","Galician","","","");
INSERT INTO `languages`  VALUES ( "GN","GuaranÃÂ­","","","");
INSERT INTO `languages`  VALUES ( "GU","Gujarati","","","");
INSERT INTO `languages`  VALUES ( "GV","Manx","","","");
INSERT INTO `languages`  VALUES ( "HA","Hausa","","","");
INSERT INTO `languages`  VALUES ( "HE","Hebrew","","","");
INSERT INTO `languages`  VALUES ( "HI","Hindi","","","");
INSERT INTO `languages`  VALUES ( "HO","Hiri Motu","","","");
INSERT INTO `languages`  VALUES ( "HR","Croatian","","","");
INSERT INTO `languages`  VALUES ( "HT","Haitian","","","");
INSERT INTO `languages`  VALUES ( "HU","Hungarian","","","");
INSERT INTO `languages`  VALUES ( "HY","Armenian","","","");
INSERT INTO `languages`  VALUES ( "HZ","Herero","","","");
INSERT INTO `languages`  VALUES ( "IA","Interlingua","","","");
INSERT INTO `languages`  VALUES ( "ID","Indonesian","","","");
INSERT INTO `languages`  VALUES ( "IE","Interlingue","","","");
INSERT INTO `languages`  VALUES ( "IG","Igbo","","","");
INSERT INTO `languages`  VALUES ( "II","Nuosu","","","");
INSERT INTO `languages`  VALUES ( "IK","Inupiaq","","","");
INSERT INTO `languages`  VALUES ( "IO","Ido","","","");
INSERT INTO `languages`  VALUES ( "IS","Icelandic","","","");
INSERT INTO `languages`  VALUES ( "IT","Italian","","","");
INSERT INTO `languages`  VALUES ( "IU","Inuktitut","","","");
INSERT INTO `languages`  VALUES ( "JA","Japanese","","","");
INSERT INTO `languages`  VALUES ( "JV","Javanese","","","");
INSERT INTO `languages`  VALUES ( "KA","Georgian","","","");
INSERT INTO `languages`  VALUES ( "KG","Kongo","","","");
INSERT INTO `languages`  VALUES ( "KI","Kikuyu","","","");
INSERT INTO `languages`  VALUES ( "KJ","Kwanyama","","","");
INSERT INTO `languages`  VALUES ( "KK","Kazakh","","","");
INSERT INTO `languages`  VALUES ( "KL","Kalaallisut","","","");
INSERT INTO `languages`  VALUES ( "KM","Khmer","","","");
INSERT INTO `languages`  VALUES ( "KN","Kannada","","","");
INSERT INTO `languages`  VALUES ( "KO","Korean","","","");
INSERT INTO `languages`  VALUES ( "KR","Kanuri","","","");
INSERT INTO `languages`  VALUES ( "KS","Kashmiri","","","");
INSERT INTO `languages`  VALUES ( "KU","Kurdish","","","");
INSERT INTO `languages`  VALUES ( "KV","Komi","","","");
INSERT INTO `languages`  VALUES ( "KW","Cornish","","","");
INSERT INTO `languages`  VALUES ( "KY","Kirghiz","","","");
INSERT INTO `languages`  VALUES ( "LA","Latin","","","");
INSERT INTO `languages`  VALUES ( "LB","Luxembourgish","","","");
INSERT INTO `languages`  VALUES ( "LG","Luganda","","","");
INSERT INTO `languages`  VALUES ( "LI","Limburgish","","","");
INSERT INTO `languages`  VALUES ( "LN","Lingala","","","");
INSERT INTO `languages`  VALUES ( "LO","Lao","","","");
INSERT INTO `languages`  VALUES ( "LT","Lithuanian","","","");
INSERT INTO `languages`  VALUES ( "LU","Luba-Katanga","","","");
INSERT INTO `languages`  VALUES ( "LV","Latvian","","","");
INSERT INTO `languages`  VALUES ( "MG","Malagasy","","","");
INSERT INTO `languages`  VALUES ( "MH","Marshallese","","","");
INSERT INTO `languages`  VALUES ( "MI","MÃ"Âori","","","");
INSERT INTO `languages`  VALUES ( "MK","Macedonian","","","");
INSERT INTO `languages`  VALUES ( "ML","Malayalam","","","");
INSERT INTO `languages`  VALUES ( "MN","Mongolian","","","");
INSERT INTO `languages`  VALUES ( "MR","Marathi","","","");
INSERT INTO `languages`  VALUES ( "MS","Malay","","","");
INSERT INTO `languages`  VALUES ( "MT","Maltese","","","");
INSERT INTO `languages`  VALUES ( "MY","Burmese","","","");
INSERT INTO `languages`  VALUES ( "NA","Nauru","","","");
INSERT INTO `languages`  VALUES ( "NB","Norwegian BokmÃÂ¥l","","","");
INSERT INTO `languages`  VALUES ( "ND","North Ndebele","","","");
INSERT INTO `languages`  VALUES ( "NE","Nepali","","","");
INSERT INTO `languages`  VALUES ( "NG","Ndonga","","","");
INSERT INTO `languages`  VALUES ( "NL","Dutch","","","");
INSERT INTO `languages`  VALUES ( "NN","Norwegian Nynorsk","","","");
INSERT INTO `languages`  VALUES ( "NO","Norwegian","","","");
INSERT INTO `languages`  VALUES ( "NR","South Ndebele","","","");
INSERT INTO `languages`  VALUES ( "NV","Navajo","","","");
INSERT INTO `languages`  VALUES ( "NY","Chichewa","","","");
INSERT INTO `languages`  VALUES ( "OC","Occitan","","","");
INSERT INTO `languages`  VALUES ( "OJ","Ojibwe","","","");
INSERT INTO `languages`  VALUES ( "OM","Oromo","","","");
INSERT INTO `languages`  VALUES ( "OR","Oriya","","","");
INSERT INTO `languages`  VALUES ( "OS","Ossetian","","","");
INSERT INTO `languages`  VALUES ( "PA","Panjabi","","","");
INSERT INTO `languages`  VALUES ( "PI","PÃ"Âli","","","");
INSERT INTO `languages`  VALUES ( "PL","Polish","","","");
INSERT INTO `languages`  VALUES ( "PS","Pashto","","","");
INSERT INTO `languages`  VALUES ( "PT","Portuguese","","","");
INSERT INTO `languages`  VALUES ( "QU","Quechua","","","");
INSERT INTO `languages`  VALUES ( "RM","Romansh","","","");
INSERT INTO `languages`  VALUES ( "RN","Kirundi","","","");
INSERT INTO `languages`  VALUES ( "RO","Romanian","","","");
INSERT INTO `languages`  VALUES ( "RU","Russian","","","");
INSERT INTO `languages`  VALUES ( "RW","Kinyarwanda","","","");
INSERT INTO `languages`  VALUES ( "SA","Sanskrit","","","");
INSERT INTO `languages`  VALUES ( "SC","Sardinian","","","");
INSERT INTO `languages`  VALUES ( "SD","Sindhi","","","");
INSERT INTO `languages`  VALUES ( "SE","Northern Sami","","","");
INSERT INTO `languages`  VALUES ( "SG","Sango","","","");
INSERT INTO `languages`  VALUES ( "SI","Sinhala","","","");
INSERT INTO `languages`  VALUES ( "SK","Slovak","","","");
INSERT INTO `languages`  VALUES ( "SL","Slovene","","","");
INSERT INTO `languages`  VALUES ( "SM","Samoan","","","");
INSERT INTO `languages`  VALUES ( "SN","Shona","","","");
INSERT INTO `languages`  VALUES ( "SO","Somali","","","");
INSERT INTO `languages`  VALUES ( "SQ","Albanian","","","");
INSERT INTO `languages`  VALUES ( "SR","Serbian","","","");
INSERT INTO `languages`  VALUES ( "SS","Swati","","","");
INSERT INTO `languages`  VALUES ( "ST","Southern Sotho","","","");
INSERT INTO `languages`  VALUES ( "SU","Sundanese","","","");
INSERT INTO `languages`  VALUES ( "SV","Swedish","","","");
INSERT INTO `languages`  VALUES ( "SW","Swahili","","","");
INSERT INTO `languages`  VALUES ( "TA","Tamil","","","");
INSERT INTO `languages`  VALUES ( "TE","Telugu","","","");
INSERT INTO `languages`  VALUES ( "TG","Tajik","","","");
INSERT INTO `languages`  VALUES ( "TH","Thai","","","");
INSERT INTO `languages`  VALUES ( "TI","Tigrinya","","","");
INSERT INTO `languages`  VALUES ( "TK","Turkmen","","","");
INSERT INTO `languages`  VALUES ( "TL","Tagalog","","","");
INSERT INTO `languages`  VALUES ( "TN","Tswana","","","");
INSERT INTO `languages`  VALUES ( "TO","Tonga","","","");
INSERT INTO `languages`  VALUES ( "TR","Turkish","","","");
INSERT INTO `languages`  VALUES ( "TS","Tsonga","","","");
INSERT INTO `languages`  VALUES ( "TT","Tatar","","","");
INSERT INTO `languages`  VALUES ( "TW","Twi","","","");
INSERT INTO `languages`  VALUES ( "TY","Tahitian","","","");
INSERT INTO `languages`  VALUES ( "UG","Uighur","","","");
INSERT INTO `languages`  VALUES ( "UK","Ukrainian","","","");
INSERT INTO `languages`  VALUES ( "UR","Urdu","","","");
INSERT INTO `languages`  VALUES ( "UZ","Uzbek","","","");
INSERT INTO `languages`  VALUES ( "VE","Venda","","","");
INSERT INTO `languages`  VALUES ( "VI","Vietnamese","","","");
INSERT INTO `languages`  VALUES ( "VO","VolapÃÂ¼k","","","");
INSERT INTO `languages`  VALUES ( "WA","Walloon","","","");
INSERT INTO `languages`  VALUES ( "WO","Wolof","","","");
INSERT INTO `languages`  VALUES ( "XH","Xhosa","","","");
INSERT INTO `languages`  VALUES ( "YI","Yiddish","","","");
INSERT INTO `languages`  VALUES ( "YO","Yoruba","","","");
INSERT INTO `languages`  VALUES ( "ZA","Zhuang","","","");
INSERT INTO `languages`  VALUES ( "ZH","Chinese","","","");
INSERT INTO `languages`  VALUES ( "ZU","Zulu","","","");


--
-- Table structure for table `locations`
--
DROP TABLE  IF EXISTS `locations`;
CREATE TABLE `locations` (
  `location_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `entry_label` varchar(45) NOT NULL DEFAULT '',
  `entry_street_address` varchar(255) NOT NULL DEFAULT '',
  `entry_suburb` varchar(255) DEFAULT NULL,
  `entry_postcode` varchar(10) NOT NULL DEFAULT '',
  `entry_city` varchar(32) NOT NULL DEFAULT '',
  `entry_state` varchar(32) DEFAULT NULL,
  `province_id` int(11) unsigned DEFAULT NULL,
  `entry_country_code` char(2) NOT NULL,
  `latitude` decimal(11,8) DEFAULT NULL,
  `longtitude` decimal(11,8) DEFAULT NULL,
  PRIMARY KEY (`location_id`),
  KEY `fk_locations_countries1_idx` (`entry_country_code`),
  KEY `fk_locations_provinces1_idx` (`province_id`),
  CONSTRAINT `fk_locations_countries1` FOREIGN KEY (`entry_country_code`) REFERENCES `countries` (`country_code`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_locations_provinces1` FOREIGN KEY (`province_id`) REFERENCES `provinces` (`province_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

INSERT INTO `locations`  VALUES ( "1","Aimas UK","","","","","","","GB","","");
INSERT INTO `locations`  VALUES ( "2","Aimas NG","","","","","","1","NG","","");
INSERT INTO `locations`  VALUES ( "3","Aimas NG","","","","","","1","NG","","");
INSERT INTO `locations`  VALUES ( "4","Aimas UK","","","","","","","GB","","");


--
-- Table structure for table `logger`
--
DROP TABLE  IF EXISTS `logger`;
CREATE TABLE `logger` (
  `logger_id` varchar(60) NOT NULL,
  `log_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `value` text NOT NULL,
  `logged_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`log_id`,`logger_id`)
) ENGINE=InnoDB AUTO_INCREMENT=48 DEFAULT CHARSET=utf8;

INSERT INTO `logger`  VALUES ( "admin.log","1","Setup has created a new admin account for Gboyega Dada.","2014-12-26 03:36:54");
INSERT INTO `logger`  VALUES ( "missing-fingerprint.log","2","finger print missing in session!! This was also sent in the request:  by /store/october/login.php","2014-12-26 03:37:19");
INSERT INTO `logger`  VALUES ( "missing-fingerprint.log","3","finger print missing in session!! This was also sent in the request:  by /store/october/login.php","2014-12-26 03:37:35");
INSERT INTO `logger`  VALUES ( "missing-fingerprint.log","4","finger print missing in session!! This was also sent in the request:  by /store/october/landing.catalog.php","2014-12-26 03:57:00");
INSERT INTO `logger`  VALUES ( "missing-fingerprint.log","5","finger print missing in session!! This was also sent in the request:  by /store/october/login.php","2014-12-26 03:57:01");
INSERT INTO `logger`  VALUES ( "missing-fingerprint.log","6","finger print missing in session!! This was also sent in the request:  by /store/october/login.php","2014-12-26 04:04:22");
INSERT INTO `logger`  VALUES ( "missing-fingerprint.log","7","finger print missing in session!! This was also sent in the request:  by /store/october/login.php","2014-12-26 07:28:53");
INSERT INTO `logger`  VALUES ( "missing-fingerprint.log","8","finger print missing in session!! This was also sent in the request:  by /store/october/login.php","2014-12-26 07:29:03");
INSERT INTO `logger`  VALUES ( "missing-fingerprint.log","9","finger print missing in session!! This was also sent in the request:  by /store/october/landing.catalog.php","2014-12-26 07:37:39");
INSERT INTO `logger`  VALUES ( "missing-fingerprint.log","10","finger print missing in session!! This was also sent in the request:  by /store/october/login.php","2014-12-26 07:37:39");
INSERT INTO `logger`  VALUES ( "missing-fingerprint.log","11","finger print missing in session!! This was also sent in the request:  by /store/october/login.php","2014-12-26 07:38:04");
INSERT INTO `logger`  VALUES ( "missing-fingerprint.log","12","finger print missing in session!! This was also sent in the request:  by /store/october/login.php","2014-12-26 08:23:01");
INSERT INTO `logger`  VALUES ( "missing-fingerprint.log","13","finger print missing in session!! This was also sent in the request:  by /store/october/login.php","2014-12-26 08:23:12");
INSERT INTO `logger`  VALUES ( "missing-fingerprint.log","14","finger print missing in session!! This was also sent in the request:  by /store/october/landing.catalog.php","2014-12-26 12:00:48");
INSERT INTO `logger`  VALUES ( "missing-fingerprint.log","15","finger print missing in session!! This was also sent in the request:  by /store/october/login.php","2014-12-26 12:00:48");
INSERT INTO `logger`  VALUES ( "missing-fingerprint.log","16","finger print missing in session!! This was also sent in the request:  by /store/october/login.php","2014-12-26 12:04:00");
INSERT INTO `logger`  VALUES ( "missing-fingerprint.log","17","finger print missing in session!! This was also sent in the request:  by /store/october/landing.catalog.php","2014-12-26 13:43:14");
INSERT INTO `logger`  VALUES ( "missing-fingerprint.log","18","finger print missing in session!! This was also sent in the request:  by /store/october/login.php","2014-12-26 13:43:14");
INSERT INTO `logger`  VALUES ( "missing-fingerprint.log","19","finger print missing in session!! This was also sent in the request:  by /store/october/login.php","2014-12-26 14:25:28");
INSERT INTO `logger`  VALUES ( "missing-fingerprint.log","20","finger print missing in session!! This was also sent in the request:  by /store/october/login.php","2014-12-26 14:40:31");
INSERT INTO `logger`  VALUES ( "error.log","21","Error: //                                                                    Error in class (Cataleya_Class_Sale): [ HY000, 1364, Field 'sort_order' doesn't have a default value ] on line 363","2014-12-26 15:29:21");
INSERT INTO `logger`  VALUES ( "error.log","22","Error: //                                                                    Error in class (Cataleya_Class_Sale): [ 22007, 1292, Incorrect datetime value: '1' for column 'date_created' at row 1 ] on line 364","2014-12-26 15:40:26");
INSERT INTO `logger`  VALUES ( "error.log","23","Error: //Error in class (Cataleya_Class_Coupon): [ Bad param (indefinite) in method: create  ] on line 362","2014-12-26 15:59:47");
INSERT INTO `logger`  VALUES ( "missing-fingerprint.log","24","finger print missing in session!! This was also sent in the request:  by /store/october/landing.catalog.php","2014-12-26 16:37:11");
INSERT INTO `logger`  VALUES ( "missing-fingerprint.log","25","finger print missing in session!! This was also sent in the request:  by /store/october/login.php","2014-12-26 16:37:11");
INSERT INTO `logger`  VALUES ( "missing-fingerprint.log","26","finger print missing in session!! This was also sent in the request:  by /store/october/login.php","2014-12-26 16:37:36");
INSERT INTO `logger`  VALUES ( "missing-fingerprint.log","27","finger print missing in session!! This was also sent in the request:  by /store/october/login.php","2014-12-26 17:00:49");
INSERT INTO `logger`  VALUES ( "missing-fingerprint.log","28","finger print missing in session!! This was also sent in the request:  by /store/october/login.php","2014-12-26 17:01:04");
INSERT INTO `logger`  VALUES ( "missing-fingerprint.log","29","finger print missing in session!! This was also sent in the request:  by /store/october/landing.catalog.php","2014-12-27 02:41:33");
INSERT INTO `logger`  VALUES ( "missing-fingerprint.log","30","finger print missing in session!! This was also sent in the request:  by /store/october/login.php","2014-12-27 02:41:33");
INSERT INTO `logger`  VALUES ( "missing-fingerprint.log","31","finger print missing in session!! This was also sent in the request:  by /store/october/login.php","2014-12-27 02:43:51");
INSERT INTO `logger`  VALUES ( "missing-fingerprint.log","32","finger print missing in session!! This was also sent in the request:  by /store/october/login.php","2014-12-27 03:13:02");
INSERT INTO `logger`  VALUES ( "missing-fingerprint.log","33","finger print missing in session!! This was also sent in the request:  by /store/october/login.php","2014-12-27 03:13:15");
INSERT INTO `logger`  VALUES ( "missing-fingerprint.log","34","finger print missing in session!! This was also sent in the request:  by /store/october/landing.catalog.php","2014-12-27 05:16:36");
INSERT INTO `logger`  VALUES ( "missing-fingerprint.log","35","finger print missing in session!! This was also sent in the request:  by /store/october/login.php","2014-12-27 05:16:36");
INSERT INTO `logger`  VALUES ( "missing-fingerprint.log","36","finger print missing in session!! This was also sent in the request:  by /store/october/login.php","2014-12-27 05:27:10");
INSERT INTO `logger`  VALUES ( "error.log","37","Error: //                                                                        Error in class (Cataleya_Class_Catalog): [ HY093, ,  ] on line 237","2014-12-27 05:40:12");
INSERT INTO `logger`  VALUES ( "missing-fingerprint.log","38","finger print missing in session!! This was also sent in the request:  by /store/october/login.php","2014-12-27 06:06:43");
INSERT INTO `logger`  VALUES ( "missing-fingerprint.log","39","finger print missing in session!! This was also sent in the request:  by /store/october/login.php","2014-12-27 06:06:54");
INSERT INTO `logger`  VALUES ( "error.log","40","Error: //                                                                        Error in class (Cataleya_Class_Catalog): [ 42000, 1064, You have an error in your SQL syntax; check the manual that corresponds to your MySQL server version for the right syntax to use near '*                                                                             ' at line 5 ] on line 243","2014-12-27 06:50:54");
INSERT INTO `logger`  VALUES ( "missing-fingerprint.log","41","finger print missing in session!! This was also sent in the request:  by /store/october/login.php","2014-12-27 07:32:54");
INSERT INTO `logger`  VALUES ( "missing-fingerprint.log","42","finger print missing in session!! This was also sent in the request:  by /store/october/login.php","2014-12-27 07:33:09");
INSERT INTO `logger`  VALUES ( "missing-fingerprint.log","43","finger print missing in session!! This was also sent in the request:  by /store/october/index.php","2014-12-27 07:50:04");
INSERT INTO `logger`  VALUES ( "missing-fingerprint.log","44","finger print missing in session!! This was also sent in the request:  by /store/october/login.php","2014-12-27 07:50:04");
INSERT INTO `logger`  VALUES ( "missing-fingerprint.log","45","finger print missing in session!! This was also sent in the request:  by /store/october/login.php","2014-12-27 07:53:45");
INSERT INTO `logger`  VALUES ( "missing-fingerprint.log","46","finger print missing in session!! This was also sent in the request:  by /store/october/landing.customers.php","2014-12-27 13:22:17");
INSERT INTO `logger`  VALUES ( "missing-fingerprint.log","47","finger print missing in session!! This was also sent in the request:  by /store/october/login.php","2014-12-27 13:22:19");


--
-- Table structure for table `notification_profile`
--
DROP TABLE  IF EXISTS `notification_profile`;
CREATE TABLE `notification_profile` (
  `notification_id` smallint(4) unsigned NOT NULL AUTO_INCREMENT,
  `keyword` varchar(60) NOT NULL,
  `description_id` bigint(20) unsigned NOT NULL,
  `icon` varchar(255) NOT NULL DEFAULT 'no-icon.png',
  `entity_name` varchar(60) NOT NULL,
  `entity_name_plural` varchar(60) NOT NULL,
  `notification_text` tinytext NOT NULL,
  `send_after_period` int(11) NOT NULL DEFAULT '1',
  `send_after_interval` enum('second','minute','hour','day','week','month','year') NOT NULL DEFAULT 'day',
  `send_after_increments` smallint(5) unsigned NOT NULL DEFAULT '5',
  `increments_since_last_sent` int(10) unsigned NOT NULL DEFAULT '0',
  `last_sent_at` bigint(20) unsigned NOT NULL DEFAULT '0',
  `request_uri` varchar(255) NOT NULL DEFAULT '/',
  `date_added` datetime NOT NULL,
  `last_modified` datetime NOT NULL,
  PRIMARY KEY (`notification_id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8;

INSERT INTO `notification_profile`  VALUES ( "1","stock.low","69","flag-4-50x50.png","Product","Products","almost out of stock.","1","hour","10","0","0","http://dev.aimas.com.ng/store/october/landing.catalog.php?show=low","2014-12-26 03:36:02","2014-12-26 03:36:02");
INSERT INTO `notification_profile`  VALUES ( "2","stock.out","70","flag-4-50x50.png","Product","Products","out of stock.","1","hour","5","0","0","http://dev.aimas.com.ng/store/october/landing.catalog.php?show=out","2014-12-26 03:36:02","2014-12-26 03:36:02");
INSERT INTO `notification_profile`  VALUES ( "3","customer.new","71","user-5-50x50.png","Customer Account","Customer Acounts","created at the store.","1","week","10","0","0","http://dev.aimas.com.ng/store/october/browse.customers.php?ob=4&o=2","2014-12-26 03:36:02","2014-12-26 03:36:02");
INSERT INTO `notification_profile`  VALUES ( "4","tranx.error","72","document-fill-50x50.png","Transaction error","Transaction errors","logged.","15","minute","1","0","0","http://dev.aimas.com.ng/store/october/logs.settings.php?id=tranx.error","2014-12-26 03:36:02","2014-12-26 03:36:02");
INSERT INTO `notification_profile`  VALUES ( "5","flag.failed.login","73","lock-stroke-50x50.png","Failed Login","Failed Logins","logged.","3","day","50","0","0","http://dev.aimas.com.ng/store/october/logs.settings.php?id=flag.failed.login","2014-12-26 03:36:02","2014-12-26 03:36:02");
INSERT INTO `notification_profile`  VALUES ( "6","admin.deleted","74","user-3-50x50.png","Admin Account","Admin Accounts","deleted.","1","minute","1","0","0","http://dev.aimas.com.ng/store/october/logs.settings.php?id=admin.log","2014-12-26 03:36:02","2014-12-26 03:36:02");
INSERT INTO `notification_profile`  VALUES ( "7","admin.new","75","user-3-50x50.png","Admin Account","Admin Accounts","just been created.","1","minute","1","1","0","http://dev.aimas.com.ng/store/october/logs.settings.php?id=admin.log","2014-12-26 03:36:02","2014-12-26 03:36:54");
INSERT INTO `notification_profile`  VALUES ( "8","store.new","76","map-pin-fill-50x50.png","New Store","New Stores","created.","1","minute","1","0","0","http://dev.aimas.com.ng/store/october/landing.stores.php","2014-12-26 03:36:02","2014-12-26 03:36:02");
INSERT INTO `notification_profile`  VALUES ( "9","6b18c0fd53ea33e496d42bd0670c5c6f7a6d0e80","84","basket-50x50.png","New Order","New Orders","waiting to be processed.","1","hour","10","0","0","http://dev.aimas.com.ng/store/october/landing.orders.php?show=pending","2014-12-26 03:36:55","2014-12-26 03:36:55");
INSERT INTO `notification_profile`  VALUES ( "10","c11ad8fe205554ec7e05e2b7d76cd23112115bcc","85","flight-50x50.png","Unconfirmed Delivery","Unconfirmed Deliveries","waiting to be processed.","1","hour","10","0","0","http://dev.aimas.com.ng/store/october/landing.orders.php?show=shipped","2014-12-26 03:36:55","2014-12-26 03:36:55");
INSERT INTO `notification_profile`  VALUES ( "11","90d1d256eb7e93f7b0595515130907d1facd8b28","87","basket-50x50.png","New Order","New Orders","waiting to be processed.","1","hour","10","0","0","http://dev.aimas.com.ng/store/october/landing.orders.php?show=pending","2014-12-26 03:36:55","2014-12-26 03:36:55");
INSERT INTO `notification_profile`  VALUES ( "12","7e3b8329b1b33bda2c31711f8c0780edbeb5efb6","88","flight-50x50.png","Unconfirmed Delivery","Unconfirmed Deliveries","waiting to be processed.","1","hour","10","0","0","http://dev.aimas.com.ng/store/october/landing.orders.php?show=shipped","2014-12-26 03:36:55","2014-12-26 03:36:55");


--
-- Table structure for table `options_to_attributes`
--
DROP TABLE  IF EXISTS `options_to_attributes`;
CREATE TABLE `options_to_attributes` (
  `option_id` bigint(20) unsigned NOT NULL,
  `attribute_id` bigint(20) unsigned NOT NULL,
  PRIMARY KEY (`option_id`,`attribute_id`),
  KEY `fk_option_id_idx` (`option_id`),
  KEY `fk_attribute_id_idx` (`attribute_id`),
  CONSTRAINT `fk_attribute_id` FOREIGN KEY (`attribute_id`) REFERENCES `attributes` (`attribute_id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `fk_option_id` FOREIGN KEY (`option_id`) REFERENCES `product_options` (`option_id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `options_to_attributes`  VALUES ( "1","1");
INSERT INTO `options_to_attributes`  VALUES ( "1","4");
INSERT INTO `options_to_attributes`  VALUES ( "2","1");
INSERT INTO `options_to_attributes`  VALUES ( "2","5");
INSERT INTO `options_to_attributes`  VALUES ( "3","1");
INSERT INTO `options_to_attributes`  VALUES ( "3","6");
INSERT INTO `options_to_attributes`  VALUES ( "4","2");
INSERT INTO `options_to_attributes`  VALUES ( "4","4");
INSERT INTO `options_to_attributes`  VALUES ( "5","2");
INSERT INTO `options_to_attributes`  VALUES ( "5","5");
INSERT INTO `options_to_attributes`  VALUES ( "6","2");
INSERT INTO `options_to_attributes`  VALUES ( "6","6");
INSERT INTO `options_to_attributes`  VALUES ( "7","3");
INSERT INTO `options_to_attributes`  VALUES ( "7","4");
INSERT INTO `options_to_attributes`  VALUES ( "8","3");
INSERT INTO `options_to_attributes`  VALUES ( "8","5");
INSERT INTO `options_to_attributes`  VALUES ( "9","3");
INSERT INTO `options_to_attributes`  VALUES ( "9","6");
INSERT INTO `options_to_attributes`  VALUES ( "10","1");
INSERT INTO `options_to_attributes`  VALUES ( "10","4");
INSERT INTO `options_to_attributes`  VALUES ( "11","1");
INSERT INTO `options_to_attributes`  VALUES ( "11","5");
INSERT INTO `options_to_attributes`  VALUES ( "12","1");
INSERT INTO `options_to_attributes`  VALUES ( "12","6");
INSERT INTO `options_to_attributes`  VALUES ( "13","2");
INSERT INTO `options_to_attributes`  VALUES ( "13","4");
INSERT INTO `options_to_attributes`  VALUES ( "14","2");
INSERT INTO `options_to_attributes`  VALUES ( "14","5");
INSERT INTO `options_to_attributes`  VALUES ( "15","2");
INSERT INTO `options_to_attributes`  VALUES ( "15","6");
INSERT INTO `options_to_attributes`  VALUES ( "16","3");
INSERT INTO `options_to_attributes`  VALUES ( "16","4");
INSERT INTO `options_to_attributes`  VALUES ( "17","3");
INSERT INTO `options_to_attributes`  VALUES ( "17","5");
INSERT INTO `options_to_attributes`  VALUES ( "18","3");
INSERT INTO `options_to_attributes`  VALUES ( "18","6");
INSERT INTO `options_to_attributes`  VALUES ( "34","1");
INSERT INTO `options_to_attributes`  VALUES ( "35","2");
INSERT INTO `options_to_attributes`  VALUES ( "36","3");
INSERT INTO `options_to_attributes`  VALUES ( "37","1");
INSERT INTO `options_to_attributes`  VALUES ( "38","2");
INSERT INTO `options_to_attributes`  VALUES ( "39","3");


--
-- Table structure for table `order_detail`
--
DROP TABLE  IF EXISTS `order_detail`;
CREATE TABLE `order_detail` (
  `order_detail_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `order_id` int(11) unsigned NOT NULL,
  `product_id` int(11) unsigned NOT NULL,
  `option_id` bigint(20) unsigned NOT NULL,
  `quantity` float NOT NULL DEFAULT '1',
  `gift_wrap` tinyint(1) DEFAULT '0',
  `wrapper_id` int(11) DEFAULT '0',
  `price` decimal(15,4) NOT NULL DEFAULT '0.0000',
  `original_price` decimal(15,4) NOT NULL DEFAULT '0.0000',
  `subtotal` decimal(15,4) NOT NULL DEFAULT '0.0000',
  `tax` decimal(7,4) NOT NULL DEFAULT '0.0000',
  `tax_json` text NOT NULL,
  `product_is_free` tinyint(1) NOT NULL DEFAULT '0',
  `one_time_charges` decimal(15,4) NOT NULL DEFAULT '0.0000',
  `item_name` varchar(255) NOT NULL DEFAULT 'New Item',
  `item_description` varchar(255) NOT NULL DEFAULT 'no description',
  `coupon_id` int(11) unsigned NOT NULL DEFAULT '0',
  `coupon_code` varchar(45) NOT NULL DEFAULT '',
  `coupon_discount_amount` decimal(15,4) NOT NULL DEFAULT '0.0000',
  `sale_id` int(11) unsigned NOT NULL DEFAULT '0',
  `sale_description` varchar(160) NOT NULL DEFAULT 'Sale',
  `sale_discount_amount` decimal(15,4) NOT NULL DEFAULT '0.0000',
  `ean` varchar(13) DEFAULT NULL,
  `upc` varchar(12) DEFAULT NULL,
  `reference` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`order_detail_id`,`order_id`),
  KEY `fk_order_detail_orders1` (`order_id`),
  CONSTRAINT `fk_order_detail_orders1` FOREIGN KEY (`order_id`) REFERENCES `orders` (`order_id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



--
-- Table structure for table `order_status_history`
--
DROP TABLE  IF EXISTS `order_status_history`;
CREATE TABLE `order_status_history` (
  `order_status_history_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `order_id` int(11) unsigned NOT NULL,
  `order_status_id` int(5) NOT NULL DEFAULT '0',
  `date_added` datetime NOT NULL DEFAULT '0001-01-01 00:00:00',
  `customer_notified` int(1) DEFAULT '0',
  `comments` text,
  PRIMARY KEY (`order_status_history_id`),
  KEY `idx_orders_id_status_id_zen` (`order_id`,`order_status_id`),
  KEY `fk_order_status_history_orders1_idx` (`order_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;



--
-- Table structure for table `orders`
--
DROP TABLE  IF EXISTS `orders`;
CREATE TABLE `orders` (
  `order_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `store_id` smallint(5) unsigned DEFAULT NULL,
  `store_name` varchar(160) NOT NULL,
  `order_date` datetime NOT NULL,
  `date_modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `customer_id` int(11) unsigned NOT NULL,
  `shipping_option_id` int(10) unsigned DEFAULT NULL,
  `shipping_description` varchar(160) NOT NULL,
  `shipping_charges` decimal(10,4) NOT NULL,
  `shipping_before_tax` decimal(10,4) NOT NULL,
  `total_cost` decimal(15,4) NOT NULL,
  `product_tax_json` text,
  `shipping_tax_json` text,
  `currency_code` char(3) NOT NULL,
  `exp_delivery_date` datetime NOT NULL,
  `order_processed_by` int(11) unsigned DEFAULT NULL,
  `order_status` enum('pending','shipped','delivered','cancelled') NOT NULL DEFAULT 'pending',
  `payment_status` enum('pending','paid','cash on delivery','bank transfer','cancelled','failed') NOT NULL DEFAULT 'pending',
  PRIMARY KEY (`order_id`),
  KEY `fk_orders_customers1_idx` (`customer_id`),
  KEY `fk_orders_currencies1_idx` (`currency_code`),
  KEY `fk_orders_stores1_idx` (`store_id`),
  CONSTRAINT `fk_orders_currencies1` FOREIGN KEY (`currency_code`) REFERENCES `currencies` (`currency_code`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_orders_customers1` FOREIGN KEY (`customer_id`) REFERENCES `customers` (`customer_id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `fk_orders_stores1` FOREIGN KEY (`store_id`) REFERENCES `stores` (`store_id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



--
-- Table structure for table `payment_types`
--
DROP TABLE  IF EXISTS `payment_types`;
CREATE TABLE `payment_types` (
  `payment_type_id` smallint(5) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `display_name` varchar(255) NOT NULL,
  `payment_class` varchar(255) NOT NULL,
  `config_id` varchar(255) NOT NULL,
  `requires_billing_address` tinyint(1) NOT NULL,
  `requires_redirect` tinyint(1) NOT NULL,
  `active` tinyint(1) NOT NULL DEFAULT '0',
  `logo` varchar(160) DEFAULT NULL,
  `max_amount` decimal(15,4) NOT NULL DEFAULT '300.0000',
  `min_amount` decimal(15,4) NOT NULL DEFAULT '0.0000',
  PRIMARY KEY (`payment_type_id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

INSERT INTO `payment_types`  VALUES ( "1","Interswitch WebPay","WebPay","Cataleya_Class_WebPay","webpay.config","0","1","1","webpay.gif","300.0000","0.0000");
INSERT INTO `payment_types`  VALUES ( "2","Interswitch E-Tranzact","E-Tranzact","Cataleya_Class_eTranzact","webpay.config","0","1","1","etranzact.gif","300.0000","0.0000");
INSERT INTO `payment_types`  VALUES ( "3","PayPal Express Checkout","PayPal","Cataleya_Class_PayPalExpressCheckout","paypal.config","0","1","1","paypal.gif","300.0000","0.0000");
INSERT INTO `payment_types`  VALUES ( "4","PayPal Direct Payment","Credit Card","Cataleya_Class_PayPalDirectPayment","paypal.config","1","1","0","paypal-direct.gif","300.0000","0.0000");
INSERT INTO `payment_types`  VALUES ( "5","Cash On Delivery","Cash On Delivery","Cataleya_Class_CashOnDelivery","cash.on.delivery.config","0","0","0","cash-on-delivery.gif","300.0000","0.0000");
INSERT INTO `payment_types`  VALUES ( "6","Bank Transfer","Bank Transfer","Cataleya_Class_BankTransfer","bank.transfer.config","0","0","0","bank-transfer.gif","300.0000","0.0000");


--
-- Table structure for table `permissions`
--
DROP TABLE  IF EXISTS `permissions`;
CREATE TABLE `permissions` (
  `role_id` smallint(5) unsigned NOT NULL,
  `privilege_id` varchar(255) NOT NULL,
  `permission` tinyint(1) unsigned NOT NULL,
  `last_modified` datetime NOT NULL,
  PRIMARY KEY (`role_id`,`privilege_id`),
  KEY `fk_permissions_admin_roles1_idx` (`role_id`),
  KEY `fk_permissions_privileges1` (`privilege_id`),
  CONSTRAINT `fk_permissions_admin_roles1` FOREIGN KEY (`role_id`) REFERENCES `admin_roles` (`role_id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `fk_permissions_privileges1` FOREIGN KEY (`privilege_id`) REFERENCES `privileges` (`privilege_id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `permissions`  VALUES ( "1","SUPER_USER","1","2014-12-26 03:36:02");
INSERT INTO `permissions`  VALUES ( "2","CREATE_CUSTOMER_ACCOUNT","1","2014-12-26 03:36:02");
INSERT INTO `permissions`  VALUES ( "2","CREATE_EMPLOYEE_ACCOUNT","1","2014-12-26 03:36:02");
INSERT INTO `permissions`  VALUES ( "2","CREATE_MANAGER_ACCOUNT","1","2014-12-26 03:36:02");
INSERT INTO `permissions`  VALUES ( "2","CREATE_ORDERS","1","2014-12-26 03:36:02");
INSERT INTO `permissions`  VALUES ( "2","CREATE_PRODUCT","1","2014-12-26 03:36:02");
INSERT INTO `permissions`  VALUES ( "2","CREATE_STORE","1","2014-12-26 03:36:02");
INSERT INTO `permissions`  VALUES ( "2","DELETE_CUSTOMER_ACCOUNT","1","2014-12-26 03:36:02");
INSERT INTO `permissions`  VALUES ( "2","DELETE_EMPLOYEE_ACCOUNT","1","2014-12-26 03:36:02");
INSERT INTO `permissions`  VALUES ( "2","DELETE_MANAGER_ACCOUNT","1","2014-12-26 03:36:02");
INSERT INTO `permissions`  VALUES ( "2","DELETE_ORDERS","1","2014-12-26 03:36:02");
INSERT INTO `permissions`  VALUES ( "2","DELETE_PRODUCT","1","2014-12-26 03:36:02");
INSERT INTO `permissions`  VALUES ( "2","DELETE_STORE","1","2014-12-26 03:36:02");
INSERT INTO `permissions`  VALUES ( "2","EDIT_CATALOG","1","2014-12-26 03:36:02");
INSERT INTO `permissions`  VALUES ( "2","EDIT_CUSTOMER_ACCOUNT","1","2014-12-26 03:36:02");
INSERT INTO `permissions`  VALUES ( "2","EDIT_EMPLOYEE_ACCOUNT","1","2014-12-26 03:36:02");
INSERT INTO `permissions`  VALUES ( "2","EDIT_MANAGER_ACCOUNT","1","2014-12-26 03:36:02");
INSERT INTO `permissions`  VALUES ( "2","EDIT_ORDERS","1","2014-12-26 03:36:02");
INSERT INTO `permissions`  VALUES ( "2","EDIT_PRODUCT","1","2014-12-26 03:36:02");
INSERT INTO `permissions`  VALUES ( "2","EDIT_STORE","1","2014-12-26 03:36:02");
INSERT INTO `permissions`  VALUES ( "2","EDIT_STORE_OWNER_ACCOUNT","1","2014-12-26 03:36:02");
INSERT INTO `permissions`  VALUES ( "2","SET_PRODUCT_PRICE","1","2014-12-26 03:36:02");
INSERT INTO `permissions`  VALUES ( "2","SET_PRODUCT_STOCK","1","2014-12-26 03:36:02");
INSERT INTO `permissions`  VALUES ( "2","VIEW_ADMININISTRATOR_ACCOUNT","1","2014-12-26 03:36:02");
INSERT INTO `permissions`  VALUES ( "2","VIEW_CATALOG","1","2014-12-26 03:36:02");
INSERT INTO `permissions`  VALUES ( "2","VIEW_CUSTOMER_ACCOUNT","1","2014-12-26 03:36:02");
INSERT INTO `permissions`  VALUES ( "2","VIEW_EMPLOYEE_ACCOUNT","1","2014-12-26 03:36:02");
INSERT INTO `permissions`  VALUES ( "2","VIEW_MANAGER_ACCOUNT","1","2014-12-26 03:36:02");
INSERT INTO `permissions`  VALUES ( "2","VIEW_ORDERS","1","2014-12-26 03:36:02");
INSERT INTO `permissions`  VALUES ( "2","VIEW_PPRODUCT","1","2014-12-26 03:36:02");
INSERT INTO `permissions`  VALUES ( "2","VIEW_STORE","1","2014-12-26 03:36:02");
INSERT INTO `permissions`  VALUES ( "2","VIEW_STORES","1","2014-12-26 03:36:02");
INSERT INTO `permissions`  VALUES ( "2","VIEW_STORE_OWNER_ACCOUNT","1","2014-12-26 03:36:02");
INSERT INTO `permissions`  VALUES ( "3","CREATE_CUSTOMER_ACCOUNT","1","2014-12-26 03:36:02");
INSERT INTO `permissions`  VALUES ( "3","CREATE_EMPLOYEE_ACCOUNT","1","2014-12-26 03:36:02");
INSERT INTO `permissions`  VALUES ( "3","CREATE_ORDERS","1","2014-12-26 03:36:02");
INSERT INTO `permissions`  VALUES ( "3","CREATE_PRODUCT","1","2014-12-26 03:36:02");
INSERT INTO `permissions`  VALUES ( "3","CREATE_STORE","1","2014-12-26 03:36:02");
INSERT INTO `permissions`  VALUES ( "3","DELETE_CUSTOMER_ACCOUNT","1","2014-12-26 03:36:02");
INSERT INTO `permissions`  VALUES ( "3","DELETE_EMPLOYEE_ACCOUNT","1","2014-12-26 03:36:02");
INSERT INTO `permissions`  VALUES ( "3","DELETE_ORDERS","1","2014-12-26 03:36:02");
INSERT INTO `permissions`  VALUES ( "3","DELETE_PRODUCT","1","2014-12-26 03:36:02");
INSERT INTO `permissions`  VALUES ( "3","DELETE_STORE","1","2014-12-26 03:36:02");
INSERT INTO `permissions`  VALUES ( "3","EDIT_CATALOG","1","2014-12-26 03:36:02");
INSERT INTO `permissions`  VALUES ( "3","EDIT_CUSTOMER_ACCOUNT","1","2014-12-26 03:36:02");
INSERT INTO `permissions`  VALUES ( "3","EDIT_EMPLOYEE_ACCOUNT","1","2014-12-26 03:36:02");
INSERT INTO `permissions`  VALUES ( "3","EDIT_MANAGER_ACCOUNT","1","2014-12-26 03:36:02");
INSERT INTO `permissions`  VALUES ( "3","EDIT_ORDERS","1","2014-12-26 03:36:02");
INSERT INTO `permissions`  VALUES ( "3","EDIT_PRODUCT","1","2014-12-26 03:36:02");
INSERT INTO `permissions`  VALUES ( "3","EDIT_STORE","1","2014-12-26 03:36:02");
INSERT INTO `permissions`  VALUES ( "3","SET_PRODUCT_PRICE","1","2014-12-26 03:36:02");
INSERT INTO `permissions`  VALUES ( "3","SET_PRODUCT_STOCK","1","2014-12-26 03:36:02");
INSERT INTO `permissions`  VALUES ( "3","VIEW_CATALOG","1","2014-12-26 03:36:02");
INSERT INTO `permissions`  VALUES ( "3","VIEW_CUSTOMER_ACCOUNT","1","2014-12-26 03:36:02");
INSERT INTO `permissions`  VALUES ( "3","VIEW_EMPLOYEE_ACCOUNT","1","2014-12-26 03:36:02");
INSERT INTO `permissions`  VALUES ( "3","VIEW_MANAGER_ACCOUNT","1","2014-12-26 03:36:02");
INSERT INTO `permissions`  VALUES ( "3","VIEW_ORDERS","1","2014-12-26 03:36:02");
INSERT INTO `permissions`  VALUES ( "3","VIEW_PPRODUCT","1","2014-12-26 03:36:02");
INSERT INTO `permissions`  VALUES ( "3","VIEW_STORE","1","2014-12-26 03:36:02");
INSERT INTO `permissions`  VALUES ( "3","VIEW_STORES","1","2014-12-26 03:36:02");
INSERT INTO `permissions`  VALUES ( "3","VIEW_STORE_OWNER_ACCOUNT","1","2014-12-26 03:36:02");
INSERT INTO `permissions`  VALUES ( "4","EDIT_EMPLOYEE_ACCOUNT","1","2014-12-26 03:36:02");
INSERT INTO `permissions`  VALUES ( "4","VIEW_CATALOG","1","2014-12-26 03:36:02");
INSERT INTO `permissions`  VALUES ( "4","VIEW_CUSTOMER_ACCOUNT","1","2014-12-26 03:36:02");
INSERT INTO `permissions`  VALUES ( "4","VIEW_EMPLOYEE_ACCOUNT","1","2014-12-26 03:36:02");
INSERT INTO `permissions`  VALUES ( "4","VIEW_MANAGER_ACCOUNT","1","2014-12-26 03:36:02");
INSERT INTO `permissions`  VALUES ( "4","VIEW_ORDERS","1","2014-12-26 03:36:02");
INSERT INTO `permissions`  VALUES ( "4","VIEW_PPRODUCT","1","2014-12-26 03:36:02");
INSERT INTO `permissions`  VALUES ( "4","VIEW_STORE","1","2014-12-26 03:36:02");
INSERT INTO `permissions`  VALUES ( "4","VIEW_STORES","1","2014-12-26 03:36:02");


--
-- Table structure for table `price_values`
--
DROP TABLE  IF EXISTS `price_values`;
CREATE TABLE `price_values` (
  `price_id` bigint(20) unsigned NOT NULL,
  `store_id` smallint(5) unsigned NOT NULL,
  `value` decimal(15,4) NOT NULL DEFAULT '0.0000',
  PRIMARY KEY (`price_id`,`store_id`),
  KEY `fk_price_values_prices1_idx` (`price_id`),
  KEY `fk_price_values_stores1_idx` (`store_id`),
  CONSTRAINT `fk_price_values_prices1` FOREIGN KEY (`price_id`) REFERENCES `prices` (`price_id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `fk_price_values_stores1` FOREIGN KEY (`store_id`) REFERENCES `stores` (`store_id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `price_values`  VALUES ( "1","1","5.0888");
INSERT INTO `price_values`  VALUES ( "2","1","7.6332");
INSERT INTO `price_values`  VALUES ( "3","1","9.5415");
INSERT INTO `price_values`  VALUES ( "4","2","6.4513");
INSERT INTO `price_values`  VALUES ( "5","2","9.6770");
INSERT INTO `price_values`  VALUES ( "6","2","16.1283");
INSERT INTO `price_values`  VALUES ( "7","1","31.8050");
INSERT INTO `price_values`  VALUES ( "7","2","80.6415");
INSERT INTO `price_values`  VALUES ( "8","1","31.8050");
INSERT INTO `price_values`  VALUES ( "8","2","80.6415");
INSERT INTO `price_values`  VALUES ( "9","1","31.8050");
INSERT INTO `price_values`  VALUES ( "9","2","80.6415");
INSERT INTO `price_values`  VALUES ( "10","1","31.8050");
INSERT INTO `price_values`  VALUES ( "10","2","80.6415");
INSERT INTO `price_values`  VALUES ( "11","1","31.8050");
INSERT INTO `price_values`  VALUES ( "11","2","80.6415");
INSERT INTO `price_values`  VALUES ( "12","1","31.8050");
INSERT INTO `price_values`  VALUES ( "12","2","80.6415");
INSERT INTO `price_values`  VALUES ( "13","1","31.8050");
INSERT INTO `price_values`  VALUES ( "13","2","80.6415");
INSERT INTO `price_values`  VALUES ( "14","1","31.8050");
INSERT INTO `price_values`  VALUES ( "14","2","80.6415");
INSERT INTO `price_values`  VALUES ( "15","1","31.8050");
INSERT INTO `price_values`  VALUES ( "15","2","80.6415");
INSERT INTO `price_values`  VALUES ( "31","1","95.4150");
INSERT INTO `price_values`  VALUES ( "32","1","95.4150");
INSERT INTO `price_values`  VALUES ( "33","1","95.4150");
INSERT INTO `price_values`  VALUES ( "34","1","127.2200");
INSERT INTO `price_values`  VALUES ( "35","1","127.2200");
INSERT INTO `price_values`  VALUES ( "36","1","127.2200");
INSERT INTO `price_values`  VALUES ( "37","1","127.2200");
INSERT INTO `price_values`  VALUES ( "38","1","127.2200");
INSERT INTO `price_values`  VALUES ( "39","1","127.2200");
INSERT INTO `price_values`  VALUES ( "40","1","127.2200");
INSERT INTO `price_values`  VALUES ( "41","1","127.2200");
INSERT INTO `price_values`  VALUES ( "42","1","127.2200");
INSERT INTO `price_values`  VALUES ( "43","1","50.8880");
INSERT INTO `price_values`  VALUES ( "44","1","50.8880");
INSERT INTO `price_values`  VALUES ( "45","1","50.8880");


--
-- Table structure for table `prices`
--
DROP TABLE  IF EXISTS `prices`;
CREATE TABLE `prices` (
  `price_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `type` varchar(10) NOT NULL DEFAULT '(=)',
  PRIMARY KEY (`price_id`)
) ENGINE=InnoDB AUTO_INCREMENT=46 DEFAULT CHARSET=utf8;

INSERT INTO `prices`  VALUES ( "1","(=)");
INSERT INTO `prices`  VALUES ( "2","(=)");
INSERT INTO `prices`  VALUES ( "3","(=)");
INSERT INTO `prices`  VALUES ( "4","(=)");
INSERT INTO `prices`  VALUES ( "5","(=)");
INSERT INTO `prices`  VALUES ( "6","(=)");
INSERT INTO `prices`  VALUES ( "7","(=)");
INSERT INTO `prices`  VALUES ( "8","(=)");
INSERT INTO `prices`  VALUES ( "9","(=)");
INSERT INTO `prices`  VALUES ( "10","(=)");
INSERT INTO `prices`  VALUES ( "11","(=)");
INSERT INTO `prices`  VALUES ( "12","(=)");
INSERT INTO `prices`  VALUES ( "13","(=)");
INSERT INTO `prices`  VALUES ( "14","(=)");
INSERT INTO `prices`  VALUES ( "15","(=)");
INSERT INTO `prices`  VALUES ( "16","(=)");
INSERT INTO `prices`  VALUES ( "17","(=)");
INSERT INTO `prices`  VALUES ( "18","(=)");
INSERT INTO `prices`  VALUES ( "19","(=)");
INSERT INTO `prices`  VALUES ( "20","(=)");
INSERT INTO `prices`  VALUES ( "21","(=)");
INSERT INTO `prices`  VALUES ( "22","(=)");
INSERT INTO `prices`  VALUES ( "23","(=)");
INSERT INTO `prices`  VALUES ( "24","(=)");
INSERT INTO `prices`  VALUES ( "25","(=)");
INSERT INTO `prices`  VALUES ( "26","(=)");
INSERT INTO `prices`  VALUES ( "27","(=)");
INSERT INTO `prices`  VALUES ( "28","(=)");
INSERT INTO `prices`  VALUES ( "29","(=)");
INSERT INTO `prices`  VALUES ( "30","(=)");
INSERT INTO `prices`  VALUES ( "31","(=)");
INSERT INTO `prices`  VALUES ( "32","(=)");
INSERT INTO `prices`  VALUES ( "33","(=)");
INSERT INTO `prices`  VALUES ( "34","(=)");
INSERT INTO `prices`  VALUES ( "35","(=)");
INSERT INTO `prices`  VALUES ( "36","(=)");
INSERT INTO `prices`  VALUES ( "37","(=)");
INSERT INTO `prices`  VALUES ( "38","(=)");
INSERT INTO `prices`  VALUES ( "39","(=)");
INSERT INTO `prices`  VALUES ( "40","(=)");
INSERT INTO `prices`  VALUES ( "41","(=)");
INSERT INTO `prices`  VALUES ( "42","(=)");
INSERT INTO `prices`  VALUES ( "43","(=)");
INSERT INTO `prices`  VALUES ( "44","(=)");
INSERT INTO `prices`  VALUES ( "45","(=)");


--
-- Table structure for table `privileges`
--
DROP TABLE  IF EXISTS `privileges`;
CREATE TABLE `privileges` (
  `privilege_id` varchar(255) NOT NULL,
  `description_id` bigint(20) unsigned NOT NULL,
  PRIMARY KEY (`privilege_id`),
  UNIQUE KEY `privilege_id_UNIQUE` (`privilege_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `privileges`  VALUES ( "CANCEL_ORDERS","15");
INSERT INTO `privileges`  VALUES ( "CHANGE_STORE_STATUS","22");
INSERT INTO `privileges`  VALUES ( "CLOSE_ORDERS","16");
INSERT INTO `privileges`  VALUES ( "CREATE_ADMININISTRATOR_ACCOUNT","30");
INSERT INTO `privileges`  VALUES ( "CREATE_ADMIN_PROFILES","26");
INSERT INTO `privileges`  VALUES ( "CREATE_ADVANCED_SETTINGS","34");
INSERT INTO `privileges`  VALUES ( "CREATE_CUSTOMER_ACCOUNT","8");
INSERT INTO `privileges`  VALUES ( "CREATE_EMPLOYEE_ACCOUNT","46");
INSERT INTO `privileges`  VALUES ( "CREATE_MANAGER_ACCOUNT","42");
INSERT INTO `privileges`  VALUES ( "CREATE_ORDERS","12");
INSERT INTO `privileges`  VALUES ( "CREATE_PRODUCT","52");
INSERT INTO `privileges`  VALUES ( "CREATE_PROVINCES","60");
INSERT INTO `privileges`  VALUES ( "CREATE_STORE","21");
INSERT INTO `privileges`  VALUES ( "CREATE_STORE_OWNER_ACCOUNT","38");
INSERT INTO `privileges`  VALUES ( "CREATE_TAX_CLASSES","63");
INSERT INTO `privileges`  VALUES ( "CREATE_TAX_RATES","67");
INSERT INTO `privileges`  VALUES ( "DELETE_ADMININISTRATOR_ACCOUNT","29");
INSERT INTO `privileges`  VALUES ( "DELETE_ADMIN_PROFILES","25");
INSERT INTO `privileges`  VALUES ( "DELETE_ADVANCED_SETTINGS","33");
INSERT INTO `privileges`  VALUES ( "DELETE_CUSTOMER_ACCOUNT","9");
INSERT INTO `privileges`  VALUES ( "DELETE_EMPLOYEE_ACCOUNT","45");
INSERT INTO `privileges`  VALUES ( "DELETE_MANAGER_ACCOUNT","41");
INSERT INTO `privileges`  VALUES ( "DELETE_ORDERS","13");
INSERT INTO `privileges`  VALUES ( "DELETE_PRODUCT","51");
INSERT INTO `privileges`  VALUES ( "DELETE_PROVINCES","59");
INSERT INTO `privileges`  VALUES ( "DELETE_STORE","20");
INSERT INTO `privileges`  VALUES ( "DELETE_STORE_OWNER_ACCOUNT","37");
INSERT INTO `privileges`  VALUES ( "DELETE_TAX_CLASSES","64");
INSERT INTO `privileges`  VALUES ( "DELETE_TAX_RATES","68");
INSERT INTO `privileges`  VALUES ( "EDIT_ADMININISTRATOR_ACCOUNT","28");
INSERT INTO `privileges`  VALUES ( "EDIT_ADMIN_PROFILES","24");
INSERT INTO `privileges`  VALUES ( "EDIT_ADVANCED_SETTINGS","32");
INSERT INTO `privileges`  VALUES ( "EDIT_CATALOG","48");
INSERT INTO `privileges`  VALUES ( "EDIT_COUNTRIES","56");
INSERT INTO `privileges`  VALUES ( "EDIT_CUSTOMER_ACCOUNT","7");
INSERT INTO `privileges`  VALUES ( "EDIT_EMPLOYEE_ACCOUNT","44");
INSERT INTO `privileges`  VALUES ( "EDIT_MANAGER_ACCOUNT","40");
INSERT INTO `privileges`  VALUES ( "EDIT_ORDERS","11");
INSERT INTO `privileges`  VALUES ( "EDIT_PRODUCT","50");
INSERT INTO `privileges`  VALUES ( "EDIT_PROVINCES","58");
INSERT INTO `privileges`  VALUES ( "EDIT_STORE","19");
INSERT INTO `privileges`  VALUES ( "EDIT_STORE_OWNER_ACCOUNT","36");
INSERT INTO `privileges`  VALUES ( "EDIT_TAX_CLASSES","62");
INSERT INTO `privileges`  VALUES ( "EDIT_TAX_RATES","66");
INSERT INTO `privileges`  VALUES ( "PROCESS_ORDERS","14");
INSERT INTO `privileges`  VALUES ( "SET_PRODUCT_PRICE","53");
INSERT INTO `privileges`  VALUES ( "SET_PRODUCT_STOCK","54");
INSERT INTO `privileges`  VALUES ( "SUPER_USER","5");
INSERT INTO `privileges`  VALUES ( "VIEW_ADMININISTRATOR_ACCOUNT","27");
INSERT INTO `privileges`  VALUES ( "VIEW_ADMIN_PROFILES","23");
INSERT INTO `privileges`  VALUES ( "VIEW_ADVANCED_SETTINGS","31");
INSERT INTO `privileges`  VALUES ( "VIEW_CATALOG","47");
INSERT INTO `privileges`  VALUES ( "VIEW_COUNTRIES","55");
INSERT INTO `privileges`  VALUES ( "VIEW_CUSTOMER_ACCOUNT","6");
INSERT INTO `privileges`  VALUES ( "VIEW_EMPLOYEE_ACCOUNT","43");
INSERT INTO `privileges`  VALUES ( "VIEW_MANAGER_ACCOUNT","39");
INSERT INTO `privileges`  VALUES ( "VIEW_ORDERS","10");
INSERT INTO `privileges`  VALUES ( "VIEW_PPRODUCT","49");
INSERT INTO `privileges`  VALUES ( "VIEW_PROVINCES","57");
INSERT INTO `privileges`  VALUES ( "VIEW_STORE","18");
INSERT INTO `privileges`  VALUES ( "VIEW_STORES","17");
INSERT INTO `privileges`  VALUES ( "VIEW_STORE_OWNER_ACCOUNT","35");
INSERT INTO `privileges`  VALUES ( "VIEW_TAX_CLASSES","61");
INSERT INTO `privileges`  VALUES ( "VIEW_TAX_RATES","65");


--
-- Table structure for table `product_group`
--
DROP TABLE  IF EXISTS `product_group`;
CREATE TABLE `product_group` (
  `product_id` int(11) unsigned NOT NULL,
  `member_product_id` int(11) unsigned NOT NULL,
  PRIMARY KEY (`product_id`,`member_product_id`),
  KEY `fk_product_group_products1_idx` (`product_id`),
  KEY `fk_product_group_products2_idx` (`member_product_id`),
  CONSTRAINT `fk_product_group_products1` FOREIGN KEY (`product_id`) REFERENCES `products` (`product_id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `fk_product_group_products2` FOREIGN KEY (`member_product_id`) REFERENCES `products` (`product_id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



--
-- Table structure for table `product_images`
--
DROP TABLE  IF EXISTS `product_images`;
CREATE TABLE `product_images` (
  `product_id` int(11) unsigned NOT NULL,
  `image_id` bigint(20) unsigned NOT NULL,
  PRIMARY KEY (`product_id`,`image_id`),
  KEY `fk_product_images_products1_idx` (`product_id`),
  KEY `fk_product_images_images1_idx` (`image_id`),
  CONSTRAINT `fk_product_images_images1` FOREIGN KEY (`image_id`) REFERENCES `images` (`image_id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `fk_product_images_products1` FOREIGN KEY (`product_id`) REFERENCES `products` (`product_id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `product_images`  VALUES ( "1","1");
INSERT INTO `product_images`  VALUES ( "2","2");
INSERT INTO `product_images`  VALUES ( "8","6");
INSERT INTO `product_images`  VALUES ( "9","8");


--
-- Table structure for table `product_options`
--
DROP TABLE  IF EXISTS `product_options`;
CREATE TABLE `product_options` (
  `option_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `product_id` int(11) unsigned NOT NULL,
  `price_id` bigint(20) unsigned NOT NULL,
  `stock_id` bigint(20) unsigned NOT NULL,
  `image_id` bigint(20) unsigned DEFAULT NULL,
  `ean` varchar(13) DEFAULT NULL,
  `upc` varchar(12) DEFAULT NULL,
  `reference` varchar(32) DEFAULT NULL,
  `is_taxable` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`option_id`,`product_id`),
  KEY `fk_product_options_products1_idx` (`product_id`),
  KEY `fk_product_options_prices1_idx` (`price_id`),
  KEY `fk_product_options_product_stock1_idx` (`stock_id`),
  KEY `fk_product_options_images1_idx` (`image_id`),
  CONSTRAINT `fk_product_options_images1` FOREIGN KEY (`image_id`) REFERENCES `images` (`image_id`) ON DELETE SET NULL ON UPDATE NO ACTION,
  CONSTRAINT `fk_product_options_prices1` FOREIGN KEY (`price_id`) REFERENCES `prices` (`price_id`) ON UPDATE NO ACTION,
  CONSTRAINT `fk_product_options_products1` FOREIGN KEY (`product_id`) REFERENCES `products` (`product_id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `fk_product_options_product_stock1` FOREIGN KEY (`stock_id`) REFERENCES `product_stock` (`stock_id`) ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=40 DEFAULT CHARSET=utf8;

INSERT INTO `product_options`  VALUES ( "1","1","7","1","","","","FK50RDS","0");
INSERT INTO `product_options`  VALUES ( "2","1","8","2","","","","FK50RDM","0");
INSERT INTO `product_options`  VALUES ( "3","1","9","3","","","","FK50RDL","0");
INSERT INTO `product_options`  VALUES ( "4","1","10","4","","","","FK50YLS","0");
INSERT INTO `product_options`  VALUES ( "5","1","11","5","","","","FK50YLM","0");
INSERT INTO `product_options`  VALUES ( "6","1","12","6","","","","FK50YLL","0");
INSERT INTO `product_options`  VALUES ( "7","1","13","7","","","","FK50GRS","0");
INSERT INTO `product_options`  VALUES ( "8","1","14","8","","","","FK50GRM","0");
INSERT INTO `product_options`  VALUES ( "9","1","15","9","","","","FK50GRL","0");
INSERT INTO `product_options`  VALUES ( "10","2","16","10","","","","","0");
INSERT INTO `product_options`  VALUES ( "11","2","17","11","","","","","0");
INSERT INTO `product_options`  VALUES ( "12","2","18","12","","","","","0");
INSERT INTO `product_options`  VALUES ( "13","2","19","13","","","","","0");
INSERT INTO `product_options`  VALUES ( "14","2","20","14","","","","","0");
INSERT INTO `product_options`  VALUES ( "15","2","21","15","","","","","0");
INSERT INTO `product_options`  VALUES ( "16","2","22","16","","","","","0");
INSERT INTO `product_options`  VALUES ( "17","2","23","17","","","","","0");
INSERT INTO `product_options`  VALUES ( "18","2","24","18","","","","","0");
INSERT INTO `product_options`  VALUES ( "34","8","40","34","","","","BGRD","0");
INSERT INTO `product_options`  VALUES ( "35","8","41","35","","","","BGYL","0");
INSERT INTO `product_options`  VALUES ( "36","8","42","36","","","","BGGR","0");
INSERT INTO `product_options`  VALUES ( "37","9","43","37","","","","ANKRD","0");
INSERT INTO `product_options`  VALUES ( "38","9","44","38","","","","ANKYL","0");
INSERT INTO `product_options`  VALUES ( "39","9","45","39","","","","ANKGR","0");


--
-- Table structure for table `product_stock`
--
DROP TABLE  IF EXISTS `product_stock`;
CREATE TABLE `product_stock` (
  `stock_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`stock_id`)
) ENGINE=InnoDB AUTO_INCREMENT=40 DEFAULT CHARSET=utf8;

INSERT INTO `product_stock`  VALUES ( "1");
INSERT INTO `product_stock`  VALUES ( "2");
INSERT INTO `product_stock`  VALUES ( "3");
INSERT INTO `product_stock`  VALUES ( "4");
INSERT INTO `product_stock`  VALUES ( "5");
INSERT INTO `product_stock`  VALUES ( "6");
INSERT INTO `product_stock`  VALUES ( "7");
INSERT INTO `product_stock`  VALUES ( "8");
INSERT INTO `product_stock`  VALUES ( "9");
INSERT INTO `product_stock`  VALUES ( "10");
INSERT INTO `product_stock`  VALUES ( "11");
INSERT INTO `product_stock`  VALUES ( "12");
INSERT INTO `product_stock`  VALUES ( "13");
INSERT INTO `product_stock`  VALUES ( "14");
INSERT INTO `product_stock`  VALUES ( "15");
INSERT INTO `product_stock`  VALUES ( "16");
INSERT INTO `product_stock`  VALUES ( "17");
INSERT INTO `product_stock`  VALUES ( "18");
INSERT INTO `product_stock`  VALUES ( "19");
INSERT INTO `product_stock`  VALUES ( "20");
INSERT INTO `product_stock`  VALUES ( "21");
INSERT INTO `product_stock`  VALUES ( "22");
INSERT INTO `product_stock`  VALUES ( "23");
INSERT INTO `product_stock`  VALUES ( "24");
INSERT INTO `product_stock`  VALUES ( "25");
INSERT INTO `product_stock`  VALUES ( "26");
INSERT INTO `product_stock`  VALUES ( "27");
INSERT INTO `product_stock`  VALUES ( "28");
INSERT INTO `product_stock`  VALUES ( "29");
INSERT INTO `product_stock`  VALUES ( "30");
INSERT INTO `product_stock`  VALUES ( "31");
INSERT INTO `product_stock`  VALUES ( "32");
INSERT INTO `product_stock`  VALUES ( "33");
INSERT INTO `product_stock`  VALUES ( "34");
INSERT INTO `product_stock`  VALUES ( "35");
INSERT INTO `product_stock`  VALUES ( "36");
INSERT INTO `product_stock`  VALUES ( "37");
INSERT INTO `product_stock`  VALUES ( "38");
INSERT INTO `product_stock`  VALUES ( "39");


--
-- Table structure for table `product_stock_quantity`
--
DROP TABLE  IF EXISTS `product_stock_quantity`;
CREATE TABLE `product_stock_quantity` (
  `stock_id` bigint(20) unsigned NOT NULL,
  `store_id` smallint(5) unsigned NOT NULL,
  `quantity` float unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`stock_id`,`store_id`),
  KEY `fk_product_stock_quantity_shops1_idx` (`store_id`),
  CONSTRAINT `fk_product_stock_quantity_product_stock1` FOREIGN KEY (`stock_id`) REFERENCES `product_stock` (`stock_id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `fk_product_stock_quantity_shops1` FOREIGN KEY (`store_id`) REFERENCES `stores` (`store_id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `product_stock_quantity`  VALUES ( "1","1","10");
INSERT INTO `product_stock_quantity`  VALUES ( "1","2","10");
INSERT INTO `product_stock_quantity`  VALUES ( "2","1","20");
INSERT INTO `product_stock_quantity`  VALUES ( "2","2","20");
INSERT INTO `product_stock_quantity`  VALUES ( "3","1","10");
INSERT INTO `product_stock_quantity`  VALUES ( "3","2","10");
INSERT INTO `product_stock_quantity`  VALUES ( "4","1","10");
INSERT INTO `product_stock_quantity`  VALUES ( "4","2","10");
INSERT INTO `product_stock_quantity`  VALUES ( "5","1","10");
INSERT INTO `product_stock_quantity`  VALUES ( "5","2","10");
INSERT INTO `product_stock_quantity`  VALUES ( "6","1","10");
INSERT INTO `product_stock_quantity`  VALUES ( "6","2","10");
INSERT INTO `product_stock_quantity`  VALUES ( "7","1","10");
INSERT INTO `product_stock_quantity`  VALUES ( "7","2","10");
INSERT INTO `product_stock_quantity`  VALUES ( "8","1","20");
INSERT INTO `product_stock_quantity`  VALUES ( "8","2","20");
INSERT INTO `product_stock_quantity`  VALUES ( "9","1","15");
INSERT INTO `product_stock_quantity`  VALUES ( "9","2","15");
INSERT INTO `product_stock_quantity`  VALUES ( "25","1","10");
INSERT INTO `product_stock_quantity`  VALUES ( "25","2","10");
INSERT INTO `product_stock_quantity`  VALUES ( "26","1","10");
INSERT INTO `product_stock_quantity`  VALUES ( "26","2","10");
INSERT INTO `product_stock_quantity`  VALUES ( "27","1","10");
INSERT INTO `product_stock_quantity`  VALUES ( "27","2","10");
INSERT INTO `product_stock_quantity`  VALUES ( "28","1","10");
INSERT INTO `product_stock_quantity`  VALUES ( "28","2","10");
INSERT INTO `product_stock_quantity`  VALUES ( "29","1","10");
INSERT INTO `product_stock_quantity`  VALUES ( "29","2","10");
INSERT INTO `product_stock_quantity`  VALUES ( "30","1","10");
INSERT INTO `product_stock_quantity`  VALUES ( "30","2","10");
INSERT INTO `product_stock_quantity`  VALUES ( "31","1","10");
INSERT INTO `product_stock_quantity`  VALUES ( "31","2","10");
INSERT INTO `product_stock_quantity`  VALUES ( "32","1","10");
INSERT INTO `product_stock_quantity`  VALUES ( "32","2","10");
INSERT INTO `product_stock_quantity`  VALUES ( "33","1","10");
INSERT INTO `product_stock_quantity`  VALUES ( "33","2","10");
INSERT INTO `product_stock_quantity`  VALUES ( "34","1","10");
INSERT INTO `product_stock_quantity`  VALUES ( "34","2","10");
INSERT INTO `product_stock_quantity`  VALUES ( "35","1","10");
INSERT INTO `product_stock_quantity`  VALUES ( "35","2","10");
INSERT INTO `product_stock_quantity`  VALUES ( "36","1","10");
INSERT INTO `product_stock_quantity`  VALUES ( "36","2","10");
INSERT INTO `product_stock_quantity`  VALUES ( "37","1","10");
INSERT INTO `product_stock_quantity`  VALUES ( "37","2","10");
INSERT INTO `product_stock_quantity`  VALUES ( "38","1","10");
INSERT INTO `product_stock_quantity`  VALUES ( "38","2","10");
INSERT INTO `product_stock_quantity`  VALUES ( "39","1","10");
INSERT INTO `product_stock_quantity`  VALUES ( "39","2","10");


--
-- Table structure for table `product_types`
--
DROP TABLE  IF EXISTS `product_types`;
CREATE TABLE `product_types` (
  `product_type_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `name_plural` varchar(255) DEFAULT NULL,
  `image_id` bigint(20) unsigned DEFAULT NULL,
  PRIMARY KEY (`product_type_id`),
  KEY `fk_image_id_idx` (`image_id`),
  CONSTRAINT `fk_image_id` FOREIGN KEY (`image_id`) REFERENCES `images` (`image_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

INSERT INTO `product_types`  VALUES ( "1","TShirt","TShirts","");
INSERT INTO `product_types`  VALUES ( "2","Bag","Bags","");


--
-- Table structure for table `product_types_to_attribute_types`
--
DROP TABLE  IF EXISTS `product_types_to_attribute_types`;
CREATE TABLE `product_types_to_attribute_types` (
  `product_type_id` int(11) unsigned NOT NULL,
  `attribute_type_id` int(11) unsigned NOT NULL,
  `sort_order` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'This is actually NOT a table wide sorting. It is local to product_types to attribute_types associations.',
  PRIMARY KEY (`product_type_id`,`attribute_type_id`),
  KEY `fk_attribute_group_id_idx` (`attribute_type_id`),
  CONSTRAINT `fk_attribute_type_id1233` FOREIGN KEY (`attribute_type_id`) REFERENCES `attribute_types` (`attribute_type_id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `fk_product_type_id1223` FOREIGN KEY (`product_type_id`) REFERENCES `product_types` (`product_type_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `product_types_to_attribute_types`  VALUES ( "1","1","0");
INSERT INTO `product_types_to_attribute_types`  VALUES ( "1","2","0");
INSERT INTO `product_types_to_attribute_types`  VALUES ( "2","1","0");


--
-- Table structure for table `products`
--
DROP TABLE  IF EXISTS `products`;
CREATE TABLE `products` (
  `product_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `product_type_id` int(11) unsigned NOT NULL,
  `description_id` bigint(20) unsigned NOT NULL,
  `styling_ideas_text_id` bigint(20) unsigned NOT NULL,
  `default_option_id` bigint(20) unsigned DEFAULT NULL,
  `primary_image_id` bigint(20) unsigned DEFAULT NULL,
  `url_rewrite_id` bigint(20) unsigned NOT NULL,
  `ean` varchar(13) DEFAULT NULL,
  `upc` varchar(12) DEFAULT NULL,
  `reference` varchar(32) DEFAULT NULL,
  `date_added` datetime NOT NULL,
  `last_modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `date_available` datetime DEFAULT NULL,
  `product_weight` float NOT NULL DEFAULT '0',
  `product_status` tinyint(1) NOT NULL DEFAULT '0',
  `manufacturers_id` int(11) unsigned DEFAULT NULL,
  `quantity_order_min` float NOT NULL DEFAULT '1',
  `is_free` tinyint(1) NOT NULL DEFAULT '0',
  `is_call` tinyint(1) NOT NULL DEFAULT '0',
  `is_always_free_shipping` tinyint(1) NOT NULL DEFAULT '0',
  `quantity_order_max` float NOT NULL DEFAULT '0',
  `sort_order` int(11) NOT NULL DEFAULT '0',
  `discount_type` tinyint(1) NOT NULL DEFAULT '0',
  `hatched` tinyint(1) NOT NULL DEFAULT '0',
  `display_status` tinyint(1) NOT NULL DEFAULT '1',
  `total_sold` float NOT NULL DEFAULT '0',
  `views` int(10) unsigned NOT NULL DEFAULT '0',
  `wishlists` int(10) unsigned NOT NULL DEFAULT '0',
  `price_by_attribute` enum('flat','option','color','size') NOT NULL DEFAULT 'flat',
  `meta_keywords_text_id` bigint(20) unsigned NOT NULL,
  `meta_description_text_id` bigint(20) unsigned NOT NULL,
  PRIMARY KEY (`product_id`),
  KEY `idx_products_date_added_zen` (`date_added`),
  KEY `idx_products_status_zen` (`product_status`),
  KEY `idx_products_date_available_zen` (`date_available`),
  KEY `idx_products_ordered_zen` (`total_sold`),
  KEY `idx_products_sort_order_zen` (`sort_order`),
  KEY `idx_manufacturers_id_zen` (`manufacturers_id`),
  KEY `fk_products_text2_idx` (`meta_description_text_id`),
  KEY `fk_products_text3_idx` (`styling_ideas_text_id`),
  KEY `fk_products_product_options1_idx` (`default_option_id`),
  KEY `fk_product_type_id_idx` (`product_type_id`),
  KEY `fk_products_text1_idx` (`meta_keywords_text_id`),
  KEY `fk_product_description_id_idx` (`description_id`),
  KEY `fk_product_primary_image_idx` (`primary_image_id`),
  CONSTRAINT `fk_products_product_options1` FOREIGN KEY (`default_option_id`) REFERENCES `product_options` (`option_id`) ON DELETE SET NULL ON UPDATE NO ACTION,
  CONSTRAINT `fk_products_text1` FOREIGN KEY (`meta_keywords_text_id`) REFERENCES `text` (`text_id`) ON UPDATE NO ACTION,
  CONSTRAINT `fk_products_text2` FOREIGN KEY (`meta_description_text_id`) REFERENCES `text` (`text_id`) ON UPDATE NO ACTION,
  CONSTRAINT `fk_products_text3` FOREIGN KEY (`styling_ideas_text_id`) REFERENCES `text` (`text_id`) ON UPDATE NO ACTION,
  CONSTRAINT `fk_product_description_id` FOREIGN KEY (`description_id`) REFERENCES `description` (`description_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_product_primary_image` FOREIGN KEY (`primary_image_id`) REFERENCES `images` (`image_id`) ON DELETE SET NULL ON UPDATE NO ACTION,
  CONSTRAINT `fk_product_type_id` FOREIGN KEY (`product_type_id`) REFERENCES `product_types` (`product_type_id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;

INSERT INTO `products`  VALUES ( "1","1","98","7","1","1","6","","","","2014-12-26 03:36:55","2014-12-28 07:11:51","","0","0","","1","0","0","0","0","0","0","0","1","0","0","0","flat","8","9");
INSERT INTO `products`  VALUES ( "2","1","99","10","10","2","7","","","","2014-12-26 03:36:55","2014-12-28 05:38:54","","0","0","","1","0","0","0","0","0","0","0","1","0","0","0","flat","11","12");
INSERT INTO `products`  VALUES ( "8","2","110","30","34","6","14","","","","2014-12-27 03:47:56","2014-12-28 05:38:49","","0","0","","1","0","0","0","0","0","0","0","1","0","0","0","flat","31","32");
INSERT INTO `products`  VALUES ( "9","2","111","33","37","8","15","","","","2014-12-27 07:46:44","2014-12-28 05:38:26","","0","0","","1","0","0","0","0","0","0","0","1","0","0","0","flat","34","35");


--
-- Table structure for table `products_discount_quantity`
--
DROP TABLE  IF EXISTS `products_discount_quantity`;
CREATE TABLE `products_discount_quantity` (
  `discount_id` int(4) unsigned NOT NULL DEFAULT '0',
  `products_id` int(11) unsigned NOT NULL DEFAULT '0',
  `discount_qty` float NOT NULL DEFAULT '0',
  `discount_price` decimal(15,4) NOT NULL DEFAULT '0.0000',
  KEY `idx_id_qty_zen` (`products_id`,`discount_qty`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;



--
-- Table structure for table `products_notifications`
--
DROP TABLE  IF EXISTS `products_notifications`;
CREATE TABLE `products_notifications` (
  `products_id` int(11) NOT NULL DEFAULT '0',
  `customers_id` int(11) NOT NULL DEFAULT '0',
  `date_added` datetime NOT NULL DEFAULT '0001-01-01 00:00:00',
  PRIMARY KEY (`products_id`,`customers_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;



--
-- Table structure for table `products_on_sale`
--
DROP TABLE  IF EXISTS `products_on_sale`;
CREATE TABLE `products_on_sale` (
  `sale_id` int(11) unsigned NOT NULL,
  `product_id` int(11) unsigned NOT NULL,
  PRIMARY KEY (`sale_id`,`product_id`),
  UNIQUE KEY `product_id_UNIQUE` (`product_id`),
  KEY `fk_products_on_sale_sale1_idx` (`sale_id`),
  KEY `fk_products_on_sale_products1_idx` (`product_id`),
  CONSTRAINT `fk_products_on_sale_products1` FOREIGN KEY (`product_id`) REFERENCES `products` (`product_id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `fk_products_on_sale_sale1` FOREIGN KEY (`sale_id`) REFERENCES `sale` (`sale_id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `products_on_sale`  VALUES ( "1","1");


--
-- Table structure for table `products_to_coupons`
--
DROP TABLE  IF EXISTS `products_to_coupons`;
CREATE TABLE `products_to_coupons` (
  `coupon_id` int(10) unsigned NOT NULL,
  `product_id` int(11) unsigned NOT NULL,
  PRIMARY KEY (`coupon_id`,`product_id`),
  KEY `fk_products_to_coupons_coupons1_idx` (`coupon_id`),
  KEY `fk_products_to_coupons_products1_idx` (`product_id`),
  CONSTRAINT `fk_products_to_coupons_coupons1` FOREIGN KEY (`coupon_id`) REFERENCES `coupons` (`coupon_id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `fk_products_to_coupons_products1` FOREIGN KEY (`product_id`) REFERENCES `products` (`product_id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



--
-- Table structure for table `products_to_tags`
--
DROP TABLE  IF EXISTS `products_to_tags`;
CREATE TABLE `products_to_tags` (
  `product_id` int(11) unsigned NOT NULL DEFAULT '0',
  `tag_id` int(11) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`product_id`,`tag_id`),
  KEY `idx_tag_prod_id` (`tag_id`,`product_id`),
  KEY `fk_products_to_tags_products1_idx` (`product_id`),
  KEY `fk_products_to_tags_tags1_idx` (`tag_id`),
  CONSTRAINT `fk_products_to_tags_products10` FOREIGN KEY (`product_id`) REFERENCES `products` (`product_id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `fk_products_to_tags_tags1` FOREIGN KEY (`tag_id`) REFERENCES `tags` (`tag_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `products_to_tags`  VALUES ( "1","1");
INSERT INTO `products_to_tags`  VALUES ( "1","3");
INSERT INTO `products_to_tags`  VALUES ( "2","2");
INSERT INTO `products_to_tags`  VALUES ( "2","3");
INSERT INTO `products_to_tags`  VALUES ( "8","3");
INSERT INTO `products_to_tags`  VALUES ( "9","1");


--
-- Table structure for table `project_version`
--
DROP TABLE  IF EXISTS `project_version`;
CREATE TABLE `project_version` (
  `project_version_id` tinyint(3) NOT NULL AUTO_INCREMENT,
  `project_version_key` varchar(40) NOT NULL DEFAULT '',
  `project_version_major` varchar(20) NOT NULL DEFAULT '',
  `project_version_minor` varchar(20) NOT NULL DEFAULT '',
  `project_version_patch1` varchar(20) NOT NULL DEFAULT '',
  `project_version_patch2` varchar(20) NOT NULL DEFAULT '',
  `project_version_patch1_source` varchar(20) NOT NULL DEFAULT '',
  `project_version_patch2_source` varchar(20) NOT NULL DEFAULT '',
  `project_version_comment` varchar(250) NOT NULL DEFAULT '',
  `project_version_date_applied` datetime NOT NULL DEFAULT '0001-01-01 01:01:01',
  PRIMARY KEY (`project_version_id`),
  UNIQUE KEY `idx_project_version_key_zen` (`project_version_key`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='Database Version Tracking';



--
-- Table structure for table `project_version_history`
--
DROP TABLE  IF EXISTS `project_version_history`;
CREATE TABLE `project_version_history` (
  `project_version_id` tinyint(3) NOT NULL AUTO_INCREMENT,
  `project_version_key` varchar(40) NOT NULL DEFAULT '',
  `project_version_major` varchar(20) NOT NULL DEFAULT '',
  `project_version_minor` varchar(20) NOT NULL DEFAULT '',
  `project_version_patch` varchar(20) NOT NULL DEFAULT '',
  `project_version_comment` varchar(250) NOT NULL DEFAULT '',
  `project_version_date_applied` datetime NOT NULL DEFAULT '0001-01-01 01:01:01',
  PRIMARY KEY (`project_version_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='Database Version Tracking History';



--
-- Table structure for table `provinces`
--
DROP TABLE  IF EXISTS `provinces`;
CREATE TABLE `provinces` (
  `province_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `country_code` char(2) NOT NULL,
  `iso_code` varchar(4) NOT NULL,
  `printable_name` varchar(160) DEFAULT NULL,
  `name` varchar(160) DEFAULT NULL,
  `date_added` datetime NOT NULL,
  `last_modified` datetime NOT NULL,
  PRIMARY KEY (`province_id`,`country_code`),
  UNIQUE KEY `iso_code_UNIQUE` (`iso_code`),
  KEY `fk_provinces_countries1_idx` (`country_code`),
  CONSTRAINT `fk_provinces_countries1` FOREIGN KEY (`country_code`) REFERENCES `countries` (`country_code`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

INSERT INTO `provinces`  VALUES ( "1","NG","LA","Lagos","LAGOS","2014-12-26 03:36:54","2014-12-26 03:36:54");
INSERT INTO `provinces`  VALUES ( "2","NG","KW","Kwara","KWARA","2014-12-26 03:36:54","2014-12-26 03:36:54");
INSERT INTO `provinces`  VALUES ( "3","NG","KD","Kaduna","KADUNA","2014-12-26 03:36:54","2014-12-26 03:36:54");
INSERT INTO `provinces`  VALUES ( "4","NG","AB","Abia","ABIA","2014-12-26 03:36:54","2014-12-26 03:36:54");
INSERT INTO `provinces`  VALUES ( "5","NG","PH","Port Harcourt","PORT HARCOURT","2014-12-26 03:36:54","2014-12-26 03:36:54");


--
-- Table structure for table `recipient`
--
DROP TABLE  IF EXISTS `recipient`;
CREATE TABLE `recipient` (
  `order_id` int(11) unsigned NOT NULL,
  `entry_gender` char(1) NOT NULL DEFAULT '',
  `entry_company` varchar(64) DEFAULT NULL,
  `entry_firstname` varchar(32) NOT NULL DEFAULT '',
  `entry_lastname` varchar(32) NOT NULL DEFAULT '',
  `location_id` bigint(20) unsigned NOT NULL,
  PRIMARY KEY (`order_id`),
  KEY `idx_recipient_order_id` (`order_id`),
  KEY `fk_recipient_locations1_idx` (`location_id`),
  CONSTRAINT `fk_recipient_locations1` FOREIGN KEY (`location_id`) REFERENCES `locations` (`location_id`) ON UPDATE NO ACTION,
  CONSTRAINT `fk_recipient_orders1` FOREIGN KEY (`order_id`) REFERENCES `orders` (`order_id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



--
-- Table structure for table `refunds`
--
DROP TABLE  IF EXISTS `refunds`;
CREATE TABLE `refunds` (
  `refund_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `transaction_id` bigint(20) unsigned NOT NULL,
  `amount` decimal(11,2) DEFAULT NULL,
  `currency_code` char(3) NOT NULL,
  `type` enum('full','partial') NOT NULL,
  PRIMARY KEY (`refund_id`,`transaction_id`),
  KEY `fk_refunds_transactions1_idx` (`transaction_id`),
  KEY `fk_refunds_currencies1_idx` (`currency_code`),
  CONSTRAINT `fk_refunds_currencies1` FOREIGN KEY (`currency_code`) REFERENCES `currencies` (`currency_code`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `fk_refunds_transactions1` FOREIGN KEY (`transaction_id`) REFERENCES `transactions` (`transaction_id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



--
-- Table structure for table `sale`
--
DROP TABLE  IF EXISTS `sale`;
CREATE TABLE `sale` (
  `sale_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `store_id` smallint(5) unsigned NOT NULL,
  `description_id` bigint(20) unsigned NOT NULL,
  `image_id` bigint(20) unsigned DEFAULT NULL,
  `discount_type` varchar(20) NOT NULL DEFAULT '(%)',
  `amount` decimal(15,4) NOT NULL DEFAULT '0.0000',
  `sale_begin` datetime DEFAULT NULL,
  `sale_end` datetime DEFAULT NULL,
  `indefinite` tinyint(1) unsigned DEFAULT '0',
  `price_min` decimal(5,4) DEFAULT NULL,
  `price_max` decimal(5,4) DEFAULT NULL,
  `sort_order` int(10) unsigned NOT NULL DEFAULT '0',
  `is_active` tinyint(1) unsigned NOT NULL,
  `date_created` datetime NOT NULL,
  `last_modified` datetime NOT NULL,
  PRIMARY KEY (`sale_id`,`store_id`),
  KEY `fk_sale_images1_idx` (`image_id`),
  KEY `fk_sale_description1_idx` (`description_id`),
  KEY `fk_sale_stores1_idx` (`store_id`),
  CONSTRAINT `fk_sale_description1` FOREIGN KEY (`description_id`) REFERENCES `description` (`description_id`) ON UPDATE NO ACTION,
  CONSTRAINT `fk_sale_images1` FOREIGN KEY (`image_id`) REFERENCES `images` (`image_id`) ON DELETE SET NULL ON UPDATE NO ACTION,
  CONSTRAINT `fk_sale_stores1` FOREIGN KEY (`store_id`) REFERENCES `stores` (`store_id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

INSERT INTO `sale`  VALUES ( "1","1","103","","(-)(%)","50.0000","","","1","","","0","1","2014-12-26 15:42:41","2014-12-26 15:42:41");


--
-- Table structure for table `sessions`
--
DROP TABLE  IF EXISTS `sessions`;
CREATE TABLE `sessions` (
  `sesskey` varchar(255) NOT NULL DEFAULT '',
  `expiry` datetime NOT NULL,
  `value` mediumblob NOT NULL,
  PRIMARY KEY (`sesskey`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `sessions`  VALUES ( "gae4dufo5qjkh74rolhdhqdbj1","2014-12-27 13:22:19","zjbk4fmKDUCcEX5a7TgWRU9nv0tZciWmEehfXlh4/OvrCpcZ/+3sm9iaMKmcTVShO9WNfOrrBBfs/UzC99bFB01T71fcoXFL5m1Ob6ZNjuZ+iU+xAodKqcvl9V1hi3XDlMsienSLzT8kkYj1qyxLijxYLKS70uCEJxvZ5n58Xli4MWpOpJkvPW3qVcANm8yOZergjHE+4wql7lRswpDqCU9KsRoDVaanXs3Sw882Sx1sBE4boMjvdYUfaUoR/lLZgarUA0pIafCM/3ByegDzczHKk+wweOxR5A9ZNlhLDi+JrGGuCuZ8wjs5+cvOvr3xQtoGTmaV9pZRvqs6+eUUbaVg2tucDJe5tH7mi84C84F4p6FhfnVTn261FYvaPseq4pbHniWvGNqLb/JuiQNL97WfOtK4csMjBu9tXLBYQQ6A1726o7ax4Nevk7vP7WwuazffMGb9YkClqBT0dSgy4B9q/126IVtb1+Rq5cyljDBFD75DVqy3J+fT/OYka+uQzYyQWzvLBbomshqifXHZ/NjcC8T2XtK+eQ8SRd3fvrs=");
INSERT INTO `sessions`  VALUES ( "gjggbgbd6gdebsmnj6ffjlaut1","2014-12-27 17:15:58","m9181OWdxaP5Ta5YYLdONU6qDeSVCIqZ6GAew+61osV5vy3lql0byZThCOWWnJux8x+s9lQZn+mMq8HXmmEf77mQ4EcIMGTDh43GR/5d/PJZ2Nw2MeHGfOc4cLFtQThgWix7fPVVlIqUd6GqjKD9nHHiPeACNB5PWPpE9F/tmcXT8HRMhno0puA3q8GkDfT8Noxm6EUjxXVMbYBQcO6psK/4lNumqIkh41FgGpUteeFB3UnHQdKGXPPDIBcgQwJOMMgRR/gMceiUl/kLoMTluVTYoKx/oQ8IdF0+g0lc3MkidTdb+U/yUCSq6BL6Vh6NLEzORfPXYJnepr7l30lr+7PAdNNJmqaXBINgfDcnsXoyXAor+KjiKXm0MRSOaQpbkWUmZt8JbZe6pk6HxWenqvZI74PDlrsTBcgH9nDy8ghMOYeBucU/atuQZUDaluKveHuEYqcDcYI05fy2UpBJoMasingsjmNPD7/aIwpxg/7WAYjipR3BvHE7AIBqFI9BhiTEdKQBY6ABg4hPjKBpH1cZgn0C2HlhM2StiZxjxc8=");
INSERT INTO `sessions`  VALUES ( "l2nbaddfi7c3mcv2416t6o3ab1","2014-12-28 07:11:51","kTM0tknC1XCjuyKqWT7TiKixJpFWT41r/KNwSudMotoh9SRHJ8Lx5ft03AX01TN1Hjpsyfsnt/s2PLX4uDj3s7Y50N9C/j6W1Dy70O2rKZ/nEvtu1Y6rhakuQaiSwEHfdilR/NKwvG8fCkOXmI8wQvcpFBtfFyQqRMqCv0NSx9DgaR1mYa31WFSAvu3BryYSd+oF6kLNYAxRkob3BIPrkkvA4bh9aB/VpsKaUFztAP0e1wdrK/YcjyVma8a3oOS2PeS8H17mX1+AoLOtuXPthEtsZlMc8YRHiT1InD0eT/Qmk4cuC2MKTvvSpG1iTwZFTb69mEgO5Rh0Chkerf2aTK2bbk1uinBKMfcx6hBKYihKnz8oJk8VDr0oTH/e0779u5IFx8N/xtnfO7b0xZVG1B3IeVk9cnTnCEUMKNPiWRhnjzFW3OPZ0T3iF0O13nG01wbRReNIqZ8E0oXiuxD1z6YRx3/kLjC20ZhMhZmFcDRaqbsSHkqNqmIRTsfj5ycTawC2LUB3Sxkn7zANE7IErKLOVxAd5ax/RwGJd8rEjkE=");
INSERT INTO `sessions`  VALUES ( "nl7ud216jn44l57jp07f57jth0","2014-12-27 08:47:45","gwtVfibMxgFrrj+c4EJPT4daXexX7DQ81WiKZGLYtgRjUQPRdILNOlq4M2Wd7QVOLmxbn1JQyOQUpQf1XZ0ltegvpZ0RfltITX59dU37XI5iSv7f289hcQdS+vwUvfesonQv9FhS7EdRM2FlY8ZLLCMSUmyg4q5df/en8tOGmSovRNJns18jXSxW3vPAlie6jo9JIBjVsU0rJYvxBusPRsUZtRGkQhdegspf08b1DiOjzRtQNRWCAky5J72yqeuK+jpCFLM0xfVqqV5llkM4imlxCH/khViJZqDtA3lPyweFvd1tJA/5iWbO1Ti5fLDMNG+pZzW9X0qojcYC2Nvt1PS7lmRvq/hNpYc1vlb0Za7FYKZZy/0RJZa5u9s4rfrqtpTYgXkQVk2eBm2wsXQmo3BtTFYPXEefsYXz0Af9C1CAZh2IZTN9jpxjp+oHtXl4U5ud1jU2rW2HJ+1tYgbog4pPNwEM6hrEs3Aq/QdduAMdCLPRVfpIRrK8D+Or9eLI6HfdECLidJ+/iaECuejlCh4nDPgUry/lkiuotWxXs4pc2njEY+Y9XR0EF4ydvLiUiAvfg8aZbtTCEM7xUJfKCGWtdCCdpCksiVI7CFzEE5V575lo4Mlsl4OPB9vjZMtnmO2FvAC/8Lhs1t5PeWkdENuuXJKyuQcfjrQNybAMQhcjrDgFAe7Tpu6x4uSNNnH8HE0V0WMSce++qgducrlmjUfO5GuHp7oUtg7V1AxOWOXuYPBqaf5yrvHIH0Oka8kDHp2aJF8DpnCUUNFGs0YoyikQrKLa+jLvwk52dXZWZY7rZB2/hFU+968h5e7wbSNhxED/Ff0OpKXX5G1jdYDCV9FLXt5SLYI+RdPD1JMNBO701bPkP9cMgpTsnoY+BJpDqNmhyFpPH0eIzoZgnKco6JIdZPZR4o2hHdDuY5efcKdfxtPpk46jPfvgtNnIzDgeZ9yRWBjHsiFVuhe8RYf2s2M4hLCgkdBE7ElR/ieTCWDa/fy0vGo5Ugo992rOC18sUBL0S8yeoNIRoQIU5ygG5ageBIpHsDvZiB5fTgog0uN/HsjrXwgkS473zaqsvNl9ap1uocjfsqiRneNBfxb+HFbMjymh6sIZ7ylNVBpkA3Aghwm3iJX+1oCjA0cOS3HIekiTDBdk8mh0p3t16LTPq3daol6OlQsjgUpVdJyCuYXwKUInLtDkAO32MU37ITu7eQiLaqogwwpAr3JxjVHh697kAFFrSHi4V+Ja1QN7+HUbadv7jyQokw5Osdh33RNI3BHU/4MxGRIF1EwplVOjyVmTkl3cLT1gTlvUrN23sMd9Kn7DD2nvAAaYNEv1277b4SPA9gGRZydBEgNUbL/6DQ8dIrI/H8d0QuYwW6cNu3jwbq6z4exYCY0HMryMnm3a5b/kOJdRa2Sez87wN4dld/gvzkCoiFfcqi4YOHF+GH5uL78fc6+x/nf8BmskfEqg8kUfh9u4DN7Bh39JoJctAJjcf4pmtwUkYvrBZ1znBVAngVWBhLrVo+qsyoEHMQn28a+q95WsZfqj4UToMVYzNLeAZQzgJ8ALuCd4y1uo+CIkTQj+L79yboiL1CSptGRu6svc+b3ng7YLFnPF0kp4rk1iaj4Erw0cSXh9tPa2c/vEA+kfx/Gg5uj4gWtOCOQjRosXX/pXWUxidD9FQVD8D0Ootbjwaqe5H7SCMDUGurm2bU0RTGkDCEy8NRwBjJLmThfEml8M/3wI/yUWTm/w+Tg5wM3889PzEpb3h5WhGjq5pgMedbjpfNxVT1PfYb/iuwITOWG33Wj2mkIeZmsfcYc2LSvBuI46Y6WBev13TIuvYRGaRXFBd2FzL53Mzs1vOj5IU95jtGA+IIv3ep3RD88NfCja3oRnjGfIuj0Zr6FRPXPIXUhw2pMIrKO4Ssp4jsJcxa9qykfMILD84HNKdL8nhmbcf8U2sbUxGQ6L2wzrew8hCe8RNP48FF5y0hRV99wyMv/HANBoXVUtFVa2TQ6Zs3t6E/ZhRy1DWNDjBVct5gZdVUdKf06VKDKHl87iO5FcIN15y4LsQ2RSgyudyYPOO4VzGU++0PJuAnj9cIYFKd5NjYax7DHKBJMG3O2IMiXB6iAtYUcljJXnLVmrb8Q5j/urQYgNuMjPJh1pwRV3VcDLmu0JfX6jlKkM5hUrj2Rfi1NQO5TFF8VtAwhT9RkcKD9SQAw2XSTepUrxe4qm5OLQv+OZyN8Zcvf7jJMNnfsYukWKuV+JXBbAvs8xmsSBgQXuFY0ReKcxOdlSKB9Gip+WtCOarWBAIU7hw9/+MpJ2ynts4NgTdjjMbFCXXaE9dmeSg0Pq2LUUWxx5AUDPKgNJlqKGANXhH48Ff2Xin/kCN9Qj0p51iM7PRV12VbFiczs8cqyJsErbWskSlQBz5XxITaUbBBIxEpsezGtoOW4zT2XlAJY4u9Ni/ZX/lE7yzjTGDAgn3u7rYVuU6VbD7bZVjNz2wcdDCjvjABzNB0zpk1jskMhZAV1swZZBseW9DHvcR0dt0WEavs8ozWZLaRiLyRQN4vd48Ena9q0wcV55LkGu/VjvuRp7Oincl/iRCy2JXHFVBLKaQXEVN5wUhxiiOvs0U3d1luB44FnVWmfxTabXSyGxTvjCWUoeY34DeQeWyIWmHvJ4VxyceOlzZ2cLx4wjWWkLLTE48wGqcQewQNEvGcAPTcFkzU8QWQPljDDd4QMJMw1xjKgG0waj/WQPKOktn/BBDE+TotjMZ5qyWaVJFjB3xiIfp+s2rw0pxryiXv3T7DYph0UUFDialm9YEc8bwXi18bAUuaz+67pDIlpX4Qi6LmYhuLP3iKjfFBispD53CwXtyITs2uhr13pTYicNHpYxRz2hn4SPrkR26DPtPg73Y70KZMyWu97yViSxl6CF5CtdzvMwtLVfIOPWBJFeM/f+Sch28HgUn87HvlE3PXBXksdds2hAdWhyxafIUEEoA4+OToN811clMd161fyCE4E4dKnuQfVm/i1Gyex4FZptuDLf7HQ+uG8IWnskPENoWdx0sPOWZET2zriJXIUiNVl1q4wfrxDP7VPAjIe2VYe0QMAZPruZcBy/s41igVQG/GLiG7H0f8W7Ff8J172XqRvjgxKBOWxEXgkXWro5MDMMbzPCCahQPqSTK8KsEPkHPLosoTEn5F8E2UwYltv9/qfEik2Pm6lMfrE4znAJ2XEjfAkoOtZNO5Ron6Efc+SRqBiv/rVTDauC30ICTeYnTnWPrmrTahu9p9IJgGmERT5JAktHC+4UeVmLUm8NXmrWB/3++ROJDlCt55gyBxoHWQjyz5Lz+GRbTauPc7fBfKpxh2j7YiPesWY8SedIjqIHaoexcE9MZ/FBqdpxXb0nGx15b5rVi0ftbOO4R5LalMgofHaUgbxHVgkAIh1FDH5+m1ad4skvRi/oaJJqOsGko1dQhl7mw5p77OYxdHINyQFHJHp8swjTk4+jT6BHGNBS9lsUGVdGwUZclyivIreJiP/C+QT8/+cx7+Pa5vvgFWY+Dyz6IYIePL+TyOvB3f7EapkRbD2Gv6M9fN/9wjPJ0BbfvKpYMuKv+DzPAY3O1xVNpViHMH8qBiiDr+CzoXv+tO9W7S5kZXz8sXg3d3T/OF1BAMnDqgoHKKYNx+LHAWoHhQCWd2gxqW2prIBc8YUv7ZodmfeWkKrrSgcKRjnew6c7LWZZI9i3UG15Vd8A1gKG7BzFv8j4ZJxgeSdgCqQJqbx5t7AJcM8YBH4+pZyCf4/Y7pInIjPcTRmg6C/gk+f4sYyCU8TRJxJQ02EzjcGG57amcuKJqJoASy8lU5+TZI27V84Ol5p0cMQBGzYNtyaj//IZBUU84l94igfoGhJKIRy+e+WIk2jdcGe8IROOZYgUBIqwpKmsbgUaILF6ymRcI8ivGzNxPak7WoSZVYWRJqwAeSRKaecw2HYG9/cHJ9WiSuuV29iYdjIC7SSFZN1hp03hzjpKCHY2q+wCUdOM6TiWtCh8jqD1jsrr0Vzgsk9qT7IM6n4VXpjqn/TX0b+Dg1utTw6lmKZNv0v811NOGshK9o2uTPFIqie3Am/fO0wlurec0BxlSIgswyez9/D1f9xF0vQwmXIevnIwZrrWabf3lSkjsCfU8frQTL/ikfxO0e55CCX6nFjtYV3lM74Qz7wkpIGFdOj80V+XLzRQuPod+VOkWxJa9Rnl14gKQluuq6czl8ON83zL6knEHAswTugHav+6iHFjpbzvcdsPDmCdn3rZCT21WNFUSdx07b6tdiFJCYdXycUhFna4EmrpN4UsA4u1VDGnLl+mYKXfttpQvWHGuD4N5+Cv0DjlZDfHdd9Xj3QFbYjsmpfUWl3QC2csP0Ntz9EryGhQHxc2DUDwfu6qJHsazwzuFRIkc2AO9F6+uLq+Sq1VwsOLHrbDN48lp5sTAiKTb4bHZ/Wpmu21N2KTKDWvsNJkzE9yOdBNziszFjmrpnGbizsF7gEocLjF2+QirZyaol0YmgZhWChCnru77wM=");
INSERT INTO `sessions`  VALUES ( "o7heuejp5pmk6jmdis69mv2n57","2014-12-28 04:22:18","xRVtZnGLhK3KXHx0p9tBqltDSFvWsVbJm+w3O0K/FV5ju7P7brZ8Pw8tHrQIxaNgeWeO35ADoSGeOL8HzWCu4gonbvNL+TgtD9jL/SQxqVeX7Hn04HA2QN/HrOwwxLDsAD0Bugs5g4/PwIfo5B3ODi9b7ZBOJ2QaeSNoVDLHwLNducI9hR+14DPKawE4czmC07rn3tJ39vi6SNOlHHfWXGpfZXQr3/AdKAFAQQJCzEWPwaiajP9pZdotbssd7iTZX9m2UR6pcjxJpYSt/1za9EKuLrdXMX3hn6yl3+9OqjxGOwz/H20elMQdp7cEWw4z5wDfLv6YIRCXBtbOfd0627iFLzhdUjKF2xvMhVYGORPCkWcgCEpmsG9rQ7MQufezXNtXGbKhbe4MHzOg7g+Yz2YpC5DCnvWmuU214dF4yJNAdAfulCoeJmNrg0DvudT/HTUBhjQFcZ71K7h2JHA9hvN/qRU/XZyEui71AP9rJWrGjy8iBGfviV8uPa7Oh5nbuC7mKPB58pkTA6VtlqDVF0n6uZnjoPKYXsd6LNb38vg=");
INSERT INTO `sessions`  VALUES ( "pp898t3m0kgrauutouk8llaha1","2014-12-27 07:52:32","60cmZRvkkJJCEmeGi5Pq/SXPdzOyTH/f46jXNb7B2YUo6wv5AdU7cGKEgEv3W9wALUElzPe2rhhD9tpucisH5Dy9wk81VuAVF6VmO9J06kf0j837CWlESqW0ksK8M5RIRzEce6nrLCq3zOAY1Egc6L9eCaVtDZsrrLLcdz4ePVeXT66+P/NHImjEDfkDD4yoDNLLU9kUvU29pbtQo5Oh2eOvCt+sRYBvLkxHg92MCXdWSm/6bpJrXcKPAPV/TLmRE0vaftI4M76GITZKTMJ12Bi3GhGRGaBA8mhPltPkFKWvx2iPMkRxXwxLGsxok4fOhrJFXsyCsZHRyjhnfVkFScAFfPzRjPYuNcGNzVYC1inrQfcDP/aCGu/6mxdDvX9ARhkF+i0+hRcmx8tHEz5oBEHlK14xIqXs2LXiSu5gtgTJgUh0U99yiQmVuZV2KRh/dqti+pL5bMkSJ+OKnBMyzhSczbFK6o3z9J0MmKOfvhkAoknqM/uoswgsYSt4LZC/ZQ9MPf9cSBF/W5CkBzaDKY4eI3GSJdqbFrx23ZE9kYQ=");
INSERT INTO `sessions`  VALUES ( "ukp20ckir5a3dkh7en1dsoshj2","2014-12-28 04:26:35","LzYIS7prvyUut/XeuFigIkGVTumHT/1ZMvAO6Cw7duO6DTpoad/4xPsiY5Vrmzpob9UReD/UrirvLAN8WMAJB2aXU0GUcpHzZRwxSEZgB+NGM1TX6kadbiyBT4Y0pI7YU13UGnY72MWpsL+dwBsVZA1fDqHk+oRQqqkV8R668pzJRKyvk/6ck9e6zelR0cvXiiL9t8fBedhUOkOViA8lLIJEDjDQYWVOOeY4PeaQ8/jynTwB6uYK18jcLHNf/WYEacvMMTQ8nJ/w6uZYnbfCXIGzS6zqAG3i7gW3Khm53BgJVFAtXcP6mjbAA1tHY1yizVN3Yhzzyfd8ji7RCAoSt3MlUzdidCfX+hlDVnLmCRnbImRfZQAyzmwJtUhzhfhzoKb00kYtMTEalxqwtjbO1rTNQUiNRZmXj/6YZ2rM49fJyEPCrb3Nk9OG94CaI6mqpMlsS42cGC23BBIv3qwD93MkueeK1j+FGqwDuDAcfi7aKzSCjlYKR/Uo7kz+Z7vRcyO+sIHoZUcfGj+kdmGLrxivr5bKhahAnheNvxYRDow=");


--
-- Table structure for table `shipment`
--
DROP TABLE  IF EXISTS `shipment`;
CREATE TABLE `shipment` (
  `order_id` int(11) unsigned NOT NULL,
  `shipment_date` datetime NOT NULL,
  `delivery_status` varchar(24) NOT NULL,
  `actual_delivery_date` datetime NOT NULL,
  PRIMARY KEY (`order_id`),
  CONSTRAINT `fk_shipment_orders1` FOREIGN KEY (`order_id`) REFERENCES `orders` (`order_id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



--
-- Table structure for table `shipping_options`
--
DROP TABLE  IF EXISTS `shipping_options`;
CREATE TABLE `shipping_options` (
  `shipping_option_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `store_id` smallint(5) unsigned NOT NULL,
  `zone_id` int(11) unsigned NOT NULL,
  `carrier_id` int(10) unsigned NOT NULL,
  `price_id` bigint(20) unsigned NOT NULL,
  `is_taxable` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `rate_is_per` enum('weight','unit','volume','flat') NOT NULL DEFAULT 'unit',
  `description_id` bigint(20) unsigned NOT NULL,
  `max_delivery_days` varchar(45) DEFAULT NULL,
  `active` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `date_added` datetime NOT NULL,
  `last_modified` datetime NOT NULL,
  PRIMARY KEY (`shipping_option_id`,`store_id`),
  KEY `fk_shipping_options_prices1_idx` (`price_id`),
  KEY `fk_shipping_options_description1_idx` (`description_id`),
  KEY `fk_shipping_options_carriers1_idx` (`carrier_id`),
  KEY `fk_shipping_options_zones1_idx` (`zone_id`),
  KEY `fk_shipping_options_stores1_idx` (`store_id`),
  CONSTRAINT `fk_shipping_options_carriers1` FOREIGN KEY (`carrier_id`) REFERENCES `carriers` (`carrier_id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `fk_shipping_options_description1` FOREIGN KEY (`description_id`) REFERENCES `description` (`description_id`) ON UPDATE NO ACTION,
  CONSTRAINT `fk_shipping_options_prices1` FOREIGN KEY (`price_id`) REFERENCES `prices` (`price_id`) ON UPDATE NO ACTION,
  CONSTRAINT `fk_shipping_options_stores1` FOREIGN KEY (`store_id`) REFERENCES `stores` (`store_id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `fk_shipping_options_zones1` FOREIGN KEY (`zone_id`) REFERENCES `zones` (`zone_id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

INSERT INTO `shipping_options`  VALUES ( "1","1","3","2","1","0","unit","89","5","0","2014-12-26 03:36:55","2014-12-26 03:36:55");
INSERT INTO `shipping_options`  VALUES ( "2","1","3","2","2","0","unit","90","2","0","2014-12-26 03:36:55","2014-12-26 03:36:55");
INSERT INTO `shipping_options`  VALUES ( "3","1","3","2","3","0","unit","91","1","0","2014-12-26 03:36:55","2014-12-26 03:36:55");
INSERT INTO `shipping_options`  VALUES ( "4","2","1","1","4","0","unit","92","5","0","2014-12-26 03:36:55","2014-12-26 03:36:55");
INSERT INTO `shipping_options`  VALUES ( "5","2","1","1","5","0","unit","93","2","0","2014-12-26 03:36:55","2014-12-26 03:36:55");
INSERT INTO `shipping_options`  VALUES ( "6","2","1","1","6","0","unit","94","8","0","2014-12-26 03:36:55","2014-12-26 03:36:55");


--
-- Table structure for table `slides`
--
DROP TABLE  IF EXISTS `slides`;
CREATE TABLE `slides` (
  `slide_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `tile_id` int(10) unsigned NOT NULL,
  `type` enum('image','text','html') NOT NULL DEFAULT 'image',
  `text_id` bigint(20) unsigned DEFAULT NULL,
  `image_id` bigint(20) unsigned DEFAULT NULL,
  `href` mediumtext NOT NULL,
  `target` enum('_blank','_self','_parent','_top') NOT NULL DEFAULT '_blank',
  `sort_order` smallint(5) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`slide_id`,`tile_id`),
  KEY `fk_slides_tiles1_idx` (`tile_id`),
  CONSTRAINT `fk_slides_tiles1` FOREIGN KEY (`tile_id`) REFERENCES `tiles` (`tile_id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



--
-- Table structure for table `staff_members`
--
DROP TABLE  IF EXISTS `staff_members`;
CREATE TABLE `staff_members` (
  `admin_id` int(11) unsigned NOT NULL,
  `store_id` smallint(5) unsigned NOT NULL,
  PRIMARY KEY (`admin_id`,`store_id`),
  KEY `fk_staff_members_admin1_idx` (`admin_id`),
  KEY `fk_staff_members_stores1_idx` (`store_id`),
  CONSTRAINT `fk_staff_members_admin1` FOREIGN KEY (`admin_id`) REFERENCES `admin` (`admin_id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `fk_staff_members_stores1` FOREIGN KEY (`store_id`) REFERENCES `stores` (`store_id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



--
-- Table structure for table `store_keychain`
--
DROP TABLE  IF EXISTS `store_keychain`;
CREATE TABLE `store_keychain` (
  `admin_id` int(11) unsigned NOT NULL,
  `key_hash` varchar(255) NOT NULL,
  `date_added` datetime NOT NULL,
  PRIMARY KEY (`admin_id`),
  CONSTRAINT `fk_store_keychain_admin1` FOREIGN KEY (`admin_id`) REFERENCES `admin` (`admin_id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `store_keychain`  VALUES ( "1","c2b0e5766727e56f801212881fc1ac6206ca825c","2014-12-27 07:53:45");


--
-- Table structure for table `store_locations`
--
DROP TABLE  IF EXISTS `store_locations`;
CREATE TABLE `store_locations` (
  `store_id` smallint(5) unsigned NOT NULL,
  `location_id` bigint(20) unsigned NOT NULL,
  PRIMARY KEY (`store_id`,`location_id`),
  KEY `fk_shop_locations_locations1_idx` (`location_id`),
  CONSTRAINT `fk_shop_locations_locations1` FOREIGN KEY (`location_id`) REFERENCES `locations` (`location_id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `fk_shop_locations_shops1` FOREIGN KEY (`store_id`) REFERENCES `stores` (`store_id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `store_locations`  VALUES ( "1","3");
INSERT INTO `store_locations`  VALUES ( "2","4");


--
-- Table structure for table `stores`
--
DROP TABLE  IF EXISTS `stores`;
CREATE TABLE `stores` (
  `store_id` smallint(5) unsigned NOT NULL AUTO_INCREMENT,
  `language_code` char(2) NOT NULL DEFAULT 'EN',
  `currency_code` char(3) NOT NULL,
  `image_id` bigint(20) unsigned DEFAULT NULL,
  `description_id` bigint(20) unsigned NOT NULL,
  `url_rewrite_id` bigint(20) unsigned NOT NULL,
  `hours` tinytext NOT NULL,
  `telephone` varchar(16) NOT NULL,
  `email` varchar(255) NOT NULL,
  `contact_name` varchar(255) NOT NULL DEFAULT 'Contact Name',
  `default_location_id` bigint(20) unsigned NOT NULL,
  `date_added` datetime NOT NULL,
  `date_modified` datetime NOT NULL,
  `active` tinyint(1) NOT NULL,
  `notification_profile_new_order` varchar(60) NOT NULL,
  `notification_profile_confirm_delivery` varchar(60) NOT NULL,
  `timezone` varchar(160) NOT NULL DEFAULT 'Europe/London',
  PRIMARY KEY (`store_id`),
  UNIQUE KEY `shop_id_UNIQUE` (`store_id`),
  KEY `fk_shops_images1_idx` (`image_id`),
  KEY `fk_stores_currencies1_idx` (`currency_code`),
  KEY `fk_stores_locations1_idx` (`default_location_id`),
  KEY `fk_stores_description1_idx` (`description_id`),
  KEY `fk_stores_url_rewrite1_idx` (`url_rewrite_id`),
  KEY `fk_shops_languages1` (`language_code`),
  CONSTRAINT `fk_shops_images1` FOREIGN KEY (`image_id`) REFERENCES `images` (`image_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_shops_languages1` FOREIGN KEY (`language_code`) REFERENCES `languages` (`language_code`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_stores_currencies1` FOREIGN KEY (`currency_code`) REFERENCES `currencies` (`currency_code`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_stores_description1` FOREIGN KEY (`description_id`) REFERENCES `description` (`description_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_stores_locations1` FOREIGN KEY (`default_location_id`) REFERENCES `locations` (`location_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_stores_url_rewrite1` FOREIGN KEY (`url_rewrite_id`) REFERENCES `url_rewrite` (`url_rewrite_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

INSERT INTO `stores`  VALUES ( "1","EN","NGN","9","83","1","8AM to 5PM","07010000000","example@email.com","Contact Name","3","2014-12-26 03:36:55","2014-12-27 07:49:41","1","6b18c0fd53ea33e496d42bd0670c5c6f7a6d0e80","c11ad8fe205554ec7e05e2b7d76cd23112115bcc","Europe/London");
INSERT INTO `stores`  VALUES ( "2","EN","GBP","","86","2","8AM to 5PM","07010000000","example@email.com","Contact Name","4","2014-12-26 03:36:55","2014-12-26 03:36:55","0","90d1d256eb7e93f7b0595515130907d1facd8b28","7e3b8329b1b33bda2c31711f8c0780edbeb5efb6","Europe/London");


--
-- Table structure for table `tags`
--
DROP TABLE  IF EXISTS `tags`;
CREATE TABLE `tags` (
  `tag_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `description_id` bigint(20) unsigned DEFAULT NULL,
  `url_rewrite_id` bigint(20) unsigned DEFAULT NULL,
  `image_id` bigint(20) unsigned DEFAULT NULL,
  `sort_order` int(3) DEFAULT '0',
  `date_added` datetime NOT NULL,
  `last_modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `status` tinyint(1) NOT NULL DEFAULT '0',
  `meta_keywords_text_id` bigint(20) unsigned NOT NULL,
  `meta_description_text_id` bigint(20) unsigned NOT NULL,
  `parent_id` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`tag_id`),
  KEY `idx_parent_id_tag_id` (`tag_id`),
  KEY `idx_sort_order` (`sort_order`),
  KEY `fk_tags_url_rewrite1_idx` (`url_rewrite_id`),
  KEY `fk_tags_text1_idx` (`meta_keywords_text_id`),
  KEY `fk_tags_text2_idx` (`meta_description_text_id`),
  KEY `fk_parent_id_idx` (`parent_id`),
  KEY `fk_banner_image_id_idx` (`image_id`),
  CONSTRAINT `fk_banner_image_id` FOREIGN KEY (`image_id`) REFERENCES `images` (`image_id`) ON DELETE SET NULL ON UPDATE NO ACTION,
  CONSTRAINT `fk_parent_id` FOREIGN KEY (`parent_id`) REFERENCES `tags` (`tag_id`) ON DELETE SET NULL ON UPDATE NO ACTION,
  CONSTRAINT `fk_tags_text1` FOREIGN KEY (`meta_keywords_text_id`) REFERENCES `text` (`text_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_tags_text2` FOREIGN KEY (`meta_description_text_id`) REFERENCES `text` (`text_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_tags_url_rewrite1` FOREIGN KEY (`url_rewrite_id`) REFERENCES `url_rewrite` (`url_rewrite_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

INSERT INTO `tags`  VALUES ( "1","95","3","","0","2014-12-26 03:36:55","2014-12-26 03:36:55","0","1","2","");
INSERT INTO `tags`  VALUES ( "2","96","4","","0","2014-12-26 03:36:55","2014-12-26 03:36:55","0","3","4","");
INSERT INTO `tags`  VALUES ( "3","97","5","","0","2014-12-26 03:36:55","2014-12-26 03:36:55","0","5","6","");
INSERT INTO `tags`  VALUES ( "4","105","9","","0","2014-12-26 16:15:17","2014-12-26 16:16:05","1","16","17","");


--
-- Table structure for table `tax_rules`
--
DROP TABLE  IF EXISTS `tax_rules`;
CREATE TABLE `tax_rules` (
  `tax_rule_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `type` enum('product','shipping') NOT NULL,
  `tax_zone_id` int(11) unsigned NOT NULL,
  `tax_priority` int(5) DEFAULT '1',
  `tax_rate` decimal(7,4) NOT NULL DEFAULT '0.0000',
  `description_id` bigint(20) unsigned NOT NULL,
  `last_modified` datetime NOT NULL,
  `date_added` datetime NOT NULL,
  PRIMARY KEY (`tax_rule_id`),
  KEY `idx_tax_zone_id_zen` (`tax_zone_id`),
  KEY `fk_tax_rates_description1_idx` (`description_id`),
  CONSTRAINT `fk_tax_rates_description1` FOREIGN KEY (`description_id`) REFERENCES `description` (`description_id`) ON UPDATE NO ACTION,
  CONSTRAINT `fk_tax_rates_zones1` FOREIGN KEY (`tax_zone_id`) REFERENCES `zones` (`zone_id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

INSERT INTO `tax_rules`  VALUES ( "1","product","1","1","6.0000","79","2014-12-26 03:36:55","2014-12-26 03:36:55");
INSERT INTO `tax_rules`  VALUES ( "2","product","3","1","5.0000","80","2014-12-26 03:36:55","2014-12-26 03:36:55");
INSERT INTO `tax_rules`  VALUES ( "3","shipping","1","1","6.0000","81","2014-12-26 03:36:55","2014-12-26 03:36:55");
INSERT INTO `tax_rules`  VALUES ( "4","shipping","3","1","5.0000","82","2014-12-26 03:36:55","2014-12-26 03:36:55");


--
-- Table structure for table `text`
--
DROP TABLE  IF EXISTS `text`;
CREATE TABLE `text` (
  `text_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `date_added` datetime NOT NULL,
  PRIMARY KEY (`text_id`)
) ENGINE=InnoDB AUTO_INCREMENT=36 DEFAULT CHARSET=utf8;

INSERT INTO `text`  VALUES ( "1","2014-12-26 03:36:55");
INSERT INTO `text`  VALUES ( "2","2014-12-26 03:36:55");
INSERT INTO `text`  VALUES ( "3","2014-12-26 03:36:55");
INSERT INTO `text`  VALUES ( "4","2014-12-26 03:36:55");
INSERT INTO `text`  VALUES ( "5","2014-12-26 03:36:55");
INSERT INTO `text`  VALUES ( "6","2014-12-26 03:36:55");
INSERT INTO `text`  VALUES ( "7","2014-12-26 03:36:55");
INSERT INTO `text`  VALUES ( "8","2014-12-26 03:36:55");
INSERT INTO `text`  VALUES ( "9","2014-12-26 03:36:55");
INSERT INTO `text`  VALUES ( "10","2014-12-26 03:36:55");
INSERT INTO `text`  VALUES ( "11","2014-12-26 03:36:55");
INSERT INTO `text`  VALUES ( "12","2014-12-26 03:36:55");
INSERT INTO `text`  VALUES ( "13","2014-12-26 03:36:55");
INSERT INTO `text`  VALUES ( "16","2014-12-26 16:15:17");
INSERT INTO `text`  VALUES ( "17","2014-12-26 16:15:17");
INSERT INTO `text`  VALUES ( "18","2014-12-27 02:57:19");
INSERT INTO `text`  VALUES ( "21","2014-12-27 03:24:29");
INSERT INTO `text`  VALUES ( "24","2014-12-27 03:42:27");
INSERT INTO `text`  VALUES ( "27","2014-12-27 03:45:16");
INSERT INTO `text`  VALUES ( "30","2014-12-27 03:47:56");
INSERT INTO `text`  VALUES ( "31","2014-12-27 03:47:56");
INSERT INTO `text`  VALUES ( "32","2014-12-27 03:47:56");
INSERT INTO `text`  VALUES ( "33","2014-12-27 07:46:44");
INSERT INTO `text`  VALUES ( "34","2014-12-27 07:46:44");
INSERT INTO `text`  VALUES ( "35","2014-12-27 07:46:44");


--
-- Table structure for table `text_content`
--
DROP TABLE  IF EXISTS `text_content`;
CREATE TABLE `text_content` (
  `text_id` bigint(20) unsigned NOT NULL,
  `language_code` char(2) NOT NULL,
  `text` text,
  `last_modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`language_code`,`text_id`),
  KEY `fk_products_description_languages1_idx` (`language_code`),
  KEY `fk_text_content_text1_idx` (`text_id`),
  CONSTRAINT `fk_products_description_languages100` FOREIGN KEY (`language_code`) REFERENCES `languages` (`language_code`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_text_content_text1` FOREIGN KEY (`text_id`) REFERENCES `text` (`text_id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `text_content`  VALUES ( "1","EN","Women","2014-12-26 03:36:55");
INSERT INTO `text_content`  VALUES ( "2","EN","Women","2014-12-26 03:36:55");
INSERT INTO `text_content`  VALUES ( "3","EN","Chicy Aimas T-Shirts","2014-12-26 03:36:55");
INSERT INTO `text_content`  VALUES ( "4","EN","Chicy Aimas T-Shirts","2014-12-26 03:36:55");
INSERT INTO `text_content`  VALUES ( "5","EN","Chicy Clutch Handbags","2014-12-26 03:36:55");
INSERT INTO `text_content`  VALUES ( "6","EN","Chicy Clutch Handbags","2014-12-26 03:36:55");
INSERT INTO `text_content`  VALUES ( "7","EN","","2014-12-26 03:36:55");
INSERT INTO `text_content`  VALUES ( "8","EN","Chicy Pink Tee","2014-12-26 03:36:55");
INSERT INTO `text_content`  VALUES ( "9","EN","Chicy Pink Tee","2014-12-26 03:36:55");
INSERT INTO `text_content`  VALUES ( "10","EN","","2014-12-26 03:36:55");
INSERT INTO `text_content`  VALUES ( "11","EN","Blue Summer Top","2014-12-26 03:36:55");
INSERT INTO `text_content`  VALUES ( "12","EN","Blue Summer Top","2014-12-26 03:36:55");
INSERT INTO `text_content`  VALUES ( "13","EN","","2014-12-26 03:36:55");
INSERT INTO `text_content`  VALUES ( "16","EN","New Category","2014-12-26 16:15:17");
INSERT INTO `text_content`  VALUES ( "17","EN","New Category","2014-12-26 16:15:17");
INSERT INTO `text_content`  VALUES ( "18","EN","","2014-12-27 02:57:19");
INSERT INTO `text_content`  VALUES ( "21","EN","","2014-12-27 03:24:29");
INSERT INTO `text_content`  VALUES ( "24","EN","","2014-12-27 03:42:27");
INSERT INTO `text_content`  VALUES ( "27","EN","","2014-12-27 03:45:16");
INSERT INTO `text_content`  VALUES ( "30","EN","","2014-12-27 03:47:56");
INSERT INTO `text_content`  VALUES ( "31","EN","Pink, Leather, Clutch","2014-12-27 03:48:06");
INSERT INTO `text_content`  VALUES ( "32","EN","New Bag","2014-12-27 03:47:56");
INSERT INTO `text_content`  VALUES ( "33","EN","","2014-12-27 07:46:44");
INSERT INTO `text_content`  VALUES ( "34","EN","Ankara, Dress","2014-12-27 07:47:05");
INSERT INTO `text_content`  VALUES ( "35","EN","New Bag","2014-12-27 07:46:44");


--
-- Table structure for table `tiles`
--
DROP TABLE  IF EXISTS `tiles`;
CREATE TABLE `tiles` (
  `store_id` smallint(5) unsigned NOT NULL,
  `tile_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `pixel_width` float unsigned NOT NULL DEFAULT '1024',
  `pixel_height` float unsigned NOT NULL DEFAULT '25',
  `sort_order` smallint(5) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`tile_id`,`store_id`),
  KEY `fk_tiles_stores1_idx` (`store_id`),
  CONSTRAINT `fk_tiles_stores1` FOREIGN KEY (`store_id`) REFERENCES `stores` (`store_id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



--
-- Table structure for table `transaction_errors`
--
DROP TABLE  IF EXISTS `transaction_errors`;
CREATE TABLE `transaction_errors` (
  `transaction_error_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `transaction_id` bigint(20) unsigned NOT NULL,
  `error_code` varchar(20) DEFAULT NULL,
  `short_message` varchar(255) DEFAULT NULL,
  `long_message` mediumtext,
  PRIMARY KEY (`transaction_error_id`,`transaction_id`),
  KEY `fk_transaction_errors_transactions1_idx` (`transaction_id`),
  CONSTRAINT `fk_transaction_errors_transactions1` FOREIGN KEY (`transaction_id`) REFERENCES `transactions` (`transaction_id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



--
-- Table structure for table `transactions`
--
DROP TABLE  IF EXISTS `transactions`;
CREATE TABLE `transactions` (
  `transaction_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `order_id` int(11) unsigned NOT NULL,
  `payment_type_id` smallint(5) unsigned NOT NULL,
  `customer_id` int(11) unsigned NOT NULL,
  `paypal_payer_id` varchar(128) DEFAULT NULL,
  `paypal_payer_email` varchar(255) DEFAULT NULL,
  `paypal_correlation_id` varchar(128) DEFAULT NULL,
  `paypal_transaction_id` varchar(128) DEFAULT NULL,
  `token` varchar(255) NOT NULL,
  `amount` decimal(11,2) NOT NULL,
  `currency_code` char(3) NOT NULL,
  `tax_amount` decimal(11,2) NOT NULL DEFAULT '0.00',
  `status` enum('pending','failed','cancelled','paid','cash on delivery','bank transfer') NOT NULL DEFAULT 'pending',
  `last_modified` datetime NOT NULL,
  `date_created` datetime NOT NULL,
  PRIMARY KEY (`transaction_id`,`order_id`,`payment_type_id`),
  KEY `fk_transactions_payment_types1_idx` (`payment_type_id`),
  KEY `fk_transactions_customers1_idx` (`customer_id`),
  KEY `fk_transactions_currencies1_idx` (`currency_code`),
  KEY `fk_transactions_orders1_idx` (`order_id`),
  CONSTRAINT `fk_transactions_currencies1` FOREIGN KEY (`currency_code`) REFERENCES `currencies` (`currency_code`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `fk_transactions_customers1` FOREIGN KEY (`customer_id`) REFERENCES `customers` (`customer_id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `fk_transactions_orders1` FOREIGN KEY (`order_id`) REFERENCES `orders` (`order_id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `fk_transactions_payment_types1` FOREIGN KEY (`payment_type_id`) REFERENCES `payment_types` (`payment_type_id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



--
-- Table structure for table `url_rewrite`
--
DROP TABLE  IF EXISTS `url_rewrite`;
CREATE TABLE `url_rewrite` (
  `url_rewrite_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `keyword` varchar(225) NOT NULL,
  `target_path` varchar(225) NOT NULL,
  `is_admin` smallint(5) NOT NULL DEFAULT '0',
  PRIMARY KEY (`url_rewrite_id`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8;

INSERT INTO `url_rewrite`  VALUES ( "1","aimas-ng","index.php","0");
INSERT INTO `url_rewrite`  VALUES ( "2","aimas-uk","index.php","0");
INSERT INTO `url_rewrite`  VALUES ( "3","women","category.php?c=1","0");
INSERT INTO `url_rewrite`  VALUES ( "4","chicy-aimas-t-shirts","category.php?c=2","0");
INSERT INTO `url_rewrite`  VALUES ( "5","chicy-clutch-handbags","category.php?c=3","0");
INSERT INTO `url_rewrite`  VALUES ( "6","chicy-pink-tee","product.php?p=1","0");
INSERT INTO `url_rewrite`  VALUES ( "7","blue-summer-top","product.php?p=2","0");
INSERT INTO `url_rewrite`  VALUES ( "9","shoes","category.php?c=4","0");
INSERT INTO `url_rewrite`  VALUES ( "14","pink-leather-clutch","product.php?p=8","0");
INSERT INTO `url_rewrite`  VALUES ( "15","ankara-dress","product.php?p=9","0");


--
-- Table structure for table `whos_online`
--
DROP TABLE  IF EXISTS `whos_online`;
CREATE TABLE `whos_online` (
  `customer_id` int(11) DEFAULT NULL,
  `full_name` varchar(64) NOT NULL DEFAULT '',
  `session_id` varchar(128) NOT NULL DEFAULT '',
  `ip_address` varchar(20) NOT NULL DEFAULT '',
  `time_entry` varchar(14) NOT NULL DEFAULT '',
  `time_last_click` varchar(14) NOT NULL DEFAULT '',
  `last_page_url` varchar(255) NOT NULL DEFAULT '',
  `host_address` text NOT NULL,
  `user_agent` varchar(255) NOT NULL DEFAULT '',
  KEY `idx_ip_address_zen` (`ip_address`),
  KEY `idx_session_id_zen` (`session_id`),
  KEY `idx_customer_id_zen` (`customer_id`),
  KEY `idx_time_entry_zen` (`time_entry`),
  KEY `idx_time_last_click_zen` (`time_last_click`),
  KEY `idx_last_page_url_zen` (`last_page_url`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;



--
-- Table structure for table `wishlist_items`
--
DROP TABLE  IF EXISTS `wishlist_items`;
CREATE TABLE `wishlist_items` (
  `wishlist_item_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `wishlist_id` int(11) unsigned NOT NULL,
  `product_id` int(11) unsigned NOT NULL,
  `option_id` bigint(20) unsigned NOT NULL,
  `quantity` float NOT NULL DEFAULT '1',
  `date_added` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`wishlist_item_id`,`wishlist_id`,`product_id`,`option_id`),
  KEY `fk_cart_items_products1_idx` (`product_id`),
  KEY `fk_cart_items_options1_idx` (`option_id`),
  KEY `fk_cart_items_cart100` (`wishlist_id`),
  CONSTRAINT `fk_cart_items_cart100` FOREIGN KEY (`wishlist_id`) REFERENCES `wishlists` (`wishlist_id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `fk_cart_items_options10` FOREIGN KEY (`option_id`) REFERENCES `product_options` (`option_id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `fk_cart_items_products10` FOREIGN KEY (`product_id`) REFERENCES `products` (`product_id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



--
-- Table structure for table `wishlists`
--
DROP TABLE  IF EXISTS `wishlists`;
CREATE TABLE `wishlists` (
  `wishlist_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `store_id` smallint(5) unsigned DEFAULT NULL,
  `customer_id` int(11) unsigned NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `secure_key` varchar(255) NOT NULL,
  PRIMARY KEY (`wishlist_id`),
  KEY `fk_cart_products1_idx` (`customer_id`),
  KEY `fk_cart_stores1_idx` (`store_id`),
  CONSTRAINT `fk_cart_stores10` FOREIGN KEY (`store_id`) REFERENCES `stores` (`store_id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `fk_wishlist_customers` FOREIGN KEY (`customer_id`) REFERENCES `customers` (`customer_id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



--
-- Table structure for table `zones`
--
DROP TABLE  IF EXISTS `zones`;
CREATE TABLE `zones` (
  `zone_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `zone_name` varchar(255) NOT NULL DEFAULT '',
  `list_type` enum('exclude','normal') NOT NULL DEFAULT 'normal',
  `date_added` datetime NOT NULL,
  `last_modified` datetime NOT NULL,
  PRIMARY KEY (`zone_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

INSERT INTO `zones`  VALUES ( "1","UK Shipping","normal","2014-12-26 03:36:54","2014-12-26 03:36:55");
INSERT INTO `zones`  VALUES ( "2","Outside UK Shipping","exclude","2014-12-26 03:36:54","2014-12-26 03:36:55");
INSERT INTO `zones`  VALUES ( "3","Lagos Shipping","normal","2014-12-26 03:36:55","2014-12-26 03:36:55");
INSERT INTO `zones`  VALUES ( "4","Rest of Nigeria","exclude","2014-12-26 03:36:55","2014-12-26 03:36:55");


--
-- Table structure for table `zones_to_countries`
--
DROP TABLE  IF EXISTS `zones_to_countries`;
CREATE TABLE `zones_to_countries` (
  `zone_id` int(11) unsigned NOT NULL,
  `country_code` char(2) NOT NULL,
  PRIMARY KEY (`zone_id`,`country_code`),
  KEY `fk_zones_to_countries_zones1_idx` (`zone_id`),
  KEY `fk_zones_to_countries_countries1_idx` (`country_code`),
  CONSTRAINT `fk_zones_to_countries_countries1` FOREIGN KEY (`country_code`) REFERENCES `countries` (`country_code`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `fk_zones_to_countries_zones1` FOREIGN KEY (`zone_id`) REFERENCES `zones` (`zone_id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `zones_to_countries`  VALUES ( "1","GB");
INSERT INTO `zones_to_countries`  VALUES ( "2","GB");
INSERT INTO `zones_to_countries`  VALUES ( "3","NG");
INSERT INTO `zones_to_countries`  VALUES ( "4","NG");


--
-- Table structure for table `zones_to_provinces`
--
DROP TABLE  IF EXISTS `zones_to_provinces`;
CREATE TABLE `zones_to_provinces` (
  `zone_id` int(11) unsigned NOT NULL,
  `province_id` int(11) unsigned NOT NULL,
  PRIMARY KEY (`zone_id`,`province_id`),
  KEY `fk_zones_to_states_zones1_idx` (`zone_id`),
  KEY `fk_zones_to_states_states1_idx` (`province_id`),
  CONSTRAINT `fk_zones_to_states_states1` FOREIGN KEY (`province_id`) REFERENCES `provinces` (`province_id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `fk_zones_to_states_zones1` FOREIGN KEY (`zone_id`) REFERENCES `zones` (`zone_id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `zones_to_provinces`  VALUES ( "3","1");
INSERT INTO `zones_to_provinces`  VALUES ( "4","1");


SET FOREIGN_KEY_CHECKS = 1 ; 
COMMIT ; 
SET AUTOCOMMIT = 1 ; 
