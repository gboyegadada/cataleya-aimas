<?php

namespace Cataleya\Apps\PayOnDelivery;
use Cataleya\Apps\PayOnDelivery;

/**
 * 
 *
 * @package PayOnDelivery
 * @author Gboyega Dada <gboyega@fpplabs.com>
 * @version 0.0.1
 */
class Main implements \Cataleya\Plugins\Controller {


    protected static $_plugin_handle = 'payondelivery';
    private static $_log_table = 'app_payondelivery_log';
    protected static $_Config = null;

    public static $_actions = [

        'get-options' => 'getPaymentOptions', 
        'get-currency-code' => 'getCurrencyCode', 
        'get-max-amount' => 'getMaxAmount', 
        'get-min-amount' => 'getMinAmount', 

        'get-config-form-controls' => 'getConfigFormControls', 
        'update-config' => 'saveConfig', 
        'get-form-controls' => 'getCheckoutFormControls', 
        'validate-form' => 'validateForm', 
        'get-transaction-reference' => 'getTransactionRef', 
        'start-transaction' => 'startTransaction', 
        'complete-transaction' => 'getTransactionStatus', 
        'get-transaction-status' => 'getTransactionStatus', 

        // Returns transactions made on specified order_id
        'get-transactions' => 'getTransactions', 
        'get-instructions' => 'getPaymentInstructions'

    ];




    /**
     * init
     *
     * @return void
     */
    public static function init () {

        if (self::$_initialized) { return true; }

        /* -----------------  Do init stuff ----------------- */

        self::getConfig(true);

        /* ---------------- End of init stuff --------------- */


        self::$_initialized = true;
    }




    /**
     * getActions            Get list of events/hooks that Plugin Controller will repond to
     *
     * @return array         An rray of |actions| (strings), that is, a list of events/hooks our plugin 
     *                       will respond to. Eg. 'payments.start-transaction' and so on.
     *
     */
    public static function getActions () {

        return array_keys(self::$_actions);

    }



    /**
     * getHandle
     *
     * @return string
     */
    public static function getHandle() {

        return self::$_plugin_handle;
    }


    /**
     * getKey   Alias for ::getHandle()
     *
     * @return string
     */
    public static function getKey() {

        return self::getHandle();;
    }




    /**
     * getConfig
     *
     * @return void
     */
    public static function getConfig ($_reload=false) {
        
        if (empty(self::$_Config) || $_reload===true) self::$_Config = \__('this.config');
        return self::$_Config;

    }



    /**
     * getLogTable
     *
     * @return string
     */
    public static function getLogTable() {
        return self::$_log_table;

    }



    /**
     * getStatusCodes
     *
     * @return array
     */
    public static function getStatusCodes () {
        return self::$_status_codes;
    }



    /**
     * getStatusDescription
     *
     * @param string $_code     Code is numeric but should supplied like this: '00', '08'...
     *                          (that is -- zero padded quoted string).
     * @return string
     */
    public static function getStatusDescription ($_code) {

        return (isset(self::$_status_codes[$_code])) 
            ? self::$_status_codes[$_code] 
            : 'Unknown'; 

    }




    /**
     * run    This is like the regular 'exec' method that a controller has; 
     *        basically functioning as a gateway.
     *
     * @param string $_action 
     * @param array $_params
     * @param mixed $_data
     * @return mixed
     */
    public static function run ($_action, array $_params, $_data = null) {
        
        if (!is_string($_action) || !isset(self::$_actions[$_action])) { 
            
            throw new \Cataleya\Error ("Invalid plugin action: [" . $_action . "] in Pay on Delivery App.");

        }
        
       return call_user_func_array([ __CLASS__, self::$_actions[$_action] ], [$_params, $_data]);
                   
    }
    



    
    /**
     * run    This works like '::run'. It will be called whenever an event  
     *        your app is listening to is triggered. You have to specify there using 'events' in 
     *        your app.json file just like your plugin scopes.
     *
     * @param string $_event 
     * @param array $_params
     * @return void
     */
    public static function notify ($_event, array $_params = []) {
    
        
        
    }

    





    /**
     * install
     *
     * @param array $_params
     * @param array $_data
     * @return void
     */
    public static function install () {


        // 1. Save plugin_handle ??
        $_Config = \__('this.config.init', [

            'NOTES' => 'After payment, please send payment details to help@shop.com.'  
        
        ]);


    }


    /**
     * uninstall
     *
     * @param array $_params 
     * @param array $_data
     * @return void
     */
    public static function uninstall () {

        // 1. Remove db tables
        $_drop_handle = \Cataleya\Helper\DBH::getInstance()->prepare('DROP TABLE '.self::$_log_table);
        if (!$_drop_handle->execute()) {
            throw new \Cataleya\Error(implode(', ', $_drop_handle->errorInfo()), $_drop_handle->errorCode()); 
        }

        // 2. General cleanup
        $_Config = self::getConfig();
        if (!empty($_Config)) { $_Config->drop(); }

    }





    
    /* ----------------------- PLUGIN ACTIONS -------------------------- */


    /**
     * getConfigFormControls
     *
     * @param array $_params
     * @param array $_data
     * @return array
     */
    protected static function getConfigFormControls (array $_params, array $_data = []) {

        $_fields = array (

            [
                'name' => 'NOTES',
                'type' => 'html', 
                'required' => true, 
                'label' => 'Instructions',
                'value' => self::getConfig()->getParam('NOTES'),  
                'hint' => 'Instructions for customers on delivery of ordered items.',
                'control' => 'textarea'
            ]
        );


        return array_merge($_data, $_fields);


    }

    


    /**
     * saveConfig
     *
     * @param array $_params
     * @param array $_data
     * @return void
     */
    public static function saveConfig (array $_params, array $_data = []) 
    {



    }








    /**
     * getPaymentOptions
     *
     * @param array $_params
     * @param array $_data
     * @return array
     */
    protected static function getPaymentOptions (array $_params, array $_data = []) {

        return (
                isset($_params['currency_codes']) && 
                !in_array(self::getCurrencyCode(), $_params['currency_codes'])
                ) 
                
                ? $_data 

                : array_merge($_data, array (

                    [
                        'name' => 'Pay on Delivery',
                        'id' => self::getHandle(), 
                        'currency_code' => self::getCurrencyCode(), 
                        'max_amount' => self::getMaxAmount(), 
                        'min_amount' => self::getMinAmount()
                    ]


                ));


    }




    /**
     * getCurrencyCode
     *
     * @param array $_params
     * @param array $_data
     * @return array
     */
    protected static function getCurrencyCode (array $_params = null, array $_data = []) {

        return 'NGN';

    }



    /**
     * getMaxAmount
     *
     * @param array $_params
     * @param array $_data
     * @return array
     */
    protected static function getMaxAmount (array $_params = null, array $_data = []) {

        return 200000;

    }




    /**
     * getMinAmount
     *
     * @param array $_params
     * @param array $_data
     * @return array
     */
    protected static function getMinAmount (array $_params = null, array $_data = []) {

        return 50;

    }






    /**
     * getCheckoutFormControls
     *
     * @param array $_params
     * @param array $_data
     * @return array
     */
    protected static function getCheckoutFormControls (array $_params, array $_data = []) {

        static $_fields = [];


        return array_merge($_data, $_fields);


    }









    /**
     * getTransactionRef
     *
     * @param array $_params
     * @param array $_data
     * @return void
     */
    private static function getTransactionRef (array $_params, array $_data) {


            // generate transaction reference..
            $_tranx_ref = date('y').\Cataleya\Helper::uuid(16);

            return $_tranx_ref;

            
    }







    /**
     * startTransaction
     *
     * @param array $_params
     * @param array $_data
     * @return void
     */
    private static function startTransaction (array $_params, array $_data) {


            // Load order...
            $_Order = \Cataleya\Store\Order::load($_params['order_id']);
            $_description = \Cataleya\Helper::countInEnglish($_Order->getQuantity(), 'item', 'items') 
                            . ' (delivery charges included) from ' 
                            . __('shop')->getName();
            
            $_Customer = $_Order->getCustomer(); 


            // generate transaction reference..
            $_tranx_ref = $_params['transaction_ref'];
            $_amount = $_Order->getTotalCost(TRUE);


            
            // Log transaction
            PayOnDelivery\Log::insert([
                'transaction_ref' => $_tranx_ref, 
                'order_id' => $_Order->getID(), 
                'order_description' => $_description,
                'customer_id' => $_Customer->getID(),
                'customer_email' => $_Customer->getEmailAddress(),  
                'customer_name' => $_Customer->getName(),  
                'status' => 'pending',
                'status_code' => '00',  
                'response_code' => '00', 
                'response_description' => 'Please confirm all bank transfers.', 
                'amount' => $_amount, 
                'transaction_date' => date(''), 
                'currency_code' => 'NGN', 
                'gateway_mode' => 'live'

                ]);

            // Redirect shopper to Interswitch...
            // self::redirect($NVP_Array);
            
    }






    /**
     * completeTransaction
     *
     * @param array $_params
     * @param array $_data
     * @return void
     */
    private static function getTransactionStatus(array $_params, array $_data)
    {

        $_tranx = PayOnDelivery\Log::fetch($_params['transaction_ref']);
        if (empty($_tranx)) 
        {
            // Tranx record not found...
            return $_data;

        } else $_tranx = $_tranx[0];


        $_response = [
            'transaction_ref' => $_tranx['transaction_ref'],  
            'amount' => $_tranx['amount'],  

            // This is specific to Cataleya!! 
            // Can only be: pending | paid | failed | cancelled | error | flagged
            'status' => $_tranx['status'],

            // Response from payment processor.
            // [response_description] is especially useful for displaying gateway specific 
            // response to shopper.
            'response_code' => $_tranx['response_code'], 
            'response_description' => $_tranx['response_description'] 
        ]; 
        
        return $_response;

        
        
    }  



    /**
     * getTransactions
     *
     * @param array $_params
     * @param array $_data
     * @return array
     */
    private static function getTransactions (array $_params, array $_data) {

        $_query = PayOnDelivery\Log::fetch($_params['order_id'], 'order_id');
        $_reciepts = [];

        foreach ($_query as $_t) {

            $_reciepts[] = [
                'app_handle' => self::getHandle(), 
                'transaction_ref' => $_t['transaction_ref'],  
                'amount' => $_t['amount'],  

                // This is specific to Cataleya!! 
                // Can only be: pending | paid | failed | cancelled | error | flagged
                'status' => $_t['status'],

                // Payment Processor
                'payment_processor' => 'Pay on Delivery', 

                // Response from payment processor.
                // [response_description] is especially useful for displaying gateway specific 
                // response to shopper.
                'response_code' => $_t['response_code'], 
                'response_description' => $_t['response_description'], 
                'datetime' => $_t['date_added']

            ]; 

        }


        return array_merge($_data, $_reciepts);

    }
        
    

    /**
     * getPaymentInstructions
     *
     * @param array $_params
     * @param array $_data
     * @return void
     */
    private static function getPaymentInstructions(array $_params, array $_data)
    {

        return $_data;

        
        
    }  
    
    
 

}
