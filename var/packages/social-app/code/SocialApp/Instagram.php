<?php

namespace Cataleya\Apps\SocialApp;
use \Cataleya\Apps\SocialApp;

/**
 * Instagram
 *
 * @package Social App      This is an Instagram helper for the Social App plugin.
 * @author Gboyega Dada <gboyega@fpplabs.com>
 * @version $Id$
 */
class Instagram {




    /**
     * getConfigFormControls
     *
     * @param string $_action
     * @param array $_data
     * @return array
     */
    public static function getConfigFormControls () {

        return [


            [
                'name' => 'INSTAGRAM_USER_HANDLE',
                'type' => 'alphanum',
                'required' => true,  
                'label' => 'Instagram Handle',
                'value' => SocialApp\Main::getConfig()->getParam('INSTAGRAM_USER_HANDLE'),  
                'hint' => 'Enter your Instagram ID / Handle (eg. @example).'
            ],
            [
                'name' => 'INSTAGRAM_TAGS',
                'type' => 'alphanum',
                'label' => 'Hash Tags', 
                'value' => SocialApp\Main::getConfig()->getParam('INSTAGRAM_TAGS'),  
                'hint' => 'Enter a comma separated list of tags (this way, only photos you tag using the keywords will be displayed.)'
            ],
            [
                'name' => 'INSTAGRAM_COUNT',
                'type' => 'int',
                'label' => 'Number of Posts', 
                'value' => SocialApp\Main::getConfig()->getParam('INSTAGRAM_COUNT'),  
                'hint' => 'Number of photos to load.'
            ]
        ];




    }


    /**
     * getUserIDFromUserName
     *
     * @return void
     */
    public static function refreshUserID() {

        $_Config = SocialApp\Main::getConfig(true);

        $q = self::q(
            'https://api.instagram.com/v1/users/search?q=' 
            . $_Config->getParam('INSTAGRAM_USER_HANDLE') . '&client_id=' 
            . $_Config->getParam('INSTAGRAM_API_APP_ID')
        );

        if(!empty($q) && $q->meta->code=='200' && $q->data[0]->id>0){
            //Found
            $_Config->setParam('INSTAGRAM_USER_ID', $q->data[0]->id);
            $_Config->save();

        }


    }





    /**
     * getFeed
     *
     * @return void
     */
    public static function getFeed () 
    {

        $_Config = SocialApp\Main::getConfig();

        $_id = $_Config->getParam('INSTAGRAM_USER_ID');
        $_handle = $_Config->getParam('INSTAGRAM_USER_HANDLE');
        $tags = self::getTags();

        if (empty($_id) && !empty($_handle))  {
            self::refreshUserID();
            $_Config = SocialApp\Main::getConfig(true);
        }

        $_id = $_Config->getParam('INSTAGRAM_USER_ID');
        if (empty($_id)) return [];

        $url = 'https://api.instagram.com/v1/users/' 
            . $_id . '/media/recent/?client_id=' 
            . $_Config->getParam('INSTAGRAM_API_APP_ID') 
            . '&count=' . $_Config->getParam('INSTAGRAM_COUNT');

        //Execute and return query
        $query = self::q($url);
        
        if (empty($tags)) { return $query->data; }
        
        $results = array ();
        foreach ($query->data as $p) {
            if (self::test_tags($p, $tags)) {
                $results[] = $p;
            }
        }
        
        return $results;

    }
    
    
    
    protected static function getTags() 
    {
        $_str = SocialApp\Main::getConfig()->getParam('INSTAGRAM_TAGS');
        $_tags = empty($_str) ? [] : preg_split("/[\s,]+/", $_str);
        
        return $_tags;
    }

    



    /**
     * query
     *
     * @return void
     */
    protected static function q($url) 
    {

        $_Config = SocialApp\Main::getConfig();


        $cachekey = 'ig_'.md5($url);
        $cachefile = SocialApp\Main::getCacheDir().DIRECTORY_SEPARATOR.$cachekey.'_'.date('i').'.txt'; //cached for one minute

        //If not cached, -> instagram request
        if(!file_exists($cachefile)){
            //Request
            $request='error';
            if(!extension_loaded('openssl')){ $request = 'This class requires the php extension open_ssl to work as the instagram api works with httpS.'; }
            else { $request = file_get_contents($url); }

            //remove old caches
            $oldcaches = glob(SocialApp\Main::getCacheDir().DIRECTORY_SEPARATOR.$cachekey."*.txt");
            if(!empty($oldcaches)){foreach($oldcaches as $todel){
                unlink($todel);
            }}

            //Cache result
            file_put_contents($cachefile, $request);
        }

        //Execute and return query
        $results =  json_decode(file_get_contents($cachefile));
               
        return $results;


    }





    

	/**
	 * test_tags
	 *
	 * @param mixed $test_me
	 * @param array $tag_array
	 * @return bool
	 */
	protected static function test_tags($test_me, array $tag_array)
	{
         return (count(array_intersect($test_me->tags, $tag_array)) === count($tag_array)) ? true : false;
	}



}
