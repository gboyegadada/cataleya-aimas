<?php

namespace Cataleya\Apps\SocialApp;
use \Cataleya\Apps\SocialApp;

/**
 * Twitter
 *
 * @package Social App      This is Twitter helper for the Social App plugin.
 * @author Gboyega Dada <gboyega@fpplabs.com>
 * @version $Id$
 */
class Twitter {




    /**
     * getConfigFormControls
     *
     * @param string $_action
     * @param array $_data
     * @return array
     */
    public static function getConfigFormControls () {

        return [


            [
                'name' => 'TWITTER_USER_HANDLE',
                'type' => 'alphanum', 
                'required' => true, 
                'label' => 'Twitter Handle',
                'value' => SocialApp\Main::getConfig()->getParam('TWITTER_USER_HANDLE'),  
                'hint' => 'Enter your Twitter ID / Handle (eg. @example).'
            ],
            [
                'name' => 'TWITTER_TAGS',
                'type' => 'alphanum', 
                'required' => true, 
                'label' => 'Hash Tags', 
                'value' => SocialApp\Main::getConfig()->getParam('TWITTER_TAGS'),  
                'hint' => 'Enter a comma separated list of tags (this way, only photos you tag using the keywords will be displayed.'
            ],
            [
                'name' => 'TWITTER_COUNT',
                'type' => 'int',
                'label' => 'Number of Tweets', 
                'value' => SocialApp\Main::getConfig()->getParam('TWITTER_COUNT'),  
                'hint' => 'Number of Tweets to load.'
            ]
        ];




    }


    /**
     * getUserIDFromUserName
     *
     * @return void
     */
    public static function refreshUserID() {

        $_Config = SocialApp\Main::getConfig(true);

        $q = self::q(
            'https://api.twitter.com/v1/users/search?q=' 
            . $_Config->getParam('TWITTER_USER_HANDLE') . '&client_id=' 
            . $_Config->getParam('TWITTER_API_APP_ID')
        );

        if(!empty($q) && $q->meta->code=='200' && $q->data[0]->id>0){
            //Found
            $_Config->setParam('TWITTER_USER_ID', $q->data[0]->id);
            $_Config->save();

        }


    }





    /**
     * getFeed
     *
     * @return void
     */
    public static function getFeed () 
    {

        $_Config = SocialApp\Main::getConfig();
        $tags = self::getTags();


        $url = 'https://api.twitter.com/v1/users/' 
            . $_Config->getParam('TWITTER_USER_ID') . '/media/recent/?client_id=' 
            . $_Config->getParam('TWITTER_API_APP_ID');

        //Execute and return query
        $query = self::q($url);
        
        if (empty($tags)) { return $query->data; }
        
        $results = array ();
        foreach ($query->data as $p) {
            if (self::test_tags($p, $tags)) {
                $results[] = $p;
            }
        }
        
        return $results;

    }






    /**
     * query
     *
     * @return void
     */
    protected static function q($url) 
    {

        $_Config = SocialApp\Main::getConfig();


        $cachekey = 'tw_'.md5($url);
        $cachefile = SocialApp\Main::getCacheDir().DIRECTORY_SEPARATOR.$cachekey.'_'.date('i').'.txt'; //cached for one minute

        //If not cached, -> twitter request
        if(!file_exists($cachefile)){
            //Request
            $request='error';
            if(!extension_loaded('openssl')){ $request = 'This class requires the php extension open_ssl to work as the twitter api works with httpS.'; }
            else { $request = file_get_contents($url); }

            //remove old caches
            $oldcaches = glob($cachefolder.DIRECTORY_SEPARATOR.$cachekey."*.txt");
            if(!empty($oldcaches)){foreach($oldcaches as $todel){
                unlink($todel);
            }}

            //Cache result
            file_put_contents($cachefile, $request);
        }

        //Execute and return query
        $results =  json_decode(file_get_contents($cachefile));
               
        return $results;


    }


    
    protected static function getTags() 
    {
        $_str = SocialApp\Main::getConfig()->getParam('TWITTER_TAGS');
        $_tags = empty($_str) ? [] : preg_split("/[\s,]+/", $_str);
        
        return $_tags;
    }



    

	/**
	 * test_tags
	 *
	 * @param mixed $test_me
	 * @param array $tag_array
	 * @return bool
	 */
	protected static function test_tags($test_me, array $tag_array)
	{
         return (count(array_intersect($test_me->tags, $tag_array)) === count($tag_array)) ? true : false;
	}



}
