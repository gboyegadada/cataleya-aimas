<?php

namespace Cataleya\Apps\SocialApp;
use \Cataleya\Apps\SocialApp;
    


/**
 * 
 *
 * @package Social App    Pull photos, posts and tweets from Intagram, Twitter and Facebook!
 * @author Gboyega Dada <gboyega@fpplabs.com>
 * @version 0.0.1
 */
class Main implements \Cataleya\Plugins\Controller {


    protected static $_plugin_handle = 'social-app';
    protected static $_Config = null;
    protected static $_initialized = false;
    protected static $_cache_dir = '';

    public static $_actions = [
        'get-partials' => 'getPartials', 
        'get-config-form-controls' => 'getConfigFormControls', 
        'update-config' => 'saveConfig', 
    ];





    /**
     * init
     *
     * @return void
     */
    public static function init () {

        if (self::$_initialized) { return true; }

        /* -----------------  Do init stuff ----------------- */

        self::getConfig(true);
        self::$_cache_dir = \__path('this.cache');

        /* ---------------- End of init stuff --------------- */


        self::$_initialized = true;
    }



    /**
     * getActions            Get list of events/hooks that Plugin Controller will repond to
     *
     * @return array         An rray of |actions| (strings), that is, a list of events/hooks our plugin 
     *                       will respond to. Eg. 'payments.start-transaction' and so on.
     *
     */
    public static function getActions () {

        return array_keys(self::$_actions);

    }



    /**
     * getHandle
     *
     * @return string
     */
    public static function getHandle() {

        // if (empty(self::$_plugin_handle)) {
        //    self::init();
        // }

        return self::$_plugin_handle;
    }


    /**
     * getKey   Alias for ::getHandle()
     *
     * @return string
     */
    public static function getKey() {

        return self::getHandle();
    }



    /**
     * getConfig
     *
     * @return void
     */
    public static function getConfig ($_reload=false) {
        
        if (empty(self::$_Config) || $_reload===true) self::$_Config = \__('this.config');
        return self::$_Config;

    }


    /**
     * getCacheDir
     *
     * @return string
     */
    public static function getCacheDir () {

        self::init();
        return self::$_cache_dir;


    }



    /**
     * run    This is like the regular 'exec' method that a controller has; 
     *        basically functioning as a gateway.
     *
     * @param string $_action 
     * @param array $_params
     * @param mixed $_data
     * @return mixed
     */
    public static function run ($_action, array $_params, $_data = null) {
        
        if (!is_string($_action) || !isset(self::$_actions[$_action])) { 
            
            throw new \Cataleya\Error ("Invalid plugin action: [" . $_action . "] in Social App Plugin.");

        }
        
       return call_user_func_array([ __CLASS__, self::$_actions[$_action] ], [$_params, $_data]);
                   
    }
    

        
    /**
     * run    This works like '::run'. It will be called whenever an event  
     *        your app is listening to is triggered. You have to specify there using 'events' in 
     *        your app.json file just like your plugin scopes.
     *
     * @param string $_event 
     * @param array $_params
     * @return void
     */
    public static function notify ($_event, array $_params = []) {
    
        
        
    }

    




    /* -------------- LOCAL ACTIONS: install, unistall etc. (actions to be performed on plugin itself -------------- */


    /**
     * install
     *
     * @param array $_params
     * @param array $_data
     * @return void
     */
    public static function install () {

        // 1. Setup db tables
       
        

        // 2. Initialize Config.
        self::$_Config = \__('this.config.init', [


            'TWITTER_API_APP_ID' => 'y9VRa2qVN91p6otdYyxYMp72B', 
            'TWITTER_API_SECRET' => 'BirBytr29ydB9ZpJoFm5KzJHanFoqdUxkVR6zfizFbSmhAZtEm', 
            'TWITTER_USER_HANDLE' => 'CataleyaApp',
            'TWITTER_USER_ID' => '',
            'TWITTER_TAGS' => '',  
            'TWITTER_COUNT' => 20,  

            
            'INSTAGRAM_API_APP_ID' => 'fa293d25ca3b4297bcff433b433ce36f', 
            'INTAGRAM_API_SECRET' => 'e6035f7b36c04633a50b4c7315b6ef48', 
            'INSTAGRAM_USER_HANDLE' => 'CataleyaApp',  
            'INSTAGRAM_USER_ID' => '',
            'INSTAGRAM_TAGS' => '',  
            'INSTAGRAM_COUNT' => 20,  

            'FACEBOOK_API_APP_ID' => '', 
            'FACEBOOK_API_SECRET' => '', 
            'FACEBOOK_USER_ID' => '',
            'FACEBOOK_TAGS' => '',  
            'FACEBOOK_COUNT' => 20,  
        
        ]);

        // 3. Prepare Cache Directory
        self::$_cache_dir = CATALEYA_CACHE_PATH.DIRECTORY_SEPARATOR.self::getHandle();
        if (!file_exists(self::$_cache_dir)) {
            mkdir(self::$_cache_dir, 0777);
        }

    }


    /**
     * uninstall
     *
     * @param array $_params 
     * @param array $_data
     * @return void
     */
    public static function uninstall () {

        // 1. Remove db tables
        // 2. General cleanup
        \Cataleya\Helper\File::remove(self::getCacheDir());

        $_Config = \__('this.config');
        if (!empty($_Config)) { $_Config->drop(); }

    }





    
    /* --------------- WORLD/APP-WIDE : Actions to be performed of stuff outside the plugin ------- */


    /**
     * getConfigFormControls
     *
     * @param array $_params
     * @param array $_data
     * @return array
     */
    protected static function getConfigFormControls (array $_params, array $_data = []) {

        $_fields = [

            'instagram' => [

                'field_group' => true,
                'handle' => 'instagram', 
                'title' => 'Instagram Settings',  
                'fields' => SocialApp\Instagram::getConfigFormControls()

            ], 
            'twitter' => [

                'field_group' => true,
                'handle' => 'twitter', 
                'title' => 'Twitter Settings',  
                'fields' => SocialApp\Twitter::getConfigFormControls()

            ]
        ];


        return array_merge($_data, $_fields);


    }

    


    /**
     * saveConfig
     *
     * @param array $_params
     * @param array $_data
     * @return void
     */
    public static function saveConfig (array $_params, $_data = null) 
    {


        // 1. Whenever this is called, convert social handles to user_ids and save to config.
        
        // a. Refresh instagram user id...
        SocialApp\Instagram::refreshUserID();

    }








    /**
     * getPartials
     *
     * @param array $_params
     * @param array $_data
     * @return array
     */
    protected static function getPartials (array $_params, $_data = []) {

        $_partials = [];

        // 1. Instagram
        // if (self::getConfig()->getParam('INSTAGRAM_ENABLED')) {
            $_partials['instagram'] = SocialApp\Instagram::getFeed();
        // } 

        return array_merge($_data, $_partials);

    }



 

}
