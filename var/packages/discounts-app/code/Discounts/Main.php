<?php

namespace Cataleya\Apps\Discounts;
use \Cataleya\Apps\Discounts;
    


/**
 * 
 *
 * @package Discounts    Create and manage discount codes.
 * @author Gboyega Dada <gboyega@fpplabs.com>
 * @version 0.0.1
 */
class Main implements \Cataleya\Plugins\Controller {


    protected static $_plugin_handle = 'discounts-app';
    protected static $_Config = null;
    protected static $_initialized = false;
    protected static $_cache_dir = '';

    public static $_actions = [
        'get-sale-price' => 'getSalePrice', 
        'apply-code' => 'applyDiscountCode', 
        'get-partials' => 'getPartials', 
        'get-config-form-controls' => 'getConfigFormControls', 
        'update-config' => 'saveConfig', 
        'get-form-controls' => 'getCheckoutFormControls', 
    ];





    /**
     * init
     *
     * @return void
     */
    public static function init () {

        if (self::$_initialized) { return true; }

        /* -----------------  Do init stuff ----------------- */

        self::getConfig(true);
        // self::$_cache_dir = CATALEYA_CACHE_DIR.DIRECTORY_SEPARATOR.self::getHandle();

        /* ---------------- End of init stuff --------------- */


        self::$_initialized = true;
    }



    /**
     * getActions            Get list of events/hooks that Plugin Controller will repond to
     *
     * @return array         An rray of |actions| (strings), that is, a list of events/hooks our plugin 
     *                       will respond to. Eg. 'payments.start-transaction' and so on.
     *
     */
    public static function getActions () {

        return array_keys(self::$_actions);

    }



    /**
     * getHandle
     *
     * @return string
     */
    public static function getHandle() {

        return self::$_plugin_handle;
    }


    /**
     * getKey   Alias for ::getHandle()
     *
     * @return string
     */
    public static function getKey() {

        return self::getHandle();
    }



    /**
     * getConfig
     *
     * @return void
     */
    public static function getConfig ($_reload=false) {
        
        if (empty(self::$_Config) || $_reload===true) self::$_Config = \__('this.config');
        return self::$_Config;

    }



    /**
     * run    This is like the regular 'exec' method that a controller has; 
     *        basically functioning as a gateway.
     *
     * @param string $_action 
     * @param array $_params
     * @param mixed $_data
     * @return mixed
     */
    public static function run ($_action, array $_params, $_data = null) {
        
        if (!is_string($_action) || !isset(self::$_actions[$_action])) { 
            
            throw new \Cataleya\Error ("Invalid plugin action: [" . $_action . "] in Discounts Plugin.");

        }
        
       return call_user_func_array([ __CLASS__, self::$_actions[$_action] ], [$_params, $_data]);
                   
    }
    
    
    /**
     * run    This works like '::run'. It will be called whenever an event  
     *        your app is listening to is triggered. You have to specify there using 'events' in 
     *        your app.json file just like your plugin scopes.
     *
     * @param string $_event 
     * @param array $_params
     * @return void
     */
    public static function notify ($_event, array $_params = []) {
    
        
        
    }

    



    /* -------------- LOCAL ACTIONS: install, unistall etc. (actions to be performed on plugin itself -------------- */


    /**
     * install
     *
     * @param array $_params
     * @param array $_data
     * @return void
     */
    public static function install () {

        // 1. Setup db tables
        // tables: 'app_ca_discount_codes', 'app_ca_redeem_track'
       
        

        // 2. Initialize Config.
        self::$_Config = \__('this.config.init', [


            'TWITTER_API_APP_ID' => 'y9VRa2qVN91p6otdYyxYMp72B', 
            'TWITTER_API_SECRET' => 'BirBytr29ydB9ZpJoFm5KzJHanFoqdUxkVR6zfizFbSmhAZtEm', 
            'TWITTER_USER_HANDLE' => 'CataleyaApp',
            'TWITTER_USER_ID' => '',
            'TWITTER_TAGS' => '',  
        
        ]);

        // 3. Prepare Cache Directory
        self::$_cache_dir = \__path('this.cache');
        if (!file_exists(self::$_cache_dir)) {
            mkdir(self::$_cache_dir, 0777);
        }

    }


    /**
     * uninstall
     *
     * @param array $_params 
     * @param array $_data
     * @return void
     */
    public static function uninstall () {

        // 1. Remove db tables
        // 2. General cleanup
        // \Cataleya\Helper\File::remove(self::getCacheDir());

        $_Config = \__('this.config');
        if (!empty($_Config)) { $_Config->drop(); }

    }





    
    /* --------------- WORLD/APP-WIDE : Actions to be performed of stuff outside the plugin ------- */


    /**
     * getConfigFormControls
     *
     * @param array $_params
     * @param array $_data
     * @return array
     */
    protected static function getConfigFormControls (array $_params, $_data = []) {

        $_fields = [

        ];


        return $_data;


    }

    


    /**
     * saveConfig
     *
     * @param array $_params
     * @param array $_data
     * @return void
     */
    public static function saveConfig (array $_params, $_data = []) 
    {


        

    }



    /**
     * getCheckoutFormControls
     *
     * @param array $_params
     * @param array $_data
     * @return array
     */
    protected static function getCheckoutFormControls (array $_params, array $_data = []) {

        static $_fields = [

            [
                'name' => 'discount_app_handle*',
                'type' => 'alphanum', 
                'label' => '', 
                'hint' => '',
                'control' => 'hidden'
            ],

            [
                'name' => 'discount_code*',
                'type' => 'alphanum', 
                'label' => 'Coupon Code', 
                'hint' => 'Please enter your Coupon Code.'
            ]
            
        ];


        return array_merge($_data, $_fields);


    }








    /**
     * getSalePrice
     *
     * @param array $_params
     * @param float $_data
     * @return void
     */
    public static function getSalePrice(array $_params = [], $_data = 0) 
    {

        $_discount_price = is_numeric($_data) ? (float)$_data : 0;
        if ($_discount_price === 0) return $_data;

        foreach (\Cataleya\Sales\Sales::load($_params['store']) as $_Sale) {

            if ($_Sale->isOn() && $_Sale->hasProductID($_params['product_id'])) {

                $_discount_price = $_Sale->computeDiscountPrice($_discount_price);

            }

        }


        return $_discount_price;


    }






    /**
     * applyDiscountCode
     *
     * @param array $_params
     * @param mixed $_data
     * @return void
     */
    public static function applyDiscountCode(array $_params, $_data = []) 
    {

        $_Code = \Cataleya\Sales\Coupon::load($_params['store'], $_params['discount_code']);
        if (empty($_Code)) return [
            'error' => [
                'code' => 0, 
                'message' => 'Coupon does not exist. Please check that your discount code is entered correctly.'
            ],
            'discount' => null
        ];


        // Redeemed ??
        if ($_Code->isRedeemed($_params['customer']))  return [
            'error' => [
                'code' => 0, 
                'message' => 'Discount code has already been used.'
            ],
            'discount' => null
        ];

        // if cart is not successfully loaded i.e. no existing cart, create.
        $_Cart->setCookie (); // Make sure cookie is fresh

        $_label = 'COUPON ';



        $_discount = [
            'app_handle' => self::getHandle(), 
            'label' => $_params['discount_code'], 
            'description' => $_label,
            'code' => $_params['discount_code'],  
            'type' => $_Code->getDiscountType(), 
            'value' => $_Code->getDiscountAmount(), 
            'currency_code' => $_params['store']->getCurrencyCode(), 
            'less' => $_Code->calculateDiscount($_params['cart'])
        ];


        $_params['cart']->addDiscount($_discount);

        $_data[] = [
            'error' => null,
            'discount' => $_discount
        ];


        return $_data;

    }



 

}
