<?php

namespace Cataleya\Apps\Coupons;
use \Cataleya\Apps\Coupons;
    


/**
 * 
 *
 * @package Coupons    Create and manage discount codes.
 * @author Gboyega Dada <gboyega@fpplabs.com>
 * @version 0.0.1
 */
class Main implements \Cataleya\Plugins\Controller {


    protected static $_plugin_handle = 'coupons-app';
    protected static $_Config = null;
    protected static $_initialized = false;
    protected static $_cache_dir = '';

    public static $_actions = [
        'apply' => 'applyDiscountCode', 
        'get-partials' => 'getPartials', 
        'get-config-form-controls' => 'getConfigFormControls', 
        'update-config' => 'saveConfig', 
    ];





    /**
     * init
     *
     * @return void
     */
    public static function init () {

        if (self::$_initialized) { return true; }

        /* -----------------  Do init stuff ----------------- */

        self::getConfig(true);
        // self::$_cache_dir = CATALEYA_CACHE_DIR.DIRECTORY_SEPARATOR.self::getHandle();

        /* ---------------- End of init stuff --------------- */


        self::$_initialized = true;
    }



    /**
     * getActions            Get list of events/hooks that Plugin Controller will repond to
     *
     * @return array         An rray of |actions| (strings), that is, a list of events/hooks our plugin 
     *                       will respond to. Eg. 'payments.start-transaction' and so on.
     *
     */
    public static function getActions () {

        return array_keys(self::$_actions);

    }



    /**
     * getHandle
     *
     * @return string
     */
    public static function getHandle() {

        return self::$_plugin_handle;
    }


    /**
     * getKey   Alias for ::getHandle()
     *
     * @return string
     */
    public static function getKey() {

        return self::getHandle();
    }



    /**
     * getConfig
     *
     * @return void
     */
    public static function getConfig ($_reload=false) {
        
        if (empty(self::$_Config) || $_reload===true) self::$_Config = \__('this.config');
        return self::$_Config;

    }



    /**
     * run    This is like the regular 'exec' method that a controller has; 
     *        basically functioning as a gateway.
     *
     * @param string $_action
     * @param array $_data
     * @return mixed
     */
    public static function run ($_action, array $_data = []) {
        
        if (!is_string($_action) || !isset(self::$_actions[$_action])) { 
            
            throw new \Cataleya\Error ("Invalid plugin action: [" . $_action . "] in Coupons Plugin.");

        }
        
       return call_user_func_array([ __CLASS__, self::$_actions[$_action] ], [$_action, $_data]);
                   
    }
    
    
    /**
     * run    This works like '::run'. It will be called whenever an event  
     *        your app is listening to is triggered. You have to specify there using 'events' in 
     *        your app.json file just like your plugin scopes.
     *
     * @param string $_event 
     * @param array $_params
     * @return void
     */
    public static function notify ($_event, array $_params = []) {
    
        
        
    }

    



    /* -------------- LOCAL ACTIONS: install, unistall etc. (actions to be performed on plugin itself -------------- */


    /**
     * install
     *
     * @param string $_action
     * @param array $_data
     * @return void
     */
    public static function install () {

        // 1. Setup db tables
        // tables: 'app_ca_discount_codes', 'app_ca_redeem_track'
       
        

        // 2. Initialize Config.
        self::$_Config = \__('this.config.init', [


            'TWITTER_API_APP_ID' => 'y9VRa2qVN91p6otdYyxYMp72B', 
            'TWITTER_API_SECRET' => 'BirBytr29ydB9ZpJoFm5KzJHanFoqdUxkVR6zfizFbSmhAZtEm', 
            'TWITTER_USER_HANDLE' => 'CataleyaApp',
            'TWITTER_USER_ID' => '',
            'TWITTER_TAGS' => '',  

            
            'INSTAGRAM_API_APP_ID' => 'fa293d25ca3b4297bcff433b433ce36f', 
            'INTAGRAM_API_SECRET' => 'e6035f7b36c04633a50b4c7315b6ef48', 
            'INSTAGRAM_USER_HANDLE' => 'CataleyaApp',  
            'INSTAGRAM_USER_ID' => '',
            'INSTAGRAM_TAGS' => '',  

            'FACEBOOK_API_APP_ID' => '', 
            'FACEBOOK_API_SECRET' => '', 
            'FACEBOOK_USER_ID' => '',
            'FACEBOOK_TAGS' => '',  
        
        ]);

        // 3. Prepare Cache Directory
        self::$_cache_dir = CATALEYA_CACHE_PATH.DIRECTORY_SEPARATOR.self::getHandle();
        if (!file_exists(self::$_cache_dir)) {
            mkdir(self::$_cache_dir, 0777);
        }

    }


    /**
     * uninstall
     *
     * @param string $_action 
     * @param array $_data
     * @return void
     */
    public static function uninstall () {

        // 1. Remove db tables
        // 2. General cleanup
        // \Cataleya\Helper\File::remove(self::getCacheDir());

        $_Config = \__('this.config');
        if (!empty($_Config)) { $_Config->drop(); }

    }





    
    /* --------------- WORLD/APP-WIDE : Actions to be performed of stuff outside the plugin ------- */


    /**
     * getConfigFormControls
     *
     * @param string $_action
     * @param array $_data
     * @return array
     */
    protected static function getConfigFormControls ($_action, array $_data = []) {

        if (!in_array(self::getHandle(), $_data)) { return []; }

        $_fields = [

            
        ];


        return [ self::getHandle() => $_fields ];


    }

    


    /**
     * saveConfig
     *
     * @param string $_action
     * @param array $_data
     * @return void
     */
    public static function saveConfig ($_action, array $_data = []) 
    {


        

    }





    public static function applyDiscountCode($_action, array $_data = []) 
    {

        $_Code = \Cataleya\Sales\Coupon::load($_data['store'], $_data['discount_code']);
        if (empty($_Code)) return [
            'error' => [
                'code' => 0, 
                'message' => 'Coupon does not exist. Please check that your discount code is entered correctly.'
            ],
            'discount' => null
        ];


        // Redeemed ??
        if ($_Coupon->isRedeemed($_data['customer']))  return [
            'error' => [
                'code' => 0, 
                'message' => 'Discount code has already been used.'
            ],
            'discount' => null
        ];

        // if cart is not successfully loaded i.e. no existing cart, create.
        $_Cart->setCookie (); // Make sure cookie is fresh

        $_label = 'COUPON ';



        $_discount = [
            'app_handle' => self::getHandle(), 
            'label' => $_data['discount_code'], 
            'description' => $_label,
            'code' => $_data['discount_code'],  
            'type' => $_Code->getDiscountType(), 
            'value' => $_Code->getDiscountAmount(), 
            'currency_code' => $_data['store']->getCurrencyCode(), 
            'less' => $_Code->calculateDiscount($_data['cart'])
        ];


        return [
            'error' => null,
            'discount' => $_discount
        ];




    }



 

}
