<?php


namespace Cataleya\Apps\Coupons;
use \Cataleya\Apps\Coupons;
    


/**
 * 
 * DiscountCode
 *
 * @package Coupons    Create and manage discount codes.
 * @author Gboyega Dada <gboyega@fpplabs.com>
 * @version 0.0.1
 */
class DiscountCode extends \Cataleya\Catalog\ProductCollection {
   
    // To be used by [\Cataleya\Collection]
    /*
    protected $_collection_obj;
    protected $_collection = array ();
    protected $_position = 0;
    
    // CONST VALUES 1 - 15 RESERVED FOR [ _Abstract_Collection ]
    const ORDER_BY_TITLE = 16;
    const ORDER_BY_VIEWS = 17;
    const ORDER_BY_SALES = 18;
    const ORDER_BY_PRICE = 19;
    */
    
    protected $_data = array();
    protected $_modified = array();
    
    
    static $_just_redeemed = array();
	
    private $_description;
    
    
    protected 
                $_page_size, 
                $_page_num;

 

        public function __construct() {
        parent::__construct();
        
        // Get database handle...
        $this->dbh = \Cataleya\Helper\DBH::getInstance();
		
        // Get error handler
        $this->e = \Cataleya\Helper\ErrorHandler::getInstance();
        

        
    }
    
    
  
    public function __destruct() {
        $this->saveData();
    }

    







    
	/*
	 *
	 * [ load ]
	 * ________________________________________________________________
	 *
	 *
	 *
	 */
	 
	 
	public static function loadByID ($id = 0, $filters = array(), $sort = NULL, $page_size = 16, $last_seen = 0, $page_num = 0)
	{
		
		if (!is_numeric($page_size) || !is_numeric($page_num)) throw new \Cataleya\Error('Invalid argument (invalid number or pagesize)');
                
        if (filter_var($id, FILTER_VALIDATE_INT) === false || (int)$id < 1) throw new \Cataleya\Error ('Invalid argument (invalid coupon id)');
                
                
		
        // Create [Coupon] instance...
		$instance = new static ();
                
		
                
		// LOAD: SALE INFO
		static $select_handle1, $param_coupon_id;
		
			
		if (empty($select_handle1)) {
			// PREPARE SELECT STATEMENT...
			$select_handle1 = $instance->dbh->prepare('
                            SELECT  
                            a.coupon_id, a.code, a.store_id, c.count, a.discount_type, a.amount, a.minimum_order, 
                            DATE(a.start_date) as start_date, DATE(a.expire_date) as expire_date, 
                            a.indefinite, a.uses_per_coupon, a.uses_per_user, a.is_active, 
                            a.restrict_to_zone, 
                            a.date_created, a.last_modified 
                            FROM app_ca_discount_codes a 
                            INNER JOIN collection c 
                            ON a.collection_id = c.collection_id 
                            WHERE a.coupon_id = :coupon_id LIMIT 1
                                ');
			$select_handle1->bindParam(':coupon_id', $param_coupon_id, \PDO::PARAM_INT);
		}
		
		
		$param_coupon_id = $id;
		
		if (!$select_handle1->execute()) throw new \Cataleya\Error ('DB Error: ' . implode(', ', $select_handle1->errorInfo()));

		$instance->_data = $select_handle1->fetch(\PDO::FETCH_ASSOC);
		if (empty($instance->_data)) return NULL;
		


                
        /////////////////// LOAD ASSOCIATED PRODUCTS /////////////////////
        $instance->_loadCollection($filters, $sort, $page_size, $last_seen, $page_num);

        
        if (!key_exists((int)$id, self::$_just_redeemed)) self::$_just_redeemed[(int)$id] = array ();

        // RETURN SALE INSTANCE
		return $instance;	
		
		
	}
        
        

        /*
         *
         * [ loadByCode ] 
         *_____________________________________________________
         *
         *
         */

        public static function load(\Cataleya\Store $_Store, $code = NULL, $sort = NULL, $page_size = 16, $page_num = 1)
        {
                if (!is_string($code)) return NULL;


		static $select_handle, $param_coupon_code, $param_store_id;
		
			
		if (!isset($select_handle)) {
			// PREPARE SELECT STATEMENT...
			$select_handle = \Cataleya\Helper\DBH::getInstance()->prepare('SELECT coupon_id FROM app_ca_discount_codes WHERE code = :coupon_code AND store_id = :store_id LIMIT 1');
			$select_handle->bindParam(':coupon_code', $param_coupon_code, \PDO::PARAM_STR);
                        $select_handle->bindParam(':store_id', $param_store_id, \PDO::PARAM_INT);
		}
		
		
		$param_coupon_code = $code;
                $param_store_id = $_Store->getStoreId();
		
		if (!$select_handle->execute()) \Cataleya\Helper\ErrorHandler::getInstance()->triggerException('
                                                                            Error in class (' . __CLASS__ . '): [ ' . 
                                                                            implode(', ', $select_handle->errorInfo()) . 
                                                                            ' ] on line ' . __LINE__);

		$row = $select_handle->fetch(\PDO::FETCH_ASSOC);
                
		if (empty($row)) return NULL;
                else return self::loadByID ($row['coupon_id'], array(), $sort, $page_size, $page_num);
                

        }    







	/*
	 *
	 *  [ create ]
	 * ________________________________________________________________
	 * 
	 * 
	 *
	 *
	 *
	 */
	 
    static public function create (
        \Cataleya\Store $_Store, 
        $_code = NULL, 
        array $_options = [ 
            'discount_type' => \Cataleya\Catalog\Price::TYPE_REDUCTION_PERCENT, 
            'discount_amount' => 0, 
            'expires_after_uses' => 100, 
            'active' => TRUE 
        ]) 
	{
		
                $code = (is_string($_code)) ? trim($_code) : '';
		
                // Validate arguments
		if (
                !isset($_options['discount_type'], $_options['discount_amount'], $_options['is_active'], $_options['start_date'], $_options['end_date'], $_options['expires_after_uses']) || empty($_code) || 
                        
                                (
				$_options['discount_type'] !== \Cataleya\Catalog\Price::TYPE_REDUCTION && 
				$_options['discount_type'] !== \Cataleya\Catalog\Price::TYPE_REDUCTION_PERCENT && 
                                !empty($_code)
                                        )
				
				) \Cataleya\Helper\ErrorHandler::getInstance()->triggerException('Error in class (' . __CLASS__ . '): [ Invalid arguments supplied in method: ' . __FUNCTION__ . '  ] on line ' . __LINE__);
            
		
                $_code = strtoupper($_code);
                $_params = array (
                    'store_id'  =>  $_Store->getStoreId(), 
                    'code'  =>  \Cataleya\Helper\Validator::couponcode($_code), 
                    'collection_id' => self::_initCollection(), 
                    'amount'    =>  \Cataleya\Helper\Validator::float($_options['discount_amount']), 
                    'discount_type' =>  $_options['discount_type'], 
                    'start_date'    =>  \Cataleya\Helper\Validator::date($_options['start_date']), 
                    'expire_date'    =>  \Cataleya\Helper\Validator::date($_options['end_date']), 
                    'indefinite'    =>  \Cataleya\Helper\Validator::bool($_options['valid_until_never']), 
                    'uses_per_coupon'    =>  \Cataleya\Helper\Validator::int($_options['expires_after_uses'], 1), 
                    'is_active' =>  \Cataleya\Helper\Validator::bool($_options['is_active'])
                );
                

                if ($_params['discount_type'] === \Cataleya\Catalog\Price::TYPE_REDUCTION_PERCENT && $_params['amount'] > 100) $_params['amount'] = FALSE;
                
                
                foreach ($_params as $_k=>$_param) {
                    
                    if (($_param === FALSE && !in_array($_k, ['is_active', 'indefinite'])) || ($_k === 'is_active' && $_param === NULL)) \Cataleya\Helper\ErrorHandler::getInstance()->triggerException('Error in class (' . __CLASS__ . '): [ Bad param (' . $_k . ') in method: ' . __FUNCTION__ . '  ] on line ' . __LINE__);
                    
                }
            

		
		// Get database handle...
        $dbh = \Cataleya\Helper\DBH::getInstance();
		
		// Get error handler
		$e = \Cataleya\Helper\ErrorHandler::getInstance();
		
		// construct
		static 
				$insert_handle;
		
		if (empty($insert_handle))
		{
			$insert_handle = $dbh->prepare('
                            INSERT INTO app_ca_discount_codes 
                            (store_id, collection_id, code, discount_type, amount, ' . 
                            'start_date, expire_date, indefinite, uses_per_coupon, is_active, ' . 
                            'date_created, last_modified) 
                            VALUES (:store_id, :collection_id, :code, :discount_type, :amount, ' . 
                            ':start_date, :expire_date, :indefinite, :uses_per_coupon, ' . 
                            ':is_active, NOW(), NOW())
                        ');
		}
		
		foreach ($_params as $key=>$param) $insert_handle->bindParam(':'.$key, $_params[$key], \Cataleya\Helper\DBH::getTypeConst($param));
                
                
		
		if (!$insert_handle->execute()) $e->triggerException('
                                                            Error in class (' . __CLASS__ . '): [ ' . 
                                                            implode(', ', $insert_handle->errorInfo()) . 
                                                            ' ] on line ' . __LINE__);

		
		// AUTOLOAD NEW PRODUCT AND RETURN IT
		$instance = self::load($dbh->lastInsertId());
               
		
		return $instance;
		
		
	}
        
        
        
        
        

        
       




	
	
	public function saveData () 
	{
		

                
		if (empty($this->_modified)) return;
                
                $this->_modified = array_unique($this->_modified);
                
		$_update_params = array();
		$key_val_pairs = array();
		
		foreach ($this->_modified as $key) $key_val_pairs[$key] = $key . ' = :' . $key;
		
	
		// Do prepared statement for 'INSERT'...
		$update_handle = $this->dbh->prepare('
												UPDATE app_ca_discount_codes 
												SET ' . implode (', ', $key_val_pairs) . ', last_modified=now()       
												WHERE coupon_id = :coupon_id 
												');
		$update_handle->bindParam(':coupon_id', $_update_params['coupon_id'], \PDO::PARAM_INT);
		$_update_params['coupon_id'] = $this->_data['coupon_id'];
                
		foreach ($this->_modified as $key) {
                    
                        $_update_params[$key] = $this->_data[$key];                        
			$update_handle->bindParam(':'.$key, $_update_params[$key], \Cataleya\Helper\DBH::getTypeConst($_update_params[$key]));

		}
		
		if (!$update_handle->execute()) $this->e->triggerException('Error in class (' . __CLASS__ . '): [ ' . implode(', ', $update_handle->errorInfo()) . ' ] on line ' . __LINE__);
		
	}
        
        
        
        

	 
	 /*
	 *  [ delete ]
	 * ________________________________________________________________
	 * 
	 * 
	 *
	 *
	 */

	 
	public function delete () 
	{
            
            
            
            
                // //// BEGIN TRANSACTION /////
                \Cataleya\Helper\DBH::getInstance()->beginTransaction();



                \Cataleya\Helper\DBH::sanitize(array('coupons'), 'coupon_id', $this->getID());
                
                // //// COMMIT TRANSACTION /////
                \Cataleya\Helper\DBH::getInstance()->commit();

		return TRUE;
		
		
	}


	
	


        
      /*
       * 
       * Misc [getters]
       * 
       */
        

        
       
        
        
        /*
         *
         * [ getSize ] 
         *_____________________________________________________
         *
         *
         */

        public function getSize()
        {
                return count($this->_collection);
        }  
        
        
        
        
        /*
         *
         * [ getCode ] 
         *_____________________________________________________
         *
         *
         */

        public function getCode()
        {
                return $this->_data['code'];
        } 
        
        
        
        /*
         *
         * [ getCouponId ] 
         *_____________________________________________________
         *
         *
         */

        public function getCouponId()
        {
                return $this->_data['coupon_id'];
        } 
        
        
        
        
        /*
         *
         * [ getID ] 
         *_____________________________________________________
         *
         *
         */

        public function getID()
        {
                return $this->_data['coupon_id'];
        }  



        
        
        /*
         *
         * [ getUsesPerShopper ] 
         * ______________________________________________
         *
         */

        public function getUsesPerShopper()
        {
                return (int)$this->_data['uses_per_user'];
        }
        
        
        
        /*
         *
         * [ getUsesPerCoupon ] 
         * ______________________________________________
         *
         */

        public function getUsesPerCoupon()
        {
                return (int)$this->_data['uses_per_coupon'];
        }
        
        
        
        
        /*
         *
         * [ getUses ] 
         * ______________________________________________
         *
         */

        public function countUses($_force_recount = FALSE)
        {
		
		static $select_handle, $param_instance_id, $_row;
		
		if (empty($_row) || $_force_recount === TRUE) 
                {
                        if (!isset($select_handle)) {
                                // PREPARE SELECT STATEMENT...
                                $select_handle = $this->dbh->prepare('SELECT COUNT(*) as uses FROM app_ca_redeem_track WHERE coupon_id = :instance_id LIMIT 1');
                                $select_handle->bindParam(':instance_id', $param_instance_id, \PDO::PARAM_INT);
                        }


                        $param_instance_id = $this->getID();

                        if (!$select_handle->execute()) $this->e->triggerException('
                                                                                    Error in class (' . __CLASS__ . '): [ ' . 
                                                                                    implode(', ', $select_handle->errorInfo()) . 
                                                                                    ' ] on line ' . __LINE__);

                        $_row = $select_handle->fetch(\PDO::FETCH_ASSOC);
                }
                
                return $_row['uses'];
        }
        
        
        
        
        
        
        /*
        *
        * [ getStartDate ]
        * ______________________________________________________ 
        *
        */

       public function getStartDate($_as_array = FALSE)
       {
               if (is_bool($_as_array) && $_as_array === TRUE) 
               {
                   $_date = array ();
                   list($_date['year'], $_date['month'], $_date['day']) = explode('-', $this->_data['start_date']);
                   
                   return $_date;
               }
               
               else return $this->_data['start_date'];
       } 
       
       
       
        /*
        *
        * [ getEndDate ]
        * ______________________________________________________ 
        *
        */

       public function getEndDate($_as_array = FALSE)
       {
               if (is_bool($_as_array) && $_as_array === TRUE) 
               {
                   $_date = array ();
                   list($_date['year'], $_date['month'], $_date['day']) = explode('-', $this->_data['expire_date']);
                   
                   return $_date;
               }
               
               else return $this->_data['expire_date'];
       } 
       
   
  
        /*
         *
         * [ isExpired ] 
         *_____________________________________________________
         *
         *
         */

        public function isExpired()
        {
                return (strtotime($this->_data['expire_date']) < time() && !$this->isIndefinite()) ? TRUE : FALSE;
        }
        
        
        
        
        /*
         *
         * [ isValid ] 
         *_____________________________________________________
         *
         *
         */

        public function isValid()
        {
                return (!$this->isExpired() && $this->isActive() && ($this->countUses() < $this->getUsesPerCoupon())) ? TRUE : FALSE;
        }
        
        
        

        /*
         *
         * [ isIndefinite ] 
         *_____________________________________________________
         *
         *
         */

        public function isIndefinite()
        {
                return ((int)$this->_data['indefinite'] === 1) ? TRUE : FALSE;
        }
       
       
       
       
 
        
        /*
         *
         * [ getDateCreated ] 
         * ______________________________________________
         *
         */

        public function getDateCreated()
        {
                return $this->_data['date_created'];
        }
        
        
        
        
        /*
         *
         * [ getLastModified ]
         * ______________________________________________ 
         *
         */
        
        public function getLastModified()
        {

                return $this->_data['last_modified'];
        }        

        
        
        
        
        

        /*
         *
         * [ isOn ] 
         *_____________________________________________________
         *
         *
         */

        public function isOn()
        {
                return ((int)$this->_data['is_active'] === 1) ? TRUE : FALSE;
        }
        
        
        
        /*
         *
         * [ isActive ] 
         *_____________________________________________________
         *
         *
         */

        public function isActive()
        {
                return ((int)$this->_data['is_active'] === 1) ? TRUE : FALSE;
        }



        
            

        /*
         * 
         * [ enable ]
         * ______________________________________________________
         * 
         * 
         */


        public function enable () 
        {

            if ((int)$this->_data['is_active'] === 0) 
            {
                $this->_data['is_active'] = 1;
                $this->_modified[] = 'is_active';
            }

            return $this;
        }





        /*
         * 
         * [ disable ]
         * ______________________________________________________
         * 
         * 
         */


        public function disable () 
        {
            if ((int)$this->_data['is_active'] === 1) 
            {
                $this->_data['is_active'] = 0;
                $this->_modified[] = 'is_active';
            }

            return $this;
        }
        
        
        
        
        
        
        /*
         *
         * [ getStoreId ] 
         *_____________________________________________________
         *
         *
         */

        public function getStoreId()
        {
                return $this->_data['store_id'];
        }
        
        
        
        /*
         *
         * [ getStore ] 
         *_____________________________________________________
         *
         *
         */

        public function getStore()
        {
                return \Cataleya\Store::load($this->_data['store_id']);
        }

        
        
         
        /*
         * 
         * [ setStore ]
         * ______________________________________________________
         * 
         * 
         */


        public function setStore (\Cataleya\Store $_Store) 
        {
            $this->_data['store_id'] = $_Store->getID();
            $this->_modified[] = 'store_id';

            return $this;
        }
        
        
        
        
        
        
        
        /*
         * 
         *  [ getDiscountFor ]
         * 
         * 
         */
        
        public function getDiscountFor (\Cataleya\Catalog\Product $_product) 
        {
            
        }





        
        /*
         *
         * [ redeem ] 
         *_____________________________________________________
         *
         *
         */

        public function redeem(\Cataleya\Store\Order $_Order)
        {
            
                $k = $_Order->getCustomerId();
                $cid = (int)$this->getCouponId();
                
                if (key_exists($k, self::$_just_redeemed[$cid])) return $this;
                else self::$_just_redeemed[$cid][$k] = array (
                    'customer_id' => $_Order->getCustomerId(), 
                    'order_id' => $_Order->getOrderId()
                        );
                

                static 
                        $insert_handle, 
                        $insert_handle_param_coupon_id, 
                        $insert_handle_param_customer_id, 
                        $insert_handle_param_order_id;

                if (empty($insert_handle))
                {
                        $insert_handle = $this->dbh->prepare('
                                                            INSERT INTO app_ca_redeem_track  (coupon_id, customer_id, order_id, redeem_date)
                                                            VALUES (:coupon_id, :customer_id, :order_id, NOW())  
                                                            ');

                    $insert_handle->bindParam(':coupon_id', $insert_handle_param_coupon_id, \PDO::PARAM_INT);
                    $insert_handle->bindParam(':customer_id', $insert_handle_param_customer_id, \PDO::PARAM_INT);
                    $insert_handle->bindParam(':order_id', $insert_handle_param_order_id, \PDO::PARAM_INT);
                }
                
                

                $insert_handle_param_coupon_id = $this->getID();
                $insert_handle_param_customer_id = $_Order->getCustomerId();
                $insert_handle_param_order_id = $_Order->getOrderId();


                if (!$insert_handle->execute()) $this->e->triggerException('
                                                                        Error in class (' . __CLASS__ . '): [ ' . 
                                                                        implode(', ', $insert_handle->errorInfo()) . 
                                                                        ' ] on line ' . __LINE__);
                
                
                return $this;

        }
        
        
        
        

        
        

        /*
         *
         * [ isRedeemed ] 
         *_____________________________________________________
         *
         *
         */

        public function isRedeemed(\Cataleya\Customer $_Customer)
        {
            
                $k = $_Customer->getCustomerId();
                $cid = (int)$this->getCouponId();
                
                if (key_exists($k, self::$_just_redeemed[$cid])) return FALSE;
            
		static 
				$select_handle, 
                                $param_coupon_id, 
				$param_customer_id;
		
		if (!isset($select_handle)) {
			// PREPARE SELECT STATEMENT...
			$select_handle = $this->dbh->prepare('SELECT COUNT(*) FROM app_ca_redeem_track WHERE coupon_id = :coupon_id AND customer_id = :customer_id');
			$select_handle->bindParam(':coupon_id', $param_coupon_id, \PDO::PARAM_INT);
                        $select_handle->bindParam(':customer_id', $param_customer_id, \PDO::PARAM_INT);
		}
		
		$param_coupon_id = $this->getCouponId();
		$param_customer_id = $_Customer->getCustomerId();
		
		if (!$select_handle->execute()) $this->e->triggerException('
                                                                            Error in class (' . __CLASS__ . '): [ ' . 
                                                                            implode(', ', $select_handle->errorInfo()) . 
                                                                            ' ] on line ' . __LINE__);

		$row = $select_handle->fetch(\PDO::FETCH_NUM);
                
                return ((int)$row[0] >= $this->getUsesPerShopper()) ? TRUE : FALSE;
        }

        
        
        
        



       /*
       * 
       * Misc [ SETTERS ]
       * 
       */
        
         
        /*
        *
        * [ setCode ]
        * ______________________________________________________ 
        *
        */

       public function setCode($value)
       {
               $value = \Cataleya\Helper\Validator::couponcode($value);
               if ($value === FALSE) $this->e->triggerException('Error in class (' . __CLASS__ . '): [ Argument supplied in method: ' . __FUNCTION__ . ' must be an alpha-numeric string (1 to 40 long)  ] on line ' . __LINE__);
            
               
               $this->_data['code'] = strtoupper($value);
               $this->_modified[] = 'code';

               return $this;
       } 
       
       
       
     
       
        /*
        *
        * [ setStartDate ]
        * ______________________________________________________ 
        *
        */

       public function setStartDate($value = NULL)
       {
               $value = \Cataleya\Helper\Validator::date($value);
               if ($value === FALSE) $this->e->triggerException('Error in class (' . __CLASS__ . '): [ Argument supplied in method: ' . __FUNCTION__ . ' must be an alpha-numeric string (1 to 40 long)  ] on line ' . __LINE__);
            
               
               $this->_data['start_date'] = $value;
               $this->_modified[] = 'start_date';

               return $this;
       } 
       
       
       
        /*
        *
        * [ setEndDate ]
        * ______________________________________________________ 
        *
        */

       public function setEndDate($value = NULL)
       {
               if ($value === '0000-00-00') 
               {
                   $this->setIndefinite(TRUE);
               }
               
               else {
               $value = \Cataleya\Helper\Validator::date($value);
               if ($value === FALSE) $this->e->triggerException('Error in class (' . __CLASS__ . '): [ Argument supplied in method: ' . __FUNCTION__ . ' must be an alpha-numeric string (1 to 40 long)  ] on line ' . __LINE__);
               
               $this->setIndefinite(FALSE);
               
               $this->_data['expire_date'] = $value;
               $this->_modified[] = 'expire_date';
               
               }

               return $this;
       } 
       
       
       
       
        /*
         * 
         * [ setIndefinite ]
         * ______________________________________________________
         * 
         * 
         */


        private function setIndefinite ($value = NULL) 
        {
            $value = \Cataleya\Helper\Validator::bool($value);
            if ($value === NULL) $this->e->triggerException('Error in class (' . __CLASS__ . '): [ Argument supplied in method: ' . __FUNCTION__ . ' must be boolean.  ] on line ' . __LINE__);

            $this->_data['indefinite'] = $value;
            $this->_modified[] = 'indefinite';

            return $this;
        }
       
       
       
       
        /*
         *
         * [ setUsesPerCoupon ] 
         * ______________________________________________
         *
         */

        public function setUsesPerCoupon($value = 0)
        {
               $value = \Cataleya\Helper\Validator::int($value);
               if ($value === FALSE) throw new \Cataleya\Error ('Argument must be an integer.');

               $this->_data['uses_per_coupon'] = $value;
               $this->_modified[] = 'uses_per_coupon';
               
               return $this;
        }
        
        
        
        /*
         *
         * [ setExpiresAfterUses ] 
         * ______________________________________________
         *
         */

        public function setExpiresAfterUses($value = 0)
        {
            return $this->setUsesPerCoupon($value);
        }
       
       
       
       
       
        /*
        *
        * [ getDiscountType ]
        * ______________________________________________________ 
        *
        */

       public function getDiscountType()
       {

               return $this->_data['discount_type'];

       }     
       
       
       
       
        /*
        *
        * [ setDiscountType ]
        * ______________________________________________________ 
        *
        */

       public function setDiscountType($value = NULL)
       {

            // Validate discount type...
            if (
                            empty($value) || 

                            (
                            $value !== \Cataleya\Catalog\Price::TYPE_REDUCTION && 
                            $value !== \Cataleya\Catalog\Price::TYPE_REDUCTION_PERCENT 
                                    )

                            ) $this->e->triggerException('Error in class (' . __CLASS__ . '): [ Argument supplied in method: ' . __FUNCTION__ . ' must be an instance of appropriate discount type  ] on line ' . __LINE__);
            

           
               $this->_data['discount_type'] = $value;
               $this->_modified[] = 'discount_type';
               return $this;

       }    

       
       
        /*
        *
        * [ setDiscountAmount ]
        * ______________________________________________________ 
        *
        */

       public function setDiscountAmount($value = NULL)
       {

            // Validate discount type...
           $value = filter_var($value, FILTER_VALIDATE_FLOAT);
           
           if ($value === FALSE) $this->e->triggerException('Error in class (' . __CLASS__ . '): [ Argument supplied in method: ' . __FUNCTION__ . ' must be a float or integer  ] on line ' . __LINE__);
           
           if ($this->getDiscountType() === \Cataleya\Catalog\Price::TYPE_REDUCTION_PERCENT && $value > 100) $this->e->triggerException('Error in class (' . __CLASS__ . '): [ Argument supplied in method: ' . __FUNCTION__ . ' must be a float or integer and less than 100  ] on line ' . __LINE__);
           
           
            $this->_data['amount'] = $value;
            $this->_modified[] = 'amount';
            return $this;

       }    

       
       
        /*
        *
        * [ getDiscountAmount ]
        * ______________________________________________________ 
        *
        */

       public function getDiscountAmount()
       {

               return round($this->_data['amount'], 2);

       }  
       
       
       
       
       
       
        /*
         *
         * [ computeDiscountPrice ] 
         *_____________________________________________________
         *
         * Returns price WITH coupon discounts applied (if any)
         *
         */

        public function computeDiscountPrice($_cost = NULL)
        {
            
            // Validate cost argument...
           $_cost = filter_var($_cost, FILTER_VALIDATE_FLOAT);
           
           if ($_cost === FALSE) throw new \Cataleya\Error ('Argument must be a float or integer.');
           
                       
            // Check if SALE is active
            if (!$this->isOn()) return $_cost;
            
            
            // Calculate discount

            // Percentage reduction
            if ($this->getDiscountType() === \Cataleya\Catalog\Price::TYPE_REDUCTION_PERCENT && $_cost > 0) 
            {
                $_cost = $_cost - (($this->getDiscountAmount()/100) * $_cost);
            }

            // or normal price reduction
            else if ($this->getDiscountType() === \Cataleya\Catalog\Price::TYPE_REDUCTION && $_cost > $this->getDiscountAmount()) 
            {
                $_cost = $_cost - $this->getDiscountAmount();
            }

            return round($_cost, 2);
            
        }


       
       


        
}


