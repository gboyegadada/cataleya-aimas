<?php

namespace Cataleya\Apps\DiamondPay;
use Cataleya\Apps\DiamondPay;

/**
 * 
 *
 * @package DiamondPay
 * @author Gboyega Dada <gboyega@fpplabs.com>
 * @version 0.0.1
 */
class Main implements \Cataleya\Plugins\Controller {


    protected static $_plugin_handle = 'diamondpay';
    private static $_log_table = 'app_diamondpay_log';
    protected static $_Config = null;

    public static $_actions = [

        'get-options' => 'getPaymentOptions', 
        'get-currency-code' => 'getCurrencyCode', 
        'get-max-amount' => 'getMaxAmount', 
        'get-min-amount' => 'getMinAmount', 

        'get-config-form-controls' => 'getConfigFormControls', 
        'get-form-controls' => 'getCheckoutFormControls', 
        'get-transaction-reference' => 'getTransactionRef', 
        'start-transaction' => 'startTransaction', 
        'complete-transaction' => 'getTransactionStatus', 
        'get-transaction-status' => 'getTransactionStatus', 

        // Returns transactions made on specified order_id
        'get-transactions' => 'getTransactions', 
        'get-instructions' => 'getPaymentInstructions'
    ];


    private static $_status_codes = [
            '00' => 'Successful', 
            '01' => 'Failed', 
            '02' => 'Pending', 
            '03' => 'Cancelled', 
            '04' => 'Not Processed', 
            '05' => 'Invalid Merchant', 
            '06' => 'Inactive Merchant', 
            '07' => 'Invalid Order ID', 
            '08' => 'Duplicate Order ID', 
            '09' => 'Invalid Amount'

        ];




    /**
     * init
     *
     * @return void
     */
    public static function init () {

        if (self::$_initialized) { return true; }

        /* -----------------  Do init stuff ----------------- */

        self::getConfig(true);

        /* ---------------- End of init stuff --------------- */


        self::$_initialized = true;
    }




    /**
     * getActions            Get list of events/hooks that Plugin Controller will repond to
     *
     * @return array         An rray of |actions| (strings), that is, a list of events/hooks our plugin 
     *                       will respond to. Eg. 'payments.start-transaction' and so on.
     *
     */
    public static function getActions () {

        return array_keys(self::$_actions);

    }



    /**
     * getHandle
     *
     * @return string
     */
    public static function getHandle() {

        if (empty(self::$_plugin_handle)) {
            self::init();
        }

        return self::$_plugin_handle;
    }


    /**
     * getKey   Alias for ::getHandle()
     *
     * @return string
     */
    public static function getKey() {

        return self::getHandle();;
    }




    /**
     * getConfig
     *
     * @return void
     */
    public static function getConfig ($_reload=false) {
        
        if (empty(self::$_Config) || $_reload===true) self::$_Config = \__('this.config');
        return self::$_Config;

    }



    /**
     * getLogTable
     *
     * @return string
     */
    public static function getLogTable() {
        return self::$_log_table;

    }



    /**
     * getStatusCodes
     *
     * @return array
     */
    public static function getStatusCodes () {
        return self::$_status_code;
    }



    /**
     * getStatusDescription
     *
     * @param string $_code     Code is numeric but should supplied like this: '00', '08'...
     *                          (that is -- zero padded quoted string).
     * @return string
     */
    public static function getStatusDescription ($_code) {

        return (isset(self::$_status_code[$_code])) 
            ? self::$_status_code[$_code] 
            : null; 

    }



    
    /**
     * run    This is like the regular 'exec' method that a controller has; 
     *        basically functioning as a gateway.
     *
     * @param string $_action 
     * @param array $_params
     * @param mixed $_data
     * @return mixed
     */
    public static function run ($_action, array $_params, $_data = null) {
        
        if (!is_string($_action) || !isset(self::$_actions[$_action])) { 
            
            throw new \Cataleya\Error ("Invalid plugin action: [" . $_action . "] in Diamond Pay App.");

        }
        
       return call_user_func_array([ __CLASS__, self::$_actions[$_action] ], [$_params, $_data]);
                   
    }


    
    /**
     * run    This works like '::run'. It will be called whenever an event  
     *        your app is listening to is triggered. You have to specify there using 'events' in 
     *        your app.json file just like your plugin scopes.
     *
     * @param string $_event 
     * @param array $_params
     * @return void
     */
    public static function notify ($_event, array $_params = []) {
    
        
        
    }

    



    /**
     * install
     *
     * @param array $_params
     * @param mixed $_data
     * @return void
     */
    public static function install () {

        $_Config = \__('this.config.init', [
            
            'MODE' => 'test', 

            'MERCHANT_ID' => 'Your Merchant ID',
            'MAC_KEY' => 'MAC KEY',  

            'TRANX_ENDPOINT' => 'https://cipg.diamondbank.com/cipg/MerchantServices/MakePayment.aspx',
            'TRANX_STATUS_ENDPOINT' => 'https://cipg.diamondbank.com/cipg/MerchantServices/UpayTransactionStatus.ashx',
            'USER_TRANX_HISTORY_ENDPOINT' => 'https://cipg.diamondbank.com/cipg/MerchantCustomerView/MerchantCustomerReport.aspx', 
        

            'SANDBOX_TRANX_ENDPOINT' => 'https://www.cashenvoy.com/sandbox/?cmd=cepay', 
            'SANDBOX_TRANX_STATUS_ENDPOINT' => 'https://www.cashenvoy.com/sandbox/?cmd=requery'
        ]);


    }


    /**
     * uninstall
     *
     * @param array $_params 
     * @param mixed $_data
     * @return void
     */
    public static function uninstall () {


        // 2. General cleanup
        $_Config = self::getConfig();
        if (!empty($_Config)) { $_Config->drop(); }

    }





    
    /* ----------------------- PLUGIN ACTIONS -------------------------- */


    /**
     * getConfigFormControls
     *
     * @param array $_params
     * @param mixed $_data
     * @return array
     */
    protected static function getConfigFormControls (array $_params, $_data = []) {

        $_fields = array (

            [
                'name' => 'MODE',
                'type' => 'alphanum', 
                'required' => true, 
                'label' => 'Gateway Mode',
                'value' => self::getConfig()->getParam('MODE'),  
                'hint' => 'Enter "test" to use test settings - OR - "live" to use live settings.',
                'control' => 'select', 
                'options' => [
                    [ 'label' => 'Sandbox (test)', 'value' => 'test' ], 
                    [ 'label' => 'Live (real payments)', 'value' => 'live' ] 
                ]
            ],
            [
                'name' => 'MERCHANT_ID',
                'type' => 'alphanum', 
                'required' => true, 
                'label' => 'Merchant ID',
                'value' => self::getConfig()->getParam('MERCHANT_ID'),  
                'hint' => 'Please enter your Merchant ID (this should issued to you at Diamond Bank Plc.)'
            ],
            [
                'name' => 'MAC_KEY',
                'type' => 'alphanum', 
                'required' => true, 
                'label' => 'MAC KEY', 
                'value' => self::getConfig()->getParam('MAC_KEY'),  
                'hint' => 'Please enter your MAC key (no spaces).'
            ]
        );


        return array_merge($_data, [ self::getHandle() => $_fields ]);


    }

    


    /**
     * saveConfig
     *
     * @param array $_params
     * @param mixed $_data
     * @return void
     */
    public static function saveConfig (array $_params, $_data = []) 
    {



    }








    /**
     * getPaymentOptions
     *
     * @param array $_params
     * @param mixed $_data
     * @return array
     */
    protected static function getPaymentOptions (array $_params, $_data = []) {

        return (
                isset($_params['currency_codes']) && 
                !in_array(self::getCurrencyCode(), $_params['currency_codes'])
                ) 
                
                ? $_data 

                : array_merge($_data, array (

                    [
                        'name' => 'Naira Master Card',
                        'logo' => 'path/to/logo.png', 
                        'id' => self::getHandle(), 
                        'currency_code' => self::getCurrencyCode(), 
                        'max_amount' => self::getMaxAmount(), 
                        'min_amount' => self::getMinAmount()
                    ],

                    [
                        'name' => 'Verve',
                        'logo' => 'path/to/logo.png', 
                        'id' => self::getHandle(), 
                        'currency_code' => self::getCurrencyCode(), 
                        'max_amount' => self::getMaxAmount(), 
                        'min_amount' => self::getMinAmount()
                    ],


                ));


    }




    /**
     * getCurrencyCode
     *
     * @param array $_params
     * @param array $_data
     * @return array
     */
    protected static function getCurrencyCode (array $_params = null, array $_data = []) {

        return 'NGN';

    }



    /**
     * getMaxAmount
     *
     * @param array $_params
     * @param array $_data
     * @return array
     */
    protected static function getMaxAmount (array $_params = null, array $_data = []) {

        return 200000;

    }




    /**
     * getMinAmount
     *
     * @param array $_params
     * @param array $_data
     * @return array
     */
    protected static function getMinAmount (array $_params = null, array $_data = []) {

        return 50;

    }




    /**
     * getCheckoutFormControls
     *
     * @param array $_params
     * @param mixed $_data
     * @return array
     */
    protected static function getCheckoutFormControls (array $_params, $_data = []) {

        static $_fields = array (

            [
                'name' => 'first_name*',
                'type' => 'name', 
                'label' => 'First Name', 
                'hint' => 'Please enter your first name.'
            ],
            [
                'name' => 'last_name*',
                'type' => 'name', 
                'label' => 'Last Name', 
                'hint' => 'Please enter your last name.'
            ],
            [
                'name' => 'acc_type*',
                'type' => 'num', 
                'label' => 'Account Type', 
                'hint' => 'Please select your account type.',
                'control' => 'select', 
                'options' => [
                    [ 'label' => 'Savings Account', 'value' => 0 ], 
                    [ 'label' => 'Current Account', 'value' => 1 ]
                ]
            ],
            [
                'name' => 'address-1*',
                'type' => 'address', 
                'label' => 'Address 1', 
                'hint' => 'Please enter your address (eg. apartment number).'
            ],
            [
                'name' => 'address-2*',
                'type' => 'address', 
                'label' => 'Address 2', 
                'hint' => 'Please enter your street address (optional).'
            ],
            [
                'name' => 'city*',
                'type' => 'name', 
                'label' => 'City', 
                'hint' => 'Please provide the name of your city.'
            ],
            [
                'name' => 'province*',
                'type' => 'name', 
                'label' => 'State', 
                'hint' => 'Please provide the name of your state.'
            ],

            [
                'name' => 'country*',
                'type' => 'name', 
                'label' => 'Country', 
                'hint' => 'Please provide the name of country your card was issued in.'
            ],
            [
                'name' => 'postal-code',
                'type' => 'alphanum', 
                'label' => 'Postal Code', 
                'hint' => 'Please enter your postal code.'
            ]
        );


        return $_fields;


    }



    /**
     * getTransactionRef
     *
     * @param array $_params
     * @param array $_data
     * @return void
     */
    private static function getTransactionRef (array $_params, array $_data) {


            // generate transaction reference..
            $_tranx_ref = date('y').\Cataleya\Helper::uuid(16);

            return $_tranx_ref;

            
    }








    /**
     * startTransaction
     *
     * @param array $_params
     * @param mixed $_data
     * @return void
     */
    protected static function startTransaction (array $_params, $_data) {


            // Mode

            if (trim(strtolower(self::getConfig()->getParam('MODE'))) === 'live') {

                $_mode = 'live';

                $_merchant_id = self::getConfig()->getParam('MERCHANT_ID');

                $_tranx_url = self::getConfig()->getParam('TRANX_ENDPOINT');
                $_tranx_status_url = self::getConfig()->getParam('TRANX_STATUS_ENDPOINT');


            } else {

                $_mode = 'test';

                $_merchant_id = 1;

                $_tranx_url = self::getConfig()->getParam('SANDBOX_TRANX_ENDPOINT');
                $_tranx_status_url = self::getConfig()->getParam('SANDBOX_TRANX_STATUS_ENDPOINT');

            }




            // Load order...
            $_Order = \Cataleya\Store\Order::load($_params['order_id']);
            $_description = \Cataleya\Helper::countInEnglish($_Order->getQuantity(), 'item', 'items') 
                            . ' (delivery charges included) from ' 
                            . __('shop')->getName();
            
            $_Customer = $_Order->getCustomer(); 
            
            $NVP_Array = array (
                'mercId' => $_merchant_id, 
                'orderId' => $_Order->getID(),
                'amt' => $_Order->getTotalCost(TRUE), 
                'currCode' => '566', 
                'email'  => $_Customer->getEmailAddress(), 
                'prod' => $_description
            );
            
            
            
            
            // Update transaction info
            // Log transaction query
            DiamondPay\Log::insert([
                'transaction_ref' => $_params['transaction_ref'],  
                'order_id' => $_Order->getID(), 
                'order_description' => $_description,
                'customer_id' => $_Customer->getID(),
                'customer_email' => $_Customer->getEmailAddress(),  
                'customer_name' => $_Customer->getName(),  
                'payment_gateway' => '', 
                'status' => 'pending',
                'status_code' => '02',  
                'response_code' => '', 
                'response_description' => '', 
                'amount' => $NVP_Array['amt'], 
                'payment_ref' => '', 
                'merchant_id' => $NVP_Array['mercId'], 
                'currency_code' => $NVP_Array['currCode']

                ]);

            // Redirect shopper to Interswitch...
            self::redirect($NVP_Array);
            
    }



    
    /**
     * redirect
     *
     * @param array $NVP_Array
     * @return void
     */
    private static function redirect(array $NVP_Array) 
    {
        // Send auto-submit form to shopper's browser...
        $NVP_Form_Fields = \Cataleya\Helper\NVPParser::toHTML($NVP_Array);
        
        echo '<body onload="document.upay_form.submit()">';
        echo '<form id="upay_form" name="upay_form" method="POST" action="'.self::getConfig()->getParam('TRANX_URL').'" target="_self">';
        echo $NVP_Form_Fields;
        echo '</form></body>';
        
        exit(1);
    }








    /**
     * getTransactionDetails
     *
     * @return void
     */
    protected function getTransactionDetails($_tranx_ref) 
    {

        
        // Mode

        if (trim(strtolower(self::getConfig()->getParam('MODE'))) === 'live') {

            $_merchant_id = self::getConfig()->getParam('MERCHANT_ID');
            $_tranx_status_url = self::getConfig()->getParam('TRANX_STATUS_ENDPOINT');


        } else {

            $_merchant_id = 1;
            $_tranx_status_url = self::getConfig()->getParam('SANDBOX_TRANX_STATUS_ENDPOINT');

        }

        $_tranx = CashEnvoy\Log::fetch($_tranx_ref);
        if (empty($_tranx)) 
        {

            // Tranx record not found...
            return null;

        } else $_tranx = $_tranx[0];

        $NVP_Array = array (
            'ORDER_ID'    => $_tranx['order_id'], 
            'MERCHANT_ID' => $_merchant_id
        );
        
        
        
        $NVP_String = $_tranx_status_url . '?' . \Cataleya\Helper\NVPParser::toString($NVP_Array);
        $result = \Cataleya\Helper\NVPParser::curlRequest($NVP_String, false);
        
        return new \SimpleXMLElement($result);
        
    }



    



    /**
     * completeTransaction
     *
     * @param array $_params
     * @param mixed $_data
     * @return void
     */
    public function getTransactionStatus(array $_params, $_data)
    {

        $_response = [
            'transaction_ref' => $_params['transaction_ref'],  
            'transaction_amount' => '',  

            // This is specific to Cataleya!! 
            // Can only be: pending | paid | failed | cancelled | error | flagged
            'status' => 'pending',

            // Response from payment processor.
            // [response_description] is especially useful for displaying gateway specific 
            // response to shopper.
            'response_code' => '', 
            'response_description' => ''
        ]; 

        
        $_tranx = CashEnvoy\Log::fetch($_params['transaction_ref']);
        if (empty($_tranx)) 
        {

            // Tranx record not found...
            return $_data;

        } else $_tranx = $_tranx[0];


        $resultXML = self::getTransactionDetails($_params['transaction_ref']);

        

        // Validate response
        if ((int)$resultXML->OrderID !== (int)$_params['order_id']) return FALSE;

        $_Order = \Cataleya\Store\Order::load($_params['order_id']);


        // Log transaction response
        // Update transaction info
        DiamondPay\Log::update(
            $resultXML->TransactionRef, 
            [
                'transaction_ref' => $resultXML->TransactionRef, 
                'payment_gateway' => $resultXML->PaymentGateway, 
                'status' => $resultXML->Status,
                'status_code' => $resultXML->StatusCode,  
                'response_code' =>$resultXML->ResponseCode, 
                'response_description' => $resultXML->ResponseDescription, 
                'transaction_date' => $resultXML->Date, 
                'approved_amount' => $resultXML->Amount, 
                'payment_ref' => $resultXML->PaymentRef 
            ]);

        


        // Update status array
        $_response['amount'] = $resultXML->Amount;
        $_response['response_code'] = $resultXML->ResponseCode;
        $_response['response_description'] = $resultXML->ResponseDescription;


        // If successful...
        switch ($resultXML->StatusCode) {

        case '00': 
            $_response['status'] = 'paid';
            break;

        case '03': 
            $_response['status'] = 'cancelled';
            break;

        default:
            $_response['status'] = 'error';
            break;
        
        }
        
        
        return $_response;
        
    }  
        








    /**
     * getTransactions
     *
     * @param array $_params
     * @param array $_data
     * @return array
     */
    private static function getTransactions (array $_params, array $_data) {

        $_query = DiamondPay\Log::fetch($_params['order_id'], 'order_id');
        $_reciepts = [];

        foreach ($_query as $_t) {

            $_reciepts[] = [
                'app_handle' => self::getHandle(), 
                'transaction_ref' => $_t['transaction_ref'],  
                'transaction_amount' => $_t['approved_amount'],  

                // This is specific to Cataleya!! 
                // Can only be: pending | paid | failed | cancelled | error | flagged
                'status' => $_t['status'],

                // Payment Processor
                'payment_processor' => 'Diamond Pay', 

                // Response from payment processor.
                // [response_description] is especially useful for displaying gateway specific 
                // response to shopper.
                'response_code' => $_t['response_code'], 
                'response_description' => $_t['response_description'], 
                'datetime' => $_t['date_added']

            ]; 

        }


        return array_merge($_data, $_reciepts);

    }
        

    

    /**
     * getPaymentInstructions
     *
     * @param array $_params
     * @param array $_data
     * @return void
     */
    private static function getPaymentInstructions(array $_params, array $_data)
    {

        return $_data;

        
        
    }  
    
    






 

}
