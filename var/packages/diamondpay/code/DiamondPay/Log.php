<?php

namespace Cataleya\Apps\DiamondPay;
use \Cataleya\Apps\DiamondPay;
use \Cataleya\Helper\DBH;
use \Cataleya\Error;


class Log     
{
	

        static protected $_collection = array ();

        static protected $_page_offset = 0;
        static protected $_prev_page_offset = 0;
        

        
        const ORDER_ASC = 1;
        const ORDER_DESC = 2; 

        const ORDER_BY_SORT = 3;
        const ORDER_BY_CREATED = 4;
        const ORDER_BY_MODIFIED = 5;
        const ORDER_BY_NAME = 16;
        const ORDER_BY_AMOUNT = 19;
        const ORDER_BY_ID = 20;

        
	


	/**
	 * __construct
	 *
	 * @return void
	 */
	public function __construct ()
	{
		
		
	}
	




    public function __destruct()
    {
		//$this->saveData();
    }
	





    /**
     * 
     * @param array $_params
     * 
     * @return void
     * 
     */
    static public function insert (array $_params = array ()) 
    {


        if (empty($_params)) { return; }

        $_insert_params = array();
        $_placeholders = array();

        foreach (array_keys($_params) as $key) { $_placeholders[$key] = ':' . $key; }



        // Do prepared statement for 'INSERT'...
        $_qstr = '
                    INSERT INTO '.DiamondPay\Main::getLogTable().' 
                    (' . implode (', ', array_keys($_placeholders)) . ', date_added)     
                    VALUES (' . implode (', ', $_placeholders) . ', NOW())';
        
        $insert_handle = DBH::getInstance()->prepare($_qstr);

        foreach ($_params as $key=>$val) {

                $_insert_params[$key] = $_params[$key];                        
                $insert_handle->bindParam(':'.$key, $_insert_params[$key], DBH::getTypeConst($_insert_params[$key]));

        }


        if (!$insert_handle->execute()) throw new \Cataleya\Error (
            implode(', ', $insert_handle->errorInfo()),
            $insert_handle->errorCode());



    }


        
        
        
        
        

	
	/**
	 * fetch
	 *
	 * @param mixed $_id
	 * @param array $_params
	 * @return void
	 */
	public static function fetch ($_id, $_type='transaction_ref') 
	{
		

		$_col = in_array($_type, ['order_id', 'transaction_ref']) ? $_type : 'transaction_ref';
	
		// Do prepared statement...
		$select_handle = DBH::getInstance()->prepare(
                                                        'SELECT * FROM ' . DiamondPay\Main::getLogTable() .  
                                                        ' WHERE ' . $_col . ' = :instance_id' 
                                                        );
                
		$select_handle->bindParam(':instance_id', $_id, \PDO::PARAM_STR);
                

		
        if (!$select_handle->execute()) throw new Error (
            implode(', ', $select_handle->errorInfo()), 
            $select_handle->errorCode()
        );

        $_rows = [];
        while ($row = $select_handle->fetch(\PDO::FETCH_ASSOC)) $_rows[] = $row;
        

        return $_rows;
		
	}
	
	
    
    
    
       

	 
 
	/**
	 * delete
	 *
	 * @param mixed $_id
	 * @return void
	 */
	public static function delete ($_id) 
	{
		
            
            static $delete, $_param_id;
            
            if ($delete) {
                // DELETE 
                $delete = DBH::getInstance()->prepare('DELETE FROM '.DiamondPay\Main::getLogTable().' WHERE id = :instance_id');
                $delete->bindParam(':instance_id', $_param_id, \PDO::PARAM_STR);
            }
                
            $_param_id = $_id;

            if (!$delete->execute()) throw new \Cataleya\Error (
                implode(', ', $delete->errorInfo()), 
                $delete->getCode()); 

            return TRUE;
		
		
	}


        
        
	
	
	/**
	 * update
	 *
	 * @param mixed $_id
	 * @param array $_params
	 * @return void
	 */
	public static function update ($_id, array $_params) 
	{
		

		if (!is_string($_id) || empty($_params)) return;
		$_update_params = array();
		$key_val_pairs = array();
                $_keys = array_keys($_params);
		
		foreach ($_keys as $key) $key_val_pairs[$key] = $key . ' = :' . $key;
		
	
		// Do prepared statement for 'INSERT'...
		$update_handle = DBH::getInstance()->prepare(
                                                        'UPDATE ' . DiamondPay\Main::getLogTable() .  
                                                        ' SET ' . implode (', ', $key_val_pairs) .     
                                                        ' WHERE transaction_ref = :instance_id' 
                                                        );
                
		$update_handle->bindParam(':instance_id', $_id, \PDO::PARAM_STR);
                
		foreach ($_keys as $key) {
                    
            $_update_params[$key] = $_params[$key];                        
			$update_handle->bindParam(':'.$key, $_update_params[$key], DBH::getTypeConst($_update_params[$key]));

		}

		
        if (!$update_handle->execute()) throw new \Cataleya\Error (
            implode(', ', $update_handle->errorInfo()), 
            $update_handle->errorCode()
        );
		
	}
	
	
       
 
 
        
        


    public static function getPageOffset() {
        return self::$_page_offset;
    }

    
        

        
        
        



        





}
	
	
	
	
