<?php

namespace Cataleya\Apps\CashEnvoy;
use Cataleya\Apps\CashEnvoy;

/**
 * 
 *
 * @package CashEnvoy
 * @author Gboyega Dada <gboyega@fpplabs.com>
 * @version 0.0.1
 */
class Main implements \Cataleya\Plugins\Controller {


    protected static $_plugin_handle = 'cashenvoy';
    private static $_log_table = 'app_cashenvoy_log';
    protected static $_Config = null;

    public static $_actions = [

        'get-options' => 'getPaymentOptions', 
        'get-currency-code' => 'getCurrencyCode', 
        'get-max-amount' => 'getMaxAmount', 
        'get-min-amount' => 'getMinAmount', 

        'get-config-form-controls' => 'getConfigFormControls', 
        'update-config' => 'saveConfig', 
        'get-form-controls' => 'getCheckoutFormControls', 
        'get-transaction-reference' => 'getTransactionRef', 
        'start-transaction' => 'startTransaction', 
        'complete-transaction' => 'getTransactionStatus', 
        'get-transaction-status' => 'getTransactionStatus', 

        // Returns transactions made on specified order_id
        'get-transactions' => 'getTransactions', 
        'get-instructions' => 'getPaymentInstructions'

    ];


    private static $_status_codes = [
            'C00' => 'Successful', 
            'C01' => 'Cancelled', 
            'C02' => 'Cancelled by inactivity', 
            'C03' => 'No transaction record', 
            'C04' => 'Insufficient funds',  
            'C05' => 'Failed' 

        ];




    /**
     * init
     *
     * @return void
     */
    public static function init () {

        if (self::$_initialized) { return true; }

        /* -----------------  Do init stuff ----------------- */

        self::getConfig(true);

        /* ---------------- End of init stuff --------------- */


        self::$_initialized = true;
    }




    /**
     * getActions            Get list of events/hooks that Plugin Controller will repond to
     *
     * @return array         An rray of |actions| (strings), that is, a list of events/hooks our plugin 
     *                       will respond to. Eg. 'payments.start-transaction' and so on.
     *
     */
    public static function getActions () {

        return array_keys(self::$_actions);

    }



    /**
     * getHandle
     *
     * @return string
     */
    public static function getHandle() {

        return self::$_plugin_handle;
    }


    /**
     * getKey   Alias for ::getHandle()
     *
     * @return string
     */
    public static function getKey() {

        return self::getHandle();
    }




    /**
     * getConfig
     *
     * @return void
     */
    public static function getConfig ($_reload=false) {
        
        if (empty(self::$_Config) || $_reload===true) self::$_Config = \__('this.config');
        return self::$_Config;

    }



    /**
     * getLogTable
     *
     * @return string
     */
    public static function getLogTable() {
        return self::$_log_table;

    }



    /**
     * getStatusCodes
     *
     * @return array
     */
    public static function getStatusCodes () {
        return self::$_status_codes;
    }



    /**
     * getStatusDescription
     *
     * @param string $_code     Code is numeric but should supplied like this: '00', '08'...
     *                          (that is -- zero padded quoted string).
     * @return string
     */
    public static function getStatusDescription ($_code) {

        return (isset(self::$_status_codes[$_code])) 
            ? self::$_status_codes[$_code] 
            : 'Unknown'; 

    }




    /**
     * run    This is like the regular 'exec' method that a controller has; 
     *        basically functioning as a gateway.
     *
     * @param string $_action 
     * @param array $_params
     * @param mixed $_data
     * @return mixed
     */
    public static function run ($_action, array $_params, $_data = null) {
        
        if (!is_string($_action) || !isset(self::$_actions[$_action])) { 
            
            throw new \Cataleya\Error ("Invalid plugin action: [" . $_action . "] in Cash Envoy App.");

        }
        
       return call_user_func_array([ __CLASS__, self::$_actions[$_action] ], [$_params, $_data]);
                   
    }
    


    
    /**
     * run    This works like '::run'. It will be called whenever an event  
     *        your app is listening to is triggered. You have to specify there using 'events' in 
     *        your app.json file just like your plugin scopes.
     *
     * @param string $_event 
     * @param array $_params
     * @return void
     */
    public static function notify ($_event, array $_params = []) {
    
        
        
    }

    






    /**
     * install
     *
     * @param array $_params
     * @param array $_data
     * @return void
     */
    public static function install () {


        // 1. Save plugin_handle ??
        $_Config = \__('this.config.init', [
            
            'MERCHANT_ID' => '1',
            'MERCHANT_KEY' => '2dawdszfcasdq24434242ffsdsfd',  

            'MODE' => 'test', 

            'TRANX_ENDPOINT' => 'https://www.cashenvoy.com/webservice/?cmd=cepay',
            'TRANX_STATUS_ENDPOINT' => 'https://www.cashenvoy.com/webservice/?cmd=requery',

            'SANDBOX_TRANX_ENDPOINT' => 'https://www.cashenvoy.com/sandbox/?cmd=cepay', 
            'SANDBOX_TRANX_STATUS_ENDPOINT' => 'https://www.cashenvoy.com/sandbox/?cmd=requery'
        
        ]);


    }


    /**
     * uninstall
     *
     * @param array $_params 
     * @param array $_data
     * @return void
     */
    public static function uninstall () {

        // 1. Remove db tables
        $_drop_handle = \Cataleya\Helper\DBH::getInstance()->prepare('DROP TABLE '.self::$_log_table);
        if (!$_drop_handle->execute()) {
            throw new \Cataleya\Error(implode(', ', $_drop_handle->errorInfo()), $_drop_handle->errorCode()); 
        }

        // 2. General cleanup
        $_Config = self::getConfig();
        if (!empty($_Config)) { $_Config->drop(); }

    }





    
    /* ----------------------- PLUGIN ACTIONS -------------------------- */


    /**
     * getConfigFormControls
     *
     * @param array $_params
     * @param array $_data
     * @return array
     */
    protected static function getConfigFormControls (array $_params, array $_data = []) {

        $_fields = array (

            [
                'name' => 'MODE',
                'type' => 'alphanum', 
                'required' => true, 
                'label' => 'Gateway Mode',
                'value' => self::getConfig()->getParam('MODE'),  
                'hint' => 'Enter "test" to use test settings - OR - "live" to use live settings.',
                'control' => 'select', 
                'options' => [
                    [ 'label' => 'Sandbox (test)', 'value' => 'test' ], 
                    [ 'label' => 'Live (real payments)', 'value' => 'live' ] 
                ]
            ],
            [
                'name' => 'MERCHANT_ID',
                'type' => 'alphanum', 
                'required' => true, 
                'label' => 'Merchant ID',
                'value' => self::getConfig()->getParam('MERCHANT_ID'),  
                'hint' => 'Please enter your Merchant ID (this should issued to you at Diamond Bank Plc.)'
            ],
            [
                'name' => 'MERCHANT_KEY',
                'type' => 'alphanum', 
                'required' => true, 
                'label' => 'Merchant Key',
                'value' => self::getConfig()->getParam('MERCHANT_KEY'),  
                'hint' => 'Login to your cashenvoy account, your merchant key is displayed on the dashboard page.'
            ]
        );


        return array_merge($_data, $_fields);


    }

    


    /**
     * saveConfig
     *
     * @param array $_params
     * @param array $_data
     * @return void
     */
    public static function saveConfig (array $_params, array $_data = []) 
    {



    }








    /**
     * getPaymentOptions
     *
     * @param array $_params
     * @param array $_data
     * @return array
     */
    protected static function getPaymentOptions (array $_params, array $_data = []) {

        return (
                isset($_params['currency_codes']) && 
                !in_array(self::getCurrencyCode(), $_params['currency_codes'])
                ) 
                
                ? $_data 

                : array_merge($_data, array (
                    [
                        'name' => 'Cash Envoy',
                        'id' => self::getHandle(), 
                        'currency_code' => self::getCurrencyCode(), 
                        'max_amount' => self::getMaxAmount(), 
                        'min_amount' => self::getMinAmount()
                    ]


                ));


    }




    /**
     * getCurrencyCode
     *
     * @param array $_params
     * @param array $_data
     * @return array
     */
    protected static function getCurrencyCode (array $_params = null, array $_data = []) {

        return 'NGN';

    }



    /**
     * getMaxAmount
     *
     * @param array $_params
     * @param array $_data
     * @return array
     */
    protected static function getMaxAmount (array $_params = null, array $_data = []) {

        return 200000;

    }




    /**
     * getMinAmount
     *
     * @param array $_params
     * @param array $_data
     * @return array
     */
    protected static function getMinAmount (array $_params = null, array $_data = []) {

        return 50;

    }






    /**
     * getCheckoutFormControls
     *
     * @param array $_params
     * @param array $_data
     * @return array
     */
    protected static function getCheckoutFormControls (array $_params, array $_data = []) {

        static $_fields = [];


        return array_merge($_data, $_fields);


    }






    /**
     * getTransactionRef
     *
     * @param array $_params
     * @param array $_data
     * @return void
     */
    private static function getTransactionRef (array $_params, array $_data) {



            // generate transaction reference..
            $_mt = explode('.', number_format(microtime(true),4));
            $_tranx_ref = $_num_code = strval(date('dmy')) . 
                    str_pad(strval($_params['order_id']), 2, '0', STR_PAD_LEFT) . 
                    $_mt[1];

            return $_tranx_ref;

            
    }





    /**
     * startTransaction
     *
     * @param array $_params
     * @param array $_data
     * @return void
     */
    private static function startTransaction (array $_params, array $_data) {

            // Mode

            if (trim(strtolower(self::getConfig()->getParam('MODE'))) === 'live') {

                $_mode = 'live';

                $_merchant_id = self::getConfig()->getParam('MERCHANT_ID');
                $_merchant_key = self::getConfig()->getParam('MERCHANT_KEY');

                $_tranx_url = self::getConfig()->getParam('TRANX_ENDPOINT');
                $_tranx_status_url = self::getConfig()->getParam('TRANX_STATUS_ENDPOINT');


            } else {

                $_mode = 'test';

                $_merchant_id = self::getConfig()->getParam('MERCHANT_ID');
                $_merchant_key = self::getConfig()->getParam('MERCHANT_KEY');

                $_tranx_url = self::getConfig()->getParam('SANDBOX_TRANX_ENDPOINT');
                $_tranx_status_url = self::getConfig()->getParam('SANDBOX_TRANX_STATUS_ENDPOINT');

            }

            // Load order...
            $_Order = \Cataleya\Store\Order::load($_params['order_id']);
            $_description = \Cataleya\Helper::countInEnglish($_Order->getQuantity(), 'item', 'items') 
                            . ' (delivery charges included) from ' 
                            . \__('shop')->getName();
            
            $_Customer = $_Order->getCustomer(); 


            // generate transaction reference..
            $_tranx_ref = $_params['transaction_ref'];
            $_amount = $_Order->getGrandTotal(TRUE);


            // generate request signature
            $data = $_merchant_key.$_tranx_ref.$_amount;
            $_hash = hash_hmac('sha256', $data, $_merchant_key, false);

            $NVP_Array = array (
                'ce_merchantid' => $_merchant_id, 
                'ce_transref' => $_tranx_ref,
                'ce_amount' => $_amount, 
                'ce_customerid' => $_Customer->getID(),  
                'ce_notifyurl'  => \__url('checkout.complete'), 
                'ce_memo' => $_description,
                'ce_window' => 'parent',
                'ce_signature' => $_hash
            );
            
            
            
            
            // Log transaction
            CashEnvoy\Log::insert([
                'transaction_ref' => $_tranx_ref, 
                'order_id' => $_Order->getID(), 
                'order_description' => $_description,
                'customer_id' => $_Customer->getID(),
                'customer_email' => $_Customer->getEmailAddress(),  
                'customer_name' => $_Customer->getName(),  
                'status' => 'pending',
                'status_code' => '02',  
                'response_code' => '', 
                'response_description' => '', 
                'amount' => $_amount, 
                'transaction_date' => date(''), 
                'currency_code' => 'NGN', 
                'gateway_mode' => $_mode

                ]);

            // Redirect shopper to Interswitch...
            self::redirect($NVP_Array);
            
    }



    
    /**
     * redirect
     *
     * @param array $NVP_Array
     * @return void
     */
    private static function redirect(array $NVP_Array) 
    {
        if (trim(strtolower(self::getConfig()->getParam('MODE'))) === 'live') {

            $_tranx_url = self::getConfig()->getParam('TRANX_ENDPOINT');

        } else {

            $_tranx_url = self::getConfig()->getParam('SANDBOX_TRANX_ENDPOINT');
        }
        
        // Send auto-submit form to shopper's browser...
        $NVP_Form_Fields = \Cataleya\Helper\NVPParser::toHTML($NVP_Array);
        
        echo '<body onload="document.submit2cepay_form.submit()">';
        echo '<form id="submit2cepay_form" name="submit2cepay_form" method="POST" action="'.$_tranx_url.'" target="_self">';
        echo $NVP_Form_Fields;
        echo '</form></body>';
        
        exit(1);
    }








    /**
     * getTransactionDetails
     *
     * @return void
     */
    private static function getTransactionDetails($_tranx_ref) 
    {


        // Mode

        if (trim(strtolower(self::getConfig()->getParam('MODE'))) === 'live') {

            $_merchant_id = self::getConfig()->getParam('MERCHANT_ID');
            $_merchant_key = self::getConfig()->getParam('MERCHANT_KEY');

            $_tranx_url = self::getConfig()->getParam('TRANX_ENDPOINT');
            $_tranx_status_url = self::getConfig()->getParam('TRANX_STATUS_ENDPOINT');


        } else {

            $_merchant_id = self::getConfig()->getParam('MERCHANT_ID');
            $_merchant_key = self::getConfig()->getParam('MERCHANT_KEY');
            
            $_tranx_url = self::getConfig()->getParam('SANDBOX_TRANX_ENDPOINT');
            $_tranx_status_url = self::getConfig()->getParam('SANDBOX_TRANX_STATUS_ENDPOINT');

        }
        
        $cdata = $_merchant_key.$_tranx_ref.$_merchant_id;
        $sign = hash_hmac('sha256', $cdata, $_merchant_key, false);


        $request =  'mertid='.$_merchant_id.
                    '&transref='.$_tranx_ref.
                    '&respformat=json'.
                    '&signature='.$sign; //initialize the request variables
        
        $ch = curl_init(); //initialize curl handle
        curl_setopt($ch, CURLOPT_URL, $_tranx_status_url); //set the url
        curl_setopt($ch, CURLOPT_RETURNTRANSFER,true); //return as a variable
        curl_setopt($ch, CURLOPT_POST, 1); //set POST method
        curl_setopt($ch, CURLOPT_POSTFIELDS, $request); //set the POST variables
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        $response = curl_exec($ch); // grab URL and pass it to the browser. Run the whole process and return the response
        curl_close($ch); //close the curl handle
        return json_decode($response);   



        /*
        $NVP_Array = array (
            'ORDER_ID'    => $_order_id, 
            'MERCHANT_ID' => $_merchant_id
        );
        
        
        
        $NVP_String = $_tranx_status_url . '?' . \Cataleya\Helper\NVPParser::toString($NVP_Array);
        $result = \Cataleya\Helper\NVPParser::curlRequest($NVP_String, false);
        
        return json_decode($result);
         */
    }



    



    /**
     * completeTransaction
     * 
     * {"TransactionId":"15NOmI4FRNnpQzLVqq","TransactionStatus":"C00","TransactionAmount":"1800.00"}
     *
     * @param array $_params
     * @param array $_data
     * @return void
     */
    private static function getTransactionStatus(array $_params, array $_data)
    {
        

        $_response = [
            'transaction_ref' => $_params['transaction_ref'],  
            'transaction_amount' => '',  

            // This is specific to Cataleya!! 
            // Can only be: pending | paid | failed | cancelled | error | flagged
            'status' => 'pending',

            // Response from payment processor.
            // [response_description] is especially useful for displaying gateway specific 
            // response to shopper.
            'response_code' => '', 
            'response_description' => '',  
            'html' => ''
        ]; 


        
        $_tranx = CashEnvoy\Log::fetch($_params['transaction_ref']);
        if (empty($_tranx)) 
        {

            // Tranx record not found...
            return $_data;

        } else $_tranx = $_tranx[0];


        $result = self::getTransactionDetails($_params['transaction_ref']);



        // Transaction Error ??
        if ($result->TransactionStatus !== '') {


            // Validate response
            if (
                $result->TransactionId !== $_tranx['transaction_ref'] || 
                (float)$result->TransactionAmount !== (float)$_tranx['amount']   
            ) {
                $_response['status'] = 'flagged';
                
                return $_response;
            }


            // Update status array
            $_response['amount'] = $_tranx['amount'];
            $_response['response_code'] = $result->TransactionStatus;
            $_response['response_description'] = $result->TransactionStatus;



            // If successful...
            switch ($result->TransactionStatus) {

            case 'C00': 
                $_response['status'] = 'paid';
                break;

            case 'C01': 
                $_response['status'] = 'cancelled';
                break;

            case 'C02': 
                $_response['status'] = 'cancelled'; // by inactivity
                break;

            case 'C03': 
                $_response['status'] = 'error';
                break;

            case 'C04': 
                $_response['status'] = 'failed';
                break;

            case 'C05': 
                $_response['status'] = 'failed';
                break;

            default:
                $_response['status'] = 'error';
                break;
            
            }
            
            
            // Update transaction log
            CashEnvoy\Log::update(
                $result->TransactionId, 
                [
                    'status' => $_response['status'],
                    'status_code' => $result->TransactionStatus,  
                    'response_code' =>$result->TransactionStatus, 
                    'response_description' => self::getStatusDescription($result->TransactionStatus), 
                    'approved_amount' => $result->TransactionAmount 
                ]);


        }
        
        
        // HTML Response...


        $_skin = \__('this.skin');
        $_success = ($_tranx['status'] === 'paid') ? true : false;
        $_temp_vars = [

            'transaction' => [
                'success' => $_success, 
                'reference' => $_tranx['transaction_ref'], 
                'status' => $_response['status'],  
                'response_code' =>$result->TransactionStatus, 
                'response_description' => self::getStatusDescription($result->TransactionStatus), 
                'approved_amount' => $result->TransactionAmount 
             ], 
            'logo' => $_skin->getAssetsPath().'/images/ce_logo.png'

        ];


        $_response['html'] = $_skin->render('response.html.twig', $_temp_vars);
        
        return $_response;

        
        
    }  



    /**
     * getTransactions
     *
     * @param array $_params
     * @param array $_data
     * @return array
     */
    private static function getTransactions (array $_params, array $_data) {

        $_query = CashEnvoy\Log::fetch($_params['order_id'], 'order_id');
        $_reciepts = [];

        foreach ($_query as $_t) {

            $_reciepts[] = [
                'app_handle' => self::getHandle(), 
                'transaction_ref' => $_t['transaction_ref'],  
                'amount' => $_t['amount'],  

                // This is specific to Cataleya!! 
                // Can only be: pending | paid | failed | cancelled | error | flagged
                'status' => $_t['status'],

                // Payment Processor
                'payment_processor' => 'Cash Envoy', 

                // Response from payment processor.
                // [response_description] is especially useful for displaying gateway specific 
                // response to shopper.
                'response_code' => $_t['response_code'], 
                'response_description' => $_t['response_description'], 
                'datetime' => $_t['date_added']

            ]; 

        }


        return array_merge($_data, $_reciepts);

    }
        

    
    
    

    /**
     * getPaymentInstructions
     *
     * @param array $_params
     * @param array $_data
     * @return void
     */
    private static function getPaymentInstructions(array $_params, array $_data)
    {

        return $_data;

        
        
    }  
    
    
    
    
    
    
 

}
