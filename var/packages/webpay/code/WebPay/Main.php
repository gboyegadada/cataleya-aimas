<?php

namespace Cataleya\Apps\WebPay;
use Cataleya\Apps\WebPay;

/**
 * 
 *
 * @package WebPay
 * @author Gboyega Dada <gboyega@fpplabs.com>
 * @version 0.0.1
 */
class Main implements \Cataleya\Plugins\Controller {


    protected static $_plugin_handle = 'webpay';
    private static $_log_table = 'app_webpay_log';
    protected static $_Config = null;

    public static $_actions = [

        'get-options' => 'getPaymentOptions', 
        'get-currency-code' => 'getCurrencyCode', 
        'get-max-amount' => 'getMaxAmount', 
        'get-min-amount' => 'getMinAmount', 

        'get-config-form-controls' => 'getConfigFormControls', 
        'update-config' => 'saveConfig', 
        'get-form-controls' => 'getCheckoutFormControls', 
        'get-transaction-reference' => 'getTransactionRef', 
        'start-transaction' => 'startTransaction', 
        'complete-transaction' => 'getTransactionStatus', 
        'get-transaction-status' => 'getTransactionStatus', 

        // Returns transactions made on specified order_id
        'get-transactions' => 'getTransactions', 
        'get-instructions' => 'getPaymentInstructions'

    ];


    private static $_status_codes = [
        '00'=>'Approved by Financial Institution'
        ,'01'=>'Refer to Financial Institution'
        ,'02'=>'Refer to Financial Institution, Special Condition'
        ,'03'=>'Invalid Merchant'
        ,'04'=>'Pick-up card'
        ,'05'=>'Do Not Honor'
        ,'06'=>'Error'
        ,'07'=>'Pick-Up Card, Special Condition'
        ,'08'=>'Honor with Identification'
        ,'09'=>'Request in Progress'
        ,'10'=>'Approved by Financial Institution, Partial'
        ,'11'=>'Approved by Financial Institution, VIP'
        ,'12'=>'Invalid Transaction'
        ,'13'=>'Invalid Amount'
        ,'14'=>'Invalid Card Number'
        ,'15'=>'No Such Financial Institution'
        ,'16'=>'Approved by Financial Institution, Update Track 3'
        ,'17'=>'Customer Cancellation'
        ,'18'=>'Customer Dispute'
        ,'19'=>'Re-enter Transaction'
        ,'20'=>'Invalid Response from Financial Institution'
        ,'21'=>'No Action Taken by Financial Institution'
        ,'22'=>'Suspected Malfunction'
        ,'23'=>'Unacceptable Transaction Fee'
        ,'24'=>'File Update not Supported'
        ,'25'=>'Unable to Locate Record'
        ,'26'=>'Duplicate Record'
        ,'27'=>'File Update Field Edit Error'
        ,'28'=>'File Update File Locked'
        ,'29'=>'File Update Failed'
        ,'30'=>'Format Error'
        ,'31'=>'Bank Not Supported'
        ,'32'=>'Completed Partially by Financial Institution'
        ,'33'=>'Expired Card, Pick-Up'
        ,'34'=>'Suspected Fraud, Pick-Up'
        ,'35'=>'Contact Acquirer, Pick-Up'
        ,'36'=>'Restricted Card, Pick-Up'
        ,'37'=>'Call Acquirer Security, Pick-Up'
        ,'38'=>'PIN Tries Exceeded, Pick-Up'
        ,'39'=>'No Credit Account'
        ,'40'=>'Function not Supported'
        ,'41'=>'Lost Card, Pick-Up'
        ,'42'=>'No Universal Account'
        ,'43'=>'Stolen Card, Pick-Up'
        ,'44'=>'No Investment Account'
        ,'51'=>'Insufficient Funds'
        ,'52'=>'No Check Account'
        ,'53'=>'No Savings Account'
        ,'54'=>'Expired Card'
        ,'55'=>'Incorrect PIN'
        ,'56'=>'No Card Record'
        ,'57'=>'Transaction not permitted to Cardholder'
        ,'58'=>'Transaction not permitted on Terminal'
        ,'59'=>'Suspected Fraud'
        ,'60'=>'Contact Acquirer'
        ,'61'=>'Exceeds Withdrawal Limit'
        ,'62'=>'Restricted Card'
        ,'63'=>'Security Violation'
        ,'64'=>'Original Amount Incorrect'
        ,'65'=>'Exceeds withdrawal frequency'
        ,'66'=>'Call Acquirer Security'
        ,'67'=>'Hard Capture'
        ,'68'=>'Response Received Too Late'
        ,'75'=>'PIN tries exceeded'
        ,'76'=>'Reserved for Future Postilion Use'
        ,'77'=>'Intervene, Bank Approval Required'
        ,'78'=>'Intervene, Bank Approval Required for Partial Amount'
        ,'90'=>'Cut-off in Progress'
        ,'91'=>'Issuer or Switch Inoperative'
        ,'92'=>'Routing Error'
        ,'93'=>'Violation of law'
        ,'94'=>'Duplicate Transaction'
        ,'95'=>'Reconcile Error'
        ,'96'=>'System Malfunction'
        ,'98'=>'Exceeds Cash Limit'
        ,'A0'=>'Unexpected error'
        ,'A4'=>'Transaction not permitted to card holder, via channels'
        ,'Z0'=>'Transaction Status Unconfirmed'
        ,'Z1'=>'Transaction Error'
        ,'Z2'=>'Bank account error'
        ,'Z3'=>'Bank collections account error'
        ,'Z4'=>'Interface Integration Error'
        ,'Z5'=>'Duplicate Reference Error'
        ,'Z6'=>'Incomplete Transaction'
        ,'Z7'=>'Transaction Split Pre-processing Error'
        ,'Z8'=>'Invalid Card Number, via channels'
        ,'Z9'=>'Transaction not permitted to card holder, via channels'
    ];





    /**
     * init
     *
     * @return void
     */
    public static function init () {

        if (self::$_initialized) { return true; }

        /* -----------------  Do init stuff ----------------- */

        self::getConfig(true);

        /* ---------------- End of init stuff --------------- */


        self::$_initialized = true;
    }




    /**
     * getActions            Get list of events/hooks that Plugin Controller will repond to
     *
     * @return array         An rray of |actions| (strings), that is, a list of events/hooks our plugin 
     *                       will respond to. Eg. 'payments.start-transaction' and so on.
     *
     */
    public static function getActions () {

        return array_keys(self::$_actions);

    }



    /**
     * getHandle
     *
     * @return string
     */
    public static function getHandle() {

        return self::$_plugin_handle;
    }


    /**
     * getKey   Alias for ::getHandle()
     *
     * @return string
     */
    public static function getKey() {

        return self::getHandle();;
    }




    /**
     * getConfig
     *
     * @return void
     */
    public static function getConfig ($_reload=false) {
        
        if (empty(self::$_Config) || $_reload===true) self::$_Config = \__('this.config');
        return self::$_Config;

    }



    /**
     * getLogTable
     *
     * @return string
     */
    public static function getLogTable() {
        return self::$_log_table;

    }



    /**
     * getStatusCodes
     *
     * @return array
     */
    public static function getStatusCodes () {
        return self::$_status_codes;
    }



    /**
     * getStatusDescription
     *
     * @param string $_code     Code is numeric but should supplied like this: '00', '08'...
     *                          (that is -- zero padded quoted string).
     * @return string
     */
    public static function getStatusDescription ($_code) {

        return (isset(self::$_status_codes[$_code])) 
            ? self::$_status_codes[$_code] 
            : 'Unknown'; 

    }




    /**
     * run    This is like the regular 'exec' method that a controller has; 
     *        basically functioning as a gateway.
     *
     * @param string $_action 
     * @param array $_params
     * @param mixed $_data
     * @return mixed
     */
    public static function run ($_action, array $_params, $_data = null) {
        
        if (!is_string($_action) || !isset(self::$_actions[$_action])) { 
            
            throw new \Cataleya\Error ("Invalid plugin action: [" . $_action . "] in Interswitch Web Pay App.");

        }
        
       return call_user_func_array([ __CLASS__, self::$_actions[$_action] ], [$_params, $_data]);
                   
    }
    



    
    /**
     * run    This works like '::run'. It will be called whenever an event  
     *        your app is listening to is triggered. You have to specify there using 'events' in 
     *        your app.json file just like your plugin scopes.
     *
     * @param string $_event 
     * @param array $_params
     * @return void
     */
    public static function notify ($_event, array $_params = []) {
    
        
        
    }

    





    /**
     * install
     *
     * @param array $_params
     * @param array $_data
     * @return void
     */
    public static function install () {


        // 1. Save plugin_handle ??
        $_Config = \__('this.config.init', [
            
            'MERCHANT_ID' => '1',
            'MERCHANT_WEBSITE' => 'www.example.com',
            'PRODUCT_ID' => '6205',  
            'PAYMENT_ITEM_ID' => '101',  
            'MAC_KEY' => '2dawdszfcasdq24434242ffsdsfd',  

            'MODE' => 'test', 
            'CURRENCY_CODE' => '566', 

            'TRANX_ENDPOINT' => 'https://webpay.interswitchng.com/paydirect/pay',
            'TRANX_STATUS_ENDPOINT' => 'https://webpay.interswitchng.com/paydirect/api/v1/gettransaction.json',
            'WEB_SERVICES_ENDPOINT' => 'https://webpay.interswitchng.com/paydirect/services/webpayservice.svc',

            'SANDBOX_PRODUCT_ID' => '6205',  
            'SANDBOX_PAYMENT_ITEM_ID' => '101',  
            'SANDBOX_MAC_KEY' => 'D3D1D05AFE42AD50818167EAC73C109168A0F108F32645C8B59E897FA930DA44F9230910DAC9E20641823799A107A02068F7BC0F4CC41D2952E249552255710F',  
            'SANDBOX_TRANX_ENDPOINT' => 'https://stageserv.interswitchng.com/test_paydirect/pay', 
            'SANDBOX_TRANX_STATUS_ENDPOINT' => 'https://stageserv.interswitchng.com/test_paydirect/api/v1/gettransaction.json', 
            'SANDBOX_SERVICES_ENDPOINT' => 'https://stageserv.interswitchng.com/test_paydirect/services/webpayservice.svc'
        
        ]);


    }


    /**
     * uninstall
     *
     * @param array $_params 
     * @param array $_data
     * @return void
     */
    public static function uninstall () {

        // 1. Remove db tables
        $_drop_handle = \Cataleya\Helper\DBH::getInstance()->prepare('DROP TABLE '.self::$_log_table);
        if (!$_drop_handle->execute()) {
            throw new \Cataleya\Error(implode(', ', $_drop_handle->errorInfo()), $_drop_handle->errorCode()); 
        }

        // 2. General cleanup
        $_Config = self::getConfig();
        if (!empty($_Config)) { $_Config->drop(); }

    }





    
    /* ----------------------- PLUGIN ACTIONS -------------------------- */


    /**
     * getConfigFormControls
     *
     * @param array $_params
     * @param array $_data
     * @return array
     */
    protected static function getConfigFormControls (array $_params, array $_data = []) {

        $_fields = array (

            [
                'name' => 'MODE',
                'type' => 'alphanum', 
                'required' => true, 
                'label' => 'Gateway Mode',
                'value' => self::getConfig()->getParam('MODE'),  
                'hint' => 'Enter "test" to use test settings - OR - "live" to use live settings.',
                'control' => 'select', 
                'options' => [
                    [ 'label' => 'Sandbox (test)', 'value' => 'test' ], 
                    [ 'label' => 'Live (real payments)', 'value' => 'live' ] 
                ]
            ],
            [
                'name' => 'MERCHANT_ID',
                'type' => 'alphanum', 
                'required' => true, 
                'label' => 'Merchant ID',
                'value' => self::getConfig()->getParam('MERCHANT_ID'),  
                'hint' => 'Please enter your Merchant ID (as provided by Interswitch.)'
            ],
            [
                'name' => 'MERCHANT_WEBSITE',
                'type' => 'url', 
                'required' => true, 
                'label' => 'Merchant Website (this website)',
                'value' => self::getConfig()->getParam('MERCHANT_WEBSITE'),  
                'hint' => 'The name of this website.'
            ],
            [
                'name' => 'PRODUCT_ID',
                'type' => 'num', 
                'required' => true, 
                'label' => 'Product ID',
                'value' => self::getConfig()->getParam('PRODUCT_ID'),  
                'hint' => 'Please enter your PRODUCT ID (as provided by Interswitch.)'
            ],
            [
                'name' => 'PAYMENT_ITEM_ID',
                'type' => 'num', 
                'required' => true, 
                'label' => 'Payment Item ID',
                'value' => self::getConfig()->getParam('PAYMENT_ITEM_ID'),  
                'hint' => 'Please enter your Payment Item ID (as provided by Interswitch.)'
            ],
            [
                'name' => 'MAC_KEY',
                'type' => 'alphanum', 
                'required' => true, 
                'label' => 'MAC Key',
                'value' => self::getConfig()->getParam('MAC_KEY'),  
                'hint' => 'This is the MAC key given to you by Interswitch.'
            ]
        );


        return array_merge($_data, $_fields);


    }

    


    /**
     * saveConfig
     *
     * @param array $_params
     * @param array $_data
     * @return void
     */
    public static function saveConfig (array $_params, array $_data = []) 
    {



    }








    /**
     * getPaymentOptions
     *
     * @param array $_params
     * @param array $_data
     * @return array
     */
    protected static function getPaymentOptions (array $_params, array $_data = []) {


        return (
                isset($_params['currency_codes']) && 
                !in_array(self::getCurrencyCode(), $_params['currency_codes'])
                ) 
                
                ? $_data 

                : array_merge($_data, array (

                    [
                        'name' => 'Interswitch Web Pay',
                        'id' => self::getHandle(), 
                        'currency_code' => self::getCurrencyCode(), 
                        'max_amount' => self::getMaxAmount(), 
                        'min_amount' => self::getMinAmount()
                    ]


                ));


    }



    /**
     * getCurrencyCode
     *
     * @param array $_params
     * @param array $_data
     * @return array
     */
    protected static function getCurrencyCode (array $_params = null, array $_data = []) {

        return 'NGN';

    }



    /**
     * getMaxAmount
     *
     * @param array $_params
     * @param array $_data
     * @return array
     */
    protected static function getMaxAmount (array $_params = null, array $_data = []) {

        return 200000;

    }




    /**
     * getMinAmount
     *
     * @param array $_params
     * @param array $_data
     * @return array
     */
    protected static function getMinAmount (array $_params = null, array $_data = []) {

        return 50;

    }







    /**
     * getCheckoutFormControls
     *
     * @param array $_params
     * @param array $_data
     * @return array
     */
    protected static function getCheckoutFormControls (array $_params, array $_data = []) {

        static $_fields = [];


        return array_merge($_data, $_fields);


    }






    /**
     * getTransactionRef
     *
     * @param array $_params
     * @param array $_data
     * @return void
     */
    private static function getTransactionRef (array $_params, array $_data) {


            // generate transaction reference..
            $_mt = explode('.', number_format(microtime(true),4));
            $_tranx_ref = $_num_code = strval(date('dmy')) . 
                    str_pad(strval($_params['order_id']), 2, '0', STR_PAD_LEFT) . 
                    $_mt[1];

            return $_tranx_ref;

            
    }





    /**
     * startTransaction
     *
     * @param array $_params
     * @param array $_data
     * @return void
     */
    private static function startTransaction (array $_params, array $_data) {

            // Mode

            if (trim(strtolower(self::getConfig()->getParam('MODE'))) === 'live') {

                $_mode = 'live';

                $_merchant_id = self::getConfig()->getParam('MERCHANT_ID');
                $_merchant_key = self::getConfig()->getParam('MAC_KEY');
                $_webpay_product_id = self::getConfig()->getParam('PRODUCT_ID');
                $_webpay_pay_item_id = self::getConfig()->getParam('PAYMENT_ITEM_ID');

                $_tranx_url = self::getConfig()->getParam('TRANX_ENDPOINT');
                $_tranx_status_url = self::getConfig()->getParam('TRANX_STATUS_ENDPOINT');
                $_return_url = \__url('checkout.complete');


            } else {

                $_mode = 'test';

                $_merchant_id = self::getConfig()->getParam('MERCHANT_ID');
                $_merchant_key = self::getConfig()->getParam('SANDBOX_MAC_KEY');
                $_webpay_product_id = self::getConfig()->getParam('SANDBOX_PRODUCT_ID');
                $_webpay_pay_item_id = self::getConfig()->getParam('SANDBOX_PAYMENT_ITEM_ID');

                $_tranx_url = self::getConfig()->getParam('SANDBOX_TRANX_ENDPOINT');
                $_tranx_status_url = self::getConfig()->getParam('SANDBOX_TRANX_STATUS_ENDPOINT');
                $_return_url = \__url('checkout.complete');


            }

            // Load order...
            $_Order = \__('order', $_params['order_id']);
            $_description = \Cataleya\Helper::countInEnglish($_Order->getQuantity(), 'item', 'items') 
                            . ' (delivery charges included) from ' 
                            . \__('shop')->getName();
            
            $_Customer = $_Order->getCustomer(); 


            // generate transaction reference..
            $_tranx_ref = $_params['transaction_ref'];
            $_amount = $_Order->getGrandTotal(true, true); // TODO: Go to Order class & make this return small amounts!!


            // generate request signature
            $_hash_array = array (
                // txn_ref
                $_tranx_ref, 
                
                // product_id
                $_webpay_product_id, 
                
                // pay_item_id
                $_webpay_pay_item_id, 
                
                // amount
                $_amount, 
                
                // site_redirect_url
                $_return_url, 
                
                // <mackey>
                $_merchant_key
                
                
            );
            
            $_hash = hash('sha512', implode('', $_hash_array));

            $NVP_Array = array (
                'cust_id'=> $_Customer->getCustomerId(),                 
                'cust_id_desc' => 'Customer ID',                 
                'cust_name' => $_Customer->getFirstname(),                
                'cust_name_desc' => 'Customer Name',                
                'site_name' => self::getConfig()->getParam('MERCHANT_WEBSITE'),                 
                'product_id' => $_webpay_product_id,    
                'pay_item_id' => $_webpay_pay_item_id,                 
                'pay_item_name' => $_description,                 
                'amount' => $_amount,                 
                'currency' => self::getConfig()->getParam('CURRENCY_CODE'),                 
                'site_redirect_url' => $_return_url,               
                'txn_ref' => $_tranx_ref,                 
                'hash' => $_hash
            );
            
            
            
            // Log transaction
            WebPay\Log::insert([
                'transaction_ref' => $_tranx_ref, 
                'order_id' => $_Order->getID(), 
                'order_description' => $_description,
                'customer_id' => $_Customer->getID(),
                'customer_email' => $_Customer->getEmailAddress(),  
                'customer_name' => $_Customer->getName(),  
                'status' => 'pending', 
                'amount' => $_Order->getGrandTotal(false, false), // Store in Naira NOT small amount... 
                'currency_code' => 'NGN', 
                'gateway_mode' => $_mode

                ]);

            // Redirect shopper to Interswitch...
            self::redirect($NVP_Array);
            
    }



    
    /**
     * redirect
     *
     * @param array $NVP_Array
     * @return void
     */
    private static function redirect(array $NVP_Array) 
    {
        

        // Mode

        if (trim(strtolower(self::getConfig()->getParam('MODE'))) === 'live') {

            $_tranx_url = self::getConfig()->getParam('TRANX_ENDPOINT');


        } else {

            $_tranx_url = self::getConfig()->getParam('SANDBOX_TRANX_ENDPOINT');


        }
            
            
        // Send auto-submit form to shopper's browser...
        $NVP_Form_Fields = \Cataleya\Helper\NVPParser::toHTML($NVP_Array);
        
        echo '<body onload="document.upay_form.submit()">';
        echo '<form id="upay_form" name="upay_form" method="POST" action="'.$_tranx_url.'" target="_self">';
        echo $NVP_Form_Fields;
        echo '</form></body>';
        
        exit(1);
    }








    /**
     * getTransactionDetails
     *
     * @return void
     */
    private static function getTransactionDetails($_tranx_ref) 
    {


        $_tranx = WebPay\Log::fetch($_tranx_ref);
        if (empty($_tranx)) 
        {

            // Tranx record not found...
            return null;

        } else $_tranx = $_tranx[0];


        // Mode

        if (trim(strtolower($_tranx['gateway_mode'])) === 'live') {

            $_merchant_id = self::getConfig()->getParam('MERCHANT_ID');
            $_merchant_key = self::getConfig()->getParam('MAC_KEY');
            $_webpay_product_id = self::getConfig()->getParam('PRODUCT_ID');

            $_tranx_status_url = self::getConfig()->getParam('TRANX_STATUS_ENDPOINT');


        } else {

            $_merchant_id = 1;
            $_merchant_key = self::getConfig()->getParam('SANDBOX_MAC_KEY');
            $_webpay_product_id = self::getConfig()->getParam('SANDBOX_PRODUCT_ID');

            $_tranx_status_url = self::getConfig()->getParam('SANDBOX_TRANX_STATUS_ENDPOINT');

        }



        $NVP_Array = [
            'productid' => $_webpay_product_id, 
            'transactionreference' => $_tranx_ref, 
            'amount' => floatval($_tranx['amount'])*100, // convert to small amount... 
            'hash' => hash('sha512', $_webpay_product_id.$_tranx_ref.$_merchant_key)
        ];


        $NVP_String = $_tranx_status_url . '?' . \Cataleya\Helper\NVPParser::toString($NVP_Array);
        $result = \Cataleya\Helper\NVPParser::curlRequest($NVP_String, false, 'GET', [ 'hash: '.$NVP_Array['hash'] ]);
        
        if (empty($result)) {
            throw new \Cataleya\Error ('CURL Error: ' . \Cataleya\Helper\NVPParser::getLastCurlError());
        } else {
            file_put_contents(__path('logs').'/webpay.txt', $result);
        }

        
        return (!empty($result)) 
            ? json_decode($result) 
            : null;
        
    }



    



    /**
     * completeTransaction
     *
     * @param array $_params
     * @param array $_data
     * @return void
     */
    private static function getTransactionStatus(array $_params, array $_data)
    {

        $_response = [
            'transaction_ref' => $_params['transaction_ref'],  
            'transaction_amount' => '',  

            // This is specific to Cataleya!! 
            // Can only be: pending | paid | failed | cancelled | error | flagged
            'status' => 'pending',

            // Response from payment processor.
            // [response_description] is especially useful for displaying gateway specific 
            // response to shopper.
            'response_code' => '', 
            'response_description' => ''
        ]; 


        
        $_tranx = WebPay\Log::fetch($_params['transaction_ref']);
        if (empty($_tranx)) 
        {

            // Tranx record not found...
            return $_data;

        } else $_tranx = $_tranx[0];




        /* ------------------------ UPDATE TRANSACTION RECORD --------------------- */



        /*
        *
        * [Amount] => 17000 
        * [CardNumber] => 0095
        * [MerchantReference] => 2279023062d75a7f20c41f713c0fdd23323b28f9 
        * [PaymentReference] => FBN|WEB|WEBP|1-06-2015|125885 
        * [RetrievalReferenceNumber] => 000000065516 
        * [LeadBankCbnCode] =>
        * [LeadBankName] =>
        * [SplitAccounts] => Array ( ) 
        * [TransactionDate] => 2015-06-01T17:12:02.943 
        * [ResponseCode] => 00
        * [ResponseDescription] => Approved Successful )
        *
        * */
        $result = self::getTransactionDetails($_params['transaction_ref']);
        

        // Transaction Error ??
        if (!empty($result)) {


            // Validate response
            if (
                (int)$result->OrderID !== (int)$_params['order_id'] || 
                $result->MerchantReference !== $_tranx['transaction_ref'] || 
                (float)$result->Amount !== floatval($_tranx['amount'])*100   
            ) {
                $_response['status'] = 'flagged';
                return $_response;
            }


            // Update status array
            $_response['amount'] = $_tranx['amount'];
            $_response['response_code'] = $result->ResponseCode;
            $_response['response_description'] = $result->ResponseDescription;
            $_response['html'] = '<div>'.$result->ResponseDescription.'</div>';



            // If successful...
            switch ($result->ResponseCode) {

            case '00': 
                $_response['status'] = 'paid';
                break;

            case '17': 
                $_response['status'] = 'cancelled';
                break;

            case '06': 
                $_response['status'] = 'error'; // by inactivity
                break;

            case '91': // Issuer inoperative... 
                $_response['status'] = 'failed';
                break;

            case 'A0': 
                $_response['status'] = 'error';
                break;

            case '51': // insufficient funds... 
                $_response['status'] = 'failed';
                break;

            case '59': // Suspected fraud... 
                $_response['status'] = 'flagged';
                break;

            default:
                $_response['status'] = 'error';
                break;
            
            }
            
            
            // Update transaction log
            WebPay\Log::update(
                $_params['transaction_ref'], 
                [
                   'status' => $_response['status'], 
                   'webpay_card_num' => $result->CardNumber, 
                   'webpay_pay_ref' => $result->PaymentReference, 
                   'webpay_ret_ref' => $result->RetrievalReferenceNumber, 
                   'webpay_response_code' => $result->ResponseCode, 
                   'webpay_response_description' => $result->ResponseDescription, 
                   'webpay_tranx_date' => $result->TransactionDate, 
                   'webpay_lead_bank_name' => $result->LeadBankName, 
                   'webpay_cbn_code' => $result->LeadBankCbnCode, 
                   'webpay_appr_amount' => $result->Amount
                ]);


        }
        
        return $_response;

        
        
    }  



    /**
     * getTransactions
     *
     * @param array $_params
     * @param array $_data
     * @return array
     */
    private static function getTransactions (array $_params, array $_data) {

        $_query = WebPay\Log::fetch($_params['order_id'], 'order_id');
        $_reciepts = [];

        foreach ($_query as $_t) {

            $_reciepts[] = [
                'app_handle' => self::getHandle(), 
                'transaction_ref' => $_t['transaction_ref'],  
                'amount' => $_t['amount'],  

                // This is specific to Cataleya!! 
                // Can only be: pending | paid | failed | cancelled | error | flagged
                'status' => $_t['status'],

                // Payment Processor
                'payment_processor' => 'Interswitch Web Pay', 

                // Response from payment processor.
                // [response_description] is especially useful for displaying gateway specific 
                // response to shopper.
                'response_code' => $_t['webpay_response_code'], 
                'response_description' => $_t['webpay_response_description'], 
                'datetime' => $_t['date_added']

            ]; 

        }


        return array_merge($_data, $_reciepts);

    }
        
    

    /**
     * getPaymentInstructions
     *
     * @param array $_params
     * @param array $_data
     * @return void
     */
    private static function getPaymentInstructions(array $_params, array $_data)
    {

        return $_data;

        
        
    }  
    
    
 

}
