SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';


CREATE  TABLE IF NOT EXISTS `app_webpay_log` (
  `gateway_mode` VARCHAR(45) NOT NULL ,
  `transaction_ref` VARCHAR(160) NOT NULL ,
  `customer_id` INT(11) UNSIGNED NOT NULL DEFAULT '0' ,
  `customer_name` VARCHAR(255) NOT NULL DEFAULT 'name' ,
  `customer_email` VARCHAR(255) NOT NULL DEFAULT 'email' ,
  `order_id` INT(11) UNSIGNED NOT NULL DEFAULT '0' ,
  `order_description` TEXT NULL ,
  `amount` DECIMAL(11,2) NOT NULL DEFAULT '0.00' ,
  `currency_code` VARCHAR(45) NULL ,
  `status` VARCHAR(255) NULL ,
  `sort_order` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `date_added` DATETIME NOT NULL ,
  `webpay_pay_ref` VARCHAR(255) NULL ,
  `webpay_ret_ref` VARCHAR(45) NULL ,
  `webpay_appr_amount` VARCHAR(160) NULL ,
  `webpay_response_code` VARCHAR(255) NULL ,
  `webpay_response_description` MEDIUMTEXT NULL ,
  `webpay_tranx_date` VARCHAR(255) NULL ,
  `webpay_card_num` VARCHAR(45) NULL ,
  `webpay_cbn_code` VARCHAR(255) NULL ,
  `webpay_lead_bank_name` VARCHAR(255) NULL ,
  PRIMARY KEY (`transaction_ref`, `sort_order`) ,
  INDEX `idx_transaction_ref` (`sort_order` DESC, `transaction_ref` DESC) ) 
ENGINE = InnoDB;




SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;

