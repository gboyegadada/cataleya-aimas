<?php



define ('OUTPUT', 'JSON'); // Whether to output as JSON or HTML (especially errors)...


// LOAD APPLICATION TOP...
require_once ('app_top.php');




$WHITE_LIST = array(
						'auth_token',  
                                                'target_id', 
                                                'do', 
                                                'data'
						);





// DIE IF INPUT IS NOT VIA POST METHOD...
if ($_SERVER['REQUEST_METHOD'] != 'POST') _catch_error('POST METHOD ONLY PLEASE. NOW GET.', __LINE__, true);

// SANITIZE INPUT DATA...
$_CLEAN = filter_input_array(INPUT_POST, array(
                                                    'auth_token'		=>	FILTER_SANITIZE_STRIPPED,
                                                    'target_id'	=>	FILTER_VALIDATE_INT, 
                                                    'data'	=>	array( 'filter'=> FILTER_SANITIZE_STRIPPED, 'flags' => FILTER_REQUIRE_ARRAY ), 
                                                    'do'	=>	array('filter'		=>	FILTER_VALIDATE_REGEXP, 
                                                                                                'options'	=>	array('regexp' => '/^[A-Za-z ]{1,50}$/')
                                                                                                                ), 

                                                    )
										
					);


// Check if INPUT keys are ALL expected AND MADE IT THROUGH...
$_suspect = anySuspects();
if ($_suspect !== FALSE) _catch_error('Error processing white_list!' . $_suspect, __LINE__, true);




// VALIDATE AUTH TOKEN...
validate_auth_token ();

// Validate task

$_FUNCS = array (
    'void', 
    'newSale', 
    'saveSale', 
    'deleteSale', 
    'getInfo'
    
);





if (!in_array($_CLEAN['do'], $_FUNCS) || !is_callable($_CLEAN['do'])) _catch_error('Invalid action.', __LINE__, true);



    
// OUTPUT...
 $json_reply = array (
                 "status" => 'ok', 
                 "message" => 'Ok'
                 );
 
 

// EXECUTE API CALL

// API FUNC CALL
$_CLEAN['do']();



// HELPER FUNCS


function digestParams () 
{
    global $_CLEAN;
    $_params = $_CLEAN['data'];
    $_bad_params = FALSE;
    
    // [1] Validate description
    $_params['language_code'] = 'EN';
    
    //$_params['title'] = Cataleya\Helper\Validator::couponCode($_params['title']);
    if ($_params['title'] === FALSE) $_bad_params = TRUE;
    
    
    // [2] Discount Type
    // 0: percentage, 1: cash amount
    $_params['discount_type'] = Cataleya\Helper\Validator::int($_params['discount_type'], 0, 1);
    if ($_params['discount_type'] === FALSE) $_bad_params = TRUE;  
    else {
        $_params['discount_type'] = ($_params['discount_type'] === 0) 
                ? Cataleya\Catalog\Price::TYPE_REDUCTION_PERCENT 
                : Cataleya\Catalog\Price::TYPE_REDUCTION;
    }
    
    // [3] Discount Amount
    $_max = ($_params['discount_type'] !== FALSE && $_params['discount_type'] === Cataleya\Catalog\Price::TYPE_REDUCTION_PERCENT) ? 100 : NULL;
    $_params['discount_amount'] = Cataleya\Helper\Validator::float($_params['discount_amount'], 0, $_max);
    if ($_params['discount_amount'] === FALSE) $_bad_params = TRUE;
    
    
    // [7] Enable / Disable
    $_params['is_active'] = Cataleya\Helper\Validator::bool($_params['is_active']);
    if ($_params['is_active'] === NULL) $_bad_params = TRUE;
    
    // [8] Store id
    $_params['store_id'] = Cataleya\Helper\Validator::int($_params['store_id']);
    if ($_params['store_id'] === FALSE) $_bad_params = TRUE;
    
    
    // Check if every one made it through
    if ($_bad_params) _catch_error('Bad params.', __LINE__, true);
    
    return $_params;
}









/*
 * 
 * 
 * CALLBACKS
 * 
 */


function void () {
    
}






/*
 * 
 * [ newSale ]
 * ______________________________________________________
 * 
 * @param: int $_POST['target_id']
 * @param: int $_POST['data']
 * 
 */


function newSale () {

    global $json_reply;
    $_params = digestParams();
    
    $_Store = Cataleya\Store::load($_params['store_id']);
    if ($_Store === NULL ) _catch_error('Store could not be found.', __LINE__, true);
    
    $_Sale = Cataleya\Sales\Sale::create($_Store, $_params);
    
    
    $json_reply['message'] = 'New sale saved.';
    $json_reply['Sale'] = Cataleya\Front\Dashboard\Juicer::juice($_Sale);

};




/*
 * 
 * [ saveSale ]
 * ______________________________________________________
 * 
 * @param: int $_POST['target_id']
 * @param: int $_POST['data']
 * 
 */


function saveSale () {

    global $_CLEAN, $json_reply;
    
    $_params = digestParams();
    $_Sale = Cataleya\Sales\Sale::load($_CLEAN['target_id']);
    if ($_Sale === NULL ) _catch_error('Sale could not be found.', __LINE__, true);
    
    // [1] Sale Description
    $_Sale->getDescription()->setTitle($_params['title'], 'EN');
    
    // [2] Discount Type
    $_Sale->setDiscountType($_params['discount_type'])
            
    // [3] Discount Amount
    ->setDiscountAmount($_params['discount_amount']);

    
    // [7] Enable  / Disable
    if ($_params['is_active']) $_Sale->enable ();
    else $_Sale->disable ();
    
    
    $json_reply['message'] = 'New sale saved.';
    $json_reply['Sale'] = Cataleya\Front\Dashboard\Juicer::juice($_Sale);

};





/*
 * 
 * [ deleteSale ]
 * ______________________________________________________
 * 
 * @param: int $_POST['target_id']
 * 
 * 
 */


function deleteSale () {

    global $_CLEAN, $json_reply;
 
    $_Sale = Cataleya\Sales\Sale::load($_CLEAN['target_id']);
    if ($_Sale === NULL ) _catch_error('Sale could not be found.', __LINE__, true);
    
    
    $message = 'Are you sure you want to delete this SALE: "' . $_Sale->getDescription()->getTitle('EN') . '" (' . $_Sale->getStore()->getDescription()->getTitle('EN') .  ')?';
    confirm($message, FALSE);
    
    $_Sale->delete();
    
    $json_reply['message'] = 'Sale deleted.';
    $json_reply['Sale'] = array ('id'=>$_CLEAN['target_id']);

};






/*
 * 
 * [ getInfo ]
 * ______________________________________________________
 * 
 * @param: int $_POST['target_id']
 * 
 * 
 */


function getInfo () {

    global $_CLEAN, $json_reply;
 
    $_Sale = Cataleya\Sales\Sale::load($_CLEAN['target_id']);
    if ($_Sale === NULL ) _catch_error('Sale could not be found.', __LINE__, true);
    
    $json_reply['message'] = 'New sale saved.';
    $json_reply['Sale'] = Cataleya\Front\Dashboard\Juicer::juice($_Sale);

};






// Remove token...
if (isset($_SESSION[SESSION_PREFIX.'CONFIRM_TOKEN'])) unset ($_SESSION[SESSION_PREFIX.'CONFIRM_TOKEN']);




 echo (json_encode($json_reply));
 exit();  

?>