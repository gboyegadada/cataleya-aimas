<?php



define ('OUTPUT', 'AJAX_JSON'); // Whether to output as JSON or HTML (especially errors)...


// LOAD APPLICATION TOP...
require_once ('app_top.php');




$_ids = Cataleya\Helper\Validator::array_of('foldername', $_GET['id'], 1, 150);
if ($_ids === FALSE) _catch_error('Invalid ID.', __LINE__, true);



$_theme_id = Cataleya\System::load()->getDashboardThemeID();
$_templates = array ();

foreach ($_ids as $_id) {
    $_path_to_mustache = TEMPLATES_PATH . $_theme_id . '/mustache/' . $_id . '.mustache';

    if (file_exists($_path_to_mustache)) $_templates[$_id] = file_get_contents ($_path_to_mustache);
    else _catch_error('Template (' . $_path_to_mustache . ') not found.', __LINE__, true);
    
}


// OUTPUT...
$json_reply = array (
		"status"=>'ok', 
		"message"=>'Mustache templates.', 
                "templates"=>$_templates
		);

echo (json_encode($json_reply));
exit();