<?php



define ('OUTPUT', 'HTML'); // Whether to output as JSON or HTML (especially errors)...


// LOAD APPLICATION TOP...
require_once ('app_top.php');





/*
 * 
 * CHECK USER PRIVILEGES
 * 
 */


if ((!isset($_admin_role) || !$_admin_role->hasPrivilege('VIEW_CUSTOMER_ACCOUNT')) && !IS_SUPER_USER)  include_once INC_PATH.'forbidden.php';


define('READONLY', (!$_admin_role->hasPrivilege('EDIT_CUSTOMER_ACCOUNT') && !IS_SUPER_USER) ? FALSE : TRUE);




// SANITIZE GET DATA...
$_CLEAN = filter_input_array(INPUT_GET, 
								array(	
										 // Page number...
										'pg'	=>	array('filter'		=>	FILTER_VALIDATE_INT, 
																'options'	=>	array('min_range' => 1, 'max_range' => 1000)
																), 
                                                                                // Page size...
										'pgsize'	=>	array('filter'		=>	FILTER_VALIDATE_INT, 
																'options'	=>	array('min_range' => 1, 'max_range' => 1000)
																),                                                                     
										
										 // order 						
										'o'	=>	array('filter'		=>	FILTER_VALIDATE_INT, 
																'options'	=>	array('min_range' => 0, 'max_range' => 100)
																), 
                                                                                // order by 						
										'ob'	=>	array('filter'		=>	FILTER_VALIDATE_INT, 
																'options'	=>	array('min_range' => 0, 'max_range' => 100)
																),    
                                                                                // index 						
										'i'	=>	array('filter'		=>	FILTER_VALIDATE_REGEXP, 
														'options'	=>	array('regexp' => '/^[A-Za-z]{1}$/')
																), 

										)
								);


// Deafault page size...
define ('DEFAULT_PAGE_SIZE', 1);


// Check if INPUT keys are ALL expected AND MADE IT THROUGH...

$_CLEAN['pg'] = empty($_CLEAN['pg']) ? 1 : (int)$_CLEAN['pg'];
$_CLEAN['i'] = empty($_CLEAN['i']) ? 'all' : $_CLEAN['i'];
$_CLEAN['pgsize'] = empty($_CLEAN['pgsize']) ? DEFAULT_PAGE_SIZE : (int)$_CLEAN['pgsize'];
$_CLEAN['o'] = empty($_CLEAN['o']) ? Cataleya\Geo\Countries::ORDER_ASC : $_CLEAN['o'];
$_CLEAN['ob'] = empty($_CLEAN['ob']) ? Cataleya\Customers::ORDER_BY_FIRSTNAME : $_CLEAN['ob'];

define ('NO_OF_PAGE_INDEX_ANCHORS', 3);

define('ORDER_BY', $_CLEAN['ob']);
define('ORDER', $_CLEAN['o']);
define('INDEX', $_CLEAN['i']);
define('PAGE_SIZE', $_CLEAN['pgsize']);


$_Customers = Cataleya\Customers::load(array('order_by'=>ORDER_BY, 'order'=>ORDER, 'index'=>INDEX), PAGE_SIZE, $_CLEAN['pg'])




////////////////////// PAGINATION /////////////





?>




<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Admin | Browse Customers (<?= Cataleya\Helper::countInEnglish($_Customers->getPopulation(), 'customer', 'customers') ?>)</title>


<!-- JS FILES -->
<?php
require_once(INC_PATH.'js_libs.php');
?>
<script type="text/javascript" src="ui/jscript/customers.js"></script>


<!-- STYLE SHEETS -->
<?php
require_once(INC_PATH.'css_libs.php');
?>

<link href="ui/css/indexed-paginated-list.css" rel="stylesheet" type="text/css" />
<link href="ui/css/bootmetro-tiles.css" rel="stylesheet" type="text/css" />
<link href="ui/css/customers-browse.css" rel="stylesheet" type="text/css" />


<style type="text/css" media="all">
.content {
	height:<?=(PAGE_SIZE*210)?>px;
	
}




.mediumListIconTextItem > .tile-icon-large {
    margin-left: 10px;
}


.mediumListIconTextItem {
    background-color:#111; 
}



</style>


<script type="text/javascript">


</script>


</head>

<body>

<!-- BEGIN: META -->
<input type="hidden" id="meta_token" value="<?=AUTH_TOKEN?>"  />
<!-- END: META -->




<div class="container">
<?php include_once INC_PATH.'photo-upload-forms.php'; ?>
    
    
<div class="header-wrapper absolute">
    <div class="header">

    <a href="<?=BASE_URL?>landing.customers.php"><div id="page-title">Customers</div></a><?php include_once INC_PATH.'page.nav.php'; ?> 

    </div>
    
    
<?php
        include_once INC_PATH.'main.nav.php';
?>
    
</div>


  <div class="content">



<div id="search-tool-wrapper">
		
<?php
define('QUICK_SEARCH_URL', BASE_URL.'customer_search.ajax.php');
include INC_PATH.'quick-search-widget.php';
?>



</div>




<!-- CUSTOMER INDEX -->

<ul id="list-index">
<a href="browse.customers.php?pg=0&ob=<?=$_CLEAN['ob']?>&o=<?=$_CLEAN['o']?>&i=all" <?=(($_CLEAN['i']=='all') ? 'class="button-1-active"' : '')?>><li>all</li></a>

<?php
$alpha = array('A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z');

for ($i = 0; $i < (count($alpha)); $i++) {
	$a = strtolower($alpha[$i]);
	?>
<a href="browse.customers.php?pg=0&ob=<?=$_CLEAN['ob']?>&o=<?=$_CLEAN['o']?>&i=<?=$a?>" <?=(($_CLEAN['i']==$a) ? 'class="button-1-active"' : '')?>><li><?=$alpha[$i]?></li></a>

<?php
}

?>



</ul>



<div id="hor-line-1">&nbsp;</div>






<!-- CUSTOMERS -->

<ul id="list-table">
<?php if ($_Customers->getPopulation() > (int)$_CLEAN['pgsize']): ?>    
    
<li class="list-table-header">

        <div class="list-pagination">
        <?php if ($_Customers->getPopulation() > 0) { ?>
            Page <?=$_Customers->getPageNumber()?> of <?=$_Customers->getLastPageNum()?>
        <?php } else { ?>
        
        No customers with first name starting with '<?=strtoupper($_CLEAN['i'])?>'.
        
        <?php } ?>
        
        </div>

        <ul class="list-page-nav">
            <li><a <?php if (($_Customers->getPageNumber() < $_Customers->getLastPageNum())) { ?>href="browse.customers.php?pg=<?=$_Customers->getNextPageNum()?>&o=<?=$_CLEAN['o']?>&ob=<?=$_CLEAN['ob']?>&i=<?=$_CLEAN['i']?>"  class="button-1"<?php } else { ?>NOHREF class="button-1-no-href"<?php } ?>>next</a></li>
        

        <li><a <?php if ($_Customers->getPageNumber() > 1) { ?>href="browse.customers.php?pg=<?=$_Customers->getPrevPageNum()?>&o=<?=$_CLEAN['o']?>&ob=<?=$_CLEAN['ob']?>&i=<?=$_CLEAN['i']?>"  class="button-1"<?php } else { ?>NOHREF class="button-1-no-href"<?php } ?>>previous</a></li>
        
        </ul>

</li>
<?php endif; ?>




<li>

        
        <div id="grid-wrapper" class="listview-container grid-layout">
            
            <?php
            foreach ($_Customers as $_Customer) {

            ?>
            <a href="<?=BASE_URL?>customer_profile.customers.php?id=<?=$_Customer->getCustomerId()?>" class="admin-profile-tile" id="admin-profile-<?=$_Customer->getCustomerId()?>" >
            <div class="mediumListIconTextItem user-status <?=$_Customer->isOnline() ? 'online' : 'offline'?>">
                    

                    <span class="icon-user-4 tile-icon-large" ></span>
                    
                    <div class="mediumListIconTextItem-Detail">
                        <h4><?=$_Customer->getFirstname()?> <?=$_Customer->getLastname()?></h4>
                        <h6>
                        <span>&nbsp;<?=$_Customer->getTelephone()?></span><br />
                        <span>&nbsp;<?=$_Customer->getEmailAddress()?></span>
                        </h6>
                     </div>
            </div>
            </a>

            <?php

            }
            ?>
            
            
            <!-- NEW ADMIN PROFILE -->
            <?php if (IS_SUPER_USER || $adminUser->getRole()->hasPrivilege('CREATE_CUSTOMER_ACCOUNT')): ?> 
            <a href="javascript:void(0);" onClick="show_new_customer_form(); return false;" >
            <div class="mediumListIconTextItem button-4">
                    
                    <span class="icon-contact tile-icon-large" ></span>
                    <div class="mediumListIconTextItem-Detail">
                        <h4>New Customer Profile</h4>
                     </div>
            </div>
            </a>
            <?php endif ?>
            
        </div>
        
        
        
</li>



</ul>




<!--
<br/><br/><br/>

<div id="new-customer-link-wrapper">
<a href="javascript:void(0);" onClick="$('#new-customer-link-wrapper').hide(); $('#new-customer-form-wrapper').fadeIn(200); return false;" >Create a new customer profile.</a>

</div>

-->



<div id="new-customer-form-wrapper">	
	<h2 id="new-customer-form-label">
	New Customer&nbsp;&nbsp;&nbsp;
	</h2>
    
    <ul class="password-strength-meter" id="password-meter-1">
    	<li class="password-strength-poor password-strength-weak password-strength-good password-strength-excellent">&nbsp;</li>
    	<li class="password-strength-weak password-strength-good password-strength-excellent">&nbsp;</li>
    	<li class="password-strength-good password-strength-excellent">&nbsp;</li>
    	<li class="password-strength-excellent">&nbsp;</li>
    </ul>

	

            <form id="new-customer-form" name="new_customer_form" style="padding-left:30px; " action="customer.html">
            
            <input type="hidden" name="auth_token" value="<?=AUTH_TOKEN?>"  />
            <input type="hidden" name="rw_token" value="<?=RW_TOKEN?>"  />
        
            <input id="new-customer-fname" name="fname" type="text" placeholder="First name..." tip="Enter customer's <b>first name</b> <small>Use only A-Z, a-z, 0-9, spaces, dots or dashes!</small>"  onfocus="showTip(this);" size=20 />
            <input id="new-customer-lname" name="lname" type="text" placeholder="Last name..." tip="Enter customer's <strong>last name</strong> <small>Use only A-Z, a-z, 0-9, spaces, dots or dashes!</small>"  onfocus="showTip(this);" size=20 />
            <br/><br/>
            <input id="new-customer-tel" name="tel" type="text" onkeypress="return checkPhoneDigit(event, this);" onkeyup="return recheckPhoneDigit(event, this);" placeholder="Telephone..." tip="Enter customer's <strong>phone number</strong> <small></small>"  onfocus="showTip(this);" size=20 />
            <input id="new-customer-email" name="email" type="text" placeholder="Email..." tip="Enter customer's <strong>email address</strong> <small></small>"  onfocus="showTip(this);" size=20 />
            <br/><br/>
            <input id="new-customer-dob" name="dob" type="text" placeholder="dd / mm / yyyy" tip="Enter customer's <strong>date of birth</strong> <small>Use only A-Z, a-z, 0-9, spaces, dots or dashes!</small>"  onfocus="showTip(this);" size=20 />
            <input id="new-customer-password" name="password" type="password" value="Password..." tip="Enter customer's <strong>password</strong> <small>Use only A-Z, a-z, 0-9, spaces, dots or dashes!</small>" onblur="resetTextbox(this); "  onfocus="resetTextbox(this); showTip(this);" size=20 />
        
            <br/><br/>
                
            
            <input type="submit" value="Create" size=20>
            <input class="cancel-button" type="button" value="Cancel" size=20 />
        
            </form>


</div>




<div id="footer">
&nbsp;
</div>



</div>

</div>


</body>
</html>