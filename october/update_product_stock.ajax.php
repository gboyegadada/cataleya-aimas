<?php





define ('OUTPUT', 'JSON'); // Whether to output as JSON or HTML (especially errors)...


// LOAD APPLICATION TOP...
require_once ('app_top.php');








// GET JSON
$_JSON = get_json();

if ($_JSON === FALSE || !isset($_JSON->auth_token) || !isset($_JSON->product_id)) _catch_error('Bad params.', __LINE__, true);

$_CLEAN = array (
                    'auth_token' => $_JSON->auth_token, 
                    'product_id' => $_JSON->product_id
        
        );


// SANITIZE INPUT DATA...
$_CLEAN = filter_var_array($_CLEAN, 
								array(
										'auth_token'		=>	FILTER_SANITIZE_STRIPPED, 
										'product_id'		=>	FILTER_VALIDATE_INT

										)
										
								);


// ROLL CALL
if (in_array(FALSE, array_values($_CLEAN))) _catch_error('Bad params.', __LINE__, true);


// VALIDATE AUTH TOKEN...
validate_auth_token ();


// Load product..
$product = Cataleya\Catalog\Product::load($_CLEAN['product_id']);
if ($product === NULL) _catch_error('Product not found.', __LINE__, true);




// CHECK PRICE DATA
$data = (array) $_JSON->data;
foreach ($data as $entry)
{
    if (!isset($entry->option_id) || !isset($entry->store_id) || !isset($entry->stock)) _catch_error('Stock information could not be updated.', __LINE__, true);
    else if (filter_var($entry->option_id, FILTER_VALIDATE_INT) === FALSE || filter_var($entry->store_id, FILTER_VALIDATE_INT) === FALSE || filter_var($entry->stock, FILTER_VALIDATE_INT) === FALSE) _catch_error('Stock information could not be updated.', __LINE__, true);
   
}



foreach ($data as $entry)
{
    // Load store
    $store = Cataleya\Store::load($entry->store_id);
    if ($store === NULL) _catch_error('Error loading store.', __LINE__, true);


    // Load option
    $option = Cataleya\Catalog\Product\Option::load($entry->option_id);
    if ($option === NULL) _catch_error('Error loading option.', __LINE__, true);
    
    $stock = $option->getStock('stock');

    
    // Set new stock
    $stock->setValue($store, $entry->stock);
}   








// Output...
$json_data = array (
                'status' => 'Ok', 
                'message' => 'Stock information updated.'
                );
echo json_encode($json_data);
exit();








?>