<?php


            

//define ('OUTPUT', 'JSON'); // Whether to output as JSON or HTML (especially errors)...

// For our file upload...allow up to 5 minutes...
set_time_limit (60 * 5); 


//define ('OUTPUT', 'JSON'); // Whether to output as JSON or HTML (especially errors)...


// LOAD APPLICATION TOP...
require_once ('app_top.php');






/* ------------------ 2. Read CSV data ---------------------------------------


$_path_to_csv = ROOT_PATH . 'var/imports/' . 'shopify_products_export.csv';
foreach (Cataleya\Stores::load() as $_Store) {
  $_Importer = new Cataleya\Helper\Importer($_Store, new Cataleya\Helper\Importer\Reader\ShopifyCSV($_path_to_csv));
  break;
}


exit();
 */




$WHITE_LIST = array(
						'auth_token',  
                                                'target_id', 
                                                'csv_uploader', 
                                                'do', 
                                                'data'
						);




// DIE IF INPUT IS NOT VIA POST METHOD...
if ($_SERVER['REQUEST_METHOD'] != 'POST') _catch_error('POST METHOD ONLY PLEASE. NOW GET.', __LINE__, true);

// SANITIZE INPUT DATA...
$_CLEAN = filter_input_array(INPUT_POST, array(
                                                    'auth_token'		=>	FILTER_SANITIZE_STRIPPED,
                                                    'target_id'	=>	FILTER_VALIDATE_INT, 
                                                    'data'	=>	array( 'flags' => FILTER_REQUIRE_ARRAY ), 
                                                    'do'	=>	array('filter'		=>	FILTER_VALIDATE_REGEXP, 
                                                                                                'options'	=>	array('regexp' => '/^[A-Za-z ]{1,50}$/')
                                                                                                                ), 

                                                    )
										
					);






// Check if INPUT keys are ALL expected AND MADE IT THROUGH...
$_suspect = anySuspects();
if ($_suspect !== FALSE) _catch_error('Error processing white_list!' . $_suspect, __LINE__, true);




// VALIDATE AUTH TOKEN...
validate_auth_token ();



    
    
define("FILE_INPUT_NAME", "csv_uploader");



/* ------------------- 1: Handle Upload. --------------------------------------- */

$_DateTime = new DateTime ();

$src = $_FILES[FILE_INPUT_NAME]['tmp_name'];

if (empty($src) || !file_exists($src) || !is_uploaded_file($src)) _catch_error('Import failed.', __LINE__, true);


$file_name = 'import_' . $_DateTime->format('_d-m-Y_h.i.s') . '.csv';
define('CSV_PATH', ROOT_PATH . 'var/imports/' . $file_name);


if(!move_uploaded_file($src, CSV_PATH))_catch_error('Import failed.', __LINE__, true);






// Validate task

$_FUNCS = array (
    'void' => array (), 
    'importCSV' => array (
        'store_id' => 'int', 
        'type' => 'int'
    )
    
);






if (!array_key_exists($_CLEAN['do'], $_FUNCS) || !is_callable($_CLEAN['do'])) _catch_error('Invalid action.', __LINE__, true);



define('DASH_LANGUAGE_CODE', Cataleya\System::load()->getDashboardLanguageCode());
define('SHOPFRONT_LANGUAGE_CODE', Cataleya\System::load()->getShopFrontLanguageCode());
     

    
// OUTPUT...
 $json_reply = array (
                 "status" => 'ok', 
                 "message" => 'Ok'
                 );
 
 

// EXECUTE API CALL

// API FUNC CALL
$_CLEAN['do']();



// HELPER FUNCS



function digestParams () 
{
    global $_CLEAN, $_FUNCS;
    $_params = Cataleya\Helper\Validator::params($_FUNCS[$_CLEAN['do']], $_CLEAN['data']);
    $_bad_params = array ();
    

    if (!isset($_params['language_code'])) 
    {
        $_params['language_code'] = DASH_LANGUAGE_CODE;

    }
    
    
    
    
    
    // Check if every one made it through
    
    foreach ($_params as $k=>$v) {
        if ($v === NULL) $_bad_params[] = $k;
    }
    

    if (!empty($_bad_params)) _catch_error('Unable to save.' . implode, __LINE__, true);
    
    return $_params;
}












/*
 * 
 * [ importCSV ]
 * ______________________________________________________
 * 
 * @param: int $_POST['target_id']
 * @param: int $_POST['data']
 * 
 */


function importCSV () {

    global $json_reply;
    $_params = digestParams();
    
    $_Store = Cataleya\Store::load($_params['store_id']);
    if ($_Store === NULL ) _catch_error('Store could not be found.', __LINE__, true);
    
    $_Importer = new Cataleya\Helper\Importer(new Cataleya\Helper\Importer\Reader\ShopifyCSV(CSV_PATH));
    
    //$_Page = Cataleya\Front\Shop\Page::create($_Store, $_params['title'], $_params['description'], 'EN');
    
    $json_reply['message'] = 'CSV has been importedt.';

}








echo json_encode($data);

