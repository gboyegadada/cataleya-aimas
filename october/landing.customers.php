<?php



define ('OUTPUT', 'HTML'); // Whether to output as JSON or HTML (especially errors)...


// LOAD APPLICATION TOP...
require_once ('app_top.php');


/*
 * 
 * Check user permissions...
 * 
 */

if (!IS_SUPER_USER && !$adminUser->getRole()->hasPrivilege('VIEW_CUSTOMER_ACCOUNT')) include_once INC_PATH.'forbidden.php';


$WHITE_LIST = array(
						'a'
					);





// SANITIZE GET DATA...
$_CLEAN = filter_input_array(INPUT_GET, 
								array(	
										// auth_token...
										'a'		=>	FILTER_SANITIZE_STRIPPED

										)
								);



// Check if INPUT keys are ALL expected AND MADE IT THROUGH...
// if (anySuspects()) _catch_error('Error processing white_list!', __LINE__, true);



?>




<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<?php
include(INC_PATH.'meta.php');
?>
<title>Admin | Customers</title>


<!-- JS FILES -->
<?php
require_once(INC_PATH.'js_libs.php');
?>
<script type="text/javascript" src="ui/jscript/customers.js"></script>


<!-- STYLESHEETS -->
<?php
require_once(INC_PATH.'css_libs.php');
?>

<link href="ui/css/customers-landing.css" rel="stylesheet" type="text/css" />


<style type="text/css" media="all">

.content {
	height:750px;
	
}


#search-tool-wrapper {
	left:200px;
	top:170px;
	
	width:590px;
	
}


#quick-search-results {
	left:163px;
	top:70px;
	
}


#quick-search-form-wrapper {
	padding:27px 0px 10px 20px;
	
}


</style>


<script type="text/javascript">


</script>


</head>

<body>

<!-- BEGIN: META -->
<input type="hidden" id="meta_token" value="<?=AUTH_TOKEN?>"  />
<input type="hidden" id="meta_admin_root" value="<?=BASE_URL?>"  />
<!-- END: META -->




<div class="container">
<?php include_once INC_PATH.'photo-upload-forms.php'; ?>
    
<div class="header-wrapper absolute">
    <div class="header">

    <a href="<?=BASE_URL?>"><div id="page-title">Customers</div></a><?php include_once INC_PATH.'page.nav.php'; ?>   

    </div>
    
<?php include_once INC_PATH.'main.nav.php'; ?>
    
</div>
    
    
<div class="content">





<div id="search-tool-wrapper">
		
    <div id="customer-browse-wrapper">
        <a href="<?=BASE_URL?>browse.customers.php?pg=0&o=1&i=all&a=<?=AUTH_TOKEN?>">Browse</a>
	</div>
	
        <?php
        define('QUICK_SEARCH_URL', BASE_URL.'customer_search.ajax.php');
        include INC_PATH.'quick-search-widget.php';
        ?>



</div>




<br/><br/><br/>


<?php if ((isset($_admin_role) && $_admin_role->hasPrivilege('CREATE_CUSTOMER_ACCOUNT')) || IS_SUPER_USER): ?>
<div id="new-customer-link-wrapper">
<a href="javascript:void(0);" onClick="$('#new-customer-link-wrapper').hide(); $('#new-customer-form-wrapper').fadeIn(200); return false;" >Create a new customer profile.</a>
</div>

<?php endif ?>


<div id="new-customer-form-wrapper">	
	<h2 id="new-customer-form-label">
	New Customer&nbsp;&nbsp;&nbsp;
	</h2>
    
    <ul class="password-strength-meter" id="password-meter-1">
    	<li class="password-strength-poor password-strength-weak password-strength-good password-strength-excellent">&nbsp;</li>
    	<li class="password-strength-weak password-strength-good password-strength-excellent">&nbsp;</li>
    	<li class="password-strength-good password-strength-excellent">&nbsp;</li>
    	<li class="password-strength-excellent">&nbsp;</li>
    </ul>

	

            <form id="new-customer-form" name="new_customer_form" style="padding-left:30px; " action="customer.html">
            
            <input type="hidden" name="auth_token" value="<?=AUTH_TOKEN?>"  />
            <input type="hidden" name="rw_token" value="<?=AUTH_TOKEN?>"  />
        
            <input id="new-customer-fname" name="fname" type="text" require="name" placeholder="First name..." tip="Enter customer's <b>first name</b> <small>Use only A-Z, a-z, 0-9, spaces, dots or dashes!</small>"  onfocus="showTip(this);" size=20 />
            <input id="new-customer-lname" name="lname" type="text" require="name" placeholder="Last name..." tip="Enter customer's <strong>last name</strong> <small>Use only A-Z, a-z, 0-9, spaces, dots or dashes!</small>"  onfocus="showTip(this);" size=20 />
            <br/><br/>
            <input id="new-customer-tel" name="tel" type="text" require="phone" onkeypress="return checkPhoneDigit(event, this);" onkeyup="return recheckPhoneDigit(event, this);" placeholder="Telephone..." tip="Enter customer's <strong>phone number</strong> <small></small>"  onfocus="showTip(this);" size=20 />
            <input id="new-customer-email" name="email" type="text" require="email" placeholder="Email..." tip="Enter customer's <strong>email address</strong> <small></small>"  onfocus="showTip(this);" size=20 />
            <br/><br/>
            <input id="new-customer-dob" name="dob" type="text" placeholder="dd / mm / yyyy" tip="Enter customer's <strong>date of birth</strong> <small>Use only A-Z, a-z, 0-9, spaces, dots or dashes!</small>"  onfocus="showTip(this);" size=20 />
            <input id="new-customer-password" name="password" require="password" type="password" value="Password..." tip="Enter customer's <strong>password</strong> <small>Use only A-Z, a-z, 0-9, spaces, dots or dashes!</small>" onblur="resetTextbox(this); "  onfocus="resetTextbox(this); showTip(this);" size=20 />
        
            <br/><br/>
                
            
            <input type="submit" value="Create" size=20>
            <input id="new-customer-cancel-button" type="button" value="Cancel" size=20 >
        
            </form>


</div>





<div id="footer">
&nbsp;
</div>



</div>

</div>


</body>
</html>