<?php



define ('OUTPUT', 'HTML'); // Whether to output as JSON or HTML (especially errors)...


// LOAD APPLICATION TOP...
require_once ('app_top.php');



/*
 * 
 * CHECK USER PRIVILEGES
 * 
 */


if ((!isset($_admin_role) || !$_admin_role->hasPrivilege('VIEW_COUNTRIES')) && !IS_SUPER_USER)  include_once INC_PATH.'forbidden.php';


define('READONLY', (!$_admin_role->hasPrivilege('EDIT_COUNTRIES') && !IS_SUPER_USER) ? FALSE : TRUE);




// SANITIZE GET DATA...
$_CLEAN = filter_input_array(INPUT_GET, 
								array(	
										 // Page number...
										'pg'	=>	array('filter'		=>	FILTER_VALIDATE_INT, 
																'options'	=>	array('min_range' => 1, 'max_range' => 1000)
																), 
										 // zone id...
										'id'	=>	array('filter'		=>	FILTER_VALIDATE_INT, 
																'options'	=>	array('min_range' => 1, 'max_range' => 1000)
																), 
                                                                                // Page size...
										'pgsize'	=>	array('filter'		=>	FILTER_VALIDATE_INT, 
																'options'	=>	array('min_range' => 1, 'max_range' => 1000)
																),                                                                     
										
										 // order 						
										'o'	=>	array('filter'		=>	FILTER_VALIDATE_INT, 
																'options'	=>	array('min_range' => 0, 'max_range' => 100)
																), 
                                                                                // order by 						
										'ob'	=>	array('filter'		=>	FILTER_VALIDATE_INT, 
																'options'	=>	array('min_range' => 0, 'max_range' => 100)
																),    
                                                                                // index 						
										'i'	=>	array('filter'		=>	FILTER_VALIDATE_REGEXP, 
														'options'	=>	array('regexp' => '/^[A-Za-z]{1}$/')
																), 

										)
								);


// Deafault page size...
define ('DEFAULT_PAGE_SIZE', 50);


// Check if INPUT keys are ALL expected AND MADE IT THROUGH...
if ($_CLEAN['id'] === FALSE) _catch_error('Invalid zone id.', __LINE__, true);

$_CLEAN['pg'] = empty($_CLEAN['pg']) ? 1 : (int)$_CLEAN['pg'];
$_CLEAN['i'] = empty($_CLEAN['i']) ? 'all' : $_CLEAN['i'];
$_CLEAN['pgsize'] = empty($_CLEAN['pgsize']) ? DEFAULT_PAGE_SIZE : (int)$_CLEAN['pgsize'];
$_CLEAN['o'] = empty($_CLEAN['o']) ? Cataleya\Geo\Countries::ORDER_ASC : $_CLEAN['o'];
$_CLEAN['ob'] = empty($_CLEAN['ob']) ? Cataleya\Geo\Countries::ORDER_BY_NAME : $_CLEAN['ob'];

define ('NO_OF_PAGE_INDEX_ANCHORS', 3);

define('ORDER_BY', $_CLEAN['ob']);
define('ORDER', $_CLEAN['o']);
define('INDEX', $_CLEAN['i']);
define('PAGE_SIZE', $_CLEAN['pgsize']);

// Load countries
$countries = Cataleya\Geo\Countries::load(array('order_by'=>ORDER_BY, 'order'=>ORDER, 'index'=>INDEX), PAGE_SIZE, $_CLEAN['pg']);

// Load zone
$zone = Cataleya\Geo\Zone::load($_CLEAN['id']);
if ($zone === NULL) _catch_error('Zone could not be loaded.', __LINE__, true);





////////////////////// PAGINATION /////////////





?>




<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Zone: <?=$zone->getZoneName()?> | Admin</title>


<!-- JS FILES -->
<?php
require_once(INC_PATH.'js_libs.php');
?>
<script type="text/javascript" src="ui/jscript/zones.js"></script>


<!-- STYLE SHEETS -->
<?php
require_once(INC_PATH.'css_libs.php');
?>

<link href="ui/css/indexed-paginated-list.css" rel="stylesheet" type="text/css" />
<link href="ui/css/bootmetro-tiles.css" rel="stylesheet" type="text/css" />
<link href="ui/css/zones.css" rel="stylesheet" type="text/css" />


<style type="text/css" media="all">
.content {
	height:<?= (ceil(((PAGE_SIZE > $countries->getPopulation()) ? $countries->getPopulation() : PAGE_SIZE)/3)*100)+520 ?>px;
	
}


#search-tool-wrapper {
	left:650px;
	top:130px;
	
}


#quick-search-results {
	left:10px;
	top:70px;
	
}


#list-table {
    top:300px;
}



</style>


<script type="text/javascript">


</script>


</head>

<body>

<!-- BEGIN: META -->
<input type="hidden" id="meta_token" value="<?=AUTH_TOKEN?>"  />
<input type="hidden" id="meta_zone_id" value="<?=$zone->getID()?>"  />
<!-- END: META -->




<div class="container">
    
    
<div class="header-wrapper absolute">
    <div class="header">

        <a href="<?=BASE_URL?>landing.zones.php"><div id="page-title">Zone: <?=$zone->getZoneName()?></div></a><?php include_once INC_PATH.'page.nav.php'; ?> 

    </div>
    
    
<?php
        include_once INC_PATH.'main.nav.php';
?>
    
</div>


  <div class="content">



<div id="search-tool-wrapper">
		
<?php
define('QUICK_SEARCH_URL', BASE_URL.'zone_search.ajax.php');
include INC_PATH.'quick-search-widget.php';
?>




</div>




<!-- COUNTRY INDEX -->

<ul id="list-index">
    <a href="zone.zones.php?pg=0&ob=<?=$_CLEAN['ob']?>&o=<?=$_CLEAN['o']?>&id=<?=$zone->getID()?>&i=all" <?=(($_CLEAN['i']=='all') ? 'class="button-1-active"' : '')?>><li>all</li></a>

<?php
$alpha = array('A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z');

for ($i = 0; $i < (count($alpha)); $i++) {
	$a = strtolower($alpha[$i]);
	?>
<a href="zone.zones.php?pg=0&ob=<?=$_CLEAN['ob']?>&o=<?=$_CLEAN['o']?>&id=<?=$zone->getID()?>&i=<?=$a?>" <?=(($_CLEAN['i']==$a) ? 'class="button-1-active"' : '')?>><li><?=$alpha[$i]?></li></a>

<?php
}

?>



</ul>



<div id="hor-line-1">&nbsp;</div>


<div id="page-options-panel">
    
            <a class="button-1" href="javascript:void(0);" onClick="zones.editZone(<?=$zone->getID()?>); return false;" >
                Edit this zone
            </a>
            &nbsp;&nbsp;
            <a class="button-1" href="javascript:void(0);" onClick="zones.deleteZone(<?=$zone->getID()?>); return false;" >
                Delete this zone
            </a>

            
            
            <div id="zone-type-description">
                <?php if ($zone->getListType() === 'exclude'): ?>
                This zone includes all countries except the selected ones. This is suitable for when you're setting up shipping for deliveries outside 
                a specific country. For example, setting up shipping for deliveries everywhere except for the U.K.
                <?php else: ?>
                This zone includes all selected countries. This is suitable for when you're setting up shipping for deliveries within a chosen country 
                (e.g. deliveries within the U.K only).
                <?php endif ?>
            </div>
</div>



<!-- COUNTRIES -->

<ul id="list-table">
    
<?php if ($countries->getPopulation() > (int)$_CLEAN['pgsize']): ?>
<li class="list-table-header">

        <div class="list-pagination">
        <?php if ($countries->getPopulation() > 0) { ?>
            Page <?=$countries->getPageNumber()?> of <?=$countries->getLastPageNum()?>
        <?php } else { ?>
        
        No countries with name starting with '<?=strtoupper($_CLEAN['i'])?>'.
        
        <?php } ?>
        
        </div>

        <ul class="list-page-nav">
            <li><a <?php if (($countries->getPageNumber() < $countries->getLastPageNum())) { ?>href="zone.zones.php?pg=<?=$countries->getNextPageNum()?>&o=<?=$_CLEAN['o']?>&ob=<?=$_CLEAN['ob']?>&id=<?=$zone->getID()?>&i=<?=$_CLEAN['i']?>"  class="button-1"<?php } else { ?>NOHREF class="button-1-no-href"<?php } ?>>next</a></li>
        

        <li><a <?php if ($countries->getPageNumber() > 1) { ?>href="zone.zones.php?pg=<?=$countries->getPrevPageNum()?>&o=<?=$_CLEAN['o']?>&ob=<?=$_CLEAN['ob']?>&id=<?=$zone->getID()?>&i=<?=$_CLEAN['i']?>"  class="button-1"<?php } else { ?>NOHREF class="button-1-no-href"<?php } ?>>previous</a></li>
        
        </ul>

</li>
<?php endif; ?>

    <li>
        <div id="countries-grid-wrapper" class="listview-container grid-layout">
            
            <?php
            foreach ($countries as $country) {

            ?>
            <a href="javascript:void(0);" onclick="zone.updateZone('<?=$country->getCountryCode()?>', <?=$country->hasProvinces() ? 'true' : 'false'?>);">
                <div id="country-tile-<?= strtolower($country->getCountryCode())?>" class="mediumListIconTextItem <?=($zone->hasCountry($country, FALSE, FALSE)) ? 'selected': '' ?>" data_country_iso="<?=$country->getCountryCode()?>" data_country_name="<?=$country->getPrintableName()?>">

                <img src="<?=DOMAIN_FULL.'/'.ROOT_URI?>ui/images/ui/countries/48/<?= strtolower($country->getCountryCode())?>.png" class="mediumListIconTextItem-Image" data-src="holder.js/60x60" alt="60x60" style="width: 40px; height: 40px; background-color: transparent;">
                    <div class="mediumListIconTextItem-Detail">
                        <h4><?=$country->getPrintableName()?></h4>
                        <h6><?=($country->getPopulation() < 1) ? 'No': $country->getPopulation() ?> Provinces</h6>
                     </div>
            </div>
            </a>

            <?php

            }
            ?>
        </div>
        
        
    </li>





</ul>





<!-- ZONE EDITOR DIALOG BOX -->
<?php
     include_once INC_PATH.'edit-zone-dialog-box.php';

?>


<!-- ADD TO ZONE DIALOG BOX -->


<div id="add-to-zone-dialog-box" class="alert-box">
        
    <h2 class="bubble-title">Add "<span class="dialog-country-name"></span>" to zone</h2>
    
    <div id="remove-country-wrapper">
         <div class="tiny-yellow-text">Click the button to remove this country (<span class="dialog-country-name"></span>) and all it's provinces.</div>
         <br />
         <a class="button-1" href="javascript:void(0);" onClick="zone.removeCountry(); return false;" >
            remove country and all provinces
         </a>
    </div>
    
    <form name="add-to-zone-form" id="add-to-zone-form"  method="POST" action="<?=BASE_URL?>add-to-zone.ajax.php">
        <input type="hidden" name="auth-token" value="<?=AUTH_TOKEN?>"  />
        <input type="hidden" id="add-to-zone-dialog-field-zone-id" name="zone-id" value=<?=$zone->getID()?>  />
        <input type="hidden" id="add-to-zone-dialog-field-country-code" name="country-code" value=0  />
        <input type="hidden" id="add-to-zone-dialog-field-do" name="do" value=""  />

        <p class="bubble-text">
                    <!-- <div class="alert-box-section-header">PROVINCES</div> -->
                    <div class="tiny-yellow-text">To add or remove specific provinces from this zone, choose from the list below then click [ DONE ]</div>
                    <br />
                    <div id="dialog-province-list">

                    </div>  
        </p>

        <br />
        
        <a class="button-1" id="cancel-add-to-zone-button" href="javascript:void(0)" onClick="return false;" >cancel</a>
        &nbsp;&nbsp;
        <input type="submit" class="button-1" id="add-to-zone-button" value="done" />
        <br /><br/>
    </form>
</div>








<div id="footer">
&nbsp;
</div>



</div>

</div>


</body>
</html>
