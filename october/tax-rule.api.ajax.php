<?php



define ('OUTPUT', 'JSON'); // Whether to output as JSON or HTML (especially errors)...


// LOAD APPLICATION TOP...
require_once ('app_top.php');




$WHITE_LIST = array(
						'auth_token',  
                                                'target_id', 
                                                'do', 
                                                'data'
						);





// DIE IF INPUT IS NOT VIA POST METHOD...
if ($_SERVER['REQUEST_METHOD'] != 'POST') _catch_error('POST METHOD ONLY PLEASE. NOW GET.', __LINE__, true);

// SANITIZE INPUT DATA...
$_CLEAN = filter_input_array(INPUT_POST, array(
                                                    'auth_token'		=>	FILTER_SANITIZE_STRIPPED,
                                                    'target_id'	=>	FILTER_VALIDATE_INT, 
                                                    'data'	=>	array( 'filter'=> FILTER_SANITIZE_STRIPPED, 'flags' => FILTER_REQUIRE_ARRAY ), 
                                                    'do'	=>	array('filter'		=>	FILTER_VALIDATE_REGEXP, 
                                                                                                'options'	=>	array('regexp' => '/^[A-Za-z ]{1,50}$/')
                                                                                                                ), 

                                                    )
										
					);


// Check if INPUT keys are ALL expected AND MADE IT THROUGH...
$_suspect = anySuspects();
if ($_suspect !== FALSE) _catch_error('Error processing white_list!' . $_suspect, __LINE__, true);




// VALIDATE AUTH TOKEN...
validate_auth_token ();

// Validate task

$_FUNCS = array (
    'void', 
    'newTaxRule', 
    'saveTaxRule', 
    'deleteTaxRule', 
    'getTaxRule'
    
);





if (!in_array($_CLEAN['do'], $_FUNCS) || !is_callable($_CLEAN['do'])) _catch_error('Invalid action.', __LINE__, true);



    
// OUTPUT...
 $json_reply = array (
                 "status" => 'ok', 
                 "message" => 'Ok'
                 );
 
 

// EXECUTE API CALL

// API FUNC CALL
$_CLEAN['do']();



// HELPER FUNCS


function digestParams () 
{
    global $_CLEAN;
    $_params = $_CLEAN['data'];
    $_bad_params = array ();
    

    $_params['name'] = Cataleya\Helper\Validator::name($_params['name'], 1, 160);
    if ($_params['name'] === FALSE) $_bad_params[] = 'name';

    if (!in_array($_params['type'], array('product', 'shipping'))) $_bad_params[] = 'type';
    
    $_params['rate'] = Cataleya\Helper\Validator::float($_params['rate'], 0, 100);
    if ($_params['rate'] === FALSE) $_bad_params[] = 'rate';
    
    $_params['zone_id'] = Cataleya\Helper\Validator::int($_params['zone_id'], 1, 1000000);
    if ($_params['zone_id'] === FALSE) $_bad_params[] = 'zone_id';
    
    $_params['priority'] = Cataleya\Helper\Validator::int($_params['priority'], 0, 1000000);
    if ($_params['priority'] === FALSE) $_bad_params[] = 'priority';
    
    

    
   
    
    // Check if every one made it through
    if (!empty($_bad_params)) _catch_error('Bad params.', __LINE__, true);
    
    return $_params;
}





function juice (Cataleya\Tax\TaxRule $_TaxRule) 
{
    
    
    $_Zone = $_TaxRule->getTaxZone();

    $zone_info = array (
            'id'    =>  $_Zone->getID(), 
            'name'  =>  $_Zone->getZoneName(), 
            'label'  =>  $_Zone->getZoneName(), 
            'listType'  =>  $_Zone->getListType(),
            'countryPopulation' => $_Zone->getCountryPopulation(), 
            'provincePopulation' => $_Zone->getProvincePopulation()
        );
        
        

    
    $_info = array ( 
            'id'   =>  $_TaxRule->getID(), 
            'type'  =>  $_TaxRule->getType(), 
            'priority'  =>  $_TaxRule->getTaxPriority(), 
            'name'  =>  $_TaxRule->getDescription()->getTitle('EN'), 
            'description'  =>  $_TaxRule->getDescription()->getText('EN'), 
            'rate'  =>  $_TaxRule->getRate(), 
            'Zone'  =>  $zone_info
    );
    
    return $_info;
    
    
}





/*
 * 
 * 
 * CALLBACKS
 * 
 */


function void () {
    
}






/*
 * 
 * [ newTaxRule ]
 * ______________________________________________________
 * 
 * @param: int $_POST['target_id']
 * @param: int $_POST['data']
 * 
 */


function newTaxRule () {

    global $json_reply;
    $_params = digestParams();
    
    // Load Zone
    $_Zone = Cataleya\Geo\Zone::load($_params['zone_id']);
    if ($_Zone === NULL) _catch_error('Zone could not be loaded.', __LINE__, true);
    
    $_TaxRule = Cataleya\Tax\TaxRule::create($_Zone, $_params['type'], $_params['rate'], $_params['name']);
    
    
    $json_reply['message'] = 'New tax rule saved.';
    $json_reply['TaxRule'] = juice($_TaxRule);

};




/*
 * 
 * [ saveTaxRule ]
 * ______________________________________________________
 * 
 * @param: int $_POST['target_id']
 * @param: int $_POST['data']
 * 
 */


function saveTaxRule () {

    global $_CLEAN, $json_reply;
    
    $_params = digestParams();
    
    // Load Zone
    $_Zone = Cataleya\Geo\Zone::load($_params['zone_id']);
    if ($_Zone === NULL) _catch_error('Zone could not be loaded.', __LINE__, true);
    
    $_TaxRule = Cataleya\Tax\TaxRule::load($_CLEAN['target_id']);
    if ($_TaxRule === NULL ) _catch_error('TaxRule could not be found.', __LINE__, true);
    
    // [1] save
    $_TaxRule
    
    ->setRate($_params['rate'])
    ->setTaxPriority($_params['priority'])
    ->setTaxZone($_Zone)
    ->getDescription()
    ->setTitle($_params['name'], 'EN');
    

    
    
    $json_reply['message'] = 'New tax rule saved.';
    $json_reply['TaxRule'] = juice($_TaxRule);

};








/*
 * 
 * [ deleteTaxRule ]
 * ______________________________________________________
 * 
 * @param: int $_POST['target_id']
 * 
 * 
 */


function deleteTaxRule () {

    global $_CLEAN, $json_reply;
 
    $_TaxRule = Cataleya\Tax\TaxRule::load($_CLEAN['target_id']);
    if ($_TaxRule === NULL ) _catch_error('TaxRule could not be found.', __LINE__, true);

    $message = 'Are you sure you want to delete this tax rule: "' . $_TaxRule->getDescription()->getTitle('EN') . '"?';
    confirm($message, FALSE);
    
    $_TaxRule->delete();
    
    $json_reply['message'] = 'TaxRule deleted.';
    $json_reply['TaxRule'] = array ('id'=>$_CLEAN['target_id']);

};






/*
 * 
 * [ getInfo ]
 * ______________________________________________________
 * 
 * @param: int $_POST['target_id']
 * 
 * 
 */


function getTaxRule () {

    global $_CLEAN, $json_reply;
 
    $_TaxRule = Cataleya\Tax\TaxRule::load($_CLEAN['target_id']);
    if ($_TaxRule === NULL ) _catch_error('TaxRule could not be found.', __LINE__, true);
    
    $json_reply['message'] = 'New tax rule saved.';
    $json_reply['TaxRule'] = juice($_TaxRule);

};






// Remove token...
if (isset($_SESSION[SESSION_PREFIX.'CONFIRM_TOKEN'])) unset ($_SESSION[SESSION_PREFIX.'CONFIRM_TOKEN']);




 echo (json_encode($json_reply));
 exit();  

?>