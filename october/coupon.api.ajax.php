<?php



define ('OUTPUT', 'JSON'); // Whether to output as JSON or HTML (especially errors)...


// LOAD APPLICATION TOP...
require_once ('app_top.php');




$WHITE_LIST = array(
						'auth_token',  
                                                'target_id', 
                                                'do', 
                                                'data'
						);





// DIE IF INPUT IS NOT VIA POST METHOD...
if ($_SERVER['REQUEST_METHOD'] != 'POST') _catch_error('POST METHOD ONLY PLEASE. NOW GET.', __LINE__, true);

// SANITIZE INPUT DATA...
$_CLEAN = filter_input_array(INPUT_POST, array(
                                                    'auth_token'		=>	FILTER_SANITIZE_STRIPPED,
                                                    'target_id'	=>	FILTER_VALIDATE_INT, 
                                                    'data'	=>	array( 'filter'=> FILTER_SANITIZE_STRIPPED, 'flags' => FILTER_REQUIRE_ARRAY ), 
                                                    'do'	=>	array('filter'		=>	FILTER_VALIDATE_REGEXP, 
                                                                                                'options'	=>	array('regexp' => '/^[A-Za-z ]{1,50}$/')
                                                                                                                ), 

                                                    )
										
					);


// Check if INPUT keys are ALL expected AND MADE IT THROUGH...
$_suspect = anySuspects();
if ($_suspect !== FALSE) _catch_error('Error processing white_list!' . $_suspect, __LINE__, true);




// VALIDATE AUTH TOKEN...
validate_auth_token ();

// Validate task

$_FUNCS = array (
    'void', 
    'newCoupon', 
    'saveCoupon', 
    'deleteCoupon', 
    'getInfo'
    
);


    
// OUTPUT...
$json_reply = array (
                "status" => 'ok', 
                "message" => 'Ok'
                );



if (!in_array($_CLEAN['do'], $_FUNCS) || !is_callable($_CLEAN['do'])) _catch_error('Invalid action.', __LINE__, true);




// EXECUTE API CALL

// API FUNC CALL
$_CLEAN['do']();



// HELPER FUNCS


function digestParams () 
{
    global $_CLEAN;
    $_params = $_CLEAN['data'];
    $_bad_params = array ();
    
    // [1] Validate code
    $_params['code'] = Cataleya\Helper\Validator::couponCode($_params['code']);
    if ($_params['code'] === FALSE) $_bad_params[] = 'code';
    
    // [2] Discount Type
    // 0: percentage, 1: cash amount
    $_params['discount_type'] = Cataleya\Helper\Validator::int($_params['discount_type'], 0, 1);
    if ($_params['discount_type'] === FALSE) $_bad_params[] = 'discount_type';  
    else {
        $_params['discount_type'] = ($_params['discount_type'] === 0) 
                ? Cataleya\Catalog\Price::TYPE_REDUCTION_PERCENT 
                : Cataleya\Catalog\Price::TYPE_REDUCTION;
    }
    
    // [3] Discount Amount
    $_max = ($_params['discount_type'] !== FALSE && $_params['discount_type'] === Cataleya\Catalog\Price::TYPE_REDUCTION_PERCENT) ? 100 : NULL;
    $_params['discount_amount'] = Cataleya\Helper\Validator::float($_params['discount_amount'], 0, $_max);
    if ($_params['discount_amount'] === FALSE) $_bad_params[] = 'discount_amount';
    
    // [4] Start Date
    $_params['start_day'] = Cataleya\Helper\Validator::int($_params['start_day'], 0, 31);
    $_params['start_month'] = Cataleya\Helper\Validator::int($_params['start_month'], 0, 12);
    $_params['start_year'] = Cataleya\Helper\Validator::int($_params['start_year'], 1900, 3000);
    
    if ($_params['start_day'] === FALSE || $_params['start_month'] === FALSE || $_params['start_year'] === FALSE) $_bad_params[] = 'start_date';
    else $_params['start_date'] = $_params['start_year'].'-'.$_params['start_month'].'-'.$_params['start_day'];
    
    // [5] End Date
    $_params['end_day'] = Cataleya\Helper\Validator::int($_params['end_day'], 0, 31);
    $_params['end_month'] = Cataleya\Helper\Validator::int($_params['end_month'], 0, 12);
    $_params['end_year'] = Cataleya\Helper\Validator::int($_params['end_year'], 1900, 3000);
    
    if ($_params['end_day'] === FALSE || $_params['end_month'] === FALSE || $_params['end_year'] === FALSE) $_bad_params[] = 'end_date';
    else $_params['end_date'] = $_params['end_year'].'-'.$_params['end_month'].'-'.$_params['end_day'];
    
    // [6] End Date
    $_params['valid_until_never'] = Cataleya\Helper\Validator::bool($_params['valid_until_never']);
    if ($_params['valid_until_never'] === NULL) $_bad_params[] = 'valid_until_never';
    
    // [7] Expires After Uses
    $_params['expires_after_uses'] = Cataleya\Helper\Validator::int($_params['expires_after_uses'], 1);
    if ($_params['expires_after_uses'] === FALSE) $_bad_params[] = 'expires_after_uses';
    
    // [8] Enable / Disable
    $_params['is_active'] = Cataleya\Helper\Validator::bool($_params['is_active']);
    if ($_params['is_active'] === NULL) $_bad_params[] = 'is_active';
    
    // [9] Store id
    $_params['store_id'] = Cataleya\Helper\Validator::int($_params['store_id']);
    if ($_params['store_id'] === FALSE) $_bad_params[] = 'store_id';
    
    
    // Check if every one made it through
    if (!empty($_bad_params)) _catch_error('Bad params.' . implode(', ', $_bad_params), __LINE__, true);
    
  
    return $_params;
}






/*
 * 
 * 
 * CALLBACKS
 * 
 */


function void () {
    
}






/*
 * 
 * [ newCoupon ]
 * ______________________________________________________
 * 
 * @param: int $_POST['target_id']
 * @param: int $_POST['data']
 * 
 */


function newCoupon () {

    global $json_reply;
    $_params = digestParams();
    
    $_Store = Cataleya\Store::load($_params['store_id']);
    if ($_Store === NULL ) _catch_error('Store could not be found.', __LINE__, true);
    
    $_Coupon = Cataleya\Sales\Coupon::create($_Store, $_params['code'], $_params);
    
    $json_reply['message'] = 'New coupon saved.';
    $json_reply['Coupon'] = Cataleya\Front\Dashboard\Juicer::juice($_Coupon);

};




/*
 * 
 * [ saveCoupon ]
 * ______________________________________________________
 * 
 * @param: int $_POST['target_id']
 * @param: int $_POST['data']
 * 
 */


function saveCoupon () {

    global $_CLEAN, $json_reply;
    
    $_params = digestParams();
    
    $_Coupon = Cataleya\Sales\Coupon::load($_CLEAN['target_id']);
    if ($_Coupon === NULL ) _catch_error('Coupon could not be found.', __LINE__, true);
    
    $_Store = Cataleya\Store::load($_params['store_id']);
    if ($_Store === NULL ) _catch_error('Store could not be found.', __LINE__, true);
    
    // [1] Coupon code
    $_Coupon->setCode($_params['code'])
            
    // [2] Store
    ->setStore($_Store)
    
    // [3] Discount Type
    ->setDiscountType($_params['discount_type'])
            
    // [4] Discount Amount
    ->setDiscountAmount($_params['discount_amount'])
    
    // [5] Start Date
    ->setStartDate(($_params['valid_from_today']) ? date('Y-m-d') : $_params['start_date'])
    
    // [6] End Date
    ->setEndDate(($_params['valid_until_never']) ? '0000-00-00' : $_params['end_date'])
    
    // [7] Expires After Uses
    ->setUsesPerCoupon($_params['expires_after_uses'], 1);
    
    // [8] Enable  / Disable
    if ($_params['is_active']) $_Coupon->enable ();
    else $_Coupon->disable ();
    
    
    
    $json_reply['message'] = 'Coupon saved.';
    $json_reply['Coupon'] = Cataleya\Front\Dashboard\Juicer::juice($_Coupon);

};





/*
 * 
 * [ deleteCoupon ]
 * ______________________________________________________
 * 
 * @param: int $_POST['target_id']
 * 
 * 
 */


function deleteCoupon () {

    global $_CLEAN, $json_reply;
 
    $_Coupon = Cataleya\Sales\Coupon::load($_CLEAN['target_id']);
    if ($_Coupon === NULL ) _catch_error('Coupon could not be found.', __LINE__, true);
    
    $message = 'Are you sure you want to delete this coupon: "' . $_Coupon->getCode() . '" (' . $_Coupon->getStore()->getDescription()->getTitle('EN') .  ')?';
    confirm($message, FALSE);
    
    $_Coupon->delete();
    
    
    $json_reply['message'] = 'Coupon deleted.';
    $json_reply['Coupon'] = array ('id'=>$_CLEAN['target_id']);

};






/*
 * 
 * [ getInfo ]
 * ______________________________________________________
 * 
 * @param: int $_POST['target_id']
 * 
 * 
 */


function getInfo () {

    global $_CLEAN, $json_reply;
 
    $_Coupon = Cataleya\Sales\Coupon::load($_CLEAN['target_id']);
    if ($_Coupon === NULL ) _catch_error('Coupon could not be found.', __LINE__, true);
    
    $json_reply['message'] = 'Coupon info.';
    $json_reply['Coupon'] = Cataleya\Front\Dashboard\Juicer::juice($_Coupon);
    
};






// Remove token...
if (isset($_SESSION[SESSION_PREFIX.'CONFIRM_TOKEN'])) unset ($_SESSION[SESSION_PREFIX.'CONFIRM_TOKEN']);


 echo (json_encode($json_reply));
 exit();  

?>