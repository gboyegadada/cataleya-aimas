<?php



define ('OUTPUT', 'HTML'); // Whether to output as JSON or HTML (especially errors)...


// LOAD APPLICATION TOP...
require_once ('app_top.php');




/*
 * 
 * CHECK USER PRIVILEGES
 * 
 */


if ((!isset($_admin_role) || !$_admin_role->hasPrivilege('VIEW_STORES')) && !IS_SUPER_USER)    forbidden();


define('READONLY', ($_admin_role->hasPrivilege('EDIT_STORES') || IS_SUPER_USER) ? FALSE : TRUE);






// SANITIZE GET DATA...
$_CLEAN = filter_input_array(INPUT_GET, 
								array(
										'auth_token'		=>	FILTER_SANITIZE_STRIPPED, 
										't'		=>	FILTER_SANITIZE_STRIPPED, 
										'd'	=>	FILTER_SANITIZE_STRING, 
										's'	=>	FILTER_VALIDATE_INT

										)
								);







// IF EDIT
if ($_CLEAN['d'] === 'edit' && $_CLEAN['s'] !== NULL && $_CLEAN['s'] !== false)  {
	// A OK...	
} 

// IF NEW
else if ($_CLEAN['d'] === 'new' && $_CLEAN['s'] === NULL && isset($_SESSION[SESSION_PREFIX.'NEW_TOKEN']) && $_SESSION[SESSION_PREFIX.'NEW_TOKEN'] === $_CLEAN['t'] )  {
	// A OK...
        if (!$_admin_role->hasPrivilege('EDIT_STORES') && !IS_SUPER_USER)    forbidden();
} 

// IF DELETE
else if ($_CLEAN['d'] === 'del' && $_CLEAN['s'] !== NULL && $_CLEAN['s'] !== false)  {
	// A OK...	
        if (!$_admin_role->hasPrivilege('DELETE_STORES') && !IS_SUPER_USER)    forbidden();
} 

// IF NOTHING ADDS UP :D
else {
	// REDIRECT TO CATALOG LANDING
			Header ("Location: " . BASE_URL . "landing.stores.php", true); 
			exit();
}






if ($_CLEAN['d'] == 'new') {
		// reset 'new store' token ...
		unset($_SESSION[SESSION_PREFIX.'NEW_TOKEN']);

                $_Country = Cataleya\Geo\Country::load('NG');
                
                if ($_Country->hasProvinces()) 
                {
                    foreach ($_Country as $_Province) 
                    {
                        $_Contact = Cataleya\Asset\Contact::create($_Province);
                        break;
                    }
                } else {
                    $_Contact = Cataleya\Asset\Contact::create($_Country);
                }
                
                
		$store = Cataleya\Store::create($_Contact, 'Untitled', 'No description', 'EN', 'NGN');

		
} 


// EDIT
else if ($_CLEAN['d'] == 'edit') {
	
		// PRODUCT...
		
		
		
		$store = Cataleya\Store::load($_CLEAN['s']);
                
                if ($store === NULL) {
                	// REDIRECT TO CATALOG LANDING
			Header ("Location: " . BASE_URL . "landing.stores.php", true); 
			exit();                   
                }
		

		
	
}


else {
	
	// REDIRECT TO CATALOG LANDING
			Header ("Location: " . BASE_URL . "landing.stores.php", true); 
			exit();
	
}




// RETRIEVE STORE INFO...
define('STORE_ID', $store->getID());
$store_description = $store->getDescription();
$_DefaultContact = $store->getDefaultContact();
// $store_location = $_DefaultContact->getLocation();


?>




<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<?php
include "includes/meta.php";

?>

<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Edit Store | <?=$store_description->getTitle('EN')?> | Admin</title>


<!-- JS FILES -->
<?php
require_once(INC_PATH.'js_libs.php');
?>

<script type="text/javascript" src="ui/jscript/libs/jquery.form.js"></script>
<script type="text/javascript" src="ui/jscript/libs/jquery.Jcrop.min.js"></script>
<script type="text/javascript" src="ui/jscript/libs/quill.min.js"></script>
<script type="text/javascript" src="ui/jscript/store.edit.js"></script>
<script type="text/javascript" src="ui/jscript/stores.js"></script>


<!-- STYLE SHEETS -->
<?php
require_once(INC_PATH.'css_libs.php');
?>
<link href="ui/css/bootmetro-tiles.css" rel="stylesheet" type="text/css" />
<link href="ui/css/jquery.Jcrop.css" rel="stylesheet" type="text/css" />
<link href="ui/css/quill.base.css" rel="stylesheet" type="text/css" />
<link href="ui/css/quill.snow.css" rel="stylesheet" type="text/css" />
<link href="ui/css/jcrop.canvas.css" rel="stylesheet" type="text/css" />
<link href="ui/css/store.edit.css" rel="stylesheet" type="text/css" />
<link href="ui/css/page.dialog.css" rel="stylesheet" type="text/css" />


<style type="text/css" media="screen">



</style>


</head>

<body>
<!-- BEGIN: META -->
<input type="hidden" id="meta_token" value="<?=AUTH_TOKEN?>"  />
<input type="hidden" id="meta_store_id" value="<?=STORE_ID?>"  />
<input type="hidden" id="meta_admin_root" value="<?=BASE_URL?>"  />


<input type="hidden" id="meta_r_token" value=""  />
<input type="hidden" id="meta_rw_token" value=""  />
<!-- END: META -->



<div class="container">
    
<!-- HEADER -->
<div class="header-wrapper absolute">
    <div class="header">

    <a href="<?=BASE_URL?>landing.stores.php"><div id="page-title">Store</div> </a>
    <?php include_once INC_PATH.'page.nav.php'; ?>   


    </div>
    

<div class="side-bar">
    
</div> 
    
    
<?php include_once INC_PATH.'main.nav.php'; ?>    
  
</div>


    
<div class="content">
<?php include_once INC_PATH.'photo-upload-forms.php'; ?>







<div id="search-tool-wrapper">
		
<?php
define('QUICK_SEARCH_URL', BASE_URL.'store_profile_search.ajax.php');
include INC_PATH.'quick-search-widget.php';
?>


</div>



<!-- ADMIN NAME -->
<h2 id="profile-name-in-header" class="user-status <?=$store->isActive() ? 'online' : 'offline'?>">
    <span class="store-name"><?=$store_description->getTitle('EN')?></span>


</h2>

<div id="hor-line-1">&nbsp;</div>


<div id="page-options-panel">
            
            <?php if (!READONLY && ($_admin_role->hasPrivilege('CHANGE_STORE_STATUS') || IS_SUPER_USER)): ?>
            <a id="store-status-toggle-button" class="button-1" href="javascript:void(0);"  onclick="storeWidget.toggleStoreStatus(); return false;" >
                <?=$store->isActive() ? 'close this store' : 'open this store'?>
            </a>
            <?php endif; ?>

            
            
            <div id="page-description-panel">
                This zone includes all countries except the selected ones. This is suitable for when you're setting up shipping for deliveries outside 
                a specific country. For example, setting up shipping for deliveries everywhere except for the U.K.
            </div>
</div>





<ul id="side-bar">
<li id="store-dp">

    <h3 class="section-header">Display picture</h3>
    <div id="image-picker-wrapper">
         <small class="text-input-label">Photo Upload</small>
         <a href="javascript:void(0);" class="image-picker-add-button-a" onclick="PhotoUploader.initPhotoUpload(storeWidget.saveDisplayPicture, 400, 400); return false;">
             <span class="image-picker-add-button" style="background: url('<?=$store->getDisplayPicture()->getHref(Cataleya\Asset\Image::LARGE)?>') no-repeat center center; background-size: contain;">&nbsp;
             </span>
         </a>     

    </div>
    <br /><br />

</li>

<li>
    <h3 class="section-header">Label Colour</h3>
    
    <!-- <small class="text-input-label">Page Layout</small> -->
    
    <div id="label-color-button-wrapper">
    

    <span class="uni-label">
        <span class="<?= $store->getLabelColor() ?>" data-sync-id="store-label-color">label color</span>
    </span>

    
    <select id="select-label-color" name="store-label-color" data-sync-id="store-label-color" tinyStatus="#store-label-color-status"  onchange="storeWidget.save(this)">
        
    <?php

    $_LabelColors = Cataleya\Store::getLabelColors();
    $_active_color = $store->getLabelColor();

    foreach ($_LabelColors as $_k=>$_LabelColor) 
    {
    ?>
        <option value="<?= $_k ?>" <?= ($_active_color === $_k) ? 'selected="selected"' : '' ?>><?= $_LabelColor ?></option>
            
    <?php } ?>
    </select>
    <small class="tiny-status select-status" id="store-label-color-status">&nbsp;</small>

    
    </div>
    
</li>
    
</ul>



<div id="store-edit-form-outer-wrapper" >
    &nbsp;

    <ul id="store-edit-form-wrapper">

        <li>
            
            <h3 class="section-header">Description</h3>
            <div id="store-description-wrapper">
                
                <small class="text-input-label">Store Name</small>
                <input id="text-field-store-name" class="placeholder-color-1" name="store-name" type="text" value="<?=$store_description->getTitle('EN')?>" placeholder="Store Name"  tinyStatus="#store-name-status"  onchange="storeWidget.save(this)" size=30 />
                <small class="tiny-status" id="store-name-status">&nbsp;</small>
                
                
                <small class="text-input-label">Description</small>
                <textarea id="text-field-store-description" class="placeholder-color-1" name="store-description" rows=6 cols="30" placeholder="Description..."  tinyStatus="#store-description-status"  onchange="storeWidget.save(this)" ><?=$store_description->getText('EN')?></textarea>
                <small class="tiny-status" id="store-description-status">&nbsp;</small>

                <small class="text-input-label">Shop Handle</small>
                <span class="uni-label large">
                <span class="left <?= $store->getLabelColor() ?>" title="<?=$store_description->getTitle('EN')?>" data-sync-id="store-label-color">@</span>
                <span class="right grey status" id="store-handle-status">&nbsp;</span>
                <input class="right grey store-handle-field" name="store-handle" data-sync-id="store-handle" data-store-id="<?= $store->getID() ?>" data-uni-label-status="#store-handle-status" type="text" value="<?= $store->getHandle() ?>" />
                </span>
            </div>
               <div style="height: 20px; width: 1px; clear: both;">&nbsp;</div>        
        </li>
        
        
        

        

        <li>
            
            <h3 class="section-header">Pages</h3>
            
            
            
                        <?php $Pages = Cataleya\Front\Shop\Pages::load($store); ?>

                        <ul class="editable-list" id="pages-editable-list">

                            <li class="editable-list-add-button-wrapper"  >
                                <a href="javascript:void(0);" class="editable-list-add-button icon-plus-alt" onclick="pageWidget.newPage(); return false;" >Click here to add a new page for <?=$store_description->getTitle('EN')?>.</a>
                            </li>

                            <li class="editable-list-tile last-child" style="display:<?= ($Pages->getPopulation() < 1) ? 'block' : 'none' ?>;" >
                            There are no pages for <?=$store_description->getTitle('EN')?> (yet).
                            </li>




                        <?php
                        

                        foreach ($Pages as $_Page) {


                        ?>
                                <li class="editable-list-tile trashable" id="page-<?=$_Page->getID()?>"  trash_type="shipping-option-tile" data_page_id="<?=$_Page->getID()?>"  data_page_title="<?=$_Page->getTitle('EN')?>" >

                                <?=$_Page->getTitle('EN')?> 

                                <a class="button-1 editable-list-edit-button fltrt" href="javascript:void(0)" onclick="pageWidget.editPage(<?=$_Page->getID()?>); return false;" >edit</a>
                                <a class="button-1 editable-list-delete-button fltrt" href="javascript:void(0)" onclick="pageWidget.removePage(<?=$_Page->getID()?>);" >remove</a>

                                </li>

                        <?php

                        }

                        ?>


                        </ul>




            <br /><br />
            <small class="text-input-label">Shop Theme</small>    
            <div id="store-theme-chooser-wrap">
                <select id="select-store-theme" name="store-theme" tinyStatus="#store-theme-status"  onchange="storeWidget.save(this)">
                <?php
                
                $_Themes = Cataleya\Front\Shop\Themes::load();
                
                foreach ($_Themes as $_Theme) 
                {
                ?>
                <option value="<?=$_Theme->getID()?>" <?=($_Theme->getID() == $store->getThemeID()) ? 'selected="selected"' : ''?> ><?=$_Theme->getName()?></option>
                <?php
                }
                
                
                
                ?>
            </select>
            <small class="tiny-status select-status" id="store-theme-status">&nbsp;</small>

            </div>
            
               <div style="height: 20px; width: 1px; clear: both;">&nbsp;</div>        
        </li>
        
        
        
        
        
        
        
        <li>
            
            <h3 class="section-header">Staff</h3>
                        <?php $Staff = Cataleya\Store\Staff::load($store); ?>

                        <ul class="editable-list" id="staff-members-editable-list">

                            <li class="editable-list-add-button-wrapper" >
                                <a href="javascript:void(0);" class="editable-list-add-button icon-plus-alt" onclick="Staff.showDialog(); return false;" >Click here to add a new staff member for <?=$store_description->getTitle('EN')?>.</a>
                            </li>

                            
                            <li class="editable-list-tile last-child" style="display:<?= ($Staff->getPopulation() < 1) ? 'block' : 'none' ?>;" > 
                            There are no staff members for <?=$store_description->getTitle('EN')?> (yet).
                            </li>




                        <?php
                        

                        foreach ($Staff as $StaffMember) {


                        ?>
                                <li class="editable-list-tile trashable" id="staff-member-<?=$StaffMember->getID()?>"  trash_type="staff-member-tile" data_admin_id="<?=$StaffMember->getID()?>"  data_name="<?=$StaffMember->getName()?>" data_fname="<?=$StaffMember->getFirstname()?>" data_lname="<?=$StaffMember->getLastname()?>" >
                                
                                    <span class="user-dp tiny fltlft" ><img src="<?=$StaffMember->getDisplayPicture()->getHref(Cataleya\Asset\Image::THUMB)?>"></span>&nbsp;&nbsp;
                                <?=$StaffMember->getName()?> (<?=$StaffMember->getRole()->getDescription()->getTitle('EN')?>)

                                <a class="button-1 editable-list-delete-button fltrt" href="javascript:void(0)" onclick="Staff.removeStaffMember(<?=$StaffMember->getID()?>);" >remove</a>

                                </li>

                        <?php

                        }

                        ?>


                        </ul>



               <div style="height: 20px; width: 1px; clear: both;">&nbsp;</div>        
        </li>
        


        
        
       <li>
            
            <h3 class="section-header">Contact Person</h3>
            <div class="tiny-yellow-text">
                This information will be used whenever there is a need for a customer to speak to someone at the store regarding a product order or purchase.
                
            </div>
            <br />
            <div class="dotted-yellow-hor-line tiny-yellow-arrow-down">&nbsp;</div>
            <br />
            <div id="store-contact-wrapper">
                <form name="store-contact-form">
                <small class="text-input-label">Contact Person</small>
                <input id="text-field-contact-name" name="contact-name" class="placeholder-color-1" type="text" value="<?=$_DefaultContact->getEntryName()?>" placeholder="Contact Name" tinyStatus="#store-contact-name-status"  onchange="storeWidget.save(this)" size=30 />
                <small class="tiny-status" id="store-contact-name-status">&nbsp;</small>
                <br/>
                <small class="text-input-label">Telephone</small>
                <input id="text-field-contact-phone" name="contact-phone" class="placeholder-color-1" type="text" value="<?=$_DefaultContact->getEntryWorkPhone()?>" placeholder="Telephone" tinyStatus="#store-contact-phone-status"  onchange="storeWidget.save(this)" onkeypress="return checkPhoneDigit(event, this);" onkeyup="return recheckPhoneDigit(event, this);"  size=30 />
                <small class="tiny-status" id="store-contact-phone-status">&nbsp;</small>
                <br/>
                <small class="text-input-label">Email</small>
                <input id="text-field-contact-email" name="contact-email" class="placeholder-color-1" type="text" value="<?=$_DefaultContact->getEntryEmail()?>" placeholder="Email"  tip="Enter contact's <strong>email address</strong> <small></small>" tinyStatus="#store-contact-email-status"  onchange="storeWidget.save(this)" size=30 />
                <small class="tiny-status" id="store-contact-email-status">&nbsp;</small>
                <br/> 
                </form>
                
            </div>   
            <br /><br />
            
        </li>
        
        
        
       <li>
            
            <h3 class="section-header">Store Location</h3>
            <div id="store-location-wrapper">
                <form name="store-location-form">
                <small class="text-input-label">Street Address</small>
                <input id="text-field-street-address" name="street-address" class="placeholder-color-1" type="text" value="<?=$_DefaultContact->getEntryStreetAddress()?>" tinyStatus="#store-street-address-status"  onchange="storeWidget.save(this)" size=30 />
                <small class="tiny-status" id="store-street-address-status">&nbsp;</small>
                <br/>
                
<!--                
                <small class="text-input-label">Address Line 2</small>
                <input id="text-field-suburb" name="suburb" class="placeholder-color-1" type="text" value="<?=$_DefaultContact->getEntrySuburb()?>" tinyStatus="#store-suburb-status"  onchange="saveStoreAddress(this)" size=30 />
                <small class="tiny-status" id="store-suburb-status">&nbsp;</small>
                <br/>
-->                
                
                
                
                <small class="text-input-label">City</small>
                <input id="text-field-city" name="city" class="placeholder-color-1" type="text" value="<?=$_DefaultContact->getEntryCity()?>" tinyStatus="#store-city-status"  onchange="storeWidget.save(this)" />
                <small class="tiny-status" id="store-city-status">&nbsp;</small>
                <br/>
                <small class="text-input-label">State / Province</small>
                <input id="text-field-state" name="state" class="placeholder-color-1" type="text" value="<?=$_DefaultContact->getEntryState()?>" tinyStatus="#store-state-status"  onchange="storeWidget.save(this)" size=30 />
                <small class="tiny-status" id="store-state-status">&nbsp;</small>
                <br/> 
                 <small class="text-input-label">Postal Code</small>
                <input id="text-field-postal-code" name="postcode" class="placeholder-color-1" type="text" value="<?=$_DefaultContact->getEntryPostcode()?>" tinyStatus="#store-postcode-status"  onchange="storeWidget.save(this)" size=30 />
                <small class="tiny-status" id="store-postcode-status">&nbsp;</small>
                <br/> 
                <small class="text-input-label">Country</small>
                <select id="select-store-country" name="country" tinyStatus="#store-country-status"  onchange="storeWidget.save(this)" size=1 />
                
                <?php
                $countries = Cataleya\Geo\Countries::load();
                
                foreach ($countries as $country)
                {
                ?>
                    <option value="<?=$country->getCountryCode()?>" <?=($country->getCountryCode() == $_DefaultContact->getEntryCountryCode()) ? 'selected="selected"' : ''?> ><?=$country->getPrintableName()?></option>
                <?php
                
                }
                ?>
                    
                </select> 
                <small class="tiny-status select-status" id="store-country-status">&nbsp;</small>
                <br/> 
                </form>
                
            </div>   
            <br /><br />

        </li> 
        
        
        
       <li>
            
            <h3 class="section-header">Settings</h3>
            <div id="store-settings-wrapper">
            
            
            <br />
            <small class="text-input-label">Language</small>
            <select id="select-store-language" DISABLED name="store-language" tinyStatus="#store-language-status"  onchange="storeWidget.save(this)">
               
                <?php
                $languages = new Cataleya\Locale\Languages();
                
                foreach ($languages as $language)
                {
                ?>
                <option value="<?=$language->getLanguageCode()?>" <?=($language->getLanguageCode() == $store->getLanguageCode()) ? 'selected="selected"' : ''?> ><?=$language->getName()?></option>
                <?php
                
                }
                ?>  
                
            </select>
            <small class="tiny-status select-status" id="store-language-status">&nbsp;</small>
            <br />
            <small class="text-input-label">Currency</small>
            <select id="select-store-currency" name="store-currency" tinyStatus="#store-currency-status"  onchange="storeWidget.save(this)"> 
                
                <?php
                $currencies = new Cataleya\Locale\Currencies();
                
                foreach ($currencies as $currency)
                {
                 if ((int)$currency->isActive() === 0) continue;
                 
                ?>
                <option value="<?=$currency->getCurrencyCode()?>" <?=($currency->getCurrencyCode() == $store->getCurrencyCode()) ? 'selected="selected"' : ''?> ><?=$currency->getCurrencyName()?></option>
                <?php
                
                }
                ?>  
                
            </select> 
            <small class="tiny-status select-status" id="store-currency-status select-status">&nbsp;</small>
          
            
            
            
            <br />
            <small class="text-input-label">Time Zone</small>
            <select id="select-store-timezone" name="store-timezone" tinyStatus="#store-timezone-status"  onchange="storeWidget.save(this)"> 
                
                <?php
                $_TimeZones = Cataleya\Helper::getTimeZones();
                $_StoreTimeZone = $store->getTimezone()->getName();
                
                foreach ($_TimeZones as $k=>$v)
                {

                ?>
                <option value="<?=$v?>" <?=($v == $_StoreTimeZone) ? 'selected="selected"' : ''?> ><?=$k?></option>
                <?php
                
                }
                ?>  
                
            </select> 
            <small class="tiny-status select-status" id="store-timezone-status select-status">&nbsp;</small>
            
            </div>
            <div style="height: 40px; width: 1px; clear: both;">&nbsp;</div> 
           
            
        </li>
        
        
       <li>
            
            <h3 class="section-header">Payment Processors</h3>
            <div class="tiny-yellow-text">
                To make a payment option available for this store 'check' it. For example, if this store is for shoppers in Nigeria make sure 'WebPay' and 'eTranzact' are checked.
                
            </div>
            <br />
            <div class="dotted-yellow-hor-line tiny-yellow-arrow-down">&nbsp;</div>
            <br />
            
<?php

   $_payment_options = \Cataleya\Plugins\Package::getAppList('payment');

   foreach ($_payment_options as $_handle) 
   {
    if (!\Cataleya\Plugins\Package::isOn($_handle)) continue;

    $css_id2 = 'payment-type-'.$_handle;
    $_name = \Cataleya\Plugins\Package::getAppName($_handle);

?>

            <a href="javascript:void(0);" id="<?=$css_id2?>-anchor" class="payment-type-anchor <?=($store->hasPaymentProcessor($_handle)) ? 'icon-checkbox-partial' : 'icon-checkbox-unchecked'?>" onclick="storeWidget.toggleAcceptedPayment('<?=$_handle?>'); return false;"><?=$_name?></a>
                <small class="tiny-status" id="<?=$css_id2?>-status">&nbsp;</small>
                <input type="hidden" id="<?=$css_id2?>-input" name="payment-types[<?=$_handle?>]" value="-1" />
                <br/>
<?php

}
       
?>

            <div style="height: 40px; width: 1px; clear: both;">&nbsp;</div> 
            
        </li>
        
        
        
        
        
        
        
        <li>
            
            <h3 class="section-header">Shipping Options</h3>
                        <?php $ShippingOptions = Cataleya\Shipping\ShippingOptions::load($store); ?>

                        <ul class="editable-list" id="shipping-options-editable-list">

                            <li class="editable-list-add-button-wrapper"  >
                                <a href="javascript:void(0);" class="editable-list-add-button icon-plus-alt" onclick="ShippingOptions.newShippingOption(); return false;" >Click here to add a new shipping option for <?=$store_description->getTitle('EN')?>.</a>
                            </li>

                            <li class="editable-list-tile last-child" style="display:<?= ($ShippingOptions->getPopulation() < 1) ? 'block' : 'none' ?>;" >
                            There are no shipping options for <?=$store_description->getTitle('EN')?> (yet).
                            </li>




                        <?php
                        

                        foreach ($ShippingOptions as $ShippingOption) {

                            $ShippingOptionDescr = $ShippingOption->getDescription();
                            $carrier = $ShippingOption->getCarrier();

                        ?>
                                <li class="editable-list-tile trashable" id="shipping-option-<?=$ShippingOption->getID()?>"  trash_type="shipping-option-tile" data_shipping_option_id="<?=$ShippingOption->getID()?>"  data_shipping_option_name="<?=$ShippingOptionDescr->getTitle('EN')?>" >

                                <?=$ShippingOptionDescr->getTitle('EN')?> 

                                <a class="button-1 editable-list-edit-button fltrt" href="javascript:void(0)" onclick="ShippingOptions.editShippingOption(<?=$ShippingOption->getID()?>); return false;" >edit</a>
                                <a class="button-1 editable-list-delete-button fltrt" href="javascript:void(0)" onclick="ShippingOptions.removeShippingOption(<?=$ShippingOption->getID()?>);" >remove</a>

                                </li>

                        <?php

                        }

                        ?>


                        </ul>



               <div style="height: 20px; width: 1px; clear: both;">&nbsp;</div>        
        </li>
        

        
       
        <?php if (!READONLY && ($_admin_role->hasPrivilege('DELETE_STORES') || IS_SUPER_USER)): ?>        
        <li>
            <h3 class="section-header">Delete Store</h3>
            <div class="tiny-yellow-text">
                WARNING: <br /><br />
                Deleting this store (<?=$store_description->getTitle('EN')?>) will remove ALL product pricing, stock information and shipping options you have setup for '<?=$store_description->getTitle('EN')?>'. 
            </div>
            <br />
            <div class="dotted-yellow-hor-line tiny-yellow-arrow-down">&nbsp;</div>
            <br />
            <br/>
            <a class="button-1" href="javascript:void(0);" onClick="deleteStore(); return false;" >
                Delete this store
            </a>
            <br/>
            <br />
            
        </li>
        <?php endif; ?>
                
    </ul>



    
</div>
    


<?php
     include_once INC_PATH.'edit-shipping-option-dialog.php';

?>



</div>
</div>


</body>
</html>
