<?php
/*
SEARCH ORDERS AND RETURN RESULTS AS JSON...
*/


define ('OUTPUT', 'JSON'); // Whether to output as JSON or HTML (especially errors)...


// LOAD APPLICATION TOP...
require_once ('app_top.php');

/*
 * 
 * CHECK USER PRIVILEGES
 * 
 */


if ((!isset($_admin_role) || !$_admin_role->hasPrivilege('VIEW_ORDERS')) && !IS_SUPER_USER) forbidden();



$WHITE_LIST = array(
						'term'
					);



// RETRIEVE DATA ONLY IF VIA POST METHOD...
if ($_SERVER['REQUEST_METHOD'] != 'GET') _catch_error('GET METHOD ONLY PLEASE. NOW GET.', __LINE__, true);



// SANITIZE INPUT DATA...
$_CLEAN = filter_input_array(INPUT_GET, 
								array(
										'term'		=>	FILTER_SANITIZE_STRING

										)
								);




// Check if INPUT keys are ALL expected AND MADE IT THROUGH...
if (anySuspects()) 
{


    // Output...
    $json_data = array (
                    'status' => 'Ok', 
                    'message' => 'No orders found.', 
                    'Orders' => array(), 
                    'count' => 0
                    );
    echo json_encode($json_data);
    exit();

    
}







// First, take out funny looking characters...
$_CLEAN['term'] = preg_replace('/[^-\s\.A-Za-z0-9@*#+%]+/', '', $_CLEAN['term']);




// Load search results as a collection of objects...
$search_results = Cataleya\Store\Orders::search($_CLEAN['term']);


$orders_info = array ();

foreach ($search_results as $_Order) 
{
    $orders_info[] = \Cataleya\Front\Dashboard\Juicer::juice($_Order);
    
}



// Output...
$json_data = array (
		'status' => 'Ok', 
		'message' => Cataleya\Helper::countInEnglish($search_results->getPopulation(), 'Order', 'Orders') . ' found.', 
		'Orders' => $orders_info, 
		'count' => $search_results->getPopulation()
		);
echo json_encode($json_data);
exit();




?>