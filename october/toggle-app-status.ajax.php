<?php



define ('OUTPUT', 'JSON'); // Whether to output as JSON or HTML (especially errors)...


// LOAD APPLICATION TOP...
require_once ('app_top.php');







// ENV...
$WHITE_LIST = array(
						'auth_token', 
                                                'active', 
                                                
                                                // expected values (a work-around for type defs)
                                                'active'    =>  TRUE
						);




// DIE IF INPUT IS NOT VIA POST METHOD...
if ($_SERVER['REQUEST_METHOD'] != 'POST') _catch_error('POST METHOD ONLY PLEASE. NOW GET.', __LINE__, true);

// SANITIZE INPUT DATA...
$_CLEAN = filter_input_array(INPUT_POST, array(
                                                    'auth_token'		=>	FILTER_SANITIZE_STRIPPED, 
                                                    'active'	=>	array('filter'		=>	FILTER_VALIDATE_BOOLEAN, 
                                                                                    'flags' => FILTER_NULL_ON_FAILURE
                                                                                                    )

                                                    )
										
					);


// Check if INPUT keys are ALL expected AND MADE IT THROUGH...
$_suspect = anySuspects();
if ($_suspect !== FALSE) _catch_error('Error processing white_list!', __LINE__, true);



// VALIDATE AUTH TOKEN...
validate_auth_token ();



$App = Cataleya\System::load();





// Confirm delete action (and require password)
$message = 'To ' . (($_CLEAN['active']) ? 'turn off' : 'turn on') . ' Cataleya please confirm your password...';
confirm($message, TRUE);

// Disable or enable Cataleya (if we make it past the 'confirm' function)...
if ($_CLEAN['active'] === TRUE) $App->activate();
else if ($_CLEAN['active'] === FALSE) $App->shutdown();



// OUTPUT...
 $json_reply = array (
                 "status" => 'ok', 
                 "message" => 'Cataleya is now ' . (($_CLEAN['active']) ? 'on.' : 'off.'), 
                 "App"    =>  array ("isActive"=>$App->isActive())
                 );


 echo (json_encode($json_reply));
 exit();  
