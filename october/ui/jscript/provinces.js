






$('document').ready(function () {
//    $('#countries-grid-wrapper').on('click', '.mediumListIconTextItem', function(e){
//       e.preventDefault();
//       $(this).toggleClass('selected');
//    });

$('.province-tile').mouseenter(function () {
        $(this).find('.button-1').fadeIn('fast');
    })
    .mouseleave(function () {
        $(this).find('.button-1').hide();
    });


$('#edit-province-form').submit(function () {
    
    // Validate form
    var isoField = $('#text-field-province-iso');
    var nameField = $('#text-field-province-name');
    var province_id = $('#province-id').val();
    
    
    if (!PROVINCE_ISO_REGEXP.test(isoField.val()) || (provinces.exists(isoField.val()) && province_id == 0)) {
        showTip(isoField);
        isoField
        .focus()
        .one('blur', function () { hideTip(); });
        
        return false;
    }
    if (NAME_REGEXP.test(nameField.val()) || nameField.val() == '') {
        showTip(nameField);
        nameField
        .focus()
        .one('blur', function () { hideTip(); });
        
        return false;
    }
    
    $('#edit-province-dialog-box').hide();
    hold('Adding new province...');
    
    ajax.jsonRequest ({
            method: 'POST', 
            url: $('#edit-province-form').prop('action'), 
            data: $('#edit-province-form').serialize(), 
            dataType: 'json', 
            success: function (response) {
                            
                            var data = response.json();
                        
                      // In case we're modifing an existing province, remove from list first...
                      $('#province-'+data.province.id).detach();
                      
                      // Then add new tile...
                      var new_tile = $('<li class="province-tile trashable" id="province-' + data.province.id + '" trash_type="province-tile"  data_province_id="' + data.province.id + 
                           '"  data_province_name="' + data.province.name + '" data_province_iso_code="' + data.province.iso + '" >' + 
                           data.province.name + ' (' + data.province.iso + ')' +
                           '<a class="button-1 province-edit-button fltrt" href="javascript:void(0)" onclick="editProvince(' + data.province.id + '); return false;" >edit</a>' + 
                            '<a class="button-1 province-delete-button fltrt" href="javascript:void(0)" onclick="provinces.removeProvince(' + data.province.id + ');" >remove</a></li>'
                            );

                             
//                       new_tile.css('cursor', openHandCursor).draggable({
//                                                        revert: 'invalid', 
//                                                        revertDuration: 200,
//                                                        appendTo: "body", 
//                                                        containment: 'document',  
//                                                        opacity: 0.5, 
//                                                        zIndex: 2700, 
//                                                        helper: "clone"
//                                                        });
                                                        
                       new_tile.mouseenter(function () {
                                    $(this).find('.button-1').fadeIn('fast');
                                })
                                .mouseleave(function () {
                                    $(this).find('.button-1').hide();
                                });
                                                        
                          
                      // Insert new size tile...
                       $('#province-list').append(new_tile);
                       
                       // Set province count
                        var provinceCount1 = (data.provinceCount == 1) ? 'is ' : 'are ';
                        provinceCount1 += countInEnglish(data.provinceCount, 'province', 'provinces');
                        
                        var provinceCount2 = (data.provinceCount > 0) ? '(' + data.provinceCount + ')' : '';
                        
                        $('.province-count-1').html(provinceCount1);
                        $('.province-count-2').html(provinceCount2);
                       
                       // Reset form fields
                       $('#text-field-province-iso, #text-field-province-name').val('');
                       
                       // Update cache, then bring it back
                       provinces.refresh(release);
                       
                       
                       
                        return false;

                    },

            error: function (xhr) {
                return false;
            }
    });
    return false;
});




// Construct...
provinces = Cataleya.loadProvinces($('#meta_country_code').val());


});












/*
* 
* 
* MISC FUNCTIONS
* 
* 
*/





// CHECK FLOAT NUMBER ENTRY
function validateISO (e, box) {
	
	var chrTyped, chrCode=0, evt= e ? e : event;
	 if (evt.charCode!=null)     chrCode = evt.charCode;
	 else if (evt.which!=null)   chrCode = evt.which;
	 else if (evt.keyCode!=null) chrCode = evt.keyCode;
	
	 if (chrCode==0) chrTyped = 'SPECIAL KEY';
	 else chrTyped = String.fromCharCode(chrCode);

		
	 //Digits, special keys & backspace [\b] work as usual:
	 if (chrTyped.match(/[A-Za-z]|[\b]|SPECIAL/)) return true;
	 if (evt.altKey || evt.ctrlKey || chrCode<28) return true;
        
        
	revalidateISO (e, box);
        
        return true;
}


function revalidateISO (e, box) {
	evt= e ? e : event;
	if (/[^A-Za-z]/.test(box.value)) {
			box.value = box.value.replace(/[^A-Za-z]/, '');
			if (evt.preventDefault) {evt.preventDefault(); } else {return false; }
			evt.returnValue=false;
	}
        
        // check if iso isn't already taken
        //if (provinces.exists(box.value)) $(box).addClass('illegal');
        //else $(box).removeClass('illegal');
        
        return true;

}










/*
 * 
 *  [ editProvince ]
 *  
 */

function editProvince (province_id) {
    
    
    // Remove previous click events...
    $('#edit-province-dialog-box #edit-province-button-cancel, #fullscreen-bg2').unbind('click');
    
    var dialogBox = $('#edit-province-dialog-box');
    
    // Set up dialog box...
    if (province_id == 0) {
        
        // NEW
        dialogBox.find('.bubble-title').html('New Province');
        $('#province-id').val(0);
        $('#text-field-province-iso, #text-field-province-name').val('');
        $('#edit-province-button-ok').val('add new province');

    } else {
        
        // EDIT
        dialogBox.find('.bubble-title').html('Edit Province');
        $('#province-id').val(province_id);

        var province = provinces.getProvince(province_id);
        $('#text-field-province-iso').val(province.iso);
        $('#text-field-province-name').val(province.printableName);
        $('#edit-province-button-ok').val('save changes');
    }
    
    // Init dialog 'cancel' button...
    $('#edit-province-dialog-box #edit-province-button-cancel, #fullscreen-bg2').click(function () {
            $('#edit-province-dialog-box, #fullscreen-bg2').fadeOut(300);
            return false;
    });
    
    // Show dialog box...
    $('#edit-province-dialog-box, #fullscreen-bg2').show();
    
    
}