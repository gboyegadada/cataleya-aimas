// JavaScript Document
var Validator, Customer;

$('document').ready(function() {
	
/*	
	$('#new-customer-dob').datepicker({ 
							defaultDate: +1, 
							dateFormat: 'mm/dd/yy', 
							changeMonth: false, 
							changeYear: true
							
							});
							
	
*/	
	

	// NEW CUSTOMER SUBMIT BIND
	$('#new-customer-form').submit(function () {
		
		new_customer();
		return false;
		
	});
	
	// NEW CUSTOMER CANCEL BUTTON BIND
	$('#new-customer-cancel-button').click(function () {
		
		$('#new-customer-link-wrapper').show(); 
                $('#new-customer-form-wrapper').fadeOut(200);
                hideTip();
                
		return false;
		
	});	
		
	
        Validator = Cataleya.newValidator();
        
        // Password meter
        Validator.newPasswordMeter('new-customer-password', 'password-meter-1');


});



function show_new_customer_form () {
    
             $('#fullscreen-bg2, #new-customer-form-wrapper').fadeIn(200);
             $('#fullscreen-bg2, #new-customer-form-wrapper .cancel-button').unbind('click').click(function () {
                
                hideTip(true);
                
                $('#new-customer-form-wrapper').fadeOut(200);
                $('#fullscreen-bg2').unbind('click').hide();
                $(document).unbind('scroll'); 
 		$('body').css({'overflow':'visible'});	

             })
             .show();
}

function new_customer() {

                // Validate form
                var failedItems = Validator.validateForm('new-customer-form');

                if (failedItems.length > 0) {
                    showTip(failedItems[0]);
                    $(failedItems[0])
                    .focus()
                    .one('blur', function () { hideTip(); });

                    return false;
                }
                
//                alert('Passed!');
//                return false;


		ajax.jsonRequest ({
		method: 'POST', 
		url: './new_customer.ajax.php', 
		data: $('#new-customer-form').serialize(), 
		dataType: 'json', 
		success: function(response) {
                            
                            var data = response.json();

                            $('#new-customer-link-wrapper').show(); 
                            $('#new-customer-form-wrapper').fadeOut(200);
                            $('#new-customer-form')[0].reset()	


                            prettyAlert('A new customer account has been created for ' + data.Customer.name + '...');

                            window.location = 'customer_profile.customers.php?id=' + data.Customer.id;
                            return false;
			
			
			}, 
		error: function (xhr) {
                    $('#new-customer-link-wrapper').show(); 
                    $('#new-customer-form-wrapper').fadeOut(200);
                    return false;
                }
		
		});

return false;		
}





    window.quickSearchCallBack = function (response) 
    {

            var data = response.json();
    
	$('#quick-search-results').html('');
        
        if (data.count > 0) {
                for (i in data.customers) {
                        var details = (data.customers[i].telephone == '') ? '' : '<small class="quick-search-result-detail icon-phone-2">' + data.customers[i].telephone + '</small>';
                        details += (data.customers[i].email_address == '') ? '' : '<small class="quick-search-result-detail icon-email">' + data.customers[i].email_address + '</small>';
                        var online_status = (data.customers[i].isOnline) ? 'online' : 'offline';
                        
                        $('#quick-search-results').prepend('<a href="' + data.customers[i].url + '"><li class="user-status ' + online_status + '">' + data.customers[i].firstname + ' ' + data.customers[i].lastname + '<br />' + details + '</li></a>');
                }

        $('#quick-search-results').show();

        } else {$('#quick-search-results').hide();	}
}