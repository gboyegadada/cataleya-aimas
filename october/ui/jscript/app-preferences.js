

$('document').ready(function () {
    

       

});


/*
 * 
 * [ FUNCTION ] saveAppPreference 
 * ____________________________________________________________________________
 * 
 * Self explanatory.
 * 
 */



function saveAppPreference(options) {
        

	var tinyStatusSelector = options.tinyStatus;
        $(tinyStatusSelector).html('Saving...').toggleClass('blink');

        var query = {
            auth_token: $('#meta_token').val(),
            who: options.who, 
            what: options.what
        };

		
		
	var action = 'save-app-preference.ajax.php';
	
	ajax.jsonRequest ({
		method: 'POST', 
		url: action, 
		data: query, 
		dataType: 'json', 
                tinyStatus: tinyStatusSelector, 
		success: function (response) {
                            
                        var data = response.json();
                        
                        options.success(data);
			return false;
                        
			},
			
		error: function (xhr) {
                        options.error(xhr);
                        return false;
                }
        });
        
        return false;
	
	
}







/*
 * 
 * [ FUNCTION ] setTaxClass
 * ____________________________________________________________________________
 * 
 * Self explanatory.
 * 
 */



function setTaxClass (control) {
        var options = {
          who: $(control).prop('name'), 
          what: $(control).val(), 
          tinyStatus: $(control).attr('tinyStatus'), 
          success: function () {
              
          }, 
          error: function () {


          }
        };
        

	
        saveAppPreference(options);
	
	
	return false;
    
}







/*
 * 
 * [ FUNCTION ] toggle 
 * ____________________________________________________________________________
 * 
 * 
 * 
 */



function toggle (control) {
        
        var options = {
          who: $(control).attr('who'), 
          what: 0, 
          tinyStatus: $(control).attr('tinyStatus'), 
          success: function () {
              
          }, 
          error: function () {
                if ($(control).hasClass('icon-checkbox-unchecked')) $(control).removeClass('icon-checkbox-unchecked').addClass('icon-checkbox-partial');
                else $(control).removeClass('icon-checkbox-partial').addClass('icon-checkbox-unchecked');
          }
        };
        
        
        
        if ($(control).hasClass('icon-checkbox-unchecked')) {
            options.what = 1;
            $(control).removeClass('icon-checkbox-unchecked').addClass('icon-checkbox-partial');
        } else {
            options.what = 0;
            $(control).removeClass('icon-checkbox-partial').addClass('icon-checkbox-unchecked');
        }
        
	
        saveAppPreference(options);
	
	
	return false;
}





function saveControl (control) {
        
        control = $(control);
        
        var options = {
          who: control.prop('name'), 
          what: control.val(), 
          tinyStatus: control.attr('tinyStatus'), 
          success: function (data) {
              
          }, 
          error: function (xhr) {
                alert('Error', xhr.responseText);
          }
        };
        
        
        
        saveAppPreference(options);
	
	
	return false;
}






/*
 * 
 * [ FUNCTION ] toggleAppStatus
 * ____________________________________________________________________________
 * 
 * Self explanatory.
 * 
 */



function toggleAppStatus () {
        
        var query = {
            active: ($('#profile-name-in-header').hasClass('offline')) ? true : false

        };
        
	
	ajax.doAPICall ({
                api: 'system', 
                'do': 'toggleAppStatus', 
                data: query, 
		success: function (data) {
                    
                    console.log(data);
                    
                                alert ('Ok', data.message, true);
                                
                                if (data.App.isActive && $('#profile-name-in-header').hasClass('offline')) { 
                                    $('#profile-name-in-header').removeClass('offline').addClass('online');
                                    $('#status-toggle-button').html('turn application off');
                                }
                                else if (!data.App.isActive && $('#profile-name-in-header').hasClass('online')) { 
                                    $('#profile-name-in-header').removeClass('online').addClass('offline');
                                    $('#status-toggle-button').html('turn application on');
                                }
                                return false;
                        
			},
			
		error: function (xhr) {
                    console.log(xhr);
                    return false;
                }
        });
        
        return false; 
    
}

