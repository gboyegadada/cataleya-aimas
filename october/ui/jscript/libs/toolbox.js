var init = Array ();
var errorCallBackCue = Array ();
window.confirmToken = '';
window.whisper = '';
window.mainMenuTimeout = null;
window.pageMenuTimeout = null;
window.mainMenuHoverTimeout = null;





/*
 *
 * APPEND DIALOG BOXES
 * 
 */
 
 
 $('document').ready(function (){
    
    // Alert Bubble
    $('body')
    .prepend('<div id="alert-box" class="alert-box" style="display:none; position:fixed; top:140px; left:490px; width:300px; height:auto; background-color:#111; color:#bbb; z-index:10051;">&nbsp;</div>')    
    
    // Tip Bubble (used in form validation)
    .prepend('<div id="bubble-1" onclick="hideTip()" class="message-bubble message-bubble-above"><span class="bubble-arrow">&nbsp;</span><p></p>&nbsp;</div>')
   
    // Confirm Bubble
    .prepend('<div id="confirm-box" class="alert-box" style="display:none; position:fixed; top:140px; left:490px; width:300px; height:auto; background-color:#111; color:#bbb; z-index:10202;"><h2 class="bubble-title">Hi,</h2><p class="bubble-text"></p><a class="button-1" id="confirm-button-ok" href="#" onClick="return false;" >confirm</a>&nbsp;&nbsp;<a class="button-1" id="confirm-button-cancel" href="#" onClick="return false;" >cancel</a><small class="bubble-tiny-text">click anywhere to close X</small></div>')

    // Tranlucent background
    .prepend('<div id="fullscreen-bg2" style="display:none; position:fixed; top:0px; left:0px; height:100%; width:100%; background-color:rgba(0, 0, 0, .3); z-index:10000;">')
    
    // Tranlucent background
    .prepend('<div id="fullscreen-bg3" style="display:none; position:fixed; top:0px; left:0px; height:100%; width:100%; background-color:rgba(0, 0, 0, .3); z-index:10000;">')
    
    
    // Tranlucent background (for 'hold')
    .prepend('<div id="fullscreen-hold-bg" style="display:none; position:fixed; top:0px; left:0px; height:100%; width:100%; background-color:rgba(0, 0, 0, .3); z-index:10200;">');

    window.alert = prettyAlert;
    window.originalTipBoxZIndex = $('#bubble-1').css('z-index');
    
    // Initialize main nav
    $('#main-nav-button')
    .click(function () {
        return false;
    })
    .mouseover(function () {
        clearTimeout (window.mainMenuTimeout, window.mainMenuHoverTimeout);
        window.mainMenuHoverTimeout = setTimeout (function () {
                    showMenu('#main-nav', '#main-nav-button');
                }, 300);
    })
    .mouseout(function () {
           clearTimeout(window.mainMenuHoverTimeout);
           window.mainMenuTimeout = setTimeout(function () {
           hideMenu('#main-nav', '#main-nav-button');
            
        }, 300);
    });
    
    $('#main-nav').mouseover(function () {
        clearTimeout (window.mainMenuTimeout);
    })
    .mouseout(function () {
       window.mainMenuTimeout = setTimeout(function () {
           hideMenu('#main-nav', '#main-nav-button');
       }, 300);
    });
    
    
    var _lfp = $('#lfp-'+$('#login_token').val());
    var _lform = $('#lform-'+$('#login_token').val());
    if (_lfp.length > 0 && _lform.length > 0) { 
        
        _lform.submit (function () {
            _lfp.val($.jFingerprint.getHash());
            return true;
        });
        
    }







});
 





function ge (elmt)
{
	if (window.all)
	{
	return window.all[elmt];
	} else {
	return document.getElementById(elmt);
	}


}




function hideMenu (menu, menuButton)
{
    
    $(menu).fadeOut(300); 
    $('#bread-crumbs').fadeIn('fast');
    $(menuButton).removeClass('on');
    
    return false;
}


function showMenu (menu, menuButton)
{
    $(menu).slideDown('fast'); 
    $('#bread-crumbs').fadeOut('fast');
    $(menuButton).addClass('on');
    
    return false;
}
 
 
 

/*

CUSTOM ALERT ...


*/




function prettyAlert (title, message, autoHide) {
    
        release(); // In case user was previoulsy on hold...
        
        var autoHideTimeout;
        if (typeof autoHide == 'boolean' && autoHide == true) autoHideTimeout = setTimeout(function () { $('#alert-box').click(); }, 3000); 
        
        
        var cancelAlert = function cancelAlert () {

                clearTimeout(autoHideTimeout);
                
		$('#alert-box, #fullscreen-bg2').fadeOut(300);
		$('#alert-box, #fullscreen-bg2').unbind('click');
                $(document).unbind('scroll'); 
 		$('body').css({'overflow':'visible'});	


                MyShortcuts.clear('cancel', cancelAlert);
        };
        
	$('#alert-box, #fullscreen-bg2').unbind('click').click(cancelAlert);
        MyShortcuts.register('cancel', cancelAlert);
        

	
        message = sanitizeDialogText(message);
        title = sanitizeDialogText(title);
        if (message == '' || message == 'undefined') {
            message = title;
            title = 'Alert';
        }
	
	
	$('#alert-box').html('<a href="javascript:void(0);" class="cancel-button" ><span class="icon-close">&nbsp;</span></a><h2>'+title+'</h2><p>'+message+'</p>');
	$('#alert-box, #fullscreen-bg2').show();
	
	// lock scrollbars temporarily...
	//$('body').css({'overflow':'hidden'});
	$(document).bind('scroll',function () { 
	   window.scrollTo(0,0); 
	});	
	  
	  
	  return false;
	
	
}




// HOLD (use if there's server request that shouldn't be interrupted)
// This box cannot be removed by a user event 
// It has to be removed by the application itself...
function hold (message) {
        
        release();
        
	message = sanitizeDialogText(message);
	
	
	$('#alert-box').html('<p>'+message+'</p>').css({'z-index': 10201});
	$('#alert-box, #fullscreen-hold-bg').show();
	
	// lock scrollbars temporarily...
	//$('body').css({'overflow':'hidden'});
	$(document).bind('scroll',function () { 
	   window.scrollTo(0,0); 
	});	
	  
	  
	return false;
	
	
}


// RELEASE (remove 'hold' bubble...)
function release () {


	
	$('#alert-box').html('<p></p>');
        $('#alert-box, #fullscreen-bg2').unbind('click');
	$('#alert-box, #fullscreen-bg2, #fullscreen-hold-bg').hide();
	
	
        $(document).unbind('scroll'); 
        $('body').css({'overflow':'visible'});	
	  
	  
	return false;
	
	
}






function sanitizeDialogText (message)
{
  	try {
	if (typeof message == 'string') {

	message = message
				.replace(/[^\s]{30,40}/g, '$& ')
				.replace(/&/g, '&amp;')
				.replace(/</g, '&lt;')
				.replace(/>/g, '&gt;')
				.replace(/"/g, '&quot;')
                                .replace(/\n/g, '<br/>');

	} else if (typeof(message) !== 'number' && typeof(message) !== 'boolean') {
		message = typeof(message);
		
	}
	
	} catch (e) {
		
                message = '';
	} 
        
        return message;
    
}



// TOOL TIP BUBBLE SETUP

init[init.length] = function () {
	
	// any element with 'tip' attribute...
	$("#new-customer-form input[tip]").blur(function () {
		
		$('#bubble-1').hide();
		
	})

}





function showTip (target, warning) {

        
        
        $('#bubble-1 > p').html($(target).attr('tip'));
		if (typeof warning === 'string') {
			$('#bubble-1 > p > small').html(warning);
		}
	
		$('#bubble-1').show().position({
				my: "left bottom", // hor-vert
				at: "left top", // hor-vert
				offset: "-20 -25", // left-top
				of: $(target)
		});
	
}


function hideTip (preserveZ) {
    var _bubble = $('#bubble-1');
    
    _bubble
    .fadeOut(300)
    .delay(400);
    
    if (typeof preserveZ === 'boolean' && preserveZ === true) {}
    else _bubble.css('z-index', window.originalTipBoxZIndex); // Reset z-index (in case 'confirmPassword()' messed with it... 
}






function resetTextbox (box) {
if (box.value == box.defaultValue) box.value = "";
else if (box.value == "") box.value = box.defaultValue;

}





// ESCAPE UNSAFE CHARACTERS FOR JSON...
function babyJSON (text) {
	
		if (typeof text != 'string') return text;
                
		return text
				.replace(/\\/g, '\\\\')	
				.replace(/\//g, '\\/')
				.replace(/[\b]/g, '\\b')
				.replace(/\f/g, '\\f')
				.replace(/\n/g, '\\n')
				.replace(/\r/g, '\\r')
				.replace(/\t/g, '\\t')	
				.replace(/"/g, '\"')
				.replace(/\u[0-9a-fA-F]{4}/g, '\\u[0-9a-fA-F]{4}');

	
}



// PROPER STRINGIFY FUNCTION

function encodeJSON (jsonObjct) {
    
}




// FORMAT FLOAT PRICES

/**
*   Usage:  numberFormat(123456.789, 2, '.', ',');
*   result: 123,456.79
**/


function numberFormat (number, decimals, dec_point, thousands_sep) {
    
        var dec = (typeof dec_point === 'undefined') ? '.' : dec_point;    
        var sep = (typeof thousands_sep === 'undefined') ? ',' : thousands_sep;
        decimals = (typeof decimals === 'undefined') ? 2 : decimals;
    
    
 
        // Convert to string temporarily
        var numStr = ''+number;
        
        // Remove commas if any
        numStr = numStr.replace(/,/g, '');
        
        // Convert to float temporarily
        numStr = parseFloat(numStr, 10).toFixed(decimals);
        
        // Separate integer from decimal
        var num = (numStr+'').split('.');
        
        // Reverse integer string for a sec
        num[0] = num[0].split('').reverse().join('');
        
        // Return or add commas to integer
        if (num[0].length > 3) num[0] = num[0].replace(/([\d]{3})/g, '$&'+sep);
        
        // Bring it back
        num[0] = num[0].split('').reverse().join('')
        
        // Fix stray comma
        .replace(/^([,]+)/, '');
        
        // All together now
        return num.join(dec);
}
 









// CHECK PHONE NUMBER ENTRY
function checkPhoneDigit (e, box) {
	
	var chrTyped, chrCode=0, evt= e ? e : event;
	 if (evt.charCode!=null)     chrCode = evt.charCode;
	 else if (evt.which!=null)   chrCode = evt.which;
	 else if (evt.keyCode!=null) chrCode = evt.keyCode;
	
	 if (chrCode==0) chrTyped = 'SPECIAL KEY';
	 else chrTyped = String.fromCharCode(chrCode);
		
	 //Digits, special keys & backspace [\b] work as usual:
	 if (chrTyped.match(/\d|[\b]|SPECIAL/)) return true;
	 if (evt.altKey || evt.ctrlKey || chrCode<28) return true;




	if (/[^+#*\d]/.test(box.value.substr(-1, 1))) {
			box.value = box.value.replace(/[^+#*\d]/, '');
			if (evt.preventDefault) {evt.preventDefault(); } else {return false; }
			evt.returnValue=false;

	}

}


function recheckPhoneDigit (e, box) {
	evt= e ? e : event;
	if (/[^+#*\d]/.test(box.value.substr(-1, 1))) {
			box.value = box.value.replace(/[^+#*\d]/, '');
			if (evt.preventDefault) {evt.preventDefault(); } else {return false; }
			evt.returnValue=false;
	}

}





// CHECK FLOAT NUMBER ENTRY
function validateFloatEntry (e, box) {
	
	var chrTyped, chrCode=0, evt= e ? e : event;
	 if (evt.charCode!=null)     chrCode = evt.charCode;
	 else if (evt.which!=null)   chrCode = evt.which;
	 else if (evt.keyCode!=null) chrCode = evt.keyCode;
	
	 if (chrCode==0) chrTyped = 'SPECIAL KEY';
	 else chrTyped = String.fromCharCode(chrCode);
		
	 //Digits, special keys & backspace [\b] work as usual:
	 if (chrTyped.match(/\d|[\b]|\.|\-|\+|,|SPECIAL/)) return true;
	 if (evt.altKey || evt.ctrlKey || chrCode<28) return true;




	revalidateFloatEntry (e, box);

}


function revalidateFloatEntry (e, box) {
	evt= e ? e : event;
	if (/[^-+\.,\d]/.test(box.value)) {
			box.value = box.value.replace(/[^-+\.,\d]/, '');
			if (evt.preventDefault) {evt.preventDefault(); } else {return false; }
			evt.returnValue=false;
	}

}




// find a string 'needle' in an array 'haystack' and return the array key if found...
 
function findNeedle (needle, haystack) {
	for (var key in haystack) { if (haystack[key] == needle) return key;	}
	return '';
}








/*

CURSOR FIX

*/


var curBrowser = $.browser;
// IE doesn't support co-ordinates
var cursCoords = (curBrowser == "msie") ? "" : " 4 4"; 
closedHandCursor = (curBrowser == "mozilla") ? "-moz-grabbing" : "url(ui/images/closedhand.cur)" + cursCoords + ", move";
// Opera doesn't support url cursors and doesn't fall back well...
if (curBrowser == "opera") openHandCursor = "pointer";
openHandCursor = (curBrowser == "mozilla") ? "-moz-grab" : "url(ui/images/openhand.cur)" + cursCoords + ", move";



		
init[init.length] = function () {
	

			 
}
			


// Passprase REGEXP...

var PASSPHRASE_REGEXP2 = /([-_a-zA-Z0-9]+)*\s([-_a-zA-Z0-9]+)/;
var PASSPHRASE_REGEXP3 = /([-_a-zA-Z0-9]+)*\s([-_a-zA-Z0-9]+)*\s([-_a-zA-Z0-9]+)/;
var EMAIL_REGEXP = /^[^@\.]?[\w\._-]+@([\w\.]+)\.([A-Za-z]{2,4})$/;
var PROVINCE_ISO_REGEXP = /^([A-Za-z]{2,4})$/;
var COUNTRY_ISO_REGEXP = /^([A-Za-z]{2})$/;
var NAME_REGEXP = /[^-\s\.A-Za-z0-9\(\)]/;
var DESCR_REGEXP = /[^-\s\.A-Za-z0-9\W]/;








/*
 * 
 * QUICK SEARCH
 * 
 * 
 */




// JavaScript Document

$('document').ready(function() {
	

        
        
	$('#quick-search-term').keyup(function (e) {
                if (e.keyCode == 13) {
                    // enter
                } else if (e.keyCode == 27) {
                    $(this).val('').blur();
                    $('#main-tile-grid').show();	
                    $('#quick-search-tile-grid').hide();
                    return;
                }
		quick_search();
		
	}).focus(function () {
		$('#quick-search-term').data('status', true);
		$('#quick-search-form-wrapper .search-icon-inset').fadeTo(300, .3);
		$('#quick-search-form-wrapper .search-icon-inset').toggleClass('blink-2');
		
		if ($.trim($('#quick-search-term').val()).length > 2 && $('#quick-search-results > li').length > 0) $('#quick-search-results').show();
                
	}).blur(function () {
		
		$('#quick-search-term').data('status', false);
		
	});
	

	$('body, document, window').click(function (e) {
			if ($('#quick-search-term').data('status') || $('#quick-search-results').css('display') == 'none') return;
			$('#quick-search-results').hide();
			$('#quick-search-form-wrapper .search-icon-inset').fadeTo(300, 1);
			$('#quick-search-form-wrapper .search-icon-inset').toggleClass('blink-2');
	});
	
	


	
	// QUICK SEARCH SUBMIT BIND
	$('#quick-search-form').submit(function () {
		
		quick_search();
		return false;
		
	});
	


});


var _last_req = new Date ();

var quick_search = (function () {

        
//        function setStatus (status) {
//            _busy = status;
//        } 

        return function (options) {
            
		if($.trim($('#quick-search-term').val()).length > 2) {
                    
	
                    var _new_req = new Date ();

                    if (_new_req.getTime() < (_last_req.getTime()+1000)) return;
                    else _last_req = new Date ();
		
                    ajax.jsonRequest ({
                    type: 'GET', 
                    url: $('#quick-search-form').prop('action'), // 'product-search.ajax.php', 
                    data: { term: $('#quick-search-term').val() }, 
                    dataType: 'json', 
                    beforeSend: function () {
                                    //$('#quick-search-form-wrapper .search-icon-inset').toggleClass('blink-2');	
                                    //setStatus(true); // busy

                    },
                    success: quickSearchCallBack,

                    error: function (xhr) {
                            if (typeof window.noResult == 'function') window.noResult();
                            return false;
                            }

                    });

		} else {
			
			$('#quick-search-results').hide();
                        if (typeof window.noResult == 'function') window.noResult();
			
		}
                
        }
        
        
		
})();  // Immediately invoke the function to create the singleton.




/*
 * 
 * function: [ countInEnglish ]
 * 
 */

function countInEnglish (count, singular, plural) {
    
            switch (count) {
                case 0:
                    return 'No ' + plural;
                    break;

                case 1:
                    return '1 ' + singular;
                    break;

                default:
                    return count + ' ' + plural;
                    break;
            }
                      
}




