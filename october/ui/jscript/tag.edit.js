// JavaScript Document

var tries = 0;

var Tags;
var Product;




// GENERIC STORE INFO QUERY
var tag_info_query_template;

$('document').ready(function () {
    
        tag_info_query_template = {

            auth_token: $('#meta_token').val(),
            tag_id: $('#meta_tag_id').val(), 
            name: '', 
            description: ''
        };
       

});



$("#tag_info_form").submit(function (){
	saveDescription();
	
	return false;

});

	


/*
 * 
 * [ FUNCTION ] saveDescription 
 * ____________________________________________________________________________
 * 
 * Self explanatory.
 * 
 */



function saveTagInfo(field) {
	

        if ($(field).attr('defaultValue') == $(field).val()) return false;
        
        

	var tinyStatusSelector = $(field).attr('tinyStatus');
        $(tinyStatusSelector).html('Saving...').toggleClass('blink');

        var query = tag_info_query_template;
        query[$(field).attr('name')] = $(field).val();
        
               

		
		
	var action = $('#meta_admin_root').val() + 'save_tag_info.ajax.php';
	
	ajax.jsonRequest ({
		method: 'POST', 
		url: action, 
		data: query, 
		dataType: 'json', 
                tinyStatus: tinyStatusSelector, 
		success: function (response) {
                            
                            var data = response.json();
			return false;
                        
			},
			
		error: function (xhr) {
                    return false;
                }
        });
        
        return false;
	
	
}







/*
 * 
 * [ FUNCTION ] saveDescription 
 * ____________________________________________________________________________
 * 
 * Self explanatory.
 * 
 */



function deleteCategory () {
        
        hold ('Please wait...');
        
        var query = {
            auth_token: $('#meta_token').val(),
            tag_id: $('#meta_tag_id').val()

        };
        
               

		
		
	var action = $('#meta_admin_root').val() + 'delete_tag.ajax.php';
	
	ajax.jsonRequest ({
		method: 'POST', 
		url: action, 
		data: query, 
		dataType: 'json', 
		success: function (response) {
                            
                            var data = response.json();
                            indow.location = $('#meta_admin_root').val() + 'landing.category.php';
                            return false;
                        
			},
			
		error: function (xhr) {
                    return false;
                }
        });
        
        return false; 
    
}


