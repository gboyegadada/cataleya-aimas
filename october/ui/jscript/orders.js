/* 
 * orders
 * 
 */



var Order, Orders;

$('document').ready(function () {
    

        
    Order = Cataleya.loadOrder();
    Orders = Cataleya.loadOrders();
        
    $('.order-tile')
    .mouseenter(function () {
        if ($(this).hasClass('selected')) return false;
        
        $(this).find('.order-tile-dashboard-wrapper')
        .animate({bottom: '+=50'}, 200)
        .css({'background-color': 'rgba(0, 0, 0, .7)'});
        
        return true;
    })
    
    .mouseleave(function () {
        if ($(this).hasClass('selected')) return false;
        
        $(this).find('.order-tile-dashboard-wrapper')
        .animate({bottom: '-=50'}, 200)
        .css({'background-color': 'rgba(0, 0, 0, .3)'});
        
        return true;
    });


    window.quickSearchCallBack = function (response) 
    {

            var data = response.json();
            var _grid = $('#quick-search-tile-grid > .tiles-wrapper');
            if ($('li.list-table-header').is(":visible")) $('li.list-table-header').slideUp(200);
            
            // Order.deselectAll(true);

            
            $('#main-tile-grid').hide();	
            $('#quick-search-tile-grid').show();

            if (data.count > 0) {
                
                _grid.html('');
                for (i in data.Orders) {
                        var _tile = Order.makeTile(data.Orders[i]); 
                        _grid.append(_tile);
                        
                        _tile
                        .mouseenter(function () {
                            if ($(this).hasClass('selected')) return false;

                            $(this).find('.order-tile-dashboard-wrapper')
                            .animate({bottom: '+=50'}, 200)
                            .css({'background-color': 'rgba(0, 0, 0, .7)'});

                            return true;
                        })

                        .mouseleave(function () {
                            if ($(this).hasClass('selected')) return false;

                            $(this).find('.order-tile-dashboard-wrapper')
                            .animate({bottom: '-=50'}, 200)
                            .css({'background-color': 'rgba(0, 0, 0, .3)'});

                            return true;
                        });
                }
                
                

                
                
                    
//                $('#main-tile-grid').hide();
//                $('#quick-search-tile-grid').show();
                


            } else {
                
                _grid.html('');
                var _exit_button = $('<span class="no-results">No Results (click here to exit search)</span>');
                _exit_button.click(function () {
                    $(this).detach();
                    $('#quick-search-tile-grid').hide();
                    $('#main-tile-grid').show();
                });

                _grid.append(_exit_button);

            
            }
    }
    
    
    
    

    window.noResult = function (data) 
    {
                var _grid = $('#quick-search-tile-grid > .tiles-wrapper');
                _grid.html('');
                var _exit_button = $('<span class="no-results">No Results (click here to exit search)</span>');
                _exit_button.click(function () {
                    $(this).detach();
                    $('#quick-search-tile-grid').hide();
                    $('#main-tile-grid').show();
                    if (!$('li.list-table-header').is(":visible")) $('li.list-table-header').slideDown(200);
                });

                _grid.append(_exit_button);
    }
    
    
    
 

});

