// JavaScript Document

var tries = 0;

// JavaScript Document



var json_uploader_flag = false;
var retries = 0;





function initCurrencyUpdate() {
        
	$('#json_uploader').click();
		
}


function set_json_uploader_flag () {
	
	json_uploader_flag = true;
	
}



function upload_json_file (file_input) {
    
    hold('Updatting currency rates...');
    if (!json_uploader_flag || $(file_input).val() == "") return false;
    json_uploader_engaged = true;
    
    var form_hash_id = '#'+file_input.form.id;
    
    
	var options = {
		dataType: 'json', 
		success: function(data, statusText, xhr, formObjct) {
			json_uploader_engaged = false;
			$(file_input).val('');
                        
                        // args: data (json object), tinyStatus id, successCallBack, errorCallBack
                        alert(data.message);
			return false;

		}, 
		
		error: function (xhr) {
			json_uploader_engaged = false;
			$(file_input).val('');
			
                        alert('Error', xhr);
                        return false;
						
			}

		
	};
	
	$(form_hash_id).ajaxSubmit(options);    
    
        json_uploader_flag = false;
        
        return false;
    
}



/*
 * 
 * [ FUNCTION ] toggleTag 
 * ____________________________________________________________________________
 * 
 * Adds or removes a product tag.
 * 
 */



function toggleCurrencyStatus (currency_code) {
        
        hashId = '#currency-' + currency_code; // variable is set as public so the error callback can reach it...
        var tinyStatusSelector = hashId + '-status';
        
	$(hashId)
                .toggleClass('icon-checkbox-partial')
                .toggleClass('icon-checkbox-unchecked');
	
        
        
                        
        if ($(hashId).hasClass('icon-checkbox-unchecked')) $(hashId+'-import').slideUp('fast');
       
        var query = {
            auth_token: $('#meta_token').val(),
            currency_code: currency_code

        };
               
        
               

	var action = $('#meta_admin_root').val() + 'toggle_currency_status.ajax.php';
	
	ajax.jsonRequest ({
		method: 'POST', 
		url: action, 
		data: query, 
		dataType: 'json', 
                tinyStatus: tinyStatusSelector, 
		success: function (response) {
                            
                            var data = response.json();
                            
                            return false;
                        
			},
			
		error: function (xhr) {
                    
                    $(hashId)
                    .toggleClass('icon-checkbox-partial')
                    .toggleClass('icon-checkbox-unchecked');
                    return false; 
                }
        });
        
        return false; 
        
        
 
}





/*
 * 
 * [ FUNCTION ] deleteStore
 * ____________________________________________________________________________
 * 
 * Self explanatory.
 * 
 */



function setBaseCurrency () {
    
        
        hold ('Please wait...');
        
        var query = {
            auth_token: $('#meta_token').val(),
            currency_code: $('#base-currency-select').val()

        };
        
               

		
		
	var action = $('#meta_admin_root').val() + 'set_base_currency.ajax.php';
	
	ajax.jsonRequest ({
		method: 'POST', 
		url: action, 
		data: query, 
		dataType: 'json', 
		success: function (response) {
                            
                            var data = response.json();
                            hold ('New base currency saved.'); 
                            setTimeout("location.reload(true)", 1500);
                            return false;
                        
			},
			
		error: function (xhr) {
                    return false;
                }
        });
        
        return false; 
    
}