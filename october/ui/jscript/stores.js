// JavaScript Document

$('document').ready(function() {
    
});



window.quickSearchCallBack = function (data) 
{
    
	$('#quick-search-results').html('');
        
        if (data.count > 0) {
                for (i in data.Stores) {
                        var details = (data.Stores[i].phone == '') ? '' : '<small class="quick-search-result-detail icon-phone-2">' + data.Stores[i].phone + '</small>';
                        details += (data.Stores[i].email == '') ? '' : '<small class="quick-search-result-detail icon-email">' + data.Stores[i].email + '</small>';
                        var online_status = (data.Stores[i].isActive) ? 'online' : 'offline';
                        
                        $('#quick-search-results').prepend('<a href="' + data.Stores[i].url + '"><li class="user-status ' + online_status + '">' + data.Stores[i].name + ' (' + data.Stores[i].location.country + ')<br />' + details + '</li></a>');
                }

        $('#quick-search-results').show();

        } else {$('#quick-search-results').hide();	}
}