


$('document').ready(function () {
   
   $('#configuration-list-wrapper > li > a')
   .mouseover(function () {
       $(this).find('.config-edit-icon').show();
   })
   .mouseout(function () {
       $(this).find('.config-edit-icon').fadeOut(300);
   });
});




function togglePermissionsPane (id) {
    if ($('#'+id+' .permissions-pane').css('display') == 'block') {
        $('#'+id+' .permissions-pane').slideUp(300);
        // $('#'+id+' .config-edit-icon').fadeIn(300);
        return false;
    }
    
    $('.permissions-pane').hide();
    // $('.config-edit-icon').hide();
    $('#'+id+' .permissions-pane').slideDown(300);
    // $('#'+id+' .config-edit-icon').fadeOut(300);
    return false;
}




function savePermissions(form_id) {
	

        hold ('Saving...');
        var form = ge(form_id);
        
           
	ajax.jsonRequest ({
		method: 'POST', 
		url: form.action, 
		data: $(form).serialize(), 
		dataType: 'json', 
		success: function (response) {
                            
                            var data = response.json();
                            alert('Saved', data.message);
                            $(form).find('.permissions-pane').slideUp(300);
                            return false;
                        
			},
			
		error: function (xhr) {
                    return false;
                }
        });
        

	return false;
}





function togglePermission (id) {
    var new_perm = $('#'+id+'-anchor').hasClass('icon-checkbox-partial') ? 0 : 1;
    
    $('#'+id+'-input').val(new_perm);
    
    if (new_perm == 1) $('#'+id+'-anchor').removeClass('icon-checkbox-unchecked').addClass('icon-checkbox-partial');
    else $('#'+id+'-anchor').removeClass('icon-checkbox-partial').addClass('icon-checkbox-unchecked');
    
}