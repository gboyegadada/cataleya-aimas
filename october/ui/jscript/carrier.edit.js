/* 
 * carrier.edit.js
 * 
 */



// GENERIC CARRIER INFO QUERY
var carrier_info_query_template;

$('document').ready(function () {
    
        carrier_info_query_template = {

            auth_token: $('#meta_token').val(),
            carrier_id: $('#meta_carrier_id').val(), 
            carrier_name: '', 
            carrier_description: '', 
            contact_name: '', 
            telephone: '', 
            email: '', 
            active: '', 
            street_address: '', 
            suburb: '', 
            city: '', 
            state: '',
            postcode: '',
            country: ''
        };
       

});


/*
 * 
 * [ FUNCTION ] saveDescription 
 * ____________________________________________________________________________
 * 
 * Self explanatory.
 * 
 */



function saveCarrierInfo(field) {
	

        if ($(field).attr('defaultValue') == $(field).val()) return false;
        
        if (field.name == 'email' && (field.value == '' || !(EMAIL_REGEXP.test(field.value))) ) {
				showTip(field, 'Please enter a valid email address.');
                                setTimeout('hideTip()', 3000);
				return false;
				}
        

	var tinyStatusSelector = $(field).attr('tinyStatus');
        $(tinyStatusSelector).html('Saving...').toggleClass('blink');

        var query = carrier_info_query_template;
        query[$(field).attr('name')] = $(field).val();
        
               

		
		
	var action = 'save_carrier_info.ajax.php';
	
	ajax.jsonRequest ({
		method: 'POST', 
		url: action, 
		data: query, 
		dataType: 'json', 
                tinyStatus: tinyStatusSelector, 
		success: function (response) {
                            
                        var data = response.json();
			return false;
                        
			},
			
		error: function (xhr) {
                    return false;
                }
        });
        
        return false;
	
	
}






/*
 * 
 * [ FUNCTION ] deleteCarrier
 * ____________________________________________________________________________
 * 
 * Self explanatory.
 * 
 */



function deleteCarrier () {
        
        hold ('Please wait...');
        
        var query = {
            auth_token: $('#meta_token').val(),
            carrier_id: $('#meta_carrier_id').val()

        };
        
               

		
		
	var action = 'delete_carrier.ajax.php';
	
	ajax.jsonRequest ({
		method: 'POST', 
		url: action, 
		data: query, 
		dataType: 'json', 
		success: function (response) {
                            
                            var data = response.json();
                            alert ('Ok', data.message);
                            window.location = $('#meta_admin_root').val() + 'landing.carriers.php';
                            return false;
                        
			},
			
		error: function (xhr) {
                    return false;
                }
        });
        
        return false; 
    
}