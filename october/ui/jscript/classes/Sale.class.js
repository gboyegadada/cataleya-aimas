
 
 
// ----------------------------------------------------------------------------------------- //


// Class: [ loadSale ]
Cataleya.loadSale = function (callBack) {
    
    // Private properties
    
    var 
    
    _dialogBox, 
    _auth_token = $('#meta_token').val(), 
    _sale_id = 0, 
    _a = Cataleya.arrayFuncs(), 
    Utility = Cataleya.getUtility(), 
    
    _templates = {}, 
    
    _query_temp = {
        auth_token: _auth_token,
        target_id: 0, 
        'do': 'void', 
        data: []

    };
    

    
    
    // Load moustache templates
    ajax.loadTemplates(['sale-dialog']);
    
    _templates.saleTile = '<li id="sale-tile-{{id}}" class="tile user-status {{#isActive}}online{{/isActive}} {{^isActive}}offline{{/isActive}}"><a class="fltlft main-anchor" href="{{href}}"><span class="tiny-label">{{Store.title}}</span> {{title}} ({{population}})</a>'
                    + '<a href="javascript:void(0)"  onclick="Sale.editSale({{id}});" ><span class="icon-pencil-2 fltrt"></span> </a>' 
                    + '<a href="javascript:void(0)" onclick="Sale.deleteSale({{id}});" ><span class="icon-close fltrt"></span></a></li>';




    
    
    function renderDialog (data) {


                    $('.grp-dialog-box').detach(); // remove existing dialog boxes...

                    _dialogBox = $(Mustache.render(ajax.getTemplate('sale-dialog'), data));  

                    // Show dialog box...
                    $('body').append(_dialogBox);
                    
                    _dialogBox.find('#sale-status-button').click(function () {
                        var _checkbox = $('#sale-status-checkbox');
                        
                        if (_checkbox.prop('checked') == true) {
                            $(this)
                            .removeClass('online')
                            .addClass('offline')
                            .html('Off (Turn On)');
                            
                            _checkbox.prop('checked', false);
                        } else {
                            $(this)
                            .removeClass('offline')
                            .addClass('online')
                            .html('On (Turn Off)');
                            
                            _checkbox.prop('checked', true);
                        }
                        
                        
                        return false;
                        
                    })
                    .end()
                    .show();



                    $('#fullscreen-bg2, .grp-dialog-box > .cancel-button').unbind('click').click(function () {

                       _dialogBox.hide().remove();
                       
                       $('#fullscreen-bg2').unbind('click').hide();
                       $(document).unbind('scroll'); 
                       $('body').css({'overflow':'visible'});	

                    })
                    .show();
                    

    }
    
   

    
    
    


    // method: [ newSale ]
    function newSale () {

            var 

            _stores = Catalog.getPickerData('Stores'), 
            _default_store = _stores[0], 

            _Sale = {
                id: 0, 
                title: 'New Sale', 
                description: 'New Sale', 
                discountType: 0, 
                discountAmount: 0, 
                Currency: _default_store.Currency, 

                isActive: false, 
                Store: _default_store
            }, 
            
            _meta = {
                title: 'New Sale', 
                buttonLabel: 'save sale'
            };
            
            renderDialog({meta: _meta, Sale: _Sale, Stores: _stores});
            _sale_id = 0;
            
            return false;
        
    }
    
    
    
    


    // method: [ editSale ]
    function editSale (id) {
            
            if (typeof id !== 'number') {throw new Exception ('Invalid sale id!'); return false; }
            
            _sale_id = parseInt(id);
            ajax.doAPICall({
                api: 'sale', 
                target_id: _sale_id, 
                'do': 'getInfo', 
                data: { store_id: 0 }, 
                success: function (data) {
                    var _stores = Catalog.getPickerData('Stores');
                    var _meta = {
                        title: 'Edit Sale', 
                        buttonLabel: 'save sale'
                    };
                    
                    
                    for (var i in _stores) {
                        _stores[i].selected = (_stores[i].id == data.Sale.Store.id) ? 'selected' : '';
                    }
                    

                    renderDialog({meta: _meta, Sale: data.Sale, Stores: _stores});
                    return false;
                },

                error: function (xhr) {
                    return false;
                }
        });
        
        return false;
        
    }
    
    
    
    
    // method: [ saveSale ]
    function saveSale (id) {
        
            if (typeof id !== 'number') {throw new Exception ('Invalid sale id!'); return false; }
            _sale_id = parseInt(id);
            
            _dialogBox.hide();
            $('#fullscreen-bg2').hide();
            
            hold('Saving sale ...');
            
            var data = Utility.getFormData($('#edit-sale-form'));

            
            ajax.doAPICall({
                api: 'sale', 
                target_id: _sale_id, 
                'do': ((_sale_id == 0) ? 'newSale' : 'saveSale'), 
                data: data, 
                success: function (data) {
                    
            

                    _dialogBox.detach();
                    
                     var new_tile = $(Mustache.render(_templates.saleTile, data.Sale));
                     var oldTile = $('li#sale-tile-'+data.Sale.id);

                     if (oldTile.length > 0) oldTile.before(new_tile);
                     else $('#new-sale-tile').before(new_tile); 


                     // In case we're modifing an existing tile, remove from list...
                     if (oldTile.hasClass('current')) new_tile.addClass('current');
                     oldTile.detach(); 
                     
                     Utility.refreshPickerData();
                    
                },

                error: function (xhr) {
                    _dialogBox.show();
                    $('#fullscreen-bg2').show();
                    return false;
                }
        }); 
        
        
    }
    
    
    
    
    

    // method: [ deleteSale ]
    function deleteSale (id) {
            
            if (typeof id !== 'number') {throw new Exception ('Invalid sale id!'); return false; }
            
            _sale_id = 0;
            ajax.doAPICall({
                api: 'sale', 
                target_id: parseInt(id), 
                'do': 'deleteSale', 
                data: { store_id: 0 }, 
                success: function (data) {

                    $('li#sale-tile-'+data.Sale.id).detach();
                    return false;
                },

                error: function (xhr) {
                    return false;
                }
        });
        
        return false;
        
    }
    
    
    
    
    
    
    
    
    
    // method: [ resetCurrency ] 
    function resetCurrency (control) {
        var code = $(control.options[control.selectedIndex]).attr('currencyCode');
        if (typeof code === 'string' && code.length == 3) _dialogBox.find('#sale-currency').text(code);
        
        //validateCode();
    }
    
    
    // method: [ validateAmount ] 
    function validateAmount () {
        var amount = _dialogBox.find('#text-field-discount-amount');
        var type = _dialogBox.find('#select-field-discount-type').get(0);
        
        if (type.selectedIndex == 0 && parseInt(amount.val()) > 100) amount.val(0);
    }
    
    

    
    
    
    
    //Return public methods
    return {
        newSale: newSale, 
        editSale: editSale, 
        saveSale: saveSale, 
        deleteSale: deleteSale, 
        
        resetCurrency: resetCurrency, 
        validateAmount: validateAmount
        
    };
    
    
    
 }
 
 
 