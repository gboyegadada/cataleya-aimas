
 
 
// ----------------------------------------------------------------------------------------- //


// Class: [ newProductWidget ]
Cataleya.newProductWidget = function (callBack) {
    
    // Private properties
    
    var 
    
    env = {
        dialogBox: undefined, 
        product_id: 0, 
        document_saved: true, 
        upload_target_id: 0, 
        slider: undefined, 
        optionsChecklist: undefined
    }, 
    
    _auth_token = $('#meta_token').val(); 
    

    loadData(callBack);


    // method: [ refresh ]
    function loadData (callBack) {

            // Load moustache templates
            ajax.loadTemplates([
                'product-dialog-box', 
                'product-option-list', 
                'product-option-list-item', 
                'product-tile', 
                'product-photo-widget-thumb', 
                'product-base-image-picker-thumb', 
                'product-option-widget', 
                'product-option-widget.plural', 
                'product-photo-widget'
            ], true);

            if (typeof callBack == 'function') callBack();
            return false;

        
    }

    

    
    
    
    // method: [ makeTile ]
    function makeTile (_data) {
       return $(Mustache.render(ajax.getTemplate('product-tile'), _data)); 
    } 
    
    
    
    
    
    /*
     *
     * private method: [ renderDialog ]
     * 
     */
    


    function renderDialog (_id, callBack) {
        
            if (typeof Catalog !== 'undefined') Catalog.deselectAll(true);
        
            if (typeof _id !== 'number') {throw new TypeError ('Invalid product id!'); return false; }
            env.product_id = parseInt(_id);
            env.document_saved = true;
            
            ajax.doAPICall({
                api: 'product', 
                target_id: env.product_id, 
                'do': 'getProductInfo', 
                data: { 'void': 0 }, 
                success: function (data) {
                            
                            env.dialogBox = $(Mustache.render(
                                    ajax.getTemplate('product-dialog-box'), 
                                    data.Product, 
                                    { 
                                        'product-option-list': ajax.getTemplate('product-option-list'), 
                                        'product-option-list-item': ajax.getTemplate('product-option-list-item'), 
                                        'product-photo-widget': ajax.getTemplate('product-photo-widget'), 
                                        'product-base-image-picker-thumb': ajax.getTemplate('product-base-image-picker-thumb')
                                    })
                            );  
                                    
                            
                            // Show dialog box...
                            $('body').append(env.dialogBox);
                            
                            var closeProductDialogBox = function closeProductDialogBox () {
                                
                                

                                if (env.document_saved) done (); 
                                else { 
                                    env.slider.gotoSlide(0);
                                    
                                    Cataleya.confirm({
                                        title: 'Save Changes...', 
                                        message: 'Do you want to save the changes you made in product details for “' + data.Product.title + '”?', // Your changes will be lost if you don’t save them.
                                        label_confirm: 'Save', 
                                        label_cancel: "Don't Save", 
                                        callBack: function (ok) {
                                            if (ok) saveContentBody();

                                            done ();

                                            }
                                    });
                                
                                }

                                
                            }, 
                                   
                            
                            done = function done () {

                                $("#base-image-uploader.droppable-uploader-area").dropper('destroy').off('.dropper');
                                $("#photo-widget-uploader.droppable-uploader-area").dropper('destroy').off('.dropper');
                                
                                env.optionsChecklist.destroy();
                                env.quill.destroy();
                                
                                env.dialogBox.remove();
                                $('#fullscreen-bg3').unbind('click').hide();
                                MyShortcuts.clear('close', closeProductDialogBox);
                                MyShortcuts.clear('save', savePageBodyHandler);
                            };
                            
                            __el('#fullscreen-bg3, #new-item-pane > .cancel-button').addEventListener('click', closeProductDialogBox)
                            .show();
                            env.dialogBox.show();
                            

                            
                            // Init: save button...
                            env.saveButton = __el('#new-item-pane .editor-save-button');
                            env.tinyStatus = __el('.rtf-editor-status-bar span.dot');
                            
                            savePageBodyHandler = function savePageBodyHandler () {
                                
                                env.saveButton.fadeOut();
                                env.tinyStatus.fadeIn();
                                saveContentBody();
                            };
                            
                            
                            env.quill = new Quill('.quill-editor > #editor', { 
                                modules: {
                                    'toolbar': { container: '.quill-editor > #toolbar' } 
                                }
                            });
                            
                            // Init: save button...
                            env.saveButton.addEventListener('click', savePageBodyHandler);

                            // on change: title...
                            document.getElementById('product-name-field').addEventListener('change', function () {
                                env.document_saved = false;
                                env.saveButton.show('inline-block');
                            });

                            //on change: body ...
                            env.quill.on('text-change', function () {
                                env.document_saved = false;
                                env.saveButton.show('inline-block');
                            });
                            
                            
                            MyShortcuts.register('close', closeProductDialogBox);
                            MyShortcuts.register('save', savePageBodyHandler, {'disable_in_input':false});
                            
                            

                            // Index the frames for the [ seekPanel() ] method.
                            
                            
                            env.slider = Cataleya.makeSlider(document.querySelector('.productWidgetSlider'), 'slide');
                            var updateFrameTitle = function updateFrameTitle (e) {
                                    
                                    $('#frame-headers li:nth-child(' + (env.slider.getLastPos()+1) + ')').fadeOut(300, function () {

                                        $('#frame-headers li:nth-child(' + (env.slider.getPos()+1) + ')').fadeIn(300);

                                    });
                                

                                    $('#bread-crumbs li.active-crumb').removeClass('active-crumb');
                                    $('#bread-crumbs li:nth(' + (env.slider.getPos()) + ')').addClass('active-crumb');

                            };
                            
                            env.slider
                                    .on('seekFrame', updateFrameTitle)
                                    .on('nextFrame', updateFrameTitle)
                                    .on('previousFrame', updateFrameTitle);
                            
                            
                            
                            var bread_crumbs = env.dialogBox[0].querySelectorAll('#bread-crumbs > a');
                            
                            for (var i=0; i<bread_crumbs.length; i++) {
                                bread_crumbs[i].onClick = function () {
                                    env.slider.gotoSlide(i);
                                };
                            }
                            
                            
                            
                            // INIT PRODUCT OPTIONS CHECKLIST
                            env.optionsChecklist = initProductOptions();
                            

                            /*

                            RECYCLE BIN

                            */

                            env.dialogBox.find( "#recycle-bin" ).droppable({
                                    accept: '.trashable', 
                                    hoverClass: 'recycle-bin-hover-state', 
                                    activate: function ( event, ui ) {
                                        

                                                    $('#recycle-bin-wrapper').slideDown(300);

                                                    $( "#recycle-bin" ).fadeTo(300, 1);
                                                    ui.helper
                                                    .css('cursor', closedHandCursor)
                                                    .toggleClass('blink-2');

                                                    if (ui.helper.hasClass('color-tile') || ui.helper.hasClass('size-entry-tile')  || ui.helper.hasClass('product-group-member-tile') ) ui.draggable.hide();

                                    }, 
                                    deactivate: function ( event, ui ) {

                                                    $('#recycle-bin-wrapper').slideUp(300);
                                                    $( "#recycle-bin" ).fadeTo(300, .4);

                                                    if (ui.helper.hasClass('color-tile') || ui.helper.hasClass('size-entry-tile') || ui.helper.hasClass('product-group-member-tile'))
                                                    {
                                                        ui.draggable
                                                        .css('cursor', openHandCursor)	 // .css('cursor', 'pointer')
                                                        .show();
                                                    } 


                                                    ui.helper
                                                    .css('cursor', openHandCursor)						
                                                    .toggleClass('blink-2');


                                    },
                                    drop: function( event, ui ) {

                                        switch (ui.draggable.attr('trash_type'))
                                        {
                                            case 'base-image':
                                                deleteImage(ui);
                                                break;

                                            case 'color-tile':
                                                deleteProductColor(ui);
                                                break;

                                            case 'size-entry-tile':
                                                deleteProductSize(ui.draggable.attr('data_size_id'));
                                                break;
                                                
                                            case 'product-group-member-tile':
                                                removeFromProductGroup(ui.draggable.attr('data_product_id'));
                                                break;
                                                
                                            default:
                                                // do nothing
                                                ui.draggable.show();
                                                break;

                                        }

                                    }
                            });



                            
                            

                            /*

                            PRIMARY PRODUCT IMAGE SETTER

                            */


                            env.dialogBox.find( "#base-image-preview" ).droppable({
                                    accept: '.primary-product-image-candidate', 
                                    activeClass: '', 
                                    hoverClass: 'base-image-preview-drag-over', 
                                    activate: function ( event, ui ) {
                                            $('#base-image-uploader').hide();
                                            
                                            var active_image_button = $( "#base-image-preview" ).attr('active_image_button');
                                            if (active_image_button != "") {
                                                            $( "#base-image-preview" ).attr('bg_src_cache', $( "#base-image-preview" ).css('background-image'))
                                                            .css('background-image', 'url(ui/images/transparent_pixel.gif)')
                                                            .find('#base-image-drop-label')
                                                            .show()
                                                            .end();

                                                    ui.draggable.fadeTo(300, .1);
                                                    //$('#'+active_image_button).show();


                                            }
                                    }, 
                                    deactivate: function ( event, ui ) {

                                            var active_image_button = $( "#base-image-preview" ).attr('active_image_button');
                                            if (active_image_button != "") {
                                                            $( "#base-image-preview" )
                                                            .css('background-image', $( "#base-image-preview" ).attr('bg_src_cache'))
                                                            .find('#base-image-drop-label')
                                                            .hide()
                                                            .end();
                                                            

                                                    ui.draggable.fadeTo(300, 1);
                                                    //$('#'+active_image_button).hide();


                                            }
                                    },
                                    drop: function( event, ui ) {
                                            setPrimaryBaseImage(ui);

                                            $('#base-image-uploader').show();
                                            
                                    }
                            });



                            
                            

                            /*

                            SET DRAGGABLES

                            */


                            env.dialogBox.find( '.size-entry-tile, .product-group-member-tile, .color-tile, .base-image-picker-thumb-button' ).css('cursor', openHandCursor).draggable({
                                                                    revert: 'invalid', 
                                                                    revertDuration: 200,
                                                                    appendTo: "body", 
                                                                    containment: 'document',  
                                                                    opacity: 0.5, 
                                                                    zIndex: 12700, 
                                                                    helper: "clone"
                                                                    });   
                                                                    

                                                                    
                               
                    
                    
                    
                    
                            /*
                             * 
                             * DROPPABLE IMAGE UPLOAD
                             * 
                           
                             var myDropzone = new Dropzone("div#base-image-preview", { 
                                    url: $('#meta_admin_root').val() + "upload.ajax.php", 
                                    paramName: "file", // The name that will be used to transfer the file
                                    maxFilesize: 2, // MB
                                    acceptedFiles: 'image/jpeg',
                                    accept: function(file, done) {
                                      if (file.name == "justinbieber.jpg") {
                                        done("Naha, you don't.");
                                      }
                                      else { done(); }
                                    }
                             
                            
                            });
                             */
                            
                            
                            $("#base-image-uploader.droppable-uploader-area").dropper({
                                    action: $('#meta_admin_root').val() + "upload.ajax.php", 
                                    postKey: 'photo_uploader', 
                                    maxSize: 3048576, 
                                    postData: { 
                                        'auth_token': _auth_token, 
                                        'pw': '600', 
                                        'ph': '400', 
                                        'tw': '400', 
                                        'th': '600'
                                    }, 
                                    label: ' '
                            }).on("fileComplete.dropper", function (e, file, data) {
                                var target = $(e.target);
                    
                                var saveHandler, setPrimaryImage;
                                env.upload_target_id = target.attr('data-id');
                                
                                switch (target.attr('data-target')) {
                                    
                                    case 'product':
                                        saveHandler = saveBaseImage;
                                        setPrimaryImage = (target.attr('data-set-primary') === 'yes') ? true : false;
                                        break;
                                    case 'product_option':
                                        saveHandler = saveOptionPreview;
                                        setPrimaryImage = false;
                                        break;  
                                }
                                
                                
                                
				PhotoUploader.launchPhotoEditor(data, { 
                                    saveHandler: saveHandler, 
                                    uploadTargetID: env.upload_target_id, 
                                    setPrimaryImage: setPrimaryImage, 
                                    tw: 400, 
                                    th: 600
                                });
                            })
                            .on("fileError.dropper", function (e, file, error) {
                                console.log("Error: " + error);
                            }).on("dragenter.dropper", function (e) {
                                
                                var _previewBox = env.dialogBox.find( "#base-image-preview" );
                                
                                _previewBox
                                        .addClass('base-image-preview-drag-over')
                                        .attr('bg_src_cache', _previewBox.css('background-image'))
                                                            .css('background-image', 'url(ui/images/transparent_pixel.gif)')
                                                            .find('#base-image-drop-label')
                                                            .show()
                                                            .end();
                                                    
                                
                                                    
                            }).on("dragleave.dropper", function (e) {
                                
                                var _previewBox = env.dialogBox.find( "#base-image-preview" );
                                
                                _previewBox
                                        .removeClass('base-image-preview-drag-over')
                                        .css('background-image', _previewBox.attr('bg_src_cache'))
                                                            .find('#base-image-drop-label')
                                                            .hide()
                                                            .end();
                            });
                            
                            
                            
                            
                            
                            
                            
                            
                    
                    
                    
                            /*
                             * 
                             * PRODUCT PHOTO WIDGET / INDEX INIT
                             * 
                            
                             */
                            

                                
                            var closePhotoWidget = function closePhotoWidget () {
                                env.dialogBox.find('#product-photo-widget-wrapper').fadeOut(100);
                                MyShortcuts.clear('cancel', closePhotoWidget);
                            };
                            
                            env.dialogBox.find('#product-photo-widget-wrapper .cancel-button').click(closePhotoWidget);
                            
                            env.dialogBox.find('#base-image-uploader, .base-image-picker-add-button-a').click(function (e) {
                                
                                e.stopPropagation();
                                e.preventDefault();
                                
                                env.upload_target_id = env.product_id;
                                console.log('target: ' + env.upload_target_id);
                                
                                $('#product-photo-widget-wrapper').fadeIn(100);
                                MyShortcuts.register('cancel', closePhotoWidget);
                            });






                            $("#photo-widget-uploader.droppable-uploader-area").dropper({
                                    action: $('#meta_admin_root').val() + "upload.ajax.php", 
                                    postKey: 'photo_uploader', 
                                    maxSize: 3048576, 
                                    postData: { 
                                        'auth_token': _auth_token, 
                                        'pw': '600', 
                                        'ph': '400', 
                                        'tw': '400', 
                                        'th': '600'
                                    }, 
                                    label: ' '
                            }).on("fileComplete.dropper", function (e, file, data) {
                                var target = $(e.target);
                                
                                
                                
                                console.log('Target:', target);
                                
                                var saveHandler, setPrimaryImage;
                                env.upload_target_id = target.attr('data-id');
                                
                                switch (target.attr('data-target')) {
                                    
                                    case 'product':
                                        saveHandler = saveBaseImage;
                                        setPrimaryImage = (target.attr('data-set-primary') === 'yes') ? true : false;
                                        break;
                                    case 'product_option':
                                        saveHandler = saveOptionPreview;
                                        setPrimaryImage = false;
                                        break;  
                                }
                                
                                
                                
				PhotoUploader.launchPhotoEditor(data, { 
                                    saveHandler: saveHandler, 
                                    uploadTargetID: env.upload_target_id,  
                                    setPrimaryImage: setPrimaryImage, 
                                    tw: 400, 
                                    th: 600
                                });
                            })
                            .on("fileError.dropper", function (e, file, error) {
                                console.log("Error: " + error);
                            }).on("dragenter.dropper", function (e) {
                                
                                env.dialogBox
                                .find('#product-photo-widget-wrapper')
                                .addClass('active');
                        
                                
                                                    
                            }).on("dragleave.dropper", function (e) {
                                
                                env.dialogBox
                                .find('#product-photo-widget-wrapper')
                                .removeClass('active');
                        
                            }).on("drop.dropper", function (e) {
                                
                                env.dialogBox
                                .find('#product-photo-widget-wrapper')
                                .removeClass('active');
                        
                            });
                            
                            
                            
                    
                    
                    
                    
                    
                          
                            /*
                             *
                             * PRODUCT GROUP SEARCH BOX INIT
                             * 
                             */
                            
                            var _search_box = env.dialogBox.find('#frame-7 .quick-search-term');
                            
                            _search_box.keyup(function () {

                                    productWidget.productSearch($(this).val(), productWidget.renderProductGroupMenu.bind(productWidget));

                            }).focus(function () {
                                    $(this).data('status', true);
                                    
                                    $('.search-field-wrapper')
                                    .find('.search-icon-inset')
                                    .fadeTo(300, .3)
                                    .toggleClass('blink-2');

                                    if ($.trim($(this).val()).length > 2 && $('#frame-7 .product-group-menu > li').length > 0) $('#frame-7 .product-group-menu').show();

                            }).blur(function () {

                                setTimeout(function () {
                                        $(this).data('status', false).val('');
                                        env.dialogBox.find('#frame-7 .product-group-menu').hide();
                                        env.dialogBox.find('#frame-7 .group-member-items').show();       
                                }, 200);


                            });
                            
                            
                            
                            /*
                             *
                             * PRODUCT GROUP MEMBER TILES
                             * 
                             */
                            
                            env.dialogBox.find('#frame-7 .group-member-items li.editable-list-tile').mouseenter(function () {
                                         $(this).find('.button-1').fadeIn('fast');
                                     })
                                     .mouseleave(function () {
                                         $(this).find('.button-1').hide();
                                     });
                                     



                            if (typeof callBack == 'function') callBack();
                            return false;

                        },

                error: function (xhr) {
                    return false;
                }
        });  

    }
    
    
    
    
    
    


    /**
     * 
     * @param {string} tinyStatusSelector
     * @returns {Boolean}
     * 
     */
    function saveContentBody () {




            var 

            _target_id = env.product_id, 


            data = {
                title: document.getElementById('product-name-field').value, 
                description: env.quill.getHTML()
            },
            _do = 'saveDescription', 
            _doHold = false;



            ajax.doAPICall({
                api: 'product', 
                target_id: _target_id, 
                'do': _do, 
                data: data, 
                hold: _doHold,
                success: function (data) {
                        
                        env.document_saved = true;
                        
                        var item = document.getElementById('product-tile-'+data.Product.id);
                        item.querySelector('.product-tile-dashboard-wrapper > .header').innerHTML = data.Product.title;
                        env.dialogBox[0].querySelector('#product-dialog-title .item-name').innerHTML = data.Product.title;
                        
                        setTimeout(function () { env.tinyStatus.fadeOut(); }, 2000);
                        return false;

                        },

                error: function (xhr) {
                        return false;
                }
            });

            return false;


    }
    
    
    
    
    
    
  
    
    
    // method: [ newProduct ]
    function newProduct () {

                return false; 
    } 
    
    
    // method: [ editProduct ]
    function editProduct (id) {
        hold('Please wait...');
        renderDialog(parseInt(id), release);
        
        return true;
    } 
    
    



    // FUNCTIONS FOR SUB_PANE NAVIGATOR ....

    function nextPane () {  
        //var _frame = activeFrame+1;
        //return navigateToFrame (_frame);
        
        env.slider.nextSlide();
    }



    function prevPane () {

        //var _frame = activeFrame-1;
        //navigateToFrame (_frame);
        
        env.slider.prevSlide();
    }
    
    
    function seekPane (_label) {
        

        return env.slider.gotoSlide (parseInt(_label));

        return false;
    }




    function navigateToFrame (frame) {


        env.slider.gotoSlide(frame); 
        
        /*
        $('#frame-headers li:nth-child(' + frame-1 + ')').fadeOut(300, function () {

                        $('#frame-headers li:nth-child(' + frame-1 + ')').fadeIn(300);

        });
        
        $('#bread-crumbs li.active-crumb').removeClass('active-crumb');
        $('#bread-crumbs li:nth(' + (frame-1) + ')').addClass('active-crumb');
        */
       
        return false;

    }
    
    
    
    
    



    /*
     * 
     * [ FUNCTION ] initProductOptions
     * ____________________________________________________________________________
     * 
     * Self explanatory.
     * 
     */


    function initProductOptions ()
    {


            // INIT PRODUCT OPTIONS CHECKLIST
            
            var 
            
                optionsChecklistContainer = document.getElementById('product-options-checklist-container'), 
                button_SelectAll = __el(env.dialogBox[0].querySelector('a.select-all-product-options-button'));
            
            if (typeof env.optionsChecklist !== 'undefined') { 
                env.optionsChecklist.destroy();
                env.optionsChecklist = null;
            }
            
            env.optionsChecklist = Cataleya.makeCheckList(optionsChecklistContainer);
            
            
            
            env.optionsChecklist
            
            .on('onFirstCheck', function () {
                MyShortcuts.register('deleteItem', deleteProductOptionHandle);
                MyShortcuts.register('open', editProductOptionHandle);
                MyShortcuts.register('shift', shiftKeyHandler);
                
            })
            .on('onAllUnchecked', function () {
                button_SelectAll.removeClass('active');
                MyShortcuts.clear('deleteItem', deleteProductOptionHandle);
                MyShortcuts.clear('open', editProductOptionHandle);
                MyShortcuts.clear('shift', shiftKeyHandler);
                
                button_SelectAll.item(0).querySelector('.button-label').innerHTML = 'Select All';
            })
            .on('onAllChecked', function () {
                button_SelectAll.addClass('active');
            })
            .on('onLastUnchecked', function () {
                button_SelectAll.removeClass('active');
            });
            


   
            // PRICE, STOCK, SKU and UPC fields..
                
            __el('input.option-price-field, input.option-stock-field, input.option-sku-field, input.option-upc-field')
            .addEventListener('focus', uniLabelInputFocusHandler);
                

            var 
            deselectAllHandler = env.optionsChecklist.deselectAll.bind(env.optionsChecklist); 
            
            function deleteProductOptionHandle () {

                MyShortcuts.clear('deleteItem', deleteProductOptionHandle);
                productWidget.deleteProductOption(env.optionsChecklist.getSelected(), deselectAllHandler);

            }; 
            
            function editProductOptionHandle () {

                MyShortcuts.clear('open', editProductOptionHandle);
                if (env.optionsChecklist.count() > 1) productWidget.editProductOption(env.optionsChecklist.getSelected(), deselectAllHandler);
                else  productWidget.editProductOption(env.optionsChecklist.getLastSelected(), deselectAllHandler);

            };
                
                

            function shiftKeyHandler (e) {
                if (e.shiftKey) {
                    // Keydown...

                    //button_SelectAll.querySelector('.icon-checkmark').addClass('active');
                    button_SelectAll.item(0).querySelector('.button-label').innerHTML = 'Deselect All';


                } else {
                    // Keyup...
                    button_SelectAll.item(0).querySelector('.button-label').innerHTML = 'Select All';
                }
            };
                
                
                
            button_SelectAll.addEventListener('click', env.optionsChecklist.toggleAllSelections.bind(env.optionsChecklist));
            env.dialogBox[0].querySelector('a.delete-selected-product-options-button').addEventListener('click', deleteProductOptionHandle);
            env.dialogBox[0].querySelector('a.edit-selected-product-options-button').addEventListener('click', editProductOptionHandle);



            // INIT: Task Buttons...
            __el(optionsChecklistContainer.querySelectorAll('.checklist-tile')).forEach(function () {
                
                var This = this;
                
                This.querySelector('.select-product-option-button').addEventListener('click', function () {
                        env.optionsChecklist.toggleSelection(This.getAttribute('data-tile-id'));
                });

                This.querySelector('.delete-product-option-button').addEventListener('click', function () {
                        productWidget.deleteProductOption(This.getAttribute('data-tile-id'));
                });

                This.querySelector('.edit-product-option-button').addEventListener('click', function () {
                        productWidget.editProductOption(This.getAttribute('data-tile-id'), deselectAllHandler);
                });

            });
            
            
            return env.optionsChecklist;
    }








    /*
     * 
     * [ FUNCTION ] uniLabelInputFocusHandler
     * ____________________________________________________________________________
     * 
     * Self explanatory.
     * 
     */


    function uniLabelInputFocusHandler (e)
    {
                e = e || window.event;
                var target = e.target || e.srcElement;
                
                var This = $(target);
                var _uniLabelInputCache = This.val();
                
                
                var formatInput = function  (i) {
                    
                    var This = $(i);
                    
                    switch (This.attr('data-format')) {
                            case 'price':
                                This.val(numberFormat(This.val(), 2));
                            break;
                            
                            case 'float':
                                This.val(parseFloat(This.val()));
                            break;
                            
                            case 'integer':
                                This.val(parseInt(This.val()));
                            break;
                            
                            case 'upper-case':
                                This.val(This.val().toUpperCase());
                            break;
                            
                            case 'lower-case':
                                This.val(This.val().toLowerCase());
                            break;
                            
                        }
                       
                };



                This.change(function () {
                        
                        formatInput(This);

                        This
                        .unbind('change')
                        .blur();

                        productWidget.saveDescription(This[0]);

                        MyShortcuts.clear('return', doUniLabelInputSave);
                        MyShortcuts.clear('cancel', cancelUniLabelInputSave);
                    });



                function cancelUniLabelInputSave () {
                    This
                    .unbind('change')
                    .blur().val(_uniLabelInputCache);


                    MyShortcuts.clear('return', doUniLabelInputSave);
                    MyShortcuts.clear('cancel', cancelUniLabelInputSave);
                }

                function doUniLabelInputSave () {
                    formatInput(This);

                    This
                    .unbind('change')
                    .blur();

                    productWidget.saveDescription(This[0]); 



                    MyShortcuts.clear('return', doUniLabelInputSave);
                    MyShortcuts.clear('cancel', cancelUniLabelInputSave);
                }


                MyShortcuts.register('return', doUniLabelInputSave, {'disable_in_input':false});
                MyShortcuts.register('cancel', cancelUniLabelInputSave, {'disable_in_input':false});

    }








    /*
     * 
     * [ FUNCTION ] saveDescription 
     * ____________________________________________________________________________
     * 
     * Self explanatory.
     * 
     */



    function saveDescription (field) {

            field = $(field);

            var _sync_id = field.attr('data-sync-id');


            // For tiny status...
            var tinyStatusSelector = field.attr('tinyStatus');
            if (typeof tinyStatusSelector === "string") { 
                $(tinyStatusSelector).html('Saving...').toggleClass('blink');
            }


            // For uni-label status...
            var 
            uniLabelStatus = field.attr('data-uni-label-status'), 
            uniLabel;
    
            if (typeof uniLabelStatus === "string") { 

                uniLabel = {
                    status: $(uniLabelStatus), 
                    field: field
                };
                uniLabelStatus = $(uniLabelStatus);
                field.attr('uni-label-title', uniLabelStatus.text());

                uniLabelStatus.html('<span class="blink">Saving...</span>');
                field.hide();
                uniLabelStatus.show();
            }




            var 

            _target_id = 0, 


            data = {
                text: field.val()
            },
            _do = 'void', 
            _doHold = true;




            switch (field.attr('name')) {
                case 'title':
                    _do = 'saveTitle';
                    _target_id = env.product_id;
                    break;
                case 'description':
                    _do = 'saveProductDetails';
                    _target_id = env.product_id;
                    break;
                case 'product-isbn':
                    _do = 'saveOptionISBN';
                    _target_id = env.product_id;
                    _doHold = false;
                    break;
                case 'product-ean':
                    _do = 'saveOptionEAN';
                    _target_id = env.product_id;
                    _doHold = false;
                    break;
                case 'option-sku':
                    _do = 'saveOptionSKU';
                    _target_id = field.attr('data-option-id');
                    _doHold = false;
                    break;
                case 'option-upc':
                    _do = 'saveOptionUPC';
                    _target_id = field.attr('data-option-id');
                    _doHold = false;
                    break;
                case 'option-price':
                    _do = 'savePrice';
                    _target_id = field.attr('data-price-id');
                    data.store_id = field.attr('data-store-id');
                    _doHold = false;
                    break;
                case 'option-stock':
                    _do = 'saveStock';
                    _target_id = field.attr('data-stock-id');
                    data.store_id = field.attr('data-store-id');
                    _doHold = false;
                    break;
                case 'options-price':
                    _do = 'savePricePlural';
                    _target_id = env.product_id;
                    data.option_id = env.optionsChecklist.getSelected();
                    data.store_id = field.attr('data-store-id');
                    _doHold = false;
                    break;
                case 'options-stock':
                    _do = 'saveStockPlural';
                    _target_id = env.product_id;
                    data.option_id = env.optionsChecklist.getSelected();
                    data.store_id = field.attr('data-store-id');
                    _doHold = false;
                    break;
                case 'new-attribute':
                    _do = 'saveNewAttribute';
                    _target_id = field.attr('data-attribute-type-id');
                    data.option_id = field.attr('data-option-id');
                    _doHold = false;
                    break;
                case 'add-attribute':
                    _do = 'addOptionAttribute';
                    _target_id = field.val();
                    data.option_id = field.attr('data-option-id');
                    _doHold = false;
                    break;
                default:
                    _do = 'void';
                    break;
            }



            ajax.doAPICall({
                api: 'product', 
                target_id: _target_id, 
                'do': _do, 
                data: data, 
                hold: _doHold,
                tinyStatus: tinyStatusSelector, 
                success: function (data) {
                    

                    switch (_do) {
                        case 'saveTitle': 

                        var item = document.getElementById('product-tile-'+data.Product.id);
                        item.querySelector('.product-tile-dashboard-wrapper > .header').innerHTML = data.Product.title;
                        env.dialogBox[0].querySelector('#product-dialog-title .item-name').innerHTML = data.Product.title;
                        
                        break;


                        case 'saveNewAttribute':

                            field.val('').hide();
                            var 

                            item = $('select[data-sync-id=' + _sync_id + ']'), 
                            attribute = $(Mustache.render('<option id="option-attribute-{{id}}" value="{{id}}">{{label}}: {{value}}</option>', data.Attribute));

                            attribute[0].selected = 'selected';

                            if (data.AddedToOption) {
                                $('#option_' + data.ProductOption.id + '.product-option-tile .product-option-title').html(data.ProductOption.description);
                            }

                            item
                            .find('optgroup').append(attribute)
                            .end().show();


                        break;

                        case 'addOptionAttribute':

                            $('#option_' + data.ProductOption.id + '.product-option-tile .product-option-title').html(data.ProductOption.description);

                        break;
                        
                        case 'saveStockPlural':
                            var _sid;
                                
                                data.Stock.forEach(function (v) {
                                    _sid = 'input[data-sync-id=' + _sync_id + '-' + v.id + '-' + v.store_id + ']';
                                    $(_sid).val(field.val());
                                    
                                    //console.log(_sid);
                                });
                        break;
                        
                        
                        case 'savePricePlural':
                            var _sid;

                                data.Prices.forEach(function (v) {
                                    _sid = 'input[data-sync-id=' + _sync_id + '-' + v.id + '-' + v.store_id + ']';
                                    $(_sid).val(field.val());

                                    //console.log(_sid);
                                });
                        break;
                        
                        

                    }

                        // update uni-label status...
                        if (
                                typeof uniLabelStatus !== 'undefined' 
                                && typeof uniLabelStatus !== 'string') {
                                    uniLabelStatus
                                            .html('<span class="saved">Saved &nbsp; &nbsp; &nbsp;</span>');
                                            // .children('span.saved').fadeOut(200);

                                    setTimeout(function () {
                                            uniLabelStatus.hide();
                                            field.show();
                                        }, 1000);
                                }


                        // re-sync...

                        if (typeof _sync_id === 'string' ) {
                            var v = field.val();
                            $('input[data-sync-id=' + _sync_id + ']').val(v);

                        } 
                        



                        return false;

                        },

                error: function (xhr) {
                        return false;
                }
            });

            return false;


    }







    /*
     * 
     * [ FUNCTION ] productSearch 
     * ____________________________________________________________________________
     * 
     * Self explanatory.
     * 
     */



    var _last_search_req = new Date ();

    function productSearch (term, callBack) {

        term = $.trim(term);

        if(term.length > 2) {


            var _new_req = new Date ();

            if (_new_req.getTime() < (_last_search_req.getTime()+1000)) { 
                // if (typeof callBack === 'function') callBack({count: 0}); 
                return false; 
            }
            else _last_search_req = new Date ();

            var data = {
                text: term
            };
            var _do = 'search';


            ajax.doAPICall({
                api: 'product', 
                target_id: env.product_id, 
                'do': _do, 
                data: data, 
                tinyStatus: '#void', 
                success: function (data) {
                        if (typeof callBack === 'function') callBack(data);
                        return false;

                        },

                error: function (xhr) {
                        return false;
                }
            });


        } else { 
                if (typeof callBack === 'function') callBack({count: 0}); 
                return false; 
        }

        return false;

    }









    /*
     * 
     * [ FUNCTION ] renderProductGroupMenu 
     * ____________________________________________________________________________
     * 
     * Self explanatory.
     * 
     */


    function renderProductGroupMenu (data) {
            var _menu = $('#frame-7 .product-group-menu');
            var _grp_member_items = $('#frame-7 .group-member-items');

            _menu.html('');

            var 
            _search_item = '<a href="javascript:void(0);" onclick="productWidget.addToProductGroup({{Product.id}})"><li>';
            _search_item += '<span class="user-dp tiny fltlft" ><img src="{{Product.primaryImage.hrefs.thumb}}"/></span><span class="item-name fltlft">{{Product.title}}</span><span class="reference-code">{{Product.referenceCode}}</span><br style="clear: both" />';
            _search_item += '</li></a>';

            if (data.count > 0) {

                for (i in data.Products) { 
                    _menu.prepend(Mustache.render(_search_item, {Product: data.Products[i]}));
                }

                _grp_member_items.hide();
                _menu.show();

            } else { 

                _menu.hide(); 
                _grp_member_items.show();	
            }

    }











    /*
     * 
     * [ saveBaseImage ]
     * 
     */




    function saveBaseImage (_options) {
        uploader_engaged = true;


        ajax.doAPICall({
            api: 'product', 
            target_id: env.upload_target_id, 
            'do': 'saveBaseImage', 
            data: _options, 
            success: function(data) {
                    uploader_engaged = false;

                    release(); //alert('Saved', data.message, true);

                    var image_button = $(Mustache.render(ajax.getTemplate('product-base-image-picker-thumb'), data.Image));
                    $('#base-image-picker-wrapper').append(image_button);
                    image_button.css('cursor', openHandCursor).draggable({
                                                            revert: 'invalid', 
                                                            revertDuration: 200,
                                                            appendTo: "body", 
                                                            containment: 'document',  
                                                            opacity: 0.5, 
                                                            zIndex: 12700, 
                                                            helper: "clone"

                                                            });

                   var photo_widget_thumb = $(Mustache.render(ajax.getTemplate('product-photo-widget-thumb'), data.Image));
                   $('#product-photo-widget-wrapper .product-photo-widget-thumb-add-button-a').before(photo_widget_thumb);

                   if (data.Image.is_primary) {
                       console.log('Set primary? yes');


                        $( "#base-image-preview" ).css('background-image', 'url(' + image_button.attr('image_src_l') + ')');
                        $('#base-image-drop-label').hide();

                        var ex_queen = $( "#base-image-preview" ).attr('active_image_button');
                        ex_queen = $('#'+ex_queen);

                        ex_queen.fadeIn();

                        image_button.hide();
                        $( "#base-image-preview" ).attr('active_image_button', image_button.attr('id'));



                        $('#product-tile-'+env.product_id).find('.product-thumb-wrapper > img').prop('src', image_button.attr('image_src_l'));

                   }


            }, 
            error: function (xhr) {


            }



            });


            return false;	

    }







    /*
     * 
     * [ FUNCTION ] addToProductGroup 
     * ____________________________________________________________________________
     * 
     * Self explanatory.
     * 
     */


    function addToProductGroup (product_id) {


                                alert('hey hey');
            var data = {
                product_id: product_id
            };
            var _do = 'addToProductGroup';
            var _grp_member_items = $('#frame-7 .group-member-items');

            env.dialogBox.find('#frame-7 .product-group-menu').html('').hide();
            env.dialogBox.find('#frame-7 .quick-search-term').val('');

            ajax.doAPICall({
                api: 'product', 
                target_id: env.product_id, 
                'do': _do, 
                data: data, 
                // tinyStatus: '#void', 
                success: function (data) {

                                console.log(data);

                        if (!data.Duplicate) {


                                var 
                                _item = '<li class="editable-list-tile product-group-member-tile trashable" id="member-product-{{id}}"  trash_type="product-group-member-tile" data_product_id="{{id}}" data_title="{{title}}">';
                                _item += '<span class="user-dp tiny fltlft" ><img src="{{primaryImage.hrefs.thumb}}"/></span><span class="item-name">{{title}}</span><span class="reference-code">{{referenceCode}}</span>';
                                _item += '<a class="button-1 editable-list-delete-button fltrt" href="javascript:void(0)" onclick="productWidget.removeFromProductGroup({{id}});" >remove</a></li>';

                                // if (typeof callBack === 'function') callBack(data);
                                var new_tile = $(Mustache.render(_item, data.MemberProduct));
                                new_tile.mouseenter(function () {
                                             $(this).find('.button-1').fadeIn('fast');
                                         })
                                         .mouseleave(function () {
                                             $(this).find('.button-1').hide();
                                         })
                                        .css('cursor', openHandCursor).draggable({
                                                                                revert: 'invalid', 
                                                                                revertDuration: 200,
                                                                                appendTo: "body", 
                                                                                containment: 'document',  
                                                                                opacity: 0.5, 
                                                                                zIndex: 12700, 
                                                                                helper: "clone"
                                                                                });   


                            _grp_member_items.prepend(new_tile).show();

                        }

                        return false;

                        },

                error: function (xhr) {
                        return false;
                }
            });



        return false;

    }









    /*
     * 
     * [ FUNCTION ] removeFromProductGroup 
     * ____________________________________________________________________________
     * 
     * Self explanatory.
     * 
     */


    function removeFromProductGroup (product_id) {


            var data = {
                product_id: product_id
            };
            var _do = 'removeFromProductGroup';


            $('#frame-7 .product-group-menu').html('').hide();

            ajax.doAPICall({
                api: 'product', 
                target_id: env.product_id, 
                'do': _do, 
                data: data, 
                // tinyStatus: '#void', 
                success: function (data) {
                        // if (typeof callBack === 'function') callBack(data);
                        $('#frame-7 .group-member-items > li#member-product-'+data.MemberProduct.id).remove();
                        return false;

                        },

                error: function (xhr) {
                        return false;
                }
            });



        return false;

    }








    /*
     * 
     * [ saveOptionPreview ]
     * 
     */




    function saveOptionPreview (_options) {
        uploader_engaged = true;
        
        _options.option_id = Array.isArray(env.upload_target_id) ? env.upload_target_id : [env.upload_target_id];


        ajax.doAPICall({
            api: 'product', 
            target_id: _options.option_id[0], 
            'do': 'saveProductOptionImage', 
            data: _options, 
            success: function(data) {
                    uploader_engaged = false;

                    release(); //alert('Saved', data.message, true);


                    $( "#variant-photo-preview-status").html('Saved...').toggleClass('blink').delay(800).fadeOut(300);
                    $( "#variant-photo-preview" ).css('background-image', 'url(' + data.Image.hrefs.large + ')');
                    $('#variant-photo-drop-label').hide();


            }, 
            error: function (xhr) {


            }



            });


            return false;	

    }








    /*
     * 
     * [ removeOptionPreview ]
     * 
     */




    function removeOptionPreviewPhoto (_id) {
        
        var _options = { 
            option_id: (env.optionsChecklist.count() > 0) ? env.optionsChecklist.getSelected() : [_id]
        };


        ajax.doAPICall({
            api: 'product', 
            target_id: _options.option_id[0], 
            'do': 'removeProductOptionImage', 
            data: _options, 
            success: function(data) {
                    
                    release(); //alert('Saved', data.message, true);


                    $( "#variant-photo-preview-status").html('Saved...').toggleClass('blink').delay(800).fadeOut(300);
                    $( "#variant-photo-preview" ).css('background-image', 'url(' + data.Image.hrefs.large + ')');
                    $('#variant-photo-drop-label').show();


            }, 
            error: function (xhr) {


            }



            });


            return false;	

    }










    /*
     *
     * public: [ setPrimaryBaseImage ]
     *________________________________________________________
     *
     *
     *
     */





    function setPrimaryBaseImage (image_button) {

        $( "#base-image-preview" ).css('background-image', 'url(../ui/images/transparent_pixel.gif)');
        $( "#base-image-preview-status").html('Saving...').toggleClass('blink').show();

        var image_id = (typeof image_button === 'object') ? image_button.draggable.attr('image_id') : image_button;
        if ($('#product-photo-widget-wrapper').is(':visible')) { 
                $('#product-photo-widget-wrapper').fadeOut(200);
        }

        ajax.doAPICall({
            api: 'product', 
            target_id: env.product_id, 
            'do': 'setPrimaryBaseImage', 
            data: { 'image_id': image_id }, 
            success: function(data) {

                    var image_button = $( "#base_img_" +  data.Image.id);

                    $( "#base-image-preview-status").html('Saved...').toggleClass('blink').delay(800).fadeOut(300);
                    $( "#base-image-preview" ).css('background-image', 'url(' + data.Image.hrefs.large + ')');
                    $('#base-image-drop-label').hide();

                    var ex_queen = $( "#base-image-preview" ).attr('active_image_button');
                    ex_queen = $('#'+ex_queen);

                    ex_queen.fadeIn();

                    image_button.hide();
                    $( "#base-image-preview" ).attr('active_image_button', "base_img_" +  data.Image.id);


                    $( "#base-image-preview .quick-remove-button" ).css({ display: 'block' });
                    $('#product-tile-'+data.product_id).find('.product-thumb-wrapper > img').prop('src', data.Image.hrefs.large);

                     env.dialogBox.find('#product-dialog-title img').prop('src', data.Image.hrefs.large);

                    }, 
            error: function (data) {
                    image_button.draggable.show();

            }

            });

    }











    /*
     *
     * public: [ removePrimaryBaseImage ]
     *________________________________________________________
     *
     *
     *
     */





    function removePrimaryBaseImage () {

        $( "#base-image-preview" ).css('background-image', 'url(../ui/images/transparent_pixel.gif)');
        $( "#base-image-preview-status").html('Saving...').toggleClass('blink').show();
        $( "#base-image-preview .quick-remove-button" ).css({ display: 'none' });
        
        $('#base-image-drop-label').show();

        

        ajax.doAPICall({
            api: 'product', 
            target_id: env.product_id, 
            'do': 'removePrimaryBaseImage', 
            data: { product_id: env.product_id }, 
            success: function(data) {

                    
                    $( "#base-image-preview-status").html('Saved...').toggleClass('blink').delay(800).fadeOut(300);
                    // $( "#base-image-preview" ).css('background-image', 'url(' + data.Image.hrefs.large + ')');
                    // $('#base-image-drop-label').hide();

                    var ex_queen = $( "#base-image-preview" ).attr('active_image_button');
                    ex_queen = $('#'+ex_queen);

                    ex_queen.fadeIn();

                    

                    $('#product-tile-'+data.product_id)
                            .find('.product-thumb-wrapper > img')
                            .prop('src', 'ui/images/transparent_pixel.gif');

                     env.dialogBox
                             .find('#product-dialog-title img')
                             .prop('src', 'ui/images/transparent_pixel.gif');

                    }, 
            error: function (data) {
                    $( "#base-image-preview" ).css('background-image', 'url(' + $( "#base-image-preview" ).attr('active_image_button') + ')');
                    $('#base-image-drop-label').hide();

                    $( "#base-image-preview .quick-remove-button" ).css({ display: 'block' });
            }

            });

    }








    /*
     * 
     * [ FUNCTION ] deleteImage 
     * ____________________________________________________________________________
     * 
     * 
     * 
     */


    function deleteImage (image_button) {


            image_button = (typeof image_button === 'object') ? image_button.draggable : $( "#base_img_" +  image_button);

            var image_button2 = $( "#base_img2_" +  image_button.attr('image_id'));
            image_button2.hide();



        ajax.doAPICall({
            api: 'product', 
            target_id: env.product_id, 
            'do': 'deleteBaseImage', 
            data: { 'image_id': image_button.attr('image_id') }, 
            success: function(data) {
                    image_button.remove();
                    image_button2.remove();

                    // If a new primary image was set (i.e. the deleted image was the primary image)...
                    if ($( "#base-image-preview" ).attr('active_image_button') !== "base_img_" + data.PrimaryImage.id) {

                        $( "#base-image-preview" )
                                .css('background-image', 'url(' + data.PrimaryImage.hrefs.large + ')')
                                .attr('active_image_button', "base_img_" +  data.PrimaryImage.id);
                        $('#product-tile-'+data.product_id).find('.product-thumb-wrapper > img').prop('src', data.PrimaryImage.hrefs.large);
                        $('#product-dialog-title img').prop('src', data.PrimaryImage.hrefs.large);

                    } 


                    if (data.PrimaryImage.id+'' === '0') {


                            $( "#base-image-preview" ).find('#base-image-drop-label, #base-image-uploader').show();


                     }



            }, 
            error: function (data) {

                    image_button.show();
                    image_button2.show();
            }

            });



    }









    /*
     * 
     * [ FUNCTION ] deleteProduct 
     * ____________________________________________________________________________
     * 
     * Self explanatory.
     * 
     */



    function deleteProduct (_id) {

            hold ('Please wait...');
            if (typeof _id === 'undefined' && typeof env.product_id !== 'undefined') _id = env.product_id;



            var query = {
                auth_token: _auth_token,
                product_id: [_id], 
                'do': 'deleteProduct', 
                target_id: 0
            };





            var action = 'catalog-api.ajax.php';

            ajax.jsonRequest ({
                    method: 'POST', 
                    url: action, 
                    data: query, 
                    dataType: 'json', 
                    success: function (response) {
                            
                            var data = response.json();
                                window.location = 'landing.catalog.php';
                                return false;

                            },

                    error: function (xhr) {
                        return false;
                    }
            });

            return false; 

    }













    /*
     * 
     * [ FUNCTION ] editProductOption 
     * ____________________________________________________________________________
     * 
     * Self explanatory.
     * 
     */



    function editProductOption (_id, _callBack) {


            hold('Please wait...');
            
            var _do, _target_id, _mode;
            
            if (Array.isArray(_id)) {
                _do = 'getProductInfo';
                _target_id = env.product_id;
                _mode = 'plural';
            } else {
                _do = (_id === 'new') ? 'newProductOption' : 'getProductOptionInfo';
                _target_id = _id;
                _mode = (_id === 'new') ? 'new' : 'edit';
            }

            ajax.doAPICall({
                api: 'product', 
                'do': _do,
                'target_id': _target_id,  
                'data': { 'void': 0 }, 
                'success': function (data) {

                    release();

                    renderProductOptionWidget(data, _mode, _callBack);    




                    }
            });

    }







    /*
     * 
     * [ FUNCTION ] renderProductOptionWidget 
     * ____________________________________________________________________________
     * 
     * Self explanatory.
     * 
     */



    function renderProductOptionWidget (data, _mode, _callBack) {

                    var html = (_mode === 'plural') 
                    ? Mustache.render(ajax.getTemplate('product-option-widget.plural'), data.Product)  
                    : Mustache.render(ajax.getTemplate('product-option-widget'), { 'ProductOption': data.ProductOption });
                    
                    
                    __el(document.getElementById('new-item-pane')).appendChild(html);
                    
                    var _productOptionWidget = __el('#product-variant-widget-wrapper');
                    
                    
                    //return;
                     

                    // Show dialog box... var _productOptionWidget =
                    //document.getElementById('new-item-pane').appendChild(_productOptionWidget.elements[0]);

                    
                    
                    if (_mode === 'new') {
                        __el('#product-options-checklist-container').prependChild(Mustache.render(
                                    ajax.getTemplate('product-option-list-item'), 
                                    data.ProductOption));

                            initProductOptions();
                    }
                    


                    /*
                     * 
                     * PRODUCT VARIANT WIDGET INIT
                     * 

                     */
                    


                    var closeProductOptionWidget = function closeProductOptionWidget () {
                        _productOptionWidget.fadeOut('block', function () { 

                            $("#variant-photo-uploader.droppable-uploader-area").dropper('destroy').off('.dropper');
                            _productOptionWidget.remove();  

                        });




                        MyShortcuts.clear('cancel', closeProductOptionWidget);
                        _callBack();
                    };

                    var _lastSelectedIndex = 0;


                    // PRICE, STOCK, SKU and UPC fields..
                    var fields = document.querySelector('.productWidgetSlider')
                    .querySelectorAll('input.option-price-field, input.option-stock-field, input.option-sku-field, input.option-upc-field');
                    
                    
                    __el(fields).addEventListener('focus', uniLabelInputFocusHandler);



                    
                    // ATTRIBUTES (SELECT CONTROLS)...
                    __el('.option-attribute-type-field')
                    .addEventListener('focus', function () {
                        _lastSelectedIndex = this.selectedIndex;
                    })
                    .addEventListener('change', function () {
                        var This = __el(this);


                        if (this.value === 'placeholder') return;


                        if (this.value === 'new') { 
                            This.hide();
                            var Helper = __el(this.getAttribute('data-helper-id'));

                            function cancelAttributeSave () {
                                Helper
                                .removeEventListener('blur', cancelAttributeSave)
                                .removeEventListener('change', doAttributeSave)
                                .hide();
                        
                                Helper.elements[0].blur();
                                Helper.elements[0].value = '';

                                this.selectedIndex = _lastSelectedIndex;
                                This.show();

                                MyShortcuts.clear('return', doAttributeSave);
                                MyShortcuts.clear('cancel', cancelAttributeSave);
                            }

                            function doAttributeSave () {
                                Helper
                                .removeEventListener('blur', cancelAttributeSave)
                                .removeEventListener('change', doAttributeSave);

                                if (Helper.elements[0].value.length > 0) {
                                    productWidget.saveDescription(Helper.elements[0]);  
                                } else {

                                    Helper.hide();

                                    this.selectedIndex = _lastSelectedIndex;
                                    This.show();

                                }

                                MyShortcuts.clear('return', doAttributeSave);
                                MyShortcuts.clear('cancel', cancelAttributeSave);
                            }

                            MyShortcuts.register('return', doAttributeSave, {'disable_in_input':false});
                            MyShortcuts.register('cancel', cancelAttributeSave, {'disable_in_input':false});



                            Helper.show();
                            
                            Helper.elements[0].focus();
                            
                            Helper
                            .addEventListener('blur', cancelAttributeSave)
                            .addEventListener('change', doAttributeSave);
                    
                        } 

                        else {
                            productWidget.saveDescription(this);
                        }

                    }); 

                    
                   
                   
                    _productOptionWidget.show();
                    
                    

                    MyShortcuts.register('cancel', closeProductOptionWidget);
                    
                    __el('#product-variant-widget-wrapper .cancel-button')
                    .addEventListener('click', closeProductOptionWidget);










                    $("#variant-photo-uploader.droppable-uploader-area").dropper({
                            action: $('#meta_admin_root').val() + "upload.ajax.php", 
                            postKey: 'photo_uploader', 
                            maxSize: 3048576, 
                            postData: { 
                                'auth_token': _auth_token, 
                                'pw': '600', 
                                'ph': '400', 
                                'tw': '400', 
                                'th': '600'
                            }, 
                            label: ' '
                    }).on("fileComplete.dropper", function (e, file, data) {
                        var target = $(e.target);



                        console.log('Target:', target);

                        switch (target.attr('data-target')) {
                            case 'product_option':
                                env.upload_target_id = target.attr('data-option-id');
                                break;
                                
                            case 'product_option_plural':
                                env.upload_target_id = env.optionsChecklist.getSelected();
                                break;
                                
                        }


                        PhotoUploader.launchPhotoEditor(data, { 
                            saveHandler: saveOptionPreview, 
                            uploadTargetID: env.upload_target_id,  
                            setPrimaryImage: false, 
                            tw: 400, 
                            th: 600
                        });
                    })
                    .on("fileError.dropper", function (e, file, error) {
                        console.log("Error: " + error);
                    }).on("dragenter.dropper", function (e) {

                            var _previewBox = env.dialogBox.find( "#variant-photo-preview" );

                            _previewBox
                                    .addClass('active')
                                    .attr('bg_src_cache', _previewBox.css('background-image'))
                                                        .css('background-image', 'url(ui/images/transparent_pixel.gif)')
                                                        .find('#variant-photo-drop-label')
                                                        .show()
                                                        .end();



                        }).on("dragleave.dropper", function (e) {

                            var _previewBox = env.dialogBox.find( "#variant-photo-preview" );

                            _previewBox
                                    .removeClass('active')
                                    .css('background-image', _previewBox.attr('bg_src_cache'))
                                                        .find('#variant-photo-drop-label')
                                                        .hide()
                                                        .end();
                        });



    }





    /*
     * 
     * [ FUNCTION ] deleteProductOption 
     * ____________________________________________________________________________
     * 
     * Self explanatory.
     * 
     */



    function deleteProductOption (_id, _onSuccess) {

            hold ('Please wait...');


            _id = Array.isArray(_id) ? _id : Array (_id);




            ajax.doAPICall({
                api: 'product', 
                target_id: env.product_id, 
                'do': 'deleteProductOption', 
                data: { 'option_id': _id }, 
                success: function(data) {

                    if (typeof _onSuccess === 'function') _onSuccess (data);

                    var toRemove = Array ();
                    data.ProductOptions.forEach(function (v) {
                        toRemove.push('.product-option-tile#option_' + v.id);
                    });


                    env.dialogBox.find(toRemove.join(', ')).remove();

                }, 
                error: function (data) {


                }

            });


            return false; 

    }























    /*
     * 
     * [ FUNCTION ] togglePickerTile 
     * ____________________________________________________________________________
     * 
     * Adds or removes a product .
     * 
     */



    function togglePickerTile (_tile) {

            var 

            _id = _tile.getAttribute('data_id'), 
            _type = _tile.getAttribute('data_type'), 
            _does; 

            switch (_type)
            {
                case 'category':
                _does = { add: 'addToCategory', remove: 'removeFromCategory'}
                break;

                case 'store':
                _does = { add: 'addToStore', remove: 'removeFromStore'};
                env.dialogBox.find('#frame-4').html('<span>Please wait...</span>');
                break;

                default:
                _does = { add: 'addToCategory', remove: 'removeFromCategory'}
                break; 

            }
            
            _tile = __el(_tile);
            
            var _do = (_tile.hasClass('picker-tile-select-state')) ? _does.remove : _does.add;
            _tile.toggleClass('picker-tile-select-state');
            


            ajax.doAPICall ({
            api: 'catalog', 
            data: { product_id: [env.product_id] },
            'do': _do, 
            target_id: _id, 
            success: function(data) {

                    switch (_type)
                    {
                        case 'category':
                            $('#category-population-'+data.Category.id).html(data.Category.population);
                            $('#store-population-'+data.Category.Store.id).html(data.Category.Store.population);
                            
                            
                        break;

                        case 'store':
                            $('#store-population-'+data.Store.id).html(data.Store.population);
                        break;

                        default:
                             // nothing
                        break; 

                    }
                    
                    

                    env.dialogBox.find('#frame-4').html(Mustache.render(
                            ajax.getTemplate('product-option-list'), 
                            { 'options': data.ProductOptions }, 
                            { 'product-option-list-item': ajax.getTemplate('product-option-list-item') }
                                    ));

                    initProductOptions();
                    

                    return false;

                    }, 
            error: function (data) {
                    _tile.toggleClass('picker-tile-select-state');
                    return false;
            }

            });

            return false;
    }









    
    
    
    
    //Return public methods
    return {
        
        refresh: loadData, 
        deleteProduct: deleteProduct,
        newProduct: newProduct, 
        editProduct: editProduct, 
        nextPane: nextPane, 
        prevPane: prevPane, 
        seekPane: seekPane, 
        saveDescription: saveDescription, 
        saveBaseImage: saveBaseImage, 
        setPrimaryBaseImage: setPrimaryBaseImage, 
        removePrimaryBaseImage: removePrimaryBaseImage, 
        saveOptionPreview: saveOptionPreview, 
        removeOptionPreviewPhoto: removeOptionPreviewPhoto, 
        
        editProductOption: editProductOption, 
        deleteProductOption: deleteProductOption, 
        
        deleteImage: deleteImage, 
        
        togglePickerTile: togglePickerTile, 
        
        renderProductGroupMenu: renderProductGroupMenu, 
        productSearch: productSearch, 
        addToProductGroup: addToProductGroup, 
        removeFromProductGroup: removeFromProductGroup, 
        
        makeTile: makeTile
        
    };
    
    
    
 };
 
 
 
