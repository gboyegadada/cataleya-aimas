
 
 
 
// ----------------------------------------------------------------------------------------- //

//var MyNotifications;
//
//$(document).ready(function () {
//    
//    if ($('#meta_token').length > 0) MyNotifications = Cataleya.loadMyNotifications();
// 
//});



// Class: [ loadMyNotifications ]
Cataleya.loadMyNotifications = function (callBack) {
    
    // Private properties
    
    var 
    
    _api = 'my-notifications';
    

    
    
    // Load moustache templates
    ajax.loadTemplates(['admin-notification-tile'], true);
    setTimeout(getNotifications, 600);



    
    
    
    function renderNotificationsBar (data) {


                    $('#notification-tiles-wrapper').html('');

                    for (var i in data.Notifications) {
                        var _tile = $(Mustache.render(ajax.getTemplate('admin-notification-tile'), data.Notifications[i]));  

                        // attach tile
                        $('#notification-tiles-wrapper').append(_tile);

                    }

    }
    
    
    
    function renderNotificationTile (_Notification) {


                    var _tile = $(Mustache.render(ajax.getTemplate('admin-notification-tile'), _Notification));  

                    // attach tile
                    var _old_tile = $('#notification-tiles-wrapper > li#notification-tile-' + _Notification.id);
                    
                    if (_old_tile.length > 0) _old_tile.replaceWith(_tile);
                    else $('#notification-tiles-wrapper').append(_tile);

    }
   

    
   
    


    // method: [ getNotifications ]
    function getNotifications () {
            

            ajax.doAPICall({
                api: _api, 
                'do': 'getNotifications', 
                data: { 'void': 0 }, 
                success: function (data) {

                    renderNotificationsBar({Notifications: data.Notifications});
                    return false;
                },

                error: function (xhr) {
                    return false;
                }
        });
        
        return false;
        
    }
    
    
    
    


    // method: [ getNotification ]
    function getNotification (_id) {
            

            ajax.doAPICall({
                api: _api, 
                'do': 'getNotifications', 
                data: { 'notification_id': _id }, 
                success: function (data) {

                    renderNotificationTile(data.Notification);
                    return false;
                },

                error: function (xhr) {
                    return false;
                }
        });
        
        return false;
        
    }
    
    

    // method: [ markDisplayed ]
    function markDisplayed (_id) {
            

            ajax.doAPICall({
                api: _api, 
                'do': 'markDisplayed', 
                data: { 'notification_id': _id }, 
                success: function (data) {

                    renderNotificationTile(data.Notification);
                    return false;
                },

                error: function (xhr) {
                    return false;
                }
        });
        
        return false;
        
    }
    
    
    
    
    
        
        

    // method: [ markNotDisplayed ]
    function markNotDisplayed (_id) {
            

            ajax.doAPICall({
                api: _api, 
                'do': 'markNotDisplayed', 
                data: { 'notification_id': _id }, 
                success: function (data) {

                    renderNotificationTile(data.Notification);
                    return false;
                },

                error: function (xhr) {
                    return false;
                }
        });
        
        return false;
        
    }
    
    
    

    // method: [ markRead ]
    function markRead (_id) {
            

            ajax.doAPICall({
                api: _api, 
                'do': 'markRead', 
                data: { 'notification_id': _id }, 
                success: function (data) {

                    renderNotificationTile(data.Notification);
                    return false;
                },

                error: function (xhr) {
                    return false;
                }
        });
        
        return false;
        
    }
    
    
    
    
    


    // method: [ markUnread ]
    function markUnread (_id) {
            

            ajax.doAPICall({
                api: _api, 
                'do': 'markUnread', 
                data: { 'notification_id': _id }, 
                success: function (data) {

                    renderNotificationTile(data.Notification);
                    return false;
                },

                error: function (xhr) {
                    return false;
                }
        });
        
        return false;
        
    }
    
    
    
    
    
    
    
    //Return public methods
    return {
        getNotifications: getNotifications, 
        getNotification: getNotification, 
        markDisplayed: markDisplayed, 
        markNotDisplayed: markNotDisplayed, 
        markRead: markRead, 
        markUnread: markUnread
        
    };
    
    
    
 }
 
