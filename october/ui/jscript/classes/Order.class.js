
 
 
// ----------------------------------------------------------------------------------------- //




// Class: [ loadOrder ]
Cataleya.loadOrder = function (_store_id, callBack) {
    
    // if (typeof _store_id != 'number') {throw new Exception ('Invalid layout id!'); return false; }
    
    
    // Private properties
    
    var 
    
    _dialogBox, 
    _order_id = parseInt(_store_id), 
    _a = Cataleya.arrayFuncs(), 
    Utility = Cataleya.getUtility(); 
    
    
    
    // Load moustache templates
    ajax.loadTemplates(['order-expanded-view', 'order-email-dialog', 'order-tile'], true);


    
    // method: [ makeTile ]
    function makeTile (_data) {
       return $(Mustache.render(ajax.getTemplate('order-tile'), { Order: _data })); 
    } 
    
    
    
    
    
    function renderDialog (data) {


                    $('.order-expanded-view-box').remove(); // remove existing dialog boxes...
                    

                    _dialogBox = $(Mustache.render(ajax.getTemplate('order-expanded-view'), data));  

                    // Show dialog box...
                    $('body').append(_dialogBox);
                    

//                    _dialogBox.find('li.layout-tile')
//                    .mouseenter(function () {
//                        $(this).find('.layout-tile-tool-box, .layout-tile-dashboard-wrapper').fadeIn(200);
//                    })
//                    .mouseleave(function () {
//                        $(this).find('.layout-tile-tool-box, .layout-tile-dashboard-wrapper').fadeOut(200);
//                    });

                    $('#fullscreen-bg3, .order-expanded-view-box > .cancel-button').unbind('click').click(function () {

                       _dialogBox.hide().remove();
                           
                       $('#fullscreen-bg3').unbind('click').hide();
                       $(document).unbind('scroll'); 
                       $('body').css({'overflow':'visible'});	

                    })
                    .show();
                    
                   
                    _dialogBox.show();
                    

                    

    }
    
    
    
    


    
    
    /*
     * 
     * [ FUNCTION ] newTile
     * ____________________________________________________________________________
     * 
     * Removes a product tag.
     * 
     */


     function editTile(_id) {
         var 
         
         Tile, 
         _do, 
         _payload = {};
         
         
        if (typeof _id == 'number') {
            
            _payload = {
                title: 'Edit Layout Tile', 
                description: 'Choose a width and a height (in pixels)', 
                buttonLabel: 'save changes', 
                width: [], 
                height: []
            };
            
            _do = 'saveTile';
            
            Tile = $('#layout-tile-'+_id).data('source');
            
        } else {
            _payload = {
                title: 'New Layout Tile', 
                description: 'Choose a width and a height (in pixels)', 
                buttonLabel: 'create new tile', 
                width: [], 
                height: []
            };
            
            _id = _order_id;
            _do = 'newTile';
            Tile = {pixel_width: 280, pixel_height: 25}
        }
        
        
        // generate width options
         for (var w = 280; w <= (140*6); w+=140) {
             _payload.width.push ({
                 value: w, 
                 label: w + 'px', 
                 selected: (Tile.pixel_width == w) ? 'selected' : ''
             });
         }
         
         
        // generate height options
         for (var h = 25; h < (25*40); h+=25) {
             _payload.height.push ({
                 value: h, 
                 label: h + 'px', 
                 selected: (Tile.pixel_height == h) ? 'selected' : ''
             });
         }
         
         


        renderTileDialog ({
            _do: false, 
            callBack: function (_data) {
                        
                        ajax.doAPICall({
                            api: 'order', 
                            target_id: _id, 
                            'do': _do, 
                            data: _data, 
                            success: function (data) {
                                
                                renderTile(data.Tile);
                                

                                return false;
                            },

                            error: function (xhr) {
                                return false;
                            }
                        });
            },

            payload: _payload

        });

        return false;

     }
     
     
     
     


  
    
    
    

    
    /*
     * 
     * [ FUNCTION ] sendEmail
     * ____________________________________________________________________________
     * 
     * 
     * 
     */
    
    function sendEmail(_id) {

        var data = Utility.getFormData('#order-email-form');
        
        $('#order-email-dialog-box').hide();
        


        ajax.doAPICall({
            api: 'order', 
            target_id: _id, 
            'do': 'sendEmail', 
            data: data, 
            success: function (data) {

                
                if (typeof data.failedItems != 'undefined' && data.failedItems.length > 0) {
                    $('#fullscreen-bg3, #order-email-dialog-box').show();
                    var field = $('#order-email-dialog-box').find('input[name=' + data.failedItems[0] + ']') ;
                    if (field.length == 0) field = $('select[name=' + data.failedItems[0] + ']') ;

                    if (field.length > 0) {
                            showTip(field);
                            $(field)
                            .focus()
                            .one('blur', function () { hideTip(); });
                    }
                    
                    return false;
                }
                
                $('#order-email-dialog-box').hide();
                if (typeof _dialogBox !== 'object' || !_dialogBox.is(":visible")) $('#fullscreen-bg3').hide();
                $('#order-email-dialog-box').remove();
                alert('Your email has been sent !');
                

                return false;
            },

            error: function (xhr) {
                $('#order-email-dialog-box').show();
                return false;
            }
        });


        return false;
    }


    
    



   
    





    // method: [ composeEmail ]
    function composeEmail (_id) {
            if (typeof _id !== 'number') {throw new Exception ('Invalid order id!'); return false; }
            
            var _options = {
                Order: { id: _id, orderNum: String('00000'+_id).slice(-5) }, 
                meta: { title: 'Compose Email', buttonLabel: 'send email'}
            };
            
            var dialogBox = $(Mustache.render(ajax.getTemplate('order-email-dialog'), _options));  

            // Show dialog box...
            $('body').append(dialogBox);
            dialogBox.show();
            
            
             
             $('#fullscreen-bg3').css({'z-index': 10009});
             $('#order-email-dialog-box').css({'z-index': 10010});
             $('#fullscreen-bg3, #order-email-dialog-box > .cancel-button').unbind('click').click(function () {
                
                hideTip(true);

                dialogBox.fadeOut(200, function () {
                    $(this).remove();
                    if (typeof _dialogBox !== 'object' || !_dialogBox.is(":visible")) $('#fullscreen-bg3').css({'z-index': 9999}).hide();
                    else $('#fullscreen-bg3').css({'z-index': 9999});
                });
                $(document).unbind('scroll'); 
 		$('body').css({'overflow':'visible'});	

             })
             .show();
             
//             
//             dialogBox.find('#field-to')
//             .tokenInput(
//                'contact-search.ajax.php', 
//                {
//                    method: 'POST', 
//                    queryParam: 'term', 
//                    searchDelay: 500, 
//                    minChars: 3, 
//                    propertyToSearch: 'name', 
//                    jsonContainer: 'Tokens', 
//                    resultsLimit: 10, 
//                    tokenLimit: 5, 
//                    preventDuplicates: true, 
//                    tokenValue: 'name', 
//                    theme: 'mac'
//                    
//                });
             

             return false;
        
    }
    
    
    
    
    


    // method: [ expandOrder ]
    function expandOrder (id) {
            
            if (typeof id !== 'number') {throw new Exception ('Invalid order id!'); return false; }
            
            _order_id = parseInt(id);
            ajax.doAPICall({
                api: 'order', 
                target_id: _order_id, 
                'do': 'getOrder', 
                data: { 'void': 0 }, 
                success: function (data) {
                    var _meta = {
                        title: 'Edit Layout', 
                        buttonLabel: 'done'
                    };

                    renderDialog({meta: _meta, Order: data.Order});
                    return false;
                },

                error: function (xhr) {
                    return false;
                }
        });
        
        return false;
        
    }
    
    
    



    // method: [ contactSearch ]
    function contactSearch () {
            

            ajax.doAPICall({
                api: 'order', 
                target_id: 0, 
                'do': 'getOrder', 
                data: { 'void': 0 }, 
                success: function (data) {
                    var _meta = {
                        title: 'Edit Layout', 
                        buttonLabel: 'done'
                    };

                    renderDialog({meta: _meta, Order: data.Order});
                    return false;
                },

                error: function (xhr) {
                    return false;
                }
        });
        
        return false;
        
    }
    
    
    
    
    

    
    
    
    
    //Return public methods
    return {
        sendEmail: sendEmail, 
        composeEmail: composeEmail, 
        expand: expandOrder, 
        makeTile: makeTile
        
    };
    
    
    
 }
 
 
 