
 
 
 
 // ------------------------------------------------------------------------------------ //



// Class: [ TaxRules ]
Cataleya.loadTaxRules = function (_type, callBack) {
    
    
    // if _type is invalid...
    if (typeof _type !== 'string') return {};
    
    // Private property
    var _collection = [ /* tax rules */ ];
    
    
    
    var query = {};



    loadData();


    // method: [ refresh ]
    function loadData (callBack) {
        
	ajax.jsonRequest ({
		method: 'POST', 
		url: 'get-tax-rules.ajax.php', 
		data: {auth_token: $('#meta_token').val(), tax_type: _type }, 
		dataType: 'json', 
		success: function (response) {
                            
                            var data = response.json();
                    
                            setData(data.TaxRules);  
                            if (typeof callBack == 'function') callBack();

                            return false;
                        
			},
			
		error: function (xhr) {
                    
                    return false;
                }
        });
    }
    
    
    
    // private method: [ setData ]
    function setData (data) {
        _collection = data;
    } 
    
    
    
    
    
    
    // method: [ getTaxRule ]
    function getTaxRule (id) {
        for (var i = 0; i < _collection.length; i++) {
            if (_collection[i].id == id) return _collection[i];
        }
        
        return {};
    } 
    
    
    
    
    // method: [ getTaxRules ]
    function getTaxRules () {
        return _collection;
    } 
    
    
    


    
    
    // method: [ removeTaxRule ]
    function removeTaxRule (_id) {
            
            hold ('Please wait...');
            
            query = {
                    auth_token: $('#meta_token').val(),
                    tax_rule_id: (typeof query.tax_rule_id == 'undefined' || query.tax_rule_id == 0) ? _id : query.tax_rule_id

                    };




            // Do request...
            ajax.jsonRequest ({
                    method: 'POST', 
                    url: 'delete-tax-rule.ajax.php', 
                    data: query, 
                    dataType: 'json', 
                    success: function (response) {
                            
                            var data = response.json();
                                    $('#tax-rule-'+data.TaxRule.id).detach();
                                    
                                    var population1 = (data.TaxRules.population == 1) ? 'is ' : 'are ';
                                    population1 += data.TaxRules.populationAsText;

                                    var population2 = (data.TaxRules.population > 0) ? '(' + data.TaxRules.population + ')' : '';

                                    $('.population-count-1').html(population1);
                                    $('.population-count-2').html(population2);
                                    
                                    query.tax_rule_id = 0; // reset id
                                    
                                    // Update cache and bring it back
                                    loadData(release);

                                    return false;

                            },

                    error: function (xhr) {

                                //errorHandler(xhr, '', function () {});
                                query.tax_rule_id = 0; // reset id
                                return false;
                            }
            });
        }
        
        
        
        
        
        
    /*
     *
     * private method: [ renderDialog ]
     * 
     */

    function renderDialog (_tax_rule_id) {
            
            

            // Remove previous click events...
            $('#edit-tax-rule-dialog-box #edit-tax-rule-button-cancel, #fullscreen-bg2').unbind('click');

            var dialogBox = $('#edit-tax-rule-dialog-box');

            // Set up dialog box...
            if (_tax_rule_id == 0) {

                // NEW
                dialogBox.find('.bubble-title').html('New Tax Rate');
                $('#edit-tax-rule-dialog-hint').html('Enter a name, zone (choose) and a percentage charge for your new tax rule.');
                
                $('#tax-rule-id').val(0);
                $('#text-field-tax-rule-zone-id option[value=0]').attr('selected', true);
                $('#text-field-tax-rule-rate').val(parseFloat('0', 10).toFixed(2));
                $('#edit-tax-rule-button-ok').val('add new tax rule');

            } else {

                // EDIT
                dialogBox.find('.bubble-title').html('Edit Tax Rate');
                $('#edit-tax-rule-dialog-hint').html('Enter a name, zone (choose) and a percentage charge then click save.');
                
                $('#tax-rule-id').val(_tax_rule_id);

                var TaxRule = TaxRules.getTaxRule(_tax_rule_id);
                $('#text-field-tax-rule-name').val(TaxRule.name);
                $('#text-field-tax-rule-zone-id option[value='+TaxRule.zone.id+']').attr('selected', true);
                $('#text-field-tax-rule-rate').val(parseFloat(TaxRule.rate, 10).toFixed(2));
                
                $('#edit-tax-rule-button-ok').val('save changes');
            }

            // Init dialog 'cancel' button...
            $('#edit-tax-rule-dialog-box #edit-tax-rule-button-cancel, #fullscreen-bg2').click(function () {
                    $('#edit-tax-rule-dialog-box, #fullscreen-bg2').fadeOut(300);
                    hideTip();
                    return false;
            });

            // Show dialog box...
            $('#edit-tax-rule-dialog-box, #fullscreen-bg2').show();



    }
    
    
    // method: [ sendRequest ]
    function sendRequest () {
        
            // Validate form
            var rateField = $('#text-field-tax-rule-rate');
            var nameField = $('#text-field-tax-rule-name');
            var zoneField = $('#text-field-tax-rule-zone-id');


            if (zoneField.val() === 0) {
                showTip(zoneField);
                zoneField
                .one('focus', function () { hideTip(); });

                return false;
            }
            
//            if (!isFloat(rateField.val())) {
//                showTip(rateField);
//                rateField
//                .one('focus', function () { hideTip(); });
//
//                return false;
//            }
            
            if (NAME_REGEXP.test(nameField.val()) || nameField.val() == '') {
                showTip(nameField);
                nameField
                .focus()
                .one('blur', function () { hideTip(); });

                return false;
            }
            
            

            $('#edit-tax-rule-dialog-box').hide();
            hold('Adding new province...');

            ajax.jsonRequest ({
                    method: 'POST', 
                    url: $('#edit-tax-rule-form').prop('action'), 
                    data: $('#edit-tax-rule-form').serialize(), 
                    dataType: 'json', 
                    success: function (response) {
                            
                            var data = response.json();


                              // Then add new tile...
                              var new_tile = $('<li class="tax-rule-tile trashable" id="tax-rule-' + data.TaxRule.id + '" trash_type="tax-rule-tile"  data_tax_rule_id="' + data.TaxRule.id + 
                                   '"  data_tax_rule_name="' + data.TaxRule.name + '" data_tax_rate="' + data.TaxRule.rate + '" >' + 
                                   data.TaxRule.name + ' (' + data.TaxRule.rate + '%)' +
                                   '<a class="button-1 tax-rule-edit-button fltrt" href="javascript:void(0)" onclick="TaxRules.editTaxRule(' + data.TaxRule.id + '); return false;" >edit</a>' + 
                                    '<a class="button-1 tax-rule-delete-button fltrt" href="javascript:void(0)" onclick="TaxRules.removeTaxRule(' + data.TaxRule.id + ');" >remove</a></li>'
                                    );



                               new_tile.mouseenter(function () {
                                            $(this).find('.button-1').fadeIn('fast');
                                        })
                                        .mouseleave(function () {
                                            $(this).find('.button-1').hide();
                                        });



                               // Insert OR replace new tile...
                                var oldTile = $('#tax-rule-'+data.TaxRule.id);

                                if (oldTile.length > 0 && oldTile.prev('.tax-rule-tile').length > 0) oldTile.prev('.tax-rule-tile').after(new_tile);
                                else $('#tax-rule-add-button-wrapper').after(new_tile); 
                                
                                
                                // In case we're modifing an existing tile, remove from list...
                                oldTile.detach(); 
                              
                               

                               // Set province count
                                var population1 = (data.TaxRules.population == 1) ? 'is ' : 'are ';
                                population1 += countInEnglish(data.TaxRules.population, 'province', 'provinces');

                                var population2 = (data.TaxRules.population > 0) ? '(' + data.TaxRules.population + ')' : '';

                                $('.population-count-1').html(population1);
                                $('.population-count-2').html(population2);

                               // Reset form fields
                               $('#text-field-tax-rule-name').val('');
                               $('#text-field-tax-rule-rate').val(0);
                               $('#text-field-tax-rule-zone-id').val(0);

                               // Update cache, then bring it back
                               loadData(release);


                            return false;

                            },

                    error: function (xhr) {

                        //errorHandler(xhr, '', function () {});
                        return false;
                    }
            });
            return false;

    }
    
    
    
    // method: [ newTaxRule ]
    function newTaxRule () {
        renderDialog(0);
        
        return true;
    } 
    
    
    // method: [ editTaxRule ]
    function editTaxRule (id) {
        renderDialog(id);
        
        return true;
    } 
    
    

    
    
    
    
    //Return public methods
    return {
        
        refresh: loadData, 
        removeTaxRule: removeTaxRule,
        getTaxRule: getTaxRule, 
        getTaxRules: getTaxRules, 
        newTaxRule: newTaxRule, 
        editTaxRule: editTaxRule, 
        submitForm:  sendRequest
        
    };
    
    
    
 }








