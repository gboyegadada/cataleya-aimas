



// -------------------------------------------------------------------------------------------------------//



// Class: [ Zone ]
Cataleya.loadZone = function (_zone_id) {
        
        // if zone_id is invalid...
        if (typeof _zone_id == 'object' || typeof _zone_id == 'undefined') return {};
        
        
        // Private properties...
        var _formSetupRequired = true;
        var _dialogBoxActive = false;
        var _new = false;
        _zone_id = parseInt(_zone_id);
        
        var _data = [ /* zone info here */ ], _provinces_array = [ /* provinces in this zone */ ], _countries_array = [ /* countries in this zone */ ];
        
        
        
        // load zone info
        loadData();
        

        // method: [ refresh ]
        function loadData (callBack) {

            ajax.jsonRequest ({
                    method: 'POST', 
                    url: 'get-zone-info.ajax.php', 
                    data: {auth_token: $('#meta_token').val(), zone_id: _zone_id }, 
                    dataType: 'json', 
                    success: function (response) {
                            
                            var data = response.json();
                                setData(data);
                                if (typeof callBack == 'function') callBack();
                                return false;
                            },

                    error: function (xhr) {
                        return false;
                    }
            });
        }
        
        


        // private method: [ setData ]
        function setData (data) {
            _data = data.zone;
            _provinces_array = data.provinces;
            _countries_array = data.countries;
        } 
        
        
        
        
        
        /*
         *
         * public method: [ getProvinces ]
         * 
         * Note: this will return of the specified country that are in this zone.
         * 
         */
        
        
        function getProvinces (countryCode) {
            
            if (!hasCountry(countryCode)) return false;
            
            ajax.jsonRequest ({
                    method: 'POST', 
                    url: 'get-zone-info.ajax.php', 
                    data: {auth_token: $('#meta_token').val(), zone_id: _zone_id, country_code: countryCode }, 
                    dataType: 'json', 
                    success: function (response) {
                            
                            var data = response.json();
                                setData(data);
                                if (typeof callBack == 'function') callBack();
                                return false;

                            },

                    error: function (xhr) {
                        return false;
                    }
            });
            
            return true;
            
        }
        
        
        
        
        /*
         *
         * public method: [ hasCountry ]
         * 
         */
        
        function hasCountry (isoCode) {
            isoCode = isoCode.toUpperCase();

            for (var i = 0; i < _countries_array.length; i++) {
                if (_countries_array[i].iso == isoCode) return true;
            }


            return false;
        } 
        
        


        
        
        /*
         *
         * public method: [ hasProvince ]
         * 
         */
        
        function hasProvince (id) {

            for (var i = 0; i < _provinces_array.length; i++) {
                if (_provinces_array[i].id == id) return true;
            }

            
            return false;
        }
        
        
        
        
        
        
        
        /*
         *
         * private method: [ renderDialog ]
         * 
         */

        function renderDialog (isoCode, hasProvinces) {

            hold ('Please wait...');


            // Set country code field
            $('#add-to-zone-dialog-field-country-code').val(isoCode);


            // Country has no provinces...
            if ((typeof hasProvinces == 'boolean' && !hasProvinces)) {
                _formSetupRequired = false;
                sendRequest();
            }


            // Country has provinces
            else if (typeof hasProvinces == 'boolean' && hasProvinces) {
                        
                        // This will return the provinces as an array in the callback function
                        Cataleya.loadProvinces(isoCode, function (province_array) {
                        

                        // Remove previous click events...
                        var dialogBox = $('#add-to-zone-dialog-box');
                        dialogBox.find('#cancel-add-to-zone-button, #fullscreen-bg2').unbind('click');


                        // hide the 'remove-country' button since we're adding a new country here...
                        if (_new) $('#remove-country-wrapper').hide();
                        else $('#remove-country-wrapper').show();

                        // Populate province list (in dialog box)
                        $('#dialog-province-list').html('');
                        for (i in province_array) {
                            
                            var checked = (hasProvince(province_array[i].id)) ? ' checked="checked" ' : '';
                            
                            var checkboxControl = '<label class="checkbox-label-1" for="province-dialog-checkbox-' + province_array[i].id + '"><input type="checkbox"' + checked + ' name="provinces[]" id="province-dialog-checkbox-' + province_array[i].id + '" value=' + province_array[i].id + ' />' + province_array[i].name + '</label><br />';

                            $('#dialog-province-list').append(checkboxControl);
                        }

                        // Set country name
                        var country_name = $('#country-tile-'+isoCode.toLowerCase()).attr('data_country_name');
                        $('.dialog-country-name').html(country_name);

                        // Set dialog box title e.t.c
                        var dialogTitle;
                        
                        if (_new) {
                             dialogTitle = (_data.listType == 'normal') ? 'Add Country (' + country_name + ')' : 'Add Country and Provinces';
                        } else {
                             dialogTitle = (_data.listType == 'normal') ? 'Edit or Remove Country (' + country_name + ')' : 'Edit or Remove Country (' + country_name + ')';
                        }
                        dialogBox.find('.bubble-title').html(dialogTitle);
                        $('#add-to-zone-button').val('done');
                        


                        // Init dialog 'cancel' button...
                        dialogBox.find('#cancel-add-to-zone-button, #fullscreen-bg2').click(function () {
                                $('#add-to-zone-dialog-box, #fullscreen-bg2').fadeOut(300);
                                hideTip();
                                return false;
                        });

                        // Free pass next time addToZone() is called i.e. form is ready to sent
                        _formSetupRequired = false;

                        // Show dialog box...
                        release();
                        $('#add-to-zone-dialog-box, #fullscreen-bg2').show();
                        _dialogBoxActive = true;


                    });

            }

        }
        
        
        /*
         *
         * method: [ sendRequest ]
         * 
         */
        
        function sendRequest () {
                
                if (_formSetupRequired) return false;
                
                // Hide dialog box
                if (_dialogBoxActive) {$('#add-to-zone-dialog-box, #fullscreen-bg2').hide(); _dialogBoxActive = false; }
                
                // Hold
                hold('Updating zone...');
                
                ajax.jsonRequest ({
                        method: 'POST', 
                        url: $('#add-to-zone-form').prop('action'), 
                        data: $('#add-to-zone-form').serialize(), 
                        dataType: 'json', 
                        success: function (response) {
                            
                            var data = response.json();
                                   var tile = $('#country-tile-'+data.country.iso.toLowerCase());
                                   
                                   // Only toggle tile select state when selected country has just been added
                                   // ignore if we're just updating it's provinces.
                                   if (data.action == 'add' && _new) tile.toggleClass('selected');
                                   
                                   // Also toggle tile select state when country is being removed from zone.
                                   else if (data.action == 'remove') tile.toggleClass('selected');
                                   
                                   // Allow form submission (i.e. all required params are set)
                                   _formSetupRequired = true;
                                   
                                   // reload zone data then bring it back
                                   loadData(release);
                                   
                                    return false;

                                },

                        error: function (xhr) {
                            return false;
                        }
                });
                
                return true;
        }





        /*
         * 
         * public method: [ updateZone ]
         * 
         */

        function updateZone(isoCode, hasProvinces) {
            _new = (hasCountry(isoCode)) ? false : true;
            
            hasProvinces = (typeof hasProvinces == 'boolean') ? hasProvinces : false;
            
            // Set action to 'add' if country has provinces else set to remove
            if (hasProvinces && hasCountry(isoCode)) $('#add-to-zone-dialog-field-do').val('add');
            else if (!hasProvinces && hasCountry(isoCode)) { $('#add-to-zone-dialog-field-do').val('remove'); }
            else $('#add-to-zone-dialog-field-do').val('add');
            
            renderDialog(isoCode, hasProvinces);
        }





        /*
         * 
         * public method: [ removeCountry ]
         * 
         */

        function removeCountry() {
            if (!_dialogBoxActive) return false;
            
            // Set action: remove
            $('#add-to-zone-dialog-field-do').val('remove');

            sendRequest();
            return true;
        }






        // Return public methods and properties...

        return {
            refresh: loadData, 
            updateZone: updateZone, 
            removeCountry: removeCountry, 
            submitForm:  sendRequest, 
            hasCountry: hasCountry, 
            hasProvince: hasProvince, 
            getProvinces: getProvinces
        }



}




