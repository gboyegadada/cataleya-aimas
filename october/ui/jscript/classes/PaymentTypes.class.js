



// ----------------------------------------------------------------------------------------- //


// Class: [ loadPaymentTypes ]
Cataleya.loadPaymentTypes = function () {
    
    // Private property
    var _collection = [ /* paymenttypes */ ];
    var _formSetupRequired = true;
    var _dialogBoxActive = false;
    
    
    var query = {};



    loadData()
    
    // method: [ loadData ]
    function loadData (callBack) {
        
	ajax.jsonRequest ({
		method: 'POST', 
		url: 'get-payment-types.ajax.php', 
		data: {auth_token: $('#meta_token').val() }, 
		dataType: 'json', 
		success: function (response) {
                            
                            var data = response.json();
                            setData(data.PaymentTypes); 
                            if (typeof callBack == 'function') callBack();
                            return false;

                        
			},
			
		error: function (xhr) {
                    return false;
                }
        });
    }
    
    
    
    // private method: [ setData ]
    function setData (data) {
        _collection = data;
    } 
    
    
    
 
        
        
        
        
        /*
         *
         * private method: [ renderDialog ]
         * 
         */

        function renderDialog (_id) {




            // Remove previous click events...
            $('#payment-type-dialog-box #payment-type-button-cancel, #fullscreen-bg2').unbind('click');

            var dialogBox = $('#payment-type-dialog-box');

            // Set up dialog box...
            if (_id == 0) {

                // invalid id
                return false;

            } else {

                // EDIT
                dialogBox.find('.bubble-title').html('Edit Payment Type');
                $('#payment-type-id').val(_id);

                var PaymentType = getPaymentType(_id);
                $('#text-field-payment-type-name').val(PaymentType.displayName);
                $('#text-field-payment-type-max-amount').val(PaymentType.MaxAmount);
                $('#text-field-payment-type-min-amount').val(PaymentType.MinAmount);
                $('.payment-type-base-currency').html(PaymentType.Currency);
                $('.payment-type-name-here').html(PaymentType.name);
                $('#payment-type-button-ok').val('save changes');

                if (PaymentType.enabled) $('#enable-payment-type-radio').prop('checked', true);
                else  $('#disable-payment-type-radio').prop('checked', true);
            }

            // Init dialog 'cancel' button...
            $('#payment-type-dialog-box #payment-type-button-cancel, #fullscreen-bg2').click(function () {
                    $('#payment-type-dialog-box, #fullscreen-bg2').fadeOut(300);
                    hideTip();
                    return false;
            });
            
            


            // Show dialog box...
            _formSetupRequired = false;
            _dialogBoxActive = true;
            $('#payment-type-dialog-box, #fullscreen-bg2').show();
            

        }
        
        
        /*
         *
         * method: [ sendRequest ]
         * 
         */
        
        function sendRequest () {
                
                if (_formSetupRequired) return false;
                
                
                // Validate form
                var nameField = $('#text-field-payment-type-name');

                if (NAME_REGEXP.test(nameField.val()) || nameField.val() == '') {
                    showTip(nameField);
                    nameField
                    .focus()
                    .one('blur', function () { hideTip(); });

                    return false;
                }
                
                
                
                // Hide dialog box
                if (_dialogBoxActive) {$('#payment-type-dialog-box, #fullscreen-bg2').hide(); _dialogBoxActive = false; }
                

                
                // Hold
                hold('Saving changes...');

                ajax.jsonRequest ({
                        method: 'POST', 
                        url: $('#payment-type-form').prop('action'), 
                        data: $('#payment-type-form').serialize(), 
                        dataType: 'json', 
                        success: function (response) {
                            
                            var data = response.json();
                                  
                                  _formSetupRequired = true;
                                  


                                  // enabled ??
                                  var _info = (data.PaymentType.enabled) ? 'Enabled' : 'Disabled';



                                  // Then add new tile...
                                  var new_tile = $('<a href="javascript:void(0)" onclick="PaymentTypes.editPaymentType('+data.PaymentType.id +'); return false;" class="payment-type-tile" id="payment-type-'+data.PaymentType.id +'"><div class="mediumListIconTextItem"><span class="icon-globe tile-icon-large" ></span><div class="mediumListIconTextItem-Detail"><h4>' + data.PaymentType.name + '</h4>' + 
                                                    '<h6>' + _info + '</h6></div></div></a>'
                                        );




  
                                    // replace tile...
                                     var oldTile = $('#payment-type-'+data.PaymentType.id);

                                     if (oldTile.length > 0 && oldTile.prev('.payment-type-tile').length > 0) oldTile.prev('.payment-type-tile').after(new_tile);
                                     else new_tile.prependTo('#payment-types-grid-wrapper');


                                     // In case we're modifing an existing tile, remove from list...
                                     oldTile.detach(); 

                                   
                                   // Update cache then bring it back
                                   loadData(release);

                                   return false;

                                },

                        error: function (xhr) {
                            _formSetupRequired = true;
                            return false;
                        }
                });              

                
                return true;
        }
        
        
    
    // method: [ getPaymentType ]
    function getPaymentType (id) {
        for (var i = 0; i < _collection.length; i++) {
            if (_collection[i].id == id) return _collection[i];
        }
        
        return {};
    } 
    
    



        /*
         * 
         *  [ editPaymentType ]
         *  
         */

        function editPaymentType (_id) {
            
            renderDialog(_id);

            return true;

        }






    
    
    
    
    //Return public methods
    return {
        
        refresh: loadData, 
        editPaymentType: editPaymentType, 
        getPaymentType: getPaymentType, 
        submitForm: sendRequest
        
    };
    
    
    
 }








