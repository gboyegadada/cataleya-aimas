



// ----------------------------------------------------------------------------------------- //


// Class: [ loadNotificationProfiles ]
Cataleya.loadNotificationProfiles = function () {
    
    // Private property
    var _collection = [ /* admin profilees */ ];
    
    
    var query = {};



    loadData();


    // method: [ refresh ]
    function loadData (callBack) {

        ajax.jsonRequest ({
                method: 'POST', 
                url: 'get-notification-profiles.ajax.php', 
                data: {auth_token: $('#meta_token').val() }, 
                dataType: 'json', 
                success: function (response) {
                            
                            var data = response.json();
                            
                            setData(data);
                            
                            // Recalculate page content height
                            var unit_height = (data.count > 1) ? data.count : 2;
                            var new_height = ((unit_height / 3) * 300)+520;
                            $('.content').css('height', new_height);
                            
                            if (typeof callBack == 'function') callBack();
                            return false;

                        },

                error: function (xhr) {
                    return false;
                }
        });
    }

    
    // private method: [ setData ]
    function setData (data) {
        _collection = data.NotificationProfiles;
    } 
    
    
    
    /*
     *
     * private method: [ renderDialog ]
     * 
     */
    


    function renderDialog (_id) {
    
            // Remove previous click events...
            $('#notification-profile-dialog-box #notification-profile-button-cancel, #fullscreen-bg2').unbind('click');

            var dialogBox = $('#notification-profile-dialog-box');

            // Set up dialog box...

                var NotificationProfile = getNotificationProfile(_id);
                
                // EDIT
                dialogBox.find('.bubble-title').html('Edit Notification Profile (' + NotificationProfile.title + ')');

                
                $('#notification-profile-dialog-field-id').val(NotificationProfile.id);
                $('#notification-profile-dialog-field-keyword').val(NotificationProfile.keyword);
                $('#text-field-profile-title').val(NotificationProfile.title);
                $('#text-field-profile-description').val(NotificationProfile.description);
                $('#text-field-profile-entity-name').val(NotificationProfile.entityName);
                $('#text-field-profile-entity-name-plural').val(NotificationProfile.entityNamePlural);
                $('#text-field-profile-send-after-period').val(NotificationProfile.sendAfterPeriod);
                $('#text-field-profile-send-after-interval option').attr('selected', false);
                $('#text-field-profile-send-after-interval option[value="'+NotificationProfile.sendAfterInterval.toLowerCase()+'"]').attr('selected', true);
                $('#text-field-profile-send-after-increments').val(NotificationProfile.sendAfterIncrements);
                
                var _column_names = $('#dialog-recipient-list > .sub-header');
                $('.alert-recipient-tile').detach();
                    
                for (i in NotificationProfile.recipients) {
                    var user = NotificationProfile.recipients[i];
                    var emailedChecked = (user.emailEnabled) ? 'checked' : '';
                    var dashChecked = (user.dashboardEnabled) ? 'checked' : '';
                    
       
                    var _entry = '<li class="alert-recipient-tile"><input type="checkbox" name="recipients-email[]" value=' + user.id + ' ' + emailedChecked + ' >&nbsp;&nbsp;<input type="checkbox" name="recipients-dashboard[]" value=' + user.id + ' ' + dashChecked + ' >&nbsp;&nbsp;' + user.name + '</li>';
                    _column_names.after(_entry);
                }
                
                $('#notification-profile-button-ok').val('save changes');
                




            // Init dialog 'cancel' button...
            $('#notification-profile-dialog-box #notification-profile-cancel-button, #fullscreen-bg2').click(function () {
                    $('#notification-profile-dialog-box, #fullscreen-bg2').fadeOut(300);
                    hideTip();
                    return false;
            });

            // Show dialog box...
            $('#notification-profile-dialog-box, #fullscreen-bg2').show();

    }
    
    
    // method: [ sendRequest ]
    function sendRequest () {
        
            // Validate form
            var failedItems = Validator.validateForm('notification-profile-form');
            
            if (failedItems.length > 0) {
                
                for (var i = 0; i < failedItems.length; i++) {
                    showTip(failedItems[i]);
                    $(failedItems[i])
                    .focus()
                    .one('blur', function () { hideTip(); });
                    break;
                    return false;
                }

            }
            
            


            $('#notification-profile-dialog-box').hide();
            hold('Saving notificaion profile...');

            ajax.jsonRequest ({
                    method: 'POST', 
                    url: 'save-notification-profile.ajax.php', 
                    data: $('#notification-profile-form').serialize(), 
                    dataType: 'json', 
                    success: function (response) {
                            
                            var data = response.json();

                              // Notification Profile
                              var _info = countInEnglish(data.NotificationProfile.incrementsSinceLastSent, 'new notification', 'new notifications');



                              // Then add new tile...
                              var new_tile = $('<a href="javascript:void(0)" class="notification-profile-tile" id="notification-profile-' + data.NotificationProfile.id + '"  onclick="NotificationProfiles.editNotificationProfile(' + data.NotificationProfile.id + '); return false;"><div class="mediumListIconTextItem"><span class="icon-broadcast-2 tile-icon-large" ></span><div class="mediumListIconTextItem-Detail"><h4>' + data.NotificationProfile.title + '</h4>' + 
                                                '<h6>' + _info + '</h6></div></div></a>'
                                    );


                               
                             // replace tile...
                             var oldTile = $('#notification-profile-'+data.NotificationProfile.id);

                             if (oldTile.length > 0 && oldTile.prev('.notification-profile-tile').length > 0) oldTile.prev('.notification-profile-tile').after(new_tile);
                             else new_tile.prependTo('#notification-profile-grid-wrapper');


                             // In case we're modifing an existing tile, remove from list...
                             oldTile.detach(); 


                            // reload data then bring it back
                            loadData(function() {alert('Saved', 'Profile saved.', true)});

                            return false;


                            },

                    error: function (xhr) {
                        return false;
                    }
            });
            return false;

    }
    
    

    
    // method: [ editNotificationProfile ]
    function editNotificationProfile (id) {
        renderDialog(id);
        
        return true;
    } 
    
    
    
    
    
    
    // method: [ getNotificationProfile ]
    function getNotificationProfile (id) {
        for (var i = 0; i < _collection.length; i++) {
            if (_collection[i].id == id) return _collection[i];
        }
        
        return {};
    } 
    
    

    

    
    
    
    
    //Return public methods
    return {
        
        refresh: loadData, 
        getNotificationProfile: getNotificationProfile, 
        editNotificationProfile: editNotificationProfile, 
        submitForm:  sendRequest
        
    };
    
    
    
 }
 
 
 
 
 
 



 
 