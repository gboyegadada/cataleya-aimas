


// ----------------------------------------------------------------------------------------- //


// Class: [ loadZones ]
Cataleya.loadZones = function () {
    
    // Private property
    var _collection = [ /* zones */ ];
    var _formSetupRequired = true;
    var _dialogBoxActive = false;
    
    
    var query = {};



    loadData()
    
    // method: [ loadData ]
    function loadData (callBack) {
        
	ajax.jsonRequest ({
		method: 'POST', 
		url: 'get-zones.ajax.php', 
		data: {auth_token: $('#meta_token').val() }, 
		dataType: 'json', 
		success: function (response) {
                            
                            var data = response.json();
                            setData(data.zones);
                            
                            // Recalculate page content height
                            var unit_height = (data.count > 1) ? data.count : 2;
                            var new_height = ((unit_height / 3) * 300)+450;
                            $('.content').css('height', new_height);
                            
                            if (typeof callBack == 'function') callBack();
                            
                            return false;

                        
			},
			
		error: function (xhr) {
                    return false;
                }
        });
    }
    
    
    
    // private method: [ setData ]
    function setData (data) {
        _collection = data;
    } 
    
    
    
    
    // method: [ getZone ]
    function getZone (id) {
        for (var i = 0; i < _collection.length; i++) {
            if (_collection[i].id == id) return _collection[i];
        }
        
        return {};
    } 
    
    

    

    
    // method: [ deleteZone ]
    function deleteZone (zone_id) {
            
            hold ('Please wait...');
            
            query = {
                    auth_token: $('#meta_token').val(),
                    zone_id: (typeof query.zone_id == 'undefined' || query.zone_id == 0) ? zone_id : query.zone_id

                    };
            


            // Do request...
            ajax.jsonRequest ({
                    method: 'POST', 
                    url: 'delete-zone.ajax.php', 
                    data: query, 
                    dataType: 'json', 
                    success: function (response) {
                            
                            var data = response.json();
                        query.zone_id = 0; 
                        window.location = 'landing.zones.php';     
                        return false;

                            },

                    error: function (xhr) {
                                query.zone_id = 0; // reset id
                                return false;
                            }
            });
        }
        
        
        
        
        
        
        
        /*
         *
         * private method: [ renderDialog ]
         * 
         */

        function renderDialog (zone_id) {




            // Remove previous click events...
            $('#zone-dialog-box #zone-button-cancel, #fullscreen-bg2').unbind('click');

            var dialogBox = $('#zone-dialog-box');

            // Set up dialog box...
            if (zone_id == 0) {

                // NEW
                dialogBox.find('.bubble-title').html('New Zone');
                $('#zone-id').val(0);
                $('#text-field-zone-name').val('');
                $('#zone-button-ok').val('create new zone');
                $('#normal-list-radio').prop('checked', true);

            } else {

                // EDIT
                dialogBox.find('.bubble-title').html('Edit Zone');
                $('#zone-id').val(zone_id);

                var zone = zones.getZone(zone_id);
                $('#text-field-zone-name').val(zone.name);
                $('#zone-button-ok').val('save changes');

                if (zone.type == 'normal') $('#normal-list-radio').prop('checked', true);
                else if (zone.type == 'exclude') $('#exclude-list-radio').prop('checked', true);
            }

            // Init dialog 'cancel' button...
            $('#zone-dialog-box #zone-button-cancel, #fullscreen-bg2').click(function () {
                    $('#zone-dialog-box, #fullscreen-bg2').fadeOut(300);
                    hideTip();
                    return false;
            });
            
            


            // Show dialog box...
            _formSetupRequired = false;
            _dialogBoxActive = true;
            $('#zone-dialog-box, #fullscreen-bg2').show();
            

        }
        
        
        /*
         *
         * method: [ sendRequest ]
         * 
         */
        
        function sendRequest () {
                
                if (_formSetupRequired) return false;
                
                
                // Validate form
                var nameField = $('#text-field-zone-name');

                if (NAME_REGEXP.test(nameField.val()) || nameField.val() == '') {
                    showTip(nameField);
                    nameField
                    .focus()
                    .one('blur', function () { hideTip(); });

                    return false;
                }
                
                
                
                // Hide dialog box
                if (_dialogBoxActive) {$('#zone-dialog-box, #fullscreen-bg2').hide(); _dialogBoxActive = false; }
                

                
                // Hold
                hold('Adding new zone...');

                ajax.jsonRequest ({
                        method: 'POST', 
                        url: $('#zone-form').prop('action'), 
                        data: $('#zone-form').serialize(), 
                        dataType: 'json', 
                        success: function (response) {
                            
                            var data = response.json();
                                  
                                  _formSetupRequired = true;
                                  
                                  if (!data.zone.isNew) {location.reload(); return true; }

                                  // In case we're modifing an existing zone, remove from list first...
                                  $('#zone-'+data.zone.id).detach();

                                  // Countries in zone
                                  var countries_info = countInEnglish(data.zone.countryPopulation, 'Country', 'Countries');

                                  // Provinces in zone
                                  var provinces_info = countInEnglish(data.zone.provincePopulation, 'Province', 'Provinces');



                                  // Then add new tile...
                                  var new_tile = $('<a href="zone.zones.php?id=' + data.zone.id + '"><div class="mediumListIconTextItem"><span class="icon-globe tile-icon-large" ></span><div class="mediumListIconTextItem-Detail"><h4>' + data.zone.name + '</h4>' + 
                                                    '<h6>' + countries_info + '</h6><h6>' + provinces_info + '</h6></div></div></a>'
                                        );


                                   new_tile.css('cursor', openHandCursor).draggable({
                                                                    revert: 'invalid', 
                                                                    revertDuration: 200,
                                                                    appendTo: "body", 
                                                                    containment: 'document',  
                                                                    opacity: 0.5, 
                                                                    zIndex: 2700, 
                                                                    helper: "clone"
                                                                    });



                                  // Insert new size tile...
                                   $('#zones-grid-wrapper').prepend(new_tile);

                                   
                                   // Update cache then bring it back
                                   zones.refresh(release);

                                   return false;

                                },

                        error: function (xhr) {
                            _formSetupRequired = true;
                            return false;
                        }
                });              

                
                return true;
        }
        
        
        


        /*
         * 
         *  [ editZone ]
         *  
         */

        function editZone (zone_id) {
            
            renderDialog(zone_id);

            return true;

        }






    
    
    
    
    //Return public methods
    return {
        
        refresh: loadData, 
        editZone: editZone, 
        deleteZone: deleteZone,
        getZone: getZone, 
        submitForm: sendRequest
        
    };
    
    
    
 }





