




 
 // ------------------------------------------------------------------------------------ //



// Class: [ ShippingOptions ]
Cataleya.loadShippingOptions = function (_id, callBack) {
    
    
    // if _id is invalid...
    if (typeof _id == 'object' || typeof _id == 'undefined') return {};
    
    // Private property
    var _collection = [ /* shipping options */ ];
    
    
    
    var 
    query = {}, 
    Utility = Cataleya.getUtility();



    loadData();


    // method: [ refresh ]
    function loadData (callBack) {
        
	ajax.jsonRequest ({
		method: 'POST', 
		url: 'get-shipping-options.ajax.php', 
		data: {auth_token: $('#meta_token').val(), store_id: _id }, 
		dataType: 'json', 
		success: function (response) {
                            
                            var data = response.json();
                            setData(data.ShippingOptions);  
                            if (typeof callBack == 'function') callBack();

                            return false;
                        
			},
			
		error: function (xhr) {
                    
                    return false;
                }
        });
    }
    
    
    
    // private method: [ setData ]
    function setData (data) {
        _collection = data;
    } 
    
    
    
    
    
    
    // method: [ getShippingOption ]
    function getShippingOption (id) {
        for (var i = 0; i < _collection.length; i++) {
            if (_collection[i].id == id) return _collection[i];
        }
        
        return {};
    } 
    
    
    
    
    // method: [ getShippingOptions ]
    function getShippingOptions () {
        return _collection;
    } 
    
    
    


    
    
    // method: [ removeShippingOption ]
    function removeShippingOption (_id) {
            
            hold ('Please wait...');
            
            query = {
                    auth_token: $('#meta_token').val(),
                    shipping_option_id: (typeof query.shipping_option_id == 'undefined' || query.shipping_option_id == 0) ? _id : query.shipping_option_id

                    };




            // Do request...
            ajax.jsonRequest ({
                    method: 'POST', 
                    url: 'delete-shipping-option.ajax.php', 
                    data: query, 
                    dataType: 'json', 
                    success: function (response) {
                            
                            var data = response.json();
                                    $('#shipping-option-'+data.ShippingOption.id).remove();
                                    if ($('#shipping-options-editable-list > li').length < 3) $('#shipping-options-editable-list > li.editable-list-tile.last-child').show();
                    
                                    
                                    var population1 = (data.population == 1) ? 'is ' : 'are ';
                                    population1 += data.populationAsText;

                                    var population2 = (data.population > 0) ? '(' + data.population + ')' : '';

                                    $('.population-count-1').html(population1);
                                    $('.population-count-2').html(population2);
                                    
                                    query.shipping_option_id = 0; // reset id
                                    
                                    // Update cache and bring it back
                                    loadData(release);

                                    return false;

                            },

                    error: function (xhr) {

                                //errorHandler(xhr, '', function () {});
                                query.shipping_option_id = 0; // reset id
                                return false;
                            }
            });
        }
        
        
        
        
        
        
    /*
     *
     * private method: [ renderDialog ]
     * 
     */

    function renderDialog (_shipping_option_id) {
            
            

            // Remove previous click events...
            $('#edit-shipping-option-dialog-box > .cancel-button, #edit-shipping-option-button-cancel, #fullscreen-bg2').unbind('click');

            var dialogBox = $('#edit-shipping-option-dialog-box');

            // Set up dialog box...
            if (_shipping_option_id == 0) {

                // NEW
                dialogBox.find('.bubble-title').html('New Shipping Option');
                $('#edit-shipping-option-dialog-hint').html('Enter a name, zone (choose) and a percentage charge for your new tax rate.');
                
                $('#shipping-option-id').val(0);
                
                // 1a. Name
                $('#text-field-shipping-option-name').val('');
                
                // 1b. Description
                $('#text-field-shipping-option-description').val('');
                
                // 2. Carrier (shipping company)
                $('#text-field-shipping-option-carrier-id option[value=0]').attr('selected', true);
                
                // 3. Shipping Zone
                $('#text-field-shipping-option-zone-id option[value=0]').attr('selected', true);
                
                // 4. Max Delivery Days
                $('#text-field-shipping-option-delivery-days').val(1);
                
                // 5. Rate Type (rate per)
                $('#text-field-shipping-option-rate-type option[value=0]').attr('selected', true);
                
                // 6. Shipping Rate
                $('#text-field-shipping-option-rate').val(0.00);
                
                // 7. Tax
                $('#field-taxable').prop('checked', false);
                
                
                $('#edit-shipping-option-button-ok').val('add new shipping option');
                
                
                

            } else {

                // EDIT
                dialogBox.find('.bubble-title').html('Edit Shipping Option');
                $('#edit-shipping-option-dialog-hint').html('Enter a name, zone (choose) and a percentage charge then click save.');
                
                $('#shipping-option-id').val(_shipping_option_id);

                var ShippingOption = getShippingOption(_shipping_option_id);
                
                // 1a. Name
                $('#text-field-shipping-option-name').val(ShippingOption.name);
                
                // 1b. Description
                $('#text-field-shipping-option-description').val(ShippingOption.description);
                
                // 2. Carrier (shipping company)
                $('#text-field-shipping-option-carrier-id option[value='+ShippingOption.Carrier.id+']').attr('selected', true);
                
                // 3. Shipping Zone
                $('#text-field-shipping-option-zone-id option[value='+ShippingOption.Zone.id+']').attr('selected', true);
                
                // 4. Max Delivery Days
                $('#text-field-shipping-option-delivery-days').val(ShippingOption.deliveryDays);
                
                // 5. Rate Type (rate per)
                $('#text-field-shipping-option-rate-type option[value='+ShippingOption.rateType+']').attr('selected', true);
                
                // 6. Shipping Rate
                $('#text-field-shipping-option-rate').val(parseFloat(ShippingOption.rate, 10).toFixed(2));
                
                // 7. Tax
                $('#field-taxable').prop('checked', ShippingOption.isTaxable);
                
                
                $('#edit-shipping-option-button-ok').val('save changes');
            }

            // Init dialog 'cancel' button...
            $('#edit-shipping-option-dialog-box > .cancel-button, #edit-shipping-option-button-cancel, #fullscreen-bg2').click(function () {
                    $('#edit-shipping-option-dialog-box, #fullscreen-bg2').fadeOut(300);
                    hideTip();
                    return false;
            });

            // Show dialog box...
            $('#edit-shipping-option-dialog-box, #fullscreen-bg2').show();



    }
    
    
    // method: [ sendRequest ]
    function sendRequest () {
        
            // Validate form
            var nameField = $('#text-field-shipping-option-name');
            var descriptionField = $('#text-field-shipping-option-description');
            var zoneField = $('#text-field-shipping-option-zone-id');
            var carrierField = $('#text-field-shipping-option-carrier-id');




            
            if (NAME_REGEXP.test(nameField.val()) || nameField.val() == '') {
                showTip(nameField);
                nameField
                .focus()
                .one('blur', function () { hideTip(); });

                return false;
            }
            
            
            if (DESCR_REGEXP.test(descriptionField.val()) || descriptionField.val() == '') {
                showTip(descriptionField);
                descriptionField
                .focus()
                .one('blur', function () { hideTip(); });

                return false;
            }
            

            if (carrierField.val() == 0) {
                showTip(carrierField);
                carrierField
                .one('focus', function () { hideTip(); });

                return false;
            }
            
            
            if (zoneField.val() == 0) {
                showTip(zoneField);
                zoneField
                .one('focus', function () { hideTip(); });

                return false;
            }
            
            
            

            
            

            $('#edit-shipping-option-dialog-box').hide();
            hold('Adding new province...');

            ajax.jsonRequest ({
                    method: 'POST', 
                    url: $('#edit-shipping-option-form').prop('action'), 
                    data: Utility.getFormData($('#edit-shipping-option-form')), 
                    dataType: 'json', 
                    success: function (response) {
                            
                            var data = response.json();

                              var _temp = '<li class="editable-list-tile trashable" id="shipping-option-{{id}}" trash_type="shipping-option-tile"  data_shipping_option_id="{{id}}"  data_shipping_option_name="{{name}}" >' 
                                            + '{{name}}<a class="button-1 editable-list-edit-button fltrt" href="javascript:void(0)" onclick="ShippingOptions.editShippingOption({{id}}); return false;" >edit</a>' 
                                            + '<a class="button-1 editable-list-delete-button fltrt" href="javascript:void(0)" onclick="ShippingOptions.removeShippingOption({{id}});" >remove</a></li>'

                              // Then add new tile...
                              var new_tile = $(Mustache.render(_temp, data.ShippingOption));



                               new_tile.mouseenter(function () {
                                            $(this).find('.button-1').fadeIn('fast');
                                        })
                                        .mouseleave(function () {
                                            $(this).find('.button-1').hide();
                                        });


                               // Insert OR replace new tile...
                                var oldTile = $('#shipping-option-'+data.ShippingOption.id);

                                if (oldTile.length > 0) oldTile.after(new_tile);
                                else $('#shipping-options-editable-list > li').first().after(new_tile); 
                                
                                
                                $('#shipping-options-editable-list > li.editable-list-tile.last-child').hide();
                                
                                
                                // In case we're modifing an existing tile, remove from list...
                                oldTile.remove(); 
                              
                               
                               
                               
                               

                               // Set province count
                                var population1 = (data.population == 1) ? 'is ' : 'are ';
                                population1 += countInEnglish(data.population, 'shipping option', 'shipping option');

                                var population2 = (data.population > 0) ? '(' + data.population + ')' : '';

                                $('.population-count-1').html(population1);
                                $('.population-count-2').html(population2);

                               // Reset form fields
                               $('#text-field-shipping-option-name').val('');
                               $('#text-field-shipping-option-rate').val(0);
                               $('#text-field-shipping-option-zone-id').val(0);

                               // Update cache, then bring it back
                               loadData(release);


                            return false;

                            },

                    error: function (xhr) {

                        //errorHandler(xhr, '', function () {});
                        return false;
                    }
            });
            return false;

    }
    
    
    
    // method: [ newShippingOption ]
    function newShippingOption () {
        renderDialog(0);
        
        return true;
    } 
    
    
    // method: [ editShippingOption ]
    function editShippingOption (id) {
        renderDialog(id);
        
        return true;
    } 
    
    

    
    
    
    
    //Return public methods
    return {
        
        refresh: loadData, 
        removeShippingOption: removeShippingOption,
        getShippingOption: getShippingOption, 
        getShippingOptions: getShippingOptions, 
        newShippingOption: newShippingOption, 
        editShippingOption: editShippingOption, 
        submitForm:  sendRequest
        
    };
    
    
    
 }






