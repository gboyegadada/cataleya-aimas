// JavaScript Document

var PhotoUploader;

$('document').ready(function () {

    PhotoUploader = Cataleya.newPhotoUploader ();

    $('body').prepend('<div id="fullscreen_bg" onclick="PhotoUploader.closeEditor();">&nbsp;</div>');
    $('#save-photo-button').unbind('click').click(PhotoUploader.savePhoto);


    
});
    
    
Cataleya.newPhotoUploader = function () {

    var 
    
    uploader_flag = false, 
    retries = 0, 
    max_retries = 5, 
    document_title = document.title, 

    jcrop_api, 
    _tw, _th, 
    ratio_tw_ow, 
    data_cache, 
    temp_img_path, 
    
    _crop_ratio = 1, 
    _saveHandler = function () {}, 
    

    canvas_orientation = true,  // true for vert. false for hor.
    canvas_angle = 0, 
    imageUploadType = 0; // Defaults to base image



    function initPhotoUpload(saveHandler, tw, th) {
            
            _saveHandler = saveHandler;
            _tw = tw;
            _th = th;
            
            
            _crop_ratio = tw/th;
            
            $('#remote_tw').val(tw);
            $('#remote_th').val(th);


            $('#photo_uploader').click();

    }
    
    
    function savePhoto () {
        

        $('#canvas_image').prop('src', './ui/images/transparent_pixel.gif');
        $('#outer-outer-canvas-wrapper, #photo_editor_controls, #upload_in_progress, #fullscreen_bg').hide();
        hold('Saving photo...');
        
        // Do query... $(window.activeProductColor).attr('data_color_id')
          var options = {
                          image_id: data_cache.image1.image_id, 
                          rotate: data_cache.rotate, 
                          sx: data_cache.select_x, 
                          sy: data_cache.select_y, 
                          sw: data_cache.select_w, 
                          sh: data_cache.select_h
                      };

          return _saveHandler(options);
          
    }
    
    
    




    function setUploadFlag () {

            uploader_flag = true;

    }





    function uploadFile (file_input) {
        if (!uploader_flag || $(file_input).val() == "") return false;
            uploader_engaged = true;


            // FILTER FILE TYPES...

            if ( ! /\.jpe?g$|\.JPE?G$/.test($(file_input).val())) {
                prettyAlert('Please select a "JPEG" photo (the kind with it\'s file name ending with \'.jpg\')');
                return false;
            }


            hold('Uploading photo...');

            var form_hash_id = '#'+file_input.form.id;
            $('#remote_pw, #remote_ph').val($('#outer_canvas_wrapper').width());


            var options = {
                    dataType: 'json', 
                    success: function(data, statusText, xhr, formObjct) {
                            document.title = document_title;
                            uploader_engaged = false;
                            $(file_input).val('');

                            if (data.status == 'error') {
                                    _hideUploader();
                                    prettyAlert (data.message);
                                    return false;
                            }


                            release();
                            $('#fullscreen_bg, #outer-outer-canvas-wrapper').fadeIn(300);
                            $('#image_id').val(data.image_id);

                            if (data.oh < data.th || data.ow < data.tw) {
                                    _hideUploader();
                                    prettyAlert ("Image too small.");
                                    return false;

                            }

                            try {
                                    if (jcrop_api) {_resetCanvas (); }

                            } catch (e) { 
                                    // JCrop not initialized...
                            }

                            data_cache = {image1: data };
                            var new_w_h = _recalcDims(data.pw, data.ph, $('#outer_canvas_wrapper').width(), $('#outer_canvas_wrapper').height());

                            $('#canvas_image').attr({
                                    'src': data.image_url,
                                    'height': new_w_h[1], 
                                    'width': new_w_h[0]
                            });

                            $('#canvas_image, #canvas, #canvas_wrapper').css({
                                    'height': new_w_h[1], 
                                    'width': new_w_h[0]
                                    });

                            $('#upload_in_progress small').toggleClass('blink');
                            $('#upload_in_progress').hide();
                            $('#photo_editor_controls').fadeIn();


                            var min_select_h = (data.th*new_w_h[1])/data.oh;
                            var min_select_w = (data.tw*min_select_h)/data.th; // (imageUploadType == 2 || imageUploadType == 3 || imageUploadType == 5) ? min_select_h : (data.tw*min_select_h)/data.th; // Make aspect ratio square if image type is swatch || store display pix || carrier logo...


                            initJcrop(min_select_w, min_select_h);
                            return false;

                    }, 

                    error: function (xhr, statusText) {
                            switch (xhr.status) {
                                    case 0:
                                            if (retries++ >= max_retries) {
                                                    // Notify user ??

                                                    document.title = 'Quiting after ' + retries + ' tries...';
                                                    uploader_engaged = false;
                                                    retries = 0;
                                                    return false;
                                            }
                                    break;

                                    default:
                                                    
                                                    document.title = document_title;
                                                    _hideUploader();
                                                    prettyAlert('Error: ' + xhr.responseText);
                                                    return false;
                                    break;

                            }


                            document.title = 'Retrying (' + retries + ')';
                            uploader_flag = true;
                            setTimeout(function () {
                                uploadFile(form_hash_id);
                            }, retries*2000);


                    }

            };

            $(form_hash_id).ajaxSubmit(options);


            uploader_flag = false;

            return false;	

    }







    /*

    JCROP HOOK AND UNHOOK

    */

    function initJcrop(min_select_w, min_select_h) {

                            $('#canvas').Jcrop({
                                    aspectRatio: _crop_ratio, 
                                    minSize: [min_select_w, min_select_h], 
                                    setSelect: [30,0,230,300], 
                                    bgFade: true, 
                                    bgOpacity: .5, 
                                    onSelect: _updateCoords, 
                                    onChange: _updateCoords 

                            }, function () {
                                    jcrop_api = this;
                            });
    }


    function _updateCoords (c) {



            // PROCESS SELECTION DATA
            data_cache.select_w = (c.w * data_cache.image1.ow) / $('#canvas').width();
            data_cache.select_h = (c.h * data_cache.image1.oh) / $('#canvas').height();

            data_cache.select_x = (c.x * data_cache.image1.ow) / $('#canvas').width();
            data_cache.select_y = (c.y * data_cache.image1.oh) / $('#canvas').height();

            data_cache.rotate = canvas_angle;


    }



    function _nonDestructiveUnhook (objct_id) {

            var objct_clone = $(objct_id).clone();


            jcrop_api.destroy();

            objct_clone.prependTo('#canvas_wrapper');

            return;	
    }



    /*

    ROTATE CANVAS

    */


    function rotateCanvas (LR) { // 0 or false for LEFT. 1 or true for RIGHT.

            if (data_cache.image1.ow < data_cache.image1.th || data_cache.image1.oh < data_cache.image1.tw) {
                    alert ("Image too small.");
                    return false;
            }


            if (LR) {
                    // RIGHT...
                    canvas_angle = (canvas_angle == 360) ? 90 : canvas_angle+90;
                    canvas_orientation = !canvas_orientation;
            } else {
                    // LEFT...
                    canvas_angle = (canvas_angle == 0) ? 270 : canvas_angle-90;
                    canvas_orientation = !canvas_orientation;
            }

    _nonDestructiveUnhook ('#canvas');

    $('#canvas_image').css({
        '-o-transform': 'rotate(' + canvas_angle + 'deg)',
        '-o-transform-origin': '50% 50%',
        '-ms-transform': 'rotate(' + canvas_angle + 'deg)',
        '-ms-transform-origin': '50% 50%',
        '-moz-transform': 'rotate(' + canvas_angle + 'deg)',
        '-moz-transform-origin': '50% 50%',
        '-webkit-transform': 'rotate(' + canvas_angle + 'deg)', 
        '-webkit-transform-origin': '50% 50%'
    });	



    _flipDims ('#canvas');
    _scaleToFit ('#canvas', $('#outer_canvas_wrapper').width(), $('#outer_canvas_wrapper').height());


    _flipDims ('#canvas_wrapper');
    _scaleToFit ('#canvas_wrapper', $('#outer_canvas_wrapper').width(), $('#outer_canvas_wrapper').height());


                    var min_select_h = (data_cache.image1.th*$('#canvas').height())/data_cache.image1.oh;
                    var min_select_w = (data_cache.image1.tw*min_select_h)/data_cache.image1.th;





            if (canvas_orientation) {

                    $('#canvas_image').height($('#canvas').height()); 
                    $('#canvas_image').width($('#canvas').width());


            } else {

                    $('#canvas_image').height($('#canvas').width()); 
                    $('#canvas_image').width($('#canvas').height());

                    var w_ = data_cache.image1.ow;
                    data_cache.image1.ow = data_cache.image1.oh;
                    data_cache.image1.oh = w_;

            }

            var offset_w = $('#canvas').width() - $('#canvas_image').width();
            var offset_h = $('#canvas').height() - $('#canvas_image').height();
            var c_offset_h = $('#outer_canvas_wrapper').height() - $('#canvas_wrapper').height();


            $('#canvas_image').css({
                    'top': parseInt(offset_h/2), 
                    'left': parseInt(offset_w/2)
            });

            $('#canvas_wrapper').css({
                    'top': parseInt(c_offset_h/2)
            });



            initJcrop(min_select_w, min_select_h);
            
            return false;

    }






    function _resetCanvas () {

            canvas_angle = 0;
            canvas_orientation = true; // true for vert. false for hor.

            try {
            if (jcrop_api) {_nonDestructiveUnhook ('#canvas'); }

            } catch (e) { 

            // No need to unhook jcrop...

            }




    $('#canvas_image').css({
        '-o-transform': 'rotate(' + canvas_angle + 'deg)',
        '-o-transform-origin': '50% 50%',
        '-ms-transform': 'rotate(' + canvas_angle + 'deg)',
        '-ms-transform-origin': '50% 50%',
        '-moz-transform': 'rotate(' + canvas_angle + 'deg)',
        '-moz-transform-origin': '50% 50%',
        '-webkit-transform': 'rotate(' + canvas_angle + 'deg)', 
        '-webkit-transform-origin': '50% 50%'
    });	



    _flipDims ('#canvas');
    _scaleToFit ('#canvas', $('#outer_canvas_wrapper').width(), $('#outer_canvas_wrapper').height());


    _flipDims ('#canvas_wrapper');
    _scaleToFit ('#canvas_wrapper', $('#outer_canvas_wrapper').width(), $('#outer_canvas_wrapper').height());


                    ratio_tw_ow = data_cache.image1.tw/ ((canvas_orientation) ? data_cache.image1.ow : data_cache.image1.oh);
                    var min_select_w = ratio_tw_ow*$('#canvas').width();
                    var min_select_h = ratio_tw_ow*$('#canvas').height();




            if (canvas_orientation) {

                    $('#canvas_image').height($('#canvas').height()); 
                    $('#canvas_image').width($('#canvas').width());

            } else {

                    $('#canvas_image').height($('#canvas').width()); 
                    $('#canvas_image').width($('#canvas').height()); 
            }

            var offset_w = $('#canvas').width() - $('#canvas_image').width();
            var offset_h = $('#canvas').height() - $('#canvas_image').height();
            var c_offset_h = $('#outer_canvas_wrapper').height() - $('#canvas_wrapper').height();


            $('#canvas_image').css({
                    'top': parseInt(offset_h/2), 
                    'left': parseInt(offset_w/2)
            });

            $('#canvas_wrapper').css({
                    'top': parseInt(c_offset_h/2)
            });





    // initJcrop(30, 50);

    }




    function _scaleToFit (objct_id, max_w, max_h) {

            var width_orig = $(objct_id).width();
            var height_orig = $(objct_id).height();
            var ratio_orig = width_orig/height_orig;

            if (max_w/max_h > ratio_orig) {

                    $(objct_id).width(max_h*ratio_orig);
                    $(objct_id).height(max_h);

            } else {

                    $(objct_id).height(max_w/ratio_orig);
                    $(objct_id).width(max_w);

            }

    }




    /*

    RECALC_DIMS

    Take one set of pixel dimensions, rotate and fit into another.
    i.e. rotate (width_orig x height) and fit into (max_w x max_h).
    return new set (one) of pixel dimensions.

    */


    function _recalcDims (width_orig, height_orig, max_w, max_h) {

            var ratio_orig = width_orig/height_orig;

            if (max_w/max_h > ratio_orig) {

                    max_w = max_h*ratio_orig;
            } else {

                    max_h = max_w/ratio_orig;
            }

            return ([max_w, max_h]);

    }




    /*

    FLIP_DIMS

    Reverse object pixel width and height...

    */

    function _flipDims (objct_id) {

            var height_orig = $(objct_id).height();

            $(objct_id).height($(objct_id).width());
            $(objct_id).width(height_orig);

    }








    function closeEditor () {
        if (uploader_engaged) return;

            $('#upload_in_progress small').toggleClass('blink');
            $('#upload_in_progress').hide();
            $('#fullscreen_bg, #outer-outer-canvas-wrapper, #photo_editor_controls').fadeOut(300);

            ajax.jsonRequest ({
            method: 'POST', 
            url: $('#meta_admin_root').val() + 'cancel_upload.ajax.php', 
            data: {auth_token: $('#meta_token').val(), image_id: data_cache.image1.image_id }, 
            dataType: 'json', 
            success: function(response) {}, 
            error: function (response) {}

            });

    }



    function _hideUploader () {

            $('#upload_in_progress small').toggleClass('blink');
            $('#upload_in_progress').hide();
            $('#fullscreen_bg, #outer-outer-canvas-wrapper, #photo_editor_controls').fadeOut(200);
    }



    return {
        initPhotoUpload: initPhotoUpload, 
        uploadFile: uploadFile, 
        rotateCanvas: rotateCanvas, 
        closeEditor: closeEditor, 
        savePhoto: savePhoto, 
        setUploadFlag: setUploadFlag

    };



}