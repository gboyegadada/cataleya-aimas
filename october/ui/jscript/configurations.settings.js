


$('document').ready(function () {
   
   $('#configuration-list-wrapper > li > a')
   .mouseover(function () {
       $(this).find('.config-edit-icon').show();
   })
   .mouseout(function () {
       $(this).find('.config-edit-icon').fadeOut(300);
   });
});




function expandConfig (id) {
    if ($('#'+id+' .nvp-wrapper').css('display') == 'block') {
        $('#'+id+' .nvp-wrapper').slideUp(300);
        // $('#'+id+' .config-edit-icon').fadeIn(300);
        return false;
    }
    
    $('.nvp-wrapper').hide();
    // $('.config-edit-icon').hide();
    $('#'+id+' .nvp-wrapper').slideDown(300);
    // $('#'+id+' .config-edit-icon').fadeOut(300);
    return false;
}




function saveConfig (form_id) {
	

        hold ('Saving configuration...');
        
        var form = $('#'+form_id);
        
        // Do query...


        ajax.jsonRequest ({
        method: 'POST', 
        url: 'save-config.ajax.php', 
        data: form.serialize(), 
        dataType: 'json', 
	success: function (response) {
                            
                            var data = response.json();
            
                        alert('Saved', data.message);
                        form.find('.nvp-wrapper').slideUp(300);
			return false;
                        
			},
        error: function (xhr) {
                        alert('Error', xhr.responseText);
                }
        });



	return false;
}
