/* 
 * store.edit.js
 * 
 */



// GENERIC STORE INFO QUERY
var 

store_info_query_template, 

ShippingOptions, 
Staff, 
storeWidget, 
pageWidget;





$('document').ready(function () {
    
        
        
        var store_id = $('#meta_store_id').val();
        
        storeWidget = Cataleya.newStoreWidget(store_id);
        ShippingOptions = Cataleya.loadShippingOptions(store_id);
        Staff = Cataleya.loadStaff(store_id);
        pageWidget = Cataleya.newPageWidget(store_id);
        
        $('#edit-shipping-option-form').submit(function (){
            ShippingOptions.submitForm();
            
            return false;
        });
        
        $('.editable-list-tile').mouseenter(function () {
                $(this).find('.button-1').fadeIn('fast');
            })
            .mouseleave(function () {
                $(this).find('.button-1').hide();
            });


       

});






