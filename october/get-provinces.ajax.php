<?php
/*
FETCH PROVINCES AND RETURN AS JSON...
*/




define ('OUTPUT', 'JSON'); // Whether to output as JSON or HTML (especially errors)...


// LOAD APPLICATION TOP...
require_once ('app_top.php');






// DIE IF INPUT IS NOT VIA POST METHOD...
if ($_SERVER['REQUEST_METHOD'] != 'POST') _catch_error('POST METHOD ONLY PLEASE. NOW GET.');
	



// FILTER VALUES ....
$_CLEAN = filter_input_array(INPUT_POST, 
								array(	
                                                                                // auth_token
                                                                                'auth_token'    =>  FILTER_SANITIZE_STRIPPED, 
										 // country id...
										'country_code'	=>	array('filter'		=>	FILTER_VALIDATE_REGEXP, 
														'options'	=>	array('regexp' => '/^[A-Za-z]{2}$/')
																)

										)
								);


$country = Cataleya\Geo\Country::load($_CLEAN['country_code']);
if ($country === NULL) _catch_error('Country could not be loaded.', __LINE__, true);


$provinces = array ();

foreach ($country as $province) 
{
    $provinces[] = array (
        'id'    =>  $province->getID(), 
        'iso'   =>  $province->getIsoCode(), 
        'name'  =>  $province->getName(), 
        'printableName' => $province->getPrintableName()
    );
}

// OUTPUT...
$json_reply = array (
		"status"=>'ok', 
		"message"=>(($country->getPopulation() < 1) ? 'No': $country->getPopulation()) . ' provinces found.', 
                "provinces"=>$provinces, 
                "count"=>$country->getPopulation()
		);

echo (json_encode($json_reply));
exit();



?>