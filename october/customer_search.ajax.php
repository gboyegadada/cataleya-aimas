<?php
/*
FETCH CATEGORIES AND RETURN AS JSON...
*/


define ('OUTPUT', 'JSON'); // Whether to output as JSON or HTML (especially errors)...


// LOAD APPLICATION TOP...
require_once ('app_top.php');



$WHITE_LIST = array(
						'term'
					);



// RETRIEVE DATA ONLY IF VIA POST METHOD...
if ($_SERVER['REQUEST_METHOD'] != 'GET') _catch_error('GET METHOD ONLY PLEASE. NOW GET.', __LINE__, true);
	



// SANITIZE INPUT DATA...
$_CLEAN = filter_input_array(INPUT_GET, 
								array(
										'term'		=>	FILTER_SANITIZE_STRING

										)
								);






// Check if INPUT keys are ALL expected AND MADE IT THROUGH...
if (anySuspects()) 
{
    // _catch_error('Error processing white_list!', __LINE__, true);
    
    // bail quietly

    // Output...
    $json_data = array (
                    'status' => 'Ok', 
                    'message' => 'No customers found.', 
                    'customers' => array(), 
                    'count' => 0
                    );
    echo json_encode($json_data);
    exit();

    
}







// First, take out funny looking characters...
$_CLEAN['term'] = preg_replace('/[^-\s\.A-Za-z0-9@*#+]+/', '', $_CLEAN['term']);




// Load search results as a collection [ _Customer ] objects...
$search_results = Cataleya\Customers::search($_CLEAN['term']);

$customers = array();

foreach ($search_results as $customer) {
        $_customer_id = $customer->getCustomerId();

        
        $customers[] = array (
          'id'  =>  $_customer_id, 
          'firstname'   =>    $customer->getFirstname(),
          'lastname'    =>  $customer->getLastname(), 
          'email_address'   =>  $customer->getEmailAddress(), 
          'telephone'   =>  $customer->getEmailAddress(), 
          'url' =>  BASE_URL.'customer_profile.customers.php?id=' . $_customer_id, 
          'isOnline'    =>  $customer->isOnline()
        );
        
}



// Output...
$json_data = array (
		'status' => 'Ok', 
		'message' => (count($customers)) . ' customers found.', 
		'customers' => $customers, 
		'count' => count($customers)
		);
echo json_encode($json_data);
exit();




?>