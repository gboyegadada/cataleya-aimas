<?php





define ('OUTPUT', 'JSON'); // Whether to output as JSON or HTML (especially errors)...


// LOAD APPLICATION TOP...
require_once ('app_top.php');






// ENV...
define ('PERMS', 'RW');
$WHITE_LIST = array(
						'auth_token', 
						'name', 
						'description', 
						'tag_id'
						);





// DIE IF INPUT IS NOT VIA POST METHOD...
if ($_SERVER['REQUEST_METHOD'] != 'POST') _catch_error('POST METHOD ONLY PLEASE. NOW GET.', __LINE__, true);

// SANITIZE INPUT DATA...
$_CLEAN = filter_input_array(INPUT_POST, 
								array(
										'auth_token'		=>	FILTER_SANITIZE_STRIPPED,  
										'name'		=>	FILTER_SANITIZE_STRING, 
										'description'	=>	FILTER_SANITIZE_STRING, 
										'tag_id'	=>	FILTER_VALIDATE_INT

										)
										
								);


// Check if INPUT keys are ALL expected AND MADE IT THROUGH...
if (anySuspects()) _catch_error('Error processing white_list!', __LINE__, true);


// VALIDATE AUTH TOKEN...
validate_auth_token ();





// Load tag
$tag = Cataleya\Catalog\Tag::load($_CLEAN['tag_id']);
if ($tag == NULL) _catch_error('Category could not be loaded.', __LINE__, true);



// Set description...
if ($_CLEAN['name'] !== '' || $_CLEAN['description'] !== '')
{
    // Get tag description
    $tag_description = $tag->getDescription();
    if ($tag_description == NULL) _catch_error('Category description could not be loaded.', __LINE__, true);

    

    if ($_CLEAN['description'] != '') $tag_description->setText($_CLEAN['description'], 'EN');
    
    if ($_CLEAN['name'] != '') 
    {
        $tag_description->setTitle($_CLEAN['name'], 'EN');
        
        // Update url_rewrite...
        $tag_url_rewrite = $tag->getURLRewrite();
        $tag_url_rewrite->setKeyword($_CLEAN['name']);

        
    }
}




// UPDATE TAGS - HATCHED ...
if ((int)$tag->getStatus() == 0) $tag->setStatus(1);


// Output...
$json_data = array (
                'status' => 'Ok', 
                'message' => 'Category description saved.', 
                'tag_id' => $_CLEAN['tag_id'] 
                );
echo json_encode($json_data);
exit();








?>