<?php



define ('OUTPUT', 'JSON'); // Whether to output as JSON or HTML (especially errors)...


// LOAD APPLICATION TOP...
require_once ('app_top.php');





/*
 * 
 * TO PERFORM THIS OPERATION, USER MUST BE AN ADMINISTRATOR
 * 
 */


if (!IS_SUPER_USER || $_admin_role->hasPrivilege('EDIT_COUNTRY')) _catch_error('You do have permission to perform this action.', __LINE__, true);




$WHITE_LIST = array(
						'auth_token',
						'country_code', 
						'set'
						);





// DIE IF INPUT IS NOT VIA POST METHOD...
if ($_SERVER['REQUEST_METHOD'] != 'POST') _catch_error('POST METHOD ONLY PLEASE. NOW GET.', __LINE__, true);

// SANITIZE INPUT DATA...
$_CLEAN = filter_input_array(INPUT_POST, array(
                                                    'auth_token'		=>	FILTER_SANITIZE_STRIPPED,
                                                     // country id...
                                                    'country_code'	=>	array('filter'		=>	FILTER_VALIDATE_REGEXP, 
                                                                                    'options'	=>	array('regexp' => '/^[A-Za-z]{2}$/')
                                                                                                    ), 
                                                     // enable or disable provinces...
                                                    'set'	=>	array('filter'		=>	FILTER_VALIDATE_REGEXP, 
                                                                                    'options'	=>	array('regexp' => '/^(on|off)$/')
                                                                                                    )

                                                    )
										
					);


// Check if INPUT keys are ALL expected AND MADE IT THROUGH...
if (anySuspects()) _catch_error('Error processing white_list!' . $_CLEAN['auth_token'], __LINE__, true);



// VALIDATE AUTH TOKEN...
validate_auth_token ();


// Load store
$country = Cataleya\Geo\Country::load($_CLEAN['country_code']);


// Check if country exists...
if ($country === NULL)
{
        if (isset($_SESSION[SESSION_PREFIX.'CONFIRM_PARAMS'])) unset ($_SESSION[SESSION_PREFIX.'CONFIRM_PARAMS']);
        
        // ERROR...
        _catch_error('Country not found.', __LINE__, true);

}


// Confirm delete action
$message = 'Are you sure you want to ' . (($_CLEAN['set'] === 'on') ? 'enable' : 'disable') . ' provinces for this country (' . $country->getPrintableName() . ')?';
confirm($message, FALSE);

// retrieve info about country
$country_info = array (
        'iso'   =>  $country->getCountryCode(), 
        'name'  =>  $country->getPrintableName()
    );


// Enable or disable provinces (if we make it past the 'confirm' function)...
if ($_CLEAN['set'] === 'on') $country->enableProvinces();
else if ($_CLEAN['set'] === 'off') $country->disableProvinces();


// Remove token...
if (isset($_SESSION[SESSION_PREFIX.'CONFIRM_TOKEN'])) unset ($_SESSION[SESSION_PREFIX.'CONFIRM_TOKEN']);

// OUTPUT...
 $json_reply = array (
                 "status" => 'ok', 
                 "message" => ($_CLEAN['set'] === 'on') ? 'Provinces enabled.' : 'Provinces disabled.', 
                 "country" => $country_info
                 );


 echo (json_encode($json_reply));
 exit();  

?>