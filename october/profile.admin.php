<?php



define ('OUTPUT', 'HTML'); // Whether to output as JSON or HTML (especially errors)...


// LOAD APPLICATION TOP...
require_once ('app_top.php');

$WHITE_LIST = array(
						'id'
					);



/*
 * 
 * CHECK USER PRIVILEGES
 * 
 */


if ((!isset($_admin_role) || !$_admin_role->hasPrivilege('VIEW_ADMIN_PROFILES')) && !IS_SUPER_USER)  include_once INC_PATH.'forbidden.php';


define('READONLY', ($_admin_role->hasPrivilege('EDIT_ADMIN_PROFILES') || IS_SUPER_USER) ? FALSE : TRUE);





// SANITIZE GET DATA...
$_CLEAN = filter_input_array(INPUT_GET, 
								array(	
										 // ADMIN id...
										'id'	=>	array('filter'		=>	FILTER_VALIDATE_INT, 
																'options'	=>	array('min_range' => 0, 'max_range' => 10000)
																)

										)
								);



// Check if INPUT keys are ALL expected AND MADE IT THROUGH...
if (anySuspects()) _catch_error('Error processing white_list!', __LINE__, true);



$AdminProfile = Cataleya\Admin\User::load($_CLEAN['id']);
if ($AdminProfile === NULL) _catch_error('Admin profile not found!', __LINE__, true);


?>




<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title><?=$AdminProfile->getName()?> | Admin Profile </title>


<!-- JS FILES -->
<?php
require_once(INC_PATH.'js_libs.php');
?>
<script type="text/javascript" src="ui/jscript/admin-profiles.js"></script>


<!-- STYLESHEETS -->
<?php
require_once(INC_PATH.'css_libs.php');
?>

<link href="ui/css/admin-profile.css" rel="stylesheet" type="text/css" />
<link href="ui/css/admin-profiles.css" rel="stylesheet" type="text/css" />


<style type="text/css" media="all">
.content {
	height:2000px;
	
}

#search-tool-wrapper {
	left:650px;
	top:130px;
	
	
}




</style>


<script type="text/javascript">


</script>


</head>

<body>

<!-- BEGIN: META -->
<input type="hidden" id="meta_token" value="<?= AUTH_TOKEN ?>"  />
<!-- END: META -->




<div class="container">
<?php include_once INC_PATH.'photo-upload-forms.php'; ?>
    
    
<div class="header-wrapper absolute">
    <div class="header">

    <a href="<?= BASE_URL ?>profiles.admin.php"><div id="page-title">Admin Profile</div></a><?php include_once INC_PATH.'page.nav.php'; ?> 

    </div>
    
    
<?php
        include_once INC_PATH.'main.nav.php';
?>
    
</div>


  <div class="content">



<div id="search-tool-wrapper">
		
<?php
define('QUICK_SEARCH_URL', BASE_URL.'admin_profile_search.ajax.php');
include INC_PATH.'quick-search-widget.php';
?>


</div>



<!-- ADMIN NAME -->
<h2 id="profile-name-in-header" class="user-status <?=$AdminProfile->isOnline() ? 'online' : 'offline'?>">
    <?=$AdminProfile->getName()?>
    
    <?php
    $_dp = $AdminProfile->getDisplayPicture();
    if ((int)$_dp->getID() !== 0):
    ?>
    <span class="user-dp mini fltlft" ><img src="<?= $_dp->getHref(Cataleya\Asset\Image::THUMB) ?>"></span>
    
    <?php endif; ?>
    
</h2>





<div id="hor-line-1">&nbsp;</div>


<div id="page-options-panel">
    
            <a class="button-1" href="javascript:void(0);"  onclick="AdminProfiles.editAdminProfile(<?=$AdminProfile->getAdminId()?>); return false;" >
                Edit this account
            </a>
            &nbsp;&nbsp;
            
            <?php if (!$AdminProfile->getRole()->hasPrivilege('SUPER_USER') && !READONLY && ($_admin_role->hasPrivilege('DELETE_ADMIN_PROFILES') || IS_SUPER_USER)): ?>
            <a class="button-1" href="javascript:void(0);"  onclick="AdminProfiles.deleteAdminProfile(<?=$AdminProfile->getAdminId()?>); return false;" >
                Delete this account
            </a>
            <?php endif; ?>

            
            
            <div id="page-description-panel">
                This zone includes all countries except the selected ones. This is suitable for when you're setting up shipping for deliveries outside 
                a specific country. For example, setting up shipping for deliveries everywhere except for the U.K.
            </div>
</div>




<!-- ADMIN INFO OUTER WRAPPER -->



<ul class="side-panel">
    
    <li class="header">
    General Info
    </li>

    <li>
        Name: <span class="entry"><?= $AdminProfile->getName() ?></span>
    </li>

    <li>
        Email: <span class="entry"><?= $AdminProfile->getEmailAddress() ?></span>
    </li>

    <li>
        Telephone: <span class="entry"><?= $AdminProfile->getTelephone() ?></span>
    </li>

    <li>
        Last Login: <span class="entry"><?= $AdminProfile->getLastLogin() ?></span>
    </li>

    <li>
    Last Failed Login: <span class="entry"><?= $AdminProfile->getLastFailedLogin() ?></span>
    </li>
    

    <li>
        Failed Logins: <span class="entry"><?= $AdminProfile->getFailedLogins() ?></span class="entry">
    </li>


    <li>
        Last Logged In At: <span  class="entry"><?= $AdminProfile->getLastLoginGeoLoc() ?><span>
    </li> 
    
    <li>
        Last Login Failure At: <span  class="entry"><?= $AdminProfile->getLastFailedLoginGeoLoc() ?><span>
    </li> 
    
    <li>
        Account Created At: <span  class="entry"><?= $AdminProfile->getDateCreated() ?><span>
    </li> 
    
    <li>
        Account Status: <span  class="entry"><?= ($AdminProfile->isActive()) ? 'Active' : 'Disabled' ?><span>
    </li>  

</ul>








<br/><br/><br/>





<?php
     include_once INC_PATH.'edit-admin-profile-dialog-box.php';

?>



<div id="footer">
&nbsp;
</div>



</div>

</div>


</body>
</html>