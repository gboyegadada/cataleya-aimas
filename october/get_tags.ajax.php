<?php
/*
FETCH CATEGORIES AND RETURN AS JSON...
*/



define ('OUTPUT', 'JSON'); // Whether to output as JSON or HTML (especially errors)...


// LOAD APPLICATION TOP...
require_once ('app_top.php');





// RETRIEVE DATA ONLY IF VIA POST METHOD...
if ($_SERVER['REQUEST_METHOD'] != 'POST') _catch_error('POST METHOD ONLY PLEASE. NOW GET.', __LINE__, true);
	



// SANITIZE INPUT DATA...
$_CLEAN = filter_input_array(INPUT_POST, 
								array(
										'auth_token'		=>	FILTER_SANITIZE_STRIPPED, 
										'r_token'		=>	FILTER_SANITIZE_STRIPPED 

										)
								);





// CORE
$dbh = Cataleya\Helper\DBH::getInstance();



// FETCH TAGS...
$sth = $dbh->prepare('SELECT DISTINCT tag_id, image, name, description
				FROM tags a 
				INNER JOIN tags_description b 
				USING(tag_id)');


if (!$sth->execute()) {
	_catch_error($sth->errorInfo(), __LINE__, true);
}


$tags = $sth->fetchAll(PDO::FETCH_OBJ);



// Output...
$json_data = array (
		'status' => 'Ok', 
		'message' => (count($tags)) . ' tags retrieved successfully.', 
		'tags' => $tags
		);
echo json_encode($json_data);
exit();




?>