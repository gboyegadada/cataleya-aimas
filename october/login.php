<?php



define ('OUTPUT', 'HTML'); // Whether to output as JSON or HTML (especially errors)...


// LOAD APPLICATION TOP...
require_once ('app_top.php');








///////////////////  IF INPUT IS NOT VIA POST METHOD //////////////////
if ($_SERVER['REQUEST_METHOD'] == 'GET' || !isset($_SESSION[SESSION_PREFIX.'LOGIN_TOKEN'])) show_login ();
else define ('LOGIN_TOKEN', $_SESSION[SESSION_PREFIX.'LOGIN_TOKEN']);

// Error / Response
$_error_string = '';
$_username = '';





////////////////  ELSE CONTINUE TO PROCESS LOGIN DETAILS  ////////////////////////
$WHITE_LIST = array(
						'auth_token-'.LOGIN_TOKEN, 
						'username-'.LOGIN_TOKEN, 
						'password-'.LOGIN_TOKEN, 
						'fp-'.LOGIN_TOKEN, 
						'login-'.LOGIN_TOKEN, 
						'cancel-'.LOGIN_TOKEN
					);




// SANITIZE GET DATA...
$_CLEAN = filter_input_array(INPUT_POST, 
								array(
										'auth_token-'.LOGIN_TOKEN	=>	FILTER_SANITIZE_STRIPPED, 
										'username-'.LOGIN_TOKEN	=>	FILTER_VALIDATE_EMAIL, 
										'password-'.LOGIN_TOKEN	=>	FILTER_SANITIZE_STRING,
										'fp-'.LOGIN_TOKEN	=>	array('filter'		=>	FILTER_VALIDATE_REGEXP, 
														'options'	=>	array('regexp' => '/^[A-Za-z0-9]{1,100}$/')
																),  
										'login-'.LOGIN_TOKEN	=>	FILTER_SANITIZE_STRING, 
										'cancel-'.LOGIN_TOKEN	=>	FILTER_SANITIZE_STRING
										)
								);



// Check if INPUT keys are ALL expected AND MADE IT THROUGH...
$_suspect = anySuspects();
if ($_suspect !== FALSE) 
{
	(defined('DEBUG_MODE') && DEBUG_MODE == TRUE) || sleep(4);
	
	show_login ('Username or Password incorrect.');
	 	
}

// 
 
$adminUser = Cataleya\Admin\User::login($_CLEAN['username-'.LOGIN_TOKEN], $_CLEAN['password-'.LOGIN_TOKEN]);
if ($adminUser === NULL)
{
        
	(defined('DEBUG_MODE') && DEBUG_MODE == TRUE) || sleep(4);
	
	$_username = ($_CLEAN['username-'.LOGIN_TOKEN] !== FALSE) ? $_CLEAN['username-'.LOGIN_TOKEN] : '';
	
	show_login ('Username or Password incorrect. 2', $_username);
	
	
} 



// PCI-DSS / PA-DSS requirements for lockouts and intervals:
define('ADMIN_LOGIN_LOCKOUT_TIMER', (30 * 60));
define('ADMIN_LOGIN_LOCKOUT_AFTER', 10);
define('ADMIN_PASSWORD_EXPIRES_INTERVAL', strtotime('- 90 day'));









////////////////////// SETUP ADMIN SESSION DATA //////////

session_regenerate_id(TRUE);


// First name // Last Name 
$_SESSION[SESSION_PREFIX.'NAME'] = $adminUser->getName();
$_SESSION[SESSION_PREFIX.'FNAME'] = $adminUser->getFirstname();
$_SESSION[SESSION_PREFIX.'LNAME'] = $adminUser->getLastname();

// IS ADMIN ?
$_SESSION[SESSION_PREFIX.'IS_ADMIN'] = TRUE;


// ADMIN ID
$_SESSION[SESSION_PREFIX.'ADMIN_ID'] = $adminUser->getAdminID();


// AUTH TOKEN // LOGGED IN

unset($_SESSION[SESSION_PREFIX.'LOGIN_TOKEN']);
$_SESSION[SESSION_PREFIX.'AUTH_TOKEN'] = md5( uniqid(rand(), TRUE) ) . '-' . CURRENT_DT;
$_SESSION[SESSION_PREFIX.'LOGGED_IN'] = $adminUser->getLoginToken();
$_SESSION[SESSION_PREFIX.'FINGER_PRINT'] = $_CLEAN['fp-'.LOGIN_TOKEN];

$referrer = (isset($_SESSION[SESSION_PREFIX.'REFERRER'])) ? $_SESSION[SESSION_PREFIX.'REFERRER'] : '/' . ADMIN_ROOT_URI;


// ADD STORE KEY (for access to store front even when closed)
setcookie(
    '_a', // Name
    $adminUser->makeStoreKey(), // Value
    0, // Expires
    '/', // Path
    ('.'.DOMAIN), // Domain
    FALSE, // Is secure
    TRUE // HTTP Only
    );
            

redirect((SSL_REQUIRED ? 'https://' : 'http://') . HOST . $referrer);
exit('<center><p>One moment please...</p></center>');

