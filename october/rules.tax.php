<?php



define ('OUTPUT', 'HTML'); // Whether to output as JSON or HTML (especially errors)...


// LOAD APPLICATION TOP...
require_once ('app_top.php');




// Check if INPUT keys are ALL expected AND MADE IT THROUGH...
if (!in_array($_GET['type'], array('product', 'shipping'))) _catch_error('Error processing white_list!', __LINE__, true);
else $_CLEAN = array ('type' => $_GET['type']);


$TaxRules = Cataleya\Tax\TaxRules::load($_CLEAN['type']);
if ($TaxRules === NULL) _catch_error('Error loading tax rules', __LINE__, true);

$_Title = ($_CLEAN['type'] === 'product') ? 'Product Taxes' : 'Shipping Taxes';


?>




<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Shop Admin | Tax Rules | <?= $_Title ?> (<?= Cataleya\Helper::countInEnglish($TaxRules->getPopulation()) ?>)</title>


<!-- JS FILES -->
<?php
require_once(INC_PATH.'js_libs.php');
?>

<script type="text/javascript" src="ui/jscript/tax.js"></script>


<!-- STYLESHEETS -->
<?php
require_once(INC_PATH.'css_libs.php');
?>


<link href="ui/css/tax.css" rel="stylesheet" type="text/css" />
<link href="ui/css/tax-rates.css" rel="stylesheet" type="text/css" />


<style type="text/css" media="all">
.content {
	height:1000px;
	
}

#search-tool-wrapper {
	left:650px;
	top:130px;
	
	
}




</style>


<script type="text/javascript">


</script>


</head>

<body>

<!-- BEGIN: META -->
<input type="hidden" id="meta_token" value="<?=AUTH_TOKEN?>"  />
<input type="hidden" id="meta_tax_type" value="<?=$_CLEAN['type']?>"  />
<!-- END: META -->




<div class="container">
<div class="header-wrapper absolute">
    <div class="header">

        <a href="<?=BASE_URL?>landing.tax.php"><div id="page-title">Tax Rules</div></a><?php include_once INC_PATH.'page.nav.php'; ?> 

    </div>
    
    
<?php
        include_once INC_PATH.'main.nav.php';
?>
    
</div>


  <div class="content">



<div id="search-tool-wrapper">
		
<?php
define('QUICK_SEARCH_URL', BASE_URL.'country_search.ajax.php');
include INC_PATH.'quick-search-widget.php';
?>


</div>



<!-- Tax Class NAME -->
<h2 id="tax-class-name-in-header">
    <span id="tax-class-name-in-header-text"><?= $_Title ?> </span>
</h2>





<div id="hor-line-1">&nbsp;</div>




<!-- Tax Class INFO OUTER WRAPPER -->

<ul id="tax-class-info-outer-wrapper" >


<li>
    
<?php

$_population = $TaxRules->getPopulation();

?>
	
<!-- Tax Class INFO 
    <div class="dotted-blue-hor-line">&nbsp;</div>
    <br/>

    
    <div class="dotted-yellow-hor-line tiny-yellow-arrow-down">&nbsp;</div>
-->


<div id="tax-class-info">


        <?= $_Title ?>

   
    
    <br/><br/><br/><br/>
    



</div>
        

</li>
</ul>



<br/><br/><br/>





<ul class="editable-list" id="tax-rule-list">
    <li class="header">Tax Rules <span class="population-count-2"><?= ($_population < 1) ? '' : ''; ?></span></li>
    
    <li class="editable-list-add-button-wrapper" >
        <a href="javascript:void(0);" class="editable-list-add-button icon-plus-alt" onclick="TaxRule.newTaxRule(); return false;" >Click here to add a new tax rule in <?= $_Title ?>.</a>
    </li>

    <li class="editable-list-tile last-child" style="display:<?= ($TaxRules->getPopulation() < 1) ? 'block' : 'none' ?>;">
    There are no tax rules under <?= $_Title ?> (yet).
    </li>
    

        

<?php
// GENERATE TAX RATES HERE... 


foreach ($TaxRules as $TaxRule) {
    
    $TaxRuleDescr = $TaxRule->getDescription();

?>
        <li class="editable-list-tile trashable" id="tax-rule-tile-<?=$TaxRule->getID()?>"  trash_type="tax-rule-tile" data_tax_rule_id="<?=$TaxRule->getID()?>" data_tax_rule="<?=$TaxRule->getRate()?>"  data_tax_rule_name="<?=$TaxRuleDescr->getTitle('EN')?>" >
            
        <?=$TaxRuleDescr->getTitle('EN')?> (<?=$TaxRule->getRate()?>%)
        
        <a class="button-1 editable-list-edit-button fltrt" href="javascript:void(0)" onclick="TaxRule.editTaxRule(<?=$TaxRule->getID()?>); return false;" >edit</a>
        <a class="button-1 editable-list-delete-button fltrt" href="javascript:void(0)" onclick="TaxRule.removeTaxRule(<?=$TaxRule->getID()?>);" >remove</a>

        </li>
        
<?php

}

?>
        
      
</ul>





<div id="footer">
&nbsp;
</div>



</div>

</div>


</body>
</html>