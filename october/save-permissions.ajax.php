<?php



define ('OUTPUT', 'JSON'); // Whether to output as JSON or HTML (especially errors)...


// LOAD APPLICATION TOP...
require_once ('app_top.php');


/*
 * 
 * TO PERFORM THIS OPERATION, USER MUST BE AN ADMINISTRATOR
 * 
 */


if (!$adminUser->getRole()->hasPrivilege(PRVLG_SUPER_USER)) _catch_error('You do have permission to perform this action.', __LINE__, true);





// ENV...
$WHITE_LIST = array(
						'auth_token', 
                                                'role_id', 
						'privileges'
						);





// DIE IF INPUT IS NOT VIA POST METHOD...
if ($_SERVER['REQUEST_METHOD'] != 'POST') _catch_error('POST METHOD ONLY PLEASE. NOW GET.', __LINE__, true);

// SANITIZE INPUT DATA...
$_CLEAN = filter_input_array(INPUT_POST, array(
                                                    'auth_token'		=>	FILTER_SANITIZE_STRIPPED, 
                                                    'role_id'                   =>      FILTER_VALIDATE_INT, 
                                                    'privileges'                =>	array( 'filter'=> FILTER_DEFAULT, 'flags' => FILTER_REQUIRE_ARRAY )

                                                    )
										
					);



// Check if INPUT keys are ALL expected AND MADE IT THROUGH...
if (anySuspects()) _catch_error('Error processing white_list!', __LINE__, true);


$remove = array();
$give = array();

// Check privileges array...
foreach ($_CLEAN['privileges'] as $k=>$v) 
{
    $v = filter_var($v, FILTER_VALIDATE_FLOAT);
    $k = filter_var($k, FILTER_SANITIZE_STRIPPED);
    if ($v === FALSE || $k === FALSE || empty($k)) 
    {
        _catch_error('Invalid parameters.', __LINE__, true);
        break;
    }
    
    // sort into 'remove' and 'give' list.
    if ((float)$v !== -1 && (int)$v === 0) $remove[] = $k;
    else if ((int)$v === 1) $give[] = $k;
    else continue;
    
}



// VALIDATE AUTH TOKEN...
validate_auth_token ();


// Load role
$role = Cataleya\Admin\Role::load($_CLEAN['role_id']);


// Check if product exists...
if ($role === NULL)
{
        if (isset($_SESSION[SESSION_PREFIX.'CONFIRM_PARAMS'])) unset ($_SESSION[SESSION_PREFIX.'CONFIRM_PARAMS']);
        
        // ERROR...
        _catch_error('User role could not be loaded.', __LINE__, true);

}


// Confirm delete action (and require password)
$message = 'To save new permissions please confirm your password...';
confirm($message, TRUE);

// Save permissions (if we make it past the 'confirm' function)...

// remove
if (!empty($remove)) $role->removePrivileges($remove);

// give 
if (!empty($give)) $role->givePrivileges($give);


// Remove token...
if (isset($_SESSION[SESSION_PREFIX.'CONFIRM_TOKEN'])) unset ($_SESSION[SESSION_PREFIX.'CONFIRM_TOKEN']);

// OUTPUT...
 $json_reply = array (
                 "status" => 'ok', 
                 "message" => 'Permissions saved.'
                 );


 echo (json_encode($json_reply));
 exit();  

?>