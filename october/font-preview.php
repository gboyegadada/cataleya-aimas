<!--
To change this template, choose Tools | Templates
and open the template in the editor.
-->


<?php

// PATHS...
$_abs_path_shared = preg_replace('!october[\\\|/]?$!', '', dirname(__FILE__));
$_abs_path = $_abs_path_shared . 'october/';


define ('ADMIN_ROOT_PATH', $_abs_path);
define ('ROOT_PATH', $_abs_path_shared);
define ('INC_PATH', $_abs_path.'includes/');
define ('SHARED_INC_PATH', $_abs_path_shared.'includes/');



define('FILE_PATH', ADMIN_ROOT_PATH.'ui/css/elusive/elusive-webfont.css');

?>



<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link href="ui/css/elusive/elusive-webfont.css" rel="stylesheet" type="text/css" />
        <link href="ui/css/bootmetro-tiles.css" rel="stylesheet" type="text/css" />

        <style type="text/css" media="screen">


        #tile-grid {
            margin: 10px auto;
            width: 600px;
            height: auto;
            
            list-style: none;
        }
        
        #tile-grid > li {
            list-style: none;
            margin: 4px;
            padding: 5px;
            
            text-align: center;
            
            float: left;
            background: #eee;
            color: #777;
            
            border-radius:5px;	
            -moz-border-radius:5px;	
            -webkit-border-radius:5px;
            
            height: 60px;
            width: 100px;
            
            font-size: 70%;
            font-family: arial;
        }
        
        
        #tile-grid > li > span {
            margin: 20px;
            color: #777;
            font-size: 240%;
        }





        </style>


        <title></title>
    </head>
    <body>
        
        

        <ul id="tile-grid" class="metro tiles-wrapper">

        <?php
            $FILE_HANDLE = fopen(FILE_PATH, "r");


            while ($line = fgets($FILE_HANDLE, 300)) 
            {
                // $line = preg_replace('/^[^A-Za-z_0-9\-]+/', '', $line);
                $_matches = array();
                if (preg_match('/^\.(?<class>icon-[a-z\-]+)\:/', $line, $_matches) === 0 || $line === '' || $line === "\n") continue;

                echo '<li><span class="' . strtolower($_matches['class']) . '">&nbsp;</span><br />.' . strtolower($_matches['class']) . '</li>';

            }

            fclose($FILE_HANDLE);
        ?>
        </ul>
    </body>
</html>
