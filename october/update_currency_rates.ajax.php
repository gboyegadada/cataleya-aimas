<?php




define ('OUTPUT', 'JSON'); // Whether to output as JSON or HTML (especially errors)...

// For our file upload...allow up to 5 minutes...
set_time_limit (60 * 5); 

// LOAD APPLICATION TOP...
require_once ('app_top.php');





$WHITE_LIST = array(
						'auth_token'
					);



// DIE IF INPUT IS NOT VIA POST METHOD...
if ($_SERVER['REQUEST_METHOD'] != 'POST') _catch_error('POST METHOD ONLY PLEASE. NOW GET.', __LINE__, true);


// SANITIZE INPUT DATA...
$_CLEAN = filter_input_array(INPUT_POST, 
								array(
										'auth_token'		=>	FILTER_SANITIZE_STRIPPED

										)
										
								);






define("FILE_INPUT_NAME", "json_file");


$temp_path = CAT_DIR . 'temp/currency_update.json';
$failed_updates = array ();

// Detect possible file attack...
if (!move_uploaded_file($_FILES[FILE_INPUT_NAME]['tmp_name'], $temp_path))
{
    _catch_error('Error processing data.', __LINE__, TRUE);
}
                

// READ JSON DATA
$json_string = file_get_contents($temp_path);
if (strlen($json_string) > 10000) _catch_error('Error processing data.', __LINE__, TRUE);


// DECODE JSN DATA
$rates = json_decode($json_string, TRUE);

if ($rates === NULL) _catch_error('Error processing data.', __LINE__, TRUE);


// Check base currency of new rates
$base_currency = Cataleya\Locale\Currency::loadBaseCurrency();
if ($rates['base'] !== $base_currency->getCurrencyCode()) _catch_error('Base currency mismatch. Please use: ' . $base_currency->getCurrencyCode(), __LINE__, TRUE);


// UPDATE CURRENCY RATES
$currencies = new Cataleya\Locale\Currencies ();

foreach ($currencies as $currency) 
{
    $code = $currency->getCurrencyCode();
    
    if (!isset($rates['rates'][$code])) 
    {
        $failed_updates[] = $code;
        continue; 
        // _catch_error('Error processing data.', __LINE__, TRUE);
    }
    
    $new_rate = floatval($rates['rates'][$code]);
    
    if (filter_var($new_rate, FILTER_VALIDATE_FLOAT) !== FALSE) $currency->setValue($new_rate);
    else  _catch_error('Error processing data.', __LINE__, TRUE);
    
    
}

unset ($temp_path);


$error_str = (empty($failed_updates)) ? '' : "\nFailed to update: " . implode(', ', $failed_updates);

// OUTPUT
$data = array (
		'status'=>'Ok', 
		'message'=>'Currency rates updated. ' . $error_str
		);
echo json_encode($data);

?>
