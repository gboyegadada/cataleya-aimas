<?php



define ('OUTPUT', 'HTML'); // Whether to output as JSON or HTML (especially errors)...


// LOAD APPLICATION TOP...
require_once ('app_top.php');


/*
$_nvp = [
            
            'MERCHANT_ID' => '1',
            'MERCHANT_WEBSITE' => 'www.example.com',
            'PRODUCT_ID' => '6205',  
            'PAYMENT_ITEM_ID' => '101',  
            'MAC_KEY' => '2dawdszfcasdq24434242ffsdsfd',  

            'MODE' => 'test', 
            'CURRENCY_CODE' => '566', 

            'TRANX_ENDPOINT' => 'https://webpay.interswitchng.com/paydirect/pay',
            'TRANX_STATUS_ENDPOINT' => 'https://webpay.interswitchng.com/paydirect/api/v1/gettransaction.json',
            'WEB_SERVICES_ENDPOINT' => 'https://webpay.interswitchng.com/paydirect/services/webpayservice.svc',

            'SANDBOX_PRODUCT_ID' => '6205',  
            'SANDBOX_PAYMENT_ITEM_ID' => '101',  
            'SANDBOX_MAC_KEY' => 'D3D1D05AFE42AD50818167EAC73C109168A0F108F32645C8B59E897FA930DA44F9230910DAC9E20641823799A107A02068F7BC0F4CC41D2952E249552255710F',  
            'SANDBOX_TRANX_ENDPOINT' => 'https://stageserv.interswitchng.com/test_paydirect/pay', 
            'SANDBOX_TRANX_STATUS_ENDPOINT' => 'https://stageserv.interswitchng.com/test_paydirect/api/v1/gettransaction.json', 
            'SANDBOX_SERVICES_ENDPOINT' => 'https://stageserv.interswitchng.com/test_paydirect/services/webpayservice.svc'
        
        ];

$_cfg = \Cataleya\System\Config::load('plugins.webpay');

foreach ($_nvp as $n=>$v) 
{
	if (!$_cfg->setParam($n, $v)) $_cfg->newParam($n, $v);
}
*/






/*
 * 
 * CHECK USER PRIVILEGES
 * 
 */


if ((!isset($_admin_role) || !$_admin_role->hasPrivilege('VIEW_ORDERS')) && !IS_SUPER_USER)  include_once INC_PATH.'forbidden.php';


define('READONLY', (!$_admin_role->hasPrivilege('EDIT_ORDERS') && !IS_SUPER_USER) ? FALSE : TRUE);




// SANITIZE GET DATA...
$_CLEAN = filter_input_array(INPUT_GET, 
								array(	
										 // Page number...
										'pg'	=>	array('filter'		=>	FILTER_VALIDATE_INT, 
																'options'	=>	array('min_range' => 1, 'max_range' => 1000)
																), 
                                                                                // Page size...
										'pgsize'	=>	array('filter'		=>	FILTER_VALIDATE_INT, 
																'options'	=>	array('min_range' => 1, 'max_range' => 1000)
																),   
                                                                                // store id
										'shop'	=>	array('filter'		=>	FILTER_VALIDATE_INT, 
																'options'	=>	array('min_range' => 0, 'max_range' => 10000)
																),                                                                     
										
										 // order 						
										'o'	=>	array('filter'		=>	FILTER_VALIDATE_INT, 
																'options'	=>	array('min_range' => 0, 'max_range' => 100)
																), 
                                                                                // order by 						
										'ob'	=>	array('filter'		=>	FILTER_VALIDATE_INT, 
																'options'	=>	array('min_range' => 0, 'max_range' => 100)
																),    
                                                                                // index 						
										'i'	=>	array('filter'		=>	FILTER_VALIDATE_REGEXP, 
														'options'	=>	array('regexp' => '/^[A-Za-z]{1}$/')
																), 
                                                                                // e.g. 'pending' | 'fulfilled' | 'closed' ... 						
										'show'	=>	array('filter'		=>	FILTER_VALIDATE_REGEXP, 
														'options'	=>	array('regexp' => '/^[a-z]{3,13}$/')
																), 

										)
								);


// Deafault page size...
define ('DEFAULT_PAGE_SIZE', 10);


// Check if INPUT keys are ALL expected AND MADE IT THROUGH...

$_CLEAN['pg'] = empty($_CLEAN['pg']) ? 1 : (int)$_CLEAN['pg'];
$_CLEAN['shop'] = (isset($_CLEAN['shop']) && is_numeric($_CLEAN['shop'])) ? (int)$_CLEAN['shop'] : 0;
$_CLEAN['i'] = empty($_CLEAN['i']) ? 'all' : $_CLEAN['i'];
$_CLEAN['pgsize'] = empty($_CLEAN['pgsize']) ? DEFAULT_PAGE_SIZE : (int)$_CLEAN['pgsize'];
$_CLEAN['o'] = empty($_CLEAN['o']) ? Cataleya\Collection::ORDER_DESC : $_CLEAN['o'];
$_CLEAN['ob'] = empty($_CLEAN['ob']) ? Cataleya\Collection::ORDER_BY_CREATED : $_CLEAN['ob'];
$_CLEAN['show'] = empty($_CLEAN['show']) ? 'all' : $_CLEAN['show'];

define ('NO_OF_PAGE_INDEX_ANCHORS', 3);

define('FILTER', $_CLEAN['show']);
define('ORDER_BY', $_CLEAN['ob']);
define('ORDER', $_CLEAN['o']);
define('INDEX', $_CLEAN['i']);
define('PAGE_SIZE', $_CLEAN['pgsize']);
define('ACTIVE_HREF', "landing.orders.php?pg=".$_CLEAN['pg']."&ob=".$_CLEAN['ob']."&o=".$_CLEAN['o']."&i=".$_CLEAN['i']);

$_FilterStore = Cataleya\Store::load((int)$_CLEAN['shop']);

$Orders = Cataleya\Store\Orders::load($_FilterStore, NULL, array('order_by'=>ORDER_BY, 'order'=>ORDER, 'index'=>INDEX, 'status'=>FILTER), PAGE_SIZE, $_CLEAN['pg']);


        
        


////////////////////// MARK NOTIFICATION ALERTS AS READ /////////////

$_Stores = Cataleya\Stores::load();
foreach ($_Stores as $_Store) $_Store->getNewOrderNotification()->getAlert($adminUser)->markRead()->markDisplayed();




?>




<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Admin | Browse Orders (<?= Cataleya\Helper::countInEnglish($Orders->getPopulation(), 'order', 'orders') ?>)</title>


<!-- JS FILES -->
<?php
require_once(INC_PATH.'js_libs.php');
?>
<script type="text/javascript" src="ui/jscript/orders.js"></script>


<!-- STYLE SHEETS -->
<?php
require_once(INC_PATH.'css_libs.php');
?>

<link href="ui/css/indexed-paginated-list.css" rel="stylesheet" type="text/css" />
<link href="ui/css/orders.css" rel="stylesheet" type="text/css" />

<!--/ <link href="ui/css/token-input-mac.css" rel="stylesheet" type="text/css" /> -->


<style type="text/css" media="all">
.content {
        height:<?= (ceil(((PAGE_SIZE > $Orders->getPopulation()) ? $Orders->getPopulation() : PAGE_SIZE))*180)+50 ?>px;
	
}




</style>


<script type="text/javascript">


</script>


</head>

<body>

<!-- BEGIN: META -->
<input type="hidden" id="meta_token" value="<?=AUTH_TOKEN?>"  />
<input type="hidden" id="meta_admin_root" value="<?=BASE_URL?>"  />
<input type="hidden" id="active-filter" value="<?=$_CLEAN['show']?>"  />
<input type="hidden" id="active-store" value=<?=$_CLEAN['shop']?>  />
<!-- END: META -->




<div class="container">
<?php include_once INC_PATH.'photo-upload-forms.php'; ?>
    
    
<div class="header-wrapper absolute">
    <div class="header">

    <a href="<?=BASE_URL?>landing.orders.php"><div id="page-title">Orders</div></a><?php include_once INC_PATH.'page.nav.php'; ?> 

    </div>
    
    
<?php
        include_once INC_PATH.'main.nav.php';
?>
    
</div>


  <div class="content">



<div id="search-tool-wrapper">
		
<?php
define('QUICK_SEARCH_URL', 'order-search.ajax.php');
include INC_PATH.'quick-search-widget.php';
?>



</div>




<!-- ORDER INDEX -->

<ul id="list-index">
<a href="landing.orders.php?pg=0&ob=<?=$_CLEAN['ob']?>&o=<?=$_CLEAN['o']?>&i=all" <?=(($_CLEAN['i']=='all') ? 'class="button-1-active"' : '')?>><li>all</li></a>

<?php
$alpha = array('A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z');

for ($i = 0; $i < (count($alpha)); $i++) {
	$a = strtolower($alpha[$i]);
	?>
<a href="landing.orders.php?pg=0&ob=<?=$_CLEAN['ob']?>&o=<?=$_CLEAN['o']?>&i=<?=$a?>" <?=(($_CLEAN['i']==$a) ? 'class="button-1-active"' : '')?>><li><?=$alpha[$i]?></li></a>

<?php
}

?>



</ul>



<div id="hor-line-1">&nbsp;</div>


<ul id="catalog-context-menu" class="context-menu-wrapper" >
    <span class="bubble-arrow">&nbsp;</span>
    
    
    
    <!--
    <a class="" href="javascript:void(0);" onclick="Orders.deleteOrders()">
    <li><span class="icon-remove"></span><br /><span class="button-label">Delete</span></li>
    </a>
    
    <a class="" href="javascript:void(0);" onclick="Orders.toggleProductDisplay(false)" >
    <li><span class="icon-minus-5"></span><br /><span class="button-label">Hide</span></li>
    </a>
    
    <a class="" href="javascript:void(0);" onclick="Orders.toggleProductDisplay(true)" >
    <li><span class="icon-eye-4"></span><br /><span class="button-label">Show</span></li>
    </a>
    
    <li class="divider"></li>
        
    -->

    <a class="" href="javascript:void(0);" onclick="Orders.addToCategory()" >
    <li><span class="icon-add"></span><br /><span class="button-label">Add to Category</span></li>
    </a>
    
    <a class="" href="javascript:void(0);" onclick="Orders.removeFromCategory()" >
    <li><span class="icon-exit"></span><br /><span class="button-label">Remove from Category</span></li>
    </a>
    
    <a class="" href="javascript:void(0);" onclick="Orders.applyCoupon()" >
    <li><span class="icon-price"></span><br /><span class="button-label">Apply Coupon</span></li>
    </a>
    
    <li class="divider"></li>
    
    <a class="" href="javascript:void(0);" onclick="Orders.removeCoupon()" >
    <li><span class="icon-cancel"></span><br /><span class="button-label">Remove Coupon</span></li>
    </a>
    
    <a class="" href="javascript:void(0);" onclick="Orders.addToSale()" >
    <li><span class="icon-sale"></span><br /><span class="button-label">Add to Sale</span></li>
    </a>
    
    <a class="" href="javascript:void(0);" onclick="Orders.removeFromSale()" >
    <li><span class="icon-remove-sale"></span><br /><span class="button-label">Remove from Sale</span></li>
    </a>

    <li class="divider"></li>
    
    <a class="" href="javascript:void(0);" onclick="Orders.setTaxClass()" >
    <li><span class="icon-add"></span><br /><span class="button-label">Set Tax Class</span></li>
    </a>


</ul>






<!-- ORDERS -->

<ul id="list-table">
    
<?php if ($Orders->getPopulation() > (int)$_CLEAN['pgsize'] || $Orders->getPopulation() === 0): ?>    
<li class="list-table-header">

        <div class="list-pagination">
        <?php if ($Orders->getPopulation() > 0) { ?>
            Page <?=$Orders->getPageNumber()?> of <?=$Orders->getLastPageNum()?>
        <?php } else { ?>
        
        No orders with first name starting with '<?=strtoupper($_CLEAN['i'])?>'.
        
        <?php } ?>
        
        </div>

        <ul class="list-page-nav">
            <li><a <?php if (($Orders->getPageNumber() < $Orders->getLastPageNum())) { ?>href="landing.orders.php?pg=<?=$Orders->getNextPageNum()?>&o=<?=$_CLEAN['o']?>&ob=<?=$_CLEAN['ob']?>&i=<?=$_CLEAN['i']?>"  class="button-1"<?php } else { ?>NOHREF class="button-1-no-href"<?php } ?>>next</a></li>
        

        <li><a <?php if ($Orders->getPageNumber() > 1) { ?>href="landing.orders.php?pg=<?=$Orders->getPrevPageNum()?>&o=<?=$_CLEAN['o']?>&ob=<?=$_CLEAN['ob']?>&i=<?=$_CLEAN['i']?>"  class="button-1"<?php } else { ?>NOHREF class="button-1-no-href"<?php } ?>>previous</a></li>
        
        </ul>

</li>
<?php endif; ?>



    
    
<li id="task-bar">
    <a id="global-select-button" class="circular-button fltlft" href="javascript:void(0);" onclick="Orders.toggleAllSelections()">
    <span class="icon-checkmark"></span><span class="button-label">Select All</span>
    </a>
    
    <a id="global-delete-button" class="circular-button fltlft" href="javascript:void(0);" onclick="Orders.deleteOrders()">
    <span class="icon-trash-fill"></span><span class="button-label">Delete</span>
    </a>
    
    <!--
    <a id="global-email-button" class="circular-button fltlft" href="javascript:void(0);" onclick="Order.composeEmail()">
    <span class="icon-email"></span><span class="button-label">Email</span>
    </a>
    -->

    <div class="order-status-control-wrapper field-group fltlft">
        <select id="order-status-control-x" class="order-status-control" onchange="Orders.setOrderStatus(this);">
            <option value="x">Change Status</option>
            <option value="pending" <?= ($_CLEAN['show'] === 'pending') ? 'selected' : '' ?> >Pending</option>
            <option value="shipped" <?= ($_CLEAN['show'] === 'shipped') ? 'selected' : '' ?> >Shipped</option>
            <option value="delivered" <?= ($_CLEAN['show'] === 'delivered') ? 'selected' : '' ?> >Delivered</option>
            <option value="cancelled" <?= ($_CLEAN['show'] === 'cancelled') ? 'selected' : '' ?> >Cancelled</option>
        </select>
    </div>
    
        <!-- 
        <a href="javascript:void(0);" id="task-bar-menu-button" class="blue-button large fltrt" onclick="Orders.toggleContextMenu();" >do something with selected items <span class="icon-grid">&nbsp;</span></a>
        -->
   </li>
    
   

    
    
    
    
<li id="side-bar-wrapper">


<!-- SIDE BAR -->

<?php
    
    $_PendingOrders = Cataleya\Store\Orders::load($_FilterStore, NULL, array('status' => 'pending'));
    $_ShippedOrders = Cataleya\Store\Orders::load($_FilterStore, NULL, array('status' => 'shipped'));
    $_DeliveredOrders = Cataleya\Store\Orders::load($_FilterStore, NULL, array('status' => 'delivered'));
    $_CancelledOrders = Cataleya\Store\Orders::load($_FilterStore, NULL, array('status' => 'cancelled'));

?>


<ul id="side-bar">
    <a href="javascript:void(0);" onclick="Orders.toggleSideBarSection(this)" >
    <li class="sb-section-header">
        ORDER STATUS<span class="<?=(FALSE) ? 'icon-plus-3' : 'icon-minus-3'?> fltrt"></span>
    </li>
    </a>
    <li class="sb-section-body-wrapper" style="<?=(FALSE) ? 'display: none;' : ''?>">
        <ul class="sb-section-body">
            <li class="tile <?= (FILTER === 'all') ? 'current' : '' ?>">
            <a class="fltlft main-anchor" href="<?=ACTIVE_HREF?>&show=all">All (<span id="all-orders-count"><?= $Orders->getPopulation() ?></span>)
            </li>
            
            <li class="tile <?= (FILTER === 'shipped') ? 'current' : '' ?>">
            <a class="fltlft main-anchor" href="<?=ACTIVE_HREF?>&show=shipped">Shipped (<span id="shipped-orders-count"><?= $_ShippedOrders->getPopulation() ?></span>)
            </li>
            
            <li class="tile <?= (FILTER === 'delivered') ? 'current' : '' ?>">
            <a class="fltlft main-anchor" href="<?=ACTIVE_HREF?>&show=delivered">Delivered (<span id="delivered-orders-count"><?= $_DeliveredOrders->getPopulation() ?></span>)
            </li>
            
            <li class="tile <?= (FILTER === 'pending') ? 'current' : '' ?>">
            <a class="fltlft main-anchor" href="<?=ACTIVE_HREF?>&show=pending">Pending (<span id="pending-orders-count"><?= $_PendingOrders->getPopulation() ?></span>)
            </li>
            
            
            <li class="tile <?= (FILTER === 'cancelled') ? 'current' : '' ?>">
            <a class="fltlft main-anchor" href="<?=ACTIVE_HREF?>&show=cancelled">Cancelled (<span id="cancelled-orders-count"><?= $_CancelledOrders->getPopulation() ?></span>)
            </li>

        </ul>
    </li>
    
<?php
    
    // Garbage collection
    unset($_PendingOrders, $_ShippedOrders, $_DeliveredOrders, $_CancelledOrders);

?>
    
    <a href="javascript:void(0);" onclick="Orders.toggleSideBarSection(this)" >
    <li class="sb-section-header">
        STORE <span class="<?=(FALSE) ? 'icon-plus-3' : 'icon-minus-3'?> fltrt"></span>
    </li>
    </a>
    <li class="sb-section-body-wrapper" >
        <ul class="sb-section-body">
            <li class="tile" >
                <form id="store-filter-form" action="landing.orders.php" method="GET">
                <input type="hidden" name="pg" value="<?=$_CLEAN['pg']?>" />
                <input type="hidden" name="ob" value="<?=$_CLEAN['ob']?>" />
                <input type="hidden" name="o" value="<?=$_CLEAN['o']?>" />
                <input type="hidden" name="i" value="<?=$_CLEAN['i']?>" />
                
                <select name="shop" class="fltlft main-anchor" onchange="$('#store-filter-form').submit();">
                    <option value=0>All Stores</option>
                    <?php foreach ($_Stores as $_Store): ?>
                    <option value=<?= $_Store->getStoreId() ?> <?= (!empty($_FilterStore) ? (($_FilterStore->getStoreId() === $_Store->getStoreId()) ? 'selected' : '') : '') ?> ><?= $_Store->getDescription()->getTitle('EN') ?></option>
                    <?php endforeach; ?>
                </select>
                </form>
            </li>
            
        </ul>
    </li>
        
</ul>
    
</li>
    
    
<li class="list-table-cell" id="main-tile-grid" >
    <div class="tiles-wrapper">


    <?php

    $_Today = new DateTime();
    foreach ($Orders as $Order) {
        
        $_TranxStatus = $Order->getPaymentStatus();
        $_Customer = $Order->getCustomer();
        //$_Today->setTimezone($Order->getStore()->getTimezone());
        
        $_OrderDateStr = ($Order->getOrderDate()->format('d M, Y') === $_Today->format('d M, Y')) ? 'today at '.$Order->getOrderDate(FALSE)->format('h:iA') : $Order->getOrderDate(FALSE)->format('h:iA, d M, Y');

    ?>
    <div class="order-tile <?= $Order->getOrderStatus() ?>" id="order-tile-<?=$Order->getID()?>"  data_order_id="<?= $Order->getID() ?>">
            <span class="order-number"><span class="order-status-indicator user-status <?= $Order->getOrderStatus() ?>"></span>#<?=$Order->getOrderNumber()?></span>
            
            <h3 class="customer-name">
            <a href="javascript:void(0);"><?=$_Customer->getFirstname()?> <?=$_Customer->getLastname()?></a>
            </h3>
            <span class="icon-mobile-3">&nbsp;<?=$_Customer->getTelephone()?></span>
            &nbsp;&nbsp;
            <span class="icon-mail">&nbsp;<?=$_Customer->getEmailAddress()?></span>
            &nbsp;&nbsp;
            <br />
            <span class="icon-basket">&nbsp;<?=$Order->getQuantity()?> items</span>
            &nbsp;&nbsp;
            <span class="icon-calendar-2">&nbsp;ordered <?= $_OrderDateStr ?></span>
            &nbsp;&nbsp;
            <span class="icon-flight">&nbsp;<?=$Order->getShippingOption()->getDescription()->getTitle('EN')?></span>
            
            <br />
            
            <span class="payment-status <?= $_TranxStatus ?>"><?= $_TranxStatus ?></span>
            
                
            
            
        
            <div class="order-tile-dashboard-wrapper">
            <div class="order-status-control-wrapper field-group">
                <select id="order-status-control-<?=$Order->getID()?>" class="order-status-control" onchange="Orders.setOrderStatus(this, <?=$Order->getID()?>);">
                    <option value="pending" <?= ($Order->getOrderStatus() === 'pending') ? 'selected' : '' ?> >Pending</option>
                    <option value="shipped" <?= ($Order->getOrderStatus() === 'shipped') ? 'selected' : '' ?> >Shipped</option>
                    <option value="delivered" <?= ($Order->getOrderStatus() === 'delivered') ? 'selected' : '' ?> >Delivered</option>
                    <option value="cancelled" <?= ($Order->getOrderStatus() === 'cancelled') ? 'selected' : '' ?> >Cancelled</option>
                </select>
            </div>
                
            <ul class="order-tile-dashboard">

                  <a class="circular-button" href="javascript:void(0);" onclick="Order.composeEmail(<?=$Order->getID()?>)">
                  <li><span class="icon-email"></span><span class="button-label">Email</span></li>
                  </a>

                  <a class="circular-button" href="javascript:void(0);" onclick="Orders.deleteOrder(<?= $Order->getID() ?>)">
                  <li><span class="icon-trash-fill"></span><span class="button-label">Delete</span></li>
                  </a>

                
                  <a class="circular-button" href="javascript:void(0);" onclick="Order.expand(<?=$Order->getID()?>)">
                  <li><span class="icon-out-2"></span><span class="button-label">Expand</span></li>
                  </a>

                  <a id="order-tile-select-button" class="circular-button" href="javascript:void(0);" onclick="Orders.toggleSelection(<?= $Order->getID() ?>)">
                  <li><span class="icon-checkmark"></span><span class="button-label">Select</span></li>
                  </a>

            </ul>
            </div>

    </div>

    <?php

    }

    ?>

    </div>
    
<br style="clear: both" />
</li>
    
    
    
    
    
<li class="list-table-cell" id="quick-search-tile-grid">
    <div class="tiles-wrapper">
        
        
    </div>
    
<br style="clear: both" />
</li>
    
    
    
    
    

</ul>





<div id="footer">
&nbsp;
</div>



</div>

</div>


</body>
</html>
