<?php





define ('OUTPUT', 'JSON'); // Whether to output as JSON or HTML (especially errors)...


// LOAD APPLICATION TOP...
require_once ('app_top.php');




define ('PERMS', 'RW');
$WHITE_LIST = array(
						'auth_token',
						'store_id', 
						'payment_type_id' 
					);






// DIE IF INPUT IS NOT VIA POST METHOD...
if ($_SERVER['REQUEST_METHOD'] != 'POST') _catch_error('POST METHOD ONLY PLEASE. NOW GET.', __LINE__, true);


// SANITIZE INPUT DATA...
$_CLEAN = filter_input_array(INPUT_POST, 
								array(
										'auth_token'		=>	FILTER_SANITIZE_STRIPPED, 
										'payment_type_id'	=>	FILTER_VALIDATE_INT, 
										'store_id'	=>	FILTER_VALIDATE_INT

										)
										
								);



// Check if INPUT keys are ALL expected AND MADE IT THROUGH...
if (anySuspects()) _catch_error('Error processing white_list!', __LINE__, true);





// Load store
$store = Cataleya\Store::load($_CLEAN['store_id']);
if ($store == NULL) _catch_error('Store could not be loaded.', __LINE__, true);

// Get store payment_types (ids)
$store_payment_types = $store->getStorePaymentTypeIDs();



		
if (in_array($_CLEAN['payment_type_id'], $store_payment_types)) 
{

    if (!$store->removePaymentType($_CLEAN['payment_type_id'])) _catch_error('Payment Type could not be removed.', __LINE__, true);


    // Output...
    $json_data = array (
                    'status' => 'Ok', 
                    'message' => 'Payment Type removed.', 
                    'payment_type_id' => $_CLEAN['payment_type_id'] 
                    );
    echo json_encode($json_data);
    exit();

} else {

    if (!$store->addPaymentType($_CLEAN['payment_type_id']))  _catch_error('Payment Type could not be added.', __LINE__, true);



                // Output...
                $json_data = array (
                                'status' => 'Ok', 
                                'message' => 'Payment Type added.', 
                                'payment_type_id' => $_CLEAN['payment_type_id'] 
                                );
                echo json_encode($json_data);
                exit();

}



?>