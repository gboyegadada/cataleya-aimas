<?php
/*
FETCH ZONES AND RETURN AS JSON...
*/




define ('OUTPUT', 'JSON'); // Whether to output as JSON or HTML (especially errors)...


// LOAD APPLICATION TOP...
require_once ('app_top.php');





/*
 * 
 * TO PERFORM THIS OPERATION, USER MUST BE AN ADMINISTRATOR
 * 
 */


if (!IS_SUPER_USER || $_admin_role->hasPrivilege('EDIT_ZONES')) _catch_error('You do have permission to perform this action.', __LINE__, true);




// DIE IF INPUT IS NOT VIA POST METHOD...
if ($_SERVER['REQUEST_METHOD'] != 'POST') _catch_error('POST METHOD ONLY PLEASE. NOW GET.');
	

$WHITE_LIST = array(
						'auth-token',  
                                                'payment-type-name', 
                                                'payment-type-status', 
                                                'payment-type-id', 
                                                'payment-type-max-amount', 
                                                'payment-type-min-amount'
					);

// FILTER VALUES ....
$_CLEAN = filter_input_array(INPUT_POST, 
								array(	
                                                                                // auth_token
                                                                                'auth-token'    =>  FILTER_SANITIZE_STRIPPED, 
                                                                    
										 // name...
										'payment-type-name'	=>	array('filter'		=>	FILTER_VALIDATE_REGEXP, 
														'options'	=>	array('regexp' => '/^[A-Za-z0-9,\-\W]{2,400}$/')
																), 
                                                                    
										 // payment-type-status...
										'payment-type-status'	=>	array('filter'		=>	FILTER_VALIDATE_INT, 
																'options'	=>	array('min_range' => 0, 'max_range' => 1)
																), 
                                                                    
										 // zone_id (>0) for save, (0) for create...
										'payment-type-id'	=>	array('filter'		=>	FILTER_VALIDATE_REGEXP, 
														'options'	=>	array('regexp' => '/^[A-Za-z0-9,\-_]{1,160}$/')
																), 
                                                                    
										 // max amount (float)
										'payment-type-max-amount'	=>	FILTER_VALIDATE_FLOAT, 
                                                                    
										 // min amount (float)
										'payment-type-min-amount'	=>	FILTER_VALIDATE_FLOAT

										)
								);



// Check if INPUT keys are ALL expected AND MADE IT THROUGH...
if (anySuspects()) _catch_error('Error processing white_list!', __LINE__, true);
$_handle = $_CLEAN['payment-type-id'];
$_name = \Cataleya\Plugins\Package::getAppName($_handle);
if (!\Cataleya\Plugins\Package::exists($_handle)) _catch_error('Payment type could not be loaded.', __LINE__, true);

$_Currency = Cataleya\Locale\Currency::loadBaseCurrency();


if ((int)$_CLEAN['payment-type-status'] === 1) \Cataleya\Plugins\Package::switchOn($_handle);
else \Cataleya\Plugins\Package::switchOn($_handle);

/*
$PaymentType

->setDisplayName($_CLEAN['payment-type-name'])
->setMaxAmount($_Currency, (float)$_CLEAN['payment-type-max-amount'])
->setMinAmount($_Currency, (float)$_CLEAN['payment-type-min-amount']);
 */

$_max_amount = \Cataleya\Plugins\Action::trigger('payment.get-max-amount', [], [ $_handle ]);
$_min_amount = \Cataleya\Plugins\Action::trigger('payment.get-min-amount', [], [ $_handle ]);
$_currency_code = \Cataleya\Plugins\Action::trigger('payment.get-currency-code', [], [ $_handle ]);


$PaymentType_info = array (
        'id'    => $_handle, 
        'name'  => $_name,  
        'displayName'  => $_name, 
        'enabled'  =>  \Cataleya\Plugins\Package::isOn($_handle), 
        'MaxAmount' => number_format ((float)(!empty($_max_amount)?$_max_amount[0]:0), 2), 
        'MinAmount' => number_format ((float)(!empty($_min_amount)?$_min_amount[0]:0), 2), 
        'Currency' => (!empty($_currency_code) ? strtoupper($_currency_code[0]) : 'XXX')
    );



// OUTPUT...
$json_reply = array (
		"status"=>'ok', 
		"message"=> 'Payment type saved.', 
                "PaymentType"=>$PaymentType_info
		);

echo (json_encode($json_reply));
exit();


