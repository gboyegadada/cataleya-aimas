<?php



define ('OUTPUT', 'JSON'); // Whether to output as JSON or HTML (especially errors)...


// LOAD APPLICATION TOP...
require_once ('app_top.php');




$WHITE_LIST = array(
						'auth_token',  
                                                'target_id', 
                                                'do', 
                                                'data'
						);





// DIE IF INPUT IS NOT VIA POST METHOD...
if ($_SERVER['REQUEST_METHOD'] != 'POST') _catch_error('POST METHOD ONLY PLEASE. NOW GET.', __LINE__, true);

// SANITIZE INPUT DATA...
$_CLEAN = filter_input_array(INPUT_POST, array(
                                                    'auth_token'		=>	FILTER_SANITIZE_STRIPPED,
                                                    'target_id'	=>	FILTER_VALIDATE_INT, 
                                                    'data'	=>	array( 'flags' => FILTER_REQUIRE_ARRAY ), 
                                                    'do'	=>	array('filter'		=>	FILTER_VALIDATE_REGEXP, 
                                                                                                'options'	=>	array('regexp' => '/^[A-Za-z ]{1,50}$/')
                                                                                                                ), 

                                                    )
										
					);


// Check if INPUT keys are ALL expected AND MADE IT THROUGH...
$_suspect = anySuspects();
if ($_suspect !== FALSE) _catch_error('Error processing white_list!' . $_suspect, __LINE__, true);




// VALIDATE AUTH TOKEN...
validate_auth_token ();

// Validate task

$_FUNCS = array (
    'void' => array (
        
        ), 
    'newCategory' => array (
        'store_id' => 'integer', 
        'title' => 'string', 
        'description' => 'html'
        ), 
    'saveCategory' => array ( 
        'title' => 'string', 
        'description' => 'html', 
        'is_active' => 'boolean'
        ), 
    'saveBanner' => array (
        'image_id'=>'integer', 
        'rotate'=>'float', 
        'sw'=>'float', 
        'sh'=>'float', 
        'sx'=>'float', 
        'sy'=>'float'
        ), 
    'deleteCategory' => array (
        
        ), 
    'getInfo' => array (
        
        ), 
    
);





if (!array_key_exists($_CLEAN['do'], $_FUNCS) || !is_callable($_CLEAN['do'])) _catch_error('Invalid action.', __LINE__, true);



    
// OUTPUT...
 $json_reply = array (
                 "status" => 'ok', 
                 "message" => 'Ok'
                 );
 
 

// EXECUTE API CALL

// API FUNC CALL
$_CLEAN['do']();



// HELPER FUNCS


function digestParams () 
{
    global $_CLEAN, $_FUNCS;
    
    $_params = Cataleya\Helper\Validator::params($_FUNCS[$_CLEAN['do']], $_CLEAN['data']);
    $_bad_params = array ();
    

    if (!isset($_params['language_code'])) 
    {
        $_params['language_code'] = DASH_LANGUAGE_CODE;

    }
    
    

    $_params['auto_confirm'] = isset($_params['auto_confirm']) 
                                ? filter_var($_params['auto_confirm'], FILTER_VALIDATE_BOOLEAN) 
                                : FALSE;
    
    
    
    // Check if every one made it through
    
    foreach ($_params as $k=>$v) {
        if ($v === NULL) $_bad_params[] = $k;
    }
    

    if (!empty($_bad_params)) _catch_error('Unable to save.' . implode(', ', $_bad_params), __LINE__, true);
    
    return $_params;
}






/*
 * 
 * 
 * CALLBACKS
 * 
 */


function void () {
    
}






/*
 * 
 * [ newCategory ]
 * ______________________________________________________
 * 
 * @param: int $_POST['target_id']
 * @param: int $_POST['data']
 * 
 */


function newCategory () {

    global $json_reply;
    $_params = digestParams();
    
    
    $_Store = Cataleya\Store::load($_params['store_id']);
    if ($_Store === NULL ) _catch_error('Store could not be found.', __LINE__, true);
    
    
    $_Category = Cataleya\Catalog\Tag::create($_Store, $_params['title'], $_params['description'], 'EN');
    
    
    $json_reply['message'] = 'New category saved.';
    $json_reply['Category'] = Cataleya\Front\Dashboard\Juicer::juiceDrop($_Category);

};




/*
 * 
 * [ saveCategory ]
 * ______________________________________________________
 * 
 * @param: int $_POST['target_id']
 * @param: int $_POST['data']
 * 
 */


function saveCategory () {

    global $_CLEAN, $json_reply;
    
    $_params = digestParams();
    $_Category = Cataleya\Catalog\Tag::load($_CLEAN['target_id']);
    if ($_Category === NULL ) _catch_error('Category could not be found.', __LINE__, true);
    
    // [1] Category Description
    $_Category->getDescription()->setTitle($_params['title'], 'EN');
    $_Category->getDescription()->setText($_params['description'], 'EN');
    
    $_Category->getURLRewrite()->setKeyword($_params['title']);
    
    // [2] Enable  / Disable
    if ($_params['is_active']) $_Category->enable ();
    else $_Category->disable ();
    
    
    $json_reply['message'] = 'New category saved.';
    $json_reply['Category'] = Cataleya\Front\Dashboard\Juicer::juiceDrop($_Category);

};






/*
 * 
 * [ saveBanner ]
 * ______________________________________________________
 * 
 * @param: int $_POST['target_id']
 * @param: int $_POST['data']
 * 
 */


function saveBanner () {

    global $_CLEAN, $json_reply;
    
    $_params = digestParams();
    

    $new_image = Cataleya\Asset\Image::load($_params['image_id']);
    if ((int)$new_image->getID() === 0) _catch_error('Image not found.' . $_params['image_id'], __LINE__, true);
    
    
    $_Category = Cataleya\Catalog\Tag::load($_CLEAN['target_id']);
    if ($_Category === NULL ) 
    {
        $new_image->destroy();
        _catch_error('Category could not be found.', __LINE__, true);
    }
    


    $options = array(
                                    'rotate'=>$_params['rotate'], 
                                    'sw'=>$_params['sw'], 
                                    'sh'=>$_params['sh'], 
                                    'sx'=>$_params['sx'], 
                                    'sy'=>$_params['sy']
                                    ); 
    
    if (!$new_image->bake($options, FALSE)) _catch_error('Error saving image.', __LINE__, true);
    $new_image->makeLarge(800);
    $new_image->makeThumb(200);
    $new_image->makeTiny(100);   
    
    
   // attach image
    $_Category->setBannerImage($new_image);
    
    
    $json_reply['message'] = 'New category saved.';
    $json_reply['Category'] = Cataleya\Front\Dashboard\Juicer::juiceDrop($_Category);

};





/*
 * 
 * [ deleteCategory ]
 * ______________________________________________________
 * 
 * @param: int $_POST['target_id']
 * 
 * 
 */


function deleteCategory () {

    global $_CLEAN, $json_reply;
    $_params = digestParams();
 
    $_Category = Cataleya\Catalog\Tag::load($_CLEAN['target_id']);
    if ($_Category === NULL ) _catch_error('Category could not be found.', __LINE__, true);
    
    if ($_params['auto_confirm'] !== TRUE) 
    {
        $message = 'Are you sure you want to delete this category: "' . $_Category->getDescription()->getTitle('EN') . '"?';
        confirm($message, FALSE);
    }
    
    $_Category->delete();
    
    $json_reply['message'] = 'Category deleted.';
    $json_reply['Category'] = array ('id'=>$_CLEAN['target_id']);

};






/*
 * 
 * [ getInfo ]
 * ______________________________________________________
 * 
 * @param: int $_POST['target_id']
 * 
 * 
 */


function getInfo () {

    global $_CLEAN, $json_reply;
 
    $_Category = Cataleya\Catalog\Tag::load($_CLEAN['target_id']);
    if ($_Category === NULL ) _catch_error('Category could not be found.', __LINE__, true);
    
    $json_reply['message'] = 'New category saved.';
    $json_reply['Category'] = Cataleya\Front\Dashboard\Juicer::juiceDrop($_Category);

};






// Remove token...
if (isset($_SESSION[SESSION_PREFIX.'CONFIRM_TOKEN'])) unset ($_SESSION[SESSION_PREFIX.'CONFIRM_TOKEN']);




 echo (json_encode($json_reply));
 exit();  
