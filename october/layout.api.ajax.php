<?php



define ('OUTPUT', 'JSON'); // Whether to output as JSON or HTML (especially errors)...


// LOAD APPLICATION TOP...
require_once ('app_top.php');




$WHITE_LIST = array(
						'auth_token',  
                                                'target_id', 
                                                'do', 
                                                'data'
						);





// DIE IF INPUT IS NOT VIA POST METHOD...
if ($_SERVER['REQUEST_METHOD'] != 'POST') _catch_error('POST METHOD ONLY PLEASE. NOW GET.', __LINE__, true);

// SANITIZE INPUT DATA...
$_CLEAN = filter_input_array(INPUT_POST, array(
                                                    'auth_token'		=>	FILTER_SANITIZE_STRIPPED,
                                                    'target_id'	=>	FILTER_VALIDATE_INT, 
                                                    'data'	=>	array( 'flags' => FILTER_REQUIRE_ARRAY ), 
                                                    'do'	=>	array('filter'		=>	FILTER_VALIDATE_REGEXP, 
                                                                                                'options'	=>	array('regexp' => '/^[A-Za-z ]{1,50}$/')
                                                                                                                ), 

                                                    )
										
					);


// Check if INPUT keys are ALL expected AND MADE IT THROUGH...
$_suspect = anySuspects();
if ($_suspect !== FALSE) _catch_error('Error processing white_list!' . $_suspect, __LINE__, true);




// VALIDATE AUTH TOKEN...
validate_auth_token ();

// Validate task

$_FUNCS = array (
    'void', 
    
    'newTile', 
    'deleteTile', 
    'saveTileSortOrder', 
    'saveTile', 
    'getTile', 
    
    'newSlide', 
    'deleteSlide', 
    'saveSlide',  
    'saveSlideImage', 
    'getSlide', 
    
    'getLayout', 
    'saveLayoutSortOrder'
    
);





if (!in_array($_CLEAN['do'], $_FUNCS) || !is_callable($_CLEAN['do'])) _catch_error('Invalid action.', __LINE__, true);



    
// OUTPUT...
 $json_reply = array (
                 "status" => 'ok', 
                 "message" => 'Ok'
                 );
 
 

// EXECUTE API CALL

// API FUNC CALL
$_CLEAN['do']();



// HELPER FUNCS


function digestParams () 
{
    global $_CLEAN;
    $_params = $_CLEAN['data'];
    $_bad_params = array ();
    

    if (isset($_params['sort_order'])) 
    {

        $_params['sort_order'] = Cataleya\Helper\Validator::int($_params['sort_order']);
        if ($_params['sort_order'] === FALSE) $_bad_params[] = 'sort_order';

    }
    
    
    if (isset($_params['layout_sort_order'])) 
    {
        if (!is_array($_params['layout_sort_order'])) $_bad_params[] = 'layout_sort_order';
        else foreach ($_params['layout_sort_order'] as $k=>$_id) if (!is_numeric($k) || !is_numeric($_id)) { $_bad_params[] = 'layout_sort_order'; break; }
    }
    
    
    
    if ($_CLEAN['do'] === 'newTile' || $_CLEAN['do'] === 'saveTile') 
    {

        $_params['pixel_width'] = Cataleya\Helper\Validator::int($_params['pixel_width'], Cataleya\Layout\ShopFront\Home\Layout::PIXEL_WIDTH_MULTIPLE, Cataleya\Layout\ShopFront\Home\Layout::PIXEL_WIDTH_MULTIPLE*6);
        if ($_params['pixel_width'] === FALSE) $_bad_params[] = 'pixel_width';
        
        $_params['pixel_height'] = Cataleya\Helper\Validator::int($_params['pixel_height'], Cataleya\Layout\ShopFront\Home\Layout::PIXEL_HEIGHT_MULTIPLE, Cataleya\Layout\ShopFront\Home\Layout::PIXEL_HEIGHT_MULTIPLE*200);
        if ($_params['pixel_height'] === FALSE) $_bad_params[] = 'pixel_height';

    }
    
    
    
    if (isset($_params['text'], $_params['href'], $_params['target'])) 
    {

        $_params['language_code'] = 'EN';
        
        if ($_CLEAN['do'] === 'newSlide' && (!isset($_params['type']) || !in_array($_params['type'], array('text', 'image')))) $_bad_params[] = 'type';
        
        $_params['text'] = Cataleya\Helper\Validator::html($_params['text'], 0, 1500);
        if ($_params['text'] === FALSE) $_bad_params[] = 'text';
        
        if ($_params['href'] === '#') $_params['href'] = 'http://' . DOMAIN;
        else $_params['href'] = Cataleya\Helper\Validator::url($_params['href']);
        
        if ($_params['href'] === FALSE) $_bad_params[] = 'href';

        $_params['target'] = Cataleya\Helper\Validator::bool($_params['target']);
        if ($_params['target'] === NULL) $_bad_params[] = 'target';
        else $_params['target'] = ($_params['target']) ? '_blank' : '_self';
        

        $_params['type'] = Cataleya\Helper\Validator::bool($_params['type']);
        if ($_params['type'] === NULL) $_bad_params[] = 'type';
        else $_params['type'] = ($_params['type']) ? 'image' : 'text';

    }
    
    
    if (isset($_params['image_id'])) 
    {
            $_params['image_id'] = Cataleya\Helper\Validator::int($_params['image_id']);
            $_params['rotate'] = Cataleya\Helper\Validator::int($_params['rotate'], 0, 360);
            $_params['sw'] = Cataleya\Helper\Validator::float($_params['sw']);
            $_params['sh'] = Cataleya\Helper\Validator::float($_params['sh']); 
            $_params['sx'] = Cataleya\Helper\Validator::float($_params['sx']); 
            $_params['sy'] = Cataleya\Helper\Validator::float($_params['sy']);
            
            
            if (
                    $_params['image_id'] === FALSE || 
                    $_params['rotate'] === FALSE || 
                    $_params['sw'] === FALSE || 
                    $_params['sh'] === FALSE || 
                    $_params['sx'] === FALSE || 
                    $_params['sy'] === FALSE )
                
                $_bad_params[] = 'image params';
    }
    
   
    
    // Check if every one made it through
    if (!empty($_bad_params)) 
    {
        if (isset($_params['image_id'])) 
        {
            $new_image = Cataleya\Asset\Image::load($_params['image_id']);
            if ((int)$new_image->getID() !== 0) $new_image->destroy();
        }
        
        _catch_error('Bad params.', __LINE__, true);
    }
    
    return $_params;
}







function juiceLayout (Cataleya\Layout\ShopFront\Home\Layout $_Layout) 
{
    
    $_Store = $_Layout->getStore();
    
    $_info = array (
        'id'    =>   $_Layout->getID(), 
        'Tiles'   =>  array (), 
        'Store' =>  array (
            'id' => $_Store->getStoreId(), 
            'title' => $_Store->getDescription()->getTitle('EN')
        )
    );
    
    
    foreach ($_Layout as $_Tile) $_info['Tiles'][] = juiceTile($_Tile);
    
    return $_info;
    
    
}







function juiceTile (Cataleya\Layout\ShopFront\Home\Tile $_Tile) 
{
    $_max_width = Cataleya\Layout\ShopFront\Home\Layout::PIXEL_WIDTH_MULTIPLE*6;
    $_pixel_width = $_Tile->getPixelWidth();
    $_adjusted_pixel_width = ($_pixel_width < $_max_width) ? $_pixel_width-5 : $_pixel_width;
    
    $_info = array (
        'id'    =>   $_Tile->getID(), 
        'population'  =>  $_Tile->getPopulation(), 
        'noSlides'  =>  (($_Tile->getPopulation() < 1) ? true : false), 
        'pixel_height' => $_Tile->getPixelHeight(),  
        'pixel_width' => $_pixel_width, 
        'adjusted_pixel_width' => $_adjusted_pixel_width, // compensate for margin 
        'slide_strip_pixel_width' => ($_adjusted_pixel_width*$_Tile->getPopulation()), 
        'sort_order'  =>   $_Tile->getSortOrder(), 
        'Slides'   =>  array ()
    );
    
    
    foreach ($_Tile as $_Slide) $_info['Slides'][] = juiceSlide($_Slide);
    
    return $_info;
    
    
}








function juiceSlide (Cataleya\Layout\ShopFront\Home\Slide $_Slide) 
{

    $_Tile = $_Slide->getTile();
    $_max_width = Cataleya\Layout\ShopFront\Home\Layout::PIXEL_WIDTH_MULTIPLE*6;
    $_pixel_width = $_Tile->getPixelWidth();
    $_adjusted_pixel_width = ($_pixel_width < $_max_width) ? $_pixel_width-5 : $_pixel_width;
    
    $_info = array (
        'id'    =>   $_Slide->getID(), 
        'type'   =>  $_Slide->getType(), 
        'is_text'   =>  ($_Slide->getType() === 'text') ? TRUE : FALSE, 
        'is_image'   =>  ($_Slide->getType() === 'image') ? TRUE : FALSE, 
        'text'  =>  $_Slide->getText(), 
        'image' =>  $_Slide->getImage()->getHrefs(), 
        'pixel_height' => $_Tile->getPixelHeight(),  
        'pixel_width' => $_pixel_width, 
        'adjusted_pixel_width' => $_adjusted_pixel_width, // compensate for margin 
        'href'  =>  $_Slide->getHref(), 
        'target'    =>  $_Slide->getTarget(), 
        'open_in_new_window'    =>  (($_Slide->getTarget() === '_blank') ? true : false), 
        'sort_order'    =>  $_Slide->getSortOrder()
    );
    
    return $_info;
    
    
}







/*
 * 
 * 
 * CALLBACKS
 * 
 */


function void () {
    
}









/*
 * 
 * [ getLayout ]
 * ______________________________________________________
 * 
 * @param: int $_POST['target_id']
 * @param: int $_POST['data']
 * 
 */


function getLayout () {

    global $json_reply, $_CLEAN;

    $_Store = Cataleya\Store::load($_CLEAN['target_id']);
    if ($_Store === NULL ) _catch_error('Store could not be found.', __LINE__, true);
    
    $_Layout = Cataleya\Layout\ShopFront\Home\Layout::load($_Store);
    if ($_Layout === NULL ) _catch_error('Shop layout could not be loaded.', __LINE__, true);
    
    
    $json_reply['message'] = 'Layout info';
    $json_reply['Layout'] = juiceLayout($_Layout);

};






/*
 * 
 * [ saveLayoutSortOrder ]
 * ______________________________________________________
 * 
 * @param: int $_POST['target_id']
 * @param: int $_POST['data']
 * 
 */


function saveLayoutSortOrder () {

    global $json_reply, $_CLEAN;
    $_params = digestParams();

    $_Store = Cataleya\Store::load($_CLEAN['target_id']);
    if ($_Store === NULL ) _catch_error('Store could not be found.', __LINE__, true);
    
    $_Layout = Cataleya\Layout\ShopFront\Home\Layout::load($_Store);
    if ($_Layout === NULL ) _catch_error('Shop layout could not be loaded.', __LINE__, true);
    
    $_Layout->saveSortOrder($_params['layout_sort_order']);
    
    
    $json_reply['message'] = 'Layout info';
    $json_reply['Layout'] = juiceLayout($_Layout);

};






/*
 * 
 * [ getTile ]
 * ______________________________________________________
 * 
 * @param: int $_POST['target_id']
 * @param: int $_POST['data']
 * 
 */


function getTile () {

    global $json_reply, $_CLEAN;

    $_Tile = Cataleya\Layout\ShopFront\Home\Tile::load($_CLEAN['target_id']);
    if ($_Tile === NULL ) _catch_error('Tile could not be found.', __LINE__, true);
    
    
    $json_reply['message'] = 'Tile info';
    $json_reply['Tile'] = juiceTile($_Tile);

};









/*
 * 
 * [ getSlide ]
 * ______________________________________________________
 * 
 * @param: int $_POST['target_id']
 * @param: int $_POST['data']
 * 
 */


function getSlide () {

    global $json_reply, $_CLEAN;

    $_Slide = Cataleya\Layout\ShopFront\Home\Slide::load($_CLEAN['target_id']);
    if ($_Slide === NULL ) _catch_error('Slide could not be found.', __LINE__, true);
    
    
    $json_reply['message'] = 'Slide info';
    $json_reply['Slide'] = juiceSlide($_Slide);

};









/*
 * 
 * [ newTile ]
 * ______________________________________________________
 * 
 * @param: int $_POST['target_id']
 * @param: int $_POST['data']
 * 
 */


function newTile () {

    global $json_reply, $_CLEAN;
    $_params = digestParams();
    
    $_Store = Cataleya\Store::load($_CLEAN['target_id']);
    if ($_Store === NULL ) _catch_error('Store could not be found.', __LINE__, true);
    
    $_Layout = Cataleya\Layout\ShopFront\Home\Layout::load($_Store);
    if ($_Layout === NULL ) _catch_error('Shop layout could not be loaded.', __LINE__, true);
    
    $_Tile = $_Layout->newTile($_params['pixel_width'], $_params['pixel_height']);
    
    $json_reply['message'] = 'New tile created.';
    $json_reply['Tile'] = juiceTile($_Tile);

};






/*
 * 
 * [ newSlide ]
 * ______________________________________________________
 * 
 * @param: int $_POST['target_id']
 * @param: int $_POST['data']
 * 
 */


function newSlide () {

    global $json_reply, $_CLEAN;
    $_params = digestParams();
    
    $_Tile = Cataleya\Layout\ShopFront\Home\Tile::load($_CLEAN['target_id']);
    if ($_Tile === NULL ) _catch_error('Tile could not be found.', __LINE__, true);
    
    
    $_Slide = $_Tile->newSlide(NULL, $_params);
    
    $json_reply['message'] = 'New slide created.';
    $json_reply['Slide'] = juiceSlide($_Slide);
    $json_reply['Tile'] = juiceTile($_Slide->getTile());

};






/*
 * 
 * [ deleteTile ]
 * ______________________________________________________
 * 
 * @param: int $_POST['target_id']
 * @param: int $_POST['data']
 * 
 */


function deleteTile () {

    global $json_reply, $_CLEAN;

    $_Tile = Cataleya\Layout\ShopFront\Home\Tile::load($_CLEAN['target_id']);
    if ($_Tile === NULL ) _catch_error('Tile could not be found.', __LINE__, true);
    
    $message = 'Are you sure you want to delete this tile?';
    confirm($message, FALSE);
    
    
    $_Tile->delete();
    
    $json_reply['message'] = 'Tile deleted.';
    $json_reply['Tile'] = array ('id'=>$_Tile->getID());

};






/*
 * 
 * [ deleteSlide ]
 * ______________________________________________________
 * 
 * @param: int $_POST['target_id']
 * @param: int $_POST['data']
 * 
 */


function deleteSlide () {

    global $json_reply, $_CLEAN;

    $_Slide = Cataleya\Layout\ShopFront\Home\Slide::load($_CLEAN['target_id']);
    if ($_Slide === NULL ) _catch_error('Slide could not be found.', __LINE__, true);
    
    $message = 'Are you sure you want to delete this slide?';
    confirm($message, FALSE);
    
    $_Slide->delete();
    
    $json_reply['message'] = 'Slide deleted.';
    $json_reply['Slide'] = array ('id'=>$_Slide->getID());
    $json_reply['Tile'] = juiceTile($_Slide->getTile());

};












/*
 * 
 * [ saveTileSortOrder ]
 * ______________________________________________________
 * 
 * @param: int $_POST['target_id']
 * @param: int $_POST['data']
 * 
 */


function saveTileSortOrder () {

    global $json_reply, $_CLEAN;
    $_params = digestParams();

    $_Tile = Cataleya\Layout\ShopFront\Home\Tile::load($_CLEAN['target_id']);
    if ($_Tile === NULL ) _catch_error('Tile could not be found.', __LINE__, true);
    
    $_Tile->setSortOrder($_params['sort_order']);
    
    $json_reply['message'] = 'Sort order saved.';
    $json_reply['Tile'] = array ('id'=>$_Tile->getID());

};








/*
 * 
 * [ saveSlideSortOrder ]
 * ______________________________________________________
 * 
 * @param: int $_POST['target_id']
 * @param: int $_POST['data']
 * 
 */


function saveSlideSortOrder () {

    global $json_reply, $_CLEAN;
    $_params = digestParams();

    $_Slide = Cataleya\Layout\ShopFront\Home\Slide::load($_CLEAN['target_id']);
    if ($_Slide === NULL ) _catch_error('Slide could not be found.', __LINE__, true);
    
    $_Slide->setSortOrder($_params['sort_order']);
    
    $json_reply['message'] = 'Sort order saved.';
    $json_reply['Slide'] = array ('id'=>$_Slide->getID());

};







/*
 * 
 * [ saveTile ]
 * ______________________________________________________
 * 
 * @param: int $_POST['target_id']
 * @param: int $_POST['data']
 * 
 */


function saveTile () {

    global $json_reply, $_CLEAN;
    $_params = digestParams();
    
    $_Tile = Cataleya\Layout\ShopFront\Home\Tile::load($_CLEAN['target_id']);
    if ($_Tile === NULL ) _catch_error('Tile could not be found.', __LINE__, true);
    
    $_Tile
    
    ->setPixelWidth($_params['pixel_width'])
    ->setPixelHeight($_params['pixel_height'])
    ->saveData();
    
    $json_reply['message'] = 'Save tile created.';
    $json_reply['Tile'] = juiceTile($_Tile);

};




/*
 * 
 * [ saveSlide ]
 * ______________________________________________________
 * 
 * @param: int $_POST['target_id']
 * @param: int $_POST['data']
 * 
 */


function saveSlide () {

    global $json_reply, $_CLEAN;
    $_params = digestParams();
    
    $_Slide = Cataleya\Layout\ShopFront\Home\Slide::load($_CLEAN['target_id']);
    if ($_Slide === NULL ) _catch_error('Slide could not be found.', __LINE__, true);
    
    
    $_Slide
    
    ->setType($_params['type'])
    ->setText($_params['text'], $_params['language_code'])
    ->setTarget($_params['target'])
    ->setHref($_params['href'])
    ->saveData();
    
    $json_reply['message'] = 'Save slide created.';
    $json_reply['Slide'] = juiceSlide($_Slide);
    $json_reply['Tile'] = juiceTile($_Slide->getTile());

};




/*
 * 
 * [ saveSlideImage ]
 * ______________________________________________________
 * 
 * @param: int $_POST['target_id']
 * @param: int $_POST['data']
 * 
 */


function saveSlideImage () {

    global $json_reply, $_CLEAN;
    
    $_params = digestParams();
    

    $new_image = Cataleya\Asset\Image::load($_params['image_id']);
    if ((int)$new_image->getID() === 0) _catch_error('Image not found.' . $_params['image_id'], __LINE__, true);
    
    $_Slide = Cataleya\Layout\ShopFront\Home\Slide::load($_CLEAN['target_id']);
    if ($_Slide === NULL ) 
    {
        $new_image->destroy();
        _catch_error('Slide could not be found.', __LINE__, true);
    }

    $options = array(
                                    'rotate'=>$_params['rotate'], 
                                    'sw'=>$_params['sw'], 
                                    'sh'=>$_params['sh'], 
                                    'sx'=>$_params['sx'], 
                                    'sy'=>$_params['sy']
                                    ); 
    
    if (!$new_image->bake($options, FALSE)) 
    {
        $new_image->destroy();
        _catch_error('Error saving image.', __LINE__, true);
    }
    
    $_Tile = $_Slide->getTile();
    $_square_dim = ($_Tile->getPixelHeight() > $_Tile->getPixelWidth()) ? $_Tile->getPixelHeight() : $_Tile->getPixelWidth();
    
    $new_image->makeLarge($_square_dim);
    $new_image->makeThumb(120);
    $new_image->makeTiny(60);   
    
    
   // attach image
    $_Slide->setImage($new_image);
    
    
    $json_reply['message'] = 'Slide image saved.';
    $json_reply['Slide'] = juiceSlide($_Slide);

};











// Remove token...
if (isset($_SESSION[SESSION_PREFIX.'CONFIRM_TOKEN'])) unset ($_SESSION[SESSION_PREFIX.'CONFIRM_TOKEN']);




 echo (json_encode($json_reply));
 exit();  

?>