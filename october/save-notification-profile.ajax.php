<?php
/*
FETCH TAX CLASSES AND RETURN AS JSON...
*/




define ('OUTPUT', 'JSON'); // Whether to output as JSON or HTML (especially errors)...


// LOAD APPLICATION TOP...
require_once ('app_top.php');





/*
 * 
 * TO PERFORM THIS OPERATION, USER MUST BE AN ADMINISTRATOR
 * 
 */


if (!IS_SUPER_USER || $_admin_role->hasPrivilege('EDIT_ADVANCED_SETTINGS')) _catch_error('You do have permission to perform this action.', __LINE__, true);




// DIE IF INPUT IS NOT VIA POST METHOD...
if ($_SERVER['REQUEST_METHOD'] != 'POST') _catch_error('POST METHOD ONLY PLEASE. NOW GET.');
	

$WHITE_LIST = array(
						'auth-token',  
                                                'notification-id', 
                                                'keyword', 
                                                'title',
                                                'description', 
                                                'entity-name', 
                                                'entity-name-plural', 
                                                //'notification-text', 
                                                'send-after-period', 
                                                'send-after-interval', 
                                                'send-after-increments', 
                                                'recipients-email',
                                                'recipients-dashboard'
                                                
					);

// FILTER VALUES ....
$_CLEAN = filter_input_array(INPUT_POST, 
								array(	
                                                                                // auth_token
                                                                                'auth-token'    =>  FILTER_SANITIZE_STRIPPED, 
                                                                    
										'notification-id'	=>	FILTER_VALIDATE_INT, 
                                                                    
										'keyword'	=>	array('filter'		=>	FILTER_VALIDATE_REGEXP, 
														'options'	=>	array('regexp' => '/^(([A-Za-z0-9\-]+\.?){1,10})$/')
																), 

										'title'	=>	array('filter'		=>	FILTER_VALIDATE_REGEXP, 
														'options'	=>	array('regexp' => '/^[A-Za-z0-9,\-\W]{2,240}$/')
																), 
										'description'	=>	array('filter'		=>	FILTER_VALIDATE_REGEXP, 
														'options'	=>	array('regexp' => '/^[A-Za-z0-9,\-\W]{2,1500}$/')
																), 
                                                                                'entity-name'	=>	array('filter'		=>	FILTER_VALIDATE_REGEXP, 
                                                                                                'options'	=>	array('regexp' => '/^[A-Za-z0-9\- ]{2,60}$/')
                                                                                                                ), 
                                                                                'entity-name-plural'	=>	array('filter'		=>	FILTER_VALIDATE_REGEXP, 
                                                                                                'options'	=>	array('regexp' => '/^[A-Za-z0-9\- ]{2,60}$/')
                                                                                                                ), 
                                                                                //'notification-text'	=>	array('filter'		=>	FILTER_VALIDATE_REGEXP, 
                                                                                //                'options'	=>	array('regexp' => '/^[A-Za-z0-9,\-\W]{2,1000}$/')
                                                                                //                                ), 
                                                                                'send-after-period'	=>	FILTER_VALIDATE_INT, 
                                                                                'send-after-interval'	=>	array('filter'		=>	FILTER_VALIDATE_REGEXP, 
                                                                                                'options'	=>	array('regexp' => '/^(second|minute|hour|day|week|month|year|decade|century)s?$/')
                                                                                                                ), 

										'send-after-increments'	=>	array('filter'		=>	FILTER_VALIDATE_INT, 
																'options'	=>	array('min_range' => 0, 'max_range' => 200)
																), 
										'recipients-email'	=>	array('filter'		=>	FILTER_VALIDATE_INT, 
														'flags'	=>	FILTER_REQUIRE_ARRAY
																), 
										'recipients-dashboard'	=>	array('filter'		=>	FILTER_VALIDATE_INT, 
														'flags'	=>	FILTER_REQUIRE_ARRAY
																), 

										)
								);



// Quick fix to avoid triggering white list error..
if ($_CLEAN['recipients-email'] === NULL) $_CLEAN['recipients-email'] = array();
if ($_CLEAN['recipients-dashboard'] === NULL) $_CLEAN['recipients-dashboard'] = array();

// Check if INPUT keys are ALL expected AND MADE IT THROUGH...
$_suspect = anySuspects();
if ($_suspect !== FALSE) _catch_error('Error processing white_list! (' . $_suspect . ')', __LINE__, true);



$NotificationProfile = Cataleya\Admin\Notification::load($_CLEAN['keyword']);
if ($NotificationProfile === NULL) _catch_error('Notification profile could not be loaded.', __LINE__, true);


// Save changes
$_description = $NotificationProfile->getDescription();
$_description->setTitle($_CLEAN['title'], 'EN');
$_description->setText($_CLEAN['description'], 'EN');

$NotificationProfile->setEntityName($_CLEAN['entity-name'])
                    ->setEntityNamePlural($_CLEAN['entity-name-plural'])
                    ->setSendAfterPeriod($_CLEAN['send-after-period'])
                    ->setSendAfterInterval($_CLEAN['send-after-interval'])
                    ->setSendAfterIncrements($_CLEAN['send-after-increments']);




// Retrieve info about notification profile (and save alert setting along the way)


foreach ($NotificationProfile as $Alert) 
{
    
    
    $AdminUser = $Alert->getRecipient();
    $_user_id = (int)$AdminUser->getAdminId();
    
    // Save alert prefs
    
    // get emails ?
    if ( in_array($_user_id, $_CLEAN['recipients-email']) ) $Alert->enableEmail();
    else $Alert->disableEmail();
    
    // get dashboard alerts ?
    if ( in_array($_user_id, $_CLEAN['recipients-dashboard']) ) $Alert->enableDashboard();
    else $Alert->disableDashboard();
    
    // Get user info
    $admin_profiles_info[] = array (
        'id'    =>  $_user_id, 
        'name'  =>  $AdminUser->getName(), 
        'firstname'  =>  $AdminUser->getFirstname(), 
        'lastname'  =>  $AdminUser->getLastname(), 
        'emailEnabled'  =>  $Alert->emailEnabled(),  
        'dashboardEnabled'  =>  $Alert->dashboardEnabled()
    );
}


$notification_profile_info = array (
    'id'    =>  $NotificationProfile->getID(), 
    'keyword'    =>  $NotificationProfile->getKeyword(), 
    'title'  =>  $NotificationProfile->getDescription()->getTitle('EN'), 
    'description'  =>  $NotificationProfile->getDescription()->getText('EN'), 
    'entityName'  =>  $NotificationProfile->getEntityName(), 
    'entityNamePlural'  =>  $NotificationProfile->getEntityNamePlural(), 
    'notificationText'  =>  $NotificationProfile->getNotificationText(), 
    'sendAfterPeriod'  =>  $NotificationProfile->getSendAfterPeriod(), 
    'sendAfterInterval'  =>  $NotificationProfile->getSendAfterInterval(), 
    'sendAfterIncrements'  =>  $NotificationProfile->getSendAfterIncrements(),  
    'incrementsSinceLastSent'  =>  $NotificationProfile->getIncrementsSinceLastSent(), 
    'url'  =>  $NotificationProfile->getRequestUri(),
    'icon'  =>  $NotificationProfile->getIcon(), 
    'recipients'    =>   $admin_profiles_info
);



// OUTPUT...
$json_reply = array (
		"status"=>'ok', 
		"message"=> 'Notification profile saved.', 
                "NotificationProfile"=>$notification_profile_info
		);

echo (json_encode($json_reply));
exit();



?>