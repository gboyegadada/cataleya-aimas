<?php



define ('OUTPUT', 'JSON'); // Whether to output as JSON or HTML (especially errors)...


// LOAD APPLICATION TOP...
require_once ('app_top.php');





/*
 * 
 * CHECK USER PRIVILEGES
 * 
 */


if (!IS_SUPER_USER || $_admin_role->hasPrivilege('DELETE_TAX_CLASS')) _catch_error('You do have permission to perform this action.', __LINE__, true);




$WHITE_LIST = array(
						'auth_token', 
						'tax_class_id'
						);





// DIE IF INPUT IS NOT VIA POST METHOD...
if ($_SERVER['REQUEST_METHOD'] != 'POST') _catch_error('POST METHOD ONLY PLEASE. NOW GET.', __LINE__, true);

// SANITIZE INPUT DATA...
$_CLEAN = filter_input_array(INPUT_POST, array(
                                                    'auth_token'		=>	FILTER_SANITIZE_STRIPPED, 
                                                    'tax_class_id'	=>	FILTER_VALIDATE_INT

                                                    )
										
					);


// Check if INPUT keys are ALL expected AND MADE IT THROUGH...
if (anySuspects()) _catch_error('Error processing white_list!', __LINE__, true);



// VALIDATE AUTH TOKEN...
validate_auth_token ();


// Load 
$TaxClass = Cataleya\Tax\TaxClass::load($_CLEAN['tax_class_id']);


// Check if tax class exists...
if ($TaxClass === NULL)
{
        if (isset($_SESSION[SESSION_PREFIX.'CONFIRM_PARAMS'])) unset ($_SESSION[SESSION_PREFIX.'CONFIRM_PARAMS']);
        
        // ERROR...
        _catch_error('Tax class not found.', __LINE__, true);

}


// Confirm delete action
$message = 'Are you sure you want to remove this tax class?';
confirm($message, FALSE);


// retrieve info about tax class to be deleted

$TaxClass_info = array (
        'id'    =>  $TaxClass->getID(), 
        'name'  => $TaxClass->getDescription()->getTitle('EN'), 
        'description'  =>  $TaxClass->getDescription()->getText('EN'),
        'population' => $TaxClass->getPopulation(), 
        'populationAsText' => count_in_english($TaxClass->getPopulation(), 'Tax rate', 'Tax rates')
    );



// Delete tax class (if we make it past the 'confirm' function)...
$TaxClass->delete();




// Remove token...
if (isset($_SESSION[SESSION_PREFIX.'CONFIRM_TOKEN'])) unset ($_SESSION[SESSION_PREFIX.'CONFIRM_TOKEN']);




// OUTPUT...
 $json_reply = array (
                 "status" => 'ok', 
                 "message" => 'Tax class deleted.',
                 "TaxClass"=>$TaxClass_info
                 );


 echo (json_encode($json_reply));
 exit();  

?>