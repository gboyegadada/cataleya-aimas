<?php





define ('OUTPUT', 'HTML'); // Whether to output as JSON or HTML (especially errors)...


// LOAD APPLICATION TOP...
require_once ('app_top.php');







// GET JSON
$_JSON = get_json();

if ($_JSON === FALSE || !isset($_JSON->auth_token) || !isset($_JSON->product_id)) _catch_error('Bad params.', __LINE__, true);

// Check if this request hasn't already been processed.
// This is only needed when query is sent in as JSON.
if (isset($_JSON->rt)) checkRequestToken($_JSON->rt);


$_CLEAN = array (
                    'auth_token' => filter_var($_JSON->auth_token, FILTER_SANITIZE_STRIPPED), 
                    'product_id' =>  filter_var($_JSON->product_id, FILTER_VALIDATE_INT), 
                    'pricing_by' =>  filter_var($_JSON->pricing_by, FILTER_VALIDATE_REGEXP, array('options'=>array('regexp' => '/^(flat|option|color|size)$/'))), 
                    'confirm_token' =>  isset($_JSON->confirm_token) ? filter_var($_JSON->confirm_token, FILTER_SANITIZE_STRIPPED) : '', 
                    'whisper' =>  isset($_JSON->whisper) ? filter_var($_JSON->whisper, FILTER_SANITIZE_STRIPPED) : '', 
                    'rt'    =>  isset($_JSON->rt) ? filter_var($_JSON->rt, FILTER_VALIDATE_REGEXP, array('options'=>array('regexp' => '/^[A-Za-z0-9,\-]{20}$/'))) : ''
        
        );




foreach ($_CLEAN as $k=>$v) 
{
    if ($v === FALSE) _catch_error('Bad params.'.$k, __LINE__, true);
}


// VALIDATE AUTH TOKEN...
validate_auth_token ();











// Output...
$json_data = array (
                'status' => 'Ok', 
                'message' => 'New price saved.'
                );
echo json_encode($json_data);
exit();








?>