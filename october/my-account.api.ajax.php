<?php



define ('OUTPUT', 'JSON'); // Whether to output as JSON or HTML (especially errors)...


// LOAD APPLICATION TOP...
require_once ('app_top.php');




$WHITE_LIST = array(
						'auth_token',  
                                                'do', 
                                                'data'
						);





// DIE IF INPUT IS NOT VIA POST METHOD...
if ($_SERVER['REQUEST_METHOD'] != 'POST') _catch_error('POST METHOD ONLY PLEASE. NOW GET.', __LINE__, true);

// SANITIZE INPUT DATA...
$_CLEAN = filter_input_array(INPUT_POST, array(
                                                    'auth_token'		=>	FILTER_SANITIZE_STRIPPED,
                                                    'data'	=>	array( 'filter'=> FILTER_SANITIZE_STRIPPED, 'flags' => FILTER_REQUIRE_ARRAY ), 
                                                    'do'	=>	array('filter'		=>	FILTER_VALIDATE_REGEXP, 
                                                                                                'options'	=>	array('regexp' => '/^[A-Za-z ]{1,50}$/')
                                                                                                                ), 

                                                    )
										
					);


// Check if INPUT keys are ALL expected AND MADE IT THROUGH...
$_suspect = anySuspects();
if ($_suspect !== FALSE) _catch_error('Error processing white_list!' . $_suspect, __LINE__, true);




// VALIDATE AUTH TOKEN...
validate_auth_token ();

// Validate task

$_FUNCS = array (
    'void', 
    'saveProfile', 
    'saveDisplayPicture', 
    'getInfo'
    
);





if (!in_array($_CLEAN['do'], $_FUNCS) || !is_callable($_CLEAN['do'])) _catch_error('Invalid action.', __LINE__, true);



    
// OUTPUT...
 $json_reply = array (
                 "status" => 'ok', 
                 "message" => 'Ok', 
                 "failedValidation" => FALSE, 
                 "failedItems"  =>  array ()
                 
                 );
 
// Load my account
$_MyAccount = (!empty($adminUser)) ? $adminUser : Cataleya\Admin\User::load($_SESSION[SESSION_PREFIX.'ADMIN_ID']);
 
 

// EXECUTE API CALL

// API FUNC CALL
$_CLEAN['do']();



// HELPER FUNCS


function digestParams () 
{
    global $_CLEAN, $json_reply;
    $_params = $_CLEAN['data'];
    $_bad_params = array ();
    

    if (isset($_params['email'])) 
    {

        $_params['fname'] = Cataleya\Helper\Validator::name($_params['fname']);
        if ($_params['fname'] === FALSE) $_bad_params[] = 'fname';
        
        $_params['lname'] = Cataleya\Helper\Validator::name($_params['lname']);
        if ($_params['lname'] === FALSE) $_bad_params[] = 'lname';
        
        $_params['email'] = Cataleya\Helper\Validator::email($_params['email']);
        if ($_params['email'] === FALSE) $_bad_params[] = 'email';
        
        $_params['phone'] = Cataleya\Helper\Validator::phone($_params['phone'], 0);
        if ($_params['phone'] === FALSE) $_bad_params[] = 'phone';
        
        $_params['set_password'] = Cataleya\Helper\Validator::bool($_params['set_password']);
        if ($_params['set_password'] === NULL) $_bad_params[] = 'set_password';
        
        
        if ($_params['set_password'] === TRUE && isset($_params['password'], $_params['confirm_password']))
        {
            $_params['password'] = Cataleya\Helper\Validator::password($_params['password']);
            if ($_params['password'] === FALSE) $_bad_params[] = 'password';

            $_params['confirm_password'] = Cataleya\Helper\Validator::password($_params['confirm_password']);
            if ($_params['confirm_password'] === FALSE || $_params['confirm_password'] !== $_params['confirm_password']) $_bad_params[] = 'confirm_password';
        }  

    }
    


    
    
    // Check if every one made it through
    if (!empty($_bad_params)) 
    {
        $json_reply['failedValidation'] = TRUE; 
        $json_reply['failedItems']  = $_bad_params; 
        
        echo (json_encode($json_reply));
        exit(); 
    }
    
    
    
    if (isset($_params['image_id'])) 
    {
            $_params['image_id'] = Cataleya\Helper\Validator::int($_params['image_id']);
            $_params['rotate'] = Cataleya\Helper\Validator::int($_params['rotate'], 0, 360);
            $_params['sw'] = Cataleya\Helper\Validator::float($_params['sw']);
            $_params['sh'] = Cataleya\Helper\Validator::float($_params['sh']); 
            $_params['sx'] = Cataleya\Helper\Validator::float($_params['sx']); 
            $_params['sy'] = Cataleya\Helper\Validator::float($_params['sy']);
            
            
            if (
                    $_params['image_id'] === FALSE || 
                    $_params['rotate'] === FALSE || 
                    $_params['sw'] === FALSE || 
                    $_params['sh'] === FALSE || 
                    $_params['sx'] === FALSE || 
                    $_params['sy'] === FALSE )
                
                $_bad_params[] = 'image params';
    }
    

    $_params['auto_confirm'] = isset($_params['auto_confirm']) 
                                ? filter_var($_params['auto_confirm'], FILTER_VALIDATE_BOOLEAN) 
                                : FALSE;
    
    
    
    // Check if every one made it through
    if (!empty($_bad_params)) _catch_error('Bad params.', __LINE__, true);
    
    return $_params;
}





function juice (Cataleya\Admin\User $AdminAccount) 
{
    
    $_info = array ( 
        'id'    =>  $AdminAccount->getAdminId(), 
        'name'  =>  $AdminAccount->getName(), 
        'fname'  =>  $AdminAccount->getFirstname(), 
        'lname'  =>  $AdminAccount->getLastname(), 
        'email'  =>  $AdminAccount->getEmail(), 
        'telephone'  =>  $AdminAccount->getPhone(), 
        'DisplayPicture'    =>  $AdminAccount->getDisplayPicture()->getHrefs(),
        'isActive'   =>  $AdminAccount->isActive()
        
    );
    
    return $_info;
    
    
}





/*
 * 
 * 
 * CALLBACKS
 * 
 */


function void () {
    
}








/*
 * 
 * [ saveProfile ]
 * ______________________________________________________
 * 
 * @param: int $_POST['target_id']
 * @param: int $_POST['data']
 * 
 */


function saveProfile () {
    // Confirm account update (and require password)
    $message = 'To continue please confirm your password...';
    confirm($message, TRUE);


    global $json_reply, $_MyAccount;
    
    $_params = digestParams();
                
    // [1] Profile 
    $_MyAccount
    ->setFirstname($_params['fname'])
    ->setLastname($_params['lname'])
    
    ->setEmail($_params['email'])
    ->setPhone($_params['phone']);
    
    // [2] Set password ??
    if ($_params['set_password'] === TRUE) $_MyAccount->setPassword($_params['password']);

    
    
    $json_reply['message'] = 'New profile saved.';
    $json_reply['Profile'] = juice($_MyAccount);

};






/*
 * 
 * [ saveDisplayPicture ]
 * ______________________________________________________
 * 
 * @param: int $_POST['target_id']
 * @param: int $_POST['data']
 * 
 */


function saveDisplayPicture () {

    global $json_reply, $_MyAccount;
    
    $_params = digestParams();
    

    $new_image = Cataleya\Asset\Image::load($_params['image_id']);
    if ((int)$new_image->getID() === 0) _catch_error('Image not found.' . $_params['image_id'], __LINE__, true);
    


    $options = array(
                                    'rotate'=>$_params['rotate'], 
                                    'sw'=>$_params['sw'], 
                                    'sh'=>$_params['sh'], 
                                    'sx'=>$_params['sx'], 
                                    'sy'=>$_params['sy']
                                    ); 
    
    if (!$new_image->bake($options, FALSE)) _catch_error('Error saving image.', __LINE__, true);
    $new_image->makeLarge(200);
    $new_image->makeThumb(120);
    $new_image->makeTiny(60);   
    
    
   // attach image
    $_MyAccount->setDisplayPicture($new_image);
    
    
    $json_reply['message'] = 'New profile saved.';
    $json_reply['Profile'] = juice($_MyAccount);

};







/*
 * 
 * [ getInfo ]
 * ______________________________________________________
 * 
 * @param: int $_POST['target_id']
 * 
 * 
 */


function getInfo () {

    global $json_reply, $_MyAccount;

    
    $json_reply['message'] = 'New profile saved.';
    $json_reply['Profile'] = juice($_MyAccount);

};






// Remove token...
if (isset($_SESSION[SESSION_PREFIX.'CONFIRM_TOKEN'])) unset ($_SESSION[SESSION_PREFIX.'CONFIRM_TOKEN']);




 echo (json_encode($json_reply));
 exit();  

?>