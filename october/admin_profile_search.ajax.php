<?php
/*
FETCH CATEGORIES AND RETURN AS JSON...
*/


define ('OUTPUT', 'JSON'); // Whether to output as JSON or HTML (especially errors)...


// LOAD APPLICATION TOP...
require_once ('app_top.php');

/*
 * 
 * CHECK USER PRIVILEGES
 * 
 */


if ((!isset($_admin_role) || !$_admin_role->hasPrivilege('VIEW_ADMIN_PROFILES')) && !IS_SUPER_USER) forbidden();



$WHITE_LIST = array(
						'auth_token', 
						'term'
					);



// RETRIEVE DATA ONLY IF VIA POST METHOD...
if ($_SERVER['REQUEST_METHOD'] != 'POST') _catch_error('POST METHOD ONLY PLEASE. NOW GET.', __LINE__, true);
	



// SANITIZE INPUT DATA...
$_CLEAN = filter_input_array(INPUT_POST, 
								array(
										'auth_token'		=>	FILTER_SANITIZE_STRIPPED, 
										'term'		=>	FILTER_SANITIZE_STRING

										)
								);




// Check if INPUT keys are ALL expected AND MADE IT THROUGH...
if (anySuspects()) 
{


    // Output...
    $json_data = array (
                    'status' => 'Ok', 
                    'message' => 'No admin profiles found.', 
                    'AdminProfiles' => array(), 
                    'count' => 0
                    );
    echo json_encode($json_data);
    exit();

    
}







// First, take out funny looking characters...
$_CLEAN['term'] = preg_replace('/[^-\s\.A-Za-z0-9@*#+]+/', '', $_CLEAN['term']);




// Load search results as a collection [ _Customer ] objects...
$search_results = Cataleya\Admin\Users::search($_CLEAN['term']);



$admin_profiles_info = array ();

foreach ($search_results as $AdminUser) 
{
    
    $Role = $AdminUser->getRole();
    // if ($Role->hasPrivilege('SUPER_USER')) continue;
    
    $admin_role_info = array (
        'id'    =>   $Role->getRoleId(), 
        'name'  =>   $Role->getDescription()->getTitle('EN'), 
        'isAdmin'   =>  ($Role->hasPrivilege('SUPER_USER')) ? TRUE : FALSE
    );
    
    $admin_profiles_info[] = array (
        'id'    =>  $AdminUser->getAdminId(), 
        'name'  =>  $AdminUser->getName(), 
        'firstname'  =>  $AdminUser->getFirstname(), 
        'lastname'  =>  $AdminUser->getLastname(), 
        'email'  =>  $AdminUser->getEmail(), 
        'phone'  =>  $AdminUser->getPhone(), 
        'isActive'   =>  $AdminUser->isActive(), 
        'isOnline'   =>  $AdminUser->isOnline(), 
        'url'   =>  BASE_URL.'profile.admin.php?id='.$AdminUser->getAdminId(),
        'role'  =>  $admin_role_info
    );
}



// Output...
$json_data = array (
		'status' => 'Ok', 
		'message' => (count($admin_profiles_info)) . ' admin profiles found.', 
		'AdminProfiles' => $admin_profiles_info, 
		'count' => count($admin_profiles_info)
		);
echo json_encode($json_data);
exit();




?>