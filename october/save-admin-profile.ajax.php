<?php



define ('OUTPUT', 'JSON'); // Whether to output as JSON or HTML (especially errors)...


// LOAD APPLICATION TOP...
require_once ('app_top.php');


use \Cataleya\Error;

/*
 * 
 * TO PERFORM THIS OPERATION, USER MUST BE AN ADMINISTRATOR
 * 
 */


if (!$_admin_role->hasPrivilege('EDIT_ADMIN_PROFILES') && !$_admin_role->is('ROOT')) _catch_error('You do not have permission to perform this action.', __LINE__, TRUE, TRUE);



$WHITE_LIST = array(
                                                // EXPECTED KEYS
    
						'auth-token', 
						'admin-id', 
                                                'role-id', 
						'fname', 
						'lname',
						'email', 
						'password', 
						'password-confirm',  
						'phone',
                                                'enabled', 
                                                'change-password', 
    
                                                // EXPECTECTED VALUE DATA TYPES (only necessary for boolean values which can confuse the func.
                                                // Note: Make values same 'data type' as expected value.
                                                
                                                'enabled'   =>  TRUE
					);




// SANITIZE GET DATA...
$_CLEAN = filter_input_array(INPUT_POST, 
								array(
										'auth-token'	=>	FILTER_SANITIZE_STRIPPED, 
                                                                                'admin-id'	=>	FILTER_VALIDATE_INT, 
                                                                                'role-id'	=>	FILTER_VALIDATE_INT, 
                                                                                'enabled'	=>	array('filter'		=>	FILTER_VALIDATE_BOOLEAN, 
														'flags' => FILTER_NULL_ON_FAILURE
																), 
                                                                                'change-password'	=>	array('filter'		=>	FILTER_VALIDATE_BOOLEAN, 
														'flags' => FILTER_NULL_ON_FAILURE
																), 
										'fname'	=>	array('filter'		=>	FILTER_VALIDATE_REGEXP, 
														'options'	=>	array('regexp' => '/[-\s\.A-Za-z0-9]{0,60}/')
																), 
										'lname'	=>	array('filter'		=>	FILTER_VALIDATE_REGEXP, 
														'options'	=>	array('regexp' => '/[-\s\.A-Za-z0-9]{0,60}/')
																), 
										'email'	=>	FILTER_VALIDATE_EMAIL, 
										'password'	=>	array(
                                                                                                                'filter'		=>	FILTER_VALIDATE_REGEXP, 
                                                                                                                'options'	=>	array('regexp' => '/[-_\W\s\.A-Za-z0-9]{0,60}/')
                                                                                                                ), 
                                                                                'password-confirm'	=>	array(
                                                                                                                'filter'		=>	FILTER_VALIDATE_REGEXP, 
                                                                                                                'options'	=>	array('regexp' => '/[-_\W\s\.A-Za-z0-9]{0,60}/')
                                                                                                                ), 
										'phone'	=>	array(
                                                                                                                'filter'		=>	FILTER_VALIDATE_REGEXP, 
                                                                                                                'options'	=>	array('regexp' => '/^[+]?[0-9]{0,20}$/')
                                                                                                                )

										)
								);



// This is a check box which won't be included if 'unchecked'
if (isset($_CLEAN['change-password']) && $_CLEAN['change-password'] !== NULL && (int)$_CLEAN['change-password'] === 1) $_CLEAN['change-password'] = 1;
else $_CLEAN['change-password'] = 0;

// Password boxes aren't included when disabled
if ($_CLEAN['password'] === NULL) $_CLEAN['password'] = '';
if ($_CLEAN['password-confirm'] === NULL) $_CLEAN['password-confirm'] = '';


// Password boxes aren't included when disabled
if ($_CLEAN['email'] === FALSE) $_CLEAN['email'] = '';



// Check if INPUT keys are ALL expected AND MADE IT THROUGH...
$_affected = anySuspects();
if ($_affected !== FALSE) _catch_error('Error processing white_list!' . $_affected, __LINE__, true);



// VALIDATE FORM DATA

// Error / Response
$_errors = array ();

// 1. First Name

if (!isset($_CLEAN['fname']) || $_CLEAN['fname'] === FALSE || $_CLEAN['fname'] == '' )
{
    $_errors['fname'] = 'Please enter first name (Make sure to only use the characters: A-Z, a-z, 0-9, dashes and dots)';
}


// 2. Last Name

if (!isset($_CLEAN['lname']) || $_CLEAN['lname'] === FALSE || $_CLEAN['lname'] == '' )
{
    $_errors['lname'] = 'Please enter last name (Make sure to only use the characters: A-Z, a-z, 0-9, dashes and dots)';
}



// 3. Email

if (!isset($_CLEAN['email']) || $_CLEAN['email'] === FALSE || $_CLEAN['email'] == '' )
{
    $_errors['email'] = 'Please enter a valid email address.';
} else {
    $_CLEAN['email'] = trim($_CLEAN['email'], ' ');
}




if (isset($_CLEAN['change-password']) && (int)$_CLEAN['change-password'] === 1):

// 4. Password

if (!isset($_CLEAN['password']) || $_CLEAN['password'] === FALSE || $_CLEAN['password'] == '' )
{
    $_errors['password'] = 'Please enter a password that is at least 8 characters long, and contains 1 uppercase letter and a number. <br/>- OR -<br/> Use a pass-phrase instead. For example: "I love bvlgari".';
    $_errors['password-confirm'] = 'Remember to re-type your password (must be the same as the first).';
    
}

// 5. Confirm Password

else if (!isset($_CLEAN['password-confirm']) || $_CLEAN['password-confirm'] === FALSE || $_CLEAN['password-confirm'] !== $_CLEAN['password'] )
{
    $_errors['password-confirm'] = 'Please re-type your password (must be the same as the first).';
}


endif;


// 5. Telephone

// Skip



// 6. Admin Role
if ((int)$_CLEAN['role-id'] === 0)$_errors['role-id'] = 'Please choose an admin role.';
else if ($_CLEAN['role-id'] === FALSE) _catch_error('Bad params.', __LINE__, true);



// CHECK VALIDATION RESULTS

if (!empty($_errors)) 
{

    // Output...
    $json_data = array (
                    'status' => 'Ok', 
                    'message' => 'Account info not saved!',
                    'failedValidation' => TRUE, 
                    'failedItems'  => array_keys($_errors) 
                    );
    echo json_encode($json_data);
    exit();
    
}





$_Role = Cataleya\Admin\Role::load($_CLEAN['role-id']);
if ($_Role === NULL) _catch_error('Admin role could not be loaded.', __LINE__, true);



// Confirm account update (and require password)
$message = 'To continue please confirm your password...';
confirm($message, TRUE);




// NEW ACCOUNT
if ((int)$_CLEAN['admin-id'] === 0)
{
    if (
        $_Role->is('ROOT') || 
        (!$_admin_role->hasPrivilege('CREATE_ADMIN_PROFILES') && !$_admin_role->is('ROOT'))
    ) throw new Error ('You do not have the required authorization to create admin accounts. Please contact your IT admin.', Error::CODE_PERMISSION_DENIED);
    
    $options = array (
                        'firstname' => $_CLEAN['fname'], 
                        'lastname' => $_CLEAN['lname'], 
                        'admin_phone' => $_CLEAN['phone']
                        );

    $AdminAccount = Cataleya\Admin\User::create($_CLEAN['email'], $_CLEAN['password'], $_Role, $options);
    if ($AdminAccount === NULL) _catch_error('Account could not be created.', __LINE__, true);
    
    // SET ACCOUNT STATUS
    if ($_CLEAN['enabled']) $AdminAccount->enable ();
    else $AdminAccount->disable();
	
}

// EXISTING ACCOUNT
else if ($_CLEAN['admin-id'] !== FALSE) 
    {
    $AdminAccount = Cataleya\Admin\User::load($_CLEAN['admin-id']);
    if ($AdminAccount === NULL) _catch_error('Account could not be load.', __LINE__, true);
    

    if (
        ($AdminAccount->is('ROOT') || 
        !$_admin_role->hasPrivilege('EDIT_ADMIN_PROFILES')) 
    ) throw new Error ('You do not have the required authorization to modify admin accounts. Please contact your IT admin.', Error::CODE_PERMISSION_DENIED);
    
    
    $AdminAccount
    ->setFirstname($_CLEAN['fname'])
    ->setLastname($_CLEAN['lname'])
    
    ->setPhone($_CLEAN['phone'])
    ->setEmail($_CLEAN['email']);
    
    if (isset($_CLEAN['change-password']) && $_CLEAN['change-password']) $AdminAccount->setPassword($_CLEAN['password']);
    
    // SET ACCOUNT STATUS
    if ($_CLEAN['enabled']) $AdminAccount->enable ();
    else $AdminAccount->disable();
}

else {
    _catch_error('Bad params.', __LINE__, true);
}



$admin_role_info = array (
    'id'    =>   $_Role->getRoleId(), 
    'handle' => $_Role->getHandle(), 
    'name'  =>   $_Role->getDescription()->getTitle('EN'), 
    'isAdmin'   =>  $_Role->is('ROOT')
);

// Retrieve account info...
$account_info = array (
    'id'    =>  $AdminAccount->getAdminId(), 
    'name'  =>  $AdminAccount->getName(), 
    'fname'  =>  $AdminAccount->getFirstname(), 
    'lname'  =>  $AdminAccount->getLastname(), 
    'email'  =>  $AdminAccount->getEmail(), 
    'telephone'  =>  $AdminAccount->getPhone(), 
    'isActive'   =>  $AdminAccount->isActive(), 
    'isOnline'   =>  $AdminAccount->isOnline(), 
    'role'  =>  $admin_role_info
);


// Output...
$json_data = array (
                'status' => 'Ok', 
                'message' => 'Voila! Account info saved!', 
                'AdminProfile' => $account_info, 
                'failedValidation' => FALSE, 
                'failedItems'  => array() 
                );
echo json_encode($json_data);
exit();
		
?>
		
?>
