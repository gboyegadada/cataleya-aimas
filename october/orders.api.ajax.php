<?php



define ('OUTPUT', 'JSON'); // Whether to output as JSON or HTML (especially errors)...


// LOAD APPLICATION TOP...
require_once ('app_top.php');




$WHITE_LIST = array(
						'auth_token',  
                                                'target_id',  
                                                'order_id', 
                                                'do', 
                                                'data'
						);





// DIE IF INPUT IS NOT VIA POST METHOD...
if ($_SERVER['REQUEST_METHOD'] != 'POST') _catch_error('POST METHOD ONLY PLEASE. NOW GET.', __LINE__, true);

// SANITIZE INPUT DATA...
$_CLEAN = filter_input_array(INPUT_POST, array(
                                                    'auth_token'		=>	FILTER_SANITIZE_STRIPPED,
                                                    'order_id'	=>	array( 'filter'=> FILTER_VALIDATE_INT, 'flags' => FILTER_REQUIRE_ARRAY ), 
                                                    'target_id'	=>	FILTER_VALIDATE_INT, 
                                                    'do'	=>	array('filter'		=>	FILTER_VALIDATE_REGEXP, 
                                                                                                'options'	=>	array('regexp' => '/^[A-Za-z ]{1,50}$/')
                                                                                                                ), 
                                                    'data'	=>	array( 'filter'=> FILTER_SANITIZE_STRIPPED, 'flags' => FILTER_REQUIRE_ARRAY ), 

                                                    )
										
					);


// Check if INPUT keys are ALL expected AND MADE IT THROUGH...
$_suspect = anySuspects();
if ($_suspect !== FALSE) _catch_error('Error processing white_list!' . $_suspect, __LINE__, true);




// VALIDATE AUTH TOKEN...
validate_auth_token ();

// Validate task

$_FUNCS = array (
    'void', 
    'sendEmail', 
    
    'saveOrder', 
    'getOrder', 
    'setOrderStatus', 
    'deleteOrder'
    
);





if (!in_array($_CLEAN['do'], $_FUNCS) || !is_callable($_CLEAN['do'])) _catch_error('Invalid action.', __LINE__, true);

// Confirm if we're deleting a item...
if ($_CLEAN['do'] === 'deleteOrder') 
{
    $message = 'Are you sure you want to delete the selected items (' . count($_CLEAN['order_id']) . ')?';
    confirm($message, FALSE);
}




    
// OUTPUT...
 $json_reply = array (
                 "status" => 'ok', 
                 "message" => 'Ok'
                 );
 
 

$_params = digestParams();


// EXECUTE API CALL
foreach ($_CLEAN['order_id'] as $_id) 
{
    if ($_CLEAN['do'] !== 'newOrder') 
    {
        // Load product
        $Order = Cataleya\Store\Order::load((int)$_id);

        // Check if product exists...
        if ($Order === NULL) _catch_error('One or more orders not found.', __LINE__, true);
    
    }

    // API FUNC CALL
    $_CLEAN['do']();
}




// HELPER FUNCS



function digestParams () 
{
    global $_CLEAN;
    $_params = $_CLEAN['data'];
    $_bad_params = array ();
    
    
    if ($_CLEAN['do'] === 'setOrderStatus') 
    {

        $_params['order_status'] = Cataleya\Helper\Validator::text($_params['order_status'], 3, 20);
        if (empty($_params['order_status'])) $_bad_params[] = 'order_status';
        
        
    }
    
    if (in_array($_CLEAN['do'], array('setOrderStatus', 'deleteOrder'))) 
    {

        $_params['active_store'] = Cataleya\Helper\Validator::int($_params['active_store'], 0, 10000);
        if ($_params['active_store'] === FALSE) $_bad_params[] = 'active_store';
    }
    

    // Check if every one made it through
    if (!empty($_bad_params)) validationFailed ($_bad_params);
    
    return $_params;
}






function juiceOrder (Cataleya\Store\Order $_Order) 
{

    $_Store = $_Order->getStore();
    $_Currency = $_Order->getCurrency();
    $_Customer = $_Order->getCustomer();
    $_Recipient = $_Order->getRecipient();
    $_Location = $_Recipient->getLocation();
    
    $_Transaction = NULL;
    
    $_TranxInfo = array (
        'TranxID' => '#XXXX', 
        'PaymentGateway' => 'Unknown', 
        'AmountPaid' => 0, 
        'PaymentStatus' => 'Unknown'
    );


    foreach (Cataleya\Payment\Transactions::load($_Order) as $_Tranx) 
    {
        $_Transaction = $_Tranx;
        break;
    }

    if (!empty($_Transaction)) 
    {   
        $_TranxInfo['TranxID'] = '#'.str_pad(strval($_Transaction->getID()), 5, '0', STR_PAD_LEFT);
        $_TranxInfo['PaymentGateway'] = $_Transaction->getPaymentType()->getName();
        $_TranxInfo['AmountPaid'] = number_format($_Transaction->getAmount(), 2) . ' ' . $_Order->getCurrencyCode();
        $_TranxInfo['PaymentStatus'] = $_Transaction->getStatus();
    }

    $_Shipping = array (
        'Street' => $_Location->getEntryStreetAddress() . ', ', 
        'Suburb'  =>  $_Location->getEntrySuburb(), 
        'City'    =>  $_Location->getEntryCity(), 
        'Postcode'    =>  $_Location->getEntryPostcode(), 
        'Province'    =>  $_Location->getEntryProvince() . ', ', 
        'Country' =>  $_Location->getEntryCountry()
    );

    $_Shipping['Suburb'] .= ((empty($_Shipping['Suburb'])) ? '' : ', ');
    $_Shipping['City'] .= ((empty($_Shipping['City'])) ? '' : ', ');
    $_Shipping['Postcode'] .= ((empty($_Shipping['Postcode'])) ? '' : ', ');


    
    $_info = array (
        'id'    =>   $_Order->getID(), 
        'orderNum'    =>   str_pad(strval($_Order->getID()), 5, '0', STR_PAD_LEFT), 
        'status'    =>   $_Order->getOrderStatus(), 
        'StatusFlag'    =>   array ($_Order->getOrderStatus() => TRUE), 
        'total' =>  number_format($_Order->getTotalCost(), 2), 
        'grandTotal'    =>  number_format($_Order->getGrandTotal(), 2), 
        'shippingCharges'   =>  number_format($_Order->getShippingCharges(), 2), 
        'quantity'  =>  $_Order->getQuantity(), 
        'population'    =>  $_Order->getPopulation(), 
        'orderDate' =>  $_Order->getOrderDate(FALSE)->format('h:ia, d M, Y'), 
        'Items' =>  array (), 
        'Discounts' =>  $_Order->getDiscounts(), 
        'Taxes' =>  $_Order->getTaxes(), 
        
        'Customer' => array (
            'id'  =>  $_Customer->getCustomerId(), 
            'name'  =>  $_Customer->getName(), 
            'firstname'  =>  $_Customer->getFirstname(), 
            'lastname'  =>  $_Customer->getLastname(), 
            'phone'  =>  $_Customer->getTelephone(), 
            'email'  =>  $_Customer->getEmailAddress()
        ), 
        'Recipient' => array (
            'name'  =>  ($_Recipient->getEntryFirstname() . ' ' . $_Recipient->getEntryLastname()), 
            'firstname'  =>  $_Recipient->getEntryFirstname(), 
            'lastname'  =>  $_Recipient->getEntryLastname()
        ),
        'Currency'  =>  array (
            'name'  =>  $_Currency->getCurrencyName(), 
            'code'  =>  $_Currency->getCurrencyCode(), 
            'utf'  =>  $_Currency->getUtfCode(), 
            'symbol' =>  ('&#'.$_Currency->getUtfCode().';')
        ), 
        'Store' =>  array (
            'id'    =>  $_Store->getStoreId(),
            'title' =>  $_Store->getDescription()->getTitle('EN'), 
            'description'   =>  $_Store->getDescription()->getText('EN')
        ), 
        'Shipping'    =>  array (
            'amount'  =>  $_Order->getShippingCharges(), 
            'amountBeforeTax'  =>  $_Order->getShippingCharges(FALSE, TRUE), 
            'description'  =>  $_Order->getShippingDescription(), 
            'Taxes' =>  $_Order->getShippingTaxes()
        ), 
        'ShippingAddress' => $_Shipping, 
        'Transaction' => $_TranxInfo
    );
    
    
    foreach ($_Order as $_OrderDetail) $_info['Items'][] = juiceOrderDetail($_OrderDetail);
    
    return $_info;
    
    
}







function juiceOrderDetail (Cataleya\Store\OrderDetail $_OrderDetail) 
{

    
    $_info = array (
        'id'    =>   $_OrderDetail->getID(), 
        'itemName'  =>  $_OrderDetail->getItemName(), 
        'itemDescription'   =>  $_OrderDetail->getItemDescription(), 
        'price' =>  number_format($_OrderDetail->getPrice(), 2), 
        'originalPrice' =>  number_format($_OrderDetail->getOriginalPrice(), 2), 
        'subtotal'  =>  number_format($_OrderDetail->getSubtotal(), 2), 
        'originalSubtotal'  =>  number_format($_OrderDetail->getSubtotal(FALSE), 2), 
        'quantity'  =>  $_OrderDetail->getQuantity(), 
        
        'couponCode'    =>  $_OrderDetail->getCouponCode(), 
        'couponDiscount'    =>  number_format($_OrderDetail->getCouponDiscountAmount(), 2), 
        
        'saleDescription'   =>  $_OrderDetail->getSaleDescription(), 
        'saleDiscount'  =>  number_format($_OrderDetail->getSaleDiscountAmount(), 2)
        
    );
    
    return $_info;
    
    
}









/*
 * 
 * 
 * CALLBACKS
 * 
 */


function void () {
    
}









function cancelOrder () {
    global $Order;

    $Order->setOrderStatus('cancelled');
}




function setOrderStatus () {
    global $Order, $_params, $json_reply;
    

    $Order->setOrderStatus($_params['order_status'])->saveData();
    
    $_FilterStore = ((int)$_params['active_store'] !== 0) ? Cataleya\Store::load($_params['active_store']) : NULL;
    
    $json_reply['Population'] = array (
        'pending' => Cataleya\Store\Orders::load($_FilterStore, NULL, array('status' => 'pending'))->getPopulation(), 
        'shipped' => Cataleya\Store\Orders::load($_FilterStore, NULL, array('status' => 'shipped'))->getPopulation(), 
        'delivered' => Cataleya\Store\Orders::load($_FilterStore, NULL, array('status' => 'delivered'))->getPopulation(), 
        'cancelled' => Cataleya\Store\Orders::load($_FilterStore, NULL, array('status' => 'cancelled'))->getPopulation()
    );
    
    
    $json_reply['OrderStatus'] = $Order->getOrderStatus();
}







function deleteOrder () {
    global $Order, $_params, $json_reply;


    Cataleya\Helper\DBH::getInstance()->beginTransaction();
    $Order->delete();
    Cataleya\Helper\DBH::getInstance()->commit();    
    
    
    $_FilterStore = ((int)$_params['active_store'] !== 0) ? Cataleya\Store::load($_params['active_store']) : NULL;
    
    $json_reply['Population'] = array (
        'all' => Cataleya\Store\Orders::load($_FilterStore, NULL, array('status' => 'all'))->getPopulation(), 
        'pending' => Cataleya\Store\Orders::load($_FilterStore, NULL, array('status' => 'pending'))->getPopulation(), 
        'shipped' => Cataleya\Store\Orders::load($_FilterStore, NULL, array('status' => 'shipped'))->getPopulation(), 
        'delivered' => Cataleya\Store\Orders::load($_FilterStore, NULL, array('status' => 'delivered'))->getPopulation(), 
        'cancelled' => Cataleya\Store\Orders::load($_FilterStore, NULL, array('status' => 'cancelled'))->getPopulation()
    );
}

    








// Remove token...
if (isset($_SESSION[SESSION_PREFIX.'CONFIRM_TOKEN'])) unset ($_SESSION[SESSION_PREFIX.'CONFIRM_TOKEN']);




 echo (json_encode($json_reply));
 exit();  

?>