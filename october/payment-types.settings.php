<?php



define ('OUTPUT', 'HTML'); // Whether to output as JSON or HTML (especially errors)...


// LOAD APPLICATION TOP...
require_once ('app_top.php');



/*
 * 
 * CHECK USER PRIVILEGES
 * 
 */


if ((!isset($_admin_role) || !$_admin_role->hasPrivilege('VIEW_PAYMENT_TYPES')) && !IS_SUPER_USER)  include_once INC_PATH.'forbidden.php';


define('READONLY', (!$_admin_role->hasPrivilege('EDIT_PAYMENT_TYPES') && !IS_SUPER_USER) ? FALSE : TRUE);




// SANITIZE GET DATA...
$_CLEAN = filter_input_array(INPUT_GET, 
								array(	
										 // Page number...
										'pg'	=>	array('filter'		=>	FILTER_VALIDATE_INT, 
																'options'	=>	array('min_range' => 1, 'max_range' => 1000)
																), 
                                                                                // Page size...
										'pgsize'	=>	array('filter'		=>	FILTER_VALIDATE_INT, 
																'options'	=>	array('min_range' => 1, 'max_range' => 1000)
																),                                                                     
										
										 // order 						
										'o'	=>	array('filter'		=>	FILTER_VALIDATE_INT, 
																'options'	=>	array('min_range' => 0, 'max_range' => 100)
																), 
                                                                                // order by 						
										'ob'	=>	array('filter'		=>	FILTER_VALIDATE_INT, 
																'options'	=>	array('min_range' => 0, 'max_range' => 100)
																),    
                                                                                // index 						
										'i'	=>	array('filter'		=>	FILTER_VALIDATE_REGEXP, 
														'options'	=>	array('regexp' => '/^[A-Za-z]{1}$/')
																), 

										)
								);


// Deafault page size...
define ('DEFAULT_PAGE_SIZE', 50);


// Check if INPUT keys are ALL expected AND MADE IT THROUGH...

$_CLEAN['pg'] = empty($_CLEAN['pg']) ? 1 : (int)$_CLEAN['pg'];
$_CLEAN['i'] = empty($_CLEAN['i']) ? 'all' : $_CLEAN['i'];
$_CLEAN['pgsize'] = empty($_CLEAN['pgsize']) ? DEFAULT_PAGE_SIZE : (int)$_CLEAN['pgsize'];
$_CLEAN['o'] = empty($_CLEAN['o']) ? Cataleya\Geo\Countries::ORDER_ASC : $_CLEAN['o'];
$_CLEAN['ob'] = empty($_CLEAN['ob']) ? Cataleya\Geo\Countries::ORDER_BY_NAME : $_CLEAN['ob'];

define ('NO_OF_PAGE_INDEX_ANCHORS', 3);

define('ORDER_BY', $_CLEAN['ob']);
define('ORDER', $_CLEAN['o']);
define('INDEX', $_CLEAN['i']);
define('PAGE_SIZE', $_CLEAN['pgsize']);


$paymentTypes = Cataleya\Payment\PaymentTypes::load(array('order_by'=>ORDER_BY, 'order'=>ORDER, 'index'=>INDEX), PAGE_SIZE, $_CLEAN['pg'])




////////////////////// PAGINATION /////////////





?>




<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Shop Admin | Browse Payment Types (<?= Cataleya\Helper::countInEnglish($paymentTypes->getPopulation()) ?>)</title>


<!-- JS FILES -->
<?php
require_once(INC_PATH.'js_libs.php');
?>
<script type="text/javascript" src="ui/jscript/payment-types.js"></script>


<!-- STYLE SHEETS -->
<?php
require_once(INC_PATH.'css_libs.php');
?>

<link href="ui/css/indexed-paginated-list.css" rel="stylesheet" type="text/css" />
<link href="ui/css/bootmetro-tiles.css" rel="stylesheet" type="text/css" />
<link href="ui/css/payment-types.css" rel="stylesheet" type="text/css" />


<style type="text/css" media="all">
.content {
    height:<?= (ceil(((PAGE_SIZE > $paymentTypes->getPopulation()) ? $paymentTypes->getPopulation() : PAGE_SIZE)/3)*100)+450 ?>px!important;
	
}




</style>


<script type="text/javascript">


</script>


</head>

<body>

<!-- BEGIN: META -->
<input type="hidden" id="meta_token" value="<?=AUTH_TOKEN?>"  />
<!-- END: META -->




<div class="container">
<?php include_once INC_PATH.'photo-upload-forms.php'; ?>
    
    
<div class="header-wrapper absolute">
    <div class="header">

    <a href="<?=BASE_URL?>#settings"><div id="page-title">Payment Types</div></a><?php include_once INC_PATH.'page.nav.php'; ?> 

    </div>
    
    
<?php
        include_once INC_PATH.'main.nav.php';
?>
    
</div>


  <div class="content">





<div id="page-options-panel">
            
    <div id="page-description-panel">
        To change the display name of a payment option (this is what the shopper will see at the store) or disable a payment option just click it.
    </div>
    
</div>





<!-- Payment Types -->

<ul id="list-table">
    
    

    <li>
        <div id="payment-types-grid-wrapper" class="listview-container grid-layout">
            
<?php

$_payment_type_list = \Cataleya\Plugins\Package::getAppList('payment');
foreach ($_payment_type_list as $_handle) {
    $_name = \Cataleya\Plugins\Package::getAppName($_handle);
    $_on = \Cataleya\Plugins\Package::isOn($_handle);

?>
            <a href="javascript:void(0)" class="payment-type-tile"  id="payment-type-<?=$_handle?>" onclick="PaymentTypes.editPaymentType(<?=$_handle?>); return false;">
            <div class="mediumListIconTextItem">
                <span class="icon-globe tile-icon-large" ></span>
                <div class="mediumListIconTextItem-Detail">
                    <h4><?=$_name?></h4>
                    <h6>
                    <?= ($_on) ? 'Enabled' : 'Disabled' ?>
                    </h6>
                 </div>
            </div>
            </a>

<?php

}
?>
            
            
            
        </div>
        
        
    </li>



</ul>







<?php
    include_once INC_PATH.'edit-payment-type-dialog-box.php';

?>




<div id="footer">
&nbsp;
</div>



</div>

</div>


</body>
</html>
