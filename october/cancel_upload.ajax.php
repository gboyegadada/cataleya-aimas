<?php



define ('OUTPUT', 'JSON'); // Whether to output as JSON or HTML (especially errors)...


// LOAD APPLICATION TOP...
require_once ('app_top.php');



$WHITE_LIST = array(
						'auth_token', 
						'image_id' 
					);


/*

VERY VERY VERY VERY VER IMPORTANT NOTICE !!!!!!

BEFORE DEPLOYMENT MAKE SURE URL IS ONLY STORE IN SESSION DATA AND ASSIGN TO AN ID OF SORT
THEN IF THERE'S A NEED TO CANCEL THE UPLOAD, THE ID IS SENT BACK NOT AN ACTUAL PATH.




*/




// DIE IF INPUT IS NOT VIA POST METHOD...
if ($_SERVER['REQUEST_METHOD'] != 'POST') _catch_error('POST METHOD ONLY PLEASE. NOW GET.');


// SANITIZE INPUT DATA...
$_CLEAN = filter_input_array(INPUT_POST, 
								array(
										'auth_token'		=>	FILTER_SANITIZE_STRIPPED, 
										'image_id'	=>	FILTER_VALIDATE_INT

										)
										
								);


// Check if INPUT keys are ALL expected AND MADE IT THROUGH...
if (anySuspects()) _catch_error('Error processing white_list!', __LINE__, true);


$data = array();




if (isset($_CLEAN['image_id'])) {
    
    $image = Cataleya\Asset\Image::load($_CLEAN['image_id']);
    if ($image != NULL) $image->destroy ();

    $data = array (
                    'status'=>'ok', 
                    'message'=>'upload canceled'
                    );

    echo json_encode($data);
    exit();

} else {
	
_catch_error('Image not specified.', __LINE__, true);
			
}






?>
