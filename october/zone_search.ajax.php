<?php
/*
FETCH CATEGORIES AND RETURN AS JSON...
*/


define ('OUTPUT', 'JSON'); // Whether to output as JSON or HTML (especially errors)...


// LOAD APPLICATION TOP...
require_once ('app_top.php');



/*
 * 
 * TO PERFORM THIS OPERATION, USER MUST BE AN ADMINISTRATOR
 * 
 */


if ((!isset($_admin_role) || !$_admin_role->hasPrivilege('VIEW_ZONES')) && !$adminUser->is('STORE_OWNER')  && !IS_SUPER_USER) _catch_error('You do have permission to perform this action.', __LINE__, true);




$WHITE_LIST = array(
						'auth_token', 
						'term'
					);



// RETRIEVE DATA ONLY IF VIA POST METHOD...
if ($_SERVER['REQUEST_METHOD'] != 'GET') _catch_error('GETT METHOD ONLY.', __LINE__, true);
	



// SANITIZE INPUT DATA...
$_CLEAN = filter_input_array(INPUT_POST, 
								array(
										'auth_token'		=>	FILTER_SANITIZE_STRIPPED, 
										'term'		=>	FILTER_SANITIZE_STRING

										)
								);




// Check if INPUT keys are ALL expected AND MADE IT THROUGH...
if (anySuspects()) 
{
    // _catch_error('Error processing white_list!', __LINE__, true);
    
    // bail quietly

    // Output...
    $json_data = array (
                    'status' => 'Ok', 
                    'message' => 'No zones found.', 
                    'zones' => array(), 
                    'count' => 0
                    );
    echo json_encode($json_data);
    exit();

    
}







// First, take out funny looking characters...
$_CLEAN['term'] = preg_replace('/[^-\s\.A-Za-z0-9@*#+]+/', '', $_CLEAN['term']);




// Load search results as a collection [ _Zone ] objects...
$search_results = Cataleya\Geo\Zones::search($_CLEAN['term']);

$zones = array();

foreach ($search_results as $zone) {
        $_zone_id = $zone->getZoneId();
	$zones[$_zone_id] = new stdClass();

        $zones[$_zone_id]->name = $zone->getZoneName();
        $zones[$_zone_id]->id = $_zone_id;
        $zones[$_zone_id]->type = $zone->getListType();
        $zones[$_zone_id]->countryPopulation = $zone->getCountryPopulation();
        $zones[$_zone_id]->provincePopulation = $zone->getProvincePopulation();
			
	$zones[$_zone_id]->url = BASE_URL.'zone.zones.php?id=' . $_zone_id;
}



// Output...
$json_data = array (
		'status' => 'Ok', 
		'message' => (($search_results->getPopulation() < 1) ? 'No': $search_results->getPopulation()) . ' zones found.', 
		'zones' => $zones, 
		'count' => $search_results->getPopulation()
		);
echo json_encode($json_data);
exit();




?>
