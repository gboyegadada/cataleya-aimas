<?php
/*
FETCH PAYMENT TYPES AND RETURN AS JSON...
*/




define ('OUTPUT', 'JSON'); // Whether to output as JSON or HTML (especially errors)...


// LOAD APPLICATION TOP...
require_once ('app_top.php');






// DIE IF INPUT IS NOT VIA POST METHOD...
if ($_SERVER['REQUEST_METHOD'] != 'POST') _catch_error('POST METHOD ONLY PLEASE. NOW GET.');
	



// FILTER VALUES ....
$_CLEAN = filter_input_array(INPUT_POST, 
								array(	
                                                                                // auth_token
                                                                                'auth_token'    =>  FILTER_SANITIZE_STRIPPED

										)
								);


$AdminProfiles = Cataleya\Admin\Users::load();
if ($AdminProfiles === NULL) _catch_error('Admin profiles could not be loaded.', __LINE__, true);


$admin_profiles_info = array ();

foreach ($AdminProfiles as $AdminUser) 
{
    
    $Role = $AdminUser->getRole();
    // if ($Role->hasPrivilege('SUPER_USER')) continue;
    
    $admin_role_info = array (
        'id'    =>   $Role->getRoleId(), 
        'name'  =>   $Role->getDescription()->getTitle('EN'), 
        'isAdmin'   =>  ($Role->hasPrivilege('SUPER_USER')) ? TRUE : FALSE
    );
    
    $admin_profiles_info[] = array (
        'id'    =>  $AdminUser->getAdminId(), 
        'name'  =>  $AdminUser->getName(), 
        'firstname'  =>  $AdminUser->getFirstname(), 
        'lastname'  =>  $AdminUser->getLastname(), 
        'email'  =>  $AdminUser->getEmail(), 
        'phone'  =>  $AdminUser->getPhone(), 
        'isActive'   =>  $AdminUser->isActive(), 
        'role'  =>  $admin_role_info
    );
}

// OUTPUT...
$json_reply = array (
		"status"=>'ok', 
		"message"=>Cataleya\Helper::countInEnglish($AdminProfiles->getPopulation(), 'admin profile', 'admin profiles') . ' found.', 
                "AdminProfiles"=>$admin_profiles_info, 
                "count"=>$AdminProfiles->getPopulation()
		);

echo (json_encode($json_reply));
exit();



?>