<?php



define ('OUTPUT', 'HTML'); // Whether to output as JSON or HTML (especially errors)...


// LOAD APPLICATION TOP...
require_once ('app_top.php');







// Logout user out
if (isset($_SESSION[SESSION_PREFIX.'LOGGED_IN']) && $_SESSION[SESSION_PREFIX.'LOGGED_IN'] !== FALSE)
{
        $adminUser = Cataleya\Admin\User::load($_SESSION[SESSION_PREFIX.'ADMIN_ID']);
        if ($adminUser !== NULL)
        {

            // Logout
            $adminUser->logout();
            Cataleya\Session::kill_session(SESSION_NAME);

        }

}




// REMOVE STORE KEY (for access to store front even when closed)
setcookie(
    '_a', // Name
    '', // Value
    time()-3600, // Expires
    '/', // Path
    ('.'.DOMAIN), // Domain
    FALSE, // Is secure
    TRUE // HTTP Only
    );




// Redirect browser...
redirect(BASE_URL, FALSE);

// A little house cleaning...
Cataleya\Asset\Image::gc();


exit('<center><p>One moment please...</p></center>');

