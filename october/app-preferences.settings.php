<?php



define ('OUTPUT', 'HTML'); // Whether to output as JSON or HTML (especially errors)...


// LOAD APPLICATION TOP...
require_once ('app_top.php');




/*
 * 
 * CHECK USER PRIVILEGES
 * 
 */


if ((!isset($_admin_role) || !$_admin_role->hasPrivilege('VIEW_APP_SETTINGS')) && !IS_SUPER_USER)    forbidden();


define('READONLY', ($_admin_role->hasPrivilege('EDIT_APP_SETTINGS') || IS_SUPER_USER) ? FALSE : TRUE);



$AppSettings = Cataleya\System::load();


?>




<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<?php
include "includes/meta.php";

?>

<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Shop Admin | Application Preferences</title>


<!-- JS FILES -->
<?php
require_once(INC_PATH.'js_libs.php');
?>

<script type="text/javascript" src="ui/jscript/app-preferences.js"></script>


<!-- STYLE SHEETS -->
<?php
require_once(INC_PATH.'css_libs.php');
?>
<link href="ui/css/bootmetro-tiles.css" rel="stylesheet" type="text/css" />
<link href="ui/css/app-preferences.css" rel="stylesheet" type="text/css" />


<style type="text/css" media="screen">



</style>


</head>

<body>
<!-- BEGIN: META -->
<input type="hidden" id="meta_token" value="<?=AUTH_TOKEN?>"  />
<input type="hidden" id="meta_admin_root" value="<?=BASE_URL?>"  />


<!-- END: META -->



<div class="container">
    
<!-- HEADER -->
<div class="header-wrapper absolute">
    <div class="header">

    <a href="<?=BASE_URL?>#settings"><div id="page-title">Application Preferences</div> </a>
    <?php include_once INC_PATH.'page.nav.php'; ?>   


    </div>
    

<div class="side-bar">
    
</div> 
    
    
<?php include_once INC_PATH.'main.nav.php'; ?>    
  
</div>


    
<div class="content">
  

    



<div id="search-tool-wrapper">
		
<?php
//define('QUICK_SEARCH_URL', BASE_URL.'store_profile_search.ajax.php');
//include INC_PATH.'quick-search-widget.php';
?>


</div>



<h2 id="profile-name-in-header" class="user-status <?= $AppSettings->isActive() ? 'online' : 'offline'?>"><?= $AppSettings->getAppName() . ' v' . $AppSettings->getAppVersion() ?></h2>





<div id="hor-line-1">&nbsp;</div>


<div id="page-options-panel">
            
            <?php if (!READONLY && ($_admin_role->hasPrivilege('CHANGE_APP_STATUS') || IS_SUPER_USER)): ?>
            <a id="status-toggle-button" class="button-1" href="javascript:void(0);"  onclick="toggleAppStatus(); return false;" >
                <?=$AppSettings->isActive() ? 'turn application off' : 'turn application on'?>
            </a>
            <?php endif; ?>

            
            
            <div id="page-description-panel">
                This zone includes all countries except the selected ones. This is suitable for when you're setting up shipping for deliveries outside 
                a specific country. For example, setting up shipping for deliveries everywhere except for the U.K.
            </div>
</div>



<ul class="side-panel">
    
    <li class="header">
    General Info
    </li>

    <li>
    Client: <span class="entry"><?= $AppSettings->getClient() ?></span>
    </li>

    <li>
    Develop: <span class="entry"><?= $AppSettings->getDeveloper() ?></span>
    </li>

    <li>
    Max Catalog Size: <span class="entry"><?= $AppSettings->getMaxCatalogSize() ?></span>
    </li>

    <li>
    Max Admin Accounts: <span class="entry"><?= $AppSettings->getMaxAdminAccounts() ?></span>
    </li>

    <li>
    Max Customer Acounts: <span class="entry"><?= $AppSettings->getMaxCustomerAccounts() ?></span>
    </li>

    <li>
    Last Turned Off: <span class="entry"><?= $AppSettings->getLastShutdown() ?></span class="entry">
    </li>


    <li>
    Last Turned On: <span  class="entry"><?= $AppSettings->getLastActivated() ?><span>
    </li> 

</ul>




<div id="app-prefs-form-outer-wrapper" >
    &nbsp;

    <ul id="app-prefs-form-wrapper">
     
        
        
        
       <li>
            
            <h3 class="section-header">Dashboard Theme</h3>
            
            <!--
            <div class="tiny-yellow-text">
                To make a payment option available for this store 'check' it. For example, if this store is for shoppers in Nigeria make sure 'WebPay' and 'eTranzact' are checked.
                
            </div>
            
            <br />
            <div class="dotted-yellow-hor-line tiny-yellow-arrow-down">&nbsp;</div>
            -->

            
            <br />
            <small class="text-input-label">Dashboard Theme</small>    
            <div id="theme-chooser-wrap">
                <select id="select-store-theme" name="dashboard-theme-id" tinyStatus="#store-theme-status"  onchange="saveControl(this)">
                <?php
                
                $_Themes = Cataleya\Front\Dashboard\Themes::load();
                
                foreach ($_Themes as $_Theme) 
                {
                ?>
                <option value="<?=$_Theme->getID()?>" <?=($_Theme->getID() === $AppSettings->getDashboardThemeID()) ? 'selected="selected"' : ''?> ><?=$_Theme->getName()?></option>
                <?php
                }
                
                
                
                ?>
            </select>
            <small class="tiny-status select-status" id="store-theme-status">&nbsp;</small>
                
            </div>
            
            
            <div style="height: 40px; width: 1px; clear: both;">&nbsp;</div> 
            
        </li>
        
        
        
        
        
        

        
       <li>
            
            <h3 class="section-header">Shop Front Theme</h3>

            
            <br />
            <small class="text-input-label">Default Shop Front Theme</small>    
            <div id="theme-chooser-wrap">
                <select id="select-store-theme" name="shopfront-theme-id" tinyStatus="#store-front-theme-status"  onchange="saveControl(this)">
                <?php
                
                $_Themes = Cataleya\Front\Shop\Themes::load();
                
                foreach ($_Themes as $_Theme) 
                {
                ?>
                    <option value="<?=$_Theme->getID()?>" <?=($_Theme->getID() === $AppSettings->getDefaultShopFrontThemeID()) ? 'selected="selected"' : ''?> ><?=$_Theme->getName()?></option>
                <?php
                }
                
                
                
                ?>
            </select>
            <small class="tiny-status select-status" id="store-front-theme-status">&nbsp;</small>
                
            </div>
            
            
            <div style="height: 40px; width: 1px; clear: both;">&nbsp;</div> 
            
        </li>
        
        
        
        
        
       <li>
            
            <h3 class="section-header">Supported Pricing Options</h3>
            
            <!--
            <div class="tiny-yellow-text">
                To make a payment option available for this store 'check' it. For example, if this store is for shoppers in Nigeria make sure 'WebPay' and 'eTranzact' are checked.
                
            </div>
            <br />
            
            
            <div class="dotted-yellow-hor-line tiny-yellow-arrow-down">&nbsp;</div>
            <br />

            
            -->

            <a href="javascript:void(0);" id="upc-code-anchor" tinyStatus="#upc-code-status" who="upc-code" class="payment-type-anchor <?=($AppSettings->upcEnabled()) ? 'icon-checkbox-partial' : 'icon-checkbox-unchecked'?>" onclick="toggle(this); return false;">Allow UPC Barcoding</a>
                <small class="tiny-status" id="upc-code-status">&nbsp;</small>
                <br/>
                
                
                <a href="javascript:void(0);" id="isbn-code-anchor" tinyStatus="#isbn-code-status" who="isbn-code" class="payment-type-anchor <?=($AppSettings->isbnEnabled()) ? 'icon-checkbox-partial' : 'icon-checkbox-unchecked'?>" onclick="toggle(this); return false;">Allow ISBN Code</a>
                <small class="tiny-status" id="isbn-code-status">&nbsp;</small>
                <br/>
                
                
                <a href="javascript:void(0);" id="sku-code-anchor" tinyStatus="#sku-code-status" who="sku-code" class="payment-type-anchor <?=($AppSettings->skuEnabled()) ? 'icon-checkbox-partial' : 'icon-checkbox-unchecked'?>" onclick="toggle(this); return false;">Allow SKU Code</a>
                <small class="tiny-status" id="sku-code-status">&nbsp;</small>
                <br/>
                
                <a href="javascript:void(0);" id="ean-code-anchor" tinyStatus="#ean-code-status" who="ean-code" class="payment-type-anchor <?=($AppSettings->eanEnabled()) ? 'icon-checkbox-partial' : 'icon-checkbox-unchecked'?>" onclick="toggle(this); return false;">Allow EAN Code</a>
                <small class="tiny-status" id="ean-code-status">&nbsp;</small>
                <br/>

            <div style="height: 40px; width: 1px; clear: both;">&nbsp;</div> 
            
        </li>
        

        
        
        <!--
       
        <?php if (!READONLY && ($_admin_role->hasPrivilege('CHANGE_APP_STATUS') || IS_SUPER_USER)): ?>        
        <li>
            <h3 class="section-header">Shut down</h3>
            <div class="tiny-yellow-text">
                WARNING: <br /><br />
                Deleting this store will remove ALL product pricing, stock information and shipping options you have setup. 
            </div>
            <br />
            <div class="dotted-yellow-hor-line tiny-yellow-arrow-down">&nbsp;</div>
            <br />
            <br/>
            <a class="button-1" href="javascript:void(0);" onClick="toggleStoreStatus(); return false;" >
                <?=$AppSettings->isActive() ? 'turn off' : 'turn off'?>
            </a>
            <br/>
            <br />
            
        </li>
        <?php endif; ?>
        
        -->
        
                
    </ul>



    
</div>
    


<?php
     include_once INC_PATH.'edit-shipping-option-dialog.php';

?>



</div>
</div>


</body>
</html>
