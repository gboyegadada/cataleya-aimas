<?php

    define ('OUTPUT', 'JSON'); // Whether to output as JSON or HTML (especially errors)...


    // LOAD APPLICATION TOP...
    require_once ('app_top.php');





    // VALIDATE AUTH TOKEN...
    //validate_auth_token ();

    $_payload = array (
        'Categories'    =>  array (),
        'Sales' =>  array (),
        'Coupons'   =>  array (),
        'TaxClasses'    =>  array ()
    );


    // [1] CATEGORIES
    $Categories = Cataleya\Catalog\Tags::load();

    foreach ($Categories as $Category)
    {
        $_payload['Categories'][] = array (
            'id'    =>  $Category->getID(),
            'label' =>  ((!$Category->isRoot() && !$Category->getParent()->isRoot()) ? $Category->getParent()->getDescription()->getTitle('EN').' / ' : '') . $Category->getDescription()->getTitle('EN')
        );

    }



    // [2] SALES
    $Sales = Cataleya\Sales\Sales::load();

    foreach ($Sales as $Sale)
    {
        $_payload['Sales'][] = array (
            'id'    =>  $Sale->getID(),
            'label' =>  $Sale->getDescription()->getTitle('EN')
        );

    }



    // [3] COUPONS
    $Coupons = Cataleya\Sales\Coupons::load();

    foreach ($Coupons as $Coupon)
    {
        $_payload['Coupons'][] = array (
            'id'    =>  $Coupon->getID(),
            'store_id'  =>  $Coupon->getStoreId(),
            'code' =>  $Coupon->getCode(),
            'label' =>  ($Coupon->getCode() . ' (' . $Coupon->getStore()->getDescription()->getTitle('EN') . ')')
        );

    }




    // [4] ZONES
    $Zones = Cataleya\Geo\Zones::load();

    foreach ($Zones as $Zone)
    {
        $_payload['Zones'][] = array (
            'id'    =>  $Zone->getID(),
            'name'  =>  $Zone->getZoneName(),
            'label'  =>  $Zone->getZoneName(),
            'listType'  =>  $Zone->getListType(),
            'countryPopulation' => $Zone->getCountryPopulation(),
            'provincePopulation' => $Zone->getProvincePopulation()
        );

    }




    // [5] STORES
    $Stores = Cataleya\Stores::load();

    foreach ($Stores as $Store)
    {
        $_Currency = $Store->getCurrency();

        $_payload['Stores'][] = array (
            'id'    =>  $Store->getID(),
            'label' =>  $Store->getDescription()->getTitle('EN'),
            'Currency'  =>  array (
                'name'  =>  $_Currency->getCurrencyName(),
                'code'  =>  $_Currency->getCurrencyCode(),
                'utf'  =>  $_Currency->getUtfCode()
            )
        );

    }





    // [6] PRODUCT TYPES
    $ProductTypes = Cataleya\Catalog\Product\Types::load();
    $_payload['ProductTypes'] = [];

    foreach ($ProductTypes as $ProductType)
    {
        $_payload['ProductTypes'][] = array (
            'id'    =>  $ProductType->getID(),
            'label' =>  $ProductType->getName(),
            'entity_name' => 'Product Type',
            'name' =>  $ProductType->getName(),
            'name_plural' =>  $ProductType->getNamePlural(),
            'input_name' => 'product_type_id'

        );

    }




    // [6] MANUFACTURERS
    $_list = Cataleya\Agent\_::load('MANUFACTURER');
    $_payload['Manufacturers'] = [];

    foreach ($_list as $_item)
    {
        $_payload['Manufacturers'][] = array (
            'id'    =>  $_item->getID(),
            'label' =>  $_item->getCompanyName() . ' (' . $_item->getContactFirstName() . ')',
            'entity_name' => $_item->getRole()->getLabel(),
            'name' =>  $_item->getCompanyName(),
            'contact_first_name' =>  $_item->getContactFirstName(),
            'contact_last_name' =>  $_item->getContactLastName(),
            'input_name' => 'manufacturer_id'

        );

    }






    // [6] SUPPLIERS
    $_list = Cataleya\Agent\_::load('SUPPLIER');
    $_payload['Suppliers'] = [];

    foreach ($_list as $_item)
    {
        $_payload['Suppliers'][] = array (
            'id'    =>  $_item->getID(),
            'label' =>  $_item->getCompanyName() . ' (' . $_item->getContactFirstName() . ')',
            'entity_name' => $_item->getRole()->getLabel(),
            'name' =>  $_item->getCompanyName(),
            'contact_first_name' =>  $_item->getContactFirstName(),
            'contact_last_name' =>  $_item->getContactLastName() ,
            'input_name' => 'supplier_id'

        );

    }




    // [6] SHIPPING AGENTS
    $_list = Cataleya\Agent\_::load('CARRIER');
    $_payload['Carriers'] = [];

    foreach ($_list as $_item)
    {
        $_payload['Carriers'][] = array (
            'id'    =>  $_item->getID(),
            'label' =>  $_item->getCompanyName() . ' (' . $_item->getContactFirstName() . ')',
            'entity_name' => $_item->getRole()->getLabel(),
            'name' =>  $_item->getCompanyName(),
            'contact_first_name' =>  $_item->getContactFirstName(),
            'contact_last_name' =>  $_item->getContactLastName(),
            'input_name' => 'carrier_id'
        );

    }







    // [6] IMPORTER FILE TYPES


    $_payload['ImporterFileTypes'] = array (

        array (
            'id'    =>  Cataleya\Helper\Importer\Reader::FILE_TYPE_CATALEYA_CSV,
            'label' =>  'CSV File',
            'name' =>  'CSV File',
            'name_plural' =>  'CSV Files'
        ),

        array (
            'id'    =>  Cataleya\Helper\Importer\Reader::FILE_TYPE_SHOPIFY_CSV,
            'label' =>  'Shopify CSV File',
            'name' =>  'Shopify CSV File',
            'name_plural' =>  'Shopify CSV Files'
        )


    );




    // Output...
    $json_data = array (
                    'status' => 'Ok',
                    'message' => 'Info.',
                    'payload'  =>  $_payload
                    );
    echo json_encode($json_data);
    exit();
