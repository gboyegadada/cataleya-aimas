<?php



define ('OUTPUT', 'JSON'); // Whether to output as JSON or HTML (especially errors)...

// For our file upload...allow up to 5 minutes...
set_time_limit (60 * 5); 

// LOAD APPLICATION TOP...
require_once ('app_top.php');




$WHITE_LIST = array(
						'auth_token', 
                                                'photo_uploader', 
						'pw', 
						'ph', 
						'tw', 
						'th'
					);



// DIE IF INPUT IS NOT VIA POST METHOD...
if ($_SERVER['REQUEST_METHOD'] != 'POST') _json_error('POST METHOD ONLY PLEASE. NOW GET.', __LINE__, true);


// SANITIZE INPUT DATA...
$_CLEAN = filter_input_array(INPUT_POST, 
								array(
										'auth_token'		=>	FILTER_SANITIZE_STRIPPED,
										'pw'	=>	array('filter'		=>	FILTER_VALIDATE_INT, 
																'options'	=>	array('min_range' => 50, 'max_range' => 1125)
																),  
										'ph'	=>	array('filter'		=>	FILTER_VALIDATE_INT, 
																'options'	=>	array('min_range' => 50, 'max_range' => 1125)
																), 
										'tw'	=>	array('filter'		=>	FILTER_VALIDATE_INT, 
																'options'	=>	array('min_range' => 10, 'max_range' => 2125)
																),  
										'th'	=>	array('filter'		=>	FILTER_VALIDATE_INT, 
																'options'	=>	array('min_range' => 10, 'max_range' => 2125)
																)    

										)
										
								);






// Check if INPUT keys are ALL expected AND MADE IT THROUGH...
$_suspect = anySuspects();
if ($_suspect !== FALSE) _catch_error('Error processing white_list! **' . $_suspect, __LINE__, true);


// VALIDATE AUTH TOKEN...
validate_auth_token ();



    
    
define("FILE_INPUT_NAME", "photo_uploader");

// Original width and height...
list($ow, $oh) = getimagesize($_FILES[FILE_INPUT_NAME]['tmp_name']);

       

// Create image ...

$new_image = Cataleya\Asset\Image::create(Cataleya\Asset\Image::SRC_UPLOAD, $_FILES[FILE_INPUT_NAME]['tmp_name'], $_CLEAN['tw'], $_CLEAN['th'], $_CLEAN['pw'], $_CLEAN['ph']);

if ($new_image == NULL) _catch_error('Image too small. Please use at least ' . $_CLEAN['tw'] . 'px by ' . $_CLEAN['th'] . 'px.', __LINE__, true);




// Preview width and height...
list($pw, $ph) = $new_image->getSize(Cataleya\Asset\Image::TEMP_PREVIEW);




$data = array (
		'status'=>'Ok', 
		'message'=>'Upload successful', 
		'image_url'=>$new_image->getHref(Cataleya\Asset\Image::TEMP_PREVIEW), 
		'image_id'=>$new_image->getID(), 
		'pw'=>$pw, 
		'ph'=>$ph, 
		'ow'=>$ow, 
		'oh'=>$oh, 
		'tw'=>$_CLEAN['tw'], 
		'th'=>$_CLEAN['th']
		);
echo json_encode($data);

