<?php



define ('OUTPUT', 'JSON'); // Whether to output as JSON or HTML (especially errors)...


// LOAD APPLICATION TOP...
require_once ('app_top.php');




$WHITE_LIST = array(
						'auth_token',  
                                                'target_id', 
                                                'do', 
                                                'data'
						);





// DIE IF INPUT IS NOT VIA POST METHOD...
if ($_SERVER['REQUEST_METHOD'] != 'POST') _catch_error('POST METHOD ONLY PLEASE. NOW GET.', __LINE__, true);

// SANITIZE INPUT DATA...
$_CLEAN = filter_input_array(INPUT_POST, array(
                                                    'auth_token'		=>	FILTER_SANITIZE_STRIPPED,
                                                    'target_id'	=>	FILTER_VALIDATE_INT, 
                                                    'data'	=>	array( 'flags' => FILTER_REQUIRE_ARRAY ), 
                                                    'do'	=>	array('filter'		=>	FILTER_VALIDATE_REGEXP, 
                                                                                                'options'	=>	array('regexp' => '/^[A-Za-z ]{1,50}$/')
                                                                                                                ), 

                                                    )
										
					);


// Check if INPUT keys are ALL expected AND MADE IT THROUGH...
$_suspect = anySuspects();
if ($_suspect !== FALSE) _catch_error('Error processing white_list!' . $_suspect, __LINE__, true);




// VALIDATE AUTH TOKEN...
validate_auth_token ();

// Validate task

$_FUNCS = array (
    'void', 
    
    
    'addPaymentType', 
    'removePaymentType',
    'deleteStore', 
    
    
    
);





if (!in_array($_CLEAN['do'], $_FUNCS) || !is_callable($_CLEAN['do'])) _catch_error('Invalid action.', __LINE__, true);



define('DASH_LANGUAGE_CODE', Cataleya\System::load()->getDashboardLanguageCode());
define('SHOPFRONT_LANGUAGE_CODE', Cataleya\System::load()->getShopFrontLanguageCode());
     



// OUTPUT...
 $json_reply = array (
                 "status" => 'ok', 
                 "message" => 'Void'
                 );
 
 

// EXECUTE API CALL

// API FUNC CALL
$_CLEAN['do']();



// HELPER FUNCS


function digestParams () 
{
    global $_CLEAN;
    $_params = $_CLEAN['data'];
    $_bad_params = array ();
    


    if (isset($_params['text'])) 
    {
        // [1] Validate description
        $_params['language_code'] = DASH_LANGUAGE_CODE;

        $_params['text'] = Cataleya\Helper\Validator::html($_params['text']);
        if ($_params['text'] === FALSE) $_bad_params[] = 'text';

    }
    
    
    
    
    if (isset($_params['store_id'])) 
    {
        $_params['store_id'] = Cataleya\Helper\Validator::int($_params['store_id']);
        if ($_params['store_id'] === FALSE) $_bad_params[] = 'store_id';

    }
    
    

    $_params['auto_confirm'] = isset($_params['auto_confirm']) 
                                ? filter_var($_params['auto_confirm'], FILTER_VALIDATE_BOOLEAN) 
                                : FALSE;
    
    
    
    // Check if every one made it through
    if (!empty($_bad_params)) _catch_error('Unable to save.' . implode, __LINE__, true);
    
    return $_params;
}












/*
 * 
 * 
 * CALLBACKS
 * 
 */


function void () {
    
}














// Remove token...
if (isset($_SESSION[SESSION_PREFIX.'CONFIRM_TOKEN'])) unset ($_SESSION[SESSION_PREFIX.'CONFIRM_TOKEN']);




 echo (json_encode($json_reply));
 exit();  
