<?php
/*
FETCH SHIPPING OPTIONS AND RETURN AS JSON...
*/




define ('OUTPUT', 'JSON'); // Whether to output as JSON or HTML (especially errors)...


// LOAD APPLICATION TOP...
require_once ('app_top.php');






// DIE IF INPUT IS NOT VIA POST METHOD...
if ($_SERVER['REQUEST_METHOD'] != 'POST') _catch_error('POST METHOD ONLY PLEASE. NOW GET.');
	

$WHITE_LIST = array(
						'auth_token', 
                                                'store_id'
					);

// FILTER VALUES ....
$_CLEAN = filter_input_array(INPUT_POST, 
								array(	
                                                                                // auth_token
                                                                                'auth_token'    =>  FILTER_SANITIZE_STRIPPED, 
                                                                    
										 // store_id
										'store_id'	=>	FILTER_VALIDATE_INT

										)
								);



// Check if INPUT keys are ALL expected AND MADE IT THROUGH...
if (anySuspects()) _catch_error('Error processing white_list!', __LINE__, true);



// VALIDATE AUTH TOKEN...
validate_auth_token ();


// Load Store
$Store = Cataleya\Store::load($_CLEAN['store_id']);
if ($Store === NULL) _catch_error('Store could not be loaded.', __LINE__, true);

// Load shipping options
$ShippingOptions = Cataleya\Shipping\ShippingOptions::load($Store);


// retrieve info about tax class
$store_info = array (
        'id'    =>  $Store->getID(), 
        'name'  => $Store->getDescription()->getTitle('EN'), 
        'description'  =>  $Store->getDescription()->getText('EN'),
        'population' => $ShippingOptions->getPopulation(), 
        'populationAsText' => count_in_english($ShippingOptions->getPopulation(), 'Shipping option', 'Shipping options')
    );


// retrieve info about tax rates
$shipping_options_info = array ();

foreach ($ShippingOptions as $ShippingOption) 
{
    
    
        
        // retrieve info about shipping carrier
    
        $Carrier = $ShippingOption->getCarrier();
    
        $carrier_info = array (
                'id'    =>  $Carrier->getID(), 
                'name'  =>  $Carrier->getCompanyName()
            );
        
        

        // retrieve info about zone
    
        $zone = $ShippingOption->getShippingZone();
    
        $zone_info = array (
                'id'    =>  $zone->getID(), 
                'name'  =>  $zone->getZoneName(), 
                'listType'  =>  $zone->getListType(),
                'countryPopulation' => $zone->getCountryPopulation(), 
                'provincePopulation' => $zone->getProvincePopulation()
            );
        
        
        // retrieve info about tax class
        

        

        
        $shipping_options_info[] = array (
            'id'   =>  $ShippingOption->getID(), 
            'name'  =>  $ShippingOption->getDescription()->getTitle('EN'), 
            'description'  =>  $ShippingOption->getDescription()->getText('EN'),  
            'deliveryDays'  =>  $ShippingOption->getMaxDeliveryDays(),  
            'rateType'  =>  $ShippingOption->getRateType(), 
            'rate'  =>  $ShippingOption->getRate(), 
            'Zone'  =>  $zone_info, 
            'Carrier'   =>  $carrier_info, 
            'isTaxable'  =>  $ShippingOption->isTaxable()
        );
}







// OUTPUT...
$json_reply = array (
		"status"=>'ok', 
		"message"=> 'Shipping options.', 
                "Store"=>$store_info, 
                "ShippingOptions"=>$shipping_options_info
		);

echo (json_encode($json_reply));
exit();



?>
