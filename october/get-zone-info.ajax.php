<?php
/*
FETCH PROVINCES AND RETURN AS JSON...
*/




define ('OUTPUT', 'JSON'); // Whether to output as JSON or HTML (especially errors)...


// LOAD APPLICATION TOP...
require_once ('app_top.php');






// DIE IF INPUT IS NOT VIA POST METHOD...
if ($_SERVER['REQUEST_METHOD'] != 'POST') _catch_error('POST METHOD ONLY PLEASE. NOW GET.');
	

$WHITE_LIST = array(
						'auth_token', 
                                                'zone_id'
					);

// FILTER VALUES ....
$_CLEAN = filter_input_array(INPUT_POST, 
								array(	
                                                                                // auth_token
                                                                                'auth_token'    =>  FILTER_SANITIZE_STRIPPED, 
                                                                    
										 // zone_id
										'zone_id'	=>	FILTER_VALIDATE_INT

										)
								);



// Check if INPUT keys are ALL expected AND MADE IT THROUGH...
if (anySuspects()) _catch_error('Error processing white_list!', __LINE__, true);


// VALIDATE AUTH TOKEN...
validate_auth_token ();


// Load Zone
$zone = Cataleya\Geo\Zone::load($_CLEAN['zone_id']);
if ($zone === NULL) _catch_error('Zone could not be loaded.', __LINE__, true);


// retrieve info about zone
$zone_info = array (
        'id'    =>  $zone->getID(), 
        'name'  =>  $zone->getZoneName(), 
        'listType'  =>  $zone->getListType(),
        'countryPopulation' => $zone->getCountryPopulation(), 
        'provincePopulation' => $zone->getProvincePopulation()
    );


// retrieve info about countries
$countries_info = array ();
$countries = $zone->getCountries();

foreach ($countries as $country) 
{
        $countries_info[] = array (
            'iso'   =>  $country->getCountryCode(), 
            'name'  =>  $country->getPrintableName()
        );
}



// retrieve info about provinces
$provinces_info = array ();
$provinces = $zone->getProvinces();

foreach ($provinces as $province) 
{
        $provinces_info[] = array (
            'id'    =>  $province->getID(), 
            'iso'   =>  $province->getIsoCode(), 
            'name'  =>  $province->getName(), 
            'printableName' => $province->getPrintableName()
        );
}






// OUTPUT...
$json_reply = array (
		"status"=>'ok', 
		"message"=> 'Zone info.', 
                "zone"=>$zone_info, 
                "countries"=>$countries_info, 
                "provinces"=>$provinces_info
		);

echo (json_encode($json_reply));
exit();



?>