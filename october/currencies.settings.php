<?php

define ('OUTPUT', 'HTML'); // Whether to output as JSON or HTML (especially errors)...


// LOAD APPLICATION TOP...
require_once ('app_top.php');



// Load base currency
$base_currency = Cataleya\Locale\Currency::loadBaseCurrency();



?>





<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<?php
include "includes/meta.php";

?>

<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Settings | Currencies</title>

<?php
require_once(INC_PATH.'js_libs.php');
?>
<script type="text/javascript" src="ui/jscript/libs/jquery.form.js"></script>
<script type="text/javascript" src="ui/jscript/currencies.edit.js"></script>



<?php
require_once(INC_PATH.'css_libs.php');
?>

<link href="ui/css/currencies.edit.css" rel="stylesheet" type="text/css" />


<style type="text/css" media="screen">


#page_h_tweak {
	position:absolute;
	top:1000px;
	left:0px;
	
	width:1px;
	height:1px;
}



#tile_grid {
	position:absolute;
	top:160px;
	left:350px;
	
	height:600px;
	width:590px;
	
	border:solid 1px #222;

	display:none;

}



</style>

<script type="text/javascript">


</script>


</head>

<body>
<!-- BEGIN: META -->
<input type="hidden" id="meta_token" value="<?=AUTH_TOKEN?>"  />
<input type="hidden" id="meta_admin_root" value="<?=BASE_URL?>"  />

<!-- END: META -->




<div class="container">
<?php include_once INC_PATH.'photo-upload-forms.php'; ?>
    
<div class="header-wrapper absolute">
    <div class="header">

    <div id="page-title">Currencies</div>   

    </div>
    
<?php include_once INC_PATH.'main.nav.php'; ?>
    
</div>

<div class="content">




<!-- UPLOADER FORM FOR JSON CURRENCY UPDATE -->
<form id="currency-update-uploader-form" action="<?=BASE_URL?>update_currency_rates.ajax.php" method="post" enctype="multipart/form-data" style="display:none;"  >

<input type="file" id="json_uploader" name="json_file" style="display:none;" onclick="set_json_uploader_flag();" onchange="upload_json_file(this);" />
<input type="hidden" name="auth_token" id="json_upload_token" style="display:none;" value="<?=AUTH_TOKEN?>" />
</form>


<!-- END: IMAGE CROP & ROTATE CANVAS -->





<div id="currency-list-outer-wrapper" >

    <ul id="currency-list-wrapper">
        

        
<?php
    $currencies = new Cataleya\Locale\Currencies ();
    
    foreach ($currencies as $currency) 
    {
        
?>

        <li>
            <a href="javascript:void(0);" id="currency-<?=$currency->getCurrencyCode()?>" class="currency-anchor <?=((int)$currency->getActive() === 1) ? 'icon-checkbox-partial' : 'icon-checkbox-unchecked'?>" onclick="toggleCurrencyStatus('<?=$currency->getCurrencyCode()?>'); return false;"><?=$currency->getCurrencyName()?> (&#<?=$currency->getUtfCode()?>;)</a>
            
            <small class="tiny-status" id="currency-<?=$currency->getCurrencyCode()?>-status">&nbsp;</small>
            
            <br/>
            <small class="currency-equiv"><?=$base_currency->getCurrencyCode()?> 1 : <?=$currency->getCurrencyCode()?> <?=$currency->getRate()?></small>
            <br/>
            
<!--
            
            <div id="currency-<?=$currency->getCurrencyCode()?>-import" class="price-import-wrapper" style="display:<?=((int)$currency->getActive() === 1) ? 'block' : 'none'?>" >
             <small class="text-input-label">Import prices from a previously enabled currency.</small><br/>
             <select id="currency-<?=$currency->getCurrencyCode()?>-import-src" name="currency" tinyStatus="#store-currency-status"  onchange="saveStoreInfo(this)"> 
                
                <?php
                
                $currencies2 = new Cataleya\Locale\Currencies ();
                foreach ($currencies2 as $currency2)
                {
                 if ((int)$currency2->getActive() === 0 || $currency2->getCurrencyCode() === $currency->getCurrencyCode()) continue;
                 
                ?>
                <option value="<?=$currency2->getCurrencyCode()?>" ><?=$currency2->getCurrencyName()?></option>
                <?php
                
                }
                ?>  
                
            </select> 
            <a class="button-2" href="javascript:void(0);" onClick="importPrices(<?=$currency->getCurrencyCode()?>); return false;" >
                Import Prices
            </a>               
                
            </div>
            
 -->          

            <div style="height: 20px; width: 1px; clear: both;">&nbsp;</div>        
        </li>

 <?php
 
    }
    
    ?>
        
        
        
        
        <li>
            <h3 class="section-header">Update Currency Rates</h3>
            
            <div class="price-import-wrapper" style="display:block" >
             <small class="text-input-label">Upload a JSON file with new currency rates.</small><br/> 
            <a class="button-2" href="javascript:void(0);" onClick="initCurrencyUpdate(); return false;" >
                upload json file
            </a>             
            </div>

            <div style="height: 20px; width: 1px; clear: both;">&nbsp;</div>
        </li>
    
        
        
        <li>
            <h3 class="section-header">Change Base Currency</h3>
            
            <div class="price-import-wrapper" style="display:block" >
             <small class="text-input-label">Change base currency. <br/><br/>Note: ALL EXCHANGE RATES and PRODUCT PRICES will be recalculated and may not be accurate. It is strongly advised that you do NOT do this.</small><br/> 
             
             <select id="base-currency-select" name="currency" tinyStatus="#base-currency-status"  > 
                
                <?php
                

                $currencies = new Cataleya\Locale\Currencies ();
                foreach ($currencies as $currency)
                {
                    
                if ((int)$currency->getActive() === 0) continue;
                 
                ?>
                <option value="<?=$currency->getCurrencyCode()?>" <?=($currency->getCurrencyCode() == $base_currency->getCurrencyCode()) ? 'selected="selected"' : ''?>><?=$currency->getCurrencyName()?></option>
                <?php
                
                }
                ?>  
                
            </select>              
             
             &nbsp;&nbsp;
             
            <a class="button-2" href="javascript:void(0);" onClick="setBaseCurrency(); return false;" >
                Change base currency
            </a>  
             
            </div>

            <div style="height: 60px; width: 1px; clear: both;">&nbsp;</div>
        </li>        
        

    
    </ul>



    
</div>        


<!--

<div id="footer">
&nbsp;
</div>

-->
  </div>
</div>



<div id="page_h_tweak">&nbsp;</div>

</body>
</html>