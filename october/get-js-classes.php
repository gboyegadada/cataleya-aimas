<?php



define ('OUTPUT', 'JSCRIPT'); // Whether to output as JSON or HTML (especially errors)...


// LOAD APPLICATION TOP...
// require_once ('app_top.php');

define ('IS_ADMIN_FLAG', TRUE);

////////// CONFIG //////////
require_once ('includes/admin.core.config.php');	


header('Content-Type: application/javascript');


$_js_file = '';


$_path_to_js_classes = ADMIN_ROOT_PATH . 'ui/jscript/classes/';
$_dir  = scandir($_path_to_js_classes);

foreach ($_dir as $filename) {
    if (preg_match('/^(([A-Za-z0-9\-_]+\.?){1,10})\.(js)$/', $filename) > 0) $_js_file .= file_get_contents ($_path_to_js_classes.$filename);
}


header('Content-Type: application/javascript');

echo "var _admin_root = '" . BASE_URL . "';\n\n";

echo $_js_file;


