<?php
/*
FETCH PROVINCES AND RETURN AS JSON...
*/




define ('OUTPUT', 'JSON'); // Whether to output as JSON or HTML (especially errors)...


// LOAD APPLICATION TOP...
require_once ('app_top.php');






// DIE IF INPUT IS NOT VIA POST METHOD...
if ($_SERVER['REQUEST_METHOD'] != 'POST') _catch_error('POST METHOD ONLY PLEASE. NOW GET.');
	

$WHITE_LIST = array(
						'auth-token', 
						'country-code', 
						'province-iso-code', 
                                                'province-name', 
                                                'province-id'
					);

// FILTER VALUES ....
$_CLEAN = filter_input_array(INPUT_POST, 
								array(	
                                                                                // auth_token
                                                                                'auth-token'    =>  FILTER_SANITIZE_STRIPPED, 
										 // country id...
										'country-code'	=>	array('filter'		=>	FILTER_VALIDATE_REGEXP, 
														'options'	=>	array('regexp' => '/^[A-Za-z]{2}$/')
																), 
										 // province id...
										'province-iso-code'	=>	array('filter'		=>	FILTER_VALIDATE_REGEXP, 
														'options'	=>	array('regexp' => '/^[A-Za-z]{2,4}$/')
																), 
										 // province id...
										'province-name'	=>	array('filter'		=>	FILTER_VALIDATE_REGEXP, 
														'options'	=>	array('regexp' => '/^[A-Za-z0-9,\-\W]{2,400}$/')
																), 
										 // province_id (>0) for save, (0) for create...
										'province-id'	=>	FILTER_VALIDATE_INT

										)
								);



// Check if INPUT keys are ALL expected AND MADE IT THROUGH...
if (anySuspects()) _catch_error('Error processing white_list!', __LINE__, true);

// Load country
$country = Cataleya\Geo\Country::load($_CLEAN['country-code']);
if ($country === NULL) _catch_error('Country could not be loaded.', __LINE__, true);


// New province ?
if ((int)$_CLEAN['province-id'] === 0) 
{
   $province = $country->addProvince($_CLEAN['province-iso-code'], $_CLEAN['province-name']);
   if ($province === NULL) _catch_error('Province could not be created.', __LINE__, true);
}

// Save province ?
else if ((int)$_CLEAN['province-id'] > 0) 
{
   $province = Cataleya\Geo\Province::load($_CLEAN['province-id']);
   if ($province === NULL) _catch_error('Province could not be loaded.', __LINE__, true);
   
   $province->setIsoCode($_CLEAN['province-iso-code']);
   $province->setPrintableName(ucfirst(strtolower($_CLEAN['province-name'])));
   $province->setName(strtoupper($_CLEAN['province-name']));
   
   
}


$province_info = array (
        'id'    =>  $province->getID(), 
        'iso'   =>  $province->getIsoCode(), 
        'name'  =>  $province->getPrintableName()
    );




// OUTPUT...
$json_reply = array (
		"status"=>'ok', 
		"message"=> ((int)$_CLEAN['province-id'] === 0) ? 'New province added.' : 'Province saved.', 
                "province"=>$province_info, 
                "provinceCount"=>$country->getPopulation()
		);

echo (json_encode($json_reply));
exit();



?>