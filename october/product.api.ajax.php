<?php



define ('OUTPUT', 'JSON'); // Whether to output as JSON or HTML (especially errors)...


// LOAD APPLICATION TOP...
require_once ('app_top.php');




$WHITE_LIST = array(
						'auth_token',  
                                                'target_id', 
                                                'do', 
                                                'data'
						);





// DIE IF INPUT IS NOT VIA POST METHOD...
if ($_SERVER['REQUEST_METHOD'] != 'POST') _catch_error('POST METHOD ONLY PLEASE. NOW GET.', __LINE__, true);

// SANITIZE INPUT DATA...
$_CLEAN = filter_input_array(INPUT_POST, array(
                                                    'auth_token'		=>	FILTER_SANITIZE_STRIPPED,
                                                    'target_id'	=>	FILTER_VALIDATE_INT, 
                                                    'data'	=>	array( 'flags' => FILTER_REQUIRE_ARRAY ), 
                                                    'do'	=>	array('filter'		=>	FILTER_VALIDATE_REGEXP, 
                                                                                                'options'	=>	array('regexp' => '/^[A-Za-z ]{1,50}$/')
                                                                                                                ), 

                                                    )
										
					);


// Check if INPUT keys are ALL expected AND MADE IT THROUGH...
$_suspect = anySuspects();
if ($_suspect !== FALSE) _catch_error('Error processing white_list!' . $_suspect, __LINE__, true);




// VALIDATE AUTH TOKEN...
validate_auth_token ();

// Validate task

$_FUNCS = array (
    'void' => array (), 
    'saveTitle' => array (
        'text' => 'text'
    ), 
    'saveProductDetails' => array (
        'text' => 'string'
    ), 
    'saveDescription' => array (
        'title' => 'text', 
        'description' => 'html'
    ), 
    'saveProductReferenceCode' => array (
        'text' => 'string'
    ), 
    
    // Product Options...
    'newProductOption' => array (), 
    'getProductOptionInfo' => array (), 
    'saveOptionSKU' => array (
        'text' => 'string'
    ), 
    'saveOptionUPC' => array (
        'text' => 'string'
    ), 
    'saveOptionEAN' => array (
        'text' => 'string'
    ), 
    'saveOptionISBN' => array (
        'text' => 'string'
    ), 
    'deleteProductOption' => array (
        'option_id' => 'integer_array'
    ), 
    'saveProductOptionImage' => array (
        'option_id' =>  'integer_array', 
        'image_id'=>'integer', 
        'rotate'=>'float', 
        'sw'=>'float', 
        'sh'=>'float', 
        'sx'=>'float', 
        'sy'=>'float'
        ),
    'removeProductOptionImage' => array (
        'option_id' => 'integer_array'
    ), 
    'savePricePlural' => array (
        'text' => 'float', 
        'store_id' => 'int',
        'option_id' => 'integer_array'
    ),
    'saveStockPlural' => array (
        'text' => 'float', 
        'store_id' => 'int',
        'option_id' => 'integer_array'
    ),
    'savePrice' => array (
        'text' => 'float', 
        'store_id' => 'int'
    ),
    'saveStock' => array (
        'text' => 'float', 
        'store_id' => 'int'
    ), 
    
    
    // Product Types / Attribute Types
    'saveNewAttribute' => array (
        'text' => 'string', 
        'option_id' => 'int'
    ), 
    'addOptionAttribute' => array (
        'option_id' => 'int'
    ), 
    
    
    'newProduct' => array (), 
    'getProductInfo' => array (),
    'saveProduct', 
    'deleteProduct', 
    'saveBaseImage' => array (
        'set_primary'=>'bool', 
        'image_id'=>'integer', 
        'rotate'=>'float', 
        'sw'=>'float', 
        'sh'=>'float', 
        'sx'=>'float', 
        'sy'=>'float'
        ), 
    'deleteBaseImage' => array (
        'image_id'=>'integer'
        ), 
    'setPrimaryBaseImage' => array (
        'image_id'=>'integer'
        ), 
    'removePrimaryBaseImage' => array (
        'product_id'=>'integer'
        ), 
    
    'search' => array (
        'text' => 'string'
    ), 
    'addToProductGroup' => array (
        'product_id'=>'integer'
        ), 
    'removeFromProductGroup' => array (
        'product_id'=>'integer'
        )
    
);






if (!array_key_exists($_CLEAN['do'], $_FUNCS) || !is_callable($_CLEAN['do'])) _catch_error('Invalid action.', __LINE__, true);



define('DASH_LANGUAGE_CODE', Cataleya\System::load()->getDashboardLanguageCode());
define('SHOPFRONT_LANGUAGE_CODE', Cataleya\System::load()->getShopFrontLanguageCode());
     
    
// OUTPUT...
 $json_reply = array (
                 "status" => 'ok', 
                 "message" => 'Ok'
                 );
 
 

// EXECUTE API CALL

// API FUNC CALL
$_CLEAN['do']();



// HELPER FUNCS


function digestParams () 
{
    global $_CLEAN, $_FUNCS;
    $_params = Cataleya\Helper\Validator::params($_FUNCS[$_CLEAN['do']], $_CLEAN['data']);
    $_bad_params = array ();
    

    if (!isset($_params['language_code'])) 
    {
        $_params['language_code'] = DASH_LANGUAGE_CODE;

    }
    
    
    
    
    
    // Check if every one made it through
    
    foreach ($_params as $k=>$v) {
        if ($v === NULL) $_bad_params[] = $k;
    }
    

    if (!empty($_bad_params)) _catch_error('Unable to save.' . implode, __LINE__, true);
    
    return $_params;
}














/*
 * 
 * 
 * CALLBACKS
 * 
 */


function void () {
    
}






/*
 * 
 * [ newProduct ]
 * ______________________________________________________
 * 
 * @param: int $_POST['target_id']
 * @param: int $_POST['data']
 * 
 */


function newProduct () {

    global $json_reply;
    $_params = digestParams();
    
    $_ProductType = Cataleya\Catalog\Product\Type::load($_params['product_type_id']);
    if ($_ProductType === NULL ) _catch_error('Product type could not be found.', __LINE__, true);
    
    Cataleya\Helper\DBH::getInstance()->beginTransaction();
    $_Product = Cataleya\Catalog\Product::create($_ProductType, $_params['title'], $_params['description'], 'EN');
    Cataleya\Helper\DBH::getInstance()->commit();
    
    $json_reply['message'] = 'New product saved.';
    $json_reply['Product'] = Cataleya\Front\Dashboard\Juicer::juice($_Product);

}





/*
 * 
 * [ newProductOption ]
 * ______________________________________________________
 * 
 * @param: int $_POST['target_id']
 * 
 * 
 */


function newProductOption () {

    global $_CLEAN, $json_reply;
 
    $_Product = Cataleya\Catalog\Product::load($_CLEAN['target_id']);
    if ($_Product === NULL ) _catch_error('Product could not be found.', __LINE__, true);
    
    $_ProductOption = Cataleya\Catalog\Product\Option::create ($_Product);
    
    $json_reply['message'] = 'Product option added.';
    $json_reply['ProductOption'] = Cataleya\Front\Dashboard\Juicer::juice($_ProductOption);
    $json_reply['ProductOption']['isNew'] = true;

}







/*
 * 
 * [ saveProduct ]
 * ______________________________________________________
 * 
 * @param: int $_POST['target_id']
 * @param: int $_POST['data']
 * 
 */


function saveTitle () {

    global $_CLEAN, $json_reply;
    
    $_params = digestParams();
    $_Product = Cataleya\Catalog\Product::load($_CLEAN['target_id']);
    if ($_Product === NULL ) _catch_error('Product could not be found.', __LINE__, true);
    
    // [1] Product Description
    $_Product->getDescription()->setTitle($_params['text'], 'EN');
    
    // Update meta_keywords
    $_Product->updateMetaKeywords()

    // Update full_text_index
    ->updateFullTextIndex()

    // Update url_rewrite...
    ->setHandle($_params['text']);
    
    
    $json_reply['message'] = 'Title saved.';
    $json_reply['Product'] = Cataleya\Front\Dashboard\Juicer::juiceDrop($_Product);

}






/*
 * 
 * [ saveProductDetails ]
 * ______________________________________________________
 * 
 * @param: int $_POST['target_id']
 * @param: int $_POST['data']
 * 
 */


function saveProductDetails () {

    global $_CLEAN, $json_reply;
    
    $_params = digestParams();
    $_Product = Cataleya\Catalog\Product::load($_CLEAN['target_id']);
    if ($_Product === NULL ) _catch_error('Product could not be found.', __LINE__, true);
    
    // [1] Product Description
    $_Product->getDescription()->setText($_params['text'], 'EN');
    
    
    $json_reply['message'] = 'Details saved.';
    $json_reply['Product'] = Cataleya\Front\Dashboard\Juicer::juiceDrop($_Product);

}








/*
 * 
 * [ saveDescription ]
 * ______________________________________________________
 * 
 * @param: int $_POST['target_id']
 * @param: int $_POST['data']
 * 
 */


function saveDescription () {

    global $_CLEAN, $json_reply;
    
    $_params = digestParams();
    $_Product = Cataleya\Catalog\Product::load($_CLEAN['target_id']);
    if ($_Product === NULL ) _catch_error('Product could not be found.', __LINE__, true);
    
    // [1] Product Description
    $_Description = $_Product->getDescription(); 
    
    $_Description->setTitle($_params['title'], 'EN');
    $_Description->setText($_params['description'], 'EN');
    
    
    $json_reply['message'] = 'Details saved.';
    $json_reply['Product'] = Cataleya\Front\Dashboard\Juicer::juiceDrop($_Product);

}





/*
 * 
 * [ saveProductReferenceCode ]
 * ______________________________________________________
 * 
 * @param: int $_POST['target_id']
 * @param: int $_POST['data']
 * 
 */


function saveProductReferenceCode () {

    global $_CLEAN, $json_reply;
    
    $_params = digestParams();
    $_Product = Cataleya\Catalog\Product::load($_CLEAN['target_id']);
    if ($_Product === NULL ) _catch_error('Product could not be found.', __LINE__, true);
    
    // [1] Product Description
    $_Product->setReferenceCode($_params['text']);
    
    
    $json_reply['message'] = 'Reference code saved.';
    // $json_reply['Product'] = Cataleya\Front\Dashboard\Juicer::juiceDrop($_Product);

}







/*
 * 
 * [ saveOptionSKU ]
 * ______________________________________________________
 * 
 * @param: int $_POST['target_id']
 * @param: array $_POST['data']
 * 
 */


function saveOptionSKU () {

    global $_CLEAN, $json_reply;
    
    $_params = digestParams();
    $_Option = Cataleya\Catalog\Product\Option::load($_CLEAN['target_id']);
    if ($_Option === NULL ) _catch_error('Product option could not be found.', __LINE__, true);
    
    // [1] Product Option
    $_Option->setSKU($_params['text']);
    
    
    $json_reply['message'] = 'Option reference code saved.';
    // $json_reply['Product'] = Cataleya\Front\Dashboard\Juicer::juiceDrop($_Product);

}





/*
 * 
 * [ saveOptionUPC ]
 * ______________________________________________________
 * 
 * @param: int $_POST['target_id']
 * @param: array $_POST['data']
 * 
 */


function saveOptionUPC () {

    global $_CLEAN, $json_reply;
    
    $_params = digestParams();
    $_Option = Cataleya\Catalog\Product\Option::load($_CLEAN['target_id']);
    if ($_Option === NULL ) _catch_error('Product option could not be found.', __LINE__, true);
    
    // [1] Product Option
    $_Option->setUPC($_params['text']);
    
    
    $json_reply['message'] = 'Option UPC code saved.';
    // $json_reply['Product'] = Cataleya\Front\Dashboard\Juicer::juiceDrop($_Product);

}





/*
 * 
 * [ saveOptionEAN ]
 * ______________________________________________________
 * 
 * @param: int $_POST['target_id']
 * @param: array $_POST['data']
 * 
 */


function saveOptionEAN () {

    global $_CLEAN, $json_reply;
    
    $_params = digestParams();
    $_Option = Cataleya\Catalog\Product\Option::load($_CLEAN['target_id']);
    if ($_Option === NULL ) _catch_error('Product option could not be found.', __LINE__, true);
    
    // [1] Product Option
    $_Option->setEAN($_params['text']);
    
    
    $json_reply['message'] = 'Option EAN code saved.';
    // $json_reply['Product'] = Cataleya\Front\Dashboard\Juicer::juiceDrop($_Product);

}





/*
 * 
 * [ saveOptionISBN ]
 * ______________________________________________________
 * 
 * @param: int $_POST['target_id']
 * @param: array $_POST['data']
 * 
 */


function saveOptionISBN () {

    global $_CLEAN, $json_reply;
    
    $_params = digestParams();
    $_Option = Cataleya\Catalog\Product\Option::load($_CLEAN['target_id']);
    if ($_Option === NULL ) _catch_error('Product option could not be found.', __LINE__, true);
    
    // [1] Product Option
    $_Option->setISBN($_params['text']);
    
    
    $json_reply['message'] = 'Option ISBN code saved.';
    // $json_reply['Product'] = Cataleya\Front\Dashboard\Juicer::juiceDrop($_Product);

}







/*
 * 
 * [ savePrice ]
 * ______________________________________________________
 * 
 * @param: int $_POST['target_id']
 * @param: array $_POST['data']
 * 
 * 
 */


function savePrice () {

    global $_CLEAN, $json_reply;
    
    $_params = digestParams();
    
    $_Price = Cataleya\Catalog\Price::load($_CLEAN['target_id']);
    if ($_Price === NULL ) _catch_error('Price could not be saved.', __LINE__, true);
    
    $_Store = Cataleya\Store::load($_params['store_id']);
    if ($_Store === NULL ) _catch_error('Store not found.', __LINE__, true);
    
    
    if (!$_Price->setValue($_Store, $_params['text'])) _catch_error('Could not set price.', __LINE__, true);
    
    
    $json_reply['message'] = 'Product option price saved.';
    $json_reply['Price'] = array (
        'id' => $_Price->getID(), 
        'value' => $_Price->getValue($_Store), 
        'formatted_value' => number_format($_Price->getValue($_Store), 2), 
        'debug_text' => $_params['text']
    ); 

}






/*
 * 
 * [ saveStock ]
 * ______________________________________________________
 * 
 * @param: int $_POST['target_id']
 * @param: array $_POST['data']
 * 
 * 
 */


function saveStock () {

    global $_CLEAN, $json_reply;
    
    $_params = digestParams();
    
    $_Stock = Cataleya\Catalog\Product\Stock::load($_CLEAN['target_id']);
    if ($_Stock === NULL ) _catch_error('Stock could not be saved.', __LINE__, true);
    
    $_Store = Cataleya\Store::load($_params['store_id']);
    if ($_Store === NULL ) _catch_error('Store not found.', __LINE__, true);
    
    
    $_Stock->setValue($_Store, $_params['text']);
    
    
    $json_reply['message'] = 'Product option stock saved.';
    $json_reply['Stock'] = array (
        'id' => $_Stock->getID(), 
        'value' => $_Stock->getValue($_Store)
    ); 

}





/*
 * 
 * [ saveNewAttribute ]
 * ______________________________________________________
 * 
 * @param: int $_POST['target_id']
 * @param: array $_POST['data']
 * 
 */


function saveNewAttribute () {

    global $_CLEAN, $json_reply;
    
    $_params = digestParams();
    $_AttributeType = Cataleya\Catalog\Product\Attribute\Type::load($_CLEAN['target_id']);
    if ($_AttributeType === NULL ) _catch_error('Attribute Type could not be found.', __LINE__, true);
    if ( empty($_params['text']) ) _catch_error('Please specify a ' . $_AttributeType->getName() . '.', __LINE__, true);
    
    // Create Attribute...
    $_Attribute = Cataleya\Catalog\Product\Attribute::create($_AttributeType, $_params['text']);
    
    
    // ADD new Attribute to product option ?
    $_Option = (isset($_params['option_id']) && !empty($_params['option_id'])) 
            ? Cataleya\Catalog\Product\Option::load ($_params['option_id'][0]) 
            : null;
    
    if ($_Option instanceof Cataleya\Catalog\Product\Option) { 
        $_Option->addAttribute ($_Attribute);
        $json_reply['AddedToOption'] = true;
        $json_reply['ProductOption'] = Cataleya\Front\Dashboard\Juicer::juice($_Option);
    } else {
        $json_reply['AddedToOption'] = false;
    }
    
    
    
    
    $json_reply['message'] = 'Option reference code saved.';
    $json_reply['Attribute'] = Cataleya\Front\Dashboard\Juicer::juice($_Attribute);

}






/*
 * 
 * [ addOptionAttribute ]
 * ______________________________________________________
 * 
 * @param: int $_POST['target_id']
 * @param: array $_POST['data']
 * 
 */


function addOptionAttribute () {

    global $_CLEAN, $json_reply;
    
    $_params = digestParams();
    $_Attribute = Cataleya\Catalog\Product\Attribute::load($_CLEAN['target_id']);
    if ($_Attribute === NULL ) _catch_error('Attribute could not be found.', __LINE__, true);
    
    
    // ADD new Attribute to product option ?
    $_Option = (isset($_params['option_id']) && !empty($_params['option_id'])) 
            ? Cataleya\Catalog\Product\Option::load ($_params['option_id'][0]) 
            : null;
    
    if ($_Option instanceof Cataleya\Catalog\Product\Option) { 
        $_Option->addAttribute ($_Attribute);
        $json_reply['ProductOption'] = Cataleya\Front\Dashboard\Juicer::juice($_Option);
    } else {
        _catch_error('Product option could not be found.', __LINE__, true);
    }
    
    
    $json_reply['message'] = $_Attribute->getAttributeType()->getName() . ' has been added.';

}










/*
 * 
 * [ saveBaseImage ]
 * ______________________________________________________
 * 
 * @param: int $_POST['target_id']
 * @param: int $_POST['data']
 * 
 */


function saveBaseImage () {

    global $_CLEAN, $json_reply;
    
    $_params = digestParams();
    

    $new_image = Cataleya\Asset\Image::load($_params['image_id']);
    if ((int)$new_image->getID() === 0) _catch_error('Image not found.' . $_params['image_id'], __LINE__, true);
    
    
    $_Product = Cataleya\Catalog\Product::load($_CLEAN['target_id']);
    if ($_Product === NULL ) 
    {
        $new_image->destroy();
        _catch_error('Your photo could not be saved. Product not be found.', __LINE__, true);
    }
    
    $_images = $_Product->getBaseImages();
    if (count($_images) > 8) {
        $new_image->destroy();
        _catch_error('Your photo could not be saved. You cannot upload more than 9 pictures for each product.', __LINE__, true);
    }
    


    $options = array(
                                    'rotate'=>$_params['rotate'], 
                                    'sw'=>$_params['sw'], 
                                    'sh'=>$_params['sh'], 
                                    'sx'=>$_params['sx'], 
                                    'sy'=>$_params['sy']
                                    ); 
    
    if (!$new_image->bake($options, TRUE)) 
    {
        $new_image->destroy();
        _catch_error('Error saving image.', __LINE__, true);
    }
    
    
   // attach image
    $_Product->addBaseImage($new_image)->saveData();
    if ($_params['set_primary']) $_Product->setPrimaryImage($new_image);
    
    
    $json_reply['message'] = 'Image saved.';
    $json_reply['Product'] = Cataleya\Front\Dashboard\Juicer::juiceDrop($_Product);
    $json_reply['Image'] = array (
                                    'id' => $new_image->getID(), 
                                    'hrefs' => $new_image->getHrefs(), 
                                    'is_primary' => ($_params['set_primary']) ? true : false
                                   );

}






/*
 * 
 * [ setPrimaryBaseImage ]
 * ______________________________________________________
 * 
 * @param: int $_POST['target_id']
 * @param: int $_POST['data']
 * 
 */


function setPrimaryBaseImage () {

    global $_CLEAN, $json_reply;
    
    $_params = digestParams();
    

    $image = Cataleya\Asset\Image::load($_params['image_id']);
    if ((int)$image->getID() === 0) _catch_error('Image not found.' . $_params['image_id'], __LINE__, true);
    
    
    $_Product = Cataleya\Catalog\Product::load($_CLEAN['target_id']);
    
    // Set primary image...

    if ($_Product != NULL) $_Product->setPrimaryImage($image);
    else _catch_error('Product not found.', __LINE__, true);



    $json_reply['message'] = 'Photo has been set as the primary product image!';
    $json_reply['product_id'] = $_CLEAN['target_id'];
    $json_reply['Image'] = array (
                                    'id' => $image->getID(), 
                                    'hrefs' => $image->getHrefs()
                                   );

}






/*
 * 
 * [ removePrimaryBaseImage ]
 * ______________________________________________________
 * 
 * @param: int $_POST['target_id']
 * @param: int $_POST['data']
 * 
 */


function removePrimaryBaseImage () {

    global $_CLEAN, $json_reply;
    
    $_params = digestParams();
    

    
    $_Product = Cataleya\Catalog\Product::load($_CLEAN['target_id']);
    
    // Delete image...

    if ($_Product != NULL) $_Product->unsetPrimaryImage();
    else _catch_error('Product not found.', __LINE__, true);


    
    $_PrimaryImage = $_Product->getPrimaryImage();

    $json_reply['message'] = 'Photo has been removed!';
    $json_reply['product_id'] = $_CLEAN['target_id'];
    $json_reply['Image'] = array (
                                    'id' => $_params['image_id'],
                                   );
    $json_reply['PrimaryImage'] = array (
                                    'id' => $_PrimaryImage->getID(), 
                                    'hrefs' => $_PrimaryImage->getHrefs()
                                   );

}







/*
 * 
 * [ deleteBaseImage ]
 * ______________________________________________________
 * 
 * @param: int $_POST['target_id']
 * @param: int $_POST['data']
 * 
 */


function deleteBaseImage () {

    global $_CLEAN, $json_reply;
    
    $_params = digestParams();
    

    $image = Cataleya\Asset\Image::load($_params['image_id']);
    if ((int)$image->getID() === 0) _catch_error('Image not found.' . $_params['image_id'], __LINE__, true);
    
    
    $_Product = Cataleya\Catalog\Product::load($_CLEAN['target_id']);
    
    // Delete image...

    if ($_Product != NULL) $_Product->removeBaseImage($image);
    else _catch_error('Product not found.', __LINE__, true);


    // Now, it is quite possible that we have just deleted the product's default photo.
    // The system should automatically set a new one. To figure out what the new one is (and inform remote client), 
    // I will reload the Product instance...
    
    $_Product = NULL;
    $_Product = Cataleya\Catalog\Product::load($_CLEAN['target_id']);
    
    $_PrimaryImage = $_Product->getPrimaryImage();

    $json_reply['message'] = 'Photo has been deleted!';
    $json_reply['product_id'] = $_CLEAN['target_id'];
    $json_reply['Image'] = array (
                                    'id' => $_params['image_id'],
                                   );
    $json_reply['PrimaryImage'] = array (
                                    'id' => $_PrimaryImage->getID(), 
                                    'hrefs' => $_PrimaryImage->getHrefs()
                                   );

}















/*
 * 
 * [ deleteProduct ]
 * ______________________________________________________
 * 
 * @param: int $_POST['target_id']
 * 
 * 
 */


function deleteProduct () {

    global $_CLEAN, $json_reply;
    $_params = digestParams();
 
    $_Product = Cataleya\Catalog\Product::load($_CLEAN['target_id']);
    if ($_Product === NULL ) _catch_error('Product could not be found.', __LINE__, true);
    
    if ($_params['auto_confirm'] !== TRUE) 
    {
        $message = 'Are you sure you want to delete this product: "' . $_Product->getDescription()->getTitle('EN') . '"?';
        confirm($message, FALSE);
    }
    

    Cataleya\Helper\DBH::getInstance()->beginTransaction();
    $_Product->delete();
    Cataleya\Helper\DBH::getInstance()->commit();
    
    $json_reply['message'] = 'Product deleted.';
    $json_reply['Product'] = array ('id'=>$_CLEAN['target_id']);

}




/*
 * 
 * [ deleteProductOption ]
 * ______________________________________________________
 * 
 * @param: int $_POST['target_id']
 * @param: int $_POST['data']
 * 
 */


function deleteProductOption () {

    global $_CLEAN, $json_reply;
    
    $_params = digestParams();
    $json_reply['ProductOptions'] = array ();
    
    

    foreach ($_params['option_id'] as $_option_id) {

        $_Option = Cataleya\Catalog\Product\Option::load($_option_id);
        if ($_Option === NULL ) _catch_error('One or more Product Options could not be found.', __LINE__, true);
    
    }   
    

    if ($_params['auto_confirm'] !== TRUE) 
    {
        $message = 'Are you sure you want to delete ' . ((count($_params['option_id']) > 1) ? 'these items' : 'this item') . ' ( ' . count($_params['option_id']) . ') ?';
        confirm($message, FALSE);
    }
    
    

    foreach ($_params['option_id'] as $_option_id) {
        

        $_Option = Cataleya\Catalog\Product\Option::load($_option_id);
        if ($_Option !== NULL ) { 
            $json_reply['ProductOptions'][] = array (
                'id' => $_Option->getID(), 
                'product_id' => $_Option->getProductId()
                    );
            $_Option->delete(); 
        }
        else _catch_error('One or more Product Options could not be found.', __LINE__, true);
    
    }   
    
    
    
    $json_reply['message'] = 'Product options deleted.';

}







/**
 * 
 * @name saveProductOptionImage
 * @param [int] $_POST['target_id']
 * @param [int] $_POST['data']
 * 
 */


function saveProductOptionImage () {

    global $_CLEAN, $json_reply;
    
    $_params = digestParams();
    

    $new_image = Cataleya\Asset\Image::load($_params['image_id']);
    if ((int)$new_image->getID() === 0) _catch_error('Image not found.' . $_params['image_id'], __LINE__, true);
    
    
    

    $options = array(
                                    'rotate'=>$_params['rotate'], 
                                    'sw'=>$_params['sw'], 
                                    'sh'=>$_params['sh'], 
                                    'sx'=>$_params['sx'], 
                                    'sy'=>$_params['sy']
                                    ); 
    
    if (!$new_image->bake($options, TRUE)) 
    {
        $new_image->destroy();
        _catch_error('Error saving image.', __LINE__, true);
    }
    
    

    $_ProductOptions = array ();
    
    foreach ($_params['option_id'] as $_option_id) {
        

        $_Option = Cataleya\Catalog\Product\Option::load($_option_id);
        
        if ($_Option === NULL ) { 
            $new_image->destroy();
            _catch_error('One or more Product Options could not be found.', __LINE__, true);
        }
        else { 
            $_ProductOptions[] = $_Option;
        }
            
        
    }
    
    
    
    
    // attach image
    
    $_image_one_saved = false;
    
    foreach ($_ProductOptions as $_Option) {

            if (!$_image_one_saved) { 
                $_Option->setPreviewImage($new_image);
                $_image_one_saved = true;
            } else {
                $_Option->setPreviewImage($new_image->duplicate());
            }
            
        
            $json_reply['ProductOptions'][] = array (
                'id' => $_Option->getID(), 
                'product_id' => $_Option->getProductId(), 
                'Image' => $_Option->getPreviewImage()->getHrefs()
                    );
    
    }   
    
    
    
    
    $json_reply['message'] = 'Image saved.';
    $json_reply['Image'] = array (
                                    'id' => $new_image->getID(), 
                                    'hrefs' => $new_image->getHrefs()
                                   );

}










/*
 * 
 * @name removeProductOptionImage
 * @param [int] $_POST['target_id']
 * @param [int] $_POST['data']
 * 
 */


function removeProductOptionImage () {

    global $json_reply;
    
    $_params = digestParams();
    


    
    foreach ($_params['option_id'] as $_option_id) {
        

        $_Option = Cataleya\Catalog\Product\Option::load($_option_id);
        
        if ($_Option === NULL ) { 
            
            _catch_error('One or more Product Options could not be found.', __LINE__, true);
        }
        else { 

            $_Option->unsetPreviewImage();
            
        
            $json_reply['ProductOptions'][] = array (
                'id' => $_Option->getID(), 
                'product_id' => $_Option->getProductId()
                    );
        }
            
        
    }
    
    
    
    
    
    $json_reply['message'] = 'Images removed.';
    $json_reply['Image'] = array (
                                    'id' => 0, 
                                    'hrefs' => Cataleya\Asset\Image::load()->getHrefs()
                                   );

}












/*
 * 
 * [ savePricePlural ]
 * ______________________________________________________
 * 
 * @param: int $_POST['target_id']
 * @param: array $_POST['data']
 * 
 * 
 */


function savePricePlural () {

    global $_CLEAN, $json_reply;
    
    $_params = digestParams();
    $_ProductOptions = array ();
    
    foreach ($_params['option_id'] as $_option_id) {
        

        $_Option = Cataleya\Catalog\Product\Option::load($_option_id);
        
        if ($_Option === NULL ) { 
            _catch_error('One or more Product Options could not be found.', __LINE__, true);
        }
        else { 
            $_ProductOptions[] = $_Option;
        }
            
        
    }
    
    
    $_Store = Cataleya\Store::load($_params['store_id']);
    if ($_Store === NULL ) _catch_error('Store not found.', __LINE__, true);
    
    
    foreach ($_ProductOptions as $_Option) {

            $_Price = $_Option->getPrice();
            if (!$_Price->setValue($_Store, $_params['text'])) _catch_error('Could not set price for one or more items.', __LINE__, true);
            
        
            $json_reply['Prices'][] = array (
                'id' => $_Price->getID(), 
                'store_id' => $_Store->getID(), 
                'value' => $_Price->getValue($_Store), 
                'formatted_value' => number_format($_Price->getValue($_Store), 2), 
                'debug_text' => $_params['text']
            ); 
    
    }   
    
    $json_reply['message'] = 'Product option price saved.';

}






/*
 * 
 * [ saveStockPlural ]
 * ______________________________________________________
 * 
 * @param: int $_POST['target_id']
 * @param: array $_POST['data']
 * 
 * 
 */


function saveStockPlural () {

    global $json_reply;
    
    $_params = digestParams();
    $_ProductOptions = array ();
    
    foreach ($_params['option_id'] as $_option_id) {
        

        $_Option = Cataleya\Catalog\Product\Option::load($_option_id);
        
        if ($_Option === NULL ) { 
            _catch_error('One or more Product Options could not be found.', __LINE__, true);
        }
        else { 
            $_ProductOptions[] = $_Option;
        }
            
        
    }
    
    
    $_Store = Cataleya\Store::load($_params['store_id']);
    if ($_Store === NULL ) _catch_error('Store not found.', __LINE__, true);

    foreach ($_ProductOptions as $_Option) {

            $_Stock = $_Option->getStock();
            if (!$_Stock->setValue($_Store, $_params['text'])) _catch_error('Could not update stock for one or more items.', __LINE__, true);
            
        
            $json_reply['Stock'][] = array (
                'id' => $_Stock->getID(), 
                'store_id' => $_Store->getID(), 
                'value' => $_Stock->getValue($_Store)
            ); 
    
    }   
    
    
    
    $json_reply['message'] = 'Product option stock saved.';

}













/*
 * 
 * [ addToProductGroup ]
 * ______________________________________________________
 * 
 * @param: int $_POST['target_id']
 * 
 * 
 */


function addToProductGroup () {

    global $_CLEAN, $json_reply;
    $_params = digestParams();
 
    $_Product = Cataleya\Catalog\Product::load($_CLEAN['target_id']);
    if ($_Product === NULL ) _catch_error('Product could not be found.', __LINE__, true);
    
    
    $_MemberProduct = Cataleya\Catalog\Product::load($_params['product_id']);
    if ($_MemberProduct === NULL ) _catch_error('Product could not be found.', __LINE__, true);
    
    if ($_Product->inProductGroup($_MemberProduct)) $json_reply['Duplicate'] = TRUE;
    else {
        $json_reply['Duplicate'] = FALSE;
        $_Product->addToProductGroup($_MemberProduct);
    }
    
    $json_reply['message'] = 'Item added.';
    $json_reply['Product'] = Cataleya\Front\Dashboard\Juicer::juiceDrop($_Product);
    $json_reply['MemberProduct'] = Cataleya\Front\Dashboard\Juicer::juiceDrop($_MemberProduct);

}








/*
 * 
 * [ removeFromProductGroup ]
 * ______________________________________________________
 * 
 * @param: int $_POST['target_id']
 * 
 * 
 */


function removeFromProductGroup () {

    global $_CLEAN, $json_reply;
    $_params = digestParams();
 
    $_Product = Cataleya\Catalog\Product::load($_CLEAN['target_id']);
    if ($_Product === NULL ) _catch_error('Product could not be found.', __LINE__, true);
    
    $_MemberProduct = Cataleya\Catalog\Product::load($_params['product_id']);
    if ($_MemberProduct === NULL ) _catch_error('Product could not be found.', __LINE__, true);
    
    $_Product->removeFromProductGroup($_MemberProduct);
    
    $json_reply['message'] = 'Item added.';
    $json_reply['Product'] = Cataleya\Front\Dashboard\Juicer::juiceDrop($_Product);
    $json_reply['MemberProduct'] = Cataleya\Front\Dashboard\Juicer::juiceDrop($_MemberProduct);

}











/*
 * 
 * [ getProductInfo ]
 * ______________________________________________________
 * 
 * @param: int $_POST['target_id']
 * 
 * 
 */


function getProductInfo () {

    global $_CLEAN, $json_reply;
 
    $_Product = Cataleya\Catalog\Product::load($_CLEAN['target_id']);
    if ($_Product === NULL ) _catch_error('Product could not be found.', __LINE__, true);
    
    $json_reply['message'] = 'Product info.';
    $json_reply['Product'] = Cataleya\Front\Dashboard\Juicer::juice($_Product);

}





/*
 * 
 * [ getProductOptionInfo ]
 * ______________________________________________________
 * 
 * @param: int $_POST['target_id']
 * 
 * 
 */


function getProductOptionInfo () {

    global $_CLEAN, $json_reply;
 
    $_ProductOption = Cataleya\Catalog\Product\Option::load($_CLEAN['target_id']);
    if ($_ProductOption === NULL ) _catch_error('Product variant could not be found.', __LINE__, true);
    
    $json_reply['message'] = 'Product option info.';
    $json_reply['ProductOption'] = Cataleya\Front\Dashboard\Juicer::juice($_ProductOption);
    $json_reply['ProductOption']['isNew'] = false;

}







/*
 * 
 * [ search ]
 * ______________________________________________________
 * 
 * @param: int $_POST['target_id']
 * 
 * 
 */


function search () {

    global $_CLEAN, $json_reply;
 
    $_params = digestParams();
    
    // First, take out funny looking characters...
    $_params['text'] = preg_replace('/[^-\s\.A-Za-z0-9@*#+]+/', '', $_params['text']);


    // Load search results as a collection of objects...
    $search_results = Cataleya\Catalog::search($_params['text'], array('show_hidden'=>TRUE));


    // $_item_num = $search_results->getPageStart();
    $json_reply['Products'] = array ();

    foreach ($search_results as $_Product) $json_reply['Products'][] = Cataleya\Front\Dashboard\Juicer::juiceDrop($_Product);
    
    $json_reply['message'] = Cataleya\Helper::countInEnglish($search_results->getPopulation(), 'Product', 'Products') . ' found.';
    $json_reply['count'] = $search_results->getPopulation();

}









// Remove token...
if (isset($_SESSION[SESSION_PREFIX.'CONFIRM_TOKEN'])) unset ($_SESSION[SESSION_PREFIX.'CONFIRM_TOKEN']);




 echo (json_encode($json_reply));
 exit();  
