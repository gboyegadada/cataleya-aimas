<?php
/*
FETCH ZONES AND RETURN AS JSON...
*/




define ('OUTPUT', 'JSON'); // Whether to output as JSON or HTML (especially errors)...


// LOAD APPLICATION TOP...
require_once ('app_top.php');






// DIE IF INPUT IS NOT VIA POST METHOD...
if ($_SERVER['REQUEST_METHOD'] != 'POST') _catch_error('POST METHOD ONLY PLEASE. NOW GET.');
	



// FILTER VALUES ....
$_CLEAN = filter_input_array(INPUT_POST, 
								array(	
                                                                                // auth_token
                                                                                'auth_token'    =>  FILTER_SANITIZE_STRIPPED

										)
								);


$zones = Cataleya\Geo\Zones::load();
if ($zones === NULL) _catch_error('Zones could not be loaded.', __LINE__, true);


$zones_info = array ();

foreach ($zones as $zone) 
{
    $zones_info[] = array (
        'id'    =>  $zone->getID(), 
        'name'  =>  $zone->getZoneName(), 
        'type'  =>  $zone->getListType(), 
        'countryPopulation' => $zone->getCountryPopulation(), 
        'provincePopulation' => $zone->getProvincePopulation()
    );
}

// OUTPUT...
$json_reply = array (
		"status"=>'ok', 
		"message"=>(($zones->getPopulation() < 1) ? 'No': $zones->getPopulation()) . ' zones found.', 
                "zones"=>$zones_info, 
                "count"=>$zones->getPopulation()
		);

echo (json_encode($json_reply));
exit();



?>