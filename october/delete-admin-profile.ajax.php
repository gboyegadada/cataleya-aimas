<?php



define ('OUTPUT', 'JSON'); // Whether to output as JSON or HTML (especially errors)...


// LOAD APPLICATION TOP...
require_once ('app_top.php');



if (!defined ('IS_ADMIN_FLAG') || !IS_ADMIN_FLAG || !defined ('CAN_DELETE_ADMIN_ACC') || !CAN_DELETE_ADMIN_ACC)  forbidden();



// ENV...
$WHITE_LIST = array(
						'auth_token', 
						'admin_id'
						);





// DIE IF INPUT IS NOT VIA POST METHOD...
if ($_SERVER['REQUEST_METHOD'] != 'POST') _catch_error('POST METHOD ONLY PLEASE. NOW GET.', __LINE__, true);

// SANITIZE INPUT DATA...
$_CLEAN = filter_input_array(INPUT_POST, array(
                                                    'auth_token'		=>	FILTER_SANITIZE_STRIPPED, 
                                                    'admin_id'	=>	FILTER_VALIDATE_INT

                                                    )
										
					);


// Check if INPUT keys are ALL expected AND MADE IT THROUGH...
if (anySuspects()) _catch_error('Error processing white_list!', __LINE__, true);



// VALIDATE AUTH TOKEN...
validate_auth_token ();


// Load account 
$AdminProfile = Cataleya\Admin\User::load($_CLEAN['admin_id']);


// Check if account  exists...
if ($AdminProfile === NULL)
{
        if (isset($_SESSION[SESSION_PREFIX.'CONFIRM_PARAMS'])) unset ($_SESSION[SESSION_PREFIX.'CONFIRM_PARAMS']);
        
        // ERROR...
        _catch_error('Account not found.', __LINE__, true);

}



// Confirm delete action (and require password)
$message = 'To delete ' . $AdminProfile->getName() . '\'s account please confirm your password...';
confirm($message, TRUE);

// Delete account  (if we make it past the 'confirm' function)...
$AdminProfile->delete();



// Remove token...
if (isset($_SESSION[SESSION_PREFIX.'CONFIRM_TOKEN'])) unset ($_SESSION[SESSION_PREFIX.'CONFIRM_TOKEN']);

// OUTPUT...
 $json_reply = array (
                 "status" => 'ok', 
                 "message" => 'Account deleted.'
                 );


 echo (json_encode($json_reply));
 exit();  

?>