<?php



define ('OUTPUT', 'HTML'); // Whether to output as JSON or HTML (especially errors)...


// LOAD APPLICATION TOP...
require_once ('app_top.php');



/*
 * 
 * CHECK USER PRIVILEGES
 * 
 */


if ((!isset($_admin_role) || !$_admin_role->hasPrivilege('VIEW_NOTIFICATIONS')) && !$adminUser->is('STORE_OWNER')  && !IS_SUPER_USER)  include_once INC_PATH.'forbidden.php';


define('READONLY', (!$_admin_role->hasPrivilege('EDIT_NOTIFICATIONS') && !IS_SUPER_USER) ? FALSE : TRUE);




// SANITIZE GET DATA...
$_CLEAN = filter_input_array(INPUT_GET, 
								array(	
										 // Page number...
										'pg'	=>	array('filter'		=>	FILTER_VALIDATE_INT, 
																'options'	=>	array('min_range' => 1, 'max_range' => 1000)
																), 
                                                                                // Page size...
										'pgsize'	=>	array('filter'		=>	FILTER_VALIDATE_INT, 
																'options'	=>	array('min_range' => 1, 'max_range' => 1000)
																),                                                                     
										
										 // order 						
										'o'	=>	array('filter'		=>	FILTER_VALIDATE_INT, 
																'options'	=>	array('min_range' => 0, 'max_range' => 100)
																), 
                                                                                // order by 						
										'ob'	=>	array('filter'		=>	FILTER_VALIDATE_INT, 
																'options'	=>	array('min_range' => 0, 'max_range' => 100)
																),    
                                                                                // index 						
										'i'	=>	array('filter'		=>	FILTER_VALIDATE_REGEXP, 
														'options'	=>	array('regexp' => '/^[A-Za-z]{1}$/')
																), 

										)
								);


// Deafault page size...
define ('DEFAULT_PAGE_SIZE', 50);


// Check if INPUT keys are ALL expected AND MADE IT THROUGH...

$_CLEAN['pg'] = empty($_CLEAN['pg']) ? 1 : (int)$_CLEAN['pg'];
$_CLEAN['i'] = empty($_CLEAN['i']) ? 'all' : $_CLEAN['i'];
$_CLEAN['pgsize'] = empty($_CLEAN['pgsize']) ? DEFAULT_PAGE_SIZE : (int)$_CLEAN['pgsize'];
$_CLEAN['o'] = empty($_CLEAN['o']) ? Cataleya\Geo\Countries::ORDER_ASC : $_CLEAN['o'];
$_CLEAN['ob'] = empty($_CLEAN['ob']) ? Cataleya\Geo\Countries::ORDER_BY_NAME : $_CLEAN['ob'];

define ('NO_OF_PAGE_INDEX_ANCHORS', 3);

define('ORDER_BY', $_CLEAN['ob']);
define('ORDER', $_CLEAN['o']);
define('INDEX', $_CLEAN['i']);
define('PAGE_SIZE', $_CLEAN['pgsize']);


$Notifications = Cataleya\Admin\Notifications::load(array('order_by'=>ORDER_BY, 'order'=>ORDER, 'index'=>INDEX), PAGE_SIZE, $_CLEAN['pg'])




////////////////////// PAGINATION /////////////





?>




<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Shop Admin | Browse Notifications (<?= Cataleya\Helper::countInEnglish($Notifications->getPopulation(), 'profile', 'profiles') ?>)</title>


<!-- JS FILES -->
<?php
require_once(INC_PATH.'js_libs.php');
?>
<script type="text/javascript" src="ui/jscript/notifications.js"></script>


<!-- STYLE SHEETS -->
<?php
require_once(INC_PATH.'css_libs.php');
?>

<link href="ui/css/indexed-paginated-list.css" rel="stylesheet" type="text/css" />
<link href="ui/css/bootmetro-tiles.css" rel="stylesheet" type="text/css" />
<link href="ui/css/notification-profiles.css" rel="stylesheet" type="text/css" />


<style type="text/css" media="all">
.content {
    height:<?= (ceil(((PAGE_SIZE > $Notifications->getPopulation()) ? $Notifications->getPopulation() : PAGE_SIZE)/3)*100)+520 ?>px;
	
}




</style>


<script type="text/javascript">


</script>


</head>

<body>

<!-- BEGIN: META -->
<input type="hidden" id="meta_token" value="<?=AUTH_TOKEN?>"  />
<input type="hidden" id="meta_admin_root" value="<?=BASE_URL?>"  />
<!-- END: META -->




<div class="container">
<?php include_once INC_PATH.'photo-upload-forms.php'; ?>
    
    
<div class="header-wrapper absolute">
    <div class="header">

    <a href="<?=BASE_URL?>#settings"><div id="page-title">Notifications</div></a><?php include_once INC_PATH.'page.nav.php'; ?> 

    </div>
    
    
<?php
        include_once INC_PATH.'main.nav.php';
?>
    
</div>


  <div class="content">



<div id="search-tool-wrapper">
		
<?php
define('QUICK_SEARCH_URL', BASE_URL.'zone_search.ajax.php');
include INC_PATH.'quick-search-widget.php';
?>



</div>




<!-- TAX CLASS INDEX -->

<ul id="list-index">
<a href="classes.tax.php?pg=0&ob=<?=$_CLEAN['ob']?>&o=<?=$_CLEAN['o']?>&i=all" <?=(($_CLEAN['i']=='all') ? 'class="button-1-active"' : '')?>><li>all</li></a>

<?php
$alpha = array('A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z');

for ($i = 0; $i < (count($alpha)); $i++) {
	$a = strtolower($alpha[$i]);
	?>
<a href="classes.tax.php?pg=0&ob=<?=$_CLEAN['ob']?>&o=<?=$_CLEAN['o']?>&i=<?=$a?>" <?=(($_CLEAN['i']==$a) ? 'class="button-1-active"' : '')?>><li><?=$alpha[$i]?></li></a>

<?php
}

?>



</ul>




<div id="hor-line-1">&nbsp;</div>


<div id="page-options-panel">
    

            <div id="page-description-panel">
                To edit notification settings...
            </div>
</div>





<!-- Notifications -->

<ul id="list-table">
    
<?php if ($Notifications->getPopulation() > (int)$_CLEAN['pgsize']): ?>
<li class="list-table-header">

        <div class="list-pagination">
        <?php if ($Notifications->getPopulation() > 0) { ?>
            Page <?=$Notifications->getPageNumber()?> of <?=$Notifications->getLastPageNum()?>
        <?php } else { ?>
        
        No notifications with name starting with '<?=strtoupper($_CLEAN['i'])?>'.
        
        <?php } ?>
        
        </div>

        <ul class="list-page-nav">
            <li><a <?php if (($Notifications->getPageNumber() < $Notifications->getLastPageNum())) { ?>href="classes.tax.php?pg=<?=$Notifications->getNextPageNum()?>&o=<?=$_CLEAN['o']?>&ob=<?=$_CLEAN['ob']?>&i=<?=$_CLEAN['i']?>"  class="button-1"<?php } else { ?>NOHREF class="button-1-no-href"<?php } ?>>next</a></li>
        

        <li><a <?php if ($Notifications->getPageNumber() > 1) { ?>href="classes.tax.php?pg=<?=$Notifications->getPrevPageNum()?>&o=<?=$_CLEAN['o']?>&ob=<?=$_CLEAN['ob']?>&i=<?=$_CLEAN['i']?>"  class="button-1"<?php } else { ?>NOHREF class="button-1-no-href"<?php } ?>>previous</a></li>
        
        </ul>

</li>
<?php endif; ?>

    <li>
        <div id="notification-profile-grid-wrapper" class="listview-container grid-layout">
            
            <?php
            foreach ($Notifications as $Notification) {

            ?>
            <a href="javascript:void(0)" class="notification-profile-tile" id="notification-profile-<?=$Notification->getID()?>" onclick="NotificationProfiles.editNotificationProfile(<?=$Notification->getID()?>); return false;">
            <div class="mediumListIconTextItem">

                    <span class="icon-broadcast-2 tile-icon-large" ></span>
                    <div class="mediumListIconTextItem-Detail">
                        <h4><?=$Notification->getDescription()->getTitle('EN')?></h4>
                        <h6>
                        <?= count_in_english($Notification->getAlert($adminUser)->getCount(), 'new notification', 'new notifications') ?>
                        </h6>
                     </div>
            </div>
            </a>

            <?php

            }
            ?>
            

            
        </div>
        
        
    </li>



</ul>







<?php
     include_once INC_PATH.'edit-notification-profile-dialog-box.php';

?>




<div id="footer">
&nbsp;
</div>



</div>

</div>


</body>
</html>
