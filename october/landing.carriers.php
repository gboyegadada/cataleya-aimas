<?php



define ('OUTPUT', 'HTML'); // Whether to output as JSON or HTML (especially errors)...


// LOAD APPLICATION TOP...
require_once ('app_top.php');


/*
 * 
 * Check user permissions...
 * 
 */

if (!IS_SUPER_USER && !$adminUser->is('STORE_OWNER')  && !$adminUser->getRole()->hasPrivilege('VIEW_CARRIER_INFO')) include_once INC_PATH.'forbidden.php';



define ('NEW_TOKEN', (md5(uniqid(rand(), TRUE))) . '-' . time()); 

// Required to create new carrier (to prevent creating one by accident)
$_SESSION[SESSION_PREFIX.'NEW_TOKEN'] = NEW_TOKEN;


$_Carriers = \Cataleya\Agent\_::load('CARRIER');

?>




<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<?php
include "includes/meta.php";

?>

<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Admin | Carriers (<?= Cataleya\Helper::countInEnglish($_Carriers->getPopulation(), 'carrier', 'carriers') ?>)</title>


<!-- JS FILES -->
<?php
require_once(INC_PATH.'js_libs.php');
?>


<!-- STYLE SHEETS -->
<?php
require_once(INC_PATH.'css_libs.php');
?>
<link href="ui/css/bootmetro-tiles.css" rel="stylesheet" type="text/css" />


<style type="text/css" media="screen">
#tiles-wrapper {
	position:absolute;
	top:140px;
	left:40px;
/*	
	border-left: dotted 1px #0ac;
	*/
	padding-top:15px!important;

}


   .mediumListIconTextItem img.mediumListIconTextItem-Image {
      border: dotted 1px #444;
      
      border-radius:7px;	
      -moz-border-radius:7px;	
      -webkit-border-radius:7px;   
   }

</style>


</head>

<body>

<div class="container">
<?php include_once INC_PATH.'photo-upload-forms.php'; ?>
    
<div class="header-wrapper absolute">
    <div class="header">

    <div id="page-title">Carriers</div><?php include_once INC_PATH.'page.nav.php'; ?>   

    </div>
    
    
<?php
        include_once INC_PATH.'main.nav.php';
?>
    
</div>


<div class="content">
  

<div id="tiles-wrapper" class="listview-container grid-layout">


    
    
 

<?php


foreach ($_Carriers as $_Carrier)
{

?>
    
     <a class="tile mediumListIconTextItem" href="<?=BASE_URL.'carrier_edit.carriers.php?d=edit&id='.$_Carrier->getID()?>" >
         <img src="<?=$_Carrier->getLogo()->getHref(Cataleya\Asset\Image::LARGE)?>" class="mediumListIconTextItem-Image" alt="60x60" style="width: 60px; height: 60px; background-color: transparent;">
        <div class="mediumListIconTextItem-Detail">
           <h4><?= $_Carrier->getCompanyName() ?></h4>
           <h6><?= $_Carrier->getCompanyName() ?></h6>
        </div>
    </a>   
    
<?php

}


?>
    
    
    
    <a class="tile mediumListIconTextItem button-4" href="<?=BASE_URL.'carrier_edit.carriers.php?d=new&t='.NEW_TOKEN?>">
            <span class="icon-new tile-icon-large" ></span>
            <div class="mediumListIconTextItem-Detail">
                <h4>New Shipping Carrier</h4>
             </div>
    </a>
        

    

    <br style="clear: both" />
    
</div>
    



</div>
</div>


</body>
</html>
