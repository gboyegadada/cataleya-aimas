<?php



define ('OUTPUT', 'JSON'); // Whether to output as JSON or HTML (especially errors)...


// LOAD APPLICATION TOP...
require_once ('app_top.php');







// ENV...
define ('PERMS', 'RW');
$WHITE_LIST = array(
						'auth_token', 
						'carrier_name', 
						'carrier_description', 
						'carrier_id', 
                                                'contact_name', 
                                                'telephone', 
                                                'email', 
                                                'active', 
    
                                                // Carrier Address
                                                'street_address', 
                                                'suburb', 
                                                'city', 
                                                'state', 
                                                'country', 
                                                'postcode'
						);





// DIE IF INPUT IS NOT VIA POST METHOD...
if ($_SERVER['REQUEST_METHOD'] != 'POST') _catch_error('POST METHOD ONLY PLEASE. NOW GET.', __LINE__, true);

// SANITIZE INPUT DATA...
$_CLEAN = filter_input_array(INPUT_POST, array(
                                                    'auth_token'		=>	FILTER_SANITIZE_STRIPPED, 
                                                    'carrier_name'		=>	FILTER_SANITIZE_STRING, 
                                                    'carrier_description'	=>	FILTER_SANITIZE_STRING, 
                                                    'carrier_id'	=>	FILTER_VALIDATE_INT, 
                                                    'contact_name'	=>	FILTER_SANITIZE_STRING, 
                                                    'telephone'	=>	FILTER_SANITIZE_STRING, 
                                                    'email'	=>	FILTER_VALIDATE_EMAIL, 
                                                    'active'	=>	FILTER_VALIDATE_INT, 
                                                    // Address
                                                    'street_address'	=>	FILTER_SANITIZE_STRING, 
                                                    'suburb'	=>	FILTER_SANITIZE_STRING, 
                                                    'city'	=>	FILTER_SANITIZE_STRING, 
                                                    'state'	=>	FILTER_SANITIZE_STRING, 
                                                    'country'	=>	FILTER_SANITIZE_STRING, 
                                                    'postcode'	=>	FILTER_SANITIZE_STRING, 
                                                    

                                                    )
										
					);




// Quick fix for email
if ($_CLEAN['email'] === FALSE) $_CLEAN['email'] = '';
if ($_CLEAN['active'] === FALSE) $_CLEAN['active'] = '';

// Check if INPUT keys are ALL expected AND MADE IT THROUGH...
$_suspect = anySuspects();

if ($_suspect) _catch_error('Error processing white_list! ' . $_suspect, __LINE__, true);

foreach ($_CLEAN as $k=>$v)
{
    if ($v === FALSE) $_CLEAN[$k] = '';
}


// VALIDATE AUTH TOKEN...
validate_auth_token ();




// Load carrier
$carrier = Cataleya\Agent::load($_CLEAN['carrier_id']);
if ($carrier === NULL) _catch_error('Unable to load carrier info.', __LINE__, true);


// Set description...
if ($_CLEAN['carrier_name'] !== '' || $_CLEAN['carrier_description'] !== '')
{
    
    if ($_CLEAN['carrier_name'] != '') $carrier->setCompanyName($_CLEAN['carrier_name'], 'EN');
    // if ($_CLEAN['carrier_description'] != '') $carrier_description->setText($_CLEAN['carrier_description'], 'EN');

}


// Carrier Info


if ($_CLEAN['contact_name'] != '') $carrier->setContactName ($_CLEAN['contact_name']);
if ($_CLEAN['telephone'] != '') $carrier->setTelephone ($_CLEAN['telephone']);
if ($_CLEAN['email'] != '') $carrier->setEmail ($_CLEAN['email']);
if ($_CLEAN['active'] != '') $carrier->setActive ($_CLEAN['active']);


// Save address changes...

$carrier_location = $carrier->getContact();
if ($carrier_location !== NULL)
{

if ($_CLEAN['street_address'] != '') $carrier_location->setEntryStreetAddress($_CLEAN['street_address']);
if ($_CLEAN['suburb'] != '') $carrier_location->setEntrySuburb($_CLEAN['suburb']);
if ($_CLEAN['city'] != '') $carrier_location->setEntryCity($_CLEAN['city']);
if ($_CLEAN['state'] != '') $carrier_location->setEntryState($_CLEAN['state']);
if ($_CLEAN['postcode'] != '') $carrier_location->setEntryPostcode($_CLEAN['postcode']);
if ($_CLEAN['country'] != '') $carrier_location->setEntryCountryCode($_CLEAN['country']);

}




// OUTPUT...
$json_reply = array (
		"status"=>'ok', 
		"message"=>'Info saved!'
		);

echo (json_encode($json_reply));
exit();





?>
