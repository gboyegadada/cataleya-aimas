<?php

define ('OUTPUT', 'HTML'); // Whether to output as JSON or HTML (especially errors)...


// LOAD APPLICATION TOP...
require_once ('app_top.php');



/*
 * 
 * TO VIEW OR EDIT THIS PAGE, USER MUST BE AN ADMINISTRATOR
 * 
 */


if (!IS_SUPER_USER)  include_once INC_PATH.'forbidden.php';


    // Load Configurations
    $configurations = new Cataleya\System\Configurations();
?>





<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<?php
include "includes/meta.php";

?>

<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Settings | Configurations (<?= Cataleya\Helper::countInEnglish($configurations->getPopulation(), 'profile', 'profiles') ?>)</title>

<?php
require_once(INC_PATH.'js_libs.php');
?>
<script type="text/javascript" src="ui/jscript/libs/jquery.form.js"></script>
<script type="text/javascript" src="ui/jscript/configurations.settings.js"></script>



<?php
require_once(INC_PATH.'css_libs.php');
?>

<link href="ui/css/configurations.edit.css" rel="stylesheet" type="text/css" />


<style type="text/css" media="screen">


#page_h_tweak {
	position:absolute;
	top:1000px;
	left:0px;
	
	width:1px;
	height:1px;
}



#tile_grid {
	position:absolute;
	top:160px;
	left:350px;
	
	height:600px;
	width:590px;
	
	border:solid 1px #222;

	display:none;

}



</style>

<script type="text/javascript">


</script>


</head>

<body>
<!-- BEGIN: META -->
<input type="hidden" id="meta_token" value="<?=AUTH_TOKEN?>"  />
<input type="hidden" id="meta_admin_root" value="<?=BASE_URL?>"  />

<!-- END: META -->




<div class="container">
<?php include_once INC_PATH.'photo-upload-forms.php'; ?>
    
<div class="header-wrapper absolute">
    <div class="header">

    <a href="<?=BASE_URL?>#settings"><div id="page-title">Configurations</div></a>   

    </div>
    
<?php include_once INC_PATH.'main.nav.php'; ?>
    
</div>

<div class="content">




<ul id="sidebar">
    <li>
        
    </li>
    
    
</ul>


<div id="configuration-list-outer-wrapper" >

    <ul id="configuration-list-wrapper">
        

        
<?php


   
    
    foreach ($configurations as $config) 
    {
        $_config_id = $config->getID();
        $_plugin_handle = substr($_config_id, strlen('plugins.'));
        $css_id = str_replace('.', '-', $config->getID());
        
?>

        <li id="config-wrapper-<?=$css_id?>">
            <a href="javascript:void(0);" onclick="expandConfig('config-wrapper-<?=$css_id?>'); return false;">
                <h3 class="section-header icon-settings"><?= $config->name ?>&nbsp;&nbsp;<span class="config-edit-icon icon-pencil"></span></h3>
            </a>
            <div class="config-description" style="display:block" >
                <small class="text-input-label"><?= $config->description ?></small>
            </div>
            
            <form id="config-nvp-form-<?=$css_id?>" name="config-nvp-form-<?=$css_id?>" method="POST" action="<?=BASE_URL?>save-config.ajax.php" >
            <div class="nvp-wrapper" id="nvp-wrapper-<?=$css_id?>">
            <input type="hidden" name="auth_token" value="<?=AUTH_TOKEN?>" />
            <input type="hidden" name="config_id" value="<?=$config->getID()?>" />
 <?php



        if (strpos($_config_id, 'plugins.', 0) !== false): 

            $_params = \Cataleya\Plugins\Action::trigger('*.get-config-form-controls', [], [ $_plugin_handle ]);
            $_ = ["!","*"];

            foreach ($_params as $grp_handle=>$grp) 
            {
                if (isset($grp['field_group']) && $grp['field_group'] === true) 
                {
?>

                <div class="field-group-heading"><?= $grp['title'] ?></div>

<?php
                
                foreach ($grp['fields'] as $field) {

                    if (isset($field['name'])) {
                        $_n = (in_array(substr($field['name'],-1), $_)) ? substr($field['name'],0,-1) : $field['name'];
                        $field['name'] = "param__".AUTH_TOKEN."__".$_n;
                    }
                    else $field['name'] = "param__".AUTH_TOKEN."__noname";

                    $field['id'] = "text-field-".$css_id."-name";
                    $field['classes'] = "placeholder-color-1";


?>
                <small class="text-input-label"><?= strtoupper($field['label']) ?></small>
                <?php \Cataleya\Helper::makeFormControl($field, true); ?>
                <br/><br/>

<?php

                }

                } else {


                $field = $grp;

                if (isset($field['name'])) {
                    $_n = (in_array(substr($field['name'],-1), $_)) ? substr($field['name'],0,-1) : $field['name'];
                    $field['name'] = "param__".AUTH_TOKEN."__".$_n;
                }
                else $field['name'] = "param__".AUTH_TOKEN."__noname";

                $field['id'] = "text-field-".$css_id."-name";
                $field['classes'] = "placeholder-color-1";


?>
                <small class="text-input-label"><?= strtoupper($field['label']) ?></small>
                <?php \Cataleya\Helper::makeFormControl($field, true); ?>
                <br/><br/>

<?php



                }


            }
 


        else:



            foreach ($config as $name=>$value) 
            {
                
                if ($name == 'name' || $name == 'description') continue;

            ?>
                <small class="text-input-label"><?= strtoupper($name) ?></small>
                       <input id="text-field-<?=$css_id?>-name" class="placeholder-color-1" name="param__<?=AUTH_TOKEN?>__<?= $name ?>" type="text" value="<?= $value ?>" tinyStatus="#<?=$css_id?>-status" size=30 />
                       <small class="tiny-status" id="<?=$css_id?>-status">&nbsp;</small>
                       <br/>
            <?php

           }

       endif; 
    ?>
            
            <a class="button-2" href="javascript:void(0);" onClick="saveConfig('config-nvp-form-<?=$css_id?>'); return false;" >
               save configuration
            </a>  
             <br /><br /><br />
             
            
            </div>
            </form> 
                   
        </li>

 <?php
 
    }
    
    ?>
        
        
        
        

        

    
    </ul>



    
</div>        


<!--

<div id="footer">
&nbsp;
</div>

-->
  </div>
</div>



<div id="page_h_tweak">&nbsp;</div>

</body>
</html>
