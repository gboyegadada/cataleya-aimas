<?php
/*
FETCH ZONES AND RETURN AS JSON...
*/




define ('OUTPUT', 'JSON'); // Whether to output as JSON or HTML (especially errors)...


// LOAD APPLICATION TOP...
require_once ('app_top.php');





/*
 * 
 * TO PERFORM THIS OPERATION, USER MUST BE AN ADMINISTRATOR
 * 
 */


if (!IS_SUPER_USER && !$adminUser->is('STORE_OWNER')  && !$_admin_role->hasPrivilege('EDIT_ZONES')) _catch_error('You do have permission to perform this action.', __LINE__, true);




// DIE IF INPUT IS NOT VIA POST METHOD...
if ($_SERVER['REQUEST_METHOD'] != 'POST') _catch_error('POST METHOD ONLY PLEASE. NOW GET.');
	

$WHITE_LIST = array(
						'auth-token',  
                                                'zone-name', 
                                                'list-type', 
                                                'zone-id'
					);

// FILTER VALUES ....
$_CLEAN = filter_input_array(INPUT_POST, 
								array(	
                                                                                // auth_token
                                                                                'auth-token'    =>  FILTER_SANITIZE_STRIPPED, 
                                                                    
										 // zone id...
										'zone-name'	=>	array('filter'		=>	FILTER_VALIDATE_REGEXP, 
														'options'	=>	array('regexp' => '/^[A-Za-z0-9,\-\W]{2,400}$/')
																), 
                                                                    
										 // list type...
										'list-type'	=>	array('filter'		=>	FILTER_VALIDATE_REGEXP, 
														'options'	=>	array('regexp' => '/^(normal|exclude)$/')
																), 
                                                                    
										 // zone_id (>0) for save, (0) for create...
										'zone-id'	=>	FILTER_VALIDATE_INT

										)
								);



// Check if INPUT keys are ALL expected AND MADE IT THROUGH...
if (anySuspects()) _catch_error('Error processing white_list!', __LINE__, true);


// New zone ?
if ((int)$_CLEAN['zone-id'] === 0) 
{
   $zone = Cataleya\Geo\Zone::create($_CLEAN['zone-name']);
   if ($zone === NULL) _catch_error('Zone could not be created.', __LINE__, true);
}

// Save zone ?
else if ((int)$_CLEAN['zone-id'] > 0) 
{
   $zone = Cataleya\Geo\Zone::load($_CLEAN['zone-id']);
   if ($zone === NULL) _catch_error('Zone could not be loaded.', __LINE__, true);
   
   $zone->setListType($_CLEAN['list-type']);
   $zone->setZoneName($_CLEAN['zone-name']);
   
}


$zone_info = array (
        'id'    =>  $zone->getID(), 
        'name'  =>  $zone->getZoneName(), 
        'type'  =>  $zone->getListType(),
        'countryPopulation' => $zone->getCountryPopulation(), 
        'provincePopulation' => $zone->getProvincePopulation(), 
        'isNew'   =>  ((int)$_CLEAN['zone-id'] === 0) ? TRUE : FALSE
    );



// OUTPUT...
$json_reply = array (
		"status"=>'ok', 
		"message"=> ((int)$_CLEAN['zone-id'] === 0) ? 'New zone created.' : 'Zone saved.', 
                "zone"=>$zone_info
		);

echo (json_encode($json_reply));
exit();



?>
