<?php





define ('OUTPUT', 'HTML'); // Whether to output as JSON or HTML (especially errors)...


// LOAD APPLICATION TOP...
require_once ('app_top.php');







/*
 *
 * CHECK USER PRIVILEGES
 *
 */


if ((!isset($_admin_role) || !$_admin_role->hasPrivilege('VIEW_CATALOG')) && !IS_SUPER_USER) forbidden();


define('READONLY', (!$_admin_role->hasPrivilege('EDIT_CATALOG') && !IS_SUPER_USER) ? FALSE : TRUE);





// SANITIZE GET DATA...
$_CLEAN = filter_input_array(INPUT_GET,
								array(


										 // collection
										'coll'	=>	array('filter'		=>	FILTER_VALIDATE_INT,
																'options'	=>	array('min_range' => 0, 'max_range' => 1000)
																),
										 // id
										'id'	=>	array('filter'		=>	FILTER_VALIDATE_INT,
																'options'	=>	array('min_range' => 0, 'max_range' => 1000)
																),
                                                                                // sid
										'sid'	=>	array('filter'		=>	FILTER_VALIDATE_INT,
																'options'	=>	array('min_range' => 0, 'max_range' => 1000)
																),
										 // Page number...
										'pg'	=>	array('filter'		=>	FILTER_VALIDATE_INT,
																'options'	=>	array('min_range' => -1, 'max_range' => 1000)
																),
                                                                                // Page size...
										'pgsize'	=>	array('filter'		=>	FILTER_VALIDATE_INT,
																'options'	=>	array('min_range' => 1, 'max_range' => 1000)
															),
										 // Offset (product ID)...
										'os'	=>	array('filter'		=>	FILTER_VALIDATE_INT,
																),
										 // Index Number...
										'inum'	=>	array('filter'		=>	FILTER_VALIDATE_INT,
																),

										 // order
										'o'	=>	array('filter'		=>	FILTER_VALIDATE_INT,
																'options'	=>	array('min_range' => 0, 'max_range' => 100)
																),
                                                                                // order by
										'ob'	=>	array('filter'		=>	FILTER_VALIDATE_INT,
																'options'	=>	array('min_range' => 0, 'max_range' => 100)
																),
                                                                                // index
										'i'	=>	array('filter'		=>	FILTER_VALIDATE_REGEXP,
														'options'	=>	array('regexp' => '/^[A-Za-z]{1}$/')
																),

										)
								);


// Deafault page size...
define ('DEFAULT_PAGE_SIZE', 23);

// collections
define('COLL_CATALOG', 0);
define('COLL_CATEGORY', 1);
define('COLL_SALE', 2);
define('COLL_COUPON', 3);
define('COLL_STORE', 4);


// Check if INPUT keys are ALL expected AND MADE IT THROUGH...

$_CLEAN['coll'] = empty($_CLEAN['coll']) ? 0 : (int)$_CLEAN['coll'];
$_CLEAN['id'] = empty($_CLEAN['id']) ? 0 : (int)$_CLEAN['id'];
$_CLEAN['sid'] = empty($_CLEAN['sid']) ? 0 : (int)$_CLEAN['sid'];
$_CLEAN['pg'] = empty($_CLEAN['pg']) ? 0 : (int)$_CLEAN['pg'];
$_CLEAN['os'] = empty($_CLEAN['os']) ? 0 : (int)$_CLEAN['os'];
$_CLEAN['inum'] = empty($_CLEAN['inum']) ? 1 : (int)$_CLEAN['inum'];
$_CLEAN['i'] = empty($_CLEAN['i']) ? 'all' : $_CLEAN['i'];
$_CLEAN['pgsize'] = empty($_CLEAN['pgsize']) ? DEFAULT_PAGE_SIZE : (int)$_CLEAN['pgsize'];
$_CLEAN['o'] = empty($_CLEAN['o']) ? Cataleya\Catalog::ORDER_ASC : $_CLEAN['o'];
$_CLEAN['ob'] = empty($_CLEAN['ob']) ? Cataleya\Catalog::ORDER_BY_TITLE : $_CLEAN['ob'];

define ('NO_OF_PAGE_INDEX_ANCHORS', 3);

define('ORDER_BY', $_CLEAN['ob']);
define('ORDER', $_CLEAN['o']);
define('INDEX', $_CLEAN['i']);
define('PAGE_SIZE', $_CLEAN['pgsize']);

$_SelectedStore = ($_CLEAN['sid'] > 0) ? Cataleya\Store::load($_CLEAN['sid']) : NULL;


define('ACTIVE_HREF', "landing.catalog.php?pg=".$_CLEAN['pg']."&os=".$_CLEAN['os']."&inum=".$_CLEAN['inum']."&ob=".$_CLEAN['ob']."&o=".$_CLEAN['o']."&i=".$_CLEAN['i']."&sid=".$_CLEAN['sid']);




switch ($_CLEAN['coll'])
{
    case COLL_CATALOG:
        $Catalog = Cataleya\Catalog::load(array('Store'=>$_SelectedStore), array('order_by'=>ORDER_BY, 'order'=>ORDER, 'index'=>INDEX, 'show_hidden'=>TRUE), PAGE_SIZE, $_CLEAN['os'], $_CLEAN['pg']);
        break;

    case COLL_CATEGORY:

        $Catalog = Cataleya\Catalog\Tag::load($_CLEAN['id'], array('Store'=>$_SelectedStore), array('order_by'=>ORDER_BY, 'order'=>ORDER, 'index'=>INDEX, 'show_hidden'=>TRUE), PAGE_SIZE, $_CLEAN['os'], $_CLEAN['pg']);
        if (!empty($Catalog)) break;

//    case COLL_STORE:
//        $_Store = Cataleya\Store::load($_CLEAN['id']);
//        $Catalog = Cataleya\Catalog::load(array('Store'=>$_Store, 'order_by'=>ORDER_BY, 'order'=>ORDER, 'index'=>INDEX, 'show_hidden'=>TRUE), PAGE_SIZE, $_CLEAN['os'], $_CLEAN['pg']);
//        if (!empty($Catalog)) break;

    case COLL_SALE:
        $Catalog = Cataleya\Sales\Sale::load($_CLEAN['id'], array('Store'=>$_SelectedStore), array('order_by'=>ORDER_BY, 'order'=>ORDER, 'index'=>INDEX), PAGE_SIZE, $_CLEAN['os'], $_CLEAN['pg']);
        if (!empty($Catalog)) break;

    case COLL_COUPON:
        $Catalog = Cataleya\Sales\Coupon::load($_CLEAN['id'], array('Store'=>$_SelectedStore), array('order_by'=>ORDER_BY, 'order'=>ORDER, 'index'=>INDEX), PAGE_SIZE, $_CLEAN['os'], $_CLEAN['pg']);
        if (!empty($Catalog)) break;

    default:
        $Catalog = Cataleya\Catalog::load(array('Store'=>$_SelectedStore), array('order_by'=>ORDER_BY, 'order'=>ORDER, 'index'=>INDEX, 'show_hidden'=>TRUE), PAGE_SIZE, $_CLEAN['os'], $_CLEAN['pg']);
        break;

}


define ('NEW_PROD_TOKEN', (md5(uniqid(rand(), TRUE))) . '-' . time());

if (in_array($_CLEAN['coll'], array(COLL_CATEGORY, COLL_SALE, COLL_COUPON))) {
    define ('PRODUCT_COUNT', $Catalog->getPopulation());
} else {
    define ('PRODUCT_COUNT', Cataleya\System::load()->getProductCount());
}

define ('NEXT_OFFSET', (int)$Catalog->getPageOffset());
define ('OFFSET', $_CLEAN['os']);
define ('ITEM_NUM_START', $_CLEAN['inum']);
define ('ITEM_NUM_START_NEXT', ( (NEXT_OFFSET !== (int)$_CLEAN['os']) ? (ITEM_NUM_START + PAGE_SIZE) : $_CLEAN['inum'] ));
define ('ITEM_NUM_START_PREV', ( (ITEM_NUM_START > PAGE_SIZE) ? (ITEM_NUM_START - PAGE_SIZE) : 1 ));

$_SESSION[SESSION_PREFIX.'NEW_PROD_TOKEN'] = NEW_PROD_TOKEN;
$_SESSION[SESSION_PREFIX.'NEW_TAG_TOKEN'] = NEW_PROD_TOKEN;




/*
 *
 * PREPARE DATA FOR CONTEXTUAL MENU
 *
 */

$PAGE_MENU = array ();

// Categories
$PAGE_MENU[] = array (
    'title' =>  'View Categories',
    'icon'  =>  'icon-list',
    'href'  =>  BASE_URL.'landing.categories.php'
);






?>


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<?php
include "includes/meta.php";

?>

<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Shop Admin | Catalog (<?= Cataleya\Helper::countInEnglish($Catalog->getPopulation()) ?>)</title>

<!-- JS FILES -->
<?php
require_once(INC_PATH.'js_libs.php');
?>
<script type="text/javascript" src="ui/jscript/products.catalog.js"></script>
<script type="text/javascript" src="ui/jscript/libs/flotr/flotr2.min.js"></script>
<script type="text/javascript" src="ui/jscript/libs/quill.min.js"></script>

<!-- STYLESHEETS -->
<?php
require_once(INC_PATH.'css_libs.php');
?>

<link href="ui/css/indexed-paginated-list.css" rel="stylesheet" type="text/css" />
<link href="ui/css/bootmetro-tiles.css" rel="stylesheet" type="text/css" />
<link href="ui/css/products.catalog.css" rel="stylesheet" type="text/css" />
<link href="ui/css/quill.base.css" rel="stylesheet" type="text/css" />
<link href="ui/css/quill.snow.css" rel="stylesheet" type="text/css" />
<link href="ui/css/product.dialog.css" rel="stylesheet" type="text/css" />
<link href="ui/css/stats.css" rel="stylesheet" type="text/css" />


<style type="text/css" media="screen">

.content {
    height:<?= (ceil(((PAGE_SIZE > PRODUCT_COUNT) ? PRODUCT_COUNT+1 : PAGE_SIZE)/3)*350)+720 ?>px;


}

#tiles-wrapper {
    margin: 10px auto;
    text-align:center;
}





</style>





</head>

<body>


<!-- BEGIN: META -->
<input type="hidden" id="meta_token" value="<?=AUTH_TOKEN?>"  />
<input type="hidden" id="meta_admin_root" value="<?=BASE_URL?>"  />
<!-- END: META -->




<!-- RECYCLE BIN (DROPPABLE) -->
<div id="main-recycle-bin-wrapper">
<div id="main-recycle-bin">

&nbsp;

</div>

</div>




<div class="container">
<?php include_once INC_PATH.'photo-upload-forms.php'; ?>



<div class="header-wrapper absolute">
    <div class="header">

    <a href="<?=BASE_URL?>"><div id="page-title">Catalog</div></a>

    </div>

<?php include_once INC_PATH.'main.nav.php'; ?>

</div>



<div class="content">
<?php include_once INC_PATH.'photo-upload-forms.php'; ?>

<div id="search-tool-wrapper">

<?php
define('QUICK_SEARCH_URL', 'product-search.ajax.php');
include INC_PATH.'quick-search-widget.php';
?>




</div>



<!-- TAX CLASS INDEX -->

<ul id="list-index">
<a href="landing.catalog.php?coll=<?=$_CLEAN['coll']?>&sid=<?=$_CLEAN['sid']?>&id=<?=$_CLEAN['id']?>&pg=0&os=0&ob=<?=$_CLEAN['ob']?>&o=<?=$_CLEAN['o']?>&i=all" <?=(($_CLEAN['i']=='all') ? 'class="button-1-active"' : '')?>><li>all</li></a>

<?php
$alpha = array('A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z');

for ($i = 0; $i < (count($alpha)); $i++) {
	$a = strtolower($alpha[$i]);
	?>
<a href="landing.catalog.php?coll=<?=$_CLEAN['coll']?>&sid=<?=$_CLEAN['sid']?>&id=<?=$_CLEAN['id']?>&pg=0&ob=<?=$_CLEAN['ob']?>&o=<?=$_CLEAN['o']?>&i=<?=$a?>" <?=(($_CLEAN['i']==$a) ? 'class="button-1-active"' : '')?>><li><?=$alpha[$i]?></li></a>

<?php
}

?>



</ul>




<div id="hor-line-1">&nbsp;</div>


<ul id="catalog-context-menu" class="context-menu-wrapper" >
    <span class="bubble-arrow">&nbsp;</span>


    <a class="" href="javascript:void(0);" onclick="Catalog.addToCategory()" >
    <li><span class="button-label">Add to Category</span><span class="icon-plus button-icon"></span></li>
    </a>

    <a class="" href="javascript:void(0);" onclick="Catalog.removeFromCategory()" >
    <li><span class="button-label">Remove from Category</span><span class="icon-minus button-icon"></span></li>
    </a>

    <li class="divider"></li>

    <a class="" href="javascript:void(0);" onclick="Catalog.applyCoupon()" >
    <li><span class="button-label">Apply Coupon</span><span class="icon-plus button-icon"></span></li>
    </a>


    <a class="" href="javascript:void(0);" onclick="Catalog.removeCoupon()" >
    <li><span class="button-label">Remove Coupon</span><span class="icon-minus button-icon"></span></li>
    </a>


    <li class="divider"></li>

    <a class="" href="javascript:void(0);" onclick="Catalog.addToSale()" >
    <li><span class="button-label">Add to Sale</span><span class="icon-plus button-icon"></span></li>
    </a>

    <a class="" href="javascript:void(0);" onclick="Catalog.removeFromSale()" >
    <li><span class="button-label">Remove from Sale</span><span class="icon-minus button-icon"></span></li>
    </a>


    <li class="divider"></li>

    <a class="" href="javascript:void(0);" onclick="Catalog.addToStore()" >
    <li><span class="button-label">Add to Store</span><span class="icon-plus button-icon"></span></li>
    </a>

    <a class="" href="javascript:void(0);" onclick="Catalog.removeFromStore()" >
    <li><span class="button-label">Remove from Store</span><span class="icon-minus button-icon"></span></li>
    </a>

    <li class="divider"></li>

    <a class="" href="javascript:void(0);" onclick="Catalog.toggleFeaturedStatus(true)" >
    <li><span class="button-label">Add to Featured</span><span class="icon-plus button-icon"></span></li>
    </a>

    <a class="" href="javascript:void(0);" onclick="Catalog.toggleFeaturedStatus(false)" >
    <li><span class="button-label">Remove from Featured</span><span class="icon-minus button-icon"></span></li>
    </a>

</ul>





<ul id="list-table">

<?php if (PRODUCT_COUNT > (int)$_CLEAN['pgsize']): ?>
<li class="list-table-header">

        <div class="list-pagination">
        <?php if (PRODUCT_COUNT > 0) { ?>
            Showing <?=ITEM_NUM_START?> to <?= ITEM_NUM_START+($Catalog->getActualPageSize()-1) ?> of <?= PRODUCT_COUNT ?> item<?= (PRODUCT_COUNT > 1) ? 's' : ''  ?>
        <?php } else { ?>

        No items with name starting with '<?=strtoupper($_CLEAN['i'])?>'.

        <?php } ?>

        </div>

        <ul class="list-page-nav">
            <li><a <?php if (PRODUCT_COUNT > PAGE_SIZE) { ?>href="landing.catalog.php?coll=<?=$_CLEAN['coll']?>&id=<?=$_CLEAN['id']?>&pg=1&os=<?=NEXT_OFFSET?>&inum=<?=ITEM_NUM_START_NEXT?>&o=<?=$_CLEAN['o']?>&ob=<?=$_CLEAN['ob']?>&i=<?=$_CLEAN['i']?>"  class="button-1"<?php } else { ?>NOHREF class="button-1-no-href"<?php } ?>>next</a></li>


            <li><a <?php if (PRODUCT_COUNT > PAGE_SIZE) { ?>href="landing.catalog.php?coll=<?=$_CLEAN['coll']?>&id=<?=$_CLEAN['id']?>&pg=-1&os=<?=OFFSET?>&inum=<?=ITEM_NUM_START_PREV?>&o=<?=$_CLEAN['o']?>&ob=<?=$_CLEAN['ob']?>&i=<?=$_CLEAN['i']?>"  class="button-1"<?php } else { ?>NOHREF class="button-1-no-href"<?php } ?>>previous</a></li>

        </ul>

</li>
<?php endif; ?>


<li id="task-bar">
    <div id="global-select-button" class="circular-button fltlft" >
    <span class="icon-checkmark"></span><span class="button-label">Select All</span>
    </div>

    <div id="global-delete-button" class="circular-button fltlft">
    <span class="icon-trash-fill"></span><span class="button-label">Delete</span>
    </div>

    <div id="global-hide-button" class="circular-button fltlft">
    <span class="icon-minus-5"></span><span class="button-label">Hide</span>
    </div>

    <div id="global-show-button" class="circular-button fltlft" >
    <span class="icon-eye-4"></span><span class="button-label">Show</span>
    </div>

    <div id="task-bar-menu-button" class="blue-button large fltrt" onclick="Catalog.toggleContextMenu();" >do something with selected items <span class="icon-grid">&nbsp;</span> <span class="count"></span></div>
</li>



<li id="catalog-side-bar-wrapper">


<!-- SIDE BAR -->

<ul id="catalog-side-bar">




    <!-- STORES -->

    <a href="javascript:void(0);" onclick="Catalog.toggleSideBarSection(this)" >
    <li class="sb-section-header">
        STORES <span class="<?=($_CLEAN['coll'] !== COLL_STORE) ? 'icon-plus-3' : 'icon-minus-3'?> fltrt"></span>
    </li>
    </a>
    <li class="sb-section-body-wrapper" style="">
        <ul class="sb-section-body">


            <li id="store-tile-0" class="tile <?= (empty($_SelectedStore)) ? 'current' : '' ?>">
                <a class="fltlft main-anchor" href="landing.catalog.php?coll=<?=$_CLEAN['coll']?>&pg=0&ob=<?=$_CLEAN['ob']?>&o=<?=$_CLEAN['o']?>&i=<?=$_CLEAN['i']?>&sid=0&id=<?= $_CLEAN['id'] ?>">All (<span id="category-population-all"><?= Cataleya\System::load()->getProductCount() ?></span>)

            </li>



            <?php

            $Stores = Cataleya\Stores::load();
            foreach ($Stores as $Store):
            ?>
            <li id="store-tile-<?= $Store->getID() ?>" data_add_to="store" data_target_id="<?= $Store->getID() ?>" class="droppable-tile tile user-status <?= ($Store->isActive()) ? 'online' : 'offline' ?> <?= ((int)$_CLEAN['sid'] === (int)$Store->getID()) ? 'current' : '' ?>">
            <a class="fltlft main-anchor" href="landing.catalog.php?coll=<?=$_CLEAN['coll']?>&pg=0&ob=<?=$_CLEAN['ob']?>&o=<?=$_CLEAN['o']?>&i=<?=$_CLEAN['i']?>&sid=<?= $Store->getID() ?>&id=<?= $_CLEAN['id'] ?>">

                <?= $Store->getDescription()->getTitle('EN') ?>
                (<span id="store-population-<?= $Store->getID() ?>"><?= $Store->getPopulation() ?></span>)
                <span class="color-label icon <?= $Store->getLabelColor() ?> icon-star-3 fltrt"></span>
            </a>

            </li>
            <?php endforeach; ?>


            <li id="product-import-tile"><br /><br /><a class="button-1" href="javascript:void(0);" onclick="Catalog.newImport();" >Import Products</a></li>
            <br class=" clearfix" />
        </ul>
    </li>





    <!-- CATEGORIES -->

    <a href="javascript:void(0);" onclick="Catalog.toggleSideBarSection(this)" >
    <li class="sb-section-header">
        CATEGORIES <span class="<?=($_CLEAN['coll'] !== COLL_CATEGORY && $_CLEAN['coll'] !== COLL_CATALOG) ? 'icon-plus-3' : 'icon-minus-3'?> fltrt"></span>
    </li>
    </a>
    <li class="sb-section-body-wrapper" style="<?=($_CLEAN['coll'] !== COLL_CATEGORY && $_CLEAN['coll'] !== COLL_CATALOG) ? 'display: none;' : ''?>">
        <ul class="sb-section-body category-section-wrap">
            <li id="category-tile-0" class="tile <?= ($_CLEAN['coll'] === COLL_CATALOG && $_CLEAN['id'] === 0) ? 'current' : '' ?>">
                <a class="fltlft main-anchor" href="<?=ACTIVE_HREF?>&coll=<?=COLL_CATALOG?>&id=0">All (<span id="category-population-all"><?= ($_SelectedStore !== NULL) ? $_SelectedStore->getPopulation() : Cataleya\System::load()->getProductCount() ?></span>)

            </li>

            <?php
            $Categories = Cataleya\Catalog\Tags::load(array('Store'=>$_SelectedStore, 'show_hidden'=>TRUE));
            foreach ($Categories as $Category):
                if ($Category->isRoot()) continue;

                // Malformed Category: delete and continue
                if(!$Category->isRoot() && !$Category->hasParent()) { $Category->delete(); continue; }
                // $_Store = $Category->getStore();
            ?>
            <li id="category-tile-<?= $Category->getID() ?>" data_add_to="category" data_target_id="<?= $Category->getID() ?>" class="droppable-tile tile <?= ($_CLEAN['coll'] === COLL_CATEGORY && $_CLEAN['id'] === (int)$Category->getID()) ? 'current' : '' ?>">

            <a class="fltlft main-anchor" href="<?=ACTIVE_HREF?>&coll=<?=COLL_CATEGORY?>&id=<?= $Category->getID() ?>">
                   <span class="color-label icon <?= $Category->getStore()->getLabelColor() ?> icon-folder-fill fltlft"></span>
                   <?php

                   $parent = $Category->getParent();
                   if (!$parent->isRoot()) echo $Category->getParent()->getDescription()->getTitle('EN')." / ";

                   ?>
                   <?= $Category->getDescription()->getTitle('EN') ?> (<span id="category-population-<?= $Category->getID() ?>"><?= $Category->getPopulation() ?></span>)

            </a>


            <div class="dashboard">
            <a href="javascript:void(0)" onclick="Category.editCategory(<?= $Category->getID() ?>); return false;" ><span class="icon-pencil-2"></span> </a>
            <a href="javascript:void(0)" onclick="Category.deleteCategory(<?= $Category->getID() ?>);" ><span class="icon-close"></span></a>
            </div>
            </li>
            <?php endforeach; ?>
            <?php if ($Categories->getPopulation() === 1): ?>
            <li><p class='notice'>There are no categories for this store yet. Click 'New Category' to add one...</p></li>
            <?php endif; ?>

            <li id="new-category-tile"><br /><br /><a class="button-1" href="javascript:void(0);" onclick="Catalog.newCategory();" >New Category</a></li>
            <br class=" clearfix" />
        </ul>
    </li>







    <!-- SALES -->

    <a href="javascript:void(0);" onclick="Catalog.toggleSideBarSection(this)" >
    <li class="sb-section-header">
        SALES <span class="<?=($_CLEAN['coll'] !== COLL_SALE) ? 'icon-plus-3' : 'icon-minus-3'?> fltrt"></span>
    </li>
    </a>
    <li class="sb-section-body-wrapper" style="<?=($_CLEAN['coll'] !== COLL_SALE) ? 'display: none;' : ''?>">
        <ul class="sb-section-body">
            <?php
            $Sales = Cataleya\Sales\Sales::load();
            foreach ($Sales as $Sale):
            ?>
            <li id="sale-tile-<?= $Sale->getID() ?>" data_add_to="sale" data_target_id="<?= $Sale->getID() ?>" class="droppable-tile tile user-status <?= ($Sale->isActive()) ? 'online' : 'offline' ?> <?= ($_CLEAN['coll'] === COLL_SALE && $_CLEAN['id'] === (int)$Sale->getID()) ? 'current' : '' ?>">
            <a class="fltlft main-anchor" href="<?=ACTIVE_HREF?>&coll=<?=COLL_SALE?>&id=<?= $Sale->getID() ?>">
                <span class="tiny-label"><?= $Sale->getStore()->getDescription()->getTitle('EN') ?></span>
                <?= $Sale->getDescription()->getTitle('EN') ?>
                (<span id="sale-population-<?= $Sale->getID() ?>"><?= $Sale->getPopulation() ?></span>)
            </a>

            <div class="dashboard">
            <a href="javascript:void(0)" onclick="Sale.editSale(<?= $Sale->getID() ?>);" ><span class="icon-pencil-2 fltrt"></span> </a>
            <a href="javascript:void(0)" onclick="Sale.deleteSale(<?= $Sale->getID() ?>);" ><span class="icon-close fltrt"></span></a>
            </div>
            </li>
            <?php endforeach; ?>
            <li id="new-sale-tile"><br /><br /><a class="button-1" href="javascript:void(0);"  onclick="Sale.newSale();">New SALE</a></li>
        </ul>
    </li>






    <!-- COUPONS -->

    <a href="javascript:void(0);" onclick="Catalog.toggleSideBarSection(this)" >
    <li class="sb-section-header">
        COUPONS <span class="<?=($_CLEAN['coll'] !== COLL_COUPON) ? 'icon-plus-3' : 'icon-minus-3'?> fltrt"></span>
    </li>
    </a>
    <li class="sb-section-body-wrapper" style="<?=($_CLEAN['coll'] !== COLL_COUPON) ? 'display: none;' : ''?>">
        <ul class="sb-section-body">
            <?php
            $Coupons = Cataleya\Sales\Coupons::load();
            foreach ($Coupons as $Coupon):
            ?>
            <li id="coupon-tile-<?= $Coupon->getID() ?>" data_add_to="coupon" data_target_id="<?= $Coupon->getID() ?>" class="droppable-tile tile user-status <?= ($Coupon->isActive()) ? 'online' : 'offline' ?> <?= ($_CLEAN['coll'] === COLL_COUPON && $_CLEAN['id'] === (int)$Coupon->getID()) ? 'current' : '' ?>">
            <a class="fltlft main-anchor" href="<?=ACTIVE_HREF?>&coll=<?=COLL_COUPON?>&id=<?= $Coupon->getID() ?>">
                <span class="tiny-label"><?= $Coupon->getStore()->getDescription()->getTitle('EN') ?></span>
                <?= $Coupon->getCode() ?>
                (<span id="coupon-population-<?= $Coupon->getID() ?>"><?= $Coupon->getPopulation() ?></span>)
            </a>

            <div class="dashboard">
            <a href="javascript:void(0)"  onclick="Coupon.editCoupon(<?= $Coupon->getID() ?>);" ><span class="icon-pencil-2 fltrt"></span> </a>
            <a href="javascript:void(0)" onclick="Coupon.deleteCoupon(<?= $Coupon->getID() ?>);" ><span class="icon-close fltrt"></span></a>
            </div>
            </li>
            <?php endforeach; ?>
            <li id="new-coupon-tile"><br /><br /><a class="button-1" href="javascript:void(0);" onclick="Coupon.newCoupon();">New Coupon</a></li>
        </ul>
    </li>

    <br style="clear: both" />
</ul>



</li>



<li id="main-tile-grid">
        <div class="metro tiles-wrapper">


        <?php if (IS_SUPER_USER || $adminUser->getRole()->hasPrivilege('CREATE_PRODUCT')): ?>
        <a class="product-tile-add-button" href="javascript:void(0);" onclick="Catalog.newProduct();">

               <div class="text-header3">new product</div>
               <div class="icon-new tile-icon-large"></div>
        </a>
        <?php endif ?>

        <?php

         $_item_num = ITEM_NUM_START;
         if(empty($Stores)) $Stores = Cataleya\Stores::load();



        foreach ($Catalog as $Product)
        {

                $product_description = $Product->getDescription();
                $_stock_low = FALSE;
                $_out = FALSE;


               foreach ($Product->getOptions() as $Option)
               {

                   $_stock = $Option->getStock();
                   foreach ($Stores as $Store)
                   {
                       if ((int)$_stock->getValue($Store) < 5) $_stock_low = TRUE;
                       if ((int)$_stock->getValue($Store) == 0) $_out = TRUE;
                   }

                   // if ($_stock_low || $_out) break;
               }


        ?>

             <div id="product-tile-<?=$Product->getProductId()?>" class="product-tile-wrapper checklist-tile trashable numbered <?= ($_stock_low) ? 'low-stock' : '' ?> <?= ($_out) ? 'out-of-stock' : '' ?> <?= ($Product->isFeatured()) ? 'featured' : '' ?> " item_number="<?= $_item_num++?>" data-tile-id="<?= $Product->getID() ?>" data-featured="<?= ($Product->isFeatured()) ? 1 : 0 ?>" data_product_id="<?= $Product->getID() ?>" >
                <div class="product-thumb-wrapper"><img src="<?=$Product->getPrimaryImage()->getHref(Cataleya\Asset\Image::LARGE)?>" width="150" alt=""></div>
                <div class="product-tile-dashboard-wrapper clearfix">
                   <div class="header" title="<?=$product_description->getTitle('EN', 500)?>"><?=$product_description->getTitle('EN', 30)?></div>
                   <div class="text1 reference-code"><?=$Product->getReferenceCode()?>&nbsp;</div>
                   <span class="featured-icon icon-star-3"></span>

                   <a class="show-product-stats-button" href="javascript:void(0);" onclick="Chart.showProductStats(0, <?=$Product->getProductId()?>)">
                   <ul class="info-charms">
                       <li class="icon-heart"><?=$Product->getFavourites()?></li>
                       <li class="icon-star-4"><?=$Product->getViews()?></li>
                       <li class="icon-basket"><?=$Product->getQuantitySold()?></li>
                   </ul>
                   </a>

                   <ul class="product-tile-dashboard">

                       <a class="circular-button edit-button" href="javascript:void(0);" >
                       <li><span class="icon-pencil-2"></span><span class="button-label">Edit</span></li>
                       </a>

                       <a class="circular-button delete-button" href="javascript:void(0);">
                       <li><span class="icon-trash-fill"></span><span class="button-label">Delete</span></li>
                       </a>

                       <?php if (!$Product->isHidden()):  ?>
                       <a class="circular-button toggle-visibility-button" id="product-hide-button" href="javascript:void(0);"  data-hidden=0 >
                       <li><span class="hide-icon icon-minus-5"></span><span class="button-label">Hide</span></li>
                       </a>

                       <?php else: ?>

                       <a class="circular-button toggle-visibility-button" id="product-show-button" href="javascript:void(0);"  data-hidden=1 >
                       <li><span class="hide-icon icon-eye-4"></span><span class="button-label">Show</span></li>
                       </a>

                       <?php endif; ?>

                       <a id="product-tile-select-button" class="circular-button select-button" href="javascript:void(0);">
                       <li><span class="icon-checkmark"></span><span class="button-label">Select</span></li>
                       </a>


                   </ul>
                </div>
             </div>

        <?php

        }


        ?>



        </div>



</li>




<li id="quick-search-tile-grid">
    <div class="metro tiles-wrapper">


    </div>
</li>




</ul>



<div id="footer">
&nbsp;
</div>




</div>
</div>



</body>
</html>
