<?php



define ('OUTPUT', 'JSON'); // Whether to output as JSON or HTML (especially errors)...


// LOAD APPLICATION TOP...
require_once ('app_top.php');




$WHITE_LIST = array(
						'auth_token',  
                                                'target_id', 
                                                'do', 
                                                'data'
						);





// DIE IF INPUT IS NOT VIA POST METHOD...
if ($_SERVER['REQUEST_METHOD'] != 'POST') _catch_error('POST METHOD ONLY PLEASE. NOW GET.', __LINE__, true);

// SANITIZE INPUT DATA...
$_CLEAN = filter_input_array(INPUT_POST, array(
                                                    'auth_token'		=>	FILTER_SANITIZE_STRIPPED,
                                                    'target_id'	=>	FILTER_VALIDATE_INT, 
                                                    'data'	=>	array( 'flags' => FILTER_REQUIRE_ARRAY ), 
                                                    'do'	=>	array('filter'		=>	FILTER_VALIDATE_REGEXP, 
                                                                                                'options'	=>	array('regexp' => '/^[A-Za-z ]{1,50}$/')
                                                                                                                ), 

                                                    )
										
					);


// Check if INPUT keys are ALL expected AND MADE IT THROUGH...
$_suspect = anySuspects();
if ($_suspect !== FALSE) _catch_error('Error processing white_list!' . $_suspect, __LINE__, true);




// VALIDATE AUTH TOKEN...
validate_auth_token ();

// Validate task

$_FUNCS = array (
    'void' => array (), 
    
    
    'newStore' => array (), 
    'getStoreInfo' => array (),
    'deleteStore' => array (), 
    
    
    'openStore' => array (), 
    'closeStore' => array (),
    
    'saveTitle' => array (
        'text' => 'string'
    ),  
    'saveDescription' => array (
        'text' => 'html'
    ), 
    'saveStoreHours' => array (
        'text' => 'string'
        ),  
    'saveTimeZone' => array (
        'text' => 'timeZone'
        ), 
    'saveLanguage' => array (
        'text' => 'iso'
        ), 
    'saveTheme' => array (
        'theme_id' => 'string', 
        'text' => 'string'
        ),
    'saveHandle' => array (
        'shop_handle' => 'string'
        ),
    
    'saveLabelColor' => array (
        'label_color' => 'alpha', 
        'text' => 'string'
        ),
      
    'saveCurrency' => array (
        'text' => 'iso'
        ), 
    'enablePaymentType' => array (
        'payment_type_id' => 'alphnum', 
        'store_id' => 'int'
    ), 
    'disablePaymentType' => array (
        'payment_type_id' => 'alphanum', 
        'store_id' => 'int'
    ),  
    
    'saveDisplayImage' => array (
        'image_id'=>'integer', 
        'rotate'=>'float', 
        'sw'=>'float', 
        'sh'=>'float', 
        'sx'=>'float', 
        'sy'=>'float'
        ), 
    
    // Store Contact
    'saveContactName' => array (
        'text' => 'name'
        ),
    'saveContactEmail' => array (
        'text' => 'email'
        ), 
    'saveContactPhone' => array (
        'text' => 'phone'
        ), 
    
    // Address 
    'saveContactStreetAddress' => array (), 
    'saveContactStreetAddress2' => array (),
    'saveContactCity' => array (
        'text' => 'name'
        ),
    'saveContactState' => array (),
    'saveContactCountry' => array (
        'text' => 'iso'
        ),
    'saveContactPostCode' => array (
        'text' => 'postalCode'
        )
    
);





if (!array_key_exists($_CLEAN['do'], $_FUNCS) || !is_callable($_CLEAN['do'])) _catch_error('Invalid action.', __LINE__, true);



define('DASH_LANGUAGE_CODE', Cataleya\System::load()->getDashboardLanguageCode());
define('SHOPFRONT_LANGUAGE_CODE', Cataleya\System::load()->getShopFrontLanguageCode());
     



// OUTPUT...
 $json_reply = array (
                 "status" => 'ok', 
                 "message" => 'Void'
                 );
 
 

// EXECUTE API CALL

// API FUNC CALL
$_CLEAN['do']();



// HELPER FUNCS


function digestParams () 
{
    global $_CLEAN, $_FUNCS;
    $_params = Cataleya\Helper\Validator::params($_FUNCS[$_CLEAN['do']], $_CLEAN['data']);
    $_bad_params = array ();
    

    if (!isset($_params['language_code'])) 
    {
        $_params['language_code'] = DASH_LANGUAGE_CODE;

    }
    
    

    $_params['auto_confirm'] = isset($_params['auto_confirm']) 
                                ? filter_var($_params['auto_confirm'], FILTER_VALIDATE_BOOLEAN) 
                                : FALSE;
    
    
    
    // Check if every one made it through
    
    foreach ($_params as $k=>$v) {
        if ($v === NULL) $_bad_params[] = $k;
    }
    

    if (!empty($_bad_params)) _catch_error('Unable to save.' . implode, __LINE__, true);
    
    return $_params;
}












/*
 * 
 * 
 * CALLBACKS
 * 
 */


function void () {
    
}






/*
 * 
 * [ newStore ]
 * ______________________________________________________
 * 
 * @param: int $_POST['target_id']
 * @param: int $_POST['data']
 * 
 */


function newStore () {

    global $json_reply;
    $_params = digestParams();
    

    $_title = (!empty($_params['title']) && is_string($_params['title'])) ? $_params['title'] : 'Untitled Store';
    $_description = (!empty($_params['description']) && is_string($_params['description'])) ? $_params['title'] : 'No Description';


    $_Country = Cataleya\Geo\Country::load('NG');

    if ($_Country->hasProvinces()) 
    {
        foreach ($_Country as $_Province) 
        {
            $_Contact = Cataleya\Asset\Contact::create($_Province);
            break;
        }
    } else {
        $_Contact = Cataleya\Asset\Contact::create($_Country);
    }


    $_Store = Cataleya\Store::create($_Contact, $_title, $_description, SHOPFRONT_LANGUAGE_CODE, 'NGN');


    

    // STORE PAGES
    Cataleya\Front\Shop\Page::create($_Store, 'Contact', 'Contact...', $_Store->getLanguageCode(), TRUE);
    Cataleya\Front\Shop\Page::create($_Store, 'About', 'About Us...', $_Store->getLanguageCode(), FALSE);
    Cataleya\Front\Shop\Page::create($_Store, 'Terms', 'Term and Condidtions', $_Store->getLanguageCode(), TRUE);
    Cataleya\Front\Shop\Page::create($_Store, 'Return Policies', 'Return Policies', $_Store->getLanguageCode(), TRUE);


    
    
    $json_reply['message'] = 'New Store saved.';
    $json_reply['Page'] = Cataleya\Front\Dashboard\Juicer::juice($_Store);

}






/*
 * 
 * [ deleteStore ]
 * ______________________________________________________
 * 
 * @param: int $_POST['target_id']
 * 
 * 
 */


function deleteStore () {
    


    global $_CLEAN, $json_reply, $_admin_role;
    $_params = digestParams();
 
    
    
    /*
    * 
    * CHECK USER PRIVILEGES
    * 
    */

    if ((!isset($_admin_role) || !$_admin_role->hasPrivilege('DELETE_STORES')) && !IS_SUPER_USER)  forbidden();

    
    
    $_Store = Cataleya\Store::load($_CLEAN['target_id']);
    if ($_Store === NULL ) _catch_error('Store could not be found.', __LINE__, true);
    

    // Confirm delete action (and require password)
    $message = 'To delete the \'' . $_Store->getTitleText() . '\' store please confirm your password...';
    confirm($message, TRUE);
    

    Cataleya\Helper\DBH::getInstance()->beginTransaction();
    $_Store->delete();
    Cataleya\Helper\DBH::getInstance()->commit();
    
    $json_reply['message'] = 'Store deleted.';
    $json_reply['Store'] = array ('id'=>$_CLEAN['target_id']);

}






/**
 * 
 * Requires admin password...
 * 
 * @param integer $_POST['target_id']
 * 
 * 
 */
function openStore () {

    global $_CLEAN, $json_reply;
 
    $_Store = Cataleya\Store::load($_CLEAN['target_id']);
    if ($_Store === NULL ) _catch_error('Store could not be found.', __LINE__, true);
    

    // Confirm delete action (and require password)
    $message = 'To open (enable) the \'' . $_Store->getTitleText() . '\' store please confirm your password...';
    confirm($message, TRUE);
    

    $_Store->enable();
    
    $json_reply['message'] = $_Store->getTitleText() . ' is now ' . (($_Store->isActive()) ? 'open.' : 'closed.');
    $json_reply['Store'] = Cataleya\Front\Dashboard\Juicer::juiceDrop($_Store);

}






/**
 * 
 * Requires admin password...
 * 
 * @param integer $_POST['target_id']
 * 
 * 
 */
function closeStore () {

    global $_CLEAN, $json_reply;
 
    $_Store = Cataleya\Store::load($_CLEAN['target_id']);
    if ($_Store === NULL ) _catch_error('Store could not be found.', __LINE__, true);
    

    // Confirm delete action (and require password)
    $message = 'To close (disable) the \'' . $_Store->getTitleText() . '\' store please confirm your password...';
    confirm($message, TRUE);
    
    $_Store->disable();
    
    $json_reply['message'] = $_Store->getTitleText() . ' is now ' . (($_Store->isActive()) ? 'open.' : 'closed.');
    $json_reply['Store'] = Cataleya\Front\Dashboard\Juicer::juiceDrop($_Store);

}










/*
 * 
 * [ getStoreInfo ]
 * ______________________________________________________
 * 
 * @param: int $_POST['target_id']
 * 
 * 
 */


function getStoreInfo () {

    global $_CLEAN, $json_reply;
 
    $_Store = Cataleya\Front\Shop\Store::load((int)$_CLEAN['target_id']);
    if ($_Store === NULL ) _catch_error('Store could not be found.', __LINE__, true);
    
    
    $json_reply['message'] = 'Store info.';
    $json_reply['Store'] = Cataleya\Front\Dashboard\Juicer::juice($_Store);
    

}












/*
 * 
 * [ saveTitle ]
 * ______________________________________________________
 * 
 * @param: int $_POST['target_id']
 * @param: int $_POST['data']
 * 
 */


function saveTitle () {

    global $_CLEAN, $json_reply;
    
    $_params = digestParams();
    $_Store = Cataleya\Store::load($_CLEAN['target_id']);
    if ($_Store === NULL ) _catch_error('Store could not be found.', __LINE__, true);
    
    // [1] Store Description
    $_Store
            
    ->setTitle($_params['text']);
    
    
    // Update url_rewrite...
    //$_Store->setHandle($_params['text']);
    
    
    $json_reply['message'] = 'Title saved.';
    $json_reply['Store'] = Cataleya\Front\Dashboard\Juicer::juiceDrop($_Store);

}






/*
 * 
 * [ saveDescription ]
 * ______________________________________________________
 * 
 * @param: int $_POST['target_id']
 * @param: int $_POST['data']
 * 
 */


function saveDescription () {


    global $_CLEAN, $json_reply;
    
    $_params = digestParams();
    $_Store = Cataleya\Store::load($_CLEAN['target_id']);
    if ($_Store === NULL ) _catch_error('Store could not be found.', __LINE__, true);
    
    // [1] Store Description
    $_Store
            
    ->setDescription($_params['text']);
    
    
    
    $json_reply['message'] = 'Description saved.';
    $json_reply['Store'] = Cataleya\Front\Dashboard\Juicer::juiceDrop($_Store);

}









/*
 * 
 * [ saveLanguage ]
 * ______________________________________________________
 * 
 * @param: int $_POST['target_id']
 * @param: int $_POST['data']
 * 
 */


function saveLanguage () {


    global $_CLEAN, $json_reply;
    
    $_params = digestParams();
    $_Store = Cataleya\Store::load($_CLEAN['target_id']);
    if ($_Store === NULL ) _catch_error('Store could not be found.', __LINE__, true);
    
    
    $_Store->setLanguageCode($_params['text']);
    
    
    
    $json_reply['message'] = 'Store Language saved.';
    $json_reply['Store'] = Cataleya\Front\Dashboard\Juicer::juiceDrop($_Store);

}










/*
 * 
 * [ saveCurrency ]
 * ______________________________________________________
 * 
 * @param: int $_POST['target_id']
 * @param: int $_POST['data']
 * 
 */


function saveCurrency () {


    global $_CLEAN, $json_reply;
    
    $_params = digestParams();
    $_Store = Cataleya\Store::load($_CLEAN['target_id']);
    if ($_Store === NULL ) _catch_error('Store could not be found.', __LINE__, true);
    
    
    $_Store->setCurrencyCode($_params['text']);
    
    
    
    $json_reply['message'] = 'Store Currency saved.';
    $json_reply['Store'] = Cataleya\Front\Dashboard\Juicer::juiceDrop($_Store);

}







/**
 * 
 * Requires admin password...
 * 
 * @param integer $_POST['target_id'] Store ID
 * @param string $_param['payment_type_id'] Payment Type ID
 * 
 * 
 */
function disablePaymentType () {

    global $_CLEAN, $json_reply;
    $_params = digestParams();
 
    $_Store = Cataleya\Store::load($_params['store_id']);
    if ($_Store === NULL ) _catch_error('Store could not be found.', __LINE__, true);
    
    $_Store->doNotAcceptPaymentType($_params['payment_type_id']);
    

    
    $json_reply['message'] = 'Payment Type removed.';
    $json_reply['PaymentType'] = []; // Cataleya\Front\Dashboard\Juicer::juice($_PaymentType, $_Store);

}







/**
 * 
 * Requires admin password...
 * 
 * @param integer $_POST['target_id'] Store ID
 * @param string $_param['payment_type_id'] Payment Type ID
 * 
 * 
 */
function enablePaymentType () {

    global $_CLEAN, $json_reply;
    $_params = digestParams();
 
    $_Store = Cataleya\Store::load($_params['store_id']);
    if ($_Store === NULL ) _catch_error('Store could not be found.', __LINE__, true);
    
    $_Store->addPaymentType($_params['payment_type_id']);
    

    
    $json_reply['message'] = 'Payment Type added.';
    $json_reply['PaymentType'] = []; // Cataleya\Front\Dashboard\Juicer::juice($_PaymentType, $_Store);

}














/*
 * 
 * [ saveTheme ]
 * ______________________________________________________
 * 
 * @param: int $_POST['target_id']
 * @param: int $_POST['data']
 * 
 */


function saveTheme () {

    global $_CLEAN, $json_reply;
    
    $_params = digestParams();
    $_Store = Cataleya\Store::load($_CLEAN['target_id']);
    if ($_Store === NULL ) _catch_error('Store could not be found.', __LINE__, true);
    
    $_Theme = Cataleya\Front\Shop\Theme::load($_params['theme_id']);
    if ($_Theme === NULL ) _catch_error('Theme could not be found.', __LINE__, true);
    
    
    // [1] Store Description
    $_Store->setTheme($_Theme);
    
    
    
    $json_reply['message'] = 'Template saved.';
    $json_reply['Store'] = Cataleya\Front\Dashboard\Juicer::juiceDrop($_Store);

}










/*
 * 
 * [ saveLabelColor ]
 * ______________________________________________________
 * 
 * @param: int $_POST['target_id']
 * @param: int $_POST['data']
 * 
 */


function saveLabelColor () {

    global $_CLEAN, $json_reply;
    
    $_params = digestParams();
    $_Store = Cataleya\Store::load($_CLEAN['target_id']);
    if ($_Store === NULL ) _catch_error('Store could not be found.', __LINE__, true);
    

    // [1] Store Description
    $_Store->setLabelColor($_params['label_color']);
    
    
    
    $json_reply['message'] = 'Label color saved.';
    $json_reply['Store'] = Cataleya\Front\Dashboard\Juicer::juiceDrop($_Store);

}






/*
 * 
 * [ saveLabelColor ]
 * ______________________________________________________
 * 
 * @param: int $_POST['target_id']
 * @param: int $_POST['data']
 * 
 */


function saveHandle () {

    global $_CLEAN, $json_reply;
    
    $_params = digestParams();
    $_Store = Cataleya\Store::load($_CLEAN['target_id']);
    if ($_Store === NULL ) _catch_error('Store could not be found.', __LINE__, true);
    

    // [1] Store Description
    $_Store->setHandle($_params['shop_handle']);
    
    
    
    $json_reply['message'] = 'Shop handle saved.';
    $json_reply['Store'] = Cataleya\Front\Dashboard\Juicer::juiceDrop($_Store);

}







/**
 * 
 * @name saveDisplayImage
 * 
 */


function saveDisplayImage () {

    global $_CLEAN, $json_reply;
    
    $_params = digestParams();
    

    $new_image = Cataleya\Asset\Image::load($_params['image_id']);
    if ((int)$new_image->getID() === 0) _catch_error('Image not found.' . $_params['image_id'], __LINE__, true);
    
    
    

    $options = array(
                                    'rotate'=>$_params['rotate'], 
                                    'sw'=>$_params['sw'], 
                                    'sh'=>$_params['sh'], 
                                    'sx'=>$_params['sx'], 
                                    'sy'=>$_params['sy']
                                    ); 
    
    if (!$new_image->bake($options, TRUE)) 
    {
        $new_image->destroy();
        _catch_error('Error saving image.', __LINE__, true);
    }
    

    $new_image->makeLarge(300);
    $new_image->makeThumb(160);
    $new_image->makeTiny(80);
    

    $_Store = Cataleya\Store::load($_CLEAN['target_id']);
    if ($_Store === NULL ) { 
            $new_image->destroy();
            _catch_error('Error saving image: Store could not be found.', __LINE__, true);
    }
    else { 
        $_Store->setDisplayPicture($new_image);
            
        
    }
    
    
    
    
    $json_reply['message'] = 'Image saved.';
    $json_reply['Image'] = array (
                                    'id' => $new_image->getID(), 
                                    'hrefs' => $new_image->getHrefs()
                                   );

}










/*
 * 
 * [ saveStoreHours ]
 * ______________________________________________________
 * 
 * @param: int $_POST['target_id']
 * @param: int $_POST['data']
 * 
 */


function saveStoreHours () {


    global $_CLEAN, $json_reply;
    
    $_params = digestParams();
    $_Store = Cataleya\Store::load($_CLEAN['target_id']);
    if ($_Store === NULL ) _catch_error('Store could not be found.', __LINE__, true);
    
    
    $_Store->setHours($_params['text']);
    
    
    
    $json_reply['message'] = 'Store Hours saved.';
    $json_reply['Store'] = Cataleya\Front\Dashboard\Juicer::juiceDrop($_Store);

}







/*
 * 
 * [ saveTimeZone ]
 * ______________________________________________________
 * 
 * @param: int $_POST['target_id']
 * @param: int $_POST['data']
 * 
 */


function saveTimeZone () {


    global $_CLEAN, $json_reply;
    
    $_params = digestParams();
    $_Store = Cataleya\Store::load($_CLEAN['target_id']);
    if ($_Store === NULL ) _catch_error('Store could not be found.', __LINE__, true);
    
    if (in_array($_params['text'], Cataleya\Helper::getTimeZones())) {
        $_Store->setTimezone(new DateTimeZone($_params['text']));
    } else {
        _catch_error('Time Zone could not be saved.', __LINE__, true);
    }
    
    
    
    $json_reply['message'] = 'Store Language saved.';
    $json_reply['Store'] = Cataleya\Front\Dashboard\Juicer::juiceDrop($_Store);

}













/*
 * 
 * [ saveContactName ]
 * ______________________________________________________
 * 
 * @param: int $_POST['target_id']
 * @param: int $_POST['data']
 * 
 */


function saveContactName () {


    global $_CLEAN, $json_reply;
    
    $_params = digestParams();
    $_Store = Cataleya\Store::load($_CLEAN['target_id']);
    if ($_Store === NULL ) _catch_error('Store could not be found.', __LINE__, true);
    
    
    $_Store->setContactName($_params['text']);
    
    
    $json_reply['message'] = 'Contact Name saved.';
    $json_reply['Store'] = Cataleya\Front\Dashboard\Juicer::juiceDrop($_Store);

}









/*
 * 
 * [ saveContactPhone ]
 * ______________________________________________________
 * 
 * @param: int $_POST['target_id']
 * @param: int $_POST['data']
 * 
 */


function saveContactPhone () {


    global $_CLEAN, $json_reply;
    
    $_params = digestParams();
    $_Store = Cataleya\Store::load($_CLEAN['target_id']);
    if ($_Store === NULL ) _catch_error('Store could not be found.', __LINE__, true);
    
    
    $_Store->setTelephone($_params['text']);
    
    
    
    $json_reply['message'] = 'Contact Phone Number saved.';
    $json_reply['Store'] = Cataleya\Front\Dashboard\Juicer::juiceDrop($_Store);

}








/*
 * 
 * [ saveContactEmail ]
 * ______________________________________________________
 * 
 * @param: int $_POST['target_id']
 * @param: int $_POST['data']
 * 
 */


function saveContactEmail () {


    global $_CLEAN, $json_reply;
    
    $_params = digestParams();
    $_Store = Cataleya\Store::load($_CLEAN['target_id']);
    if ($_Store === NULL ) _catch_error('Store could not be found.', __LINE__, true);
    
    
    $_Store->setEmail($_params['text']);
    
    
    
    $json_reply['message'] = 'Contact Email saved.';
    $json_reply['Store'] = Cataleya\Front\Dashboard\Juicer::juiceDrop($_Store);

}









/*
 * 
 * [ saveContactStreetAddress ]
 * ______________________________________________________
 * 
 * @param: int $_POST['target_id']
 * @param: int $_POST['data']
 * 
 */


function saveContactStreetAddress () {


    global $_CLEAN, $json_reply;
    
    $_params = digestParams();
    $_Store = Cataleya\Store::load($_CLEAN['target_id']);
    if ($_Store === NULL ) _catch_error('Store could not be found.', __LINE__, true);
    
    
    $_Store->getDefaultContact()->setEntryStreetAddress($_params['text']);
    
    
    
    $json_reply['message'] = 'Contact Address saved.';
    $json_reply['Store'] = Cataleya\Front\Dashboard\Juicer::juiceDrop($_Store);

}







/*
 * 
 * [ saveContactStreetAddress2 ]
 * ______________________________________________________
 * 
 * @param: int $_POST['target_id']
 * @param: int $_POST['data']
 * 
 */


function saveContactStreetAddress2 () {


    global $_CLEAN, $json_reply;
    
    $_params = digestParams();
    $_Store = Cataleya\Store::load($_CLEAN['target_id']);
    if ($_Store === NULL ) _catch_error('Store could not be found.', __LINE__, true);
    
    
    $_Store->getDefaultContact()->setEntryStreetAddress2($_params['text']);
    
    
    
    $json_reply['message'] = 'Contact Address (2) saved.';
    $json_reply['Store'] = Cataleya\Front\Dashboard\Juicer::juiceDrop($_Store);

}







/*
 * 
 * [ saveContactCity ]
 * ______________________________________________________
 * 
 * @param: int $_POST['target_id']
 * @param: int $_POST['data']
 * 
 */


function saveContactCity () {


    global $_CLEAN, $json_reply;
    
    $_params = digestParams();
    $_Store = Cataleya\Store::load($_CLEAN['target_id']);
    if ($_Store === NULL ) _catch_error('Store could not be found.', __LINE__, true);
    
    
    $_Store->getDefaultContact()->setEntryCity($_params['text']);
    
    
    
    $json_reply['message'] = 'Contact Address City saved.';
    $json_reply['Store'] = Cataleya\Front\Dashboard\Juicer::juiceDrop($_Store);

}









/*
 * 
 * [ saveContactState ]
 * ______________________________________________________
 * 
 * @param: int $_POST['target_id']
 * @param: int $_POST['data']
 * 
 */


function saveContactState () {


    global $_CLEAN, $json_reply;
    
    $_params = digestParams();
    $_Store = Cataleya\Store::load($_CLEAN['target_id']);
    if ($_Store === NULL ) _catch_error('Store could not be found.', __LINE__, true);
    
    
    $_Store->getDefaultContact()->setEntryState($_params['text']);
    
    
    
    $json_reply['message'] = 'Contact Address State saved.';
    $json_reply['Store'] = Cataleya\Front\Dashboard\Juicer::juiceDrop($_Store);

}










/*
 * 
 * [ saveContactCountry ]
 * ______________________________________________________
 * 
 * @param: int $_POST['target_id']
 * @param: int $_POST['data']
 * 
 */


function saveContactCountry () {


    global $_CLEAN, $json_reply;
    
    $_params = digestParams();
    $_Store = Cataleya\Store::load($_CLEAN['target_id']);
    if ($_Store === NULL ) _catch_error('Store could not be found.', __LINE__, true);
    
    
    $_Store->getDefaultContact()->setEntryCountryCode($_params['text']);
    
    
    
    $json_reply['message'] = 'Contact Address Country saved.';
    $json_reply['Store'] = Cataleya\Front\Dashboard\Juicer::juiceDrop($_Store);

}









/*
 * 
 * [ saveContactPostCode ]
 * ______________________________________________________
 * 
 * @param: int $_POST['target_id']
 * @param: int $_POST['data']
 * 
 */


function saveContactPostCode () {


    global $_CLEAN, $json_reply;
    
    $_params = digestParams();
    $_Store = Cataleya\Store::load($_CLEAN['target_id']);
    if ($_Store === NULL ) _catch_error('Store could not be found.', __LINE__, true);
    
    
    $_Store->getDefaultContact()->setEntryPostcode($_params['text']);
    
    
    
    $json_reply['message'] = 'Contact Address Post Code saved.';
    $json_reply['Store'] = Cataleya\Front\Dashboard\Juicer::juiceDrop($_Store);

}






















// Remove token...
if (isset($_SESSION[SESSION_PREFIX.'CONFIRM_TOKEN'])) unset ($_SESSION[SESSION_PREFIX.'CONFIRM_TOKEN']);




 echo (json_encode($json_reply));
 exit();  
