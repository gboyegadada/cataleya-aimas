<?php
/*
FETCH PAYMENT TYPES AND RETURN AS JSON...
*/




define ('OUTPUT', 'JSON'); // Whether to output as JSON or HTML (especially errors)...


// LOAD APPLICATION TOP...
require_once ('app_top.php');






// DIE IF INPUT IS NOT VIA POST METHOD...
if ($_SERVER['REQUEST_METHOD'] != 'POST') _catch_error('POST METHOD ONLY PLEASE. NOW GET.');
	



// FILTER VALUES ....
$_CLEAN = filter_input_array(INPUT_POST, 
								array(	
                                                                                // auth_token
                                                                                'auth_token'    =>  FILTER_SANITIZE_STRIPPED

										)
								);


$PaymentTypes = Cataleya\Payment\PaymentTypes::load();
if ($PaymentTypes === NULL) _catch_error('Payment Types could not be loaded.', __LINE__, true);


$payment_types_info = array ();
$_Currency = Cataleya\Locale\Currency::loadBaseCurrency();

foreach ($PaymentTypes as $PaymentType) 
{
    $payment_types_info[] = array (
        'id'    =>  $PaymentType->getID(), 
        'name'  =>  $PaymentType->getName(), 
        'displayName'  =>  $PaymentType->getDisplayName(), 
        'enabled'  =>  $PaymentType->isActive(), 
        'MaxAmount' => number_format ((float)$PaymentType->getMaxAmount($_Currency), 2), 
        'MinAmount' => number_format ((float)$PaymentType->getMinAmount($_Currency), 2), 
        'Currency' => $_Currency->getCurrencyCode()
    );
}

// OUTPUT...
$json_reply = array (
		"status"=>'ok', 
		"message"=>(($PaymentTypes->getPopulation() < 1) ? 'No': $PaymentTypes->getPopulation()) . ' Payment Types found.', 
                "PaymentTypes"=>$payment_types_info, 
                "count"=>$PaymentTypes->getPopulation()
		);

echo (json_encode($json_reply));
exit();


