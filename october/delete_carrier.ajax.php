<?php



define ('OUTPUT', 'JSON'); // Whether to output as JSON or HTML (especially errors)...


// LOAD APPLICATION TOP...
require_once ('app_top.php');



/*
 * 
 * TO PERFORM THIS OPERATION, USER MUST BE AN ADMINISTRATOR
 * 
 */


if (!IS_SUPER_USER || $adminUser->getRole()->hasPrivilege('DELETE_CARRIER')) _catch_error('You do have permission to perform this action.', __LINE__, true);




// ENV...
define ('PERMS', 'RW');
$WHITE_LIST = array(
						'auth_token',
						'carrier_id'
						);





// DIE IF INPUT IS NOT VIA POST METHOD...
if ($_SERVER['REQUEST_METHOD'] != 'POST') _catch_error('POST METHOD ONLY PLEASE. NOW GET.', __LINE__, true);

// SANITIZE INPUT DATA...
$_CLEAN = filter_input_array(INPUT_POST, array(
                                                    'auth_token'		=>	FILTER_SANITIZE_STRIPPED, 
                                                    'carrier_id'	=>	FILTER_VALIDATE_INT

                                                    )
										
					);


// Check if INPUT keys are ALL expected AND MADE IT THROUGH...
if (anySuspects()) _catch_error('Error processing white_list!', __LINE__, true);



// VALIDATE AUTH TOKEN...
validate_auth_token ();


// Load carrier
$carrier = Cataleya\Shipping\Carrier::load($_CLEAN['carrier_id']);


// Check if carrier exists...
if ($carrier === NULL)
{
        if (isset($_SESSION[SESSION_PREFIX.'CONFIRM_PARAMS'])) unset ($_SESSION[SESSION_PREFIX.'CONFIRM_PARAMS']);
        
        // ERROR...
        _catch_error('Carrier not found.', __LINE__, true);

}


// Confirm delete action
$message = 'Are you sure you want to delete this shipping carrier? (\'' . $carrier->getDescription()->getTitle('EN') . '\')';
confirm($message, FALSE);

// Delete carrier (if we make it past the 'confirm' function)...
$carrier->delete();


// Remove token...
if (isset($_SESSION[SESSION_PREFIX.'CONFIRM_TOKEN'])) unset ($_SESSION[SESSION_PREFIX.'CONFIRM_TOKEN']);

// OUTPUT...
 $json_reply = array (
                 "status" => 'ok', 
                 "message" => 'Carrier deleted.'
                 );


 echo (json_encode($json_reply));
 exit();  

?>