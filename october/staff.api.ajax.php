<?php



define ('OUTPUT', 'JSON'); // Whether to output as JSON or HTML (especially errors)...


// LOAD APPLICATION TOP...
require_once ('app_top.php');




$WHITE_LIST = array(
						'auth_token',  
                                                'target_id', 
                                                'do', 
                                                'data'
						);





// DIE IF INPUT IS NOT VIA POST METHOD...
if ($_SERVER['REQUEST_METHOD'] != 'POST') _catch_error('POST METHOD ONLY PLEASE. NOW GET.', __LINE__, true);

// SANITIZE INPUT DATA...
$_CLEAN = filter_input_array(INPUT_POST, array(
                                                    'auth_token'		=>	FILTER_SANITIZE_STRIPPED,
                                                    'target_id'	=>	FILTER_VALIDATE_INT, 
                                                    'data'	=>	array( 'filter'=> FILTER_SANITIZE_STRIPPED, 'flags' => FILTER_REQUIRE_ARRAY ), 
                                                    'do'	=>	array('filter'		=>	FILTER_VALIDATE_REGEXP, 
                                                                                                'options'	=>	array('regexp' => '/^[A-Za-z ]{1,50}$/')
                                                                                                                ), 

                                                    )
										
					);


// Check if INPUT keys are ALL expected AND MADE IT THROUGH...
$_suspect = anySuspects();
if ($_suspect !== FALSE) _catch_error('Error processing white_list!' . $_suspect, __LINE__, true);




// VALIDATE AUTH TOKEN...
validate_auth_token ();

// Validate task

$_FUNCS = array (
    'void', 
    'addStaffMember', 
    'removeStaffMember', 
    'getInfo'
    
);





if (!in_array($_CLEAN['do'], $_FUNCS) || !is_callable($_CLEAN['do'])) _catch_error('Invalid action.', __LINE__, true);



    
// OUTPUT...
 $json_reply = array (
                 "status" => 'ok', 
                 "message" => 'Ok'
                 );
 
 

// EXECUTE API CALL

// API FUNC CALL
$_CLEAN['do']();



// HELPER FUNCS


function digestParams () 
{
    global $_CLEAN;
    $_params = $_CLEAN['data'];
    $_bad_params = array ();
    

    if (isset($_params['admin_id'])) 
    {

        $_params['admin_id'] = Cataleya\Helper\Validator::int($_params['admin_id']);
        if ($_params['admin_id'] === FALSE) $_bad_params[] = 'admin_id';

    }
    
   
    
    // Check if every one made it through
    if (!empty($_bad_params)) _catch_error('Bad params.', __LINE__, true);
    
    return $_params;
}





function juiceStaff (Cataleya\Store\Staff $_Staff) 
{
    
    $_info = array ();
    $_AdminUsers = Cataleya\Admin\Users::load();
    
    foreach ($_AdminUsers as $_AdminUser) 
    {
        if ($_Staff->hasStaffMember($_AdminUser)) continue;
        
        $_info[] = array (
           'id' =>  $_AdminUser->getID(),
           'label'  => ($_AdminUser->getName() . ' (' . $_AdminUser->getRole()->getDescription()->getTitle('EN') . ')' )
        );
    }

    
    return $_info;
    
    
}







function juiceUser (Cataleya\Admin\User $_AdminUser) 
{
    
    $Role = $_AdminUser->getRole();
    // if ($Role->hasPrivilege('SUPER_USER')) continue;
    
    $admin_role_info = array (
        'id'    =>   $Role->getRoleId(), 
        'name'  =>   $Role->getDescription()->getTitle('EN'), 
        'isAdmin'   =>  ($Role->hasPrivilege('SUPER_USER')) ? TRUE : FALSE
    );
    
    $admin_profiles_info = array (
        'id'    =>  $_AdminUser->getAdminId(), 
        'name'  =>  $_AdminUser->getName(), 
        'firstname'  =>  $_AdminUser->getFirstname(), 
        'lastname'  =>  $_AdminUser->getLastname(), 
        'email'  =>  $_AdminUser->getEmail(), 
        'phone'  =>  $_AdminUser->getPhone(), 
        'isActive'   =>  $_AdminUser->isActive(), 
        'role'  =>  $admin_role_info, 
        'dp'    =>  $_AdminUser->getDisplayPicture()->getHrefs()
    );

    
    return $admin_profiles_info;
    
    
}





/*
 * 
 * 
 * CALLBACKS
 * 
 */


function void () {
    
}






/*
 * 
 * [ addStaffMember ]
 * ______________________________________________________
 * 
 * @param: int $_POST['target_id']
 * @param: int $_POST['data']
 * 
 */


function addStaffMember () {

    global $json_reply, $_CLEAN;
    $_params = digestParams();
    
    $_Store = Cataleya\Store::load($_CLEAN['target_id']);
    if ($_Store === NULL ) _catch_error('Store could not be found.', __LINE__, true);
    
    $_AdminUser = Cataleya\Admin\User::load($_params['admin_id']);
    if ($_AdminUser === NULL ) _catch_error('Admin profile could not be found.', __LINE__, true);
    
    $_Staff = $_Store->getStaffMembers();
    $_Staff->addStaffMember($_AdminUser);
    
    $json_reply['message'] = 'Staff Members';
    $json_reply['payload'] = juiceStaff($_Staff);
    $json_reply['StaffMember'] = juiceUser($_AdminUser);

};





/*
 * 
 * [ removeStaffMember ]
 * ______________________________________________________
 * 
 * @param: int $_POST['target_id']
 * @param: int $_POST['data']
 * 
 */


function removeStaffMember () {

    global $json_reply, $_CLEAN;
    $_params = digestParams();
    
    $_Store = Cataleya\Store::load($_CLEAN['target_id']);
    if ($_Store === NULL ) _catch_error('Store could not be found.', __LINE__, true);
    
    $_AdminUser = Cataleya\Admin\User::load($_params['admin_id']);
    if ($_AdminUser === NULL ) _catch_error('Admin profile could not be found.', __LINE__, true);
    
    
    $message = 'Are you sure you want to remove this person from the list: "' . $_AdminUser->getName()  . ' (' . $_AdminUser->getRole()->getDescription()->getTitle('EN') . ')"?';
    confirm($message, FALSE);
    
    
    $_Staff = $_Store->getStaffMembers();
    $_Staff->removeStaffMember($_AdminUser);
    
    $json_reply['message'] = 'Staff Members';
    $json_reply['payload'] = juiceStaff($_Staff);
    $json_reply['StaffMember'] = array('id' => $_AdminUser->getID() );

};









/*
 * 
 * [ getInfo ]
 * ______________________________________________________
 * 
 * @param: int $_POST['target_id']
 * 
 * 
 */


function getInfo () {

    global $json_reply, $_CLEAN;
    
    $_Store = Cataleya\Store::load($_CLEAN['target_id']);
    if ($_Store === NULL ) _catch_error('Store could not be found.', __LINE__, true);
    
    $json_reply['message'] = 'Staff Members';
    $json_reply['payload'] = juiceStaff($_Store->getStaffMembers());

};






// Remove token...
if (isset($_SESSION[SESSION_PREFIX.'CONFIRM_TOKEN'])) unset ($_SESSION[SESSION_PREFIX.'CONFIRM_TOKEN']);




 echo (json_encode($json_reply));
 exit();  

?>