<?php



define ('OUTPUT', 'JSON'); // Whether to output as JSON or HTML (especially errors)...


// LOAD APPLICATION TOP...
require_once ('app_top.php');




$WHITE_LIST = array(
						'auth_token',  
                                                'do', 
                                                'data'
						);





// DIE IF INPUT IS NOT VIA POST METHOD...
if ($_SERVER['REQUEST_METHOD'] != 'POST') _catch_error('POST METHOD ONLY PLEASE. NOW GET.', __LINE__, true);

// SANITIZE INPUT DATA...
$_CLEAN = filter_input_array(INPUT_POST, array(
                                                    'auth_token'		=>	FILTER_SANITIZE_STRIPPED,
                                                    'data'	=>	array( 'filter'=> FILTER_SANITIZE_STRIPPED, 'flags' => FILTER_REQUIRE_ARRAY ), 
                                                    'do'	=>	array('filter'		=>	FILTER_VALIDATE_REGEXP, 
                                                                                                'options'	=>	array('regexp' => '/^[A-Za-z ]{1,50}$/')
                                                                                                                ), 

                                                    )
										
					);


// Check if INPUT keys are ALL expected AND MADE IT THROUGH...
$_suspect = anySuspects();
if ($_suspect !== FALSE) _catch_error('Error processing white_list!' . $_suspect, __LINE__, true);




// VALIDATE AUTH TOKEN...
validate_auth_token ();

// Validate task


$_FUNCS = array (
    'void', 
    'getNotification', 
    'getNotifications', 
    
    'markDisplayed', 
    'markNotDisplayed', 
    'markRead', 
    'markUnread'
);







if (!in_array($_CLEAN['do'], $_FUNCS) || !is_callable($_CLEAN['do'])) _catch_error('Invalid action.', __LINE__, true);




    
// OUTPUT...
 $json_reply = array (
                 "status" => 'ok', 
                 "message" => 'Ok'
                 );
 
 

// EXECUTE API CALL

// API FUNC CALL
$_CLEAN['do']();



// HELPER FUNCS


function digestParams () 
{
    global $_CLEAN;
    $_params = $_CLEAN['data'];
    $_bad_params = array ();
    
    
    if (in_array($_CLEAN['do'], array('getNotification', 'markDisplayed', 'markNotDisplayed', 'markRead', 'markUnread'))) 
    {

        $_params['notification_id'] = Cataleya\Helper\Validator::token($_params['notification_id'], 1, 100);
        if (empty($_params['notification_id'])) $_bad_params[] = 'notification_id';

    }
    

    // Check if every one made it through
    if (!empty($_bad_params)) validationFailed ($_bad_params);
    
    return $_params;
}










function juiceNotification (Cataleya\Admin\Notification $_Notification, Cataleya\Admin\User $_User) 
{

    $_Alert = $_Notification->getAlert($_User);
    
    $_info = array (
        'id'    =>  $_Notification->getID(), 
        'keyword'    =>  $_Notification->getKeyword(), 
        'title'  =>  $_Notification->getDescription()->getTitle('EN'), 
        'description'  =>  $_Notification->getDescription()->getText('EN'), 
        'entityName'  =>  $_Notification->getEntityName(), 
        'entityNamePlural'  =>  $_Notification->getEntityNamePlural(), 
        'notificationText'  =>  $_Notification->getNotificationText(), 
        'sendAfterPeriod'  =>  $_Notification->getSendAfterPeriod(), 
        'sendAfterInterval'  =>  $_Notification->getSendAfterInterval(), 
        'sendAfterIncrements'  =>  $_Notification->getSendAfterIncrements(),  
        'incrementsSinceLastSent'  =>  $_Notification->getIncrementsSinceLastSent(), 
        'url'  =>  $_Notification->getRequestUri(),
        'icon'  =>  $_Notification->getIconHref(), 
        
        'count' => $_Alert->getCount(), 
        'isDisplayed' => $_Alert->isDisplayed(), 
        'isRead' => $_Alert->isRead(), 
        'dashboardEnabled' => $_Alert->dashboardEnabled(), 
        'emailEnabled' => $_Alert->emailEnabled()
    );
    
    
    return $_info;
    
    
}










/*
 * 
 * 
 * CALLBACKS
 * 
 */


function void () {
    
}









/*
 * 
 * [ getNotification ]
 * ______________________________________________________
 * 
 * @param: int $_POST['target_id']
 * @param: int $_POST['data']
 * 
 */


function getNotification () {

    global $json_reply, $adminUser;
    $_params = digestParams();
    
    $_Notification = Cataleya\Admin\Notification::load($_params['notification_id']);
    if ($_Notification === NULL ) _catch_error('Notification could not be loaded.', __LINE__, true);
    
    $json_reply['message'] = 'Notification.';
    $json_reply['Notification'] = juiceNotification ($_Notification, $adminUser);

};




/*
 * 
 * [ getNotifications ]
 * ______________________________________________________
 * 
 * @param: int $_POST['target_id']
 * @param: int $_POST['data']
 * 
 */


function getNotifications () {

    global $json_reply, $adminUser;

    $_Notifications = Cataleya\Admin\Notifications::load();
    $json_reply['Notifications'] = array ();
    
    foreach ($_Notifications as $_Notification) $json_reply['Notifications'][] = juiceNotification ($_Notification, $adminUser);
    
    
    $json_reply['message'] = 'Notifications.';

};






/*
 * 
 * [ markAsDisplayed ]
 * ______________________________________________________
 * 
 * @param: int $_POST['target_id']
 * @param: int $_POST['data']
 * 
 */


function markDisplayed () {

    global $json_reply, $adminUser;
    $_params = digestParams();
    
    $_Notification = Cataleya\Admin\Notification::load($_params['notification_id']);
    if ($_Notification === NULL ) _catch_error('Notification could not be loaded.', __LINE__, true);
    
    $_Notification->getAlert($adminUser)->markDisplayed();
    
    
    $json_reply['message'] = 'Marked as displayed.';
    $json_reply['Notification'] = juiceNotification ($_Notification, $adminUser);

};



/*
 * 
 * [ markNotDisplayed ]
 * ______________________________________________________
 * 
 * @param: int $_POST['target_id']
 * @param: int $_POST['data']
 * 
 */


function markNotDisplayed () {

    global $json_reply, $adminUser;
    $_params = digestParams();
    
    $_Notification = Cataleya\Admin\Notification::load($_params['notification_id']);
    if ($_Notification === NULL ) _catch_error('Notification could not be loaded.', __LINE__, true);
    
    $_Notification->getAlert($adminUser)->markAsNotDisplayed();
    
    
    $json_reply['message'] = 'Marked as "not displayed".';
    $json_reply['Notification'] = juiceNotification ($_Notification, $adminUser);

};





/*
 * 
 * [ markRead ]
 * ______________________________________________________
 * 
 * @param: int $_POST['target_id']
 * @param: int $_POST['data']
 * 
 */


function markRead () {

    global $json_reply, $adminUser;
    $_params = digestParams();
    
    $_Notification = Cataleya\Admin\Notification::load($_params['notification_id']);
    if ($_Notification === NULL ) _catch_error('Notification could not be loaded.', __LINE__, true);
    
    $_Notification->getAlert($adminUser)->markRead();
    
    
    $json_reply['message'] = 'Marked as displayed.';
    $json_reply['Notification'] = juiceNotification ($_Notification, $adminUser);

};






/*
 * 
 * [ markUnread ]
 * ______________________________________________________
 * 
 * @param: int $_POST['target_id']
 * @param: int $_POST['data']
 * 
 */


function markUnread () {

    global $json_reply, $adminUser;
    $_params = digestParams();
    
    $_Notification = Cataleya\Admin\Notification::load($_params['notification_id']);
    if ($_Notification === NULL ) _catch_error('Notification could not be loaded.', __LINE__, true);
    
    $_Notification->getAlert($adminUser)->markUnread();
    
    
    $json_reply['message'] = 'Marked as displayed.';
    $json_reply['Notification'] = juiceNotification ($_Notification, $adminUser);

};





    








// Remove token...
if (isset($_SESSION[SESSION_PREFIX.'CONFIRM_TOKEN'])) unset ($_SESSION[SESSION_PREFIX.'CONFIRM_TOKEN']);




 echo (json_encode($json_reply));
 exit();  

?>