<?php



define ('OUTPUT', 'JSON'); // Whether to output as JSON or HTML (especially errors)...


// LOAD APPLICATION TOP...
require_once ('app_top.php');




/*
 * 
 * TO PERFORM THIS OPERATION, USER MUST BE AN ADMINISTRATOR
 * 
 */


if (!IS_SUPER_USER) throw new Cataleya\Error ('You do have permission to perform this action.');




// DIE IF INPUT IS NOT VIA POST METHOD...
if ($_SERVER['REQUEST_METHOD'] != 'POST') throw new Cataleya\Error ('POST METHOD ONLY PLEASE. NOW GET.');


// SANITIZE INPUT DATA...
$_CLEAN = array ();
foreach ($_POST as $key=>$val) 
{
   $_CLEAN[$key] = filter_var($val, FILTER_SANITIZE_STRING);
}



// VALIDATE AUTH TOKEN...
validate_auth_token ();


$_params = [];

foreach ($_CLEAN as $key=>$val) 
{
    $param = explode('__', $key);
 
    // index 0: 'param' (label)
    // index 1: AUTH_TOKEN
    // index 2: parameter_name
   
    if (
        empty($param) || 
        !isset($param[0], $param[1], $param[2]) || 
        $param[0] !== 'param' || 
        $param[1] !== AUTH_TOKEN
    ) continue;
   
    $_params[$param[2]] = $val;
}


$_is_plugin = (strpos($_CLEAN['config_id'], 'plugins.', 0) !== false);



if ($_is_plugin) {

    $_plugin_handle = substr($_CLEAN['config_id'], strlen('plugins.'));
    $_controls = \Cataleya\Plugins\Action::trigger('*.get-config-form-controls', [], [ $_plugin_handle ]);
    $_expected_params = [];

    foreach ($_controls as $c) 
    {

        if (isset($c['field_group']) && $c['field_group'] === true) {
            foreach ($c['fields'] as $f) $_expected_params[$f['name']] = (isset($f['type'])) ? $f['type'] : 'text';
        } else {
            $_expected_params[$f['name']] = (isset($f['type'])) ? $f['type'] : 'text';
        }

    }

    // $_params = Cataleya\Helper\Validator::params($_expected_params, $_params);
}





// Load config
$config = Cataleya\System\Config::load($_CLEAN['config_id']);
if ($config === NULL) _catch_error('Config not found.', __LINE__, true);


foreach ($_params as $key=>$val) 
{
    if ($val === false || $val === null) continue;
    $config->setParam($key, $val);
}   


$config->save();


if ($_is_plugin) {

    \Cataleya\Plugins\Action::trigger(
        '*.update-config', [ 'params' => $_params ], [ $_plugin_handle ]
    );

}


// OUTPUT...
$json_reply = array (
		"status"=>'ok', 
		"message"=>'Configuration saved!', 
		"config_id"=> (!empty($config) ? $config->getID() : $_CLEAN['config_id']), 
		"config_description"=>(!empty($config) ? $config->description : '')
		);

echo (json_encode($json_reply));
exit();





?>
