    <div id="quick-search-form-wrapper">
    
        <form id="quick-search-form" name="search" action="<?=(defined('QUICK_SEARCH_URL') ? QUICK_SEARCH_URL : '#')?>" method="post">
        <a class="search-icon-inset icon-search-2" onclick='quick_search(); return false;'></a>
        <input type="text" id="quick-search-term" name="term" value=""  placeholder="Type here to start a search.">
        </form>
        
        <ul id="quick-search-results">
        
        
        </ul>
    
    </div>