
<div id="zone-dialog-box" class="alert-box">
    <form name="zone-form" id="zone-form"  method="POST" action="<?=BASE_URL?>save-zone.ajax.php">
        <input type="hidden" name="auth-token" value="<?=AUTH_TOKEN?>"  />
        <input type="hidden" id="zone-id" name="zone-id" value=0  />
        
        <h2 class="bubble-title">New Zone</h2>
        <div class="tiny-yellow-text">Enter a name and choose a list type for the zone you wish to create</div>
        <br/>
        <p class="bubble-text">
                    <div>
                    <small class="text-input-label-3">ZONE NAME</small>
                    <input id="text-field-zone-name" name="zone-name" tip="Please enter a name for the new zone. <small>Use only A-Z, a-z, 0-9, spaces, dots, commas or dashes!</small>" class="placeholder-color-1" type="text" value="" placeholder="" tinyStatus="#zone-name-status"  size=30 />
                    
                    <br /><br />
                    <div id="zone-type-label">ZONE TYPE</div>
                    <label class="radio-button-label-1" for="normal-list-radio">
                    <input type="radio" id="normal-list-radio" name="list-type" value="normal" checked="checked" />Normal 
                    <span class="tiny-yellow-text">-- will contain selected countries only.</span>
                    </label>
                    
                    <br />
                    <label class="radio-button-label-1" for="exclude-list-radio">
                    <input type="radio" name="list-type" id="exclude-list-radio" value="exclude" />Exclude 
                    <span class="tiny-yellow-text">-- will contain ALL but the selected countries.</span>
                    </label>
                    </div>  
  
        </p>

        <br /><br /><br />
        
        <a class="button-1" id="zone-button-cancel" href="javascript:void(0)" onClick="return false;" >cancel</a>
        &nbsp;&nbsp;
        <input type="submit" class="button-1" id="zone-button-ok" value="create new zone" />
        <br /><br/>
        <!-- <small class="bubble-tiny-text">click anywhere to close X</small> -->
    </form>
</div>



