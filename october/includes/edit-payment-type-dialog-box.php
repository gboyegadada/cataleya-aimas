
<div id="payment-type-dialog-box" class="alert-box">
    <form name="payment-type-form" id="payment-type-form"  method="POST" action="<?=BASE_URL?>save-payment-type.ajax.php">
        <input type="hidden" name="auth-token" value="<?=AUTH_TOKEN?>"  />
        <input type="hidden" id="payment-type-id" name="payment-type-id" value=0  />
        
        <h2 class="bubble-title">Edit Payment Type</h2>
        <div class="tiny-yellow-text">Change the display name of this payment option (<span class='payment-type-name-here'>&nbsp;</span>) or disable it.</div>
        <br/>
        <p class="bubble-text">
                    <div>
                    <small class="text-input-label-3">PAYMENT TYPE NAME (display name)</small>
                    <input id="text-field-payment-type-name" name="payment-type-name" tip="Please enter a name for this payment type. <small>This is what the shopper will see at the store during checkout.<br /><br /> Use only A-Z, a-z, 0-9, spaces, dots, commas or dashes!</small>" class="placeholder-color-1" type="text" value="" placeholder="" tinyStatus="#payment-type-name-status"  size=30 />
                    
                    <br /><br />                    
                    
                    
                    
                    <small class="text-input-label-3">MAX PAYABLE AMOUNT <span class="payment-type-base-currency"></span></small>
                    <input id="text-field-payment-type-max-amount" name="payment-type-max-amount" tip="Please enter an amount. <small>This is the maximum amount a shopper can pay per transaction/order.<br /><br /> Use only A-Z, a-z, 0-9, spaces, dots, commas or dashes!</small>" class="placeholder-color-1" type="text" value="" placeholder="" tinyStatus="#payment-type-max-amount-status"  size=30 />
                    
                    <br /><br />                    
                    
                    <small class="text-input-label-3">MIN PAYABLE AMOUNT <span class="payment-type-base-currency"></span></small>
                    <input id="text-field-payment-type-min-amount" name="payment-type-min-amount" tip="Please enter an amount. <small>This is the minimum amount a shopper can pay per transaction/order.<br /><br /> Use only A-Z, a-z, 0-9, spaces, dots, commas or dashes!</small>" class="placeholder-color-1" type="text" value="" placeholder="" tinyStatus="#payment-type-min-amount-status"  size=30 />
                    
                    <br /><br /><br />
                    <div id="payment-type-type-label">ENABLE OR DISABLE PAYMENT TYPE</div>
                    <label class="radio-button-label-1" for="enable-payment-type-radio">
                    <input type="radio" id="enable-payment-type-radio" name="payment-type-status" value=1 checked="checked" />Enable 
                    <span class="tiny-yellow-text">-- will contain selected countries only.</span>
                    </label>
                    
                    <br />
                    <label class="radio-button-label-1" for="disable-payment-type-radio">
                    <input type="radio" name="payment-type-status" id="disable-payment-type-radio" value=0 />Disable 
                    <span class="tiny-yellow-text">-- will contain ALL but the selected countries.</span>
                    </label>
                    </div>  
  
        </p>

        <br /><br /><br />
        
        <a class="button-1" id="payment-type-button-cancel" href="javascript:void(0)" onClick="return false;" >cancel</a>
        &nbsp;&nbsp;
        <input type="submit" class="button-1" id="payment-type-button-ok" value="save changes" />
        <br /><br/>
        <!-- <small class="bubble-tiny-text">click anywhere to close X</small> -->
    </form>
</div>



