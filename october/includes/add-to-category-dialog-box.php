
<div id="category-dialog-box" class="alert-box">

        <h2 class="bubble-title">Category</h2>
        <div class="tiny-yellow-text">Enter a name and choose a list type for the tax class you wish to create</div>
        <br/>
        <div class="dotted-yellow-hor-line tiny-yellow-arrow-down">&nbsp;</div>
        <p class="bubble-text">
                    <br />
                    <!-- <small class="text-input-label-3">CATEGORY</small> -->
                    
                    <select id="select-category-id" class="select-control-1" name="category-id" tip="Please choose a <b>product category</b>. <small></small>" size="1"  >
                        <option value=0 >-- Choose a product category --</option>
                        <?php
                        $Categories = Cataleya\Catalog\Tags::load();
                        
                        foreach ($Categories as $Category):
                        ?>
                        <option value=<?=$Category->getID()?> ><?=$Category->getDescription()->getTitle('EN')?></option>
                        <?php endforeach; ?>
                    </select>
                    
                    
                    <br /><br /><br />
                    
                    
    
                    <a href="javascript:void(0);" id="add-to-category-button" class="blue-button large" >add to category <span class="icon-checkmark-2">&nbsp;</span></a>
        
    
                    <br /> 
  
        </p>

        
        <!--
        <br />
        <div class="dotted-blue-hor-line">&nbsp;</div>
        <br /><br />

        &nbsp;&nbsp;                        
        <a class="button-1" id="tax-class-button-cancel" href="javascript:void(0)" onClick="return false;" >cancel</a>

        -->
        
        <br />
        <small class="bubble-tiny-text">click anywhere to close X</small>
        <br/>
        
</div>



