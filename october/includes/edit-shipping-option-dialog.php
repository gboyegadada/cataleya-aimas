
<div id="edit-shipping-option-dialog-box" class="alert-box">
<a href="javascript:void(0);" class="cancel-button" ><span class="icon-close">&nbsp;</span></a>

    <form name="edit-shipping-option-form" id="edit-shipping-option-form"  method="POST" action="<?=BASE_URL?>save-shipping-option.ajax.php">
        <input type="hidden" name="auth-token" value="<?=AUTH_TOKEN?>"  />
        <input type="hidden" name="store-id" value="<?=$store->getID()?>"  />
        <input type="hidden" id="shipping-option-id" name="shipping-option-id" value=0  />
        
        <h2 class="bubble-title">New Shipping Option</h2>
        <div id="edit-shipping-option-dialog-hint" class="tiny-yellow-text">Enter a name, zone (choose) and a percentage charge for your new tax rate.</div>
        
        <div class="dotted-yellow-hor-line tiny-yellow-arrow-down">&nbsp;</div>
        
        <p class="bubble-text">

                    <div class="fltlft">
                    <small class="text-input-label-3">NAME</small>
                    <input id="text-field-shipping-option-name" name="shipping-option-name" tip="Please enter a <b>name</b> for this shipping option. <small>Use only A-Z, a-z, 0-9, spaces, dots, commas or dashes!</small>" class="placeholder-color-1" type="text" value="" placeholder="Enter a name here." tinyStatus="#shipping-option-name-status"  size=30 />
                    <br /><br />
                    <small class="text-input-label-3">DESCRIPTION</small>
                    <textarea id="text-field-shipping-option-description" name="description" tip="Please enter a <b>description</b> for this shipping option. <small>For example: 'Delivery within 3-5 working days.' <br />Use only A-Z, a-z, 0-9, spaces, dots, commas or dashes!</small>" rows=2 cols="50" class="placeholder-color-1" placeholder="Type description here." ></textarea>
                      
                    <br/><br />
                    </div>  
  
        
                    <div class="fltlft">
                    <small class="text-input-label-3">CARRIER</small>
                    <select id="text-field-shipping-option-carrier-id" class="select-control-1" name="carrier-id" tip="Please choose a <b>shipping carrier</b>. <small>This is the shipping company (or agent) that will make deliveries for the '<?=$store_description->getTitle('EN')?>' store.</small>" size="1"  >
                        <option value=0 >-- Choose a shipping carrier --</option>
                        <?php
                        $carriers = Cataleya\Agent\_::load('CARRIER');
                        
                        foreach ($carriers as $_item):
                        ?>
                        <option value=<?=$_item->getID()?> ><?=$_item->getCompanyName() . ' (' . $_item->getContactFirstName() . ')'?></option>
                        <?php endforeach; ?>
                    </select>
                    <br/><br/>
                    </div> 
        
            
                    <div class="fltlft">
                    <small class="text-input-label-3">SHIPPING ZONE</small>
                    <select id="text-field-shipping-option-zone-id" class="select-control-1" name="zone-id" tip="Please choose a <b>shipping zone</b>. <small>Note: you have to setup a zone first. This is a way to group countries and provinces this shipping option will apply to. For example a zone named 'West Africa' would have Nigeria, Ghana and so on in it. </small>" size="1"  >
                        <option value=0 >-- Choose a shipping zone --</option>
                        <?php
                        $zones = Cataleya\Geo\Zones::load();
                        
                        foreach ($zones as $zone):
                        ?>
                        <option value=<?=$zone->getID()?> ><?=$zone->getZoneName()?></option>
                        <?php endforeach; ?>
                    </select>
                    <br/><br/>
                    </div> 
        

        
                    <div class="fltlft">
                    <small class="text-input-label-3">MAXIMUM DELIVERY DAYS</small>
                    <input id="text-field-shipping-option-delivery-days" name="max-delivery-days" tip="Please enter the maximum expected <b>delivery days</b>. <small></small>" class="placeholder-color-1" type="number"  onkeypress="return validateFloatEntry(event, this);" onkeyup="return revalidateFloatEntry(event, this);" value="" placeholder=""  size=10 />
                    <br/><br/>
                    </div>  
        
                    <div class="clearfloat">&nbsp;</div>
                    
            
                    <div class="fltlft">
                        <small class="text-input-label-3">SHIPPING RATE ( <strong><span class="store-currency-symbol">&#<?=Cataleya\Locale\Currency::load($store->getCurrencyCode())->getUtfCode()?>;</span></strong> )</small>
                    <input id="text-field-shipping-option-rate" name="rate" tip="Please enter a <b>shipping rate</b>. <small></small>" class="placeholder-color-1" type="number"  onkeypress="return validateFloatEntry(event, this);" onkeyup="return revalidateFloatEntry(event, this);" value="" placeholder="" size=5 />
                    <br/><br/>
                    </div>  
                    
                    &nbsp;&nbsp;
                    
                    <div class="fltlft">
                    <small class="text-input-label-3">RATE IS PER</small>
                    <select id="text-field-shipping-option-rate-type" class="select-control-1" name="rate-type" tip="Please select a rate type. <small></small>" size="1"  >
                                 <option value="weight" >Weight</option>
                                 <option value="unit" selected="selected">Unit</option>
                                 <option value="flat" >Flat</option>
                    </select>
                    <br/><br/>
                    </div> 


                    <div class="clearfloat">&nbsp;</div>
        
                    <div class="fltlft">
                    <label for="field-taxable" >
                    &nbsp;&nbsp;
                    <input id="field-taxable" name="is_taxable" class="" type="checkbox" value=true />
                    Require Tax
                    </label>
                    <br/><br/>
                    </div> 
                    
        


        </p>

        <br /><br /><br/>
        
        <div class="dotted-blue-hor-line">&nbsp;</div>
        <br/>
        <a class="button-1" id="edit-shipping-option-button-cancel" href="javascript:void(0)" onClick="return false;" >cancel</a>
        &nbsp;&nbsp;
        <input type="submit" class="button-1" id="edit-shipping-option-button-ok" href="javascript:void(0)" value="add new shipping option" />
        <br /><br/>
    </form>
</div>

