<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<?php
include "includes/meta.php";

?>

<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Error | Admin</title>




<!-- STYLE SHEETS -->
<?php
require_once(INC_PATH.'css_libs.php');
?>

<!-- JS FILES -->
<?php
require_once(INC_PATH.'js_libs.php');
?>

<style type="text/css" media="screen">
    
    .server-response {
        position: absolute;
        
        top: 160px;
        left: 40px;
        width: 900px;
        text-align: left;
        
        margin: 20px 0px;
        color: #555;
        
        font-size: 90%;
    }
    
  

</style>


</head>

<body>

<div class="container">
<div class="header-wrapper absolute">
    <div class="header">

    <div id="page-title">Error</div>   

    </div>
    
<?php include_once INC_PATH.'main.nav.php'; ?>
    
</div>

<div class="content">




<p class="server-response">

<?=defined('ERROR_TEXT') ? ERROR_TEXT : 'An error has occurred while processing your request. Please try again in a few minutes, or contact the administrator if this is a recurring issue.'?> You can reach the system administrator at: <a class="button-3" href="mailto:admin@aimas.com.ng">admin@aimas.com.ng</a>
<br /><br />
Sorry for any inconveniences this may cause.
    

</p>


</div>
</div>








</body>
</html>



<?php

exit ();

?>