
<div id="admin-profile-dialog-box" class="alert-box">
    <form name="admin-profile-form" id="admin-profile-form"  method="POST" action="<?=BASE_URL?>save-admin-profile.ajax.php">
        <input type="hidden" name="auth-token" value="<?=AUTH_TOKEN?>"  />
        <input type="hidden" id="hidden-field-admin-id" name="admin-id" value=0  />
        
        <h2 class="bubble-title">Edit Admin Profile</h2>
        <div class="tiny-yellow-text">
            Change the details and options of this admin profile or disable it.
            <br /><br />
            <?=(IS_SUPER_USER) ? 'Note: This is a super user account (administrator) and cannot be modified from here. To edit your profile go to "My Account" from the home page.' : '.'?>
        </div>
        <br/>
        <p class="bubble-text">
                    <div>
                        
                    <div class="fltlft">
                    <small class="text-input-label-3">FIRST NAME</small>
                    <input id="text-field-admin-fname" name="fname" require="name" tip="Please enter a first name. <small>Use only A-Z, a-z, 0-9, spaces, dots, commas or dashes!</small>" class="placeholder-color-1" type="text" value="" placeholder="First Name"  size=30 />
                    <br /><br />
                    </div>
                        
                    <div class="fltlft">
                    <small class="text-input-label-3">LAST NAME</small>
                    <input id="text-field-admin-lname" name="lname" require="name" tip="Please enter a last name. <small>Use only A-Z, a-z, 0-9, spaces, dots, commas or dashes!</small>" class="placeholder-color-1" type="text" value="" placeholder="Last Name"  size=30 />
                    <br /><br />
                    </div>
                        
                    <div class="fltlft">
                    <small class="text-input-label-3">EMAIL ADDRESS</small>
                    <input id="text-field-admin-email" name="email" require="email" tip="Please enter an email for this admin profile. <small>Use only A-Z, a-z, 0-9, spaces, dots, commas or dashes!</small>" class="placeholder-color-1" type="text" value="" placeholder="Email Address"  size=30 />
                    <br /><br />
                    </div>
                        
                    <div class="fltlft">
                    <small class="text-input-label-3">TELEPHONE</small>
                    <input id="text-field-admin-phone" name="phone" require="phone" tip="Please enter a phone number for this admin profile. <small>Use only digits (0-9). Number must be at least 6 digits long and may start with a (+).</small>" class="placeholder-color-1" type="text" value="" placeholder="Telephone"  size=30 />
                    <br /><br />
                    </div>
                        
                        <div class="dotted-blue-hor-line fltlft">&nbsp;</div>
                        
                    <div class="fltlft">
                    <br />
                    <small class="text-input-label-3">(New password)</small>
                    <input type="password" id="text-field-admin-pass1" name="password" require="password" tip='Please enter a new password. <small>Must be at least 8 characters long, and contain 1 uppercase letter and a number. <br/>- OR -<br/> Use a pass-phrase instead. For example: "I love bvlgari".</small>' size=30 value="Password"  >
                    <br/>
                        <ul class="password-strength-meter" id="password-meter-1">
                            <li class="password-strength-poor password-strength-weak password-strength-good password-strength-excellent">&nbsp;</li>
                            <li class="password-strength-weak password-strength-good password-strength-excellent">&nbsp;</li>
                            <li class="password-strength-good password-strength-excellent">&nbsp;</li>
                            <li class="password-strength-excellent">&nbsp;</li>
                        </ul>
                    <br/>
                    <small class="text-input-label-3">(confirm)</small>
                    <input type="password" id="text-field-admin-pass2" name="password-confirm" require="password-confirm" tip="Please re-type password (must be the same as the first)." size=30 value="password2" >
                    <br /><br/>
                    <label class="radio-button-label-1" for="change-admin-password">
                    <input type="checkbox" id="change-admin-password" name="change-password" value=1 />
                    <span class="tiny-yellow-text">Change Password</span>
                    </label>
                    <br/><br/>
                    </div>
                        
                    <div id="admin-profile-status-wrapper" class="fltlft">
                    <br />
                    <small class="text-input-label-3">ADMIN ROLE</small>
                    <select id="select-field-admin-role-id" class="select-control-1" name="role-id" require="select" tip="Please choose an <b>admin role</b>. <small></small>" size="1"  >
                        <option value=0 >-- Choose an admin role --</option>
                        <?php
                        $_roles = Cataleya\Admin\Roles::load();
                        
                        foreach ($_roles as $_role):
                        if ($_role->hasPrivilege('SUPER_USER')) continue;
                        
                        ?>
                        <option value=<?=$_role->getRoleId()?> ><?=$_role->getDescription()->getTitle('EN')?></option>
                        <?php endforeach; ?>
                    </select>
                    <br/><br/>
                    <div class="dialog-box-section-label">ENABLE OR DISABLE ADMIN PROFILE</div>
                    <label class="radio-button-label-1" for="enable-admin-profile-radio">
                    <input type="radio" id="enable-admin-profile-radio" name="enabled" value=1 checked="checked" />Enable 
                    <span class="tiny-yellow-text">-- unlock this account.</span>
                    </label>
                    
                    <br />
                    <label class="radio-button-label-1" for="disable-admin-profile-radio">
                    <input type="radio" id="disable-admin-profile-radio" name="enabled" value=0 />Disable 
                    <span class="tiny-yellow-text">-- lock this account.</span>
                    </label>
                    
                    </div>
                        
                    </div>  
  
        </p>
        <div class="clearfloat">&nbsp;</div>
        <br />
        <div class="dotted-blue-hor-line">&nbsp;</div>
        <br /><br />
        
        <a class="button-1" id="admin-profile-button-cancel" href="javascript:void(0)" onClick="return false;" >cancel</a>
        &nbsp;&nbsp;
        <input type="submit" class="button-1" id="admin-profile-button-ok" value="save changes" />
        <br /><br/>
        <!-- <small class="bubble-tiny-text">click anywhere to close X</small> -->
    </form>
</div>



