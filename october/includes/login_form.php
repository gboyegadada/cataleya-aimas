<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<?php
include(INC_PATH.'meta.php');
?>
<title>Admin | Login</title>


<!-- JS FILES -->
<?php
require_once(INC_PATH.'js_libs.php');
?>


<!-- STYLESHEETS -->
<?php
require_once(INC_PATH.'css_libs.php');
?>



<style type="text/css" media="all">

/*

CUSTOMER INFO

*/


#header {
	
	border:none;
}


#footer {
	border:none;
	
}


.content {
	
	border:none;
	margin:0px auto;
	padding:0px 0px;
	
	width:980px;
	
	clear:both;
	
}



#login-form-outer-wrapper {
	position:absolute;
	top:-15px;
	left:250px;
	z-index:100;
	
	width:480px;
	height:320px;
	
	border:solid 5px #222; 
	box-shadow: 1px 1px 3px rgba(0,0,0,.6);

	border-radius:15px;	
	-moz-border-radius:15px;	
	-webkit-border-radius:15px;
	
}


#login-icon:before {
	display:block;
	height:100px;
	width:100px;
	
	font-size:300%;
	font-weight: normal;
	font-style: normal;
	
	position:relative;
	left:7px;
	top:4px;
	
	line-height:0%!important;
	color:#2c2c2c!important;
	
}




#login-form-label {
	width:90px;
	height:120px; 

	text-align:right;
	font-size:110%;
	color:#444;
	
	padding:120px 40px 40px 20px;
	margin:0px 0px;
	
	/*padding:10px; */
	
	border-right:dashed 1px #333;
	float:left;

}


#login-form-wrapper {
	list-style:none;
	width:240px;
	height:230px; 

	text-align:left;
	font-size:80%;
	color:#555;
	
	padding:50px 20px 40px 20px;
	margin:0px 30px 0px 0px;
	
	/*padding:10px; */
	
	float:right;

}

#login-form-wrapper li {
	list-style:none;
	width:240px;
	border:none;
	padding:10px 0px 10px 0px;
	margin:0px 0px;
}

#login-form-wrapper li.last-child {
	border-top:dotted 1px rgba(0, 100, 160, .6);
}


#login-form-wrapper small {
	margin:0px 0px 0px 5px;
	color:#444;
	
}

#login-form-wrapper  input[type='text'], 
#login-form-wrapper  input[type='password'] {
	padding:4px 2px;
	margin:0px 5px 0px 0px;
	width:230px;
	
	color:#555;
	
	}

#login-form-wrapper input[type='text']::-webkit-input-placeholder {
	color:rgba(150, 150, 150, .7);
}

#login-form-wrapper input[type='text']:-moz-input-placeholder {
	color:rgba(150, 150, 150, .7);
}

#login-form-wrapper input[type='text']:-ms-input-placeholder {
	color:rgba(150, 150, 150, .7);
	color:#555;
}


#login-errors {
	color:#a00;
}



</style>


</head>

<body>
<!-- BEGIN: META -->
<input type="hidden" id="login_token" value="<?=NEW_LOGIN_TOKEN?>"  />
<!-- END: META -->



<div class="container">

  <div class="content">

  	<div id="header">
    
    
    </div>



<div id="login-form-outer-wrapper">
<div id="login-form-label">

<span id="login-icon" class="icon-locked">&nbsp;</span>
<!-- Login -->
</div>




<!-- CUSTOMER INFO UPDATE WRAPPER -->
<ul id="login-form-wrapper" style="display:block">

<form id="lform-<?=NEW_LOGIN_TOKEN?>" name="login_form" method="post" action="<?=BASE_URL.'login.php'?>" autocomplete="off">
    <input type="hidden" name="auth_token-<?=NEW_LOGIN_TOKEN?>" value="<?=NEW_LOGIN_TOKEN?>"  />
    <input type="hidden" id="lfp-<?=NEW_LOGIN_TOKEN?>" name="fp-<?=NEW_LOGIN_TOKEN?>" value=""  />
    <li>
    <small>Email Address</small>
    <input id="username" type="text" name="username-<?=NEW_LOGIN_TOKEN?>" size=35 placeholder="yourname@example.com"  value="<?=LOGIN_USER?>" />
    <br/>
    </li>
    
    
    <li>
    <small>(Your password)</small>
    <input type="password" name="password-<?=NEW_LOGIN_TOKEN?>" size=35 value=""  />
    <br/><br/>
    
    
    </li>
    
    <li class="last-child">
    <input class="blue-button x-large" type="submit" name="login-<?=NEW_LOGIN_TOKEN?>" value="login">
    
    </li>
    
    <li id="login-errors">
    <?=LOGIN_RESPONSE?>
    </li>


</form>

</ul>


</div>

<div id="footer">
&nbsp;
</div>



</div>

</div>



<script type="text/javascript" >
    $(document).ready(function () {
        $('input#username').focus();
    });

</script>

</body>
</html>
