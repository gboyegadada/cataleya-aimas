
<div id="sale-dialog-box" class="alert-box">

        <h2 class="bubble-title">Sale</h2>
        <div class="tiny-yellow-text">Enter a name and choose a list type for the tax class you wish to create</div>
        <br/>
        <div class="dotted-yellow-hor-line tiny-yellow-arrow-down">&nbsp;</div>
        <p class="bubble-text">
                    <br />
                    <!-- <small class="text-input-label-3">SALE</small> -->
                    
                    <select id="select-sale-id" class="select-control-1" name="sale-id" tip="Please choose a <b>sale</b>. <small></small>" size="1"  >
                        <option value=0 >-- Choose a sale --</option>
                        <?php
                        $Sales = Cataleya\Sales\Sales::load();
                        
                        foreach ($Sales as $Sale):
                        ?>
                        <option value=<?=$Sale->getID()?> ><?=$Sale->getDescription()->getTitle('EN')?></option>
                        <?php endforeach; ?>
                    </select>
                    
                    
                    <br /><br /><br />
                    
                    
    
                    <a href="javascript:void(0);" id="add-to-sale-button" class="blue-button large" >add to sale <span class="icon-checkmark-2">&nbsp;</span></a>
        
    
                    <br /> 
  
        </p>

        
        <!--
        <br />
        <div class="dotted-blue-hor-line">&nbsp;</div>
        <br /><br />

        &nbsp;&nbsp;                        
        <a class="button-1" id="tax-class-button-cancel" href="javascript:void(0)" onClick="return false;" >cancel</a>

        -->
        
        <br />
        <small class="bubble-tiny-text">click anywhere to close X</small>
        <br/>
        
</div>



