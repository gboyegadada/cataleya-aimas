<?php
if (!defined ('IS_ADMIN_FLAG') || IS_ADMIN_FLAG !== TRUE) die("Illegal Access");






/**
 *
 * PRODUCTION MODE
 * ----------------------------------------
 *
 * DEBUG_MODE: FALSE
 * SSL_REQUIRED: TRUE
 *
 *
 *
 * DEV MODE
 * ---------------------------------------
 *
 * DEBUG_MODE: TRUE
 * SSL_REQUIRED: FALSE
 *
 *
 */

 /**
  * Determine if SSL is used.
  *
  * @since 2.6.0
  *
  * @return bool True if SSL, false if not used.
  */
 if (!function_exists(is_ssl)) {
     function is_ssl() {
     	if ( isset($_SERVER['HTTPS']) ) {
     		if ( 'on' == strtolower($_SERVER['HTTPS']) )
     			return true;
     		if ( '1' == $_SERVER['HTTPS'] )
     			return true;
     	} elseif ( isset($_SERVER['SERVER_PORT']) && ( '443' == $_SERVER['SERVER_PORT'] ) ) {
     		return true;
     	}
     	return false;
     }
 }





/**
 * debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use DEBUG_MODE
 * in their development environments.
 */

define('DEBUG_MODE', TRUE);

define('SSL_REQUIRED', TRUE);


// OTHER GLOBAL STUFF
define ('DEFAULT_TIMEZONE', 'Europe/London');
date_default_timezone_set(DEFAULT_TIMEZONE);

define('CURRENT_DT', time());
define('TOKEN_EXP', CURRENT_DT-(60*60)); // After 60 min...
define('SESSION_EXP', (30*60)); // After 30 min...
define('SESSION_REGEN_AFTER', (30 * 60));

define('LOGIN_EXPIRES_AFTER', (30*60)); // After 30 min...
define('SESSION_PREFIX', 'DASH_');
define('SESSION_NAME', 'dash');


define('IP_TREND_THRESHOLD', 10);
define('BROWSER_TREND_THRESHOLD', 4);
define('GEO_LOC_TREND_THRESHOLD', 3);


// MAILCHIMP CREDENTIALS

define('MAILCHIMP_API_KEY', '909349dd7249f3b4d2522cbd5c6ddc52-us3');
define('MAILCHIMP_LIST_ID', '2c3cf7fca1');



// PATHS...
$_abs_path = preg_replace('!includes[\\\|/]?$!', '', dirname(__FILE__));
$_abs_path_shared = preg_replace('!([A-Za-z0-9]+[\\\|/])?includes[\\\|/]?$!', '', dirname(__FILE__));


define ('ADMIN_ROOT_PATH', $_abs_path);
define ('ROOT_PATH', $_abs_path_shared);
define ('INC_PATH', $_abs_path.'includes/');
define ('SHARED_INC_PATH', $_abs_path_shared.'includes/');
define('TEMP_DIR', $_abs_path_shared.'ui/images/catalog/temp/');
define('LIB_PATH', ROOT_PATH.'library/');
define('CAT_DIR', $_abs_path_shared.'ui/images/catalog/');

define('PATH_SKIN', ROOT_PATH.'var/skin');

define('CATALEYA_LOGS_PATH' , ROOT_PATH.'var/logs');
define('CATALEYA_CACHE_PATH', ROOT_PATH.'var/cache');
define('CATALEYA_CACHE_DIR', ROOT_PATH.'var/cache');
define('CATALEYA_PLUGINS_PATH', ROOT_PATH.'app/plugins');
define('PLUGIN_PACKAGES_PATH', ROOT_PATH.'var/packages');

define('APP_STORE_ENDPOINT', 'http://dev.fancypaperplanes.com/appstore/');
/////////// Cataleya INIT ///////
// require_once (SHARED_INC_PATH.'cataleya.init.php');
//Load Cataleya utility class
require ROOT_PATH . 'app/core/Cataleya/Autoloader.php';

//Start the autoloader
Cataleya\Autoloader::registerAutoload();


define('DASH_LANGUAGE_CODE', Cataleya\System::load()->getDashboardLanguageCode());
define('SHOPFRONT_LANGUAGE_CODE', Cataleya\System::load()->getShopFrontLanguageCode());





///////////////////  REGISTER SESSION HANDLER //////////////

if (IS_ADMIN_FLAG === true) {
        if (!$SESS_LIFE = (SESSION_EXP > 3600 ? 900 : SESSION_EXP)) $SESS_LIFE = (SESSION_EXP > 3600 ? 900 : SESSION_EXP);
} else {
        if (!$SESS_LIFE = get_cfg_var('session.gc_maxlifetime')) $SESS_LIFE = SESSION_EXP; //1440;
}

if (defined('DISABLE_SECURE_SESSION') && DISABLE_SECURE_SESSION) { /* don't load session class */ }
else {
    Cataleya\Session::setSessionLifeTime($SESS_LIFE);
    Cataleya\Session::load();
}






/////////// MailChimp INIT ///////
//require_once (SHARED_INC_PATH.'mailchimp.init.php');
require_once (ROOT_PATH.'library/MailChimp/MailChimp.class.php');



/////////// Twig INIT ///////
//require_once (SHARED_INC_PATH.'twig.init.php');
require_once (ROOT_PATH.'library/Twig/Autoloader.php');
Twig_Autoloader::register();

define('TEMPLATES_PATH', (ROOT_PATH.'skin/Dashboard/'));
// Cataleya\Front\Twiggy::getInstance(TEMPLATES_PATH.'/layouts');

$_theme_id = Cataleya\System::load()->getDashboardThemeID();
Cataleya\Front\Twiggy::getInstance(TEMPLATES_PATH . $_theme_id . '/layouts');




/////////// HTMLPurifier INIT ///////
//require_once (SHARED_INC_PATH.'HTMLPurifier.init.php');
require_once (ROOT_PATH . 'library/HTMLPurifier/HTMLPurifier.auto.php');
require_once 'HTMLPurifier.func.php';




// SWIFT MAILER
// require_once (SHARED_INC_PATH.'swiftmailer.init.php');
require_once (ROOT_PATH.'library/swiftmailer/lib/swift_required.php');
Swift::init(function () {

	Swift_DependencyContainer::getInstance()->register('mime.qpcontentencoder')->asAliasOf('mime.nativeqpcontentencoder');

});







// check if cataleya is setup
$_dbh = Cataleya\Helper\DBH::getInstance();


if ($_dbh === NULL)
{
    Header('Location: ' . (SSL_REQUIRED || $is_ssl ? 'https://' : 'http://') . $_SERVER['HTTP_HOST']);
    exit('<center><h2>Redirecting...</h2></center> ');
}



$Core = Cataleya\Core::getInstance();




// URLs //////////////////////////////
define('HOST', $Core->config->host);
define('DOMAIN', HOST);
define('DOMAIN_FULL', (SSL_REQUIRED || $is_ssl  ? 'https://' : 'http://') . HOST);
define('EMAIL_HOST', Cataleya\System\Config::load('mailer.config')->localhost);

$_matches = array ();
if (preg_match('!(?<folder>[A-Za-z0-9_\-]+)[\\\|/]?$!', getcwd(), $_matches) > 0) $_admin_folder = $_matches['folder'];
else    throw new Exception ('Admin directory could not be found. Make sure folder name only has [A-Za-z0-9] !!');

$is_ssl = is_ssl();
define('ROOT_URI', $Core->config->rootURI);
define('ADMIN_ROOT_URI', ROOT_URI . $_admin_folder . '/');
define('SHOP_ROOT', (SSL_REQUIRED || $is_ssl ? 'https://' : 'http://'). HOST . '/' . ROOT_URI);
define('BASE_URL', (SSL_REQUIRED || $is_ssl ? 'https://' : 'http://'). HOST . '/' . ADMIN_ROOT_URI);
define('SECURE_BASE_URL', 'https://' . HOST . '/' . ADMIN_ROOT_URI);
//define('SERVER_STORE_ROOT', $Core->config->storeRoot);




/////////////////// DEFINE PRIVILEGE CONSTANTS /////////////////

$privileges = Cataleya\Admin\Privileges::load();

foreach ($privileges as $privilege)
{
    define ('PRVLG_'.$privilege->getID(), $privilege->getID());

}

unset ($privileges);




/////////////////// REGISTER LISTENERS AND HOOKS ///////////

$_Notifications = Cataleya\Admin\Notifications::load();

foreach ($_Notifications as $_Notification) {
    Cataleya\System\Event::addListener($_Notification);
}


$_WebHooks = Cataleya\Front\WebHooks::load();

foreach ($_WebHooks as $_WebHook) {
    Cataleya\System\Event::addListener($_WebHook);
}

// Register LOGGER(S)...
Cataleya\System\Event::addListener(Cataleya\Helper\Logger::create('system/error'));
Cataleya\System\Event::addListener(Cataleya\Helper\Logger::create('admin/user/created'));
Cataleya\System\Event::addListener(Cataleya\Helper\Logger::create('session/error'));
// Cataleya\System\Event::addListener(Cataleya\Helper\Logger::create('request/requestTokenFailed'));
