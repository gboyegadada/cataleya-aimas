        <?php
        if (isset($PAGE_MENU)) {
            
        ?>
        <ul class="nav-menu" id="page-nav">    
        <?php
            
            foreach ($PAGE_MENU as $anchor) {
                ?>
                <a href="<?=(isset($anchor['href']) ? $anchor['href'] : BASE_URL.'#')?>" onclick="<?=(isset($anchor['onclick']) ? $anchor['onclick'] : 'return true;')?>">
                    <li class="<?=(isset($anchor['icon']) ? $anchor['icon'] : 'icon-arrow-right-8')?>"><?=(isset($anchor['title']) ? $anchor['title'] : 'Untitled')?></li>
                </a>

                <?php

            }
        ?>
        </ul>    
        <?php
        
        }
        
        
        ?>
            
