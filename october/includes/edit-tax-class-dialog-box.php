
<div id="edit-tax-rule-dialog-box" class="alert-box">
    <form name="tax-class-form" id="tax-class-form"  method="POST" action="<?=BASE_URL?>save-tax-class.ajax.php">
        <input type="hidden" name="auth-token" value="<?=AUTH_TOKEN?>"  />
        <input type="hidden" id="tax-class-id" name="tax-class-id" value=0  />
        
        <h2 class="bubble-title">New Tax Class</h2>
        <div class="tiny-yellow-text">Enter a name and choose a list type for the tax class you wish to create</div>
        <br/>
        <div class="dotted-yellow-hor-line tiny-yellow-arrow-down">&nbsp;</div>
        <p class="bubble-text">
                    <div>
                    <br />
                    <small class="text-input-label-3">TAX CLASS NAME</small>
                    <input id="text-field-tax-class-name" name="tax-class-name" tip="Please enter a name for the new tax class. <small>Use only A-Z, a-z, 0-9, spaces, dots, commas or dashes!</small>" class="placeholder-color-1" type="text" value="" placeholder="Type name here." size=50 />
                    
                    <br /><br />
                    <small class="text-input-label-3">TAX CLASS DESCRIPTION</small>
                    <textarea id="text-field-tax-class-description" name="tax-class-description" tip="Please enter a description for the new tax class. <small>Use only A-Z, a-z, 0-9, spaces, dots, commas or dashes!</small>" rows=5 cols="50" class="placeholder-color-1" placeholder="Type description here." ></textarea>
                    
                    </div>  
  
        </p>

        <br />
        <div class="dotted-blue-hor-line">&nbsp;</div>
        
        <br /><br />
        
        <a class="button-1" id="tax-class-button-cancel" href="javascript:void(0)" onClick="return false;" >cancel</a>
        &nbsp;&nbsp;
        <input type="submit" class="button-1" id="tax-class-button-ok" value="create new tax class" />
        <br /><br/>
    </form>
</div>



