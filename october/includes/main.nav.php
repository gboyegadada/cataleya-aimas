    <div id="main-nav-wrapper">
        <a id="main-nav-button" class="icon-list" href="<?=BASE_URL?>#" >menu</a>
        <ul class="nav-menu" id="main-nav">
            <a href="<?=BASE_URL?>"><li class="icon-home-4">home</li></a>
            <a href="<?=BASE_URL.'browse.customers.php'?>"><li class="icon-users">Customers</li></a>
            <a href="<?=BASE_URL.'landing.stores.php'?>"><li class="icon-location">Stores</li></a>
            
            <li class="divider"></li>
            
            <a href="<?=BASE_URL.'landing.catalog.php'?>"><li class="icon-tag">Catalog</li></a>
            <a href="<?=BASE_URL.'landing.orders.php'?>"><li class="icon-cart">Orders</li></a>
            
            <?php if (IS_SUPER_USER || $adminUser->getRole()->hasPrivilege('VIEW_ADMIN_PROFILES')): ?> 
            <a href="<?=BASE_URL.'profiles.admin.php'?>"><li class="icon-user-3">Employees</li></a>
            <?php endif ?>
            
            <li class="divider"></li>
            
            <a href="<?=BASE_URL.'#settings'?>"><li class="icon-settings">Settings</li></a>
            <!-- <a href="<?=BASE_URL.'landing.customers.php'?>"><li class="icon-pinterest">Social</li></a> -->
            <a href="<?=BASE_URL.'logout.php'?>"><li class="icon-locked">Logout</li></a>
        </ul>
        
    </div>
