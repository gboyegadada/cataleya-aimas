<?php

// DEBUG MODE...
define('OFOFO', (defined('DEBUG_MODE') && DEBUG_MODE === TRUE));



// UNIVERSAL ERROR HANDLER (JUST ERROR MESSAGES)
function _catch_error($message, $line = 0, $die = TRUE, $safe = FALSE) {
	
    
    
    if (!OFOFO) $line = '';
    
    if (OFOFO || $safe) 
    {
        $msg_str = (is_array($message)) ? ('@ ' . implode(', ', $message) . ' @') : $message;
        if (OFOFO) $msg_str .= ' (at line: ' . $line . ')';
    }
    else {
        $msg_str = "An error has occurred while processing your request. Please try again in a few minutes, or contact the administrator if this is a recurring issue.";
    }
    
	

    switch (OUTPUT) {
            case 'JSON':
                    _json_error($msg_str, $line, $die);
            break;
        
            case 'AJAX_JSON':
                    _json_error($msg_str, $line, $die);
            break;
        
            case 'PLAIN_TEXT':
                    _text_error($msg_str, $line, $die);
            break;
        
            case 'AJAX_HTML':
                    _text_error($msg_str, $line, $die);
            break;
        
            default:
                    _html_error($msg_str, $line, $die);
            break;
    }
			
			
    if (isset($die) && $die === true) {

    exit(1); // exit with error code
    }
	
}



// OUTPUT ERROR AS HTML...
function _html_error($message, $line, $die) {
    
    define ('ERROR_TEXT', $message);
    
    include_once INC_PATH.'error.php';
    
    // Note: There's an exit code in error.php include as well.
    if (isset($die) && $die != true) return;
	else exit(1); // exit with error code

}


// OUTPUT ERROR AS PLAIN TEXT...
function _text_error($message, $line, $die) {
	
	
if (isset($die) && $die != true) { echo $message; return; }
	else die($message); // exit with error code
}



// SEND ERROR MESSAGE TO CLIENT AS JSON...
function _json_error ($message, $line = 0, $die = true) {
$msg_str = (is_array($message)) ? ('@ ' . implode(', ', $message) . ' @') : $message;

$line = (OFOFO && isset($line)) ? $line : '';
$msg_str = (OFOFO) ? $msg_str : '';

$json_reply = array (
		"status"=>'error', 
		"line"=>$line, 
		"message"=>$msg_str
		);

echo (json_encode($json_reply));
if (isset($die) && $die != true) return;
	else exit(1); // exit with error code
	
	
}


// USE AFTER decode_json ...
function get_json_last_error () {
	
$_json_last_error = '';
	
switch (json_last_error()) {
        case JSON_ERROR_NONE:
            $_json_last_error = ' - No errors';
        break;
        case JSON_ERROR_DEPTH:
            $_json_last_error = ' - Maximum stack depth exceeded';
        break;
        case JSON_ERROR_STATE_MISMATCH:
            $_json_last_error = ' - Underflow or the modes mismatch';
        break;
        case JSON_ERROR_CTRL_CHAR:
            $_json_last_error = ' - Unexpected control character found';
        break;
        case JSON_ERROR_SYNTAX:
            $_json_last_error = ' - Syntax error, malformed JSON';
        break;
        case JSON_ERROR_UTF8:
            $_json_last_error = ' - Malformed UTF-8 characters, possibly incorrectly encoded';
        break;
        default:
            $_json_last_error = ' - Unknown error';
        break;
    }

return $_json_last_error;

}



