<?php

// RETURNS TRUE IF SUSPICIOUS POST DATA IS FOUND [ OR ] FALSE IF NONE.
function anySuspects() {
    
global $WHITE_LIST, $_CLEAN, $_POST, $INPUT_TYPES;


$INPUT = ($_SERVER['REQUEST_METHOD'] === 'POST') ? $_POST : $_GET;

if (!defined('LOGIN_TOKEN')) define ('LOGIN_TOKEN', '0000');

if (!isset($WHITE_LIST) || !isset($_CLEAN)) return true;

// Check if INPUT keys are ALL expected AND MADE IT THROUGH...



        // Pre process confirm params
        $_CLEAN['confirm_token'] = isset($_POST['confirm_token']) ? filter_var($_POST['confirm_token'], FILTER_SANITIZE_STRIPPED) : '';
        $_CLEAN['whisper'] = isset($_POST['whisper']) ? filter_var($_POST['whisper'], FILTER_SANITIZE_STRIPPED) : '';
        //$_CLEAN['rt'] = isset($_POST['rt']) ? filter_var($_POST['rt'], FILTER_VALIDATE_REGEXP, array('options'=>array('regexp' => '/^[A-Za-z0-9,\-]{20}$/'))) : '';
        //$_CLEAN['fp'] = isset($_POST['fp']) ? filter_var($_POST['fp'], FILTER_VALIDATE_REGEXP, array('options'=>array('regexp' => '/^[A-Za-z0-9]{20,100}$/'))) : '';

	// Check for keys we're NOT expecting...
	foreach ($INPUT as $exp_key=>$exp_val) {
		if (in_array($exp_key, $WHITE_LIST) || $exp_key === 'rt' || $exp_key === 'fp' || $exp_key === 'confirm_token' || $exp_key === 'whisper') continue;
		else return $exp_key . ' - unexpected'; // One agbero dey here.
	}
	// Check for values that didn't make it through the filter...
	foreach ($_CLEAN as $exp_key=>$exp_val) {
		if (($exp_val !== FALSE && $exp_val !== NULL) || $exp_key === 'login-'.LOGIN_TOKEN || $exp_key === 'cancel-'.LOGIN_TOKEN) continue;
                else if ($exp_val === FALSE && key_exists($exp_key, $WHITE_LIST) && is_bool ($WHITE_LIST[$exp_key])) continue;
		else return $exp_key . ' - failed'; // One agbero dey here.
	}
	
	return false; // No suspects found.
}



// BOMB SESSION
function bombSession ($_debug = NULL) {
                
                // DEBUG MODE...
                if (!defined('OFOFO')) define('OFOFO', (defined('DEBUG_MODE') && DEBUG_MODE === TRUE));

		// 1. Log request data (as a flag)...
				// IP
				// user_id
				// Date-time
				// User Agent
				// Page being accessed
				
		// 2. Destroy session...
		// 3. Auto Logout any active user...
	
		switch (OUTPUT) {
			
			case 'JSON':
				$json_reply = array (
				"status"=>'bomb', 
				"message"=> ("Session expired. " . ((OFOFO && $_debug !== NULL) ? $_debug : '')), 
                                "debug" =>  ((OFOFO) ? $_POST : array())
				);

				echo (json_encode($json_reply));
				exit();
			break;
			
			case 'PLAIN_TEXT':
				redirect (BASE_URL . 'login.php');
				exit('Session expired. ' . ((OFOFO && $_debug !== NULL) ? $_debug : ''));
			break;
			
			default:
				Header('Location: ' . BASE_URL . 'login.php');
				exit('<center><h2>Session expired. ' . ((OFOFO && $_debug !== NULL) ? $_debug : '') . '</h2></center> ');
			break;
		}
	
}




function validate_auth_token () {
	global $_CLEAN;
        if (
                !isset($_CLEAN['auth_token']) 
                && !isset($_POST['auth_token']) 
                && !isset($_CLEAN['auth-token']) 
                && !isset($_POST['auth-token'])
                
                ) _catch_error('Access Denied', __LINE__, true);	
        
        
        if (isset($_CLEAN['auth_token'])) $_token = $_CLEAN['auth_token'];
        else if (isset($_CLEAN['auth-token'])) $_token = $_CLEAN['auth-token'];
        else if (isset($_POST['auth_token'])) $_token = $_POST['auth_token'];
        else if (isset($_POST['auth-token'])) $_token = $_POST['auth-token'];
        

	// VALIDATE AUTH TOKENS...
	if(!isset($_SESSION[SESSION_PREFIX.'AUTH_TOKEN']) || $_token != $_SESSION[SESSION_PREFIX.'AUTH_TOKEN']) {
		
	_catch_error('Access Denied', __LINE__, true);	
	
	}

}



function show_login ($_response = '', $_user = '') {
	global $_CLEAN;
	
	

	define ('NEW_LOGIN_TOKEN', generateToken());
	$_SESSION[SESSION_PREFIX.'LOGIN_TOKEN'] = NEW_LOGIN_TOKEN; // SECURITY TOKEN...

	define ('LOGIN_RESPONSE', $_response);
	define ('LOGIN_USER', $_user);
		
	include "includes/login_form.php";
	exit();
	
	
}


function generateToken ($_timestamp = TRUE)
{
    $_ts = ($_timestamp) ? ('-' . CURRENT_DT) : '';
    return (sha1( uniqid(rand(), TRUE) ) . $_ts);
}






/*
 * [ digest ]
 * 
 */


function digest ($salt = '', $timestamp = FALSE) 
{
    if ($salt === '') 
    {
        $_keys = array ('hushpuppys', 'sagiterrian', 'olodo', 'dondeymad', 'goshitinyourpants', 'megalomeniac', 'nacciruomoalata');        
        $salt = $_keys[rand(0, count($_keys)-1)];
        
    }
    return (sha1(strval(rand(0,microtime(true))) . $salt . strval(microtime(true)))) . (($timestamp) ? '-' . CURRENT_DT : '');
}



/*
 * 
 *  function: [ confirm ]
 * ___________________________________________________________________________
 * 
 * Used for AJAX/JSON request that require confirmation or password auth.
 * 
 */

function confirm ($message = 'Please confirm...', $require_password = FALSE)
{
    $do_confirm = FALSE;
    $_confirm = Cataleya\Helper::getConfirmToken();
    $_whisper = Cataleya\Helper::getWhisper();
    $_rt = Cataleya\Helper::getRequestToken();
    
    // Check password
    if ($require_password && ($_whisper === '' || !Cataleya\Front\Dashboard\User::getInstance()->confirmPassword($_whisper)))
    {
        $do_confirm = TRUE;
    }
    
    
    // Check confirm_token
    if ($_confirm === '')
    {
        $do_confirm = TRUE;
    }  
    
    
    
    // Send confirm request ?
    
    if ($do_confirm)
    {
        

        // OUTPUT...
        $json_reply = array (
                        "status" => ($require_password) ? 'require_password' : 'confirm', 
                        "message" => $message, 
                        "rt"    =>  $_rt, 
                        "confirm_token" => digest('', TRUE)
                        );
        
        
        $_SESSION[SESSION_PREFIX.'CONFIRM_TOKEN'] = $json_reply['confirm_token'];
        
        
        
        // OUTPUT...
        echo (json_encode($json_reply));
        exit();  
    }
    
    
    
    
    
 

    // Validate confirm parameters (make token and target_id match)
    if (isset($_SESSION[SESSION_PREFIX.'CONFIRM_TOKEN']))
    {
        // Token stored in session data
        list($token, $timestamp) = explode('-', $_SESSION[SESSION_PREFIX.'CONFIRM_TOKEN']);
        $timestamp = intval($timestamp);
        
        unset ($_SESSION[SESSION_PREFIX.'CONFIRM_TOKEN']);
        
        // Token supplied by client
        list($req_token, $req_timestamp) = explode('-', $_confirm);
        $req_timestamp = intval($req_timestamp);
        
        // Token is invalidated if older than 10 minutes
        $age = 60*5;
        
        // Validate token...
        if ($req_token === $token && $req_timestamp === $timestamp && (time() - $timestamp) < $age)
        {
            return true;

        } else {

            // ERROR...

            if (isset($_SESSION[SESSION_PREFIX.'CONFIRM_TOKEN'])) unset ($_SESSION[SESSION_PREFIX.'CONFIRM_TOKEN']);

            _catch_error('Something went wrong! Please try again. (Hint: You have to confirm the requested action within 5 mins.)', __LINE__, true);

         }



    }

    

    // IF NOTHING ADDS UP: DO ERROR...
    if (isset($_SESSION[SESSION_PREFIX.'CONFIRM_TOKEN'])) unset ($_SESSION[SESSION_PREFIX.'CONFIRM_TOKEN']);
    _catch_error('Something went wrong! Please try again.', __LINE__, true);
    
    return false; // This won't be executed of course...


    
}





/**
 * 
 * This is to make sure the same request is NOT processed twice.
 * I suspect there is a better way to do this but we'll do this in 
 * the time being.
 * 
 */
function checkRequestToken ($_rt = NULL, $_die = TRUE) 
{

        if (empty($_rt)) $_rt = Cataleya\Helper::getRequestToken();
        else if (empty($_rt)) return NULL;
        
        
        // CHECK IF REQUEST HASN'T ALREADY BEEN PROCESSED
        if (!empty($_rt) && !in_array($_rt, $_SESSION[SESSION_PREFIX.'REQUEST_TOKENS'])) 
        {
            // Fresh request token (a 20 char UUID generated in JS with [ Math.uuid.js ] )
            $_rt = filter_var($_rt, FILTER_VALIDATE_REGEXP, array('options'=>array('regexp' => '/^[A-Za-z0-9,\-]{20}$/')));

            if ($_rt !== FALSE) 
            {
                $_SESSION[SESSION_PREFIX.'REQUEST_TOKENS'][] = $_rt;
               //  Cataleya\Helper\Logger::getInstance()->log('request.tokens.log', 'set: '.$_rt.' by '. $_SERVER['SCRIPT_NAME']);
                
                return TRUE;
            }
            
            // Flag if [rt] is set but invalid...
            
            Cataleya\System\Event::notify(
                    'request/requestTokenFlagged', 
                    NULL, 
                    array(
                        'logger_id'=>'request.tokens.log', 
                        'blob'=> '[ FLAGGED ] in '. $_SERVER['SCRIPT_NAME']
                    ));
            
            return NULL;
        }

        else if (!empty($_rt) && in_array($_rt, $_SESSION[SESSION_PREFIX.'REQUEST_TOKENS'])) 
        {
            // This request has already been processed
            
            Cataleya\System\Event::notify(
                    'request/requestTokenFailed', 
                    NULL, 
                    array(
                        'logger_id'=>'request.tokens.log', 
                        'blob'=> 'rt-failed (request already processed): '.$_rt.' by '. $_SERVER['SCRIPT_NAME']
                    ));
                
            if ($_die) die(0);
            else return FALSE;

        } 

        elseif (empty($_rt)) 
        {
            // No request token
            // You might want to do something 'extra secure' here though ;)
            
            
        }
        
        return NULL;



}




/**
 * 
 * A 'finger print' is a unique signature generated ONCE at LOGIN by the client 
 * and sent along with all AJAX requests in the request headers.
 *  
 * 
 * @param string $_fp
 * @param boolean $_die
 * @return boolean
 * 
 */
function checkFingerPrint ($_fp = NULL, $_die = TRUE) 
{

        if (empty($_fp)) $_fp = Cataleya\Helper::getFingerPrint();
        else if (empty($_fp)) return NULL;

        
        
        // CHECK IF REQUEST HASN'T ALREADY BEEN PROCESSED
        if (!empty($_fp) && !empty($_SESSION[SESSION_PREFIX.'FINGER_PRINT'])) 
        {

            if ($_fp === $_SESSION[SESSION_PREFIX.'FINGER_PRINT']) 
            {

                // Cataleya\Helper\Logger::getInstance()->log('request.tokens.log', 'set: '.$_fp.' by '. $_SERVER['SCRIPT_NAME']);
                
                return TRUE;
            }
            
            else {
                // Flag if [fp] is set but invalid...
                $blob = '[ FLAGGED ] new browser finger print ('.$_fp.') failed to match original ('.$_SESSION[SESSION_PREFIX.'FINGER_PRINT'].') in '. $_SERVER['SCRIPT_NAME'];
                
                Cataleya\System\Event::notify(
                        'request/fingerPrintFailed', 
                        NULL, 
                        array(
                            'logger_id'=>'fp.failure.log', 
                            'blob'=> $blob
                        ));
                
                return NULL;
            }
        }

        else if (empty($_SESSION[SESSION_PREFIX.'FINGER_PRINT'])) 
        {

            $blob = 'finger print missing in session!! This was also sent in the request: '.$_fp.' by '. $_SERVER['SCRIPT_NAME'];
            Cataleya\System\Event::notify('request/missingFingerPrint', NULL, 
                    array(
                        'logger_id'=>'missing-fingerprint.log', 
                        'blob'=> $blob
                    ));
            
            if ($_die) 
            {
                if (isset($_SESSION[SESSION_PREFIX.'ADMIN_ID']) || isset($_SESSION[SESSION_PREFIX.'IS_ADMIN'])) 
                {
                    // Something is very wrong!!
                    // do something 'extra secure' here
                    $blob = 'finger print missing in session!! This was also sent in the request: '.$_fp.' by '. $_SERVER['SCRIPT_NAME'];
                    Cataleya\System\Event::notify('request/missingFingerPrint', NULL, 
                        array(
                            'logger_id'=>'missing-fingerprint.log', 
                            'blob'=> $blob
                        ));
                                    
                    header('HTTP/1.1 406 Not Acceptable');
                    exit(0);
                }
                
                else if ($_SERVER['SCRIPT_NAME'] != SERVER_ROOT . ADMIN_ROOT . 'login.php')
                {
                    Cataleya\Session::kill_session(SESSION_NAME);
                    bombSession();
                }
            }
            else return FALSE;

        } 

        
        return NULL;



}






// FORBIDDEN

function forbidden () 
{
    
    
    
    switch (OUTPUT) {
            case 'JSON':
            $json_data = array (
                            'status' => 'forbidden', 
                            'message' => 'You do not have permission to perform this action.'
                            );
            echo json_encode($json_data);
            break;
        
            case 'PLAIN_TEXT':
                die ('x_x');
            break;
        
            default:
                include_once INC_PATH.'forbidden.php';
            break;
    }
    
    

    exit();
}






/*
 * 
 * [ validationFailed ]
 * ______________________________________________________________
 * 
 * 
 */


function validationFailed (array $_failedItems) 
{
    // Output...
    $json_data = array (
                    'status' => 'Ok', 
                    'message' => 'Validation failed!',
                    'failedValidation' => TRUE, 
                    'failedItems'  => $_failedItems 
                    );
    echo json_encode($json_data);
    exit();
}



