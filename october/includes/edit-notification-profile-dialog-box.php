<!-- EDIT NOTIFICATION PROFILE DIALOG BOX -->


<div id="notification-profile-dialog-box" class="alert-box">
        
    <h2 class="bubble-title">Edit Notification Profile (<span class="notification-profile-name"></span>)</h2>
    
    <div class="tiny-yellow-text">
        Change the details and options of this notification profile or disable it.
        <br />
    </div>
    <br />
    
    <div class="dotted-yellow-hor-line tiny-yellow-arrow-down"></div>
    <br />
    
    <form name="notification-profile-form" id="notification-profile-form"  method="POST" action="<?=BASE_URL?>save-notification-profile.ajax.php">
        <input type="hidden" name="auth-token" value="<?=AUTH_TOKEN?>"  />
        <input type="hidden" id="notification-profile-dialog-field-id" name="notification-id" value=0  />
        <input type="hidden" id="notification-profile-dialog-field-keyword" name="keyword" value=''  />

        <p class="bubble-text">
            
        <div id="dialog-box-left-col" class="fltlft">
                        
                    <div class="fltlft">
                        
                    <small class="text-input-label-3">TITLE</small>
                    <input id="text-field-profile-title" class="dialog-wide-input" name="title" require="name" tip="Please enter a title for this notification profile. <small>Use only A-Z, a-z, 0-9, spaces, dots, commas or dashes!</small>" class="placeholder-color-1" type="text" value="" placeholder="Title"  size=30 />
                    <br /><br />
                    <small class="text-input-label-3">DESCRIPTION</small>
                    <textarea id="text-field-profile-description" class="dialog-wide-input" name="description" require="name" tip="Please enter a description for the new tax class. <small>Use only A-Z, a-z, 0-9, spaces, dots, commas or dashes!</small>" rows=2 cols="50" class="placeholder-color-1" placeholder="Type description here." ></textarea>
                    <br /><br />

                    <small class="text-input-label-3">ENTITY NAME</small>
                    <input id="text-field-profile-entity-name" class="dialog-wide-input" name="entity-name" require="name" tip="Please enter a first name. <small>Use only A-Z, a-z, 0-9, spaces, dots, commas or dashes!</small>" class="placeholder-color-1" type="text" value="" placeholder="Entity Name"  size=30 />
                    <br /><br />
                    
                    <small class="text-input-label-3">ENTITY NAME PLURAL</small>
                    <input id="text-field-profile-entity-name-plural" class="dialog-wide-input" name="entity-name-plural" require="name" tip="Please enter a last name. <small>Use only A-Z, a-z, 0-9, spaces, dots, commas or dashes!</small>" class="placeholder-color-1" type="text" value="" placeholder="Entity Name - Plural"  size=30 />
                    <br /><br />
                    </div>  
        
                    
                    <div class="fltlft">
                    <small class="text-input-label-3">NOTIFICATION INTERVAL</small>
                    <input id="text-field-profile-send-after-period" name="send-after-period" require="integer" tip="Please enter the maximum expected <b>delivery days</b>. <small></small>" class="placeholder-color-1" type="number"  onkeypress="return validateFloatEntry(event, this);" onkeyup="return revalidateFloatEntry(event, this);" value="1" placeholder=""  size=10 />
                    <br/><br/>
                    </div>  
        
                    
                    <div class="fltlft">
                    <small class="text-input-label-3">&nbsp;</small>
                    <select id="text-field-profile-send-after-interval" class="select-control-1" name="send-after-interval" tip="Please select a rate type. <small></small>" size="1"  >
                                 <option value="minute" >Minute</option>
                                 <option value="hour" >Hour</option>
                                 <option value="day" selected>Day</option>
                                 <option value="week" >Week</option>
                                 <option value="month" >Month</option>
                                 <option value="year" >Year</option>
                    </select>
                    <br/><br/>
                    </div> 
                    
                    
                        
                    <div class="clearfloat">&nbsp;</div>   
                    
                    <div class="fltlft">
                    <small class="text-input-label-3">NOTIFY AFTER INCREMENT</small>
                    <input id="text-field-profile-send-after-increments" name="send-after-increments" require="integer" tip="Please enter the maximum expected <b>delivery days</b>. <small></small>" class="placeholder-color-1" type="number"  onkeypress="return validateFloatEntry(event, this);" onkeyup="return revalidateFloatEntry(event, this);" value=10 placeholder=""  size=10 />
                    <br/><br/>
                    </div>  
        
                    <br />

        
        </div>
        
        
        <div id="dialog-box-right-col" class="fltlft">
            
            
            <ul id="dialog-recipient-list">
                <li class="header icon-user-2">&nbsp;Recipients</li>
    
                <li class="sub-header">
                    &nbsp;
                    <span class="icon-email">&nbsp;</span>
                    &nbsp;&nbsp;
                    <span class="icon-flag-2">&nbsp;</span>
                </li>
                

            </ul>

            
        </div>
        
        
        <div class="clearfloat">&nbsp;</div>
        
        <div class="dotted-blue-hor-line"></div>
        <br />
        <a class="button-1" id="notification-profile-cancel-button" href="javascript:void(0)" onClick="return false;" >cancel</a>
        &nbsp;&nbsp;
        <input type="submit" class="button-1" id="notification-profile-ok-button" value="done" />
        <br /><br/>
        
        
        </p>


    </form>
</div>
