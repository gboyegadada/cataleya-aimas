<?php



define ('OUTPUT', 'JSON'); // Whether to output as JSON or HTML (especially errors)...


// LOAD APPLICATION TOP...
require_once ('app_top.php');





/*
 * 
 * TO PERFORM THIS OPERATION, USER MUST BE AN ADMINISTRATOR
 * 
 */


if (!IS_SUPER_USER && !$adminUser->is('STORE_OWNER')  && !$_admin_role->hasPrivilege('DELETE_SHIPPING_OPTION')) _catch_error('You do have permission to perform this action.', __LINE__, true);




$WHITE_LIST = array(
						'auth_token', 
						'shipping_option_id'
						);





// DIE IF INPUT IS NOT VIA POST METHOD...
if ($_SERVER['REQUEST_METHOD'] != 'POST') _catch_error('POST METHOD ONLY PLEASE. NOW GET.', __LINE__, true);

// SANITIZE INPUT DATA...
$_CLEAN = filter_input_array(INPUT_POST, array(
                                                    'auth_token'		=>	FILTER_SANITIZE_STRIPPED, 
                                                    'shipping_option_id'	=>	FILTER_VALIDATE_INT

                                                    )
										
					);


// Check if INPUT keys are ALL expected AND MADE IT THROUGH...
if (anySuspects()) _catch_error('Error processing white_list!', __LINE__, true);



// VALIDATE AUTH TOKEN...
validate_auth_token ();


// Load 
$ShippingOption = Cataleya\Shipping\ShippingOption::load($_CLEAN['shipping_option_id']);


// Check if shipping option exists...
if ($ShippingOption === NULL)
{
        if (isset($_SESSION[SESSION_PREFIX.'CONFIRM_PARAMS'])) unset ($_SESSION[SESSION_PREFIX.'CONFIRM_PARAMS']);
        
        // ERROR...
        _catch_error('Shipping option not found.', __LINE__, true);

}


// Confirm delete action
$message = 'Are you sure you want to remove this shipping option (' . $ShippingOption->getDescription()->getTitle('EN') . ')?';
confirm($message, FALSE);

// retrieve info about shipping option
$ShippingOption_info = array (
        'id'   =>  $ShippingOption->getID(), 
        'name'  =>  $ShippingOption->getDescription()->getTitle('EN'), 
        'description'  =>  $ShippingOption->getDescription()->getText('EN'),  
        'deliveryDays'  =>  $ShippingOption->getMaxDeliveryDays(),  
        'rateType'  =>  $ShippingOption->getRateType(), 
        'rate'  =>  $ShippingOption->getRate()->getValue($ShippingOption->getStoreId())
    );


// retrieve shipping options (collection)
$ShippingOptions = Cataleya\Shipping\ShippingOptions::load($ShippingOption->getStore());



// Delete shipping option (if we make it past the 'confirm' function)...
$ShippingOption->delete();





// Remove token...
if (isset($_SESSION[SESSION_PREFIX.'CONFIRM_TOKEN'])) unset ($_SESSION[SESSION_PREFIX.'CONFIRM_TOKEN']);




// OUTPUT...
 $json_reply = array (
                 "status" => 'ok', 
                 "message" => 'Shipping option deleted.', 
                 "ShippingOption" => $ShippingOption_info, 
                 "population" => $ShippingOptions->getPopulation()
                 );


 echo (json_encode($json_reply));
 exit();  

?>
