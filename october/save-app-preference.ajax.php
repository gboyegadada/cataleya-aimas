<?php

define ('OUTPUT', 'JSON'); // Whether to output as JSON or HTML (especially errors)...


// LOAD APPLICATION TOP...
require_once ('app_top.php');





// VALIDATE AUTH TOKEN...
validate_auth_token ();


$App = Cataleya\System::load();



switch ($_POST['who']) 
{

    case 'sku-code':
        $_val = filter_var($_POST['what'], FILTER_VALIDATE_BOOLEAN, FILTER_NULL_ON_FAILURE);

        if ($_val !== NULL) 
        {
            ($_val) ? $App->enableSKU() : $App->disableSKU();

        }
    break;

    case 'isbn-code':
        $_val = filter_var($_POST['what'], FILTER_VALIDATE_BOOLEAN, FILTER_NULL_ON_FAILURE);

        if ($_val !== NULL) 
        {
            ($_val) ? $App->enableISBN() : $App->disableISBN();

        }
    break;


    case 'upc-code':
        $_val = filter_var($_POST['what'], FILTER_VALIDATE_BOOLEAN, FILTER_NULL_ON_FAILURE);

        if ($_val !== NULL) 
        {
            ($_val) ? $App->enableUPC() : $App->disableUPC();

        }
    break;


    case 'ean-code':
        $_val = filter_var($_POST['what'], FILTER_VALIDATE_BOOLEAN, FILTER_NULL_ON_FAILURE);

        if ($_val !== NULL) 
        {
            ($_val) ? $App->enableEAN() : $App->disableEAN();

        }
    break;

    

    case 'dashboard-theme-id':
        $_val = Cataleya\Helper\Validator::foldername($_POST['what']);

        if (!empty($_val)) 
        {
            $_Theme = Cataleya\Front\Dashboard\Theme::load($_val);
            if ($_Theme === NULL) Cataleya\Helper\ErrorHandler::getInstance()->triggerException("Theme could not be saved.." . __LINE__);
            
            $App->setDashboardTheme($_Theme);

        }
    break;

    case 'shopfront-theme-id':
        $_val = Cataleya\Helper\Validator::foldername($_POST['what']);

        if (!empty($_val))  
        {
            $_Theme = Cataleya\Front\Shop\Theme::load($_val);
            if ($_Theme === NULL) Cataleya\Helper\ErrorHandler::getInstance()->triggerException("Theme could not be saved.." . __LINE__);
            
            $App->setDefaultShopFrontTheme($_Theme);

        }
    break;



    case 'default-product-tax-class':
        $_val = filter_var($_POST['what'], FILTER_VALIDATE_INT, FILTER_NULL_ON_FAILURE);
        
        if ($_val === 0) 
        {
            $App->unsetDefaultProductTaxClass();
        }
        else if ($_val !== NULL) 
        {

            $_taxClass = Cataleya\Tax\TaxClass::load($_val);
            if ($_taxClass === NULL) Cataleya\Helper\ErrorHandler::getInstance()->triggerException("TaxClass not found on line " . __LINE__);

            else $App->setDefaultProductTaxClass($_taxClass);

        }
    break;



    case 'default-shipping-tax-class':
        $_val = filter_var($_POST['what'], FILTER_VALIDATE_INT, FILTER_NULL_ON_FAILURE);

        if ($_val === 0) 
        {
            $App->unsetDefaultShippingTaxClass();
        }
        else if ($_val !== NULL) 
        {

        $_taxClass = Cataleya\Tax\TaxClass::load($_val);
        if ($_taxClass === NULL) Cataleya\Helper\ErrorHandler::getInstance()->triggerException("TaxClass not found on line " . __LINE__);

        else $App->setDefaultShippingTaxClass($_taxClass);

        }
    break;







}





// Output...
$json_data = array (
                'status' => 'Ok', 
                'message' => 'Preference saved.'
                );
echo json_encode($json_data);
exit();








