<?php



define ('OUTPUT', 'JSON'); // Whether to output as JSON or HTML (especially errors)...


// LOAD APPLICATION TOP...
require_once ('app_top.php');




$WHITE_LIST = array(
						'auth_token',  
                                                'store_id', 
						'product_id', 
                                                'do', 
                                                'data'
						);





// DIE IF INPUT IS NOT VIA POST METHOD...
if ($_SERVER['REQUEST_METHOD'] != 'POST') _catch_error('POST METHOD ONLY PLEASE. NOW GET.', __LINE__, true);

// SANITIZE INPUT DATA...
$_CLEAN = filter_input_array(INPUT_POST, array(
                                                    'auth_token'		=>	FILTER_SANITIZE_STRIPPED,
                                                    'store_id'	=>	array( 'filter'=> FILTER_VALIDATE_INT, 'flags' => FILTER_REQUIRE_ARRAY ), 
                                                    'product_id'	=>	array( 'filter'=> FILTER_VALIDATE_INT, 'flags' => FILTER_REQUIRE_ARRAY ), 
                                                    'data'	=>	array( 'filter'=> FILTER_SANITIZE_STRIPPED, 'flags' => FILTER_REQUIRE_ARRAY ), 
                                                    'do'	=>	array('filter'		=>	FILTER_VALIDATE_REGEXP, 
                                                                                                'options'	=>	array('regexp' => '/^[A-Za-z ]{1,50}$/')
                                                                                                                ), 

                                                    )
										
					);


// Check if INPUT keys are ALL expected AND MADE IT THROUGH...
$_suspect = anySuspects();
if ($_suspect !== FALSE) _catch_error('Error processing white_list!' . $_suspect, __LINE__, true);




// VALIDATE AUTH TOKEN...
validate_auth_token ();

// Validate task

$_FUNCS = array (
    'void', 
    'getCatalogStats', 
    'getProductStats', 
    'getFailedLoginStats'
    
);

$_INTERVALS = array (
    'day' => Cataleya\Helper\Stats::INTERVAL_DAY, 
    'week' => Cataleya\Helper\Stats::INTERVAL_WEEK, 
    'month' => Cataleya\Helper\Stats::INTERVAL_MONTH, 
    'month-3d' => Cataleya\Helper\Stats::INTERVAL_MONTH_3D, 
    'year' => Cataleya\Helper\Stats::INTERVAL_YEAR
);


$_COLORS = array (
    '#660', '#046', '#281'
);


$_payload = array ();
$_meta = array ();



if (!in_array($_CLEAN['do'], $_FUNCS) || !is_callable($_CLEAN['do'])) _catch_error('Invalid action.', __LINE__, true);




// EXECUTE API CALL

// API FUNC CALL
$_CLEAN['do']();



// HELPER FUNCS

function digestGraphData (array $_chart_data) 
{
    $_baked = array ('data'=>array(), 'labels'=>array());
    $_key = -1;
    
    
    if (isset($_chart_data['tally_views'])):
    
    $_key++;
    $_color = pickColor ();

    $_baked['data'][$_key] = array (
        'key'   =>  $_key, 
        'color' => $_color, 
        'data' => $_chart_data['tally_views']
    );

    $_baked['labels'][$_key] = array (
        'key'   =>  $_key, 
        'label' => 'Views', 
        'color' => $_color, 
        'icon'  =>  'icon-star-4'
    );
    
    endif;
    
    
    
    
    if (isset($_chart_data['tally_wishlists'])):
        
    $_key++;
    $_color = pickColor ();
    
    $_baked['data'][$_key] = array (
        'key'   =>  $_key, 
        'color' => $_color, 
        'data' => $_chart_data['tally_wishlists']
    );

    $_baked['labels'][$_key] = array (
        'key'   =>  $_key, 
        'label' => 'Wishlists', 
        'color' => $_color, 
        'icon'  =>  'icon-heart'
    );
        
    endif;
    
    
    
    if (isset($_chart_data['tally_orders'])):
        
    $_key++;
    $_color = pickColor ();
    
    $_baked['data'][$_key] = array (
        'key'   =>  $_key, 
        'color' => $_color, 
        'data' => $_chart_data['tally_orders']
    );

    $_baked['labels'][$_key] = array (
        'key'   =>  $_key, 
        'label' => 'Orders', 
        'color' => $_color, 
        'icon'  =>  'icon-basket'
    );
    
    endif;
    
    
    
    if (isset($_chart_data['order_subtotal'])):
        
//    $_key++;
//    $_color = pickColor ();
//    
//    $_baked['data'][$_key] = array (
//        'key'   =>  $_key, 
//        'color' => $_color, 
//        'data' => $_chart_data['order_subtotal']
//    );
//
//    $_baked['labels'][$_key] = array (
//        'key'   =>  $_key, 
//        'label' => 'Subtotals', 
//        'color' => $_color, 
//        'icon'  =>  'icon-basket'
//    );
    
    endif;

    
    
    if (isset($_chart_data['tally_admin'])):
 
    $_key++;
    $_color = pickColor ();
    
    $_baked['data'][$_key] = array (
        'key'   =>  $_key, 
        'color' => $_color, 
        'data' => $_chart_data['tally_admin']
    );

    $_baked['labels'][$_key] = array (
        'key'   =>  $_key, 
        'label' => 'Admin', 
        'color' => $_color, 
        'icon'  =>  'icon-user-4'
    );
    
    endif;

    
    
    if (isset($_chart_data['tally_customers'])):
        
    $_key++;
    $_color = pickColor ();
    
    $_baked['data'][$_key] = array (
        'key'   =>  $_key, 
        'color' => $_color, 
        'data' => $_chart_data['tally_customers']
    );

    $_baked['labels'][$_key] = array (
        'key'   =>  $_key, 
        'label' => 'Customer', 
        'color' => $_color, 
        'icon'  =>  'icon-user-4'
    );
    
    endif;

    
    return $_baked;
}





function digestParams () 
{
    global $_CLEAN, $_INTERVALS;
    $_data = $_CLEAN['data'];
    
    if (!isset($_data['offset'], $_data['interval']) || !isset($_INTERVALS[$_data['interval']]) || !is_numeric($_data['offset'])) _catch_error('Bad params!', __LINE__, true);
    
    $_data['offset'] = (int)$_data['offset'];
    $_data['interval'] = $_INTERVALS[$_data['interval']];
    $_data['from_now'] = (isset($_data['from_now']) && filter_var($_data['from_now'], FILTER_VALIDATE_BOOLEAN, FILTER_NULL_ON_FAILURE) === TRUE) ? TRUE : FALSE;

    return $_data;
}



function pickColor () 
{
    global $_COLORS;
    static $_index = 0;
    
    $_color = $_COLORS[$_index];
    
    $_index = ($_index < (count($_COLORS)-1)) ? $_index+1 : 0;
    return $_color;
}




/*
 * 
 * 
 * CALLBACKS
 * 
 */


function void () {
    
}






/*
 * 
 * [ getCatalogStats ]
 * ______________________________________________________
 * 
 * @param: int $_POST['store_id']
 * @param: int $_POST['data']['offset']
 * @param: string $_POST['data']['interval']
 * @param: bool $_POST['data']['from_now']
 * 
 */


function getCatalogStats () {
    
    global $_CLEAN, $_payload, $_meta;
    $_data = digestParams();
    
    
    $_Store = (empty($_CLEAN['store_id'])) ? NULL : Cataleya\Store::load((int)$_CLEAN['store_id'][0]);
    
    $_payload = digestGraphData(Cataleya\Helper\Stats::getCatalogStats($_Store, NULL, $_data['offset'], $_data['interval'], $_data['from_now']));
    $_meta = array (
       'title'  => (empty($_Store) ? 'All Stores' : $_Store->getDescription()->getTitle('EN')) 
    );
    

};






/*
 * 
 * [ getProductStats ]
 * ______________________________________________________
 * 
 * @param: int $_POST['store_id']
 * @param: int $_POST['product_id']
 * @param: int $_POST['data']['offset']
 * @param: string $_POST['data']['interval']
 * @param: bool $_POST['data']['from_now']
 * 
 */

function getProductStats () {
    
    global $_CLEAN, $_payload, $_meta;
    $_data = digestParams();
    
    
    $_Store = (empty($_CLEAN['store_id'])) ? NULL : Cataleya\Store::load((int)$_CLEAN['store_id'][0]);
    $_Product = (empty($_CLEAN['product_id'])) ? NULL : Cataleya\Catalog\Product::load((int)$_CLEAN['product_id'][0]);
    
    $_payload = digestGraphData(Cataleya\Helper\Stats::getCatalogStats($_Store, $_Product, $_data['offset'], $_data['interval'], $_data['from_now']));
    
    $_title = empty($_Product) ? 'All Products' : $_Product->getDescription()->getTitle('EN');
    $_title .= ' (' . (empty($_Store) ? 'All Stores' : $_Store->getDescription()->getTitle('EN')) . ')';
    
    $_meta = array (
       'title'  => $_title 
    );
    
};






/*
 * 
 * [ getFailedLogins ]
 * ______________________________________________________
 * 
 * @param: int $_POST['data']['offset']
 * @param: string $_POST['data']['interval']
 * @param: bool $_POST['data']['from_now']
 * 
 */


function getFailedLoginStats () {
    
    global $_payload, $_meta;
    $_data = digestParams();
    
    $_payload = digestGraphData(Cataleya\Helper\Stats::getFailedLoginStats($_data['offset'], $_data['interval'], $_data['from_now']));
    $_meta = array (
       'title'  => 'Failed Logins' 
    );
    

};





// Remove token...
if (isset($_SESSION[SESSION_PREFIX.'CONFIRM_TOKEN'])) unset ($_SESSION[SESSION_PREFIX.'CONFIRM_TOKEN']);

$_meta['labels'] = $_payload['labels'];
    
// OUTPUT...
 $json_reply = array (
                 "status" => 'ok', 
                 "message" => 'Graph data.', 
                 "payload"  =>  $_payload['data'], 
                 "meta" =>  $_meta
                 );


 echo (json_encode($json_reply));
 exit();  

?>