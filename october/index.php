<?php


define ('OUTPUT', 'HTML'); // Whether to output as JSON or HTML (especially errors)...


// LOAD APPLICATION TOP...
require_once ('app_top.php');


/*
$_apps = \Cataleya\Plugins\Package::getAppList();

foreach ($_apps as $_a) {
$_c = null;
$_c = \Cataleya\Plugins\Config::load($_a);
if (empty($_c)) {
$_app_con = \Cataleya\Plugins\Package::getAppController($_a);

            \Cataleya\Plugins\Package::rememberApp($_a);
            call_user_func([ $_app_con, 'install' ]);
}

}
*/


// Load base currency
$base_currency = Cataleya\Locale\Currency::loadBaseCurrency();



?>


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<?php
include "includes/meta.php";

?>

<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Shop Admin</title>


<!-- JS FILES -->
<?php
require_once(INC_PATH.'js_libs.php');
?>
<script type="text/javascript" src="ui/jscript/admin.landing.js"></script>


<!-- STYLE SHEETS -->
<?php
require_once(INC_PATH.'css_libs.php');
?>

<link href="ui/css/tiles.css" rel="stylesheet" type="text/css" />
<link href="ui/css/bootmetro-tiles.css" rel="stylesheet" type="text/css" />


<style type="text/css" media="screen">
#home_icon {
	position:absolute;
	top:110px;
	left:10px;
	
	background-image:url(ui/images/icons/home_20x20.png);
	
}


body {
	background-color:#000;
	background-image: url(./ui/images/wallpaper/city-view-hd-wallpapers-11750.jpeg);
	font-family: 'CenturyGothicRegular', verdana, arial; 
	
}


.header-wrapper {
    background: none;
    overflow: visible;
}

.header-wrapper > .header {
    border: none;
}

#page-title {
    color: #aac;
}

#main-nav-button {
    color: #aac;
}

#command-center-wrapper {
        
	width:100%;
	height:100%;
        
        /*
	width:890px;
	height:480px;
        */
        
}





.glass-pane {
    position:absolute;
    top: 0px;
    
    background-image: url(../images/transparent_pixel.gif);

    margin: 0px 0px;
    
    z-index: 410;
}



.glass-pane > div.inner-wrap {
	display:block;
	
	position:relative;
	left: 0px;
	
	width:960px;
	height:550px;
        
	margin:5px auto;
        margin-top: 115px;
        
        
	background:rgba(0, 0, 0, .8);
        background: -webkit-linear-gradient(top, rgba(0, 0, 0, .7), rgba(0, 0, 0, .8));
        background: -moz-linear-gradient(top, rgba(0, 0, 0, .7), rgba(0, 0, 0, .8));
        background: -o-linear-gradient(top, rgba(0, 0, 0, .7), rgba(0, 0, 0, .8));
        background: linear-gradient(top, rgba(0, 0, 0, .7), rgba(0, 0, 0, .8));
        
	border:solid 2px rgba(50, 50, 50, 1);
	
	border-radius:20px;	
	-moz-border-radius:20px;	
	-webkit-border-radius:20px;
	
	box-shadow: 2px 2px 3px rgba(0,0,0,.7);

        /*
	border:solid 5px rgba(50, 50, 50, .7);
	
        background-image: url(../images/black_denim.png);
        */
        
	border-radius:5px;	
	-moz-border-radius:5px;	
	-webkit-border-radius:5px;
	
	box-shadow: 1px 1px 2px rgba(0,0,0,.6);
}




.glass-pane > div.clear-button {
    display: block;
    position: absolute;
    top: 0px;
    left: 0px;
    
    width: 100%;
    height: 100%;
    background: rgba(0, 0, 0, 0);
    background: transparent;
    
    z-index: 1;
}


.glass-pane > div.inner-wrap > a.cancel-button > .icon-close {
    position: absolute;
    top: 10px;
    right: 10px;
    
    height: 15px;
    width: 150px;
    
    font-size: 75%;
    font-weight: bold;
    line-height: 8px;
    text-align: right;
    color: #770;
    
    display: block;
    z-index: 405;
}

.glass-pane > div.inner-wrap > a.cancel-button > .icon-close:before {
    position: relative;
    float: right;
    margin-left: 5px;
    
    font-size: 150%;
    color: #444;
}

.glass-pane > div.inner-wrap > a.cancel-button:hover > .icon-close:before {
    color: #777;
}
















.frames {
	position:relative;
	width:650px;
	height:480px;
	
        /*
	position:absolute;
	top:140px;
	left:50px;
	border-bottom: dotted 1px #0ac;
	*/
        
        float: right;
        
        border: none;
	margin:15px 35px 15px auto;
	
	padding-top:15px!important;
        overflow: hidden;
        z-index: 9;

}


.frame-strip {
	width:1700px;
	height:480px;
	
	position:absolute;
	top: 0px;
	left: 0px;
/*	
	border-left: dotted 1px #0ac;
	*/
	padding:0px!important;
        overflow: hidden;

        -webkit-transition: all 0.5s ease-in-out;
        -moz-transition: all 0.5s ease-in-out;
        -o-transition: all 0.5s ease-in-out;
        -ms-transition: all 0.5s ease-in-out;
        transition: all 0.5s ease-in-out;
    
}


.glass-panel {
	width:240px;
	height:550px;
	
	position:absolute;
	top: 0px;
	left: 0px;
	
	border-right: solid 5px  rgba(0, 0, 0, .4);
	
	padding-top:0px;
        background: rgba(0, 0, 0, .4);
        z-index: 10;
        
        
	border-radius:5px 0px 0px 5px;	
	-moz-border-radius:5px 0px 0px 5px;	
	-webkit-border-radius:5px 0px 0px 5px;
        
}


.frame {
    position: relative;
    display: block;
    
    
    width:650px;
    height:480px;

    padding: 5px 0px 0px 0px!important;
    
}





</style>


</head>

<body>
    
<!-- BEGIN: META -->
<input type="hidden" id="meta_token" value="<?=AUTH_TOKEN?>"  />
<input type="hidden" id="meta_admin_root" value="<?=BASE_URL?>"  />
<!-- END: META -->


<div class="container">
<?php include_once INC_PATH.'photo-upload-forms.php'; ?>

        
<div class="header-wrapper absolute">
    <div class="header">

    <a  href="javascript:void(0);" onclick="HomeNav.back();">
    <div id="page-title"><span class="title-text">Welcome</span></div>
    </a>

    </div>
    
    
<?php
        include_once INC_PATH.'main.nav.php';
?>
    
    
<?php
        include_once INC_PATH.'notifications-bar.php';
?>
    
</div>



<div class="content">
    
    <div id="command-center-wrapper" class="glass-pane">
        <div class="inner-wrap">
            

            <div class="glass-panel">&nbsp;</div>
    

            <!-- BEGIN FRAMES -->

            <div id="command-center-slider-wrap" class="frames" data-title-selector="#page-title .title-text">
            <div class="frame-strip strip">




            <div id="home-frame" class="metro frame" data-title="Welcome" data-title="Welcome">



                <?php 

                if (IS_SUPER_USER || $adminUser->getRole()->hasPrivilege('VIEW_CATALOG')): 
                    $_alert_stock_low =  Cataleya\Admin\Notification::load('stock.low')->getAlert($adminUser);
                    $_alert_stock_out =  Cataleya\Admin\Notification::load('stock.out')->getAlert($adminUser);
                ?> 
                <a class="tile app  <?= ((!$_alert_stock_low->isRead() && $_alert_stock_low->getCount() > 0) || (!$_alert_stock_out->isRead() && $_alert_stock_out->getCount() > 0)) ? 'new-alert' : '' ?>" href="<?=BASE_URL.'landing.catalog.php'?>">
                    <div class="image-wrapper">
                        <span class="icon-tag tile-icon-large">&nbsp;</span>
                    </div>
                    <div class="app-label">Catalog</div>
                    <div class="app-count"><?= Cataleya\System::load()->getProductCount() ?></div>
                </a>
                <?php endif ?>


                <?php 

                if (IS_SUPER_USER || $adminUser->getRole()->hasPrivilege('VIEW_ORDERS')): 
                    $_Stores = Cataleya\Stores::load();
                    $_any_new_orders = FALSE;

                    foreach ($_Stores as $_Store)
                    {
                        $_NewOrderAlert = $_Store->getNewOrderNotification()->getAlert($adminUser);
                        if (!$_NewOrderAlert->isRead() && $_NewOrderAlert->getCount() > 0) $_any_new_orders = TRUE;
                    }

                ?> 
                <a class="tile app <?= ($_any_new_orders) ? 'new-alert active-state' : '' ?>" href="<?=BASE_URL.'landing.orders.php?show=pending'?>">
                    <div class="image-wrapper">
                        <span class="icon-cart tile-icon-large">&nbsp;</span>
                    </div>
                    <div class="app-label">Orders</div>
                    <div class="app-count"><?= Cataleya\Store\Orders::load(NULL, NULL, array('status'=>'pending'), 50000)->getPopulation() ?></div>
                </a> 
                <?php endif ?>


                <?php 

                if (true || IS_SUPER_USER || $adminUser->getRole()->hasPrivilege('VIEW_CUSTOMER_PROFILES')): 
                    $_alert_new_customer =  Cataleya\Admin\Notification::load('customer.new')->getAlert($adminUser);

                ?> 
                <a class="tile app <?= (!$_alert_new_customer->isRead() && $_alert_new_customer->getCount() > 0) ? 'new-alert active-state' : '' ?>" href="<?=BASE_URL.'landing.customers.php'?>">
                    <div class="image-wrapper">
                        <span class="icon-users tile-icon-large">&nbsp;</span>
                    </div>
                    <div class="app-label">Customers</div>
                    <div class="app-count"><?= Cataleya\Customers::load()->getPopulation() ?></div>
                </a>
                <?php endif ?>


                <?php 

                if (IS_SUPER_USER || $adminUser->getRole()->hasPrivilege('VIEW_ADMIN_PROFILES')): 
                    $_alert_new_admin =  Cataleya\Admin\Notification::load('admin.new')->getAlert($adminUser);
                    $_alert_admin_deleted =  Cataleya\Admin\Notification::load('admin.deleted')->getAlert($adminUser);

                ?> 
                <a class="tile app <?= ((!$_alert_new_admin->isRead() && $_alert_new_admin->getCount() > 0) || (!$_alert_admin_deleted->isRead() && $_alert_admin_deleted->getCount() > 0)) ? 'new-alert active-state' : '' ?>" href="<?=BASE_URL.'profiles.admin.php'?>">
                    <div class="image-wrapper">
                        <span class="icon-user-3 tile-icon-large">&nbsp;</span>
                    </div>
                    <div class="app-label">Employees</div>
                    <div class="app-count"><?= Cataleya\Admin\Users::load()->getPopulation() ?></div>
                </a>
                <?php endif ?>




                <a class="tile app" href="javascript:void(0);" onclick="MyAccount.editProfile();">
                    <div class="image-wrapper">
                        <span class="icon-key tile-icon-medium">&nbsp;</span>
                    </div>
                    <div class="app-label">My Account</div>
                    <div class="app-count">&nbsp;</div>
                </a>



                <a class="tile app" href="<?=BASE_URL.'landing.stores.php'?>">
                    <div class="image-wrapper">
                        <span class="icon-location tile-icon-medium">&nbsp;</span>
                    </div>
                    <div class="app-label">Stores</div>
                    <div class="app-count"><?= Cataleya\Stores::load()->getPopulation() ?></div>
                </a>


<!--
                <a class="tile app" href="<?=BASE_URL.'landing.customers.php'?>">
                    <div class="image-wrapper">
                        <span class="icon-pinterest tile-icon-medium">&nbsp;</span>
                    </div>
                    <div class="app-label">Social</div>
                    <div class="app-count">&nbsp;</div>
                </a>

-->

                <?php if (IS_SUPER_USER || $adminUser->getRole()->hasPrivilege('VIEW_ADVANCED_SETTINGS')): ?>   
                <a class="tile app" href="javascript:void(0);" onclick="HomeNav.gotoSlide(1);">
                    <div class="image-wrapper">
                        <span class="icon-settings tile-icon-large">&nbsp;</span>
                    </div>
                    <div class="app-label">Advanced Settings</div>
                    <div class="app-count">&nbsp;</div>
                </a>
                <?php endif ?>

            </div>





            <!-- FRAME: SETTINGS -->

            <div id="settings-frame" class="metro frame" data-title="Settings">

                <a class="tile app" href="javascript:void(0);" onclick="HomeNav.gotoSlide(0);">
                    <div class="image-wrapper">
                        <span class="icon-home-4 tile-icon-large"></span>
                    </div>
                    <div class="app-label">Home</div>
                    <div class="app-count"></div>
                </a>

                <a class="tile app" href="<?=BASE_URL.'currencies.settings.php'?>">
                    <div class="image-wrapper">
                        <span class="tile-icon-large">&#<?=$base_currency->getUtfCode()?>;</span>
                    </div>
                    <div class="app-label">Currencies</div>
                    <div class="app-count"><?=$base_currency->getCurrencyCode()?></div>
                </a>

            <?php if (IS_SUPER_USER || $adminUser->getRole()->hasPrivilege('VIEW_PAYMENT_TYPES')): ?>    
                <a class="tile app" href="<?=BASE_URL.'payment-types.settings.php'?>">
                    <div class="image-wrapper">
                        <span class="icon-calculate tile-icon-large"></span>
                    </div>
                    <div class="app-label">Payment Modules</div>
                    <div class="app-count"><?= count(\Cataleya\Plugins\Package::getAppList('payment')) ?></div>
                </a>
            <?php endif ?>


            <?php if (IS_SUPER_USER || $adminUser->getRole()->hasPrivilege('VIEW_COUNTRIES')): ?>    
                <a class="tile app" href="<?=BASE_URL.'landing.countries.php'?>">
                    <div class="image-wrapper">
                        <span class="icon-flag tile-icon-large"></span>
                    </div>
                    <div class="app-label">Countries</div>
                    <div class="app-count"><?= Cataleya\Geo\Countries::load()->getPopulation() ?></div>
                </a>
            <?php endif ?>


            <?php if (IS_SUPER_USER || $adminUser->getRole()->hasPrivilege('VIEW_TAX_CLASSES')): ?>    
                <a class="tile app" href="<?=BASE_URL.'landing.tax.php'?>">
                    <div class="image-wrapper">
                        <span class="icon-calculate tile-icon-large"></span>
                    </div>
                    <div class="app-label">Tax Rules</div>
                    <div class="app-count"><?= Cataleya\Tax\TaxRules::load()->getPopulation() ?></div>
                </a>
            <?php endif ?>


            <?php if ( IS_SUPER_USER || $adminUser->is('STORE_OWNER')  || $adminUser->getRole()->hasPrivilege('VIEW_ZONES')): ?>    
                <a class="tile app" href="<?=BASE_URL.'landing.zones.php'?>">
                    <div class="image-wrapper">
                        <span class="icon-globe tile-icon-large"></span>
                    </div>
                    <div class="app-label">Zones</div>
                    <div class="app-count"><?= Cataleya\Geo\Zones::load()->getPopulation() ?></div>
                </a>
            <?php endif ?>

            <?php if (true || IS_SUPER_USER || $adminUser->getRole()->hasPrivilege('VIEW_CARRIER_INFO')): ?>    
                <a class="tile app" href="<?=BASE_URL.'landing.carriers.php'?>">
                    <div class="image-wrapper">
                        <span class="icon-flight tile-icon-large"></span>
                    </div>
                    <div class="app-label">Shipping Carriers</div>
                    <div class="app-count"><?= Cataleya\Agent\_::load('CARRIER')->getPopulation() ?></div>
                </a>
            <?php endif ?>


            <?php if (IS_SUPER_USER || $adminUser->is('STORE_OWNER') || $adminUser->getRole()->hasPrivilege('VIEW_NOTIFICATIONS')): ?>    
                <a class="tile app" href="<?=BASE_URL.'notifications.settings.php'?>">
                    <div class="image-wrapper">
                        <span class="icon-flag-4 tile-icon-large"></span>
                    </div>
                    <div class="app-label">Notifications</div>
                    <div class="app-count"></div>
                </a>
            <?php endif ?>


            <?php if (IS_SUPER_USER): ?>  

                <a class="tile app" href="<?=BASE_URL.'configurations.settings.php'?>">
                    <div class="image-wrapper">
                        <span class="icon-wrench tile-icon-large"></span>
                    </div>
                    <div class="app-label">Configurations</div>
                    <div class="app-count"></div>
                </a>

                <a class="tile app" href="<?=BASE_URL.'permissions.admin.php'?>">
                    <div class="image-wrapper">
                        <span class="icon-locked tile-icon-medium"></span>
                    </div>
                    <div class="app-label">Permissions</div>
                    <div class="app-count">&nbsp;</div>
                </a>


                <?php
                if (IS_SUPER_USER || $adminUser->getRole()->hasPrivilege('VIEW_ADMIN_PROFILES')): 
                    $_alert_new_admin =  Cataleya\Admin\Notification::load('admin.new')->getAlert($adminUser);
                    $_alert_admin_deleted =  Cataleya\Admin\Notification::load('admin.deleted')->getAlert($adminUser);

                ?> 
                <a class="tile app <?= ((!$_alert_new_admin->isRead() && $_alert_new_admin->getCount() > 0) || (!$_alert_admin_deleted->isRead() && $_alert_admin_deleted->getCount() > 0)) ? 'new-alert' : '' ?>" href="<?=BASE_URL.'profiles.admin.php'?>">
                    <div class="image-wrapper">
                        <span class="icon-users tile-icon-medium">&nbsp;</span>
                    </div>
                    <div class="app-label">Admin Accounts</div>
                    <div class="app-count"><?= Cataleya\Admin\Users::load()->getPopulation() ?></div>
                </a>
                <?php endif ?>




                <a class="tile app" href="<?=BASE_URL.'app-preferences.settings.php'?>">
                    <div class="image-wrapper">
                        <span class="icon-settings tile-icon-large"></span>
                    </div>
                    <div class="app-label">App Preferences</div>
                    <div class="app-count"></div>
                </a>

            <?php endif ?>

            </div>




            </div>    
            <!-- END FRAME STRIP -->


            </div>
            <!-- END FRAMES -->



        </div>
    </div>

</div>
</div>








</body>
</html>
