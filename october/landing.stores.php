<?php




define ('OUTPUT', 'HTML'); // Whether to output as JSON or HTML (especially errors)...


// LOAD APPLICATION TOP...
require_once ('app_top.php');



/*
 * 
 * CHECK USER PRIVILEGES
 * 
 */


if ((!isset($_admin_role) || !$_admin_role->hasPrivilege('VIEW_STORES')) && !IS_SUPER_USER) forbidden();


define('READONLY', (!$_admin_role->hasPrivilege('EDIT_STORES') && !IS_SUPER_USER) ? FALSE : TRUE);


                
                



// SANITIZE GET DATA...
$_CLEAN = filter_input_array(INPUT_GET, 
								array(	
										 // Page number...
										'pg'	=>	array('filter'		=>	FILTER_VALIDATE_INT, 
																'options'	=>	array('min_range' => 1, 'max_range' => 1000)
																), 
                                                                                // Page size...
										'pgsize'	=>	array('filter'		=>	FILTER_VALIDATE_INT, 
																'options'	=>	array('min_range' => 1, 'max_range' => 1000)
																),                                                                     
										
										 // order 						
										'o'	=>	array('filter'		=>	FILTER_VALIDATE_INT, 
																'options'	=>	array('min_range' => 0, 'max_range' => 100)
																), 
                                                                                // order by 						
										'ob'	=>	array('filter'		=>	FILTER_VALIDATE_INT, 
																'options'	=>	array('min_range' => 0, 'max_range' => 100)
																),    
                                                                                // index 						
										'i'	=>	array('filter'		=>	FILTER_VALIDATE_REGEXP, 
														'options'	=>	array('regexp' => '/^[A-Za-z]{1}$/')
																), 

										)
								);


// Deafault page size...
define ('DEFAULT_PAGE_SIZE', 20);


// Check if INPUT keys are ALL expected AND MADE IT THROUGH...

$_CLEAN['pg'] = empty($_CLEAN['pg']) ? 1 : (int)$_CLEAN['pg'];
$_CLEAN['i'] = empty($_CLEAN['i']) ? 'all' : $_CLEAN['i'];
$_CLEAN['pgsize'] = empty($_CLEAN['pgsize']) ? DEFAULT_PAGE_SIZE : (int)$_CLEAN['pgsize'];
$_CLEAN['o'] = empty($_CLEAN['o']) ? Cataleya\Geo\Countries::ORDER_ASC : $_CLEAN['o'];
$_CLEAN['ob'] = empty($_CLEAN['ob']) ? Cataleya\Geo\Countries::ORDER_BY_NAME : $_CLEAN['ob'];

define ('NO_OF_PAGE_INDEX_ANCHORS', 3);

define('ORDER_BY', $_CLEAN['ob']);
define('ORDER', $_CLEAN['o']);
define('INDEX', $_CLEAN['i']);
define('PAGE_SIZE', $_CLEAN['pgsize']);



$_Stores = Cataleya\Stores::load(array('order_by'=>ORDER_BY, 'order'=>ORDER, 'index'=>INDEX), PAGE_SIZE, $_CLEAN['pg']);


define ('NEW_TOKEN', (md5(uniqid(rand(), TRUE))) . '-' . time()); 

// Required to create new store (to prevent creating one by accident)
$_SESSION[SESSION_PREFIX.'NEW_TOKEN'] = NEW_TOKEN;



?>




<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<?php
include "includes/meta.php";

?>

<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Admin | Stores (<?= Cataleya\Helper::countInEnglish($_Stores->getPopulation(), 'stores', 'stores') ?>)</title>


<!-- JS FILES -->
<?php
require_once(INC_PATH.'js_libs.php');
?>
<script type="text/javascript" src="ui/jscript/stores.js"></script>

<!-- STYLE SHEETS -->
<?php
require_once(INC_PATH.'css_libs.php');
?>

<link href="ui/css/indexed-paginated-list.css" rel="stylesheet" type="text/css" />
<link href="ui/css/bootmetro-tiles.css" rel="stylesheet" type="text/css" />


<style type="text/css" media="screen">
    
.content {
    height:<?= (ceil(((PAGE_SIZE > $_Stores->getPopulation()) ? $_Stores->getPopulation() : PAGE_SIZE)/3)*350)+520 ?>px;
	
}




#list-table {
    top:300px;
}




#tiles-wrapper {
	
	padding-top:15px!important;

}


    .mediumListIconTextItem {
        background-color:#111; 
    }

   .mediumListIconTextItem img.mediumListIconTextItem-Image {
      border: dotted 1px #444;
      
      border-radius:7px;	
      -moz-border-radius:7px;	
      -webkit-border-radius:7px;   
   }

</style>


</head>

<body>
    
<!-- BEGIN: META -->
<input type="hidden" id="meta_token" value="<?=AUTH_TOKEN?>"  />
<input type="hidden" id="meta_admin_root" value="<?=BASE_URL?>"  />
<!-- END: META -->



<div class="container">
<?php include_once INC_PATH.'photo-upload-forms.php'; ?>
    
<div class="header-wrapper absolute">
    <div class="header">

    <div id="page-title">Stores</div><?php include_once INC_PATH.'page.nav.php'; ?>   

    </div>
    
    
<?php
        include_once INC_PATH.'main.nav.php';
?>
    
</div>



<div class="content">

<div id="search-tool-wrapper">
		
<?php
define('QUICK_SEARCH_URL', BASE_URL.'store_profile_search.ajax.php');
include INC_PATH.'quick-search-widget.php';
?>




</div>




<!-- INDEX -->

<ul id="list-index">
<a href="landing.stores.php?pg=0&ob=<?=$_CLEAN['ob']?>&o=<?=$_CLEAN['o']?>&i=all" <?=(($_CLEAN['i']=='all') ? 'class="button-1-active"' : '')?>><li>all</li></a>

<?php
$alpha = array('A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z');

for ($i = 0; $i < (count($alpha)); $i++) {
	$a = strtolower($alpha[$i]);
	?>
<a href="landing.stores.php?pg=0&ob=<?=$_CLEAN['ob']?>&o=<?=$_CLEAN['o']?>&i=<?=$a?>" <?=(($_CLEAN['i']==$a) ? 'class="button-1-active"' : '')?>><li><?=$alpha[$i]?></li></a>

<?php
}

?>



</ul>




<div id="hor-line-1">&nbsp;</div>


<div id="page-options-panel">
    

            <div id="page-description-panel">
                To edit a store front...
            </div>
</div>


    

<ul id="list-table">
    
<?php if ($_Stores->getPopulation() > (int)$_CLEAN['pgsize']): ?>
<li class="list-table-header">

        <div class="list-pagination">
        <?php if ($_Stores->getPopulation() > 0) { ?>
            Page <?=$_Stores->getPageNumber()?> of <?=$_Stores->getLastPageNum()?>
        <?php } else { ?>
        
        No admin profiles with name starting with '<?=strtoupper($_CLEAN['i'])?>'.
        
        <?php } ?>
        
        </div>

        <ul class="list-page-nav">
            <li><a <?php if (($_Stores->getPageNumber() < $_Stores->getLastPageNum())) { ?>href="classes.tax.php?pg=<?=$_Stores->getNextPageNum()?>&o=<?=$_CLEAN['o']?>&ob=<?=$_CLEAN['ob']?>&i=<?=$_CLEAN['i']?>"  class="button-1"<?php } else { ?>NOHREF class="button-1-no-href"<?php } ?>>next</a></li>
        

        <li><a <?php if ($_Stores->getPageNumber() > 1) { ?>href="classes.tax.php?pg=<?=$_Stores->getPrevPageNum()?>&o=<?=$_CLEAN['o']?>&ob=<?=$_CLEAN['ob']?>&i=<?=$_CLEAN['i']?>"  class="button-1"<?php } else { ?>NOHREF class="button-1-no-href"<?php } ?>>previous</a></li>
        
        </ul>

</li>
<?php endif; ?>

<li>
        <div id="tiles-wrapper" class="listview-container grid-layout">

        <?php

        foreach ($_Stores as $_Store)
        {

         $store_description = $_Store->getDescription();
         //$display_picture = $_Store->getDisplayPicture();

        ?>

             <a class="tile mediumListIconTextItem" href="<?=BASE_URL.'store_edit.stores.php?d=edit&s='.$_Store->getID()?>" >
                 <img src="<?=$_Store->getDisplayPicture()->getHref(Cataleya\Asset\Image::LARGE)?>" class="mediumListIconTextItem-Image" alt="60x60" style="width: 60px; height: 60px; background-color: transparent;">
                <div class="mediumListIconTextItem-Detail">
                   <h4><?=$store_description->getTitle('EN')?></h4>
                   <h6><?=$store_description->getText('EN')?></h6>
                </div>
            </a>   

        <?php

        }


        ?>


            <?php if (IS_SUPER_USER || $adminUser->getRole()->hasPrivilege('CREATE_STORES')): ?>
            <!-- NEW STORE BUTTON -->
            <a class="tile mediumListIconTextItem button-4" href="<?=BASE_URL.'store_edit.stores.php?d=new&t='.NEW_TOKEN?>">
                    <span class="icon-new tile-icon-large" ></span>
                    <div class="mediumListIconTextItem-Detail">
                        <br />
                        <h4>New Store</h4>
                     </div>
            </a>
            <?php endif ?>
            
            <br style="clear: both" />
        </div>


</li>

</ul>


<div id="footer">
&nbsp;
</div>



</div>
</div>


</body>
</html>
