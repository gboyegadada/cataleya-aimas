<?php


 
/**
 * inoculate against hack attempts which waste CPU cycles
 */
$contaminated = (isset($_FILES['GLOBALS']) || isset($_REQUEST['GLOBALS'])) ? true : false;
$paramsToAvoid = array('GLOBALS', '_COOKIE', '_ENV', '_FILES', '_GET', '_POST', '_REQUEST', '_SERVER', '_SESSION', 'HTTP_COOKIE_VARS', 'HTTP_ENV_VARS', 'HTTP_GET_VARS', 'HTTP_POST_VARS', 'HTTP_POST_FILES', 'HTTP_RAW_POST_DATA', 'HTTP_SERVER_VARS', 'HTTP_SESSION_VARS');
$paramsToAvoid[] = 'autoLoadConfig';
$paramsToAvoid[] = 'mosConfig_absolute_path';
$paramsToAvoid[] = 'hash';
$paramsToAvoid[] = 'main';
foreach($paramsToAvoid as $key) {
  if (isset($_GET[$key]) || isset($_POST[$key]) || isset($_COOKIE[$key])) {
    $contaminated = true;
    break;
  }
}

$paramsToCheck = array('pg', 'f', 'p', 't', 'd', 'o', 'c', 'ref', 'auth_token', 'a', 'id', 'pg', 'i');


if (!$contaminated) {
  foreach($paramsToCheck as $key) {
    if (isset($_GET[$key]) && !is_array($_GET[$key])) {
      if (substr($_GET[$key], 0, 4) == 'http' || strstr($_GET[$key], '//')) {
        $contaminated = true;
        break;
      }
      if (isset($_GET[$key]) && strlen($_GET[$key]) > 43) {
        $contaminated = true;
        break;
      }
    }
  }
}
unset($paramsToCheck, $paramsToAvoid, $key);
if ($contaminated)
{
  header('HTTP/1.1 406 Not Acceptable');
  exit(0);
}
unset($contaminated);
/* *** END OF INNOCULATION *** */







/////////////////////////////////////////////////////////////////
//// ADMIN PAGE TOP 




define ('IS_ADMIN_FLAG', TRUE);

////////// CONFIG //////////
require_once ('includes/admin.core.config.php');	


// GENERAL FUNCTIONS...
require_once ('includes/functions/general_funcs.admin.php');


// Check _SERVER['HTTP_HOST']
if (strtolower($_SERVER['HTTP_HOST']) !== DOMAIN && strtolower($_SERVER['HTTP_HOST']) !== "www.".DOMAIN) 
{
  header('HTTP/1.0 400 Bad Request');
  exit(0);
}

else if (strtolower($_SERVER['HTTP_HOST']) === "www.".DOMAIN) 
{
    header('Location: ' . DOMAIN_FULL . $_SERVER['REQUEST_URI']);
    exit();
}








// Check for direct calls to AJAX scripts...
if (defined('OUTPUT') && (OUTPUT == 'JSON' || OUTPUT == 'AJAX_JSON' || OUTPUT == 'AJAX_HTML')) {
	if(isset($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
		// This an ajax request. Do nothing.
	 } else {
		$_to = 'http://' . DOMAIN;
		header('Location: ' . $_to);
		exit();
	 }
}

 

// Redirect to https if  SSL is required...
if ( defined('SSL_REQUIRED') && SSL_REQUIRED === TRUE && !is_ssl() ) {
	if ( 0 === strpos($_SERVER['REQUEST_URI'], 'http') ) {
		redirect(preg_replace('|^http://|', 'https://', $_SERVER['REQUEST_URI']));
		exit();
	} else {
		redirect('https://' . DOMAIN . $_SERVER['REQUEST_URI']);
		exit();
	}
}




// FETCH ERROR FUNCTIONS...
require_once (INC_PATH.'functions/error_funcs.php');


// AUTH FUNCTIONS...
require_once (INC_PATH.'functions/auth_funcs.admin.php');


// HEADERS
header("Cache-Control: no-cache, must-revalidate");
header("Expires: Mon, 26 Jul 1997 05:00:00 GMT");
header("X-Frame-Options: DENY"); // 'same-origin': allow being framed from urls of the same origin || 'deny': block all
header("X-XSS-Protection: 1; mode=block");
header("X-Content-Security-Policy: " . ((defined('SSL_REQUIRED') && SSL_REQUIRED) ? "allow https://*:443" : "allow 'self'") . "; report-uri " . DOMAIN . "/csp-report; connect-src " . DOMAIN . "; img-src " . DOMAIN . "; style-src " . DOMAIN . "; object-src " . DOMAIN . "; script-src " . DOMAIN . "; xhr-src " . DOMAIN . "; options inline-script setTimeout setInterval;");
if (SSL_REQUIRED) header ("Strict-Transport-Security: max-age=8640000; includeSubDomains");



if (OUTPUT == 'JSON') {
	
	header('Content-type: application/x-json');
	
} else if (OUTPUT == 'TEXT') {
    	header('Content-Type: text/plain; charset="UTF-8"');
        
} else if (OUTPUT == 'JSCRIPT') {
    	header('Content-Type: application/javascript');
}




// INIT SESSION
session_name(SESSION_NAME);
Cataleya\Session::init_session_cookie(); // (array('path'=>'/'.ADMIN_ROOT_URI));
session_start();



// INIT REQUEST_TOKEN ARRAY
if (!isset($_SESSION[SESSION_PREFIX.'REQUEST_TOKENS'])) $_SESSION[SESSION_PREFIX.'REQUEST_TOKENS'] = array ();

// CHECK IF THIS REQUEST HASN'T ALREADY BEEN PROCESSED
// Only do checks when this is an AJAX request.
if(isset($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
    
    checkRequestToken();
    checkFingerPrint();

}

$_debug = '';


// FETCH USER TRENDS INCLUDE (Checks if user is connecting from an unusual location)
require_once (INC_PATH.'user_trends.php');



// Preserve referrer during logins...
if ($_SERVER['SCRIPT_NAME'] != '/' . ADMIN_ROOT_URI . 'login.php' && preg_match('/(login|logout|index)\.php/', $_SERVER['SCRIPT_NAME']) === 0 && OUTPUT == 'HTML')
{
	$_SESSION[SESSION_PREFIX.'REFERRER'] = $_SERVER['SCRIPT_NAME'];
	if ($_SERVER['QUERY_STRING'] != '') $_SESSION[SESSION_PREFIX.'REFERRER'] .=  '?' . $_SERVER['QUERY_STRING'];
}

// Check if there is a login session in progress...
if (!isset($_SESSION[SESSION_PREFIX.'ADMIN_ID'], $_SESSION[SESSION_PREFIX.'LOGGED_IN']) && $_SERVER['SCRIPT_NAME'] == '/' . ADMIN_ROOT_URI . 'login.php') 
{
	
 // Do nothing...
	//die ('doing nothing');
	
	
} 

        
        
else {

	 
		// Else do normal auth...
		if (isset($_SESSION[SESSION_PREFIX.'ADMIN_ID'], $_SESSION[SESSION_PREFIX.'LOGGED_IN']))
		{
                        
			$adminUser = Cataleya\Admin\User::authenticate($_SESSION[SESSION_PREFIX.'ADMIN_ID'], $_SESSION[SESSION_PREFIX.'LOGGED_IN']);
			
			if ($adminUser !== NULL) 
			{
				define('IS_LOGGED_IN', TRUE);
				$_SESSION[SESSION_PREFIX.'LOGGED_IN'] = $adminUser->getLoginToken();
				
			} else {
				define('IS_LOGGED_IN', FALSE);
                $_debug .= ' - failed auth - ';
			}
			
		} else {
			
			define('IS_LOGGED_IN', FALSE);
			
		}
		
		
		
		
		
		
    if (IS_LOGGED_IN)
    {
       // REdirect to home page if a logged on user tries to logon again...
       if ($_SERVER['SCRIPT_NAME'] == '/' . ADMIN_ROOT_URI . 'login.php')
        {
           
            redirect(BASE_URL);
            exit();
        }				
        
        define('AUTH_TOKEN', $_SESSION[SESSION_PREFIX.'AUTH_TOKEN']);

    } 
    
    // Redirect to login page if user is not already on the way there...
    else if ($_SERVER['SCRIPT_NAME'] != '/' . ADMIN_ROOT_URI . 'login.php')
		{
					
				

				
				$old_session = $_SESSION;
				
				session_destroy();
				
				
				// RE-INIT SESSION
				session_name(SESSION_NAME);
				session_start();
				
				// if (isset($old_session[SESSION_PREFIX.'LOGIN_TOKEN'])) unset($old_session[SESSION_PREFIX.'LOGIN_TOKEN']);
                                
                // Preserve some possibly useful stuff...
				if (isset($old_session[SESSION_PREFIX.'REFERRER'])) $_SESSION[SESSION_PREFIX.'REFERRER'] = $old_session[SESSION_PREFIX.'REFERRER'];
				if (isset($old_session[SESSION_PREFIX.'REQUEST_TOKENS'])) $_SESSION[SESSION_PREFIX.'REQUEST_TOKENS'] = $old_session[SESSION_PREFIX.'REQUEST_TOKENS'];
					
				
				bombSession ($_debug);
				
					
			
		} 
                
                // Check for old sessions...and delete
                else if (isset($_SESSION[SESSION_PREFIX.'ADMIN_ID']) || isset($_SESSION[SESSION_PREFIX.'LOGGED_IN']))
                {
			
                $old_session = $_SESSION;
				session_destroy();
				
				
				// RE-INIT SESSION
				session_name(SESSION_NAME);
				session_start();
				
				
                // Preserve some possibly useful stuff...
				if (isset($old_session[SESSION_PREFIX.'REFERRER'])) $_SESSION[SESSION_PREFIX.'REFERRER'] = $old_session[SESSION_PREFIX.'REFERRER'];
				if (isset($old_session[SESSION_PREFIX.'REQUEST_TOKENS'])) $_SESSION[SESSION_PREFIX.'REQUEST_TOKENS'] = $old_session[SESSION_PREFIX.'REQUEST_TOKENS'];
					
				


		}
		
		


}




/*
 * 
 * SET CONSTANT TO INDICATE IF USER  IS AN ADMINISTRATOR (I.E. HAS SUPER_USER PRIVILEGE
 * 
 */

if (isset($adminUser)) 
{
    $_admin_role = $adminUser->getRole();
    define('IS_SUPER_USER', ($_admin_role->hasPrivilege('SUPER_USER')));
    define('CAN_DELETE_ADMIN_ACC', ($_admin_role->hasPrivilege('DELETE_ADMIN_PROFILES') || IS_SUPER_USER));
} else {
    define('IS_SUPER_USER', FALSE);
    define('CAN_DELETE_ADMIN_ACC', FALSE);
}



