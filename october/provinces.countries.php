<?php



define ('OUTPUT', 'HTML'); // Whether to output as JSON or HTML (especially errors)...


// LOAD APPLICATION TOP...
require_once ('app_top.php');

$WHITE_LIST = array(
						'id'
					);




// SANITIZE GET DATA...
$_CLEAN = filter_input_array(INPUT_GET, 
								array(	
										 // country id...
										'id'	=>	array('filter'		=>	FILTER_VALIDATE_REGEXP, 
														'options'	=>	array('regexp' => '/^[A-Za-z]{2}$/')
																)

										)
								);



// Check if INPUT keys are ALL expected AND MADE IT THROUGH...
if (anySuspects()) _catch_error('Error processing white_list!', __LINE__, true);



$country = Cataleya\Geo\Country::load($_CLEAN['id']);
if ($country === NULL) _catch_error('Country not found!', __LINE__, true);


?>




<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Shop Admin | Country | <?=$country->getPrintableName()?> (<?= Cataleya\Helper::countInEnglish($country->getPopulation(), 'province', 'provinces') ?>)</title>


<!-- JS FILES -->
<?php
require_once(INC_PATH.'js_libs.php');
?>

<script type="text/javascript" src="ui/jscript/countries.js"></script>
<script type="text/javascript" src="ui/jscript/provinces.js"></script>


<!-- STYLESHEETS -->
<?php
require_once(INC_PATH.'css_libs.php');
?>

<link href="ui/css/provinces.css" rel="stylesheet" type="text/css" />


<style type="text/css" media="all">
.content {
	height:1000px;
	
}

#search-tool-wrapper {
	left:650px;
	top:130px;
	
	
}




</style>


<script type="text/javascript">


</script>


</head>

<body>

<!-- BEGIN: META -->
<input type="hidden" id="meta_token" value="<?=AUTH_TOKEN?>"  />
<input type="hidden" id="meta_country_code" value="<?=$country->getCountryCode()?>"  />
<!-- END: META -->




<div class="container">
<div class="header-wrapper absolute">
    <div class="header">

        <a href="<?=BASE_URL?>landing.countries.php"><div id="page-title">Country</div></a><?php include_once INC_PATH.'page.nav.php'; ?> 

    </div>
    
    
<?php
        include_once INC_PATH.'main.nav.php';
?>
    
</div>


  <div class="content">



<div id="search-tool-wrapper">
		
<?php
define('QUICK_SEARCH_URL', BASE_URL.'country_search.ajax.php');
include INC_PATH.'quick-search-widget.php';
?>


</div>



<!-- Country NAME -->
<h2 id="country-name-in-header">
    <img class="national-flag-medium" src="<?=DOMAIN_FULL.'/'.ROOT_URI?>ui/images/ui/countries/48/<?=  strtolower($country->getCountryCode())?>.png" >
    <span id="country-name-in-header-text"><?=$country->getPrintableName()?> (<?=$country->getCountryCode()?>)</span>
</h2>





<div id="hor-line-1">&nbsp;</div>




<!-- Country INFO OUTER WRAPPER -->

<ul id="country-info-outer-wrapper" >


<li>
    
<?php

$_population = $country->getPopulation();

?>
	
<!-- Country INFO -->
<div id="country-info">
    
<?php if ($country->provincesEnabled()): ?>
    
    <?php if ($_population > 1): ?>
    There <span class="province-count-1">are <?=$_population?> provinces</span> under <?=$country->getPrintableName()?>.
    <?php elseif ($_population > 0): ?>
    There <span class="province-count-1">is <?=$_population?> province</span> under <?=$country->getPrintableName()?>.
    <?php else: ?>
    There <span class="province-count-1">are no provinces</span> under <?=$country->getPrintableName()?>.
    <?php endif ?>
    <br/><br/><br/>
    <a href="javascript:void(0);" class="button-1" onClick="provinces.disableProvinces(true);">Click here to turn off provinces</a>
    
<?php else: ?>
    Provinces are presently disabled for this country (<?=$country->getPrintableName()?>) 
    <br/><br/><br/>
    <a href="javascript:void(0);" class="button-1" onClick="provinces.enableProvinces(true);">Click here to turn on provinces</a>
<?php endif ?> 
    
    
    
    

</div>
        

</li>
</ul>



<br/><br/><br/>





<ul id="province-list">
    <li class="header">Provinces <span class="province-count-2"><?= ($country->getPopulation() < 1) ? '' : '('.$country->getPopulation().')'; ?></span></li>
    
<?php if ($country->provincesEnabled()): ?>
    <li id="province-add-button-wrapper" >
        <a href="javascript:void(0);" id="province-add-button" onclick="editProvince(0); return false;" class="icon-plus-alt">Click here to add a new province for <?=$country->getPrintableName()?>.</a>
    </li>

    <?php if ($country->getPopulation() < 1): ?>
    <li class="province-tile last-child">
    There are no provinces under <?=$country->getPrintableName()?> (yet).
    </li>
    <?php endif ?>
    
<?php else: ?>
    Provinces are presently disabled for this country (<?=$country->getPrintableName()?>) 
    <br/><br/>
    <a href="javascript:void(0);" class="button-1" onClick="provinces.enableProvinces();">Click here to turn on provinces</a>
    <br/>
<?php endif ?> 
        

<?php
// GENERATE PROVINCES HERE... 


foreach ($country as $province) {

?>
        <li class="province-tile trashable" id="province-<?=$province->getID()?>"  trash_type="province-tile" data_size_id="<?=$province->getID()?>"  data_size_entry="<?=$province->getName()?>" >
            
        <?=$province->getPrintableName()?> (<?=$province->getIsoCode()?>)
        
        <a class="button-1 province-edit-button fltrt" href="javascript:void(0)" onclick="editProvince(<?=$province->getID()?>); return false;" >edit</a>
        <a class="button-1 province-delete-button fltrt" href="javascript:void(0)" onclick="provinces.removeProvince(<?=$province->getID()?>);" >remove</a>

        </li>
        
<?php

}

?>
        
      
</ul>


<div id="edit-province-dialog-box" class="alert-box">
    <form name="edit-province-form" id="edit-province-form"  method="POST" action="<?=BASE_URL?>save-province.ajax.php">
        <input type="hidden" name="auth-token" value="<?=AUTH_TOKEN?>"  />
        <input type="hidden" name="country-code" value="<?=$country->getCountryCode()?>"  />
        <input type="hidden" id="province-id" name="province-id" value=0  />
        
        <h2 class="bubble-title">New Province</h2>
        <div class="tiny-yellow-text">Enter a unique ISO code and a name then click save.</div>
        <br/>
        <p class="bubble-text">
                    <div class="fltlft">
                    <small class="text-input-label">ISO CODE</small>
                    <input id="text-field-province-iso" name="province-iso-code" tip="Please enter a unique 2 - 4 character iso code (that hasn't already been added) for the new province. <small>Tip: do a google search for 'provinces and iso codes in <?=$country->getPrintableName()?>'</small>" class="placeholder-color-1" type="text" value="" placeholder="" tinyStatus="#province-iso-status"  onkeypress="return validateISO(event, this);" onkeyup="return revalidateISO(event, this);"  size=5 maxlength="4" />
                    </div>

                    <div class="fltlft">
                    <small class="text-input-label">NAME</small>
                    <input id="text-field-province-name" name="province-name" tip="Please enter a name for the new province. <small>Use only A-Z, a-z, 0-9, spaces, dots, commas or dashes!</small>" class="placeholder-color-1" type="text" value="" placeholder="" tinyStatus="#province-name-status"  size=30 />
                    </div>  
        </p>

        <br /><br /><br/>
        <br />
        
        <a class="button-1" id="edit-province-button-cancel" href="javascript:void(0)" onClick="return false;" >cancel</a>
        &nbsp;&nbsp;
        <input type="submit" class="button-1" id="edit-province-button-ok" href="javascript:void(0)" value="add new province" />
        <br /><br/>
        <!-- <small class="bubble-tiny-text">click anywhere to close X</small> -->
    </form>
</div>


<div id="footer">
&nbsp;
</div>



</div>

</div>


</body>
</html>
