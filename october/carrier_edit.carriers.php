<?php



define ('OUTPUT', 'HTML'); // Whether to output as JSON or HTML (especially errors)...


// LOAD APPLICATION TOP...
require_once ('app_top.php');



// SANITIZE GET DATA...
$_CLEAN = filter_input_array(INPUT_GET, 
								array(
										'auth_token'		=>	FILTER_SANITIZE_STRIPPED, 
										't'		=>	FILTER_SANITIZE_STRIPPED, 
										'd'	=>	FILTER_SANITIZE_STRING, 
										'id'	=>	FILTER_VALIDATE_INT

										)
								);





/*
 * 
 * TO VIEW OR EDIT THIS PAGE, USER MUST BE AN ADMINISTRATOR
 * 
 */

$_admin_role = $adminUser->getRole();
if (!$_admin_role->hasPrivilege('VIEW_CARRIER_INFO') && !IS_SUPER_USER)  include_once INC_PATH.'forbidden.php';


define('READONLY', ($_admin_role->hasPrivilege('EDIT_CARRIER_INFO') || IS_SUPER_USER) ? FALSE : TRUE);




// IF EDIT
if ($_CLEAN['d'] === 'edit' && $_CLEAN['id'] !== NULL && $_CLEAN['id'] !== false)  {
	// A OK...
} 

// IF NEW
else if ($_CLEAN['d'] === 'new' && $_CLEAN['id'] === NULL && isset($_SESSION[SESSION_PREFIX.'NEW_TOKEN']) && $_SESSION[SESSION_PREFIX.'NEW_TOKEN'] === $_CLEAN['t'] )  {
	// A OK...
        if (!$_admin_role->hasPrivilege('CREATE_CARRIER') && !IS_SUPER_USER)  include_once INC_PATH.'forbidden.php';	
} 

// IF DELETE
else if ($_CLEAN['d'] === 'del' && $_CLEAN['id'] !== NULL && $_CLEAN['id'] !== false)  {
	// A OK...
        if (!$_admin_role->hasPrivilege('DELETE_CARRIER') && !IS_SUPER_USER)  include_once INC_PATH.'forbidden.php';	
} 

// IF NOTHING ADDS UP :D
else {
	// REDIRECT TO CATALOG LANDING
			Header ("Location: " . BASE_URL . "landing.carriers.php", true); 
			exit();
}






if ($_CLEAN['d'] == 'new') {
		// reset 'new carrier' token ...
		unset($_SESSION[SESSION_PREFIX.'NEW_TOKEN']);

		$carrier = Cataleya\Shipping\Carrier::create('Untitled', 'No description');

		
} 


// EDIT
else if ($_CLEAN['d'] == 'edit') {
	
		// PRODUCT...
		
		
		
		$carrier = Cataleya\Shipping\Carrier::load($_CLEAN['id']);
                
                if ($carrier === NULL) {
                	// REDIRECT TO CATALOG LANDING
			Header ("Location: " . BASE_URL . "landing.carriers.php", true); 
			exit();                   
                }
		

		
	
}


else {
	
	// REDIRECT TO CATALOG LANDING
        Header ("Location: " . BASE_URL . "landing.carriers.php", true); 
        exit();
	
}




// RETRIEVE CARRIER INFO...
define('CARRIER_ID', $carrier->getID());
$carrier_description = $carrier->getDescription();
$carrier_location = $carrier->getLocation();


?>




<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<?php
include "includes/meta.php";

?>

<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Edit Carrier (<?=$carrier_description->getTitle('EN')?>) | Shop Admin</title>


<!-- JS FILES -->
<?php
require_once(INC_PATH.'js_libs.php');
?>

<script type="text/javascript" src="ui/jscript/carrier.edit.js"></script>


<!-- STYLE SHEETS -->
<?php
require_once(INC_PATH.'css_libs.php');
?>
<link href="ui/css/bootmetro-tiles.css" rel="stylesheet" type="text/css" />
<link href="ui/css/carrier.edit.css" rel="stylesheet" type="text/css" />


<style type="text/css" media="screen">



</style>


</head>

<body>
<!-- BEGIN: META -->
<input type="hidden" id="meta_token" value="<?=AUTH_TOKEN?>"  />
<input type="hidden" id="meta_carrier_id" value="<?=CARRIER_ID?>"  />
<input type="hidden" id="meta_admin_root" value="<?=BASE_URL?>"  />


<input type="hidden" id="meta_r_token" value=""  />
<input type="hidden" id="meta_rw_token" value=""  />
<!-- END: META -->



<div class="container">
<?php include_once INC_PATH.'photo-upload-forms.php'; ?>
    
<!-- HEADER -->
<div class="header-wrapper fixed">
    <div class="header">

    <div id="page-title">Edit Carrier (<?=$carrier_description->getTitle('EN')?>)</div> 
    <?php include_once INC_PATH.'page.nav.php'; ?>   


    </div>
    

<div class="side-bar">
    
</div> 
    
    
<?php include_once INC_PATH.'main.nav.php'; ?>    
  
</div>


    
<div class="content">
<?php include_once INC_PATH.'photo-upload-forms.php'; ?>



<div id="carrier-edit-form-outer-wrapper" >

    <ul id="carrier-edit-form-wrapper">

        <li>
            
            <h3 class="section-header">Description</h3>
            <div id="carrier-description-wrapper">
                <small class="text-input-label">Carrier Name</small>
                <input <?=(READONLY ? 'DISABLED' : '')?> id="text-field-carrier-name" class="placeholder-color-1" name="carrier_name" type="text" value="<?=$carrier_description->getTitle('EN')?>" placeholder="Carrier Name"  tinyStatus="#carrier-name-status"  onchange="saveCarrierInfo(this)" size=30 />
                <small class="tiny-status" id="carrier-name-status">&nbsp;</small>
                <br/>
                <small class="text-input-label">Description</small>
                <textarea <?=(READONLY ? 'DISABLED' : '')?> id="text-field-carrier-description" class="placeholder-color-1" name="carrier_description" rows=6 cols="30" placeholder="Description..."  tinyStatus="#carrier-description-status"  onchange="saveCarrierInfo(this)" ><?=$carrier_description->getText('EN')?></textarea>
                <small class="tiny-status" id="carrier-description-status">&nbsp;</small>

                
            </div>
               <div style="height: 20px; width: 1px; clear: both;">&nbsp;</div>        
        </li>
        <li>
            
            <h3 class="section-header">Carrier Logo</h3>
            <div id="image-picker-wrapper">
                 <small class="text-input-label">Photo Upload</small>
                 <a href="javascript:void(0);" class="image-picker-add-button-a" onclick="initPhotoUpload(5); return false;">
                     <span class="image-picker-add-button" style="background: url('<?=$carrier->getLogo()->getHref(Cataleya\Asset\Image::LARGE)?>') no-repeat center center; background-size: contain;">&nbsp;
                     </span>
                 </a>     
             
            </div>
            

        </li>
        
        
        
       <li>
            
            <h3 class="section-header">Settings</h3>
            <div id="carrier-settings-wrapper">
            
  
            <small class="text-input-label">Enable or disable this carrier</small>
            <select <?=(READONLY ? 'DISABLED' : '')?> id="select-carrier-active" name="active" tinyStatus="#carrier-active-status"  onchange="saveCarrierInfo(this)"> 
                
                
                <option value=0 <?=(0 == (int)$carrier->getActive()) ? 'selected="selected"' : ''?> >Carrier is Disabled</option>
                <option value=1 <?=(1 == (int)$carrier->getActive()) ? 'selected="selected"' : ''?> >Carrier is Enabled</option>
                
            </select> 
            <small class="tiny-status select-status" id="carrier-active-status select-status">&nbsp;</small>
            
            <br />
            </div>
            <div style="height: 40px; width: 1px; clear: both;">&nbsp;</div> 
           
            
        </li>
        

        
       <li>
            
            <h3 class="section-header">Contact</h3>
            <div id="carrier-contact-wrapper">
                <form name="carrier-contact-form">
                <small class="text-input-label">Contact Person</small>
                <input <?=(READONLY ? 'DISABLED' : '')?> id="text-field-contact-name" name="contact_name" class="placeholder-color-1" type="text" value="<?=$carrier->getContactName()?>" placeholder="Contact Name" tinyStatus="#carrier-contact-name-status"  onchange="saveCarrierInfo(this)" size=30 />
                <small class="tiny-status" id="carrier-contact-name-status">&nbsp;</small>
                <br/>
                <small class="text-input-label">Telephone</small>
                <input <?=(READONLY ? 'DISABLED' : '')?> id="text-field-contact-phone" name="telephone" class="placeholder-color-1" type="text" value="<?=$carrier->getTelephone()?>" placeholder="Telephone" tinyStatus="#carrier-contact-phone-status"  onchange="saveCarrierInfo(this)" onkeypress="return checkPhoneDigit(event, this);" onkeyup="return recheckPhoneDigit(event, this);"  size=30 />
                <small class="tiny-status" id="carrier-contact-phone-status">&nbsp;</small>
                <br/>
                <small class="text-input-label">Email</small>
                <input <?=(READONLY ? 'DISABLED' : '')?> id="text-field-contact-email" name="email" class="placeholder-color-1" type="text" value="<?=$carrier->getEmail()?>" placeholder="Email"  tip="Enter contact's <strong>email address</strong> <small></small>" tinyStatus="#carrier-contact-email-status"  onchange="saveCarrierInfo(this)" size=30 />
                <small class="tiny-status" id="carrier-contact-email-status">&nbsp;</small>
                <br/> 
                </form>
                
            </div>   
           
            
        </li>
        
        
        
       <li>
            
            <h3 class="section-header">Carrier Location</h3>
            <div id="carrier-location-wrapper">
                <form name="carrier-location-form">
                <small class="text-input-label">Street Address</small>
                <input <?=(READONLY ? 'DISABLED' : '')?> id="text-field-street-address" name="street_address" class="placeholder-color-1" type="text" value="<?=$carrier_location->getEntryStreetAddress()?>" tinyStatus="#carrier-street-address-status"  onchange="saveCarrierInfo(this)" size=30 />
                <small class="tiny-status" id="carrier-street-address-status">&nbsp;</small>
                <br/>
                
<!--                
                <small class="text-input-label">Address Line 2</small>
                <input <?=(READONLY ? 'DISABLED' : '')?> id="text-field-suburb" name="suburb" class="placeholder-color-1" type="text" value="<?=$carrier_location->getEntrySuburb()?>" tinyStatus="#carrier-suburb-status"  onchange="saveCarrierAddress(this)" size=30 />
                <small class="tiny-status" id="carrier-suburb-status">&nbsp;</small>
                <br/>
-->                
                
                
                
                <small class="text-input-label">City</small>
                <input <?=(READONLY ? 'DISABLED' : '')?> id="text-field-city" name="city" class="placeholder-color-1" type="text" value="<?=$carrier_location->getEntryCity()?>" tinyStatus="#carrier-city-status"  onchange="saveCarrierInfo(this)" />
                <small class="tiny-status" id="carrier-city-status">&nbsp;</small>
                <br/>
                <small class="text-input-label">State / Province</small>
                <input <?=(READONLY ? 'DISABLED' : '')?> id="text-field-state" name="state" class="placeholder-color-1" type="text" value="<?=$carrier_location->getEntryState()?>" tinyStatus="#carrier-state-status"  onchange="saveCarrierInfo(this)" size=30 />
                <small class="tiny-status" id="carrier-state-status">&nbsp;</small>
                <br/> 
                 <small class="text-input-label">Postal Code</small>
                <input <?=(READONLY ? 'DISABLED' : '')?> id="text-field-postal-code" name="postcode" class="placeholder-color-1" type="text" value="<?=$carrier_location->getEntryPostcode()?>" tinyStatus="#carrier-postcode-status"  onchange="saveCarrierInfo(this)" size=30 />
                <small class="tiny-status" id="carrier-postcode-status">&nbsp;</small>
                <br/> 
                <small class="text-input-label">Country</small>
                 <select <?=(READONLY ? 'DISABLED' : '')?> id="select-carrier-country" name="country" tinyStatus="#carrier-country-status"  onchange="saveCarrierInfo(this)" size=1 />
                
                <?php
                $countries = Cataleya\Geo\Countries::load();
                
                foreach ($countries as $country)
                {
                ?>
                    <option value="<?=$country->getCountryCode()?>" <?=($country->getCountryCode() == $carrier_location->getEntryCountryCode()) ? 'selected="selected"' : ''?> ><?=$country->getPrintableName()?></option>
                <?php
                
                }
                ?>
                    
                </select> 
                <small class="tiny-status select-status" id="carrier-country-status">&nbsp;</small>
                <br/> 
                </form>
                
            </div>   
           

        </li>   
        
<?php if ((IS_SUPER_USER || $_admin_role->hasPrivilege('DELETE_CARRIER')) && !READONLY): ?>    
        <li>
            <h3 class="section-header">Delete Carrier</h3>
            <br/>
            <a class="button-1" href="javascript:void(0);" onClick="deleteCarrier(''); return false;" >
                Delete this carrier
            </a>
            <br/>
            
        </li>
<?php endif ?>
                
    </ul>



    
</div>
    



</div>
</div>


</body>
</html>
