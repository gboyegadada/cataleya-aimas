<?php



define ('OUTPUT', 'JSON'); // Whether to output as JSON or HTML (especially errors)...


// LOAD APPLICATION TOP...
require_once ('app_top.php');




$WHITE_LIST = array(
						'auth_token',  
                                                'target_id', 
                                                'do', 
                                                'data'
						);





// DIE IF INPUT IS NOT VIA POST METHOD...
if ($_SERVER['REQUEST_METHOD'] != 'POST') _catch_error('POST METHOD ONLY PLEASE. NOW GET.', __LINE__, true);

// SANITIZE INPUT DATA...
$_CLEAN = filter_input_array(INPUT_POST, array(
                                                    'auth_token'		=>	FILTER_SANITIZE_STRIPPED,
                                                    'target_id'	=>	FILTER_VALIDATE_INT, 
                                                    'data'	=>	array( 'flags' => FILTER_REQUIRE_ARRAY ), 
                                                    'do'	=>	array('filter'		=>	FILTER_VALIDATE_REGEXP, 
                                                                                                'options'	=>	array('regexp' => '/^[A-Za-z ]{1,50}$/')
                                                                                                                ), 

                                                    )
										
					);


// Check if INPUT keys are ALL expected AND MADE IT THROUGH...
$_suspect = anySuspects();
if ($_suspect !== FALSE) _catch_error('Error processing white_list!' . $_suspect, __LINE__, true);




// VALIDATE AUTH TOKEN...
validate_auth_token ();

// Validate task

$_FUNCS = array (
    'void' => array (), 
    'saveTitle' => array (
        'text' => 'string'
    ),  
    'saveBody' => array (
        'page_content' => 'html'
    ), 
    'save' => array (
        'page_title' => 'string', 
        'page_content' => 'html'
    ), 
    'autosave' => array (
        'page_title' => 'string', 
        'page_content' => 'html'
    ), 
    'getAutosave' => array (
        'page_id' => 'int'
    ), 
    'clearAutosave' => array (
        'page_id' => 'int'
    ), 
    'saveTemplate' => array (
        'template_id' => 'filename'
    ), 
    'savePageHandle' => array (
        'page_handle' => 'string'
    ), 
    'addPlugins' => array (
        'plugin_handles' => 'string_array'
    ), 
    'removePlugins' => array (
        'plugin_handles' => 'string_array'
    ), 
    'newPage' => array (
        'store_id' => 'int'
    ), 
    'getPageInfo' => array (
        'page_id' => 'int'
    ),
    'deletePage' => array (
        'page_id' => 'int'
    )
    
);






if (!array_key_exists($_CLEAN['do'], $_FUNCS) || !is_callable($_CLEAN['do'])) _catch_error('Invalid action.', __LINE__, true);



define('DASH_LANGUAGE_CODE', Cataleya\System::load()->getDashboardLanguageCode());
define('SHOPFRONT_LANGUAGE_CODE', Cataleya\System::load()->getShopFrontLanguageCode());
     

    
// OUTPUT...
 $json_reply = array (
                 "status" => 'ok', 
                 "message" => 'Ok'
                 );
 
 

// EXECUTE API CALL

// API FUNC CALL
$_CLEAN['do']();



// HELPER FUNCS



function digestParams () 
{
    global $_CLEAN, $_FUNCS;
    $_params = Cataleya\Helper\Validator::params($_FUNCS[$_CLEAN['do']], $_CLEAN['data']);
    $_bad_params = array ();
    

    if (!isset($_params['language_code'])) 
    {
        $_params['language_code'] = DASH_LANGUAGE_CODE;

    }
    
    
    
    
    
    // Check if every one made it through
    
    foreach ($_params as $k=>$v) {
        if ($v === NULL) $_bad_params[] = $k;
    }
    

    if (!empty($_bad_params)) _catch_error('Unable to save.' . implode(',', $_bad_params), __LINE__, true);
    
    return $_params;
}













/*
 * 
 * 
 * CALLBACKS
 * 
 */


function void () {
    
}






/*
 * 
 * [ newPage ]
 * ______________________________________________________
 * 
 * @param: int $_POST['target_id']
 * @param: int $_POST['data']
 * 
 */


function newPage () {

    global $json_reply;
    $_params = digestParams();
    
    $_Store = Cataleya\Store::load($_params['store_id']);
    if ($_Store === NULL ) _catch_error('Store could not be found.', __LINE__, true);
    
    $_Page = Cataleya\Front\Shop\Page::create($_Store, $_params['title'], $_params['description'], 'EN');
    
    $json_reply['message'] = 'New page saved.';
    $json_reply['Page'] = Cataleya\Front\Dashboard\Juicer::juice($_Page);

}










/*
 * 
 * [ saveProduct ]
 * ______________________________________________________
 * 
 * @param: int $_POST['target_id']
 * @param: int $_POST['data']
 * 
 */


function saveTitle () {

    global $_CLEAN, $json_reply;
    
    $_params = digestParams();
    $_Page = Cataleya\Front\Shop\Page::load($_CLEAN['target_id']);
    if ($_Page === NULL ) _catch_error('Page could not be found.', __LINE__, true);
    
    // [1] Page Description
    $_Page
            
    ->setTitle($_params['text'], 'EN');
    
    
    // Update url_rewrite...
    //$_Page->setSlug($_params['text']);
    
    
    $json_reply['message'] = 'Title saved.';
    $json_reply['Page'] = Cataleya\Front\Dashboard\Juicer::juiceDrop($_Page);

}






/*
 * 
 * [ saveBody ]
 * ______________________________________________________
 * 
 * @param: int $_POST['target_id']
 * @param: int $_POST['data']
 * 
 */


function saveBody () {

    global $_CLEAN, $json_reply;
    
    $_params = digestParams();
    $_Page = Cataleya\Front\Shop\Page::load($_CLEAN['target_id']);
    if ($_Page === NULL ) _catch_error('Page could not be found.', __LINE__, true);
    
    // [1] Page Description
    $_Page->setText($_params['page_content'], 'EN');
    
    
    // Update url_rewrite...
    // $_Page->setSlug($_params['text']);
    
    
    $json_reply['message'] = 'Body saved.';
    $json_reply['Page'] = Cataleya\Front\Dashboard\Juicer::juiceDrop($_Page);

}






/*
 * 
 * [ save ]
 * ______________________________________________________
 * 
 * @param: int $_POST['target_id']
 * @param: int $_POST['data']
 * 
 */


function save () {

    global $_CLEAN, $json_reply;
    
    $_params = digestParams();
    $_Page = Cataleya\Front\Shop\Page::load($_CLEAN['target_id']);
    if ($_Page === NULL ) _catch_error('Page could not be found.', __LINE__, true);
    
    // [1] Page Content
    $_Page->setTitle($_params['page_title'], 'EN');
    $_Page->setText($_params['page_content'], 'EN');
    
    $_Page->clearAutosave(Cataleya\Front\Dashboard\User::getInstance());
    
    
    $json_reply['message'] = 'Content saved.';
    $json_reply['Page'] = Cataleya\Front\Dashboard\Juicer::juiceDrop($_Page);

}








/*
 * 
 * [ autosave ]
 * ______________________________________________________
 * 
 * @param: int $_POST['target_id']
 * @param: int $_POST['data']
 * 
 */


function autosave () {

    global $_CLEAN, $json_reply;
    
    $_params = digestParams();
    $_Page = Cataleya\Front\Shop\Page::load($_CLEAN['target_id']);
    if ($_Page === NULL ) _catch_error('Page could not be found.', __LINE__, true);
    
    // [1] Page Description
    if (NULL === $_Page->autosave(Cataleya\Front\Dashboard\User::getInstance(), $_params['page_title'], $_params['page_content'], 'EN')) _catch_error('Page could not be autosaved.', __LINE__, true);
    
    
    
    $json_reply['message'] = 'Page autosaved at ' . date('h:iP');
    $json_reply['Page'] = Cataleya\Front\Dashboard\Juicer::juiceDrop($_Page);
    

}






/*
 * 
 * [ clearAutosave ]
 * ______________________________________________________
 * 
 * @param: int $_POST['target_id']
 * @param: int $_POST['data']
 * 
 */


function clearAutosave () {

    global $_CLEAN, $json_reply;
    
    
    $_Page = Cataleya\Front\Shop\Page::load($_CLEAN['target_id']);
    if ($_Page === NULL ) _catch_error('Page could not be found.', __LINE__, true);
    
    $_Page->clearAutosave(Cataleya\Front\Dashboard\User::getInstance());
    
    
    
    $json_reply['message'] = 'Auto save cleared at ' . date('h:iP');
    $json_reply['Page'] = Cataleya\Front\Dashboard\Juicer::juiceDrop($_Page);
    

}








/*
 * 
 * [ deleteProduct ]
 * ______________________________________________________
 * 
 * @param: int $_POST['target_id']
 * 
 * 
 */


function deletePage () {

    global $_CLEAN, $json_reply;
    $_params = digestParams();
 
    $_Page = Cataleya\Front\Shop\Page::load($_CLEAN['target_id']);
    if ($_Page === NULL ) _catch_error('Page could not be found.', __LINE__, true);
    
    if ($_params['auto_confirm'] !== TRUE) 
    {
        $message = 'Are you sure you want to delete this page: "' . $_Page->getDescription()->getTitle('EN') . '"?';
        confirm($message, FALSE);
    }
    

    $_Page->delete();
    
    $json_reply['message'] = 'Page deleted.';
    $json_reply['Product'] = array ('id'=>$_CLEAN['target_id']);

}










/*
 * 
 * [ getPageInfo ]
 * ______________________________________________________
 * 
 * @param: int $_POST['target_id']
 * 
 * 
 */


function getPageInfo () {

    global $_CLEAN, $json_reply;
 
    $_Page = Cataleya\Front\Shop\Page::load((int)$_CLEAN['target_id']);
    if ($_Page === NULL ) _catch_error('Page could not be found.', __LINE__, true);
    
    $_Store = $_Page->getStore();
    
    $json_reply['message'] = 'Page info.';
    $json_reply['Page'] = Cataleya\Front\Dashboard\Juicer::juice($_Page);
    $json_reply['Theme'] = Cataleya\Front\Dashboard\Juicer::juice($_Store->getTheme());
    
    $_temps = $_apps = [];
    
    foreach ($json_reply['Theme']['page_templates'] as $k=>$v) {
        
        $_temps[] = array(
            'id' => $v, 
            'name' => $v, 
            'selected' => ($_Page->getTemplateID() === $v)
        );
        
    }
    
    foreach (\Cataleya\Plugins\Package::getAppList('page') as $_handle) {
        $_apps[] = \Cataleya\Plugins\Package::getAppInfo($_handle);
    }


    $json_reply['Theme']['page_templates'] = $_temps;
    $json_reply['AppList'] = $_apps;

}







/*
 * 
 * [ getPageInfo ]
 * ______________________________________________________
 * 
 * @param: int $_POST['target_id']
 * 
 * 
 */


function getAutosave () {

    global $_CLEAN, $json_reply;
 
    $_Page = Cataleya\Front\Shop\Page::load((int)$_CLEAN['target_id']);
    if ($_Page === NULL ) _catch_error('Page could not be found.', __LINE__, true);
    
    $_Autosave = $_Page->getAutosave(Cataleya\Front\Dashboard\User::getInstance());
    
    $json_reply['message'] = 'Autosave info.';
    $json_reply['Autosave'] = (NULL === $_Autosave) ? FALSE : $json_reply['Autosave'] = Cataleya\Front\Dashboard\Juicer::juice($_Autosave);
    
}









/*
 * 
 * [ saveTemplate ]
 * ______________________________________________________
 * 
 * @param: int $_POST['target_id']
 * @param: int $_POST['data']
 * 
 */


function saveTemplate () {

    global $_CLEAN, $json_reply;
    
    $_params = digestParams();
    $_Page = Cataleya\Front\Shop\Page::load($_CLEAN['target_id']);
    if ($_Page === NULL ) _catch_error('Page could not be found.', __LINE__, true);
    
    // [1] Page Description
    $_Page->setTemplateID($_params['template_id']);
    
    
    
    $json_reply['message'] = 'Template saved.';
    $json_reply['Page'] = Cataleya\Front\Dashboard\Juicer::juiceDrop($_Page);

}





/**
 * addPlugins
 *
 * @access public
 * @return void
 */
function addPlugins () {

    global $_CLEAN, $json_reply;
    
    $_params = digestParams();
    $_Page = Cataleya\Front\Shop\Page::load($_CLEAN['target_id']);
    if ($_Page === NULL ) _catch_error('Page could not be found.', __LINE__, true);

    try {
        $_Page->addPlugin($_params['plugin_handles']);
    } catch (Exception $e) {
        _catch_error('Plugin' . ((count($_params['plugin_handles'])>1) ? 's' : '') . ' could not be added: '.$e->getMessage(), __LINE__, true);
    }
    
    $json_reply['message'] = 'Plugin added.';
    // $json_reply['Page'] = Cataleya\Front\Dashboard\Juicer::juiceDrop($_Page);

    foreach ($_Page->getAppList() as $_handle) {
        $_apps[] = \Cataleya\Plugins\Package::getAppInfo($_handle);
    }

    $json_reply['AppList'] = $_apps;

}







/**
 * removePlugins
 *
 * @access public
 * @return void
 */
function removePlugins () {

    global $_CLEAN, $json_reply;
    
    $_params = digestParams();
    $_Page = Cataleya\Front\Shop\Page::load($_CLEAN['target_id']);
    if ($_Page === NULL ) _catch_error('Page could not be found.', __LINE__, true);

    try {
        $_Page->removePlugin($_params['plugin_handles']);
    } catch (Exception $e) {
        _catch_error('Plugin' . ((count($_params['plugin_handles'])>1) ? 's' : '') . ' could not be removed: '.$e->getMessage(), __LINE__, true);
    }
    
    $json_reply['message'] = 'Plugin removed.';
    $json_reply['AppList'] = $_params['plugin_handles'];
    // $json_reply['Page'] = Cataleya\Front\Dashboard\Juicer::juiceDrop($_Page);

}




/*
 * 
 * [ savePageHandle ]
 * ______________________________________________________
 * 
 * @param: int $_POST['target_id']
 * @param: int $_POST['data']
 * 
 */


function savePageHandle () {

    global $_CLEAN, $json_reply;
    
    $_params = digestParams();
    $_Page = Cataleya\Front\Shop\Page::load($_CLEAN['target_id']);
    if ($_Page === NULL ) _catch_error('Page could not be found.', __LINE__, true);
    
    // [1] Page Description
    $_Page->setHandle($_params['page_handle']);
    
    
    
    $json_reply['message'] = 'Page handle saved.';
    $json_reply['Page'] = Cataleya\Front\Dashboard\Juicer::juiceDrop($_Page);

}







// Remove token...
if (isset($_SESSION[SESSION_PREFIX.'CONFIRM_TOKEN'])) unset ($_SESSION[SESSION_PREFIX.'CONFIRM_TOKEN']);




 echo (json_encode($json_reply));
 exit();  
