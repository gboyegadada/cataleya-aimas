<?php



define ('OUTPUT', 'JSON'); // Whether to output as JSON or HTML (especially errors)...


// LOAD APPLICATION TOP...
require_once ('app_top.php');




$WHITE_LIST = array(
						'auth_token',  
                                                'target_id', 
                                                'do', 
                                                'data'
						);





// DIE IF INPUT IS NOT VIA POST METHOD...
if ($_SERVER['REQUEST_METHOD'] != 'POST') _catch_error('POST METHOD ONLY PLEASE. NOW GET.', __LINE__, true);

// SANITIZE INPUT DATA...
$_CLEAN = filter_input_array(INPUT_POST, array(
                                                    'auth_token'		=>	FILTER_SANITIZE_STRIPPED,
                                                    'target_id'	=>	FILTER_VALIDATE_INT, 
                                                    'data'	=>	array( 'filter'=> FILTER_SANITIZE_STRIPPED, 'flags' => FILTER_REQUIRE_ARRAY ), 
                                                    'do'	=>	array('filter'		=>	FILTER_VALIDATE_REGEXP, 
                                                                                                'options'	=>	array('regexp' => '/^[A-Za-z ]{1,50}$/')
                                                                                                                ), 

                                                    )
										
					);


// Check if INPUT keys are ALL expected AND MADE IT THROUGH...
$_suspect = anySuspects();
if ($_suspect !== FALSE) _catch_error('Error processing white_list!' . $_suspect, __LINE__, true);




// VALIDATE AUTH TOKEN...
validate_auth_token ();

// Validate task

$_FUNCS = array (
    'void', 
    'sendEmail', 
    
    'saveOrder', 
    'getOrder', 
    'deleteOrder'
    
);





if (!in_array($_CLEAN['do'], $_FUNCS) || !is_callable($_CLEAN['do'])) _catch_error('Invalid action.', __LINE__, true);




    
// OUTPUT...
 $json_reply = array (
                 "status" => 'ok', 
                 "message" => 'Ok'
                 );
 
 

// EXECUTE API CALL

// API FUNC CALL
$_CLEAN['do']();



// HELPER FUNCS


function digestParams () 
{
    global $_CLEAN;
    $_params = $_CLEAN['data'];
    $_bad_params = array ();
    
    
    if ($_CLEAN['do'] === 'sendEmail') 
    {

        $_params['to'] = Cataleya\Helper\Validator::email(explode(',', $_params['to']), TRUE);
        if (empty($_params['to'])) $_bad_params[] = 'to';
        
        $_params['subject'] = Cataleya\Helper\Validator::text($_params['subject'], 0, 200);
        if ($_params['subject'] === FALSE) $_bad_params[] = 'subject';
        
        $_params['body'] = Cataleya\Helper\Validator::html($_params['body'], 0, 2000);
        if ($_params['body'] === FALSE) $_bad_params[] = 'body';
    }
    

    // Check if every one made it through
    if (!empty($_bad_params)) validationFailed ($_bad_params);
    
    return $_params;
}










function juiceOrder (Cataleya\Store\Order $_Order) 
{

    $_Store = $_Order->getStore();
    $_Currency = $_Order->getCurrency();
    $_Customer = $_Order->getCustomer();
    $_Recipient = $_Order->getRecipient();
    
    $_Transaction = NULL;
    
    $_TranxInfo = array (
        'TranxID' => '#XXXX', 
        'PaymentGateway' => 'Unknown', 
        'AmountPaid' => 0, 
        'PaymentStatus' => 'Unknown'
    );


    foreach (Cataleya\Payment\Transactions::load($_Order) as $_Tranx) 
    {
        $_Transaction = $_Tranx;
        break;
    }

    if (!empty($_Transaction)) 
    {   
        $_TranxInfo['TranxID'] = '#'.str_pad(strval($_Transaction->getID()), 5, '0', STR_PAD_LEFT);
        $_TranxInfo['PaymentGateway'] = $_Transaction->getPaymentType()->getName();
        $_TranxInfo['AmountPaid'] = number_format($_Transaction->getAmount(), 2) . ' ' . $_Order->getCurrencyCode();
        $_TranxInfo['PaymentStatus'] = $_Transaction->getStatus();
        $_TranxInfo['PaymentStatusClass'] = str_replace(' ', '-', $_Transaction->getStatus());
    }

    $_Shipping = array (
        'Street' => $_Recipient->getEntryStreetAddress() . ', ', 
        'Suburb'  =>  $_Recipient->getEntrySuburb(), 
        'City'    =>  $_Recipient->getEntryCity(), 
        'Postcode'    =>  $_Recipient->getEntryPostcode(), 
        'Province'    =>  $_Recipient->getEntryProvince() . ', ', 
        'Country' =>  $_Recipient->getEntryCountry()
    );

    $_Shipping['Suburb'] .= ((empty($_Shipping['Suburb'])) ? '' : ', ');
    $_Shipping['City'] .= ((empty($_Shipping['City'])) ? '' : ', ');
    $_Shipping['Postcode'] .= ((empty($_Shipping['Postcode'])) ? '' : ', ');


    
    $_info = array (
        'id'    =>   $_Order->getID(), 
        'orderNum'    =>   str_pad(strval($_Order->getID()), 5, '0', STR_PAD_LEFT), 
        'status'    =>   $_Order->getOrderStatus(), 
        'StatusFlag'    =>   array ($_Order->getOrderStatus() => TRUE), 
        'total' =>  number_format($_Order->getTotalCost(), 2), 
        'grandTotal'    =>  number_format($_Order->getGrandTotal(), 2), 
        'shippingCharges'   =>  number_format($_Order->getShippingCharges(), 2), 
        'quantity'  =>  $_Order->getQuantity(), 
        'population'    =>  $_Order->getPopulation(), 
        'orderDate' =>  $_Order->getOrderDate(FALSE)->format('h:ia, d M, Y'), 
        'Items' =>  array (), 
        'Discounts' =>  $_Order->getDiscounts(), 
        'Taxes' =>  $_Order->getTaxes(), 
        
        'Customer' => array (
            'id'  =>  $_Customer->getCustomerId(), 
            'name'  =>  $_Customer->getName(), 
            'firstname'  =>  $_Customer->getFirstname(), 
            'lastname'  =>  $_Customer->getLastname(), 
            'phone'  =>  $_Customer->getTelephone(), 
            'email'  =>  $_Customer->getEmailAddress()
        ), 
        'Recipient' => array (
            'name'  =>  ($_Recipient->getEntryFirstname() . ' ' . $_Recipient->getEntryLastname()), 
            'firstname'  =>  $_Recipient->getEntryFirstname(), 
            'lastname'  =>  $_Recipient->getEntryLastname()
        ),
        'Currency'  =>  array (
            'name'  =>  $_Currency->getCurrencyName(), 
            'code'  =>  $_Currency->getCurrencyCode(), 
            'utf'  =>  $_Currency->getUtfCode(), 
            'symbol' =>  ('&#'.$_Currency->getUtfCode().';')
        ), 
        'Store' =>  array (
            'id'    =>  $_Store->getStoreId(),
            'title' =>  $_Store->getDescription()->getTitle('EN'), 
            'description'   =>  $_Store->getDescription()->getText('EN')
        ), 
        'Shipping'    =>  array (
            'amount'  =>  $_Order->getShippingCharges(), 
            'amountBeforeTax'  =>  $_Order->getShippingCharges(FALSE, TRUE), 
            'description'  =>  $_Order->getShippingDescription(), 
            'Taxes' =>  $_Order->getShippingTaxes()
        ), 
        'ShippingAddress' => $_Shipping, 
        'Transaction' => $_TranxInfo
    );
    
    
    foreach ($_Order as $_OrderDetail) $_info['Items'][] = juiceOrderDetail($_OrderDetail);
    
    return $_info;
    
    
}







function juiceOrderDetail (Cataleya\Store\OrderDetail $_OrderDetail) 
{

    
    $_info = array (
        'id'    =>   $_OrderDetail->getID(), 
        'itemName'  =>  $_OrderDetail->getItemName(), 
        'itemDescription'   =>  $_OrderDetail->getItemDescription(), 
        'price' =>  number_format($_OrderDetail->getPrice(), 2), 
        'originalPrice' =>  number_format($_OrderDetail->getOriginalPrice(), 2), 
        'subtotal'  =>  number_format($_OrderDetail->getSubtotal(), 2), 
        'originalSubtotal'  =>  number_format($_OrderDetail->getSubtotal(FALSE), 2), 
        'quantity'  =>  $_OrderDetail->getQuantity(), 
        
        'couponCode'    =>  $_OrderDetail->getCouponCode(), 
        'couponDiscount'    =>  number_format($_OrderDetail->getCouponDiscountAmount(), 2), 
        
        'saleDescription'   =>  $_OrderDetail->getSaleDescription(), 
        'saleDiscount'  =>  number_format($_OrderDetail->getSaleDiscountAmount(), 2)
        
    );
    
    return $_info;
    
    
}









/*
 * 
 * 
 * CALLBACKS
 * 
 */


function void () {
    
}









/*
 * 
 * [ getOrder ]
 * ______________________________________________________
 * 
 * @param: int $_POST['target_id']
 * @param: int $_POST['data']
 * 
 */


function getOrder () {

    global $json_reply, $_CLEAN;

    
    $_Order = Cataleya\Store\Order::load($_CLEAN['target_id']);
    if ($_Order === NULL ) _catch_error('Order could not be loaded.', __LINE__, true);
    
    
    $json_reply['message'] = 'Order info';
    $json_reply['Order'] = juiceOrder($_Order);

};






/*
 * 
 * [ sendEmail ]
 * ______________________________________________________
 * 
 * @param: int $_POST['target_id']
 * @param: int $_POST['data']
 * 
 */


function sendEmail () {

    global $json_reply, $_CLEAN, $adminUser, $twig_vars;
    $_params = digestParams();
    
    $_Order = Cataleya\Store\Order::load($_CLEAN['target_id']);
    if ($_Order === NULL ) _catch_error('Order could not be loaded.', __LINE__, true);
    
    $twig_vars = array (
        'header_title' => $_params['subject'], 
        'inline_title' => 'New Message', 
        'title_icon' => ((SSL_REQUIRED ? 'https://' : 'http://'). HOST . '/' . ROOT_URI) . 'ui/images/ui/notification-icons/email-50x50.png', 
        'body' => (preg_replace('![\n]!', '<br />', $_params['body']))
    );


    // Load template...
    $template = Cataleya\Layout\Twiggy::loadTemplate('admin-message.email.html.twig');

    $_Email = Cataleya\Helper\Emailer::getInstance('order-details.mailer');
    
    
    $_Email
    ->setTo((is_array($_params['to']) ? $_params['to'] : array($_params['to']))) // array('user@email.com', 'user2@email.com'=>'Bola Kalejaye');
    ->setFrom(array('no-reply@'.EMAIL_HOST => $adminUser->getName())) // array('no-reply@aimas.com.ng'=>'Aimas Notification')
    ->setSender('no-reply@'.EMAIL_HOST)
    ->setReplyTo($adminUser->getEmailAddress())
    ->setSubject ($_params['subject'])
    ->setHTMLBody($template->render($twig_vars))
    ->setTextBody($_params['body'])
            
    ->attach($_Order->makePDF());
    
    // if (count($_params['to']) > 1) $_Email->setBcc(array_slice($_params['to'], 1, count($_params['to'])-1));
    
    
    if ($_Email->send() === 0) _catch_error('Email could not be sent!', __LINE__, true);;

    
    $json_reply['message'] = 'Email sent!';
    $json_reply['Order'] = array('id' => $_Order->getID());

};










    








// Remove token...
if (isset($_SESSION[SESSION_PREFIX.'CONFIRM_TOKEN'])) unset ($_SESSION[SESSION_PREFIX.'CONFIRM_TOKEN']);




 echo (json_encode($json_reply));
 exit();  

?>