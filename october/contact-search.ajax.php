<?php
/*
FETCH CATEGORIES AND RETURN AS JSON...
*/


define ('OUTPUT', 'JSON'); // Whether to output as JSON or HTML (especially errors)...


// LOAD APPLICATION TOP...
require_once ('app_top.php');

/*
 * 
 * CHECK USER PRIVILEGES
 * 
 */


if ((!isset($_admin_role) || !$_admin_role->hasPrivilege('VIEW_ADMIN_PROFILES')) && !IS_SUPER_USER) forbidden();



$WHITE_LIST = array(
						'auth_token', 
						'term'
					);



// RETRIEVE DATA ONLY IF VIA POST METHOD...
if ($_SERVER['REQUEST_METHOD'] != 'POST') _catch_error('POST METHOD ONLY PLEASE. NOW GET.', __LINE__, true);
	



// SANITIZE INPUT DATA...
$_CLEAN = filter_input_array(INPUT_POST, 
								array(
										'auth_token'		=>	FILTER_SANITIZE_STRIPPED, 
										'term'		=>	FILTER_SANITIZE_STRING

										)
								);




// Check if INPUT keys are ALL expected AND MADE IT THROUGH...
if (anySuspects()) 
{


    // Output...
    $json_data = array (
                    'status' => 'Ok', 
                    'message' => 'No admin profiles found.', 
                    'AdminProfiles' => array(), 
                    'count' => 0
                    );
    echo json_encode($json_data);
    exit();

    
}







// First, take out funny looking characters...
$_CLEAN['term'] = preg_replace('/[^-\s\.A-Za-z0-9@*#+]+/', '', $_CLEAN['term']);


$_data = array ();


// Load search results as a collection [ _AdminUsers ] objects...
$_admin_search = Cataleya\Admin\Users::search($_CLEAN['term']);

$admin_profiles_info = array ();

foreach ($_admin_search as $AdminUser) 
{
    
    $_data[] = array (
        'id'    =>  'a'.$AdminUser->getAdminId(), 
        'name'  =>  $AdminUser->getName() . ' <' . $AdminUser->getEmail() . '>', 
    );
    
    $admin_profiles_info[] = array (
        'id'    =>  $AdminUser->getAdminId(), 
        'name'  =>  $AdminUser->getName(), 
        'firstname'  =>  $AdminUser->getFirstname(), 
        'lastname'  =>  $AdminUser->getLastname(), 
        'email'  =>  $AdminUser->getEmail()
    );
}





// Load search results as a collection [ _Customer ] objects...
$_customer_search = Cataleya\Customers::search($_CLEAN['term']);

$customers = array();

foreach ($_customer_search as $customer) {
    
        $_data[] = array (
            'id'    =>  'c'.$customer->getCustomerId(), 
            'name'  =>  $customer->getName() . ' <' . $customer->getEmailAddress() . '>', 
        );
    
        $customers[] = array (
          'id'  =>  $customer->getCustomerId(), 
          'firstname'   =>    $customer->getFirstname(),
          'lastname'    =>  $customer->getLastname(), 
          'email'   =>  $customer->getEmailAddress()
        );
        
}





$_count = (count($admin_profiles_info) + count($customers));

// Output...
$json_data = array (
		'status' => 'Ok', 
		'message' => $_count . ' contacts found.', 
		'AdminUsers' => $admin_profiles_info, 
                'Customers' =>  $customers, 
                'Tokens'   =>  $_data, 
		'count' => $_count
		);

echo json_encode($json_data);
exit();




?>