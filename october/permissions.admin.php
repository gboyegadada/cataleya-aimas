<?php

define ('OUTPUT', 'HTML'); // Whether to output as JSON or HTML (especially errors)...


// LOAD APPLICATION TOP...
require_once ('app_top.php');


/*
 * 
 * TO VIEW OR EDIT THIS PAGE, USER MUST BE AN ADMINISTRATOR
 * 
 */


if (!$adminUser->getRole()->hasPrivilege(PRVLG_SUPER_USER))  include_once INC_PATH.'forbidden.php';

?>





<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<?php
include "includes/meta.php";

?>

<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Settings | Permissions</title>

<?php
require_once(INC_PATH.'js_libs.php');
?>
<script type="text/javascript" src="ui/jscript/libs/jquery.form.js"></script>
<script type="text/javascript" src="ui/jscript/permissions.admin.js"></script>



<?php
require_once(INC_PATH.'css_libs.php');
?>

<link href="ui/css/permissions.edit.css" rel="stylesheet" type="text/css" />


<style type="text/css" media="screen">


#page_h_tweak {
	position:absolute;
	top:1000px;
	left:0px;
	
	width:1px;
	height:1px;
}



#tile_grid {
	position:absolute;
	top:160px;
	left:350px;
	
	height:600px;
	width:590px;
	
	border:solid 1px #222;

	display:none;

}



</style>

<script type="text/javascript">


</script>


</head>

<body>
<!-- BEGIN: META -->
<input type="hidden" id="meta_token" value="<?=AUTH_TOKEN?>"  />
<input type="hidden" id="meta_admin_root" value="<?=BASE_URL?>"  />

<!-- END: META -->




<div class="container">
<?php include_once INC_PATH.'photo-upload-forms.php'; ?>
    
    
<div class="header-wrapper absolute">
    <div class="header">

    <div id="page-title">Permissions</div>   

    </div>
    
<?php include_once INC_PATH.'main.nav.php'; ?>
    
</div>

<div class="content">




<ul id="sidebar">
    <li>
        
    </li>
    
    
</ul>


<div id="configuration-list-outer-wrapper" >

    <ul id="configuration-list-wrapper">
        

        
<?php
    // Load Configurations
    $admin_roles = Cataleya\Admin\Roles::load();
    $privileges = Cataleya\Admin\Privileges::load();


    
    foreach ($admin_roles as $role) 
    {
        $css_id = str_replace('.', '-', $role->getID());
        
?>

        <li id="admin-role-wrapper-<?=$css_id?>">
            <a href="javascript:void(0);" onclick="togglePermissionsPane('admin-role-wrapper-<?=$css_id?>'); return false;">
                <h3 class="section-header icon-user-5"><?=$role->getDescription()->getTitle('EN')?>&nbsp;&nbsp;<span class="config-edit-icon icon-pencil"></span></h3>
            </a>
            <div class="config-description" style="display:block" >
                <small class="text-input-label"><?=$role->getDescription()->getText('EN')?></small>
            </div>
            
            <form id="permissions-form-<?=$css_id?>" name="permissions-form-<?=$css_id?>" method="POST" action="<?=BASE_URL?>save-permissions.ajax.php" >
            <input type="hidden" name="auth_token" value="<?=AUTH_TOKEN?>" />
            <input type="hidden" name="role_id" value="<?=$role->getID()?>" />
            
            <div class="permissions-pane" id="permissions-pane-<?=$css_id?>">
 <?php
 
 
    if (!$role->hasPRivilege(PRVLG_SUPER_USER)) 
    {

            foreach ($privileges as $privilege) 
           {
            if ($privilege->getID() === PRVLG_SUPER_USER) continue;
                
            $css_id2 = 'privilege-' . $role->getID() . '-' . strtolower(str_replace('_', '-', $privilege->getID()));
                
            ?>
                
            <a href="javascript:void(0);" id="<?=$css_id2?>-anchor" class="privilege-anchor <?=($role->hasPrivilege($privilege)) ? 'icon-checkbox-partial' : 'icon-checkbox-unchecked'?>" onclick="togglePermission('<?=$css_id2?>'); return false;"><?=$privilege->getDescription()->getTitle('EN')?></a>
            <small class="tiny-status" id="<?=$css_id2?>-status">&nbsp;</small>
            <input type="hidden" id="<?=$css_id2?>-input" name="privileges[<?=$privilege->getID()?>]" value="-1" />
                       <br/>
            <?php

           }
           
           
 ?>
            <a class="button-2" href="javascript:void(0);" onClick="savePermissions('permissions-form-<?=$css_id?>'); return false;" >
               save permissions
            </a> 
   <?php        
           
    } else {
            ?>
                       <span id="super-user-perm" class="privilege-anchor icon-checkbox-partial" ><?=Cataleya\Admin\Privilege::load(PRVLG_SUPER_USER)->getDescription()->getTitle('EN')?></span>
                       <br/>
            <?php  
    }
    
    ?>
            
 
             <br /><br /><br />
             
            
            </div>
            </form>
                   
        </li>

 <?php
 
    }
    
    ?>
        
        
        
        

        

    
    </ul>



    
</div>        


<!--

<div id="footer">
&nbsp;
</div>

-->
  </div>
</div>



<div id="page_h_tweak">&nbsp;</div>

</body>
</html>