<?php



define ('OUTPUT', 'JSON'); // Whether to output as JSON or HTML (especially errors)...


// LOAD APPLICATION TOP...
require_once ('app_top.php');





/*
 * 
 * TO PERFORM THIS OPERATION, USER MUST BE AN ADMINISTRATOR
 * 
 */


if (!IS_SUPER_USER || $_admin_role->hasPrivilege('DELETE_TAX_RATE')) _catch_error('You do have permission to perform this action.', __LINE__, true);




$WHITE_LIST = array(
						'auth_token', 
						'tax_rate_id'
						);





// DIE IF INPUT IS NOT VIA POST METHOD...
if ($_SERVER['REQUEST_METHOD'] != 'POST') _catch_error('POST METHOD ONLY PLEASE. NOW GET.', __LINE__, true);

// SANITIZE INPUT DATA...
$_CLEAN = filter_input_array(INPUT_POST, array(
                                                    'auth_token'		=>	FILTER_SANITIZE_STRIPPED, 
                                                    'tax_rate_id'	=>	FILTER_VALIDATE_INT

                                                    )
										
					);


// Check if INPUT keys are ALL expected AND MADE IT THROUGH...
if (anySuspects()) _catch_error('Error processing white_list!', __LINE__, true);



// VALIDATE AUTH TOKEN...
validate_auth_token ();


// Load 
$TaxRate = Cataleya\Tax\TaxRate::load($_CLEAN['tax_rate_id']);


// Check if tax rate exists...
if ($TaxRate === NULL)
{
        if (isset($_SESSION[SESSION_PREFIX.'CONFIRM_PARAMS'])) unset ($_SESSION[SESSION_PREFIX.'CONFIRM_PARAMS']);
        
        // ERROR...
        _catch_error('Tax rate not found.', __LINE__, true);

}


// Confirm delete action
$message = 'Are you sure you want to remove this tax rate?';
confirm($message, FALSE);

// retrieve info about tax rate
$TaxRate_info = array (
        'id'    =>  $TaxRate->getID(), 
        'rate'   =>  $TaxRate->getRate(), 
        'name'  =>  $TaxRate->getDescription()->getTitle('EN'), 
        'description' => $TaxRate->getDescription()->getText('EN')
    );


// retrieve info about tax class
$TaxClass = $TaxRate->getTaxClass();

$TaxClass_info = array (
        'id'    =>  $TaxClass->getID(), 
        'name'  => $TaxClass->getDescription()->getTitle('EN'), 
        'description'  =>  $TaxClass->getDescription()->getText('EN'),
        'population' => $TaxClass->getPopulation(), 
        'populationAsText' => count_in_english($TaxClass->getPopulation(), 'Tax rate', 'Tax rates')
    );


// Delete tax rate (if we make it past the 'confirm' function)...
$TaxRate->delete();




// Remove token...
if (isset($_SESSION[SESSION_PREFIX.'CONFIRM_TOKEN'])) unset ($_SESSION[SESSION_PREFIX.'CONFIRM_TOKEN']);




// OUTPUT...
 $json_reply = array (
                 "status" => 'ok', 
                 "message" => 'Tax rate deleted.', 
                 "TaxRate" => $TaxRate_info, 
                 "TaxClass"=>$TaxClass_info
                 );


 echo (json_encode($json_reply));
 exit();  

?>