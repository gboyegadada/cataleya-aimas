<?php



define ('OUTPUT', 'JSON'); // Whether to output as JSON or HTML (especially errors)...


// LOAD APPLICATION TOP...
require_once ('app_top.php');




/*
 * 
 * TO PERFORM THIS OPERATION, USER MUST BE AN ADMINISTRATOR
 * 
 */


if (!IS_SUPER_USER && !$_admin_role->hasPrivilege('CREATE_CUSTOMER')) _catch_error('You do not have permission to create a new customer profile.', __LINE__, TRUE, TRUE);



$WHITE_LIST = array(
						'auth_token', 
						'rw_token', 
						'fname', 
						'lname', 
						'dob', 
						'email', 
						'password',   
						'tel'
					);




// SANITIZE GET DATA...
$_CLEAN = filter_input_array(INPUT_POST, 
								array(
										'auth_token'	=>	FILTER_SANITIZE_STRIPPED, 
										'rw_token'	=>	FILTER_SANITIZE_STRIPPED, 
										'fname'	=>	array('filter'		=>	FILTER_VALIDATE_REGEXP, 
														'options'	=>	array('regexp' => '/[-\s\.A-Za-z0-9]{1,60}/')
																), 
										'lname'	=>	array('filter'		=>	FILTER_VALIDATE_REGEXP, 
														'options'	=>	array('regexp' => '/[-\s\.A-Za-z0-9]{1,60}/')
																), 
										'dob'	=>	array('filter'		=>	FILTER_VALIDATE_REGEXP, 
														'options'	=>	array('regexp' => '#^(\d{1,2}[\/|-|\s|,|\:]){2}(?:19|20)\d{2}$#')
																), 
										'email'	=>	FILTER_VALIDATE_EMAIL, 
										'password'	=>	FILTER_SANITIZE_STRING, 
										'tel'	=>	FILTER_SANITIZE_NUMBER_INT 

										)
								);



// Check if INPUT keys are ALL expected AND MADE IT THROUGH...
if (anySuspects()) _catch_error('Error processing white_list!', __LINE__, true);




// AUTH FUNCTIONS...
require_once (INC_PATH.'functions/password_funcs.php');



$_new_customer = Cataleya\Customer::create($_CLEAN['email'], $_CLEAN['password']);
$_new_customer->setFirstname($_CLEAN['fname']);
$_new_customer->setLastname($_CLEAN['lname']);
$_new_customer->setDob($_CLEAN['dob']);
$_new_customer->setTelephone($_CLEAN['tel']);

// Auto confirm...
$_new_customer->confirm($_new_customer->getConfirmDigest(), $_new_customer->getConfirmCode());
	

// Output...
$json_data = array (
                'status' => 'Ok', 
                'message' => 'Voila! Customer info saved!', 
                'Customer' => array (
                    'id' => $_new_customer->getCustomerId(), 
                    'firstname' => $_new_customer->getFirstname(), 
                    'lastname' => $_new_customer->getLastname(), 
                    'name' => $_new_customer->getName()
                )
                );
echo json_encode($json_data);
exit();
		
?>
		
?>