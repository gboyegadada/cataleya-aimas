<?php
/*
FETCH CATEGORIES AND RETURN AS JSON...
*/


define ('OUTPUT', 'JSON'); // Whether to output as JSON or HTML (especially errors)...


// LOAD APPLICATION TOP...
require_once ('app_top.php');



/*
 * 
 * TO PERFORM THIS OPERATION, USER MUST BE AN ADMINISTRATOR
 * 
 */


if ((!isset($_admin_role) || !$_admin_role->hasPrivilege('VIEW_COUNTRIES'))  && !$adminUser->is('STORE_OWNER') && !IS_SUPER_USER) _catch_error('You do have permission to perform this action.', __LINE__, true);




$WHITE_LIST = array(
						'auth_token', 
						'term'
					);



// RETRIEVE DATA ONLY IF VIA POST METHOD...
if ($_SERVER['REQUEST_METHOD'] != 'POST') _catch_error('POST METHOD ONLY PLEASE. NOW GET.', __LINE__, true);
	



// SANITIZE INPUT DATA...
$_CLEAN = filter_input_array(INPUT_POST, 
								array(
										'auth_token'		=>	FILTER_SANITIZE_STRIPPED, 
										'term'		=>	FILTER_SANITIZE_STRING

										)
								);




// Check if INPUT keys are ALL expected AND MADE IT THROUGH...
if (anySuspects()) 
{
    // _catch_error('Error processing white_list!', __LINE__, true);
    
    // bail quietly

    // Output...
    $json_data = array (
                    'status' => 'Ok', 
                    'message' => 'No countries found.', 
                    'countries' => array(), 
                    'count' => 0
                    );
    echo json_encode($json_data);
    exit();

    
}







// First, take out funny looking characters...
$_CLEAN['term'] = preg_replace('/[^-\s\.A-Za-z0-9@*#+]+/', '', $_CLEAN['term']);




// Load search results as a collection [ _Country ] objects...
$search_results = Cataleya\Geo\Countries::search($_CLEAN['term']);

$countries = array();

foreach ($search_results as $country) {
        $_country_code = $country->getCountryCode();
	$countries[$_country_code] = new stdClass();

        $countries[$_country_code]->name = $country->getPrintableName();
        $countries[$_country_code]->iso2 = $_country_code;
        $countries[$_country_code]->iso3 = $country->getIso3();
        $countries[$_country_code]->provinces = $country->getPopulation();
        $countries[$_country_code]->provincesText = (($country->getPopulation() < 1) ? 'No': $country->getPopulation()) . ' Provinces';
			
	$countries[$_country_code]->url = BASE_URL.'provinces.countries.php?id=' . $_country_code;
}



// Output...
$json_data = array (
		'status' => 'Ok', 
		'message' => (($search_results->getPopulation() < 1) ? 'No': $search_results->getPopulation()) . ' countries found.', 
		'countries' => $countries, 
		'count' => $search_results->getPopulation()
		);
echo json_encode($json_data);
exit();




?>
