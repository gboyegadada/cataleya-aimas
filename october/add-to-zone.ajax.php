<?php
/*
FETCH PROVINCES AND RETURN AS JSON...
*/




define ('OUTPUT', 'JSON'); // Whether to output as JSON or HTML (especially errors)...


// LOAD APPLICATION TOP...
require_once ('app_top.php');






// DIE IF INPUT IS NOT VIA POST METHOD...
if ($_SERVER['REQUEST_METHOD'] != 'POST') _catch_error('POST METHOD ONLY PLEASE. NOW GET.');
	

$WHITE_LIST = array(
						'auth-token', 
						'country-code', 
                                                'zone-id', 
                                                'provinces', 
                                                'do'
					);

// FILTER VALUES ....
$_CLEAN = filter_input_array(INPUT_POST, 
								array(	
                                                                                // auth_token
                                                                                'auth-token'    =>  FILTER_SANITIZE_STRIPPED, 
										 // country id...
										'country-code'	=>	array('filter'		=>	FILTER_VALIDATE_REGEXP, 
														'options'	=>	array('regexp' => '/^[A-Za-z]{2}$/')
																), 
										 // zone_id
										'zone-id'	=>	FILTER_VALIDATE_INT, 
                                                                    
										 // provinces
										'provinces'	=>	array('filter'		=>	FILTER_VALIDATE_INT, 
														'flags'	=>	FILTER_REQUIRE_ARRAY
																), 
                                                                                // add OR remove...
                                                                                'do'	=>	array('filter'		=>	FILTER_VALIDATE_REGEXP, 
                                                                                                               'options'	=>	array('regexp' => '/^(add|remove)$/')
                                                                                                                               )

										)
								);

// Quick fix to avoid triggering white list error..
if ($_CLEAN['provinces'] === NULL) $_CLEAN['provinces'] = array();



// Check if INPUT keys are ALL expected AND MADE IT THROUGH...
if (anySuspects()) _catch_error('Error processing white_list!', __LINE__, true);

// Load country
$country = Cataleya\Geo\Country::load($_CLEAN['country-code']);
if ($country === NULL) _catch_error('Country could not be loaded.', __LINE__, true);




// Load Zone
$zone = Cataleya\Geo\Zone::load($_CLEAN['zone-id']);
if ($zone === NULL) _catch_error('Zone could not be loaded.', __LINE__, true);



// retrieve info about provinces (will remain empty if we don't process any provinces)
$provinces_info = array ();




// ADD TO ZONE ??

if ($_CLEAN['do'] === 'add') 
{
    // Add Country (this is in case we just want to add a country without provinces
    $zone->addCountry($country);

    // Process provinces
    if (isset($_CLEAN['provinces']) && is_array($_CLEAN['provinces'])) 
    {
        foreach ($_CLEAN['provinces'] as $key=>$val) {
            $_id = filter_var($val, FILTER_VALIDATE_INT);

            if ($_id === FALSE) _catch_error('Invalid parameter.', __LINE__, true); // bail if any invalid param.

            $_CLEAN['provinces'][$key] = (int)$_id;
        }
    } 



    foreach ($country as $province) 
    {
        if (in_array((int)$province->getID(), $_CLEAN['provinces'])) 
        {
            $zone->addProvince ($province);

            // collect info about province...
            $provinces_info[] = array (
                'id'    =>  $province->getID(), 
                'iso'   =>  $province->getIsoCode(), 
                'name'  =>  $province->getName(), 
                'printableName' => $province->getPrintableName()
            );
        }
        else $zone->removeProvince ($province);
    }


}



// REMOVE FROM ZONE

elseif ($_CLEAN['do'] === 'remove') {
    
    // Remove country from zone
    $zone->removeCountry($country);
    
}


// retrieve info about country
$country_info = array (
        'iso'   =>  $country->getCountryCode(), 
        'name'  =>  $country->getPrintableName()
    );





// OUTPUT...
$json_reply = array (
		"status"=>'ok', 
		"message"=> 'Zone updated.', 
                "country"=>$country_info, 
                "provinces"=>$provinces_info, 
                "action"=>$_CLEAN['do']
		);

echo (json_encode($json_reply));
exit();



?>