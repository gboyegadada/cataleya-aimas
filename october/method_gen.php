<?php


define ('OUTPUT', 'TEXT'); // Whether to output as JSON or HTML (especially errors)...


// LOAD APPLICATION TOP...
require_once ('app_top.php');



// Get database handle...
$dbh = Cataleya\Helper\DBH::getInstance();



/*
 * LOAD DATA: CUSTOMERS
 */


$table_names = array ( 
                    'address_book'
                    /*
                    'products', 
                    'languages', 
                    'locations',
                    'tags', 
                    'currencies'
                     * 
                     */
);


$filter_constants = array (
    'int'   =>  'FILTER_VALIDATE_INT, FILTER_NULL_ON_FAILURE',
    'tinyint'   =>  'FILTER_VALIDATE_BOOLEAN, FILTER_NULL_ON_FAILURE', 
    'bigint'    =>  'FILTER_VALIDATE_INT, FILTER_NULL_ON_FAILURE', 
    'smallint'  =>  'FILTER_VALIDATE_INT, FILTER_NULL_ON_FAILURE', 
    'decimal'   =>  'FILTER_VALIDATE_FLOAT, FILTER_NULL_ON_FAILURE'
);

foreach ($table_names as $table)
{
  
        $select_handle = $dbh->prepare('SELECT column_name, data_type FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_SCHEMA = "cataleya" AND table_name = "' . $table . '"');
        if (!$select_handle->execute())    _catch_error('Error: [' . implode(', ', $select_handle->errorInfo()) . ']', __LINE__, TRUE);

        $_data = $select_handle->fetchAll(PDO::FETCH_ASSOC);
        
echo "\t" . strtoupper($table) . "\n";       
echo '/--------------------------------------------------/' . "\n\n";
            
        foreach ($_data as $row)
        {
            
            $filter_func_def = (array_key_exists($row['data_type'], $filter_constants)) 
                    ? '$value = filter_var($value, ' .$filter_constants[$row['data_type']] . ');' 
                    : '';
            
            // echo "/\n \n [ data type: " . $row['data_type'] . " ] \n\n";
            
            $bool_val_def = ($row['data_type'] === 'tinyint') ? 'return ((int)$this->_data[\'' . $row['column_name'] . '\'] === 1);' : 'return $this->_data[\'' . $row['column_name'] . '\'];';
            
            
            $method_name = preg_replace('/(^|_){1}([A-Za-z]{1})/e', 'strtoupper("$2")', $row['column_name']);

            echo "/*\n *\n * [ get" . $method_name . " ] \n *_____________________________________________________\n *\n *\n */\n\n";

            echo 'public function get' . $method_name . '()' . "\n";
            echo  "{\n";
            echo "\t" . $bool_val_def . "\n";
            echo '}' . "\n\n\n\n";


            echo "/*\n *\n * [ set" . $method_name . " ] \n *_____________________________________________________\n *\n *\n */\n\n";
            echo 'public function set' . $method_name . '($value = NULL)' . "\n";
            echo  "{\n";
            echo "\t" .  $filter_func_def . "\n\n";
            echo "\t" . 'if ($value===NULL) $this->e->triggerException(\'Error in class (\' . __CLASS__ . \'): [ invalid argument for method: \' . __FUNCTION__ . \' ] on line \' . __LINE__);' . "\n\n";
            echo "\t" . '$this->_data[\'' . $row['column_name'] . '\'] = $value;' . "\n";
            echo "\t" . '$this->_modified[] = \'' . $row['column_name'] . '\';' . "\n\n";
            echo "\t" . 'return $this;' . "\n";
            echo '}' . "\n\n\n\n";

        }
        
        
echo '/--------------------------------------------------/' . "\n\n\n\n";

}

?>
