<?php
/*
FETCH PAYMENT TYPES AND RETURN AS JSON...
*/




define ('OUTPUT', 'JSON'); // Whether to output as JSON or HTML (especially errors)...


// LOAD APPLICATION TOP...
require_once ('app_top.php');






// DIE IF INPUT IS NOT VIA POST METHOD...
if ($_SERVER['REQUEST_METHOD'] != 'POST') _catch_error('POST METHOD ONLY PLEASE. NOW GET.');
	



// FILTER VALUES ....
$_CLEAN = filter_input_array(INPUT_POST, 
								array(	
                                                                                // auth_token
                                                                                'auth_token'    =>  FILTER_SANITIZE_STRIPPED

										)
								);


$NotificationProfiles = Cataleya\Admin\Notifications::load();
if ($NotificationProfiles === NULL) _catch_error('Notifications could not be loaded.', __LINE__, true);


$notification_profiles_info = array ();

foreach ($NotificationProfiles as $Notification) 
{
    $admin_profiles_info = array();
    
    foreach ($Notification as $Alert) 
    {
        $AdminUser = $Alert->getRecipient();

        $admin_profiles_info[] = array (
            'id'    =>  $AdminUser->getAdminId(), 
            'name'  =>  $AdminUser->getName(), 
            'firstname'  =>  $AdminUser->getFirstname(), 
            'lastname'  =>  $AdminUser->getLastname(), 
            'emailEnabled'  =>  $Alert->emailEnabled(),  
            'dashboardEnabled'  =>  $Alert->dashboardEnabled()
        );
        
        
    }

    
    $notification_profiles_info[] = array (
        'id'    =>  $Notification->getID(), 
        'keyword'    =>  $Notification->getKeyword(), 
        'title'  =>  $Notification->getDescription()->getTitle('EN'), 
        'description'  =>  $Notification->getDescription()->getText('EN'), 
        'entityName'  =>  $Notification->getEntityName(), 
        'entityNamePlural'  =>  $Notification->getEntityNamePlural(), 
        'notificationText'  =>  $Notification->getNotificationText(), 
        'sendAfterPeriod'  =>  $Notification->getSendAfterPeriod(), 
        'sendAfterInterval'  =>  $Notification->getSendAfterInterval(), 
        'sendAfterIncrements'  =>  $Notification->getSendAfterIncrements(),  
        'incrementsSinceLastSent'  =>  $Notification->getIncrementsSinceLastSent(), 
        'url'  =>  $Notification->getRequestUri(),
        'icon'  =>  $Notification->getIcon(), 
        'recipients'    =>   $admin_profiles_info, 
        'count' => count($admin_profiles_info) // $Notification->getPopulation()
    );
    
    
}



// OUTPUT...
$json_reply = array (
		"status"=>'ok', 
		"message"=> Cataleya\Helper::countInEnglish($NotificationProfiles->getPopulation(), 'notification profile', 'notification profiles') . ' found.', 
                "NotificationProfiles"=>$notification_profiles_info, 
                "count"=>$NotificationProfiles->getPopulation()
		);

echo (json_encode($json_reply));
exit();



?>