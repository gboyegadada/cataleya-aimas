<?php



define ('OUTPUT', 'JSON'); // Whether to output as JSON or HTML (especially errors)...


// LOAD APPLICATION TOP...
require_once ('app_top.php');






// DIE IF INPUT IS NOT VIA POST METHOD...
if ($_SERVER['REQUEST_METHOD'] != 'POST') _catch_error('POST METHOD ONLY PLEASE. NOW GET.');
	

$WHITE_LIST = array(
						'auth_token',  
                                                'store_id', 
                                                'carrier_id', 
						'is_taxable', 
						'rate', 
						'rate_type', 
                                                'max_delivery_days', 
                                                'shipping_option_name',  
                                                'description',
                                                'shipping_option_id', 
                                                'zone_id', 
    
    
                                                // EXPECTECTED VALUE DATA TYPES (only necessary for boolean values which can confuse the func.
                                                // Note: Make values same 'data type' as expected value.
                                                
                                                'is_taxable'   =>  TRUE
					);

// FILTER VALUES ....
$_CLEAN = filter_input_array(INPUT_POST, 
								array(	
                                                                                // auth_token
                                                                                'auth_token'    =>  FILTER_SANITIZE_STRIPPED, 
                                                                    
										 // store id...
										'store_id'	=>	FILTER_VALIDATE_INT, 
                                                                    
										 // tax class id...
										'is_taxable'	=>	FILTER_VALIDATE_BOOLEAN, 
                                                                    
										 // rate...
										'rate'	=>	FILTER_VALIDATE_FLOAT, 
                                                                    
                                                                                // rate type...
										'rate_type'	=>	array('filter'		=>	FILTER_VALIDATE_REGEXP, 
														'options'	=>	array('regexp' => '/^(weight|unit|flat|volume)$/')
																), 
                                                                    
										 // shipping_option label...
										'shipping_option_name'	=>	array('filter'		=>	FILTER_VALIDATE_REGEXP, 
														'options'	=>	array('regexp' => '/^[A-Za-z0-9,\-\W]{2,400}$/')
																), 
                                                                    
										 // shipping-option description...
										'description'	=>	array('filter'		=>	FILTER_VALIDATE_REGEXP, 
														'options'	=>	array('regexp' => '/^[A-Za-z0-9,\-\W]{2,1000}$/')
																),  
                                                                    
										 // shipping-option id (>0) for save, (0) for create...
										'shipping_option_id'	=>	FILTER_VALIDATE_INT, 
                                                                    
										 // carrier id
										'carrier_id'	=>	FILTER_VALIDATE_INT, 
                                                                    
										 // zone id 
										'zone_id'	=>	FILTER_VALIDATE_INT, 
                                                                                
                                                                                // max delivery days
                                                                                'max_delivery_days' =>  array('filter'		=>	FILTER_VALIDATE_INT, 
																'options'	=>	array('min_range' => 1, 'max_range' => 360)
																)

										)
								);



// Check if INPUT keys are ALL expected AND MADE IT THROUGH...
$_suspect = anySuspects();
if ($_suspect !== FALSE || Cataleya\Helper\Validator::bool($_POST['is_taxable']) === NULL) _catch_error('Error processing white_list!'.$_suspect, __LINE__, true);



//////////////////// LOAD DEPENDENCIES ///////////////////////

// 1. Store
$Store = Cataleya\Store::load($_CLEAN['store_id']);
if ($Store === NULL) _catch_error('Store could not be loaded.', __LINE__, true);


// 2. Carrier
$Carrier = Cataleya\Agent::load($_CLEAN['carrier_id']);
if ($Carrier === NULL) _catch_error('Carrier could not be loaded.', __LINE__, true);


// 3. Zone
$Zone = Cataleya\Geo\Zone::load($_CLEAN['zone_id']);
if ($Zone === NULL) _catch_error('Zone could not be loaded.', __LINE__, true);


// Format label...
$_label = ucfirst(strtolower($_CLEAN['shipping_option_name']));



// New shipping option ?
if ((int)$_CLEAN['shipping_option_id'] === 0) 
{
   $Description = Cataleya\Asset\Description::create($_label, $_CLEAN['description'], 'EN');
   
   $ShippingOption = Cataleya\Shipping\ShippingOption::create($Store, $Carrier, $Zone, $Description);
   if ($ShippingOption === NULL) _catch_error('New shipping option could not be added.', __LINE__, true);
   
   // Set Delivery Days
   $ShippingOption->setMaxDeliveryDays($_CLEAN['max_delivery_days']);   

    // Set Rate Type (rate per: weight, unit, volume, flat)
   $ShippingOption->setRateType($_CLEAN['rate_type']);
   
    // Set Rate 
   $ShippingOption->setRate($_CLEAN['rate']);
   
}

// Existing shipping option ?
else if ((int)$_CLEAN['shipping_option_id'] > 0) 
{
   $ShippingOption = Cataleya\Shipping\ShippingOption::load($_CLEAN['shipping_option_id']);
   if ($ShippingOption === NULL) _catch_error('Shipping option could not be loaded.', __LINE__, true);
   
   // 1. Carrier
   $ShippingOption->setCarrier($Carrier);
   
   // 2. Shipping Zone
   $ShippingOption->setShippingZone($Zone);
   
   // 3. Tax
   if ($_CLEAN['is_taxable'] === TRUE) $ShippingOption->makeTaxable();
   else $ShippingOption->makeNonTaxable ();
   
   // 4. Max Delivery Days
   $ShippingOption->setMaxDeliveryDays($_CLEAN['max_delivery_days']);
   
   // 5. Rate Type (rate per: weight, unit, volume, flat)
   $ShippingOption->setRateType($_CLEAN['rate_type']);
   
   // 6. Description
   $ShippingOption
           ->getDescription()
           ->setTitle($_label, 'EN')
           ->setText($_CLEAN['description'], 'EN');
   
   // 7. Set Rate 
   $ShippingOption->setRate($_CLEAN['rate']);
   
   
}





// Load shipping options
$ShippingOptions = Cataleya\Shipping\ShippingOptions::load($Store);

// retrieve info about store (and it's shipping options
$store_info = array (
        'id'    =>  $Store->getID(), 
        'name'  => $Store->getDescription()->getTitle('EN'), 
        'description'  =>  $Store->getDescription()->getText('EN'),
        'population' => $ShippingOptions->getPopulation(), 
        'populationAsText' => count_in_english($ShippingOptions->getPopulation(), 'Shipping option', 'Shipping options')
    );

// retrieve info about shipping carrier

$carrier_info = array (
        'id'    =>  $Carrier->getID(), 
        'name'  =>  $Carrier->getCompanyName() . ' (' . $Carrier->getContactFirstName() . ')'
    );



// retrieve info about zone

$zone_info = array (
        'id'    =>  $Zone->getID(), 
        'name'  =>  $Zone->getZoneName(), 
        'listType'  =>  $Zone->getListType(),
        'countryPopulation' => $Zone->getCountryPopulation(), 
        'provincePopulation' => $Zone->getProvincePopulation()
    );





$shipping_option_info = array (
    'id'   =>  $ShippingOption->getID(), 
    'name'  =>  $ShippingOption->getDescription()->getTitle('EN'), 
    'description'  =>  $ShippingOption->getDescription()->getText('EN'),  
    'deliveryDays'  =>  $ShippingOption->getMaxDeliveryDays(),  
    'rateType'  =>  $ShippingOption->getRateType(), 
    'rate'  =>  $ShippingOption->getRate($Store->getID()), 
    'Zone'  =>  $zone_info, 
    'Carrier'   =>  $carrier_info, 
    'isTaxable'  =>  $ShippingOption->isTaxable()
);




// OUTPUT...
$json_reply = array (
		"status"=>'ok', 
		"message"=> ((int)$_CLEAN['shipping_option_id'] === 0) ? 'New tax rate added.' : 'Tax rate saved.', 
                "ShippingOption"=>$shipping_option_info, 
                "Store"=>$store_info
		);

echo (json_encode($json_reply));
exit();



?>
