<?php
/*
FETCH CATEGORIES AND RETURN AS JSON...
*/


define ('OUTPUT', 'JSON'); // Whether to output as JSON or HTML (especially errors)...


// LOAD APPLICATION TOP...
require_once ('app_top.php');

/*
 * 
 * CHECK USER PRIVILEGES
 * 
 */


if ((!isset($_admin_role) || !$_admin_role->hasPrivilege('VIEW_CATALOG')) && !IS_SUPER_USER) forbidden();



$WHITE_LIST = array(
						'term'
					);



// RETRIEVE DATA ONLY IF VIA POST METHOD...
if ($_SERVER['REQUEST_METHOD'] != 'GET') _catch_error('GET METHOD ONLY PLEASE. NOW GET.', __LINE__, true);
	



// SANITIZE INPUT DATA...
$_CLEAN = filter_input_array(INPUT_GET, 
								array(
										'term'		=>	FILTER_SANITIZE_STRING

										)
								);




// Check if INPUT keys are ALL expected AND MADE IT THROUGH...
if (anySuspects()) 
{


    // Output...
    $json_data = array (
                    'status' => 'Ok', 
                    'message' => 'No products found.', 
                    'Products' => array(), 
                    'count' => 0
                    );
    echo json_encode($json_data);
    exit();

    
}






// Words to ignore in search ...

$_IGNORE = array (
    'the'
);

$_CLEAN['term'] = preg_replace('/^(' . implode('|', $_IGNORE) . ')$/', '', $_CLEAN['term']);

foreach ($_IGNORE as $k=>$word) $_IGNORE[$k] .= '\s';
$_CLEAN['term'] = preg_replace('/(' . implode('|', $_IGNORE) . ')/', '', $_CLEAN['term']);
    
    


// First, take out funny looking characters...
$_CLEAN['term'] = preg_replace('/[^-\s\.A-Za-z0-9@*#+]+/', '', $_CLEAN['term']);




// Load search results as a collection of objects...
$search_results = Cataleya\Catalog::search($_CLEAN['term'], array('show_hidden'=>TRUE));


$_item_num = $search_results->getPageStart();
$Stores = Cataleya\Stores::load();
$products_info = array ();

foreach ($search_results as $Product) 
{
    

    $_stock_low = FALSE;
    $_out = FALSE;

   foreach ($Product->getOptions() as $Option) 
   {

       $_stock = $Option->getStock();
       foreach ($Stores as $Store) 
       {
           if ((int)$_stock->getValue($Store) < 5) $_stock_low = TRUE; 
           if ((int)$_stock->getValue($Store) == 0) $_out = TRUE; 
       }

       if ($_stock_low || $_out) break;
   }

    

    $products_info[] = Cataleya\Front\Dashboard\Juicer::juice($Product);
}



// Output...
$json_data = array (
		'status' => 'Ok', 
		'message' => Cataleya\Helper::countInEnglish($search_results->getPopulation(), 'Product', 'Products') . ' found.', 
		'Products' => $products_info, 
		'count' => $search_results->getPopulation()
		);
echo json_encode($json_data);
exit();



