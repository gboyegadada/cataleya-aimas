<?php
/*
FETCH CATEGORIES AND RETURN AS JSON...
*/


define ('OUTPUT', 'JSON'); // Whether to output as JSON or HTML (especially errors)...


// LOAD APPLICATION TOP...
require_once ('app_top.php');

/*
 * 
 * CHECK USER PRIVILEGES
 * 
 */


if ((!isset($_admin_role) || !$_admin_role->hasPrivilege('VIEW_STORES')) && !IS_SUPER_USER) forbidden();



$WHITE_LIST = array(
						'auth_token', 
						'term'
					);



// RETRIEVE DATA ONLY IF VIA POST METHOD...
if ($_SERVER['REQUEST_METHOD'] != 'POST') _catch_error('POST METHOD ONLY PLEASE. NOW GET.', __LINE__, true);
	



// SANITIZE INPUT DATA...
$_CLEAN = filter_input_array(INPUT_POST, 
								array(
										'auth_token'		=>	FILTER_SANITIZE_STRIPPED, 
										'term'		=>	FILTER_SANITIZE_STRING

										)
								);




// Check if INPUT keys are ALL expected AND MADE IT THROUGH...
if (anySuspects()) 
{


    // Output...
    $json_data = array (
                    'status' => 'Ok', 
                    'message' => 'No stores found.', 
                    'Stores' => array(), 
                    'count' => 0
                    );
    echo json_encode($json_data);
    exit();

    
}







// First, take out funny looking characters...
$_CLEAN['term'] = preg_replace('/[^-\s\.A-Za-z0-9@*#+]+/', '', $_CLEAN['term']);




// Load search results as a collection of objects...
$search_results = Cataleya\Stores::search($_CLEAN['term']);



$stores_info = array ();

foreach ($search_results as $Store) 
{
    
    $Location = $Store->getDefaultLocation();
    // if ($Location->hasPrivilege('SUPER_USER')) continue;
    
    $location_info = array (
        'id'    =>   $Location->getID(), 
        'country'  =>   $Location->getCountry()->getPrintableName(), 
        'country_code'  =>   $Location->getCountry()->getIso2()
    );
    
    $stores_info[] = array (
        'id'    =>  $Store->getStoreId(), 
        'name'  =>  $Store->getDescription()->getTitle('EN'), 
        'contactName'  =>  $Store->getContactName(), 
        'email'  =>  $Store->getEmail(), 
        'phone'  =>  $Store->getTelephone(), 
        'isActive'   =>  $Store->isActive(), 
        'url'   =>  BASE_URL.'store_edit.stores.php?d=edit&s='.$Store->getStoreId(),
        'location'  =>  $location_info
    );
}



// Output...
$json_data = array (
		'status' => 'Ok', 
		'message' => Cataleya\Helper::countInEnglish($search_results->getPopulation(), 'Store', 'Stores') . ' found.', 
		'Stores' => $stores_info, 
		'count' => $search_results->getPopulation()
		);
echo json_encode($json_data);
exit();




?>