<?php



define ('OUTPUT', 'JSON'); // Whether to output as JSON or HTML (especially errors)...


// LOAD APPLICATION TOP...
require_once ('app_top.php');




$WHITE_LIST = array(
						'auth_token', 
                                                'target_id', 
                                                'csv_file', 
                                                'do', 
                                                'data'
						);





// DIE IF INPUT IS NOT VIA POST METHOD...
if ($_SERVER['REQUEST_METHOD'] != 'POST') _catch_error('POST METHOD ONLY PLEASE. NOW GET.', __LINE__, true);

// SANITIZE INPUT DATA...
$_CLEAN = filter_input_array(INPUT_POST, array(
                                                    'auth_token'		=>	FILTER_SANITIZE_STRIPPED,
                                                    'target_id' =>  FILTER_VALIDATE_INT, 
                                                    'data'	=>	array( 'flags' => FILTER_REQUIRE_ARRAY ),
                                                    'do'	=>	array('filter'		=>	FILTER_VALIDATE_REGEXP, 
                                                                                                'options'	=>	array('regexp' => '/^[A-Za-z ]{1,50}$/')
                                                                                                                ), 

                                                    )
										
					);


// Check if INPUT keys are ALL expected AND MADE IT THROUGH...
$_suspect = anySuspects();
if ($_suspect !== FALSE) _catch_error('Error processing white_list!' . print_r($_suspect, true), __LINE__, true);




// VALIDATE AUTH TOKEN...
validate_auth_token ();

// Validate task

$_FUNCS = array (
    'void' => array (), 
    'importCSV' => array (
        'store_id' => 'int', 
        'file_type' => 'int'
    ), 
    'setTaxClass' =>  array (
        'product_id' => 'integer_array'
    ), 
    'showProduct' =>  array (
        'product_id' => 'integer_array'
    ), 
    'hideProduct' =>  array (
        'product_id' => 'integer_array'
    ), 
    'addToCategory' => array (
        'product_id' => 'integer_array'
    ), 
    'removeFromCategory' => array (
        'product_id' => 'integer_array'
    ), 
    'addToStore' => array (
        'product_id' => 'integer_array'
    ), 
    'removeFromStore' => array (
        'product_id' => 'integer_array'
    ), 
    'feature' => array (
        'product_id' => 'integer_array'
    ), 
    'unFeature' => array (
        'product_id' => 'integer_array'
    ), 
    'addToSale' => array (
        'product_id' => 'integer_array'
    ), 
    'removeFromSale' => array (
        'product_id' => 'integer_array'
    ), 
    'applyCoupon' => array (
        'product_id' => 'integer_array'
    ), 
    'removeCoupon' => array (
        'product_id' => 'integer_array'
    ), 
    'newProduct' => array (), 
    'deleteProduct' => array (
        'product_id' => 'integer_array'
        )
    
);




if (!array_key_exists($_CLEAN['do'], $_FUNCS) || !is_callable($_CLEAN['do'])) _catch_error('Invalid action.', __LINE__, true);



define('DASH_LANGUAGE_CODE', Cataleya\System::load()->getDashboardLanguageCode());
define('SHOPFRONT_LANGUAGE_CODE', Cataleya\System::load()->getShopFrontLanguageCode());
     




    
// OUTPUT...
 $json_reply = array (
                 "status" => 'ok', 
                 "message" => 'Changes saved.'
                 );


 
 
/*
// EXECUTE API CALL
foreach ($_CLEAN['product_id'] as $_id) 
{
    if (!in_array($_CLEAN['do'], ['newProduct']) )
            
    {
        // Load product
        $Product = Cataleya\Catalog\Product::load((int)$_id);

        // Check if product exists...
        if ($Product === NULL) _catch_error('One or more products not found.', __LINE__, true);
    
    }

    // API FUNC CALL
    $_CLEAN['do']();
}

*/




// EXECUTE API CALL

// API FUNC CALL
$_CLEAN['do']();



// HELPER FUNCS



function digestParams () 
{
    global $_CLEAN, $_FUNCS;
    $_params = Cataleya\Helper\Validator::params($_FUNCS[$_CLEAN['do']], $_CLEAN['data']);
    $_bad_params = array ();
    

    if (!isset($_params['language_code'])) 
    {
        $_params['language_code'] = DASH_LANGUAGE_CODE;

    }
    
    
    
    
    
    // Check if every one made it through
    
    foreach ($_params as $k=>$v) {
        if ($v === NULL) $_bad_params[] = $k;
    }
    

    if (!empty($_bad_params)) _catch_error('Unable to save.' . implode(',', $_bad_params), __LINE__, true);
    
    return $_params;
}




function juiceDropCatalog () 
{
    
    $_info = array (
        array (
            'id'    =>  'all', 
            'population'    => Cataleya\System::load()->getProductCount(), 
            'isActive'  =>  TRUE // $_Category->isActive()
        )
    );
    
    
    foreach (Cataleya\Catalog\Tags::load(array('show_hidden'=>TRUE)) as $_Category) $_info[] = Cataleya\Front\Dashboard\Juicer::juiceDrop($_Category);
    
    return $_info;
    
    
}







/*
 * 
 * 
 * CALLBACKS
 * 
 */


function void () {
    
}









/**
 * 
 * [ importCSV ]
 * ______________________________________________________
 * 
 * @param int $_POST['target_id']
 * @param int $_POST['data']
 * 
 */
function importCSV () {

    global $json_reply;
    $_params = digestParams();
    
    

    /* ------------------- 1: Handle Upload. --------------------------------------- */

    $_DateTime = new DateTime ();

    $src = $_FILES['csv_file']['tmp_name'];

    if (empty($src) || !file_exists($src) || !is_uploaded_file($src)) _catch_error('Import failed.', __LINE__, true);


    $file_name = 'import_' . $_DateTime->format('_d-m-Y_h.i.s') . '.csv';
    define('CSV_PATH', ROOT_PATH . 'var/imports/' . $file_name);


    if(!move_uploaded_file($src, CSV_PATH))_catch_error('Import failed.', __LINE__, true);

    /* ---------------------------------------------------------------------------- */

    
    
    
    $_Store = Cataleya\Store::load($_params['store_id']);
    if ($_Store === NULL ) _catch_error('Store could not be found.', __LINE__, true);
    
    $_Importer = new Cataleya\Helper\Importer($_Store, $_params['file_type'], CSV_PATH);
    
    $_Importer->doImport();
    
    $json_reply['message'] = 'CSV has been imported.';

}










function setTaxClass () {
    global $_CLEAN;
    $_params = digestParams();
    
    static $TaxClass = NULL;
    if (empty($TaxClass) && (int)$_CLEAN['target_id'] > 0 ) 
    {
        $TaxClass = Cataleya\Tax\TaxRule::load($_CLEAN['target_id']);
        if ($TaxClass === NULL ) _catch_error('The selected tax class could not be found.', __LINE__, true);
    }
    
    
    
    if ((int)$_CLEAN['target_id'] > 0 && $TaxClass !== NULL): 
        
            foreach ($_params['product_id'] as $_id) {

               // Load product
               $Product = Cataleya\Catalog\Product::load((int)$_id);
               if ($Product === NULL) _catch_error('One or more products not found.', __LINE__, true);

                $Product->setTaxClass($TaxClass);
            }
    
    else:
            
            foreach ($_params['product_id'] as $_id) {

               // Load product
               $Product = Cataleya\Catalog\Product::load((int)$_id);
               if ($Product === NULL) _catch_error('One or more products not found.', __LINE__, true);

                $Product->unsetTaxClass();
            }
            
        
    endif;
}



function showProduct () {
    
        $_params = digestParams();
        global $json_reply;
        
        $json_reply['affected'] = array ();
    
        foreach ($_params['product_id'] as $_id) {

           // Load product
           $Product = Cataleya\Catalog\Product::load((int)$_id);
           if ($Product === NULL) _catch_error('One or more products not found.', __LINE__, true);

            $Product->show();
            $json_reply['affected'][] = $Product->getID();
        }
        

}


function hideProduct () {
    
        $_params = digestParams();
        global $json_reply;
        
        $json_reply['affected'] = array ();
        
        foreach ($_params['product_id'] as $_id) {

           // Load product
           $Product = Cataleya\Catalog\Product::load((int)$_id);
           if ($Product === NULL) _catch_error('One or more products not found.', __LINE__, true);

            $Product->hide();
            $json_reply['affected'][] = $Product->getID();
        }
        

}






function feature () {
    
        $_params = digestParams();
        global $json_reply;
        
        $json_reply['affected'] = array ();
    
        foreach ($_params['product_id'] as $_id) {

           // Load product
           $Product = Cataleya\Catalog\Product::load((int)$_id);
           if ($Product === NULL) _catch_error('One or more products not found.', __LINE__, true);

            $Product->feature();
            $json_reply['affected'][] = $Product->getID();
        }
        

}


function unFeature () {
    
        $_params = digestParams();
        global $json_reply;
        
        $json_reply['affected'] = array ();
        
        foreach ($_params['product_id'] as $_id) {

           // Load product
           $Product = Cataleya\Catalog\Product::load((int)$_id);
           if ($Product === NULL) _catch_error('One or more products not found.', __LINE__, true);

            $Product->unFeature();
            $json_reply['affected'][] = $Product->getID();
        }
        

}







   

function addToCategory () {
    global $_CLEAN, $json_reply;
    $_params = digestParams();
    
    static $Tag = NULL;
    if (empty($Tag) && (int)$_CLEAN['target_id'] > 0 ) $Tag = Cataleya\Catalog\Tag::load($_CLEAN['target_id'], array(), array('show_hidden'=>TRUE));
    
    if ($Tag === NULL) _catch_error('The selected category could not be found.', __LINE__, true);
    
    foreach ($_params['product_id'] as $_id) {

       // Load product
       $Product = Cataleya\Catalog\Product::load((int)$_id);
       if ($Product === NULL) _catch_error('One or more products not found.', __LINE__, true);

       $Tag->addProduct($Product);
    }
    
    
    $json_reply['Category'] = Cataleya\Front\Dashboard\Juicer::juice($Tag);
    $json_reply['ProductOptions'] = array ();
    
    if (count($_params['product_id']) === 1) {
        $Product = Cataleya\Catalog\Product::load((int)$_params['product_id'][0]);
        
        foreach ($Product as $_Option) {
            $json_reply['ProductOptions'][] = Cataleya\Front\Dashboard\Juicer::juice($_Option);
        }
    
    }
    
}


function removeFromCategory () {
    global $_CLEAN, $json_reply;
    $_params = digestParams();
    
    static $Tag = NULL;
    if (empty($Tag) && (int)$_CLEAN['target_id'] > 0 ) $Tag = Cataleya\Catalog\Tag::load($_CLEAN['target_id'], array(), array('show_hidden'=>TRUE));
    
    if ($Tag === NULL) _catch_error('The selected category could not be found.', __LINE__, true);
    
    foreach ($_params['product_id'] as $_id) {

       // Load product
       $Product = Cataleya\Catalog\Product::load((int)$_id);
       if ($Product === NULL) _catch_error('One or more products not found.', __LINE__, true);

       $Tag->removeProduct($Product);
    }
    
    
    $json_reply['Category'] = Cataleya\Front\Dashboard\Juicer::juice($Tag);
    $json_reply['ProductOptions'] = array ();
    
    if (count($_params['product_id']) === 1) {
        $Product = Cataleya\Catalog\Product::load((int)$_params['product_id'][0]);
        
        foreach ($Product as $_Option) {
            $json_reply['ProductOptions'][] = Cataleya\Front\Dashboard\Juicer::juice($_Option);
        }
    
    }
}





function addToStore () {
    global $_CLEAN, $json_reply;
    $_params = digestParams();
    
    static $Store = NULL;
    if (empty($Store) && (int)$_CLEAN['target_id'] > 0 ) $Store = Cataleya\Store::load($_CLEAN['target_id'], array(), array('show_hidden'=>TRUE));
    
    if ($Store === NULL) _catch_error('The selected store could not be found.', __LINE__, true);
    
    
    $Tag = $Store->getRootCategory();
    
    foreach ($_params['product_id'] as $_id) {

       // Load product
       $Product = Cataleya\Catalog\Product::load((int)$_id);
       if ($Product === NULL) _catch_error('One or more products not found.', __LINE__, true);

       $Tag->addProduct($Product);
    }
    
    $json_reply['Store'] = Cataleya\Front\Dashboard\Juicer::juiceDrop($Store);
    $json_reply['ProductOptions'] = array ();
    
    if (count($_params['product_id']) === 1) {
        $Product = Cataleya\Catalog\Product::load((int)$_params['product_id'][0]);
        
        foreach ($Product as $_Option) {
            $json_reply['ProductOptions'][] = Cataleya\Front\Dashboard\Juicer::juice($_Option);
        }
    
    }
}


function removeFromStore () {
    global $_CLEAN, $json_reply;
    $_params = digestParams();
    
    static $Store = NULL;
    if (empty($Store) && (int)$_CLEAN['target_id'] > 0 ) $Store = Cataleya\Store::load($_CLEAN['target_id'], array(), array('show_hidden'=>TRUE));
    
    if ($Store === NULL) _catch_error('The selected store could not be found.', __LINE__, true);
    
    
    $_StoreCatalog = Cataleya\Catalog::load(array('Store'=>$Store), array('show_hidden'=>TRUE));
        
    foreach ($_params['product_id'] as $_id) {

       // Load product
       $Product = Cataleya\Catalog\Product::load((int)$_id);
       if ($Product === NULL) _catch_error('One or more products not found.', __LINE__, true);

       $_StoreCatalog->removeProduct($Product);
    } 
    
    
    
    $json_reply['Store'] = Cataleya\Front\Dashboard\Juicer::juiceDrop($Store);
    $json_reply['ProductOptions'] = array ();
    
    if (count($_params['product_id']) === 1) {
        $Product = Cataleya\Catalog\Product::load((int)$_params['product_id'][0]);
        
        foreach ($Product as $_Option) {
            $json_reply['ProductOptions'][] = Cataleya\Front\Dashboard\Juicer::juice($_Option);
        }
    
    }
}






function addToSale () {
    global $_CLEAN, $json_reply;
    $_params = digestParams();
    
    static $Sale = NULL;
    if (empty($Sale) && (int)$_CLEAN['target_id'] > 0 ) $Sale = Cataleya\Sales\Sale::load($_CLEAN['target_id']);
    
    if ($Sale === NULL) _catch_error('The selected category could not be found.', __LINE__, true);
    
    foreach ($_params['product_id'] as $_id) {

       // Load product
       $Product = Cataleya\Catalog\Product::load((int)$_id);
       if ($Product === NULL) _catch_error('One or more products not found.', __LINE__, true);

       $Sale->addProduct($Product);
    }
    
    
    $json_reply['Sale'] = Cataleya\Front\Dashboard\Juicer::juice($Sale);
}


function removeFromSale () {
    global $_CLEAN, $json_reply;
    $_params = digestParams();
    
    static $Sale = NULL;
    if (empty($Sale) && (int)$_CLEAN['target_id'] > 0 ) $Sale = Cataleya\Sales\Sale::load($_CLEAN['target_id']);
    
    if ($Sale === NULL) _catch_error('The selected category could not be found.', __LINE__, true);
    
    foreach ($_params['product_id'] as $_id) {

       // Load product
       $Product = Cataleya\Catalog\Product::load((int)$_id);
       if ($Product === NULL) _catch_error('One or more products not found.', __LINE__, true);

       $Sale->removeProduct($Product);
    }
    
    
    $json_reply['Sale'] = Cataleya\Front\Dashboard\Juicer::juice($Sale);
}






function applyCoupon () {
    global $_CLEAN, $json_reply;
    $_params = digestParams();
    
    static $Coupon = NULL;
    if (empty($Coupon) && (int)$_CLEAN['target_id'] > 0 ) $Coupon = Cataleya\Sales\Coupon::load($_CLEAN['target_id']);
    
    if ($Coupon === NULL) _catch_error('The selected category could not be found.', __LINE__, true);
    
    foreach ($_params['product_id'] as $_id) {

       // Load product
       $Product = Cataleya\Catalog\Product::load((int)$_id);
       if ($Product === NULL) _catch_error('One or more products not found.', __LINE__, true);

       $Coupon->addProduct($Product);
    }
    
    
    
    $json_reply['Coupon'] = Cataleya\Front\Dashboard\Juicer::juice($Coupon);
}


function removeCoupon () {
    global $_CLEAN, $json_reply;
    $_params = digestParams();
    
    static $Coupon = NULL;
    if (empty($Coupon) && (int)$_CLEAN['target_id'] > 0 ) $Coupon = Cataleya\Sales\Coupon::load($_CLEAN['target_id']);
    
    if ($Coupon === NULL) _catch_error('The selected category could not be found.', __LINE__, true);
    
    foreach ($_params['product_id'] as $_id) {

       // Load product
       $Product = Cataleya\Catalog\Product::load((int)$_id);
       if ($Product === NULL) _catch_error('One or more products not found.', __LINE__, true);

       $Coupon->removeProduct($Product);
    }
    
    
    $json_reply['Coupon'] = Cataleya\Front\Dashboard\Juicer::juice($Coupon);
}







function deleteProduct () {
    global $json_reply;
    $_params = digestParams();
    
    
    if ($_params['auto_confirm'] !== TRUE) 
    {
        $message = 'Are you sure you want to delete the selected items (' . count($_params['product_id']) . ')?';
        confirm($message, FALSE);
    }
    
    Cataleya\Helper\DBH::getInstance()->beginTransaction();
    foreach ($_params['product_id'] as $_id) {

       // Load product
       $Product = Cataleya\Catalog\Product::load((int)$_id);
       if ($Product === NULL) _catch_error('One or more products not found.', __LINE__, true);

       $Product->delete();
    }
    Cataleya\Helper\DBH::getInstance()->commit();
    $json_reply['Categories'] = juiceDropCatalog();
    
    
}

    




function newProduct () {
    

    global $_CLEAN, $json_reply;
    
    $_ProductType = Cataleya\Catalog\Product\Type::load($_CLEAN['target_id']);
    
    $title = 'New ' . $_ProductType->getName();
    
    Cataleya\Helper\DBH::getInstance()->beginTransaction();
    $Product = Cataleya\Catalog\Product::create($_ProductType, $title, $title, 'EN');
    Cataleya\Helper\DBH::getInstance()->commit();
    
    $json_reply['message'] = 'New product saved.';

    $json_reply['Product'] = array (
        'id' => $Product->getProductId(), 
        'title' =>  $title, 
        'description' =>  $title, 
        'thumb' =>  $Product->getPrimaryImage()->getHref(Cataleya\Asset\Image::LARGE), 
        'stockLow'  =>  TRUE, 
        'stockOut'  =>  TRUE, 
        'views' => $Product->getViews(), 
        'favourites'    =>  $Product->getFavourites(), 
        'sold'  =>  $Product->getTotalSold(), 
        'hidden'    =>  $Product->isHidden(), 
        'viewsInEnglish'    =>  Cataleya\Helper::countInEnglish($Product->getViews(), 'View', 'Views'), 
        'index' =>  1
    );
    
    
    
    $json_reply['Categories'] = juiceDropCatalog();
    
}









// Remove token...
if (isset($_SESSION[SESSION_PREFIX.'CONFIRM_TOKEN'])) unset ($_SESSION[SESSION_PREFIX.'CONFIRM_TOKEN']);




 echo (json_encode($json_reply));
 exit();  
