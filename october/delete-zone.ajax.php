<?php



define ('OUTPUT', 'JSON'); // Whether to output as JSON or HTML (especially errors)...


// LOAD APPLICATION TOP...
require_once ('app_top.php');





/*
 * 
 * TO PERFORM THIS OPERATION, USER MUST BE AN ADMINISTRATOR
 * 
 */


if (!IS_SUPER_USER && !$adminUser->is('STORE_OWNER')  && !$_admin_role->hasPrivilege('DELETE_ZONE')) _catch_error('You do have permission to perform this action.', __LINE__, true);




$WHITE_LIST = array(
						'auth_token', 
						'zone_id'
						);





// DIE IF INPUT IS NOT VIA POST METHOD...
if ($_SERVER['REQUEST_METHOD'] != 'POST') _catch_error('POST METHOD ONLY PLEASE. NOW GET.', __LINE__, true);

// SANITIZE INPUT DATA...
$_CLEAN = filter_input_array(INPUT_POST, array(
                                                    'auth_token'		=>	FILTER_SANITIZE_STRIPPED,
                                                    'zone_id'	=>	FILTER_VALIDATE_INT

                                                    )
										
					);


// Check if INPUT keys are ALL expected AND MADE IT THROUGH...
if (anySuspects()) _catch_error('Error processing white_list!', __LINE__, true);



// VALIDATE AUTH TOKEN...
validate_auth_token ();


// Load store
$zone = Cataleya\Geo\Zone::load($_CLEAN['zone_id']);


// Check if zone exists...
if ($zone === NULL)
{
        if (isset($_SESSION[SESSION_PREFIX.'CONFIRM_PARAMS'])) unset ($_SESSION[SESSION_PREFIX.'CONFIRM_PARAMS']);
        
        // ERROR...
        _catch_error('Zone not found.', __LINE__, true);

}


// Confirm delete action
$message = "Are you sure you want to delete this zone (" . $zone->getZoneName() . ")? \n\nWarning: All Shipping Options and Tax Rates associated with this zone will be lost.\n\n";
confirm($message, FALSE);

// retrieve info about zone to be deleted
$zone_info = array (
        'id'    =>  $zone->getID(), 
        'name'  =>  $zone->getZoneName(), 
        'countryPopulation' => $zone->getCountryPopulation(), 
        'provincePopulation' => $zone->getProvincePopulation()
    );


// Delete zone (if we make it past the 'confirm' function)...
$zone->delete();


// Remove token...
if (isset($_SESSION[SESSION_PREFIX.'CONFIRM_TOKEN'])) unset ($_SESSION[SESSION_PREFIX.'CONFIRM_TOKEN']);

// OUTPUT...
 $json_reply = array (
                 "status" => 'ok', 
                 "message" => 'Zone deleted.', 
                 "zone" => $zone_info
                 );


 echo (json_encode($json_reply));
 exit();  

?>
