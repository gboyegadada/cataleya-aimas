<?php





define ('OUTPUT', 'JSON'); // Whether to output as JSON or HTML (especially errors)...


// LOAD APPLICATION TOP...
require_once ('app_top.php');






// ENV...
define ('PERMS', 'RW');
$WHITE_LIST = array(
						'auth_token', 
						'currency_code'
						);





// DIE IF INPUT IS NOT VIA POST METHOD...
if ($_SERVER['REQUEST_METHOD'] != 'POST') _catch_error('POST METHOD ONLY PLEASE. NOW GET.', __LINE__, true);

// SANITIZE INPUT DATA...
$_CLEAN = filter_input_array(INPUT_POST, 
								array(
										'auth_token'		=>	FILTER_SANITIZE_STRIPPED,  
										'currency_code'		=>	array('filter'		=>	FILTER_VALIDATE_REGEXP, 
														'options'	=>	array('regexp' => '/[A-Za-z]{2,3}/')
																)

										)
										
								);


// Check if INPUT keys are ALL expected AND MADE IT THROUGH...
if (anySuspects()) _catch_error('Error processing white_list!', __LINE__, true);


// VALIDATE AUTH TOKEN...
validate_auth_token ();





// Load currency
$currency = Cataleya\Locale\Currency::load($_CLEAN['currency_code']);
if ($currency === NULL) _catch_error('Currency could not be loaded.', __LINE__, true);



// Toggle currency status...
$new_status = ((int)$currency->getActive() === 0) ? 1 : 0;
$currency->setActive($new_status);






// Output...
$json_data = array (
                'status' => 'Ok', 
                'message' => 'Currency status updated.', 
                'currency_status' => $currency->getActive(), 
                'currency_code' => $currency->getCurrencyCode()
                );
echo json_encode($json_data);
exit();








?>