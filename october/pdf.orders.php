<?php



define ('OUTPUT', 'PDF'); // Whether to output as JSON or HTML (especially errors)...


// LOAD APPLICATION TOP...
require_once ('app_top.php');





/*
 * 
 * CHECK USER PRIVILEGES
 * 
 */


if ((!isset($_admin_role) || !$_admin_role->hasPrivilege('VIEW_ORDERS')) && !IS_SUPER_USER)  include_once INC_PATH.'forbidden.php';


define('READONLY', (!$_admin_role->hasPrivilege('EDIT_ORDERS') && !IS_SUPER_USER) ? FALSE : TRUE);




// SANITIZE GET DATA...
$_CLEAN = filter_input_array(INPUT_GET, 
								array(	
										 // id...
										'id'	=>	array('filter'		=>	FILTER_VALIDATE_INT, 
																'options'	=>	array('min_range' => 1, 'max_range' => 999999)
																), 
										 // [f] for file?...
										'f'	=>	array('filter'		=>	FILTER_VALIDATE_INT, 
																'options'	=>	array('min_range' => 0, 'max_range' => 1)
																), 
										)
								);


if ($_CLEAN['id'] === FALSE ) _catch_error('Invalid order id!', __LINE__, true);


$_Order = Cataleya\Store\Order::load($_CLEAN['id']);
if ($_Order === NULL ) _catch_error('Order could not be loaded.', __LINE__, true);

if ($_CLEAN['f'] === FALSE || $_CLEAN['f'] === NULL) $_CLEAN['f'] = 0;

Cataleya\Helper::echoPDF($_Order->makePDF(), (((int)$_CLEAN['f'] === 1) ? TRUE : FALSE));

exit();



?>



