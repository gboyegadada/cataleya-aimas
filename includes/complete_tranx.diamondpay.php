<?php




// DIE IF INPUT IS NOT VIA POST METHOD...
if ($_SERVER['REQUEST_METHOD'] != 'GET') _catch_error('GET METHOD ONLY PLEASE. NOW GET.', __LINE__, true);

// SANITIZE INPUT DATA...
$_CLEAN = filter_input_array(INPUT_GET, array(
                                                    'TransactionReference'		=>	array('filter'		=>	FILTER_VALIDATE_REGEXP, 
                                                                                                'options'	=>	array('regexp' => '/^[A-Za-z0-9]{6,25}$/')
                                                                                                                ),
                                                    'OrderID'	=>	array('filter'		=>	FILTER_VALIDATE_REGEXP, 
                                                                                                'options'	=>	array('regexp' => '/^[0-9]{1,50}$/')
                                                                                                                )

                                                    )
										
					);





// VALIDATE AUTH TOKEN...
if ($_CLEAN['TransactionReference'] === FALSE || $_CLEAN['OrderID'] === FALSE) _catch_error('Bad request.', __LINE__, true);

$NVP_Array = array (
    'MERCHANT_ID'    =>  MERCHANT_ID, 
    'ORDER_ID' => $_CLEAN['OrderID']
);




$NVP_String = WEBPAY_STATUS_ENDPOINT . '?' . Cataleya_Class_NVPParser::toString($NVP_Array);
$result = Cataleya_Class_NVPParser::curlRequest($NVP_String, FALSE, 'GET');


if (empty($result)) _catch_error('Error fetching tranx details.', __LINE__, true);


$_TranxStatusXML = new SimpleXMLElement ($result);
if (empty($_TranxStatusXML)) _catch_error('Error processing XML response.', __LINE__, true);

//try {
//    $_TranxStatusXML = new SimpleXMLElement ($result);
//
//} catch (Exception $e) {
//    
//    _catch_error('Error processing XML response.', __LINE__, true);
//    
//    
//}



// UPDATE ORDER PAYMENT STATUS

$_id = Cataleya_Class_Validator::int($_CLEAN['OrderID']);
if ($_id === FALSE) _catch_error('Invalid order id.', __LINE__, true);

$_Order = Cataleya_Class_Order::load($_id);
if (empty($_Order)) _catch_error('Order not found.', __LINE__, true);



// Payment Status
if (in_array(strtolower($_TranxStatusXML->Status), array('pending', 'successful', 'cancelled', 'failed'))) $_Order->setPaymentStatus($_TranxStatusXML->Status);
else if (strtolower($_TranxStatusXML->Status) === 'duplicateorderid') $_Order->setPaymentStatus('paid');
else $_Order->setPaymentStatus('failed');



// Order Status
if (in_array(strtolower($_TranxStatusXML->Status), array('successful', 'duplicateorderid'))) $_Order->setOrderStatus('paid');

else if (in_array(strtolower($_TranxStatusXML->Status), array('cancelled', 'failed'))) $_Order->setOrderStatus('cancelled');

else if (in_array(strtolower($_TranxStatusXML->Status), array('pending'))) $_Order->setOrderStatus('pending');

else $_Order->setOrderStatus('cancelled');



foreach (Cataleya_Class_Transactions::load($_Order) as $_Tranx) 
{
   $_Tranx->setWebpayTransactionId($_TranxStatusXML->TransactionRef);
   break;
}




// OUTPUT

$_ResponseMessage = "";
$_ResponseColor = "blue";

switch ($_TranxStatusXML->StatusCode)
{
    case '00': // Successful
        $_ResponseMessage = "Your payment was successful!";
        $_ResponseColor = "blue";
        break;
		
    case '08': // Successful (duplicate ID: already processed)
        $_ResponseMessage = "Your payment was successful!";
        $_ResponseColor = "blue";
        break;
    
    case '03': // Cancelled
        $_ResponseMessage = "Your order has been cancelled.";
        $_ResponseColor = "red";
        break;
    
    case '02': // Pending
        $_ResponseMessage = "Your payment is pending.";
        $_ResponseColor = "red";
        break;
    
    case '01': // Failed
        $_ResponseMessage = "Your payment could not be processed" . " (" . $_TranxStatusXML->Status . ")";
        $_ResponseColor = "red";
        break;
    
    case '09': // Invalid Amount
        $_ResponseMessage = "Your payment could not be processed" . " (" . $_TranxStatusXML->Status . ")";
        $_ResponseColor = "red";
        break;
    
    default: // Invalid Amount
        $_ResponseMessage = "Your payment could not be processed" . " (" . $_TranxStatusXML->Status . ")";
        $_ResponseColor = "red";
        break;
    
}




if (!isset($twig_vars)) $twig_vars = array ();
$_Today = new DateTime();



$_Customer = $_Order->getCustomer();



$twig_vars['Meta'] = array (
    'copyright'  =>  ('&copy;' . $_Today->format('Y ') . STORE_NAME)
);




$twig_vars['Store'] = array (
    'name'  =>  STORE_NAME, 
    'StoreOwnerEmail'  =>  STORE_OWNER_EMAIL, 
    'CustomerServiceEmail' =>  STORE_OWNER_EMAIL
);

$twig_vars['Order'] = array (
    'id'  =>  $_Order->getID(), 
    'number'  =>  $_Order->getOrderNumber(), 
    'amount' => number_format(floatval($_TranxStatusXML->Amount), 2), 
    'TransactionReference'  =>  $_TranxStatusXML->TransactionRef, 
    'StatusMessage' => $_ResponseMessage, 
    'StatusColor' => $_ResponseColor, 
    'WebpayLoginURL' => ('https://cipg.diamondbank.com/cipg/MerchantCustomerView/MerchantCustomerReport.aspx?email=' . $_Customer->getEmailAddress() . '&mercID=' . MERCHANT_ID)
);


$twig_vars['Customer'] = array (
    'name'  =>  $_Customer->getName(),  
    'firstname'  =>  $_Customer->getFirstname(), 
    'lastname'  =>  $_Customer->getLastname(), 
    'email'  =>  $_Customer->getEmailAddress(), 
    'phone'  =>  $_Customer->getTelephone(), 
);


// Notify Store Owner & Customer

require_once (INC_PATH . 'send_tranx_receipt_email.php');



// Load template...
$template = Cataleya_Class_Twiggy::loadTemplate('tranx-status.html.twig');   
echo $template->render($twig_vars);


 exit();  

?>