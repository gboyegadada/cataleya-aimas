<?php

/**
 * inoculate against hack attempts which waste CPU cycles
 */
$contaminated = (isset($_FILES['GLOBALS']) || isset($_REQUEST['GLOBALS'])) ? true : false;
$paramsToAvoid = array('GLOBALS', '_COOKIE', '_ENV', '_FILES', '_GET', '_POST', '_REQUEST', '_SERVER', '_SESSION', 'HTTP_COOKIE_VARS', 'HTTP_ENV_VARS', 'HTTP_GET_VARS', 'HTTP_POST_VARS', 'HTTP_POST_FILES', 'HTTP_RAW_POST_DATA', 'HTTP_SERVER_VARS', 'HTTP_SESSION_VARS');
$paramsToAvoid[] = 'autoLoadConfig';
$paramsToAvoid[] = 'mosConfig_absolute_path';
$paramsToAvoid[] = 'hash';
$paramsToAvoid[] = 'main';
foreach($paramsToAvoid as $key) {
  if (isset($_GET[$key]) || isset($_POST[$key]) || isset($_COOKIE[$key])) {
    $contaminated = true;
    break;
  }
}

$paramsToCheck = array('pg', 'f', 'p', 'o', 'ref');


if (!$contaminated) {
  foreach($paramsToCheck as $key) {
    if (isset($_GET[$key]) && !is_array($_GET[$key])) {
      if (substr($_GET[$key], 0, 4) == 'http' || strstr($_GET[$key], '//')) {
        $contaminated = true;
        break;
      }
      if (isset($_GET[$key]) && strlen($_GET[$key]) > 43) {
        $contaminated = true;
        break;
      }
    }
  }
}
unset($paramsToCheck, $paramsToAvoid, $key);
if ($contaminated)
{
  header('HTTP/1.1 406 Not Acceptable');
  exit(0);
}
unset($contaminated);
/* *** END OF INNOCULATION *** */






/////////////////////////////////////////////////////////////////
//// STOREFRONT PAGE TOP 






// A small check on SSL requirement
// Require SSL if an administrator is online (using same browser)
if (isset($_COOKIE['_a']) && !defined('SSL_REQUIRED')) define('SSL_REQUIRED', TRUE);


/*
// Check if this an SSL PAGE...
$_ssl_pages = array (
    'myaccount', 
    'login', 
    'logout', 
    'checkout', 
    'confirm'
);

if (
        !empty($_GET['c']) 
        && in_array($_GET['c'], $_ssl_pages) 
        && !defined('SSL_REQUIRED')
        )  define('SSL_REQUIRED', TRUE);

*/





// Prevent PHP from timing out !!
set_time_limit (60 * 3); 



////////// CONFIG //////////
require_once (ROOT_PATH.'includes/store.core.config.php');	




// GENERAL FUNCTIONS...
require_once (INC_PATH.'functions/general_funcs.php');


// Check _SERVER['HTTP_HOST']
if (strtolower($_SERVER['HTTP_HOST']) !== DOMAIN && strtolower($_SERVER['HTTP_HOST']) !== "www.".DOMAIN) 
{
  header('HTTP/1.0 400 Bad Request');
  exit(0);
}

else if (strtolower($_SERVER['HTTP_HOST']) === "www.".DOMAIN) 
{
    
    header('Location: http://' . DOMAIN . $_SERVER['REQUEST_URI']);
    exit();
}




/*
// Check for direct calls to AJAX scripts...
if (defined('OUTPUT') && (OUTPUT == 'AJAX_JSON' || OUTPUT == 'AJAX_HTML')) {
	if(isset($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
		// This an ajax request. Do nothing.
	 } else {
		$_to = 'http://' . DOMAIN;
		header('Location: ' . $_to);
		exit();
	 }
}
 
 */

 

// Redirect to https if  SSL is required...
if ( defined('SSL_REQUIRED') && SSL_REQUIRED === TRUE && !is_ssl() ) {
	if ( 0 === strpos($_SERVER['REQUEST_URI'], 'http') ) {
		redirect(preg_replace('|^http://|', 'https://', $_SERVER['REQUEST_URI']));
		exit();
	} else {
		redirect('https://' . DOMAIN . $_SERVER['REQUEST_URI']);
		exit();
	}
}







// AUTH FUNCTIONS...
require_once (INC_PATH.'functions/auth_funcs.php');











// HEADERS
//header("Cache-Control: no-cache, must-revalidate");
header("Cache-Control: no-transform");
//header("Expires: Mon, 26 Jul 1997 05:00:00 GMT");
header("X-Frame-Options: DENY"); // 'same-origin': allow being framed from urls of the same origin || 'deny': block all
header("X-XSS-Protection: 1; mode=block");
header("X-Content-Security-Policy: " . 
    ((defined('SSL_REQUIRED') && SSL_REQUIRED) ? "allow https://*:443" : "allow 'self'") . 
    "; report-uri " . DOMAIN . "/csp-report" . 
    "; style-src " . DOMAIN . 
    "; img-src " . DOMAIN . 
        " assets.pinterest.com " . 
        "facebook.com facebook.net" . 
    "; object-src " . DOMAIN . 
    "; script-src " . DOMAIN . 
        " assets.pinterest.com " . 
        "facebook.com facebook.net " . 
        "https://apis.google.com " . 
        "https://platform.twitter.com" . 
    "; xhr-src " . DOMAIN . 
    "; frame-src " . 
        "https://plusone.google.com " . 
        "facebook.com facebook.net " . 
        "https://platform.twitter.com" . 
    "; options inline-script setTimeout setInterval;"
);
if (defined('SSL_REQUIRED') && SSL_REQUIRED) header ("Strict-Transport-Security: max-age=8640000; includeSubDomains");








