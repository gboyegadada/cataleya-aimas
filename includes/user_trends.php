<?php
if (!defined('IS_ADMIN_FLAG')) {
  _error_handler('Illegal Access');
}


/*

unset($_SESSION[SESSION_PREFIX.'UA_TREND'], $_SESSION[SESSION_PREFIX.'IP_TREND'], $_SESSION[SESSION_PREFIX.'GEO_LOC_TREND']);
unset($_SESSION[SESSION_PREFIX.'USER_AGENT'], $_SESSION[SESSION_PREFIX.'IP'], $_SESSION[SESSION_PREFIX.'GEO_LOC']);

*/

//////////////////// IP TREND ///////////////////



// User contacting server for the first time.
if (!isset ($_SESSION[SESSION_PREFIX.'IP'])) {
	$_SESSION[SESSION_PREFIX.'IP'] = $_SERVER['REMOTE_ADDR'];
	$_SESSION[SESSION_PREFIX.'IP_TREND'] = 1;
	
	define('IP_RED_FLAG', FALSE);
} 


// User contacting server for the nth CONSECUTIVE time with SAME IP.
else if ($_SERVER['REMOTE_ADDR'] == $_SESSION[SESSION_PREFIX.'IP']) {
	$_SESSION[SESSION_PREFIX.'IP_TREND']++;
	define('IP_RED_FLAG', FALSE);
}


// User contacting server for the nth time with a DIFFERENT IP...
// ...and (n) is still below our trend_threshold.
else if ($_SESSION[SESSION_PREFIX.'IP_TREND'] < IP_TREND_THRESHOLD) {
	$_SESSION[SESSION_PREFIX.'IP'] = $_SERVER['REMOTE_ADDR'];
	$_SESSION[SESSION_PREFIX.'IP_TREND'] = 1;
	define('IP_RED_FLAG', FALSE);
}


// User has been contacting server CONSISTENTLY using the SAME IP...
// ...and a NEW IP out of nowhere is trying to access the same session.
else {
	
	define('IP_RED_FLAG', TRUE);
}








//////////////////// USER_AGENT TREND ///////////////////

// Get USER_AGENT
$NEW_UA = $_SERVER['HTTP_USER_AGENT'];
if ( isset($_SESSION[SESSION_PREFIX.'USER_AGENT']) ) $CURRENT_UA = $_SESSION[SESSION_PREFIX.'USER_AGENT'];


// User contacting server with UA for the first time.
if (!isset ($_SESSION[SESSION_PREFIX.'USER_AGENT'])) {
	$_SESSION[SESSION_PREFIX.'USER_AGENT'] = $_SERVER['HTTP_USER_AGENT'];
	$_SESSION[SESSION_PREFIX.'UA_TREND'] = 1;
	define('UA_RED_FLAG', FALSE);
} 


// User contacting server for the nth CONSECUTIVE time with SAME BROWSER.
else if ($_SESSION[SESSION_PREFIX.'USER_AGENT'] == $_SERVER['HTTP_USER_AGENT']) {
	$_SESSION[SESSION_PREFIX.'UA_TREND']++;
	define('UA_RED_FLAG', FALSE);
}


// User contacting server for the nth time with a DIFFERENT BROWSER...
// ...and (n) is still below our trend_threshold.
else if ($_SESSION[SESSION_PREFIX.'UA_TREND'] < BROWSER_TREND_THRESHOLD) {
	$_SESSION[SESSION_PREFIX.'USER_AGENT'] = $_SERVER['HTTP_USER_AGENT'];
	$_SESSION[SESSION_PREFIX.'UA_TREND'] = 1;
	define('UA_RED_FLAG', FALSE);
}


// User has been contacting server CONSISTENTLY using the SAME GEO_LOC...
// ...and a NEW GEO_LOC out of nowhere is trying to access the same session.
else {
	
	define('UA_RED_FLAG', TRUE);
}






//////////////////// GEO_LOC TREND ///////////////////


// load GEO_IP tool
require_once(SHARED_INC_PATH."geoip.inc");

// Open geo_ip data file
$gi = geoip_open(SHARED_INC_PATH."GeoIP.dat",GEOIP_STANDARD);

// Get remote GEO_LOC
define('NEW_GEO_LOC', geoip_country_code_by_addr($gi, $_SERVER['REMOTE_ADDR']));

// Close geo_ip data file
geoip_close($gi);



// User contacting server for the first time.
if (!isset ($_SESSION[SESSION_PREFIX.'GEO_LOC'])) {
	$_SESSION[SESSION_PREFIX.'GEO_LOC'] = NEW_GEO_LOC;
	$_SESSION[SESSION_PREFIX.'GEO_LOC_TREND'] = 1;
	define('GEO_LOC_RED_FLAG', FALSE);
} 


// User contacting server for the nth CONSECUTIVE time with SAME GEO_LOC.
else if ($_SESSION[SESSION_PREFIX.'GEO_LOC'] == NEW_GEO_LOC) {
	$_SESSION[SESSION_PREFIX.'GEO_LOC_TREND']++;
	define('GEO_LOC_RED_FLAG', FALSE);
}


// User contacting server for the nth time with a DIFFERENT GEO_LOC...
// ...and (n) is still below our trend_threshold.
else if ($_SESSION[SESSION_PREFIX.'GEO_LOC_TREND'] < GEO_LOC_TREND_THRESHOLD) {
	$_SESSION[SESSION_PREFIX.'GEO_LOC'] = NEW_GEO_LOC;
	$_SESSION[SESSION_PREFIX.'GEO_LOC_TREND'] = 1;
	define('GEO_LOC_RED_FLAG', FALSE);
}


// User has been contacting server CONSISTENTLY using the SAME GEO_LOC...
// ...and a NEW GEO_LOC out of nowhere is trying to access the same session.
else {
	
	define('GEO_LOC_RED_FLAG', TRUE);
	
}



?>