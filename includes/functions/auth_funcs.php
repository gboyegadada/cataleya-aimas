<?php


// if (!defined('IS_ADMIN_FLAG')) {
//   _catch_error('Illegal Access');
// }


// RETURNS TRUE IF SUSPICIOUS POST DATA IS FOUND [ OR ] FALSE IF NONE.
function anySuspects() {
global $WHITE_LIST, $_CLEAN, $_POST;

$INPUT = ($_SERVER['REQUEST_METHOD'] === 'POST') ? $_POST : $_GET;

if (!defined('LOGIN_TOKEN')) define ('LOGIN_TOKEN', '0000');

if (!isset($WHITE_LIST) || !isset($_CLEAN)) return true;

// Check if INPUT keys are ALL expected AND MADE IT THROUGH...

	// Check for keys we're NOT expecting...
	foreach ($INPUT as $exp_key=>$exp_val) {
		if (in_array($exp_key, $WHITE_LIST)) continue;
		else return true; // One agbero dey here.
	}
	// Check for values that didn't make it through the filter...
	foreach ($_CLEAN as $exp_key=>$exp_val) {
		if (($exp_val !== FALSE && $exp_val !== NULL) || $exp_key === 'login-'.LOGIN_TOKEN || $exp_key === 'cancel-'.LOGIN_TOKEN) continue;
                else if ($exp_val === FALSE && $INPUT[$exp_key] === '') continue;
		else return true; // One agbero dey here.
	}
	
	return false; // No suspects found.
}




// BOMB SESSION
function bombSession () {
	
	
		// 1. Log request data (as a flag)...
				// IP
				// user_id
				// Date-time
				// User Agent
				// Page being accessed
				
		// 2. Destroy session...
		// 3. Auto Logout any active user...
	
		switch (OUTPUT) {
			
			case 'JSON':
				$json_reply = array (
				"status"=>'bomb', 
				"message"=>"Redirecting..."
				);

				echo (json_encode($json_reply));
				exit();
			break;
			
			case 'PLAIN_TEXT':
				redirect (SHOP_LANDING . 'login');
				exit('Redirecting...');
			break;
			
			default:
				Header('Location: ' . SHOP_LANDING . 'login');
				exit('<center><h2>Redirecting...</h2></center> ');
			break;
		}
	
}




/////////// CUSTOMER_LOGOUT ////////////////////////

function customer_logout ($show_login = false) {
	if ($customer_id == 0) return false;
	
	global $dbh;
	static $cutomer_update, $param_customer_id;
	
	if (!isset($cutomer_update)) {
		$cutomer_update = $dbh->prepare('UPDATE customer_info SET is_online = 0 WHERE customer_id = :customer_id');
		$cutomer_update->bindParam(':customer_id', $param_customer_id, PDO::PARAM_INT);
	}
	
	$param_customer_id = $customer_id;
	if (!$cutomer_update->execute()) _catch_error ($cutomer_update->errorInfo(), __LINE__, true);
	
	// destroy cart...
	destroy_cart();
	
	// reset session
	$oldSession = $_SESSION;
	session_destroy();
	
	
	if ($show_login) show_login();
	else return true;
	
	
}

/*
 * 
 * [ generateToken ]
 * 
 * 
 */

function generateToken ()
{
    return (md5( uniqid(rand(), TRUE) ) . '-' . CURRENT_DT);
}




/*
 * [ digest ]
 * 
 */


function digest ($salt = '', $timestamp = FALSE) 
{
    if ($salt === '') 
    {
        $_keys = array ('hushpuppys', 'sagiterrian', 'olodo', 'dondeymad', 'goshitinyourpants', 'megalomeniac', 'nacciruomoalata');        
        $salt = $_keys[rand(0, count($_keys)-1)];
        
    }
    return (sha1(strval(rand(0,microtime(true))) . $salt . strval(microtime(true)))) . (($timestamp) ? '-' . CURRENT_DT : '');
}





/*
 * 
 * [ goto_login ]
 * ____________________________________________________________
 * 
 * Redirect to login page
 * 
 * 
 */
function goto_login () {
	
			// Redirect to login page
			Header ("Location: ".SHOP_LANDING.'login', true); 
			exit;	
	
}



/*
 * 
 * [ show_login ]
 * ________________________________________________________
 * 
 * Removed!!
 * 
 * Use: include_once (INC_PATH.'show_login.php');
 * 
 * 
 */











?>
