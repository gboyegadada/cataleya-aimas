<?php

// DEBUG MODE...
define('OFOFO', true);



// UNIVERSAL ERROR HANDLER (JUST ERROR MESSAGES)
function _catch_error($message, $line = 0, $die = TRUE, $safe = FALSE) {
	
    if (OFOFO || $safe) $msg_str = (is_array($message)) ? ('@ ' . implode(', ', $message) . ' @') : $message;
    else $msg_str = "An error has occurred while processing your request. Please try again in a few minutes, or contact us (customer.care@aimas.com.ng) if this is a recurring issue.";
    
    
    if (!OFOFO) $line = '';
	

    switch (OUTPUT) {
            case 'JSON':
                    _json_error($msg_str, $line, $die);
            break;
            case 'PLAIN_TEXT':
                    _text_error($msg_str, $line, $die);
            break;
            default:
                    _html_error($msg_str, $line, $die);
            break;
    }
			
			
    if (isset($die) && $die == true) {

    exit(1); // exit with error code
    }
	
}



// OUTPUT ERROR AS HTML...
function _html_error($message, $line, $die) {
	?>
    
	<h2><?=$line?></h2>
    <p>
    <?=$message?>
    
    </p>
    
    <?php
	
if (isset($die) && $die != true) return;
	else exit();

}


// OUTPUT ERROR AS PLAIN TEXT...
function _text_error($message, $line, $die) {
	
	
if (isset($die) && $die != true) return;
	else exit();
}



// SEND ERROR MESSAGE TO CLIENT AS JSON...
function _json_error ($message, $line = 0, $die = true) {
$msg_str = (is_array($message)) ? ('@ ' . implode(', ', $message) . ' @') : $message;

$line = (OFOFO && isset($line)) ? $line : '';
$msg_str = (OFOFO) ? $msg_str : '';

$json_reply = array (
		"status"=>'error', 
		"line"=>$line, 
		"message"=>"Error!: <br />" . $msg_str . "<br />"
		);

echo (json_encode($json_reply));
if (isset($die) && $die != true) return;
	else exit();
	
	
}


// USE AFTER decode_json ...
function get_json_last_error () {
	
$_json_last_error = '';
	
switch (json_last_error()) {
        case JSON_ERROR_NONE:
            $_json_last_error = ' - No errors';
        break;
        case JSON_ERROR_DEPTH:
            $_json_last_error = ' - Maximum stack depth exceeded';
        break;
        case JSON_ERROR_STATE_MISMATCH:
            $_json_last_error = ' - Underflow or the modes mismatch';
        break;
        case JSON_ERROR_CTRL_CHAR:
            $_json_last_error = ' - Unexpected control character found';
        break;
        case JSON_ERROR_SYNTAX:
            $_json_last_error = ' - Syntax error, malformed JSON';
        break;
        case JSON_ERROR_UTF8:
            $_json_last_error = ' - Malformed UTF-8 characters, possibly incorrectly encoded';
        break;
        default:
            $_json_last_error = ' - Unknown error';
        break;
    }

return $_json_last_error;

}




?>