<?php

// INCLUDE DATABASE AUTH...
// require_once('../sql_connect.php');

// CONNECT TO DBASE...
// define ('PDO_DBN_GEO', 'mysql:host='.DB_HOST.';dbname=db_geoip');

try {
	$db_geoip = \Cataleya\Helper\DBH::getInstance(); // new PDO(PDO_DBN_GEO, DB_USER, DB_PASS);
	
} catch (PDOException $e) {
	
    throw new \Cataleya\Error ("Geo IP error! No db connection!", 0, $e);
	
	
}


// FUNCTIONS TO SELECT DATA
function getALLfromIP($addr)
{
global $db_geoip;
$ip_select = $db_geoip->prepare("SELECT start, cc, cn FROM ip NATURAL JOIN cc WHERE end >= :ipnum ORDER BY end ASC LIMIT 1");
$ip_select->bindParam(':ipnum', $ipnum);

// this sprintf() wrapper is needed, because the PHP long is signed by default
$ipnum = sprintf("%u", ip2long($addr));

$ip_select->execute();
$data = $ip_select->fetch();

if((! $result) || mysql_numrows($result) < 1 || $data['start'] > $ipnum )
{
return false;
}
return $data;
}

function getCCfromIP($addr) {
$data = getALLfromIP($addr);
if($data) return $data['cc'];
return false;
}

function getCOUNTRYfromIP($addr) {
$data = getALLfromIP($addr);
if($data) return $data['cn'];
return false;
}

function getCCfromNAME($name) {
$addr = gethostbyname($name);
return getCCfromIP($addr);
}

function getCOUNTRYfromNAME($name) {
$addr = gethostbyname($name);
return getCOUNTRYfromIP($addr);
}
?>
