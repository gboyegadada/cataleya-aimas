<?php


/*

GENERAL FUNCTIONS


*/


// namespace: root

use \Cataleya\Helper\DBH;
use \Cataleya\Error;


/**
 * __       This is to help decouple plugin apps from the core.
 *
 *               DO NOT: \Cataleya\Catalog\Product::load($_id) 
 *
 *               DO: __('product', $_id, $_arg2, $_arg3...) instead.
 *
 *
 * @param string $_model
 * @access protected
 * @return void
 */
function __ () 
{

    return forward_static_call_array([ '\Cataleya\Helper\Model', 'load'], func_get_args()); 


}



/**
 * __       This is to help decouple plugin apps from the core.
 *
 *               DO NOT: \Cataleya\Catalog\Product::load($_id) 
 *
 *               DO: __('product', $_id, $_arg2, $_arg3...) instead.
 *
 *
 * @param string $_model
 * @access protected
 * @return void
 */
function __url () 
{


    return forward_static_call_array([ '\Cataleya\Helper\URL', 'load'], func_get_args()); 


    
}



/**
 * __       This is to help decouple plugin apps from the core.
 *
 *               DO NOT: \Cataleya\Catalog\Product::load($_id) 
 *
 *               DO: __('product', $_id, $_arg2, $_arg3...) instead.
 *
 *
 * @param string $_model
 * @access protected
 * @return void
 */
function __path () 
{


    return forward_static_call_array([ '\Cataleya\Helper\Path', 'load'], func_get_args()); 


    
}


//////// AUTO REDIRECT URIs TO THEIR PROPER RE-WRITTEN URLS ///////

function checkUrl ($s) {
	// Get the current URL without the query string, with the initial slash
	$myurl = preg_replace('/\?.*$/', '', $_SERVER['REQUEST_URI']);
	
	// If it is not the same as the desired URL, then redirect
	if ($myurl != "/$s") {
			if (!headers_sent()) Header ("Location: /$s", true, 301); 
                        else echo ('<head><meta http-equiv="refresh" content=0;URL=/' . $s. '</head>');
			exit();
	}
	
	
}





////////// GO_HOME /////////////////////

function go_home () {
			// Redirect to shop landing
			if (!headers_sent()) Header ("Location: ".SHOP_LANDING, true); 
                        else echo ('<head><meta http-equiv="refresh" content=0;URL=' . SHOP_LANDING . '</head>');
			exit;
                        
}





/*
 * 
 * getIPAddress
 * 
 */

function get_ip ()
{
    
    
}




 
 
 

/////////////////// DATABASE CACHE FUNCTIONS //////////



function cache_query_result ($result_array = array(), $cache_id = '') {
	
	// Check if there's anything to cache...
	if (count($result_array) < 1) return array();
	

	$dbh = Cataleya\Helper\DBH::getInstance();


	// make a unique cache id i none is specified...
	$cache_id = ($cache_id != '') ? $cache_id : 'cache-'.(time());
	
	/* 
	
	Make contents safe for JSON...
	foreach ($result_array as $row_key=>$row) {
		
		foreach ($row as $col_key=>$col) {
		if (!is_numeric($col)) $result_array[$row_key][$col_key] = $col;
		
		}
	}
	
	*/
	
	
	$query_cache_insert = $dbh->prepare('
							INSERT INTO db_cache (cache_entry_name, cache_data, cache_entry_created)   
							VALUES (:cache_entry_name, :cache_data, :cache_entry_created) 
							');

	$query_cache_insert->bindParam(':cache_entry_name', $cache_entry_name, PDO::PARAM_STR);
	$query_cache_insert->bindParam(':cache_data', $cache_data, PDO::PARAM_STR);
	$query_cache_insert->bindParam(':cache_entry_created', $cache_entry_created, PDO::PARAM_INT);
	
	$cache_entry_name = $cache_id;
	$cache_data = json_encode($result_array);
	$cache_entry_created = time();
	
	
	if(!$query_cache_insert->execute()) _catch_error($query_cache_insert->errorInfo(), __LINE__, true);
	
	return $cache_id;
	
}






function get_cache ($cache_id = '') {
	
	$dbh = Cataleya\Helper\DBH::getInstance();
	
	if ($cache_id == '') return array();
	$cache_data = array(); 
	
	$query_cache_select = $dbh->prepare('
							SELECT * FROM db_cache 
							WHERE cache_entry_name = :cache_entry_name 
							LIMIT 1
							');
							
	$query_cache_select->bindParam(':cache_entry_name', $cache_entry_name, PDO::PARAM_STR);
	$cache_entry_name = $cache_id;
	
	if(!$query_cache_select->execute()) _catch_error($query_cache_select->errorInfo(), __LINE__, true);
	
	$cache_data_array = array();
	
	if ($row = $query_cache_select->fetch()) {
		$cache_data = json_decode($row['cache_data'], true);	
		
	}
	
	return $cache_data;
	
}



/////////////////// CACHE CONTROL ///////////////
function cache_cleanup () {
	$dbh = Cataleya\Helper\DBH::getInstance();
	static $cache_clean_up, $cache_expires;
	
	if (empty($cache_clean_up)) {
	$cache_clean_up = $dbh->prepare('DELETE FROM db_cache WHERE cache_entry_created <= :cache_expires');
	$cache_clean_up->bindParam(':cache_expires', $cache_expires, PDO::PARAM_INT);
	$cache_expires = DB_CACHE_EXP;
	}
	
	if (!$cache_clean_up->execute()) _catch_error($cache_clean_up->errorInfo(), __LINE__, true);

	return TRUE;
}






function get_cookie ($name = '') 
{
    $data = array();
    
    if (isset($_COOKIE[COOKIE_PREFIX.$name]))
    {
        $tmpAr = explode('|', base64_decode($_COOKIE[COOKIE_PREFIX.$name]));

        foreach ($tmpAr as $pair) 
        {
            list($k, $v) = explode('=', $pair);
            $data[$k] = urldecode($v);
        }
    }
    
    return $data;
}



function make_cookie_string ($data = array()) 
{
    if (empty($data)) return '';
    
    $tmpAr = array ();
    
    foreach ($data as $key => $value)  $tmpAr[] = $key . '=' . urlencode($value); 
    
    return base64_encode(implode('|', $tmpAr));
}




function make_cookie ($name = '', $data = array()) 
{
    if (empty($data)) return FALSE;
   
    setcookie(
            COOKIE_PREFIX.$name, 
            make_cookie_string($data), 
            COOKIE_EXPIRES, 
            COOKIE_PATH, 
            COOKIE_DOMAIN, 
            COOKIE_SECURE, 
            TRUE
            );
}









/**
 * Sanitizes title, replacing whitespace and a few other characters with dashes.
 *
 * Limits the output to alphanumeric characters, underscore (_) and dash (-).
 * Whitespace becomes a dash.
 *
 * @since 1.2.0
 *
 * @param string $title The title to be sanitized.
 * @param string $raw_title Optional. Not used.
 * @param string $context Optional. The operation for which the string is sanitized.
 * @return string The sanitized title.
 */
function sanitize_title_with_dashes($title, $raw_title = '', $context = 'display') {
	$title = strip_tags($title);
	// Preserve escaped octets.
	$title = preg_replace('|%([a-fA-F0-9][a-fA-F0-9])|', '---$1---', $title);
	// Remove percent signs that are not part of an octet.
	$title = str_replace('%', '', $title);
	// Restore octets.
	$title = preg_replace('|---([a-fA-F0-9][a-fA-F0-9])---|', '%$1', $title);

	if (seems_utf8($title)) {
		if (function_exists('mb_strtolower')) {
			$title = mb_strtolower($title, 'UTF-8');
		}
		$title = utf8_uri_encode($title, 200);
	}

	$title = strtolower($title);
	$title = preg_replace('/&.+?;/', '', $title); // kill entities
	$title = str_replace('.', '-', $title);

	if ( 'save' == $context ) {
		// Convert nbsp, ndash and mdash to hyphens
		$title = str_replace( array( '%c2%a0', '%e2%80%93', '%e2%80%94' ), '-', $title );

		// Strip these characters entirely
		$title = str_replace( array(
			// iexcl and iquest
			'%c2%a1', '%c2%bf',
			// angle quotes
			'%c2%ab', '%c2%bb', '%e2%80%b9', '%e2%80%ba',
			// curly quotes
			'%e2%80%98', '%e2%80%99', '%e2%80%9c', '%e2%80%9d',
			'%e2%80%9a', '%e2%80%9b', '%e2%80%9e', '%e2%80%9f',
			// copy, reg, deg, hellip and trade
			'%c2%a9', '%c2%ae', '%c2%b0', '%e2%80%a6', '%e2%84%a2',
		), '', $title );

		// Convert times to x
		$title = str_replace( '%c3%97', 'x', $title );
	}

	$title = preg_replace('/[^%a-z0-9 _-]/', '', $title);
	$title = preg_replace('/\s+/', '-', $title);
	$title = preg_replace('|-+|', '-', $title);
	$title = trim($title, '-');

	return $title;
}




/**
 * Checks to see if a string is utf8 encoded.
 *
 * NOTE: This function checks for 5-Byte sequences, UTF8
 *       has Bytes Sequences with a maximum length of 4.
 *
 * @author bmorel at ssi dot fr (modified)
 * @since 1.2.1
 *
 * @param string $str The string to be checked
 * @return bool True if $str fits a UTF-8 model, false otherwise.
 */
function seems_utf8($str) {
	$length = strlen($str);
	for ($i=0; $i < $length; $i++) {
		$c = ord($str[$i]);
		if ($c < 0x80) $n = 0; # 0bbbbbbb
		elseif (($c & 0xE0) == 0xC0) $n=1; # 110bbbbb
		elseif (($c & 0xF0) == 0xE0) $n=2; # 1110bbbb
		elseif (($c & 0xF8) == 0xF0) $n=3; # 11110bbb
		elseif (($c & 0xFC) == 0xF8) $n=4; # 111110bb
		elseif (($c & 0xFE) == 0xFC) $n=5; # 1111110b
		else return false; # Does not match any model
		for ($j=0; $j<$n; $j++) { # n bytes matching 10bbbbbb follow ?
			if ((++$i == $length) || ((ord($str[$i]) & 0xC0) != 0x80))
				return false;
		}
	}
	return true;
}






/**
 * Encode the Unicode values to be used in the URI.
 *
 * @since 1.5.0
 *
 * @param string $utf8_string
 * @param int $length Max length of the string
 * @return string String with Unicode encoded for URI.
 */
function utf8_uri_encode( $utf8_string, $length = 0 ) {
	$unicode = '';
	$values = array();
	$num_octets = 1;
	$unicode_length = 0;

	$string_length = strlen( $utf8_string );
	for ($i = 0; $i < $string_length; $i++ ) {

		$value = ord( $utf8_string[ $i ] );

		if ( $value < 128 ) {
			if ( $length && ( $unicode_length >= $length ) )
				break;
			$unicode .= chr($value);
			$unicode_length++;
		} else {
			if ( count( $values ) == 0 ) $num_octets = ( $value < 224 ) ? 2 : 3;

			$values[] = $value;

			if ( $length && ( $unicode_length + ($num_octets * 3) ) > $length )
				break;
			if ( count( $values ) == $num_octets ) {
				if ($num_octets == 3) {
					$unicode .= '%' . dechex($values[0]) . '%' . dechex($values[1]) . '%' . dechex($values[2]);
					$unicode_length += 9;
				} else {
					$unicode .= '%' . dechex($values[0]) . '%' . dechex($values[1]);
					$unicode_length += 6;
				}

				$values = array();
				$num_octets = 1;
			}
		}
	}

	return $unicode;
}





/**
 * Determine if SSL is used.
 *
 * @since 2.6.0
 *
 * @return bool True if SSL, false if not used.
 */

function is_ssl() {
	if ( isset($_SERVER['HTTPS']) ) {
		if ( 'on' == strtolower($_SERVER['HTTPS']) )
			return true;
		if ( '1' == $_SERVER['HTTPS'] )
			return true;
	} elseif ( isset($_SERVER['SERVER_PORT']) && ( '443' == $_SERVER['SERVER_PORT'] ) ) {
		return true;
	}
	return false;
}




/*
 * Redirect
 *
 */
 
 
 function redirect ($url, $JSON = FALSE) {
	 
		Header("Location: $url");

	// exit();
	 
	 
 }

 
 

//////////////////////// 404 REDIRECT ///////////

function send_404 () {
	
	if (!headers_sent()) header ("HTTP/1.0 404 Not Found");
	
	include '404.php';
	exit;	
	
}




//////////////// GET JSON //////////////

function get_json ()
{
    $json = file_get_contents("php://input");
    return json_decode($json);
}



////////////// COUNT IN ENGLISH //////////

function count_in_english($_count = 0, $_singular = 'item', $_plural = 'items') 
{
        switch ($_count) {
            case 0:
                return 'No '.$_plural;
                break;

            case 1:
                return '1 '.$_singular;
                break;

            default:
                return $_count . ' ' . $_plural;
                break;
        }
}

?>
