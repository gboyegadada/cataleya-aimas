<?php 
 
// if (!defined('IS_ADMIN_FLAG')) {
//   die('Illegal Access');
// }


///////////////////  CUSTOM SESSION SHELF LIFE //////////////
if (IS_ADMIN_FLAG === true) {
	if (!$SESS_LIFE = (SESSION_EXP > 900 ? 900 : SESSION_EXP)) $SESS_LIFE = (SESSION_EXP > 900 ? 900 : SESSION_EXP);
} else {
	if (!$SESS_LIFE = get_cfg_var('session.gc_maxlifetime')) $SESS_LIFE = 1440;
}




/** 
* Open function is the constructor, it prepares the session connection 
* 
* @param string $savePath The storage location of the session. Unnecessary for our operations 
* @param string $sessionId The ID of the session, unnecessary in the constructor for us 
* @return bool 
*/ 
function open( $savePath, $sessionID ) 
{ 
	return true;

} 
 
/** 
* The close function destroys the open database connection. 
* 
* This function is unnecessary for our purposes, since open database connections are killed automatically by PHP at the end of scripts. 
* However, to be explicit I'm including the proper contents here. Note that this function MUST EXIST, but could easily be empty. 
* 
* @return bool 
*/ 
function close( ) 
{ 
  return true; 
} 


/** 
* The read function is what actually loads the session from the database. 
* 
* @param string $sessionID The 32-character session ID, from $_COOKIE['PHPSESSID'] 
* @return string 
*/ 
function read( $sessionID ) 
{ 
	
  $dbh = Cataleya\Helper\DBH::getInstance();
	
  static $read_select, $param_sesskey;
	
  // prepare select statement...
  if (!isset($read_select)) {
  $read_select = $dbh->prepare('SELECT value FROM sessions WHERE sesskey = :sesskey LIMIT 1'); 
  $read_select->bindParam(':sesskey', $param_sesskey, PDO::PARAM_STR);
  }
  
  $param_sesskey = $sessionID;
  
  // execute...
  if (!$read_select->execute()) return '';
  //if the query succeeded... 
  $sess_data = $read_select->fetch(PDO::FETCH_ASSOC);
  
  if ( !empty($sess_data) ) return Cataleya\Helper\Bcrypt::decrypt($sess_data['value']); 
  else return ''; 
} 

/** 
* The write function writes the existing session data to the database AND UPDATES the most recent timestamp 
* 
* @param string $sessionID The 32-character session ID, from $_COOKIE['PHPSESSID'] 
* @param string $sessionData The serialized contents of the session 
* @return bool 
*/ 
function write( $sessionID, $sessionData ) 
{ 
  $dbh = Cataleya\Helper\DBH::getInstance();
	
  static $sess_insert, $param_sesskey, $param_value;
	
  // Build the SQL statement.  Note that since we have the session ID set up as a primary key, we can use the INSERT ... ON DUPLICATE KEY UPDATE syntax: 
  if (empty($sess_insert)) {
	$sess_insert = $dbh->prepare('INSERT INTO sessions (sesskey, value, expiry) VALUES (:sesskey, :value, NOW() ) ' . 
    'ON DUPLICATE KEY UPDATE value = :value, expiry = NOW()');
	$sess_insert->bindParam(':sesskey', $param_sesskey, PDO::PARAM_STR);
 	$sess_insert->bindParam(':value', $param_value, PDO::PARAM_STR);
  }
  
  // ser params
  $param_sesskey = $sessionID;
  $param_value = Cataleya\Helper\Bcrypt::encrypt($sessionData);
  
  // execute the query.  If successful, return true 
  if ($sess_insert->execute()) return true;
  else return false;

} 

/** 
* Destroys the session ID passed in whenever session_destroy() is called by your scripts 
* 
* @param string $sessionID The 32-character session ID, from $_COOKIE['PHPSESSID'] 
* @return bool 
*/ 
function destroy( $sessionID ) 
{ 
  $dbh = Cataleya\Helper\DBH::getInstance();
	
  static $sess_delete, $param_sesskey;
	
  // prepare delete statement...
  if (empty($sess_delete)) {
  $sess_delete = $dbh->prepare('DELETE FROM sessions WHERE sesskey = :sesskey');
  $sess_delete->bindParam(':sesskey', $param_sesskey, PDO::PARAM_STR);
  }
 
 
  // set params
  $param_sesskey = $sessionID;
  
  // execute the query.  If successful, return true 
  if ($sess_delete->execute()) return true;
  else return false;
 
} 

/** 
* Garbage collection happens at random, and destroys ALL sessions older than the lifetime passed in (in seconds) 
* 
* @param int $sessionMaxLifetime The maximum lifetime (in seconds) of sessions in your storage engine 
* @return bool 
*/ 
function garbageCollect( $sessionMaxLifetime ) 
{ 
  // Use the existing database connection created by open() 
  $dbh = Cataleya\Helper\DBH::getInstance();
	
  global $SESS_LIFE; 
  static $sess_delete, $param_sess_max_lifetime;
  
  
  
  // prepare delete statement...
  if (empty($sess_delete)) {
  $sess_delete = $dbh->prepare('DELETE FROM sessions WHERE expiry < DATE_SUB( NOW(), INTERVAL :sess_max_lifetime second )');
  $sess_delete->bindParam(':sess_max_lifetime', $param_sess_max_lifetime, PDO::PARAM_INT);
  }
 
 
  // set params (using our own custom session expiry time...
  $param_sess_max_lifetime = isset($SESS_LIFE) ? $SESS_LIFE : $sessionMaxLifetime;
  
  // execute the query.  If successful, return true 
  if ($sess_delete->execute()) return true;
  else return false;


} 



// once all the functions are delcared in the global scope, we can initiate the session handling in PHP: 
session_set_save_handler( 'open', 'close', 'read', 'write', 'destroy', 'garbageCollect' ); 
//register_shutdown_function("session_write_close");



// MISC SESSION FUNCS ////////////


// Getting rid of session cookies properly...
function kill_session($session_name = '') {

	$params = session_get_cookie_params();
	setcookie(session_name($session_name), '', time() - 42000,
		$params["path"], $params["domain"],
		$params["secure"], $params["httponly"]
	);
        
        session_unset();
	session_destroy();
}				



// Set strict session cookie

function init_session_cookie($_options = array ()) 
{
    $_options['path'] = !isset($_options['path']) ? '/' : $_options['path'];
    
    $_sub_domain = (defined('DEV_ENV') && DEV_ENV) ? 'dev.' : 'www.';
    session_set_cookie_params ( 0, $_options['path'], ($_sub_domain.DOMAIN), SSL_REQUIRED, TRUE );
    
}





?>
