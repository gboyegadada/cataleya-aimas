<?php

/*

SWIFT INIT

*/


// SWIFT MAILER
require_once (ROOT_PATH.'library/swiftmailer/lib/swift_required.php');


Swift::init(function () {
	
	Swift_DependencyContainer::getInstance()->register('mime.qpcontentencoder')->asAliasOf('mime.nativeqpcontentencoder');

});


?>