<?php


/*
 *
 *  CATALEYA::INIT
 *
 *
 *
 *
 */
 
 
 
//Load Cataleya utility class
require ROOT_PATH . 'app/models/Cataleya.php';

//Start the autoloader
Cataleya::registerAutoload();





 

///////////////////  REGISTER SESSION HANDLER //////////////

if (IS_ADMIN_FLAG === true) {
        if (!$SESS_LIFE = (SESSION_EXP > 3600 ? 900 : SESSION_EXP)) $SESS_LIFE = (SESSION_EXP > 3600 ? 900 : SESSION_EXP);
} else {
        if (!$SESS_LIFE = get_cfg_var('session.gc_maxlifetime')) $SESS_LIFE = SESSION_EXP; //1440;
}

if (defined('DISABLE_SECURE_SESSION') && DISABLE_SECURE_SESSION) { /* don't load session class */ }
else {
    Cataleya\Session::setSessionLifeTime($SESS_LIFE);
    Cataleya\Session::load();
}
