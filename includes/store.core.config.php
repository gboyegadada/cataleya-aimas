<?php

// if (!defined ('IS_ADMIN_FLAG') || IS_ADMIN_FLAG === TRUE) die("Illegal Access");






/**
 * 
 * PRODUCTION MODE
 * ----------------------------------------
 * 
 * DEBUG: FALSE
 * 
 */


/**
 * Determine if SSL is used.
 *
 * @since 2.6.0
 *
 * @return bool True if SSL, false if not used.
 */

function _is_ssl() {
	if ( isset($_SERVER['HTTPS']) ) {
		if ( 'on' == strtolower($_SERVER['HTTPS']) )
			return true;
		if ( '1' == $_SERVER['HTTPS'] )
			return true;
	} elseif ( isset($_SERVER['SERVER_PORT']) && ( '443' == $_SERVER['SERVER_PORT'] ) ) {
		return true;
	}
	return false;
}


if (!defined('SSL_REQUIRED')) 
{
    
	if (_is_ssl()) define('SSL_REQUIRED', TRUE); 
        else define('SSL_REQUIRED', FALSE);
        
}

/**
 * debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use DEBUG_MODE
 * in their development environments.
 */


 
define('DEBUG_MODE', TRUE);


// OTHER GLOBAL STUFF
define ('DEFAULT_TIMEZONE', 'Europe/London');
date_default_timezone_set(DEFAULT_TIMEZONE);

define ('CURRENT_DT', time());
define('IP_TREND_THRESHOLD', 10);
define('BROWSER_TREND_THRESHOLD', 4);
define('GEO_LOC_TREND_THRESHOLD', 3);


// MAILCHIMP CREDENTIALS 

define('MAILCHIMP_API_KEY', '909349dd7249f3b4d2522cbd5c6ddc52-us3');
define('MAILCHIMP_LIST_ID', '2c3cf7fca1');


// Get a key from https://www.google.com/recaptcha/admin/create
define('RECAPTCHA_PUBLIC_KEY', "6LexnwoTAAAAABKgh5UPPQZjsdBSpt0_L15r8hSY");
define('RECAPTCHA_PRIVATE_KEY', "6LexnwoTAAAAABs9tbZW7RcHNZCi1CDSZORNc0XV");



// PATHS...
// $_abs_path = getcwd() . '/'; // preg_replace('!conf[\\\|/]?$!', '', dirname(__FILE__));
// $_abs_path_shared = $_abs_path;

if (!defined('ROOT_PATH')) define ('ROOT_PATH', getcwd() . '/');
define ('INC_PATH', ROOT_PATH.'includes/');
define ('SHARED_INC_PATH', ROOT_PATH.'includes/');
define ('ADMIN_INC_PATH', ROOT_PATH.'october/includes/');
define('LIB_PATH', ROOT_PATH.'library/');
define('CAT_DIR', ROOT_PATH.'ui/images/catalog/');
define('CATALEYA_LOGS_PATH' , ROOT_PATH.'var/logs');
define('CATALEYA_CACHE_PATH', ROOT_PATH.'var/cache');
define('CATALEYA_CACHE_DIR', ROOT_PATH.'var/cache');
define('CATALEYA_PLUGINS_PATH', ROOT_PATH.'app/plugins');
define('PLUGIN_PACKAGES_PATH', ROOT_PATH.'var/packages');

define('PATH_SKIN', ROOT_PATH.'skin');

define('APP_STORE_ENDPOINT', 'http://dev.fancypaperplanes.com/appstore/');
define('CHECKOUT_RETURN_URL', 'http://dev.fancypaperplanes.com/appstore/');


/////////// Cataleya INIT ///////
// require_once (SHARED_INC_PATH.'cataleya.init.php');	
//Load Cataleya utility class
require ROOT_PATH . 'app/core/Cataleya/Autoloader.php';

//Start the autoloader
Cataleya\Autoloader::registerAutoload();


//$store_core_config = Cataleya\System\Config::load('store.core');
//$store_core_config->shopLanding = '';


define('DASH_LANGUAGE_CODE', Cataleya\System::load()->getDashboardLanguageCode());
define('SHOPFRONT_LANGUAGE_CODE', Cataleya\System::load()->getShopFrontLanguageCode());
     
 





/////////// MailChimp INIT ///////
//require_once (SHARED_INC_PATH.'mailchimp.init.php');	
require_once (ROOT_PATH.'library/MailChimp/MailChimp.class.php');



/////////// Twig INIT ///////
//require_once (SHARED_INC_PATH.'twig.init.php');
require_once (ROOT_PATH.'library/Twig/Autoloader.php');
Twig_Autoloader::register();

define('TEMPLATES_PATH', (ROOT_PATH.'skin/ShopFront/'));


/////////// HTMLPurifier INIT ///////
//require_once (SHARED_INC_PATH.'HTMLPurifier.init.php');	
require_once (ROOT_PATH . 'library/HTMLPurifier/HTMLPurifier.auto.php');
require_once 'HTMLPurifier.func.php';




// SWIFT MAILER
// require_once (SHARED_INC_PATH.'swiftmailer.init.php');
require_once (ROOT_PATH.'library/swiftmailer/lib/swift_required.php');
Swift::init(function () {
	
	Swift_DependencyContainer::getInstance()->register('mime.qpcontentencoder')->asAliasOf('mime.nativeqpcontentencoder');

});







// check if cataleya is setup
$_dbh = Cataleya\Helper\DBH::getInstance();


if ($_dbh === NULL) 
{
    Header('Location: ' . (SSL_REQUIRED ? 'https://' : 'http://') . $_SERVER['HTTP_HOST']);
    exit('<center><h2>Redirecting...</h2></center> ');
}


$Core = Cataleya\Core::getInstance();

// URLs //////////////////////////////
define('HOST', $Core->config->host);
define('DOMAIN', HOST);
define('EMAIL_HOST', Cataleya\System\Config::load('mailer.config')->localhost);



define('ROOT_URI', $Core->config->rootURI);
define('ADMIN_ROOT_URI', $Core->config->adminRootURI);
define('SHOP_ROOT', ((SSL_REQUIRED || _is_ssl()) ? 'https://' : 'http://'). HOST . '/' . ROOT_URI);
define('BASE_URL', ((SSL_REQUIRED || _is_ssl()) ? 'https://' : 'http://'). HOST . '/' . ROOT_URI);
define('SECURE_BASE_URL', 'https://' . HOST . '/' . ROOT_URI);
define('TEMPLATES_WEB_PATH', (BASE_URL.'skin/ShopFront/'));




// MORE CONSTANTS //

//define('LOGIN_EXPIRES_AFTER', (60*60)); // After 60 min...

define('COOKIE_PREFIX', 'AIMAS_');
define('COOKIE_DOMAIN', HOST);
define('COOKIE_PATH', '/');
define('COOKIE_SECURE', FALSE);
define('COOKIE_EXPIRES', CURRENT_DT+(360*24*60*60)); // a year

define('RELOGIN_COOKIE_EXPIRES', CURRENT_DT+(7*24*60*60)); // a week

define('DB_CACHE_EXP', CURRENT_DT-(60*60)); // After 60 min...






// Check if user is authorized to view the store...

define('AUTHORIZED_SHOPPER', (!empty($_COOKIE['_a']) && Cataleya\Admin\User::validateStoreKey($_COOKIE['_a'])));


// Remove cookie (if any)
if (!AUTHORIZED_SHOPPER && isset($_COOKIE['_a'])) {
        
        setcookie(
            '_a', // Name
            '', // Value
            time()-3600, // Expires
            '/', // Path
            ('.'.DOMAIN), // Domain
            FALSE, // Is secure
            TRUE // HTTP Only
            );

}








/////////////////// REGISTER LISTENERS AND HOOKS ///////////

$_Notifications = Cataleya\Admin\Notifications::load();

foreach ($_Notifications as $_Notification) {
    Cataleya\System\Event::addListener($_Notification);
}


$_WebHooks = Cataleya\Front\WebHooks::load();

foreach ($_WebHooks as $_WebHook) {
    Cataleya\System\Event::addListener($_WebHook);
}

// Register LOGGER(S)...
Cataleya\System\Event::addListener(Cataleya\Helper\Logger::create('system/error'));
Cataleya\System\Event::addListener(Cataleya\Helper\Logger::create('mail/error'));
Cataleya\System\Event::addListener(Cataleya\Helper\Logger::create('session/error'));



// FETCH ERROR FUNCTIONS...
require_once (INC_PATH.'functions/error_funcs.php');


