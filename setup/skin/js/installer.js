var Installer, Validator;


(function ($) {
    $('document').ready(function() {

  

        Installer = function ($) {

            var toDoList = [ 
                {text:'Checking folder permissions...', action:'folders.setup.ajax.php', template: 'folder-perms', dialog:'#folder-perms-dialog-box', dialogShown:false, form:$('#folder-perms-form')}, 
                {text:'Installing database settings...', action:'db.setup.ajax.php', dialog:'#db-settings-dialog-box', dialogShown:false, form:$('#db-settings-form') }, 
                {text:'Installing database tables...', action:'tables.setup.ajax.php'}, 
                {text:'Installing languages...', action:'languages.setup.ajax.php'}, 
                {text:'Installing countries...', action:'countries.setup.ajax.php'}, 
                {text:'Installing currencies...', action:'currencies.setup.ajax.php'}, 
                {text:'Installing application settings...', action:'app-settings.setup.ajax.php'}, 
                {text:'Installing configurations...', action:'configurations.setup.ajax.php'}, 
                {text:'Registering system events...', action:'events.setup.ajax.php'}, 
                {text:'Installing user permissions...', action:'privileges.setup.ajax.php'}, 
                {text:'Installing admin notifications...', action:'notifications.setup.ajax.php'}, 
                {text:'Installing administrator profile...', action:'admin-profile.setup.ajax.php', dialog:'#admin-profile-dialog-box', dialogShown:false, form:$('#admin-profile-form') }, 
                {text:'Initializing stores...', action:'stores.setup.ajax.php' }, 
                {text:'Installing bundled apps...', action:'plugins.setup.ajax.php' }, 
                {text:'Installing demo...', action:'demo.setup.ajax.php' }


            ];

            var _current = 0;
            var _token = $('#meta-token').val();
            var _setup_dir = $('#meta-root-url').val();

            var query = {
                token: _token
            };
            
            var _template_cache = {}, _ids = [];
            
            
            
            // Load templates...
            
            for (var i in toDoList) {
                        if (typeof toDoList[i]['template'] === 'undefined') continue;
                        
                        var k = toDoList[i]['template'];
                        _ids.push(k);
                
                        
                        $.ajax ({
                                type: 'POST', 
                                url: _setup_dir+'get-mustache.ajax.php', 
                                data: { id: _ids, token: _token }, 
                                success: function (data) {

                                            for (var name in data.templates) _template_cache[name] = data.templates[name];
                                            
                                            query.token = _token = data.token;
                                            return false;

                                },

                        error: function (xhr) {
                            alert('Error', xhr.responseText);
                            return false;
                        }
                            });
                
                
                        
                        

                    }
                    
                    
        
        



            function initSetup () {
                $('#start-screen-wrapper').hide();
                $('#installer-wrapper').show();
                makeRequest(query);
            }


            function makeRequest (query) {
                
                if (_current >= toDoList.length) {
                    _current = 0;
                    
                    setTimeout (function () {
                            $('#installer-wrapper').fadeOut('fast');
                            $('#complete-screen-wrapper').fadeIn('fast');  
                    }, 2000);

                    return false;
                }
                var action = _setup_dir+toDoList[_current].action;

                $.ajax ({
                        type: 'POST', 
                        url: action, 
                        data: query, 
                        dataType: 'json', 
                        beforeSend: function () {
                            $('#installer-status-text').html(toDoList[_current].text);
                            $('#installer-console').prepend('<li>' + toDoList[_current].text + '</li>');
                        }, 
                        success: function (data) {

                                switch (data.status) {
                                    case 'installed':

                                        $('#installer-status-text').html(data.message);
                                        $('#installer-console').prepend('<li>' + data.message + '</li>');
                                        
                                        var _wp = (1/toDoList.length)*600;
                                        $('#installer-status-bar').animate({width: '+='+_wp}, 200);

                                        ++_current;
                                        makeRequest({token: data.token});
                                        break;

                                    case 'dialog':
                                        toDoList[_current].dialogShown = true;
                                        var dialog, destroy = false;
                                        
                                        if (typeof toDoList[_current]['template'] !== 'undefined') {
                                            var k = toDoList[_current]['template'];
                                            dialog = $(Mustache.render(_template_cache[k], { result: data.result }));
                                            $('body').append(dialog);
                                            destroy = true;
                                        } else {
                                            dialog = $(toDoList[_current].dialog);
                                        }
                                        
                                        dialog
                                        .find('.response')
                                        .html(data.message)
                                        .end()
                                        .find('.ok-button')
                                        .one('click', function () {
                                           dialog.hide();

                                           var _query = toDoList[_current].form.serialize()
                                           makeRequest(_query + '&token=' + data.token);
                                           if (destroy) dialog.remove();
                                        })
                                        .end()
                                        .show();
                                        
                                        if (typeof data.failedItems != 'undefined' && data.failedItems.length > 0) {
                                            var field = $('input[name=' + data.failedItems[0] + ']') ;
                                            if (field.length == 0) field = $('select[name=' + data.failedItems[0] + ']') ;

                                            if (field.length > 0) {
                                                    showTip(field);
                                                    $(field)
                                                    .focus()
                                                    .one('blur', function () { hideTip(); });
                                            }
                                        }
                                    
                                        break;

                                    case 'error':
                                        alert('Error', data.message); // "There's been an error please reload this page to restart setup.");
                                        break;

                                    default:
                                        alert('Error', "There's been an error please reload this page to restart setup.");
                                        break;

                                }

                                return false;

                                },

                        error: function (xhr) {
                            alert('Error', xhr.responseText);
                            return false;
                        }
                });

                return false;

            }


            return {
                initSetup: initSetup
            };

        }($);
        
        
        
        Validator = Cataleya.newValidator();
        
        // Password meter
        Validator.newPasswordMeter('text-field-admin-pass1', 'password-meter-1');
        
        
        
        
        
        $('#console-button').click (function () {
           if ($('#installer-console-wrapper').hasClass('hide')) {
               $('#installer-console-wrapper').removeClass('hide');
               $(this).html('hide details');
           } else {
               $('#installer-console-wrapper').addClass('hide');
               $(this).html('show details');
           }
        });
        
        
        
        $('#installer-init-button').click(function () {
            Installer.initSetup();
            
        });



});

})(jQuery);
