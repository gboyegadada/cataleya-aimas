<?php




////////// APPLICATION HEAD //////////
require_once './includes/app.config.php';	



/*
 * 
 * CREATE ADMIN USER-1 (SYSTEM ADMINISTRATOR)
 * 
 * 
 

$options = array (
                    'firstname' => 'Gboyega', 
                    'lastname' => 'Dada'
                    );

$ADMIN_USER = Cataleya\Admin\User::create('boyega@gmail.com', 'tell dem d k0k0', $ROLE_ADMINISTRATOR, $options);

*/









?>


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<base href="<?=ADMIN_BASE_URL?>" />
<?php
include INC_PATH."meta.php";

?>

<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Cataleya | Setup</title>


<!-- JS FILES -->
<script type="text/javascript" src="ui/jscript/libs/jquery.min.js"></script>
<script type="text/javascript" src="ui/jscript/libs/jquery-ui.custom.min.js"></script>
<script type="text/javascript" src="ui/jscript/hashgrid.js"></script>
<script type="text/javascript" src="ui/jscript/libs/toolbox.js"></script> 
<script type="text/javascript" src="ui/jscript/libs/jfingerprint.js"></script> 
<script type="text/javascript" src="ui/jscript/libs/math.uuid.js"></script>
<script type="text/javascript" src="ui/jscript/libs/mustache.js"></script> 
<script type="text/javascript" src="ui/jscript/libs/shortcut.js"></script>
<script type="text/javascript" src="../setup/get-js-classes.php"></script> 

<!-- STYLE SHEETS -->
<?php
require_once(INC_PATH.'css_libs.php');
?>

<script type="text/javascript" src="../setup/skin/js/installer.js"></script>
<script type="text/javascript">


</script>



<style type="text/css" media="screen">
    .content {
        height: 800px;
    }
    
    #footer {
        border-top: dotted 1px #046;
    }
    
    
    #start-screen-wrapper {
        display: block;
        width: 700px;
        height: auto;
        
        position: absolute;
        top: 200px;
        left: 120px;
        
        text-align: center;
        
        border: none;
    }
    
    
    #complete-screen-wrapper {
        display: none;
        width: 700px;
        height: auto;
        
        position: absolute;
        top: 200px;
        left: 120px;
        
        text-align: center;
        
        border: none;
    }
    

    
    .welcome-message {
        margin-bottom: 20px;
        font-family: 'CenturyGothicRegular', verdana, arial; 
    }
    
    
    
    
    

    
    #installer-wrapper {
        display: none;
        width: 700px;
        height: auto;
        
        position: absolute;
        top: 200px;
        left: 120px;
        
        text-align: left;
        
        border: none;
    }
    
    #installer-status-header {
        width: 600px;
        height: auto;
        font-family: 'CenturyGothicRegular', verdana, arial; 
        
        font-size: 170%;
        font-weight: normal;
        display: block;
        text-align: left;
        
        padding: 0px;
        margin: 5px auto;
    }
    
    
    #installer-status-bar-wrapper {
        height: 5px;
        width: 600px;
        margin: 5px auto;
        
        border: solid 1px #555;
        background: none;
        
        overflow: hidden;
        
	border-radius:2px;	
	-moz-border-radius:2px;	
	-webkit-border-radius:2px;
	
    }
    
    
    #installer-status-bar {
        height: 5px;
        width: 0px;
        
        border: none;
        background-image: url('./ui/images/progress.gif');
        background-repeat: repeat-x;
        
	border-radius:2px;	
	-moz-border-radius:2px;	
	-webkit-border-radius:2px;
	
    }
    
    #installer-status-text {
        color: #880;
        font-size: 70%;
        
        height: auto;
        width: 600px;
        margin: 0px auto 40px auto;
    }
    
    
    #installer-console-wrapper {
        height: 300px;
        width: 600px;
        margin: 5px auto 20px auto;
        
        border: solid 2px #333;
        background: #000;
        
	border-radius:10px;	
	-moz-border-radius:10px;	
	-webkit-border-radius:10px;
    }
    
    #installer-console {
        list-style: none;
        padding: 0px;
        
        color: #880;
        font-size: 70%;
        
        height: 280px;
        width: 540px;
        margin: 15px auto;
        
        overflow-x: hidden;
        overflow-y: auto;
    }
    
    #installer-console > li {
        list-style: none;
    }
    
    #installer-console > li:before {
        content: '--';
        margin-right: 5px;
    }
    
    
    
    #console-button-wrapper {
        height: auto;
        width: 600px;
        margin: 5px auto 10px auto;
        
        text-align: left;
    }
    
    #console-button {
        font-size: 80%;
        padding: 2px 16px;
    }
    
    
    .hide {
        display: none;
    }
    
    
    
    
    
    


/*

ADMIN PROFILE DIALOG 

*/



#admin-profile-dialog-box {
    display:none; 
    position:fixed; 
    top:20px; 
    left:370px; 
    width:550px; 
    height:auto; 
    background-color:#111; 
    color:#bbb; 
    z-index:10001;
}


#admin-profile-dialog-box > .button-1 {
    font-size: 90%;
}



.dialog-box-section-label {
    width: 250px;
    border-bottom: dotted 1px #660;
    
    font-size: 80%;
    text-align: left;
    text-transform: uppercase;
    color: #555;
    
    text-indent: 6px;
    padding-bottom: 3px;
    margin-bottom: 6px;
    
}


#admin-profile-status-wrapper {
    margin-left: 15px;
}
/* bring message bubble forward */

.message-bubble {
    z-index:10002;
}





#text-field-admin-profile-name, 
#text-field-admin-profile-description {
    width: 95%;
}


#password-meter-1 {
    display: none;
    float: right;
    margin-top: 4px;
}
    





/*

DB SETTINGS DIALOG 

*/


#db-settings-dialog-box {
    display: none; 
    position:fixed; 
    top:20px; 
    left:490px; 
    width:350px; 
    height:auto; 
    background-color:#111; 
    color:#bbb; 
    z-index:10001;
}


#db-settings-dialog-box > .button-1 {
    font-size: 90%;
}



#db-settings-type-label {
    width: 320px;
    border-bottom: dotted 1px #660;
    
    font-size: 80%;
    text-align: left;
    text-transform: uppercase;
    color: #555;
    
    text-indent: 6px;
    padding-bottom: 3px;
    margin-bottom: 6px;
    
}






/*

folder perms DIALOG 

*/


#folder-perms-dialog-box {
    display: none; 
    position:fixed; 
    top:20px; 
    left:490px; 
    width:350px; 
    height:auto; 
    background-color:#111; 
    color:#bbb; 
    z-index:10001;
}


#folder-perms-dialog-box > .button-1 {
    font-size: 90%;
}



#folder-perms-type-label {
    width: 320px;
    border-bottom: dotted 1px #660;
    
    font-size: 80%;
    text-align: left;
    text-transform: uppercase;
    color: #555;
    
    text-indent: 6px;
    padding-bottom: 3px;
    margin-bottom: 6px;
    
}


.alert-box .bubble-text ul {
    margin: 20px 0px;
    padding: 10px 20px;
    background-color: #000;
    
	border-radius:7px;	
	-moz-border-radius:7px;	
	-webkit-border-radius:7px;
    
}

.alert-box .bubble-text ul > li {
    color: #01a31c;
    list-style-position: inside;
}

.alert-box .bubble-text ul > li.error {
    color: #f33;
}


/* bring message bubble forward */

.message-bubble {
    z-index:10002;
}


    


</style>


</head>

<body>
<!-- BEGIN: META -->
<input type="hidden" id="meta-token" value="<?=NEW_REQ_TOKEN?>"  />
<input type="hidden" id="meta-root-url" value="<?=BASE_URL?>"  />
<!-- END: META -->


<div class="container">
    
<div class="header-wrapper absolute">
    <div class="header">

    <div id="page-title">Cataleya Setup</div>   

    </div>
    
    

    
</div>
    
<div class="content">
  

    <div id="start-screen-wrapper">
        <div class="welcome-message">Welcome, to begin installing your new shopping cart, click the install button.</div>
        <input type="button" value="install" id="installer-init-button" class="blue-button xx-large" /> 
    </div>
    
    
    
    <div id="installer-wrapper">
        <h2 id="installer-status-header">Installing...</h2>
        <div id="installer-status-bar-wrapper">
            <div id="installer-status-bar"></div>
        </div>
        <div id="installer-status-text"></div>
        
        <div id="console-button-wrapper">
            <a id="console-button" href="javascript:void(0);" class=" button-3">show details</a>
        </div>
        <div id="installer-console-wrapper" class="hide">
            
            <ul id="installer-console">
                <!-- <li>Installing application settings...</li> -->
            </ul>
        </div>
    </div>
    
    
    

    <div id="complete-screen-wrapper">
        <div class="welcome-message">Your new shopping cart is installed and ready for use! Just click the login button below to sign in!</div>
        
        <form method="GET" action="<?=ADMIN_BASE_URL?>login.php" >
        <input type="submit" value="login" id="login-init-button" class="blue-button xx-large" />  
        </form>
    </div>
    

    
<div id="footer">
&nbsp;
</div>

</div>
</div>



<?php
     include_once 'includes/admin-profile-dialog-box.php';
     include_once 'includes/db-settings-dialog-box.php';

?>




</body>
</html>
