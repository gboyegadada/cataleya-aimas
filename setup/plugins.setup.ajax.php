<?php


/////// PAYMENT TYPES ////////////////////
define('OUTPUT', 'JSON');



////////// APP TOP //////////
require_once './includes/app.config.php';	







Cataleya\Helper::startTimer('install');

$_dbh = Cataleya\Helper\DBH::getInstance();

// ------------------------- DEMOS -------------------------------------------- //


$_project_info = file_exists(ROOT_PATH . 'project.info.json') 
        ? json_decode(file_get_contents(ROOT_PATH . 'project.info.json', FILE_IGNORE_NEW_LINES)) 
        : NULL;


if ($_project_info !== null && isset($_project_info->{'apps'})) {

    foreach ($_project_info->apps as $_app_handle) {
        if (\Cataleya\Plugins\Package::exists($_app_handle)) continue;

        \Cataleya\Plugins\Package::install($_app_handle);

    }


}



$_how_long = Cataleya\Helper::stopTimer('install');



// tables installed
$_json_reply = array (

'status' => 'installed', 
'message' => ('Bundled apps installed in - ' . $_how_long . ' seconds'), 
'time' => $_how_long, 
'token' => NEW_REQ_TOKEN
);

echo json_encode($_json_reply);
exit();


