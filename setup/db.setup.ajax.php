<?php


//// DB SETTINGS /////////
define('OUTPUT', 'JSON');

////////// CONFIG //////////
require_once './includes/app.config.php';





// check if db config is already installed
$_dbh = Cataleya\Helper\DBH::getInstance();


if ($_dbh !== NULL) 
{

    // config already installed
    $_json_reply = array (

    'status' => 'installed', 
    'message' => 'Database settings already installed.', 
    'time' => 0, 
    'token' =>  NEW_REQ_TOKEN
    );

    echo json_encode($_json_reply);
    exit();

}





// db settings not installed

// check request data

// show dialog
if (!isset($_POST['token'], $_POST['dbname'], $_POST['host'], $_POST['user'], $_POST['pass'])) 
{

    $_json_reply = array (

    'status' => 'dialog', 
    'message' => 'Please enter your database settings', 
    'token' =>  NEW_REQ_TOKEN
    );
    
    echo json_encode($_json_reply);
    exit();

}



// IF POST: validate and process data
else 
{

    
$_db_config = array (

    'dbname' => filter_var($_POST['dbname'], FILTER_VALIDATE_REGEXP, array('options'=>array('regexp' => '/^([A-Za-z0-9_\-]{1,50})$/'))), 
    'host' => filter_var($_POST['host'], FILTER_VALIDATE_REGEXP, array('options'=>array('regexp' => '/^([A-Za-z0-9\-_]+\.?){1,5}[A-Za-z]{2,4}$/'))), 
    'user' => filter_var($_POST['user'], FILTER_VALIDATE_REGEXP, array('options'=>array('regexp' => '/^([A-Za-z0-9_\-]{1,50})$/'))), 
    'pass' => filter_var($_POST['pass'], FILTER_VALIDATE_REGEXP, array('options'=>array('regexp' => '/^([A-Za-z0-9\-_!\*]{1,50})$/')))

);




// Error / Response
$_errors = array ();

foreach ($_db_config as $k=>$v) 
{
    if ($v === FALSE) $_errors[] = $k;
}

// validate params...
if ( !empty($_errors) ) 
{
    $_json_reply = array (

    'status' => 'dialog', 
    'message' => 'One or more fields are missing or invalid. Please re-enter your database settings.', 
    'token' =>  NEW_REQ_TOKEN, 
    'failedValidation' => TRUE, 
    'failedItems'  => $_errors, 
    );
    
    echo json_encode($_json_reply);
    exit();
}

}




// Validation passed
// check connection settings...
try { 

        $_dbh = new PDO("mysql:host=".$_db_config['host'].";dbname=".$_db_config['dbname'], $_db_config['user'], $_db_config['pass']);

        $_dbh = TRUE;

    } catch (PDOException $e) {

        $_dbh = FALSE;


    }


if ($_dbh === TRUE) 
{

// connection settings valid
// save


Cataleya\Helper::writeConfig($_db_config, 'rdc');

    // echo success
    $_json_reply = array (

    'status' => 'installed', 
    'message' => 'Database settings installed.', 
    'time' => 0, 
    'token' =>  NEW_REQ_TOKEN
    );

    echo json_encode($_json_reply);
    exit();


} else { 

    // invalid db settings; show dialog.
    $_json_reply = array (

    'status' => 'dialog', 
    'message' => 'Please enter your database settings', 
    'token' =>  NEW_REQ_TOKEN
    );
    
    echo json_encode($_json_reply);
    exit();


}








