<?php


/////// NOTIFICATIONS ////////////////////
define('OUTPUT', 'JSON');


////////// APP TOP //////////
require_once './includes/app.config.php';	







if (Cataleya\Admin\Notification::load('order.new') !== NULL) 
{

    // config already installed
    $_json_reply = array (

    'status' => 'installed', 
    'message' => 'Notifications already installed.', 
    'time' => 0, 
    'token' =>  NEW_REQ_TOKEN
    );

    echo json_encode($_json_reply);
    exit();

}





Cataleya\Helper::startTimer('install');

$_dbh = Cataleya\Helper\DBH::getInstance();
$_dbh->beginTransaction();






$_notifications_config = array ();


// [1] NEW ORDER

//$_notifications_config[] = array(
//    'keyword'   =>  'order.new', 
//    'language_code' =>  'EN', 
//    'title' =>  'New Order', 
//    'description'   =>  'You have new orders.', 
//    'entity_name'   =>  'New Order', 
//    'entity_name_plural'    =>  'New Orders', 
//    'send_after_period'   =>  1, 
//    'send_after_interval'   =>  'hour', 
//    'send_after_increments'   =>  10, 
//    'notification_text' =>  'waiting to be processed.',  // "1 new order waiting to be processed."
//    'icon'   =>  'new.order.icon.png', 
//    'request_uri'   =>  ADMIN_BASE_URL.'landing.orders.php?show=new', 
//    
//);



// [2] STOCK LOW

$_notifications_config[] = array(
    'keyword'   =>  'stock.low',  
    'scope'   => 'stock/low', 
    'language_code' =>  'EN', 
    'title' =>  'Low Stock', 
    'description'   =>  'Product stock is low.', 
    'entity_name'   =>  'Product', 
    'entity_name_plural'    =>  'Products', 
    'send_after_period'   =>  1, 
    'send_after_interval'   =>  'hour', 
    'send_after_increments'   =>  10, 
    'notification_text' =>  'almost out of stock.',  // "1 product almost out of stock."
    'icon'   =>  'flag-4-50x50.png', 
    'request_uri'   =>  ADMIN_BASE_URL.'landing.catalog.php?show=low', 
    
);



// [3] OUT OF STOCK

$_notifications_config[] = array(
    'keyword'   =>  'stock.out', 
    'scope'   => 'stock/out', 
    'language_code' =>  'EN', 
    'title' =>  'Products out of stock.', 
    'description'   =>  'Some products out of stock.', 
    'entity_name'   =>  'Product', 
    'entity_name_plural'    =>  'Products', 
    'send_after_period'   =>  1, 
    'send_after_interval'   =>  'hour', 
    'send_after_increments'   =>  5, 
    'notification_text' =>  'out of stock.',  // "1 product out of stock."
    'icon'   =>  'flag-4-50x50.png', 
    'request_uri'   =>  ADMIN_BASE_URL.'landing.catalog.php?show=out', 
    
);



// [4] NEW CUSTOMERS

$_notifications_config[] = array(
    'keyword'   =>  'customer.new', 
    'scope'   => 'customer/created', 
    'language_code' =>  'EN', 
    'title' =>  'New customers.', 
    'description'   =>  'You have new customers.', 
    'entity_name'   =>  'Customer Account', 
    'entity_name_plural'    =>  'Customer Acounts', 
    'send_after_period'   =>  1, 
    'send_after_interval'   =>  'week', 
    'send_after_increments'   =>  10, 
    'notification_text' =>  'created at the store.',  // "1 customer account created at the store"
    'icon'   =>  'user-5-50x50.png', 
    'request_uri'   =>  ADMIN_BASE_URL.'browse.customers.php?ob=4&o=2', // order by: 'date_created', DESC
    
);




// [5] TRANX ERROR

$_notifications_config[] = array(
    'keyword'   =>  'tranx.error', 
    'scope'   => 'payment/error', 
    'language_code' =>  'EN', 
    'title' =>  'Payment Error.', 
    'description'   =>  "There're one or more transaction errors.", 
    'entity_name'   =>  'Payament error', 
    'entity_name_plural'    =>  'Payment errors', 
    'send_after_period'   =>  15, 
    'send_after_interval'   =>  'minute', 
    'send_after_increments'   =>  1, 
    'notification_text' =>  'logged.',  // "1 transaction error logged."
    'icon'   =>  'document-fill-50x50.png', 
    'request_uri'   =>  ADMIN_BASE_URL.'logs.settings.php?id=tranx.error', 
    
);





// [6] FAILED LOGINS

$_notifications_config[] = array(
    'keyword'   =>  'flag.failed.login', 
    'scope'   => 'admin/user/login-failed', 
    'language_code' =>  'EN', 
    'title' =>  'Failed Logins.', 
    'description'   =>  "There's been one or more failed logins.", 
    'entity_name'   =>  'Failed Login', 
    'entity_name_plural'    =>  'Failed Logins', 
    'send_after_period'   =>  3, 
    'send_after_interval'   =>  'day', 
    'send_after_increments'   =>  50, 
    'notification_text' =>  'logged.',  // "1 failed login logged."
    'icon'   =>  'lock-stroke-50x50.png', 
    'request_uri'   =>  ADMIN_BASE_URL.'logs.settings.php?id=flag.failed.login', 
    
);




// [7] ADMIN ACCOUNT DELETED

$_notifications_config[] = array(
    'keyword'   =>  'admin.deleted', 
    'scope'   => 'admin/user/deleted', 
    'language_code' =>  'EN', 
    'title' =>  'Account Deleted', 
    'description'   =>  "An admin acount has just been deleted.", 
    'entity_name'   =>  'Admin Account', 
    'entity_name_plural'    =>  'Admin Accounts', 
    'send_after_period'   =>  1, 
    'send_after_interval'   =>  'minute', 
    'send_after_increments'   =>  1, 
    'notification_text' =>  'deleted.',  // "1 Admin Account deleted."
    'icon'   =>  'user-3-50x50.png', 
    'request_uri'   =>  ADMIN_BASE_URL.'logs.settings.php?id=admin.log', 
    
);



// [8] ADMIN ACCOUNT CREATED

$_notifications_config[] = array(
    'keyword'   =>  'admin.new', 
    'scope'   => 'admin/user/created', 
    'language_code' =>  'EN', 
    'title' =>  'Account Created', 
    'description'   =>  "A new admin acount has just been created.", 
    'entity_name'   =>  'Admin Account', 
    'entity_name_plural'    =>  'Admin Accounts', 
    'send_after_period'   =>  1, 
    'send_after_interval'   =>  'minute', 
    'send_after_increments'   =>  1, 
    'notification_text' =>  'just been created.',  // "1 Admin Account created."
    'icon'   =>  'user-3-50x50.png', 
    'request_uri'   =>  ADMIN_BASE_URL.'logs.settings.php?id=admin.log', 
    
);





// [9] NEW STORE CREATED

$_notifications_config[] = array(
    'keyword'   =>  'store.new', 
    'scope'   => 'store/created', 
    'language_code' =>  'EN', 
    'title' =>  'Store Created', 
    'description'   =>  "A new store has just been created.", 
    'entity_name'   =>  'New Store', 
    'entity_name_plural'    =>  'New Stores', 
    'send_after_period'   =>  1, 
    'send_after_interval'   =>  'minute', 
    'send_after_increments'   =>  1, 
    'notification_text' =>  'created.',  // "1 Store created."
    'icon'   =>  'map-pin-fill-50x50.png', 
    'request_uri'   =>  ADMIN_BASE_URL.'landing.stores.php', 
    
);



// [10] NEW STORE CREATED

$_notifications_config[] = array(
    'keyword'   =>  'store.new', 
    'scope'   => 'store/deleted', 
    'language_code' =>  'EN', 
    'title' =>  'Store Created', 
    'description'   =>  "A store has just been deleted.", 
    'entity_name'   =>  'Store Deleted', 
    'entity_name_plural'    =>  'Stores Deleted', 
    'send_after_period'   =>  1, 
    'send_after_interval'   =>  'minute', 
    'send_after_increments'   =>  1, 
    'notification_text' =>  'created.',  // "1 Store created."
    'icon'   =>  'map-pin-fill-50x50.png', 
    'request_uri'   =>  ADMIN_BASE_URL.'landing.stores.php', 
    
);





foreach ($_notifications_config as $k=>$_note_conf) 
{

    if (Cataleya\Admin\Notification::create($_note_conf) === NULL) Cataleya\Helper\ErrorHandler::getInstance()->triggerException('"' . $_note_conf['title'] . '" notification could be created in: ' . __FILE__ . ' on line: ' . __LINE__);


}









$_dbh->commit();

$_how_long = Cataleya\Helper::stopTimer('install');



// tables installed
$_json_reply = array (

'status' => 'installed', 
'message' => ('Notifications installed in - ' . $_how_long . ' seconds'), 
'time' => $_how_long, 
'token' => NEW_REQ_TOKEN
);

echo json_encode($_json_reply);
exit();

