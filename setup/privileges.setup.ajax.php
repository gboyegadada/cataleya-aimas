<?php


/////// ROLES AND PRIVILEGES ////////////////////
define('OUTPUT', 'JSON');



////////// APP TOP //////////
require_once './includes/app.config.php';	






$_results = Cataleya\Helper\DBH::getInstance()->query('SELECT 1 FROM admin_roles LIMIT 1')->fetchAll(PDO::FETCH_ASSOC);

if (count($_results) > 0)
{

    // already installed
    $_json_reply = array (

    'status' => 'installed', 
    'message' => 'User permissions already installed.', 
    'time' => 0, 
    'token' =>  NEW_REQ_TOKEN
    );

    echo json_encode($_json_reply);
    exit();

}




/*
 * 
 * USEFUL FUNCTIONS
 * 
 * 
 */

function assignPrivileges ($FILE_PATH, $ROLE) 
{
    
    if (!file_exists($FILE_PATH)) _catch_error('Source file admin privileges not found.', __LINE__, true);
    $PRIVILEGES_HANDLE = fopen($FILE_PATH, "r");


    while ($line = fgets($PRIVILEGES_HANDLE, 300)) 
    {
        $line = preg_replace('/^[^A-Za-z_0-9\-]+/', '', $line);
        if (preg_match('/^(--|\n)/', $line) > 0 || $line === '' || $line === "\n") continue;

        $privilege_id = strtoupper(preg_replace('/[^A-Za-z_0-9]/', '', $line));
        // $printable_name = ucwords(strtolower(str_replace('_', ' ', $privilege_id)));

        $ROLE->givePrivilege($privilege_id);

    }

    fclose($PRIVILEGES_HANDLE);
    return TRUE;
    
}









Cataleya\Helper::startTimer('install');

$_dbh = Cataleya\Helper\DBH::getInstance();
$_dbh->beginTransaction();




/*
 * 
 * 
 * CREATE USER ROLES
 * 
 * 
 */


// Administrator
$ROLE_ADMINISTRATOR = Cataleya\Admin\Role::create ('ROOT', 'Administrator', 'Administrator (super user)', 'EN');

// Store Owner
$ROLE_STORE_OWNER = Cataleya\Admin\Role::create ('STORE_OWNER', 'Store Owner', 'Store Owner', 'EN');

// Store Manager
$ROLE_MANAGER = Cataleya\Admin\Role::create ('STORE_MANAGER', 'Store Manager', 'Store Manager', 'EN');

// Store Attendant
$ROLE_ATTENDANT = Cataleya\Admin\Role::create ('ATTENDANT', 'Attendant', 'Store Attendant', 'EN');







/*
 * 
 * SETUP USER PREVILEGES
 * 
 */


if (!file_exists(PATH_ALL_PRIVILEGES)) _catch_error('Source file admin privileges not found.', __LINE__, true);
$PRIVILEGES_HANDLE = fopen(PATH_ALL_PRIVILEGES, "r");

$privileges = array ();

while ($line = fgets($PRIVILEGES_HANDLE, 300)) 
{
    $line = preg_replace('/^[^A-Za-z_0-9\-]+/', '', $line);
    if (preg_match('/^(--|\n)/', $line) > 0 || $line === '' || $line === "\n") continue;
    
    $privilege_id = strtoupper(preg_replace('/[^A-Za-z_0-9]/', '', $line));
    $printable_name = ucwords(strtolower(str_replace('_', ' ', $privilege_id)));
    
    $privileges[] = Cataleya\Admin\Privilege::create($privilege_id, $printable_name, $printable_name, 'EN');
    
}

fclose($PRIVILEGES_HANDLE);







/*
 * 
 * 
 * ASSIGN USER PRIVILEGES TO ROLES
 * 
 * 
*/
 


// Administrator
$ROLE_ADMINISTRATOR->givePrivilege('SUPER_USER');

// Store Owner
assignPrivileges(PATH_PRIVILEGES_STORE_OWNER, $ROLE_STORE_OWNER);

// Store Manager
assignPrivileges(PATH_PRIVILEGES_MANAGER, $ROLE_MANAGER);

// Store Attendant
assignPrivileges(PATH_PRIVILEGES_ATTENDANT, $ROLE_ATTENDANT);









$_dbh->commit();

$_how_long = Cataleya\Helper::stopTimer('install');



// tables installed
$_json_reply = array (

'status' => 'installed', 
'message' => ('Roles and privileges installed in - ' . $_how_long . ' seconds'), 
'time' => $_how_long, 
'token' => NEW_REQ_TOKEN
);

echo json_encode($_json_reply);
exit();


