<?php


/////// ADMIN PROFILE ////////////////////
define('OUTPUT', 'JSON');



////////// APP TOP //////////
require_once './includes/app.config.php';	






$_results = Cataleya\Helper\DBH::getInstance()->query('SELECT 1 FROM admin LIMIT 1')->fetchAll(PDO::FETCH_ASSOC);

if (count($_results) > 0)
{

    // config already installed
    $_json_reply = array (

    'status' => 'installed', 
    'message' => 'Admin account already installed.', 
    'time' => 0, 
    'token' =>  NEW_REQ_TOKEN
    );

    echo json_encode($_json_reply);
    exit();

}






$WHITE_LIST = array(
                                                // EXPECTED KEYS
    
						'token', 
						'admin-id', 
						'fname', 
						'lname',
						'email', 
						'password', 
						'password-confirm',  
						'phone'
					);




// SANITIZE GET DATA...
$_CLEAN = filter_input_array(INPUT_POST, 
								array(
										'token'	=>	FILTER_SANITIZE_STRIPPED, 
                                                                                'admin-id'	=>	FILTER_VALIDATE_INT, 
										'fname'	=>	array('filter'		=>	FILTER_VALIDATE_REGEXP, 
														'options'	=>	array('regexp' => '/[-\s\.A-Za-z0-9]{1,60}/')
																), 
										'lname'	=>	array('filter'		=>	FILTER_VALIDATE_REGEXP, 
														'options'	=>	array('regexp' => '/[-\s\.A-Za-z0-9]{1,60}/')
																), 
										'email'	=>	FILTER_VALIDATE_EMAIL, 
										'password'	=>	array(
                                                                                                                'filter'		=>	FILTER_VALIDATE_REGEXP, 
                                                                                                                'options'	=>	array('regexp' => '/[-_\W\s\.A-Za-z0-9]{8,60}/')
                                                                                                                ), 
                                                                                'password-confirm'	=>	array(
                                                                                                                'filter'		=>	FILTER_VALIDATE_REGEXP, 
                                                                                                                'options'	=>	array('regexp' => '/[-_\W\s\.A-Za-z0-9]{8,60}/')
                                                                                                                ), 
										'phone'	=>	array(
                                                                                                                'filter'		=>	FILTER_VALIDATE_REGEXP, 
                                                                                                                'options'	=>	array('regexp' => '/^[+]?[0-9]{6,20}$/')
                                                                                                                )

										)
								);






// VALIDATE FORM DATA

// Error / Response
$_errors = array ();

// 1. First Name

if (!isset($_CLEAN['fname']) || $_CLEAN['fname'] === FALSE || $_CLEAN['fname'] == '' )
{
    $_errors['fname'] = 'Please enter first name (Make sure to only use the characters: A-Z, a-z, 0-9, dashes and dots)';
}


// 2. Last Name

if (!isset($_CLEAN['lname']) || $_CLEAN['lname'] === FALSE || $_CLEAN['lname'] == '' )
{
    $_errors['lname'] = 'Please enter last name (Make sure to only use the characters: A-Z, a-z, 0-9, dashes and dots)';
}



// 3. Email

if (!isset($_CLEAN['email']) || $_CLEAN['email'] === FALSE || $_CLEAN['email'] == '' )
{
    $_errors['email'] = 'Please enter a valid email address.';
} else {
    $_CLEAN['email'] = trim($_CLEAN['email'], ' ');
}



// 4. Password

if (!isset($_CLEAN['password']) || $_CLEAN['password'] === FALSE || $_CLEAN['password'] == '' )
{
    $_errors['password'] = 'Please enter a password that is at least 8 characters long, and contains 1 uppercase letter and a number. <br/>- OR -<br/> Use a pass-phrase instead. For example: "I love bvlgari".';
    $_errors['password-confirm'] = 'Remember to re-type your password (must be the same as the first).';
    
}

// 5. Confirm Password

else if (!isset($_CLEAN['password-confirm']) || $_CLEAN['password-confirm'] === FALSE || $_CLEAN['password-confirm'] !== $_CLEAN['password'] )
{
    $_errors['password-confirm'] = 'Please re-type your password (must be the same as the first).';
}


// 5. Telephone

// Skip





// CHECK VALIDATION RESULTS

if (!empty($_errors)) 
{

    // Output...
    $json_data = array (
                    'status' => 'dialog', 
                    'message' => 'One or more items were missing in the admin account form. Please check and try again.',
                    'failedValidation' => TRUE, 
                    'failedItems'  => array_keys($_errors), 
                    'token' =>  NEW_REQ_TOKEN 
                    );
    echo json_encode($json_data);
    exit();
    
}





// Select admin role with SUPER_USER privilege...
$_roles = Cataleya\Admin\Roles::load();
$_Role = NULL;

foreach ($_roles as $_role)
{
    if ($_role->hasPrivilege('SUPER_USER')) 
    {
        $_Role = $_role;
        break;
    }

}




// Check admin role...
if ($_Role === NULL) _catch_error('Admin role not found.', __LINE__, true);






Cataleya\Helper::startTimer('install');



// NEW ACCOUNT

$options = array (
                    'firstname' => $_CLEAN['fname'], 
                    'lastname' => $_CLEAN['lname'], 
                    'admin_phone' => $_CLEAN['phone']
                    );

$AdminAccount = Cataleya\Admin\User::create($_CLEAN['email'], $_CLEAN['password'], $_Role, $options);
if ($AdminAccount === NULL) _catch_error('Account could not be created.', __LINE__, true);

// SET ACCOUNT STATUS
$AdminAccount->enable ();




$_how_long = Cataleya\Helper::stopTimer('install');



// tables installed
$_json_reply = array (

'status' => 'installed', 
'message' => ('Admin account installed in - ' . $_how_long . ' seconds'), 
'time' => $_how_long, 
'token' => NEW_REQ_TOKEN
);

echo json_encode($_json_reply);
exit();

