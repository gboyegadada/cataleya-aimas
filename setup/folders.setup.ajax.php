<?php


/////// PAYMENT TYPES ////////////////////
define('OUTPUT', 'JSON');



////////// APP TOP //////////
require_once './includes/app.config.php';	






Cataleya\Helper::startTimer('install');

$_dbh = Cataleya\Helper\DBH::getInstance();
// $_dbh->beginTransaction();

// ------------------------- DEMOS -------------------------------------------- //


$_project_info = file_exists(ROOT_PATH . 'setup/etc/folder-perms-checklist.json') 
        ? json_decode(file_get_contents(ROOT_PATH . 'setup/etc/folder-perms-checklist.json', FILE_IGNORE_NEW_LINES))  
        : null;

$_result = [];
$_any_errors = false;

if (!empty($_project_info)) 
{
    
    foreach ($_project_info->path as $_path) 
    {
        $_any_errors = $_error = ($_path->permissions !== substr(sprintf('%o', fileperms(ROOT_PATH . $_path->name)), -4));
        $_result[] = [ 
            'name' => $_path->name, 
            'permissions' => $_path->permissions, 
            'error' => $_error
           ];
        
    }
}


// show dialog
if ($_any_errors) 
{

    $_json_reply = array (

    'status' => 'dialog', 
    'message' => 'Please fix folder permissions marked in <strong>RED</strong> and click "continue".', 
    'result' => $_result, 
    'token' =>  NEW_REQ_TOKEN
    );
    
    echo json_encode($_json_reply);
    exit();

}




// $_dbh->commit();

$_how_long = Cataleya\Helper::stopTimer('install');



// tables installed
$_json_reply = array (

'status' => 'installed', 
'message' => ('Shop demo installed in - ' . $_how_long . ' seconds'), 
'time' => $_how_long, 
'token' => NEW_REQ_TOKEN
);

echo json_encode($_json_reply);
exit();


