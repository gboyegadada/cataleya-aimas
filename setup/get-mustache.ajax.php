<?php


//// DB SETTINGS /////////
define('OUTPUT', 'JSON');



////////// APP TOP //////////
require_once './includes/app.config.php';	






$_ids = Cataleya\Helper\Validator::array_of('foldername', $_POST['id'], 1, 150);
if ($_ids === FALSE) throw new Cataleya\Error('Invalid ID.');



$_templates = array ();

foreach ($_ids as $_id) {
    $_path_to_mustache = ROOT_PATH . 'setup/skin/mustache/' . $_id . '.mustache';

    if (file_exists($_path_to_mustache)) $_templates[$_id] = file_get_contents ($_path_to_mustache);
    else throw new Cataleya\Error('Template (' . $_path_to_mustache . ') not found.');
    
}


// OUTPUT...
$json_reply = array (
		"status"=>'ok', 
		"message"=>'Mustache templates.', 
                "templates"=>$_templates, 
    'token' =>  NEW_REQ_TOKEN
		);

echo (json_encode($json_reply));
exit();