<?php



//// CONFIGURATIONS /////////
define('OUTPUT', 'JSON');


////////// CONFIG //////////
require_once './includes/app.config.php';	





if (Cataleya\System\Config::load('store.core') !== NULL) 
{

    // config already installed
    $_json_reply = array (

    'status' => 'installed', 
    'message' => 'Configurations already installed.', 
    'time' => 0, 
    'token' =>  NEW_REQ_TOKEN
    );

    echo json_encode($_json_reply);
    exit();

}





if (preg_match('!^/(?<folder>([A-Za-z0-9_\-]+[\\\|/])+)?setup/.*$!', $_SERVER['REQUEST_URI'], $_matches) === 0 && trim($_SERVER['REQUEST_URI'], '/') !== 'setup') { 
    //throw new Exception ('Root directory could not be determined. Make sure folder name only has [A-Za-z0-9] !!');
    die($_SERVER['REQUEST_URI']);
}

$_folder = (isset($_matches) && !empty($_matches['folder'])) ? $_matches['folder'] : '';


Cataleya\Helper::startTimer('install');
$_dbh = Cataleya\Helper\DBH::getInstance();
$_dbh->beginTransaction();


// [1] STORE.CORE.CONFIG
$store_core_config = Cataleya\System\Config::init('store.core');

$store_core_config->newParam('domain', $_SERVER['HTTP_HOST']);
$store_core_config->newParam('host', $_SERVER['HTTP_HOST']);
$store_core_config->newParam('rootURI', $_folder);
// $store_core_config->newParam('shopFrontRootURI', $_uri.'october/');




// [3] JWT Secret
$store_core_config->newParam('JWT_SECRET', base64_encode(openssl_random_pseudo_bytes(64)));



// [2] MAILER.CONFIG
$mailer_config = Cataleya\System\Config::init('mailer.config');

$mailer_config->newParam('encryption', 'ssl');
$mailer_config->newParam('localhost', HOST);
$mailer_config->newParam('port', '465');
$mailer_config->newParam('smtpHost', '');
$mailer_config->newParam('user', '');
$mailer_config->newParam('pass', '');





if (!isset($store_core_config)) $store_core_config = Cataleya\System\Config::load('store.core');
define ('TRANX_RETURN_URL', 'https://' . $store_core_config->host . '/' . $store_core_config->shopLanding . 'thank-you');




/*

// [3] PAYPAL.CONFIG
$paypal_config = Cataleya\System\Config::init('paypal.config');

$paypal_config->newParam('curl_end_point', 'https://www.api-3t.sandbox.paypal.com/nvp'); // test server
$paypal_config->newParam('shopper_end_point', 'https://www.sandbox.paypal.com/webscr'); // test server

$paypal_config->newParam('user', 'selpro_1351845010_biz@gmail.com'); // for sandbox only
$paypal_config->newParam('pass', '123456789'); // for sandbox only
$paypal_config->newParam('signature', '');
$paypal_config->newParam('version', '74.0');
$paypal_config->newParam('return_url', TRANX_RETURN_URL);
$paypal_config->newParam('cancel_url', TRANX_RETURN_URL);


 */


// [9] OPEN.EXCHANGE.CONFIG
$open_exchange_config = Cataleya\System\Config::init('open.exchange.config');

$open_exchange_config->newParam('api_key', 'ddd743b503354f57959638d19fd363ac');
$open_exchange_config->newParam('url', 'http://openexchangerates.org/api/latest.json');
$open_exchange_config->newParam('secure_url', 'https://openexchangerates.org/api/latest.json');
$open_exchange_config->newParam('base_currency', 'USD');

 



$_dbh->commit();
$_how_long = Cataleya\Helper::stopTimer('install');



// tables installed
$_json_reply = array (

'status' => 'installed', 
'message' => ('Configurations installed in - ' . $_how_long . ' seconds'), 
'time' => $_how_long, 
'token' => NEW_REQ_TOKEN
);

echo json_encode($_json_reply);
exit();








