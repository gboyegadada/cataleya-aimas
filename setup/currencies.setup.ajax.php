<?php



//// CURRENCIES /////////
define('OUTPUT', 'JSON');


////////// CONFIG //////////
require_once './includes/app.config.php';	





$_results = Cataleya\Helper\DBH::getInstance()->query('SELECT 1 FROM currencies LIMIT 1')->fetchAll(PDO::FETCH_ASSOC);

if (count($_results) > 0)
{

    // already installed
    $_json_reply = array (

    'status' => 'installed', 
    'message' => 'Currencies already installed.', 
    'time' => 0, 
    'token' => NEW_REQ_TOKEN
    );

    echo json_encode($_json_reply);
    exit();

    }


else {

    // do inserts
    Cataleya\Helper::startTimer('install');
    Cataleya\Helper\DBH::runScript('currencies.setup.sql');
    $_how_long = Cataleya\Helper::stopTimer('install');


    // tables installed
    $_json_reply = array (

    'status' => 'installed', 
    'message' => ('Currencies installed in - ' . $_how_long . ' seconds'), 
    'time' => $_how_long, 
    'token' => NEW_REQ_TOKEN
    );

    echo json_encode($_json_reply);
    exit();


}






