<?php


/////// PAYMENT TYPES ////////////////////
define('OUTPUT', 'JSON');



////////// APP TOP //////////
require_once './includes/app.config.php';	


use Cataleya\Helper\DBH;


$_results = DBH::getInstance()->query('SELECT 1 FROM products LIMIT 1')->fetchAll(PDO::FETCH_ASSOC);

if (count($_results) > 0)
{

    // config already installed
    $_json_reply = array (

    'status' => 'installed', 
    'message' => 'Shop demo already installed.', 
    'time' => 0, 
    'token' =>  NEW_REQ_TOKEN
    );

    echo json_encode($_json_reply);
    exit();

}







Cataleya\Helper::startTimer('install');

// $_dbh = Cataleya\Helper\DBH::getInstance();
// $_dbh->beginTransaction();

// ------------------------- DEMOS -------------------------------------------- //




/*
// CATEGORIES

// Array of new tags

$new_tags = array (
                    0=>array(
                        'title' =>  'Women', 
                        'description'   =>  'Women&#39;s Clothing'
                    ), 
                    1=>array(
                        'title' =>  'Chicy Aimas T-Shirts', 
                        'description'   =>  'Chicy t-shirts for summer!'
                    ),
                    2=>array(
                        'title' =>  'Chicy Clutch Handbags', 
                        'description'   =>  'Chicy aimas leather handbags!'
                    )
);


foreach ($new_tags as $new_tag_info)
{
    Cataleya\Catalog\Tag::create($_ng_store, $new_tag_info['title'], $new_tag_info['description'], 'EN');
}






// PRODUCTS

// Array of new products

$new_products = array (
                    0=>array(
                        'title' =>  'Chicy Pink Tee', 
                        'description'   =>  'Chicy Pink Tee.'
                    ), 
                    1=>array(
                        'title' =>  'Blue Summer Top', 
                        'description'   =>  'Blue Summer Top.'
                    ),
                    2=>array(
                        'title' =>  'Chicy Clutch Handbag', 
                        'description'   =>  'Chicy aimas leather handbag.'
                    )
);



// Create Product Types...
$_ProductType_Tee = Cataleya\Catalog\Product\Type::create(array('name'=>'TShirt', 'name_plural'=>'TShirts', 'icon'=>'tee.png', 'active'=>TRUE));
$_ProductType_Bag = Cataleya\Catalog\Product\Type::create(array('name'=>'Bag', 'name_plural'=>'Bags', 'icon'=>'bag.png', 'active'=>TRUE));



$colors = array ('Red', 'Yellow', 'Green');
$sizes = array ('Small', 'Medium', 'Large');


// Create Attribute Types...
$_AttributeTypeColor = Cataleya\Catalog\Product\Attribute\Type::create(array ('name'=>'Color', 'name_plural'=>'Colors', 'sort_order'=>0, 'active'=>TRUE));
$_AttributeTypeSize = Cataleya\Catalog\Product\Attribute\Type::create(array ('name'=>'Size', 'name_plural'=>'Sizes', 'sort_order'=>1, 'active'=>TRUE));

// Create attributes...
foreach ($colors as $color) Cataleya\Catalog\Product\Attribute::create($_AttributeTypeColor, $color);
foreach ($sizes as $size) Cataleya\Catalog\Product\Attribute::create($_AttributeTypeSize, $size);

$_ProductType_Tee->addAttributeType($_AttributeTypeColor);
$_ProductType_Tee->addAttributeType($_AttributeTypeSize);

$_ProductType_Bag->addAttributeType($_AttributeTypeColor);



*/






/*  ------------------ 2. Read CSV data --------------------------------------- */


$_path_to_csv = ROOT_PATH . 'var/imports/shopify_products_export.csv';
$_Importer = new Cataleya\Helper\Importer(Cataleya\Store::load(1), Cataleya\Helper\Importer\Reader::FILE_TYPE_SHOPIFY_CSV, $_path_to_csv);
  
$_Importer->doImport();

// A little fix because of crazy attribute types from shopify...
DBH::getInstance()->query('update attributes set attribute_type_id=2 where attribute_type_id=1');
DBH::getInstance()->query('delete from attribute_types where attribute_type_id=1');
DBH::getInstance()->query('insert into product_types_to_attribute_types (product_type_id, attribute_type_id, sort_order) values (1, 2, 0)');



// $_dbh->commit();

$_how_long = Cataleya\Helper::stopTimer('install');



// tables installed
$_json_reply = array (

'status' => 'installed', 
'message' => ('Shop demo installed in - ' . $_how_long . ' seconds'), 
'time' => $_how_long, 
'token' => NEW_REQ_TOKEN
);

echo json_encode($_json_reply);
exit();


