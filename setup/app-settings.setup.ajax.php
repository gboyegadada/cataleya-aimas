<?php


/////// APP SETTINGS ////////////////////
define('OUTPUT', 'JSON');


////////// APP TOP //////////
require_once './includes/app.config.php';	




if (Cataleya\System::load() !== NULL) 
{

    // config already installed
    $_json_reply = array (

    'status' => 'installed', 
    'message' => 'Application settings already installed.', 
    'time' => 0, 
    'token' =>  NEW_REQ_TOKEN
    );

    echo json_encode($_json_reply);
    exit();

}



Cataleya\Helper::startTimer('install');

$_dbh = Cataleya\Helper\DBH::getInstance();
$_dbh->beginTransaction();






/*
 * 
 * 
 * 
 * SETUP APPLICATION SETTINGS
 * 
 * 
 * 
 */


$_project_info = file_exists(ROOT_PATH . 'project.info.json') 
        ? json_decode(file_get_contents(ROOT_PATH . 'project.info.json', FILE_IGNORE_NEW_LINES)) 
        : NULL;

$_license = file_exists(ROOT_PATH . '/LICENSE.txt') 
        ? file_get_contents((ROOT_PATH . 'LICENSE.txt')) 
        : 'FPP Labs (c)2015.';


$_options = array (
    
    'app_version'   =>  ($_project_info) ? $_project_info->app_version : '1', 
    'developer' =>  ($_project_info) ? $_project_info->developer :'Gboyega Dada (FPP)', 
    'client'    =>  ($_project_info) ? $_project_info->client : 'Unknown', 
    'credits'   =>  ($_project_info) ? $_project_info->credits :'Team? Just me for now :). -Gboyega for FPP.', 
    'license'   =>  $_license,
    'active'    =>  FALSE
);

$_app_settings = Cataleya\System::create($_options);






                
                

$_dbh->commit();

$_how_long = Cataleya\Helper::stopTimer('install');



// tables installed
$_json_reply = array (

'status' => 'installed', 
'message' => ('Application settings installed in - ' . $_how_long . ' seconds'), 
'time' => $_how_long, 
'token' => NEW_REQ_TOKEN
);

echo json_encode($_json_reply);
exit();

