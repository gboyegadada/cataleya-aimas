<?php


/////// PAYMENT TYPES ////////////////////
define('OUTPUT', 'JSON');



////////// APP TOP //////////
require_once './includes/app.config.php';	







Cataleya\Helper::startTimer('install');

$_dbh = Cataleya\Helper\DBH::getInstance();

// ------------------------- DEMOS -------------------------------------------- //


foreach (\Cataleya\Helper::getClassList("Cataleya\System\Event\Observable") as $_c) 
{

    $_events = call_user_func([ $_c, 'getEvents' ]);
    \Cataleya\System\Event::add($_events, $_c);

}





$_how_long = Cataleya\Helper::stopTimer('install');



// tables installed
$_json_reply = array (

'status' => 'installed', 
'message' => ('System events registered in - ' . $_how_long . ' seconds'), 
'time' => $_how_long, 
'token' => NEW_REQ_TOKEN
);

echo json_encode($_json_reply);
exit();


