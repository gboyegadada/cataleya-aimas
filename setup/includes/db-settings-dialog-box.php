
<div id="db-settings-dialog-box" class="alert-box">
    <form name="db-settings-form" id="db-settings-form"  method="POST">
        
        <h2 class="bubble-title">Data Base Settings</h2>
        <div class="tiny-yellow-text">Enter a name and choose a list type for the tax class you wish to create</div>
        <div class="tiny-yellow-text response"></div>
        <br/>
        <div class="dotted-yellow-hor-line tiny-yellow-arrow-down">&nbsp;</div>
        <p class="bubble-text">
                    <div>
                    <br />
                    <small class="text-input-label-3">DATA BASE NAME</small>
                    <input id="text-field-db-settings-name" name="dbname" tip="Please enter a name for the new data base. <small>Use only A-Z, a-z, 0-9, spaces, dots, commas or dashes!</small>" class="placeholder-color-1" type="text" value="" placeholder="Type database name here." size=50 />
                    
                    <br /><br />
                    <small class="text-input-label-3">DATA BASE HOST</small>
                    <input id="text-field-db-settings-name" name="host" tip="Please enter a name for the new data base. <small>Use only A-Z, a-z, 0-9, spaces, dots, commas or dashes!</small>" class="placeholder-color-1" type="text" value="localhost" placeholder="Type host name here." size=50 />
                    
                    <br /><br />
                    <small class="text-input-label-3">USER NAME</small>
                    <input id="text-field-db-settings-name" name="user" tip="Please enter a name for the new data base. <small>Use only A-Z, a-z, 0-9, spaces, dots, commas or dashes!</small>" class="placeholder-color-1" type="text" value="" placeholder="Type user name here." size=50 />
                    
                    <br /><br />
                    <small class="text-input-label-3">PASSWORD</small>
                    <input id="text-field-db-settings-name" name="pass" tip="Please enter a name for the new data base. <small>Use only A-Z, a-z, 0-9, spaces, dots, commas or dashes!</small>" class="placeholder-color-1" type="password" value="" size=50 />
                    
                    <br /><br />
                    
                    </div>  
  
        </p>

        <br />
        <div class="dotted-blue-hor-line">&nbsp;</div>
        
        <br /><br />
        
        <a class="button-1" id="db-settings-button-cancel" href="javascript:void(0)" onClick="return false;" >cancel</a>
        &nbsp;&nbsp;
        <input type="button" class="button-1 ok-button" id="db-settings-button-ok" value="save settings" />
        <br /><br/>
    </form>
</div>



