<?php


// die ('Cataleya is already installed!');

define ('IS_ADMIN_FLAG', TRUE);




/**
 * debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use DEBUG_MODE
 * in their development environments.
 */
 
define('DEBUG_MODE', true);

define('SSL_REQUIRED', FALSE);


// OTHER GLOBAL STUFF
define ('DEFAULT_TIMEZONE', 'Europe/London');
date_default_timezone_set(DEFAULT_TIMEZONE);

define ('CURRENT_DT', time());
define('SESSION_PREFIX', 'SETUP_');
define('SESSION_NAME', 'cs');
define('SESSION_EXP', CURRENT_DT-(60*60)); // After 60 min...




/*
 * 
 * SETUP CONSTANTS
 * 
 */

// PATHS...
$_abs_path_shared = preg_replace('!setup[\\\|/]includes[\\\|/]?$!', '', dirname(__FILE__));
$_abs_path = $_abs_path_shared . 'october/';


define ('ADMIN_ROOT_PATH', $_abs_path);
define ('ROOT_PATH', $_abs_path_shared);
define ('INC_PATH', $_abs_path.'includes/');
define ('SHARED_INC_PATH', $_abs_path_shared.'includes/');
define('TEMP_DIR', $_abs_path_shared.'ui/images/catalog/temp/');
define('CAT_DIR', $_abs_path_shared.'ui/images/catalog/');



define('LIB_PATH', ROOT_PATH.'library/');
define('CATALEYA_LOGS_PATH' , ROOT_PATH.'var/logs');
define('CATALEYA_CACHE_PATH', ROOT_PATH.'var/cache');
define('CATALEYA_CACHE_DIR', ROOT_PATH.'var/cache');
define('CATALEYA_PLUGINS_PATH', ROOT_PATH.'app/plugins');
define('PLUGIN_PACKAGES_PATH', ROOT_PATH.'var/packages');

define('PATH_SKIN', ROOT_PATH.'skin');

define('APP_STORE_ENDPOINT', 'http://dev.fancypaperplanes.com/appstore/');



if (!defined('OUTPUT')) define ('OUTPUT', 'HTML'); // Whether to output as JSON or HTML (especially errors)...
define('SETUP_DIR', ROOT_PATH.'setup/');
define('PATH_ALL_PRIVILEGES', SETUP_DIR.'etc/privileges.dat');
define('PATH_PRIVILEGES_STORE_OWNER', SETUP_DIR.'etc/store_owner.privileges.dat');
define('PATH_PRIVILEGES_MANAGER', SETUP_DIR.'etc/manager.privileges.dat');
define('PATH_PRIVILEGES_ATTENDANT', SETUP_DIR.'etc/attendant.privileges.dat');


if (preg_match('!^/(?<folder>([A-Za-z0-9_\-]+[\\\|/])+)?setup/.*$!', $_SERVER['REQUEST_URI'], $_matches) === 0 && trim($_SERVER['REQUEST_URI'], '/') !== 'setup') { 
    throw new Exception ('Root directory could not be determined. Make sure folder name only has [A-Za-z0-9] !!');
    // die($_SERVER['REQUEST_URI']);
}

$_folder = (isset($_matches) && !empty($_matches['folder'])) ? $_matches['folder'] : '';

define('HOST', $_SERVER['HTTP_HOST']);
define('DOMAIN', HOST);
define('ROOT_URI', '/'.$_folder.'setup/');
define('BASE_URL', (SSL_REQUIRED ? 'https://' : 'http://') . HOST.'/'.$_folder.'setup/');
define('ADMIN_BASE_URL', (SSL_REQUIRED ? 'https://' : 'http://') . HOST.'/'.$_folder.'october/');
define('DISABLE_SECURE_SESSION', TRUE); // DB is not yet ready for this.
define('CHECKOUT_RETURN_URL', HOST);

define('EMAIL_HOST', HOST);
define('SERVER_ROOT', '/');
define('SERVER_STORE_ROOT', 'store/');





// Prevent PHP from timing out !!
set_time_limit (60 * 5); 



// GENERAL FUNCTIONS...
require_once (SHARED_INC_PATH.'functions/general_funcs.php');





// Check for direct calls to AJAX scripts...
if (defined('OUTPUT') && OUTPUT == 'JSON') {
	if(isset($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
		// This an ajax request. Do nothing.
	 } else {
		$_to = 'http://' . HOST;
		header('Location: ' . $_to);
		exit();
	 }
}





// Redirect to https if  SSL is required...
if ( defined('SSL_REQUIRED') && SSL_REQUIRED === TRUE && !is_ssl() ) {
	if ( 0 === strpos($_SERVER['REQUEST_URI'], 'http') ) {
		redirect(preg_replace('|^http://|', 'https://', $_SERVER['REQUEST_URI']));
		exit();
	} else {
		redirect('https://' . HOST . $_SERVER['REQUEST_URI']);
		exit();
	}
}





/////////// Cataleya INIT ///////
// require_once (SHARED_INC_PATH.'cataleya.init.php');	
//Load Cataleya utility class
require ROOT_PATH . 'app/core/Cataleya/Autoloader.php';

//Start the autoloader
Cataleya\Autoloader::registerAutoload();





 

///////////////////  REGISTER SESSION HANDLER //////////////

if (IS_ADMIN_FLAG === true) {
        if (!$SESS_LIFE = (SESSION_EXP > 3600 ? 900 : SESSION_EXP)) $SESS_LIFE = (SESSION_EXP > 3600 ? 900 : SESSION_EXP);
} else {
        if (!$SESS_LIFE = get_cfg_var('session.gc_maxlifetime')) $SESS_LIFE = SESSION_EXP; //1440;
}

if (defined('DISABLE_SECURE_SESSION') && DISABLE_SECURE_SESSION) { /* don't load session class */ }
else {
    Cataleya\Session::setSessionLifeTime($SESS_LIFE);
    Cataleya\Session::load();
}










/////////// MailChimp INIT ///////
//require_once (SHARED_INC_PATH.'mailchimp.init.php');	
require_once (ROOT_PATH.'library/MailChimp/MailChimp.class.php');



/////////// Twig INIT ///////
//require_once (SHARED_INC_PATH.'twig.init.php');
require_once (ROOT_PATH.'library/Twig/Autoloader.php');
Twig_Autoloader::register();



/////////// HTMLPurifier INIT ///////
//require_once (SHARED_INC_PATH.'HTMLPurifier.init.php');	
require_once (ROOT_PATH . 'library/HTMLPurifier/HTMLPurifier.auto.php');
require_once 'HTMLPurifier.func.php';




// SWIFT MAILER
// require_once (SHARED_INC_PATH.'swiftmailer.init.php');
require_once (ROOT_PATH.'library/swiftmailer/lib/swift_required.php');
Swift::init(function () {
	
	Swift_DependencyContainer::getInstance()->register('mime.qpcontentencoder')->asAliasOf('mime.nativeqpcontentencoder');

});





// INIT SESSION
session_name(SESSION_NAME);
Cataleya\Session::init_session_cookie(array('path'=>ROOT_URI));
session_start();



// HEADERS

header("Cache-Control: no-cache, must-revalidate");
header("Expires: Mon, 26 Jul 1997 05:00:00 GMT");

if (OUTPUT == 'JSON') {
	
	header('Content-type: application/x-json');
	
} else if (OUTPUT == 'TEXT') {
    	header('Content-Type: text/plain; charset="UTF-8"');
}










if (OUTPUT === 'HTML' && preg_match('/setup\/index\.php/', $_SERVER['SCRIPT_NAME']) !== 0) {



    // set new token
    define('NEW_REQ_TOKEN', Cataleya\Helper::digest());
    $_SESSION[SESSION_PREFIX.'REQ_TOKEN'] = NEW_REQ_TOKEN;





}

else if (OUTPUT === 'JSCRIPT') {
    // do nothing
}

else {

    // validate token
    if (!isset($_SESSION[SESSION_PREFIX.'REQ_TOKEN'], $_POST['token']) || $_SESSION[SESSION_PREFIX.'REQ_TOKEN'] !== $_POST['token']) 
    {

        $_json_reply = array (

        'status' => 'error', 
        'message' => 'x_x'
        );

        echo json_encode($_json_reply);
        exit();

    } else {
        // set new token
        define('NEW_REQ_TOKEN', Cataleya\Helper::digest());
        $_SESSION[SESSION_PREFIX.'REQ_TOKEN'] = NEW_REQ_TOKEN;
    }


}






