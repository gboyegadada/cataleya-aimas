SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';


-- -----------------------------------------------------
-- Table `countries`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `countries` (
  `country_code` CHAR(2) NOT NULL DEFAULT '' ,
  `name` VARCHAR(160) NOT NULL DEFAULT '' ,
  `printable_name` VARCHAR(160) NOT NULL ,
  `iso3` CHAR(3) NULL DEFAULT '' ,
  `numcode` SMALLINT NULL ,
  `has_states` TINYINT(1) UNSIGNED NOT NULL DEFAULT 0 ,
  INDEX `idx_countries_name` (`name` ASC) ,
  INDEX `idx_iso_2` (`country_code` ASC) ,
  INDEX `idx_iso_3` (`iso3` ASC) ,
  PRIMARY KEY (`country_code`) ,
  UNIQUE INDEX `country_iso_code_2_UNIQUE` (`country_code` ASC) ,
  UNIQUE INDEX `country_iso_code_3_UNIQUE` (`iso3` ASC) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `provinces`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `provinces` (
  `province_id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT ,
  `country_code` CHAR(2) NOT NULL ,
  `iso_code` VARCHAR(4) NOT NULL ,
  `printable_name` VARCHAR(160) NULL ,
  `name` VARCHAR(160) NULL ,
  `date_added` DATETIME NOT NULL ,
  `last_modified` DATETIME NOT NULL ,
  PRIMARY KEY (`province_id`, `country_code`) ,
  UNIQUE INDEX `iso_code_UNIQUE` (`iso_code` ASC) ,
  INDEX `fk_provinces_countries1_idx` (`country_code` ASC) ,
  CONSTRAINT `fk_provinces_countries1`
    FOREIGN KEY (`country_code` )
    REFERENCES `countries` (`country_code` )
    ON DELETE CASCADE
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `address_book`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `address_book` (
  `contact_id` BIGINT UNSIGNED NOT NULL AUTO_INCREMENT ,
  `label` VARCHAR(45) NOT NULL DEFAULT 'New Contact' ,
  `gender` ENUM('undisclosed', 'female', 'male') NOT NULL DEFAULT 'undisclosed' ,
  `company` VARCHAR(128) NULL DEFAULT NULL ,
  `firstname` VARCHAR(128) NOT NULL DEFAULT '' ,
  `lastname` VARCHAR(128) NOT NULL DEFAULT '' ,
  `email` VARCHAR(255) NULL DEFAULT '' ,
  `website` VARCHAR(255) NULL DEFAULT '' ,
  `work_phone` VARCHAR(32) NULL DEFAULT '' ,
  `home_phone` VARCHAR(32) NULL DEFAULT '' ,
  `fax` VARCHAR(32) NULL DEFAULT '' ,
  `default` TINYINT(1) NOT NULL DEFAULT '0' ,
  `street_address1` VARCHAR(255) NOT NULL DEFAULT '' ,
  `street_address2` VARCHAR(255) NULL DEFAULT '' ,
  `postcode` VARCHAR(15) NULL DEFAULT '' ,
  `city` VARCHAR(45) NULL DEFAULT '' ,
  `province` VARCHAR(45) NULL DEFAULT '' ,
  `province_id` INT(11) UNSIGNED NULL ,
  `country_code` CHAR(2) NOT NULL ,
  `latitude` DECIMAL(11,8) NULL DEFAULT NULL ,
  `longtitude` DECIMAL(11,8) NULL DEFAULT NULL ,
  PRIMARY KEY (`contact_id`) ,
  INDEX `fk_country_code_idx` (`country_code` ASC) ,
  INDEX `fk_province_id_idx` (`province_id` ASC) ,
  CONSTRAINT `fk_country_code`
    FOREIGN KEY (`country_code` )
    REFERENCES `countries` (`country_code` )
    ON DELETE CASCADE
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_province_id`
    FOREIGN KEY (`province_id` )
    REFERENCES `provinces` (`province_id` )
    ON DELETE CASCADE
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `address_format`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `address_format` (
  `address_format_id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT ,
  `address_format` VARCHAR(128) NOT NULL DEFAULT '' ,
  `address_summary` VARCHAR(48) NOT NULL DEFAULT '' ,
  PRIMARY KEY (`address_format_id`) )
ENGINE = MyISAM;


-- -----------------------------------------------------
-- Table `banners`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `banners` (
  `banners_id` INT(11) NOT NULL AUTO_INCREMENT ,
  `banners_title` VARCHAR(64) NOT NULL DEFAULT '' ,
  `banners_url` VARCHAR(255) NOT NULL DEFAULT '' ,
  `banners_image` VARCHAR(64) NOT NULL DEFAULT '' ,
  `banners_group` VARCHAR(15) NOT NULL DEFAULT '' ,
  `banners_html_text` TEXT NULL DEFAULT NULL ,
  `expires_impressions` INT(7) NULL DEFAULT '0' ,
  `expires_date` DATETIME NULL DEFAULT NULL ,
  `date_scheduled` DATETIME NULL DEFAULT NULL ,
  `date_added` DATETIME NOT NULL DEFAULT '0001-01-01 00:00:00' ,
  `date_status_change` DATETIME NULL DEFAULT NULL ,
  `status` INT(1) NOT NULL DEFAULT '1' ,
  `banners_open_new_windows` INT(1) NOT NULL DEFAULT '1' ,
  `banners_on_ssl` INT(1) NOT NULL DEFAULT '1' ,
  `banners_sort_order` INT(11) NOT NULL DEFAULT '0' ,
  PRIMARY KEY (`banners_id`) ,
  INDEX `idx_status_group_zen` (`status` ASC, `banners_group` ASC) ,
  INDEX `idx_expires_date_zen` (`expires_date` ASC) ,
  INDEX `idx_date_scheduled_zen` (`date_scheduled` ASC) )
ENGINE = MyISAM;


-- -----------------------------------------------------
-- Table `banners_history`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `banners_history` (
  `banners_history_id` INT(11) NOT NULL AUTO_INCREMENT ,
  `banners_id` INT(11) NOT NULL DEFAULT '0' ,
  `banners_shown` INT(5) NOT NULL DEFAULT '0' ,
  `banners_clicked` INT(5) NOT NULL DEFAULT '0' ,
  `banners_history_date` DATETIME NOT NULL DEFAULT '0001-01-01 00:00:00' ,
  PRIMARY KEY (`banners_history_id`) ,
  INDEX `idx_banners_id_zen` (`banners_id` ASC) )
ENGINE = MyISAM;


-- -----------------------------------------------------
-- Table `customers`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `customers` (
  `customer_id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT ,
  `title` VARCHAR(20) NULL ,
  `firstname` VARCHAR(128) NOT NULL DEFAULT '' ,
  `lastname` VARCHAR(128) NOT NULL DEFAULT '' ,
  `nickname` VARCHAR(96) NOT NULL DEFAULT '' ,
  `gender` ENUM('female', 'male', 'undisclosed') NOT NULL DEFAULT 'undisclosed' ,
  `dob` DATE NOT NULL DEFAULT '0001-01-01' ,
  `email_address` VARCHAR(255) NOT NULL DEFAULT '' ,
  `default_contact_id` INT(11) NOT NULL DEFAULT 0 ,
  `telephone` VARCHAR(32) NOT NULL DEFAULT '' ,
  `fax` VARCHAR(32) NULL ,
  `password` VARCHAR(255) NOT NULL DEFAULT '' ,
  `confirm_digest` VARCHAR(255) NULL ,
  `confirm_num_code` VARCHAR(6) NULL DEFAULT '0' ,
  `newsletter` TINYINT(1) NOT NULL DEFAULT FALSE ,
  `group_pricing` INT(11) NOT NULL DEFAULT '0' ,
  `email_format` ENUM('text', 'html') NOT NULL DEFAULT 'text' ,
  `authorization` INT(1) NOT NULL DEFAULT '0' ,
  `customer_referral` VARCHAR(128) NOT NULL DEFAULT '' ,
  `is_online` VARCHAR(255) NOT NULL DEFAULT '0' ,
  `number_of_logons` INT(5) NOT NULL DEFAULT 0 ,
  `last_logon` DATETIME NULL ,
  `date_created` DATETIME NOT NULL ,
  `last_modified` DATETIME NOT NULL ,
  PRIMARY KEY (`customer_id`) ,
  INDEX `idx_email_address` (`email_address` ASC) ,
  INDEX `idx_referral` (`customer_referral`(10) ASC) ,
  INDEX `idx_name` (`firstname` ASC, `lastname` ASC) ,
  INDEX `idx_newsletter` (`newsletter` ASC) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `email_archive`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `email_archive` (
  `archive_id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT ,
  `message_id` VARCHAR(255) NOT NULL ,
  `sender` VARCHAR(255) NOT NULL ,
  `to_name` VARCHAR(255) NOT NULL DEFAULT '' ,
  `to_address` VARCHAR(255) NOT NULL DEFAULT '' ,
  `from_name` VARCHAR(255) NOT NULL DEFAULT '' ,
  `from_address` VARCHAR(255) NOT NULL DEFAULT '' ,
  `subject` VARCHAR(255) NOT NULL DEFAULT '' ,
  `html` TEXT NOT NULL ,
  `text` TEXT NOT NULL ,
  `date_sent` DATETIME NOT NULL ,
  `module` VARCHAR(64) NOT NULL DEFAULT '' ,
  PRIMARY KEY (`archive_id`) ,
  INDEX `idx_email_to_address` (`to_address` ASC) ,
  INDEX `idx_module` (`module` ASC) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `group_pricing`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `group_pricing` (
  `group_id` INT(11) NOT NULL AUTO_INCREMENT ,
  `group_name` VARCHAR(32) NOT NULL DEFAULT '' ,
  `group_percentage` DECIMAL(5,2) NOT NULL DEFAULT '0.00' ,
  `last_modified` DATETIME NULL DEFAULT NULL ,
  `date_added` DATETIME NOT NULL DEFAULT '0001-01-01 00:00:00' ,
  PRIMARY KEY (`group_id`) )
ENGINE = MyISAM;


-- -----------------------------------------------------
-- Table `currencies`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `currencies` (
  `currency_code` CHAR(3) NOT NULL ,
  `currency_name` VARCHAR(255) NULL DEFAULT '' ,
  `numeric_code` CHAR(3) NOT NULL DEFAULT '000' ,
  `minor_unit` VARCHAR(5) NOT NULL DEFAULT '2' ,
  `utf_code` INT NULL DEFAULT '0' ,
  `symbol_left` VARCHAR(24) NULL DEFAULT '' ,
  `symbol_right` VARCHAR(24) NULL DEFAULT NULL ,
  `decimal_point` CHAR(1) NULL DEFAULT NULL ,
  `thousands_point` CHAR(1) NULL DEFAULT NULL ,
  `rate` FLOAT(13,6) NULL DEFAULT NULL ,
  `last_updated` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP ,
  `active` TINYINT(1) NOT NULL DEFAULT FALSE ,
  `is_base_currency` TINYINT(1) NOT NULL DEFAULT FALSE ,
  PRIMARY KEY (`currency_code`) ,
  UNIQUE INDEX `currency_code_UNIQUE` (`currency_code` ASC) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `languages`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `languages` (
  `language_code` CHAR(2) NOT NULL ,
  `name` VARCHAR(32) NOT NULL DEFAULT '' ,
  `image` VARCHAR(64) NULL DEFAULT NULL ,
  `directory` VARCHAR(32) NULL DEFAULT NULL ,
  `sort_order` INT(3) NULL DEFAULT NULL ,
  PRIMARY KEY (`language_code`) ,
  INDEX `idx_languages_name_zen` (`name` ASC) ,
  UNIQUE INDEX `code_UNIQUE` (`language_code` ASC) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `files`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `files` (
  `uid` VARCHAR(200) NOT NULL ,
  `locks` INT UNSIGNED NOT NULL DEFAULT 1 ,
  `date_added` DATETIME NOT NULL ,
  PRIMARY KEY (`uid`) ,
  UNIQUE INDEX `uid_UNIQUE` (`uid` ASC) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `images`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `images` (
  `image_id` BIGINT UNSIGNED NOT NULL AUTO_INCREMENT ,
  `description_id` BIGINT UNSIGNED NULL ,
  `image_type` TINYINT(1) UNSIGNED NOT NULL DEFAULT 3 ,
  `image_file` VARCHAR(200) NOT NULL DEFAULT '' ,
  `date_added` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ,
  `hatched` TINYINT(1) NOT NULL DEFAULT 0 ,
  `imagescol` VARCHAR(45) NULL ,
  PRIMARY KEY (`image_id`) ,
  INDEX `files_idx` (`image_file` ASC) ,
  CONSTRAINT `files`
    FOREIGN KEY (`image_file` )
    REFERENCES `files` (`uid` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `url_rewrite`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `url_rewrite` (
  `url_rewrite_id` BIGINT UNSIGNED NOT NULL AUTO_INCREMENT ,
  `keyword` VARCHAR(225) NOT NULL ,
  `target_path` VARCHAR(225) NOT NULL ,
  `is_admin` SMALLINT(5) NOT NULL DEFAULT '0' ,
  `store_id` SMALLINT UNSIGNED NULL DEFAULT NULL ,
  PRIMARY KEY (`url_rewrite_id`) ,
  INDEX `fk_url_store_id_idx` (`store_id` ASC) ,
  CONSTRAINT `fk_url_store_id`
    FOREIGN KEY (`store_id` )
    REFERENCES `stores` (`store_id` )
    ON DELETE CASCADE
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `admin_roles`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `admin_roles` (
  `role_id` SMALLINT UNSIGNED NOT NULL AUTO_INCREMENT ,
  `handle` VARCHAR(160) NOT NULL ,
  `description_id` BIGINT UNSIGNED NOT NULL ,
  `date_created` DATETIME NOT NULL ,
  `last_modified` DATETIME NOT NULL ,
  PRIMARY KEY (`role_id`) ,
  UNIQUE INDEX `handle_UNIQUE` (`handle` ASC) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `admin`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `admin` (
  `admin_id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT ,
  `role_id` SMALLINT UNSIGNED NOT NULL ,
  `firstname` VARCHAR(32) NOT NULL DEFAULT '' ,
  `lastname` VARCHAR(32) NOT NULL ,
  `image_id` BIGINT UNSIGNED NULL ,
  `admin_email` VARCHAR(128) NOT NULL DEFAULT '' ,
  `admin_pass` VARCHAR(255) NOT NULL DEFAULT '' ,
  `prev_pass1` VARCHAR(255) NOT NULL DEFAULT '' ,
  `prev_pass2` VARCHAR(255) NOT NULL DEFAULT '' ,
  `prev_pass3` VARCHAR(255) NOT NULL DEFAULT '' ,
  `pwd_last_change_date` DATETIME NOT NULL DEFAULT '1000-01-01 00:00:00' ,
  `reset_token` VARCHAR(255) NOT NULL DEFAULT '' ,
  `date_created` DATETIME NOT NULL ,
  `last_modified` DATETIME NOT NULL DEFAULT '1000-01-01 00:00:00' ,
  `last_login_date` DATETIME NOT NULL DEFAULT '1000-01-01 00:00:00' ,
  `last_login_ip` VARCHAR(15) NOT NULL DEFAULT '' ,
  `failed_logins` SMALLINT(4) UNSIGNED NOT NULL DEFAULT '0' ,
  `lockout_expires` INT(11) NOT NULL DEFAULT '0' ,
  `last_failed_attempt` DATETIME NOT NULL DEFAULT '1000-01-01 00:00:00' ,
  `last_failed_ip` VARCHAR(15) NOT NULL DEFAULT '' ,
  `admin_phone` VARCHAR(15) NOT NULL DEFAULT '+234' ,
  `is_online` VARCHAR(255) NOT NULL DEFAULT '0' ,
  `shop_key` VARCHAR(255) NOT NULL DEFAULT '' ,
  `last_login_geo_loc` VARCHAR(225) NOT NULL DEFAULT 'unknown' ,
  `last_failed_geo_loc` VARCHAR(255) NOT NULL DEFAULT 'unknown' ,
  `active` TINYINT(1) NOT NULL DEFAULT 0 ,
  PRIMARY KEY (`admin_id`) ,
  INDEX `idx_admin_name` (`firstname` ASC, `lastname` ASC) ,
  INDEX `idx_admin_email` (`admin_email` ASC) ,
  INDEX `fk_admin_admin_roles1_idx` (`role_id` ASC) ,
  INDEX `fk_admin_images1_idx` (`image_id` ASC) ,
  CONSTRAINT `fk_admin_admin_roles1`
    FOREIGN KEY (`role_id` )
    REFERENCES `admin_roles` (`role_id` )
    ON DELETE RESTRICT
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_admin_images1`
    FOREIGN KEY (`image_id` )
    REFERENCES `images` (`image_id` )
    ON DELETE SET NULL
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `content`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `content` (
  `content_id` BIGINT UNSIGNED NOT NULL AUTO_INCREMENT ,
  `author_id` INT(11) UNSIGNED NULL DEFAULT NULL ,
  `guid` VARCHAR(255) NOT NULL DEFAULT '' ,
  `parent_id` BIGINT UNSIGNED NULL ,
  `status` TINYINT(1) NULL ,
  `is_system` TINYINT(1) NULL ,
  `template` VARCHAR(45) NOT NULL DEFAULT 'page.html.twig' ,
  `plugin_list` TEXT NULL ,
  `content_type` SMALLINT UNSIGNED NOT NULL DEFAULT 1 ,
  `content_mime_type` VARCHAR(100) NOT NULL DEFAULT 'text/plain' ,
  `store_id` SMALLINT UNSIGNED NULL ,
  `url_rewrite_id` BIGINT UNSIGNED NULL ,
  `sort_order` SMALLINT(6) NULL ,
  `date_added` DATETIME NOT NULL ,
  `last_modified` DATETIME NOT NULL ,
  PRIMARY KEY (`content_id`) ,
  INDEX `fk_page_url_rewrite_id_idx` (`url_rewrite_id` ASC) ,
  INDEX `fk_parent_page_id_idx` (`parent_id` ASC) ,
  INDEX `fk_content_store_id_idx` (`store_id` ASC) ,
  INDEX `fk_author_admin_id_idx` (`author_id` ASC) ,
  CONSTRAINT `fk_page_url_rewrite_id`
    FOREIGN KEY (`url_rewrite_id` )
    REFERENCES `url_rewrite` (`url_rewrite_id` )
    ON DELETE RESTRICT
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_parent_page_id`
    FOREIGN KEY (`parent_id` )
    REFERENCES `content` (`content_id` )
    ON DELETE CASCADE
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_content_store_id`
    FOREIGN KEY (`store_id` )
    REFERENCES `stores` (`store_id` )
    ON DELETE CASCADE
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_author_admin_id`
    FOREIGN KEY (`author_id` )
    REFERENCES `admin` (`admin_id` )
    ON DELETE SET NULL
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `stores`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `stores` (
  `store_id` SMALLINT UNSIGNED NOT NULL AUTO_INCREMENT ,
  `language_code` CHAR(2) NOT NULL DEFAULT 'EN' ,
  `currency_code` CHAR(3) NOT NULL ,
  `image_id` BIGINT UNSIGNED NULL ,
  `description_id` BIGINT UNSIGNED NOT NULL ,
  `url_rewrite_id` BIGINT UNSIGNED NOT NULL ,
  `hours` TINYTEXT NOT NULL ,
  `telephone` VARCHAR(16) NOT NULL ,
  `email` VARCHAR(255) NOT NULL ,
  `contact_name` VARCHAR(255) NOT NULL DEFAULT 'Contact Name' ,
  `default_contact_id` BIGINT UNSIGNED NULL ,
  `date_added` DATETIME NOT NULL ,
  `date_modified` DATETIME NOT NULL ,
  `product_count` INT UNSIGNED NOT NULL DEFAULT '0' ,
  `active` TINYINT(1) NOT NULL ,
  `notification_profile_new_order` VARCHAR(60) NOT NULL ,
  `notification_profile_confirm_delivery` VARCHAR(60) NOT NULL ,
  `timezone` VARCHAR(160) NOT NULL DEFAULT 'Europe/London' ,
  `theme_id` VARCHAR(255) NULL DEFAULT 'default' ,
  `default_page_id` BIGINT UNSIGNED NULL ,
  `label_color` VARCHAR(45) NULL DEFAULT 'blue' ,
  `payment_processors` TINYTEXT NULL ,
  PRIMARY KEY (`store_id`) ,
  UNIQUE INDEX `shop_id_UNIQUE` (`store_id` ASC) ,
  INDEX `fk_shops_images1_idx` (`image_id` ASC) ,
  INDEX `fk_stores_currencies1_idx` (`currency_code` ASC) ,
  INDEX `fk_stores_url_rewrite1_idx` (`url_rewrite_id` ASC) ,
  INDEX `fk_stores_locations1_idx` (`default_contact_id` ASC) ,
  INDEX `fk_store_default_page_id_idx` (`default_page_id` ASC) ,
  INDEX `fk_stores_description1_idx` (`description_id` ASC) ,
  CONSTRAINT `fk_shops_languages1`
    FOREIGN KEY (`language_code` )
    REFERENCES `languages` (`language_code` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_shops_images1`
    FOREIGN KEY (`image_id` )
    REFERENCES `images` (`image_id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_stores_currencies1`
    FOREIGN KEY (`currency_code` )
    REFERENCES `currencies` (`currency_code` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_default_contact_id`
    FOREIGN KEY (`default_contact_id` )
    REFERENCES `address_book` (`contact_id` )
    ON DELETE SET NULL
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_stores_description1`
    FOREIGN KEY (`description_id` )
    REFERENCES `content` (`content_id` )
    ON DELETE RESTRICT
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_stores_url_rewrite1`
    FOREIGN KEY (`url_rewrite_id` )
    REFERENCES `url_rewrite` (`url_rewrite_id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_store_default_page_id`
    FOREIGN KEY (`default_page_id` )
    REFERENCES `content` (`content_id` )
    ON DELETE RESTRICT
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `orders`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `orders` (
  `order_id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT ,
  `store_id` SMALLINT UNSIGNED NULL ,
  `store_name` VARCHAR(160) NOT NULL ,
  `order_date` DATETIME NOT NULL ,
  `date_modified` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP ,
  `customer_id` INT(11) UNSIGNED NOT NULL ,
  `shipping_option_id` INT UNSIGNED NULL ,
  `shipping_description` VARCHAR(160) NOT NULL ,
  `shipping_charges` DECIMAL(10,4) NOT NULL DEFAULT '0.0000' ,
  `shipping_before_tax` DECIMAL(10,4) NOT NULL DEFAULT '0.0000' ,
  `total_cost` DECIMAL(15,4) NOT NULL DEFAULT '0.0000' ,
  `product_tax_json` TEXT NULL ,
  `shipping_tax_json` TEXT NULL ,
  `currency_code` CHAR(3) NOT NULL ,
  `exp_delivery_date` DATETIME NOT NULL ,
  `order_processed_by` INT(11) UNSIGNED NULL ,
  `order_status` ENUM('pending', 'shipped', 'delivered', 'cancelled') NOT NULL DEFAULT 'pending' ,
  `payment_status` ENUM('pending', 'paid', 'cash on delivery', 'bank transfer', 'cancelled', 'failed') NOT NULL DEFAULT 'pending' ,
  `billing_address_id` BIGINT UNSIGNED NULL ,
  `shipping_address_id` BIGINT UNSIGNED NOT NULL ,
  `payment_processor_id` VARCHAR(160) NULL ,
  PRIMARY KEY (`order_id`) ,
  INDEX `fk_orders_customers1_idx` (`customer_id` ASC) ,
  INDEX `fk_orders_currencies1_idx` (`currency_code` ASC) ,
  INDEX `fk_orders_stores1_idx` (`store_id` ASC) ,
  INDEX `fk_billing_address_id_idx` (`billing_address_id` ASC) ,
  CONSTRAINT `fk_orders_customers1`
    FOREIGN KEY (`customer_id` )
    REFERENCES `customers` (`customer_id` )
    ON DELETE CASCADE
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_orders_currencies1`
    FOREIGN KEY (`currency_code` )
    REFERENCES `currencies` (`currency_code` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_orders_stores1`
    FOREIGN KEY (`store_id` )
    REFERENCES `stores` (`store_id` )
    ON DELETE CASCADE
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_billing_address_id`
    FOREIGN KEY (`billing_address_id` )
    REFERENCES `address_book` (`contact_id` )
    ON DELETE RESTRICT
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `order_status_history`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `order_status_history` (
  `order_status_history_id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT ,
  `order_id` INT(11) UNSIGNED NOT NULL ,
  `order_status_id` INT(5) NOT NULL DEFAULT '0' ,
  `date_added` DATETIME NOT NULL DEFAULT '0001-01-01 00:00:00' ,
  `customer_notified` INT(1) NULL DEFAULT '0' ,
  `comments` TEXT NULL DEFAULT NULL ,
  PRIMARY KEY (`order_status_history_id`) ,
  INDEX `idx_orders_id_status_id_zen` (`order_id` ASC, `order_status_id` ASC) ,
  INDEX `fk_order_status_history_orders1_idx` (`order_id` ASC) )
ENGINE = MyISAM;


-- -----------------------------------------------------
-- Table `products_notifications`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `products_notifications` (
  `products_id` INT(11) NOT NULL DEFAULT '0' ,
  `customers_id` INT(11) NOT NULL DEFAULT '0' ,
  `date_added` DATETIME NOT NULL DEFAULT '0001-01-01 00:00:00' ,
  PRIMARY KEY (`products_id`, `customers_id`) )
ENGINE = MyISAM;


-- -----------------------------------------------------
-- Table `project_version`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `project_version` (
  `project_version_id` TINYINT(3) NOT NULL AUTO_INCREMENT ,
  `project_version_key` VARCHAR(40) NOT NULL DEFAULT '' ,
  `project_version_major` VARCHAR(20) NOT NULL DEFAULT '' ,
  `project_version_minor` VARCHAR(20) NOT NULL DEFAULT '' ,
  `project_version_patch1` VARCHAR(20) NOT NULL DEFAULT '' ,
  `project_version_patch2` VARCHAR(20) NOT NULL DEFAULT '' ,
  `project_version_patch1_source` VARCHAR(20) NOT NULL DEFAULT '' ,
  `project_version_patch2_source` VARCHAR(20) NOT NULL DEFAULT '' ,
  `project_version_comment` VARCHAR(250) NOT NULL DEFAULT '' ,
  `project_version_date_applied` DATETIME NOT NULL DEFAULT '0001-01-01 01:01:01' ,
  PRIMARY KEY (`project_version_id`) ,
  UNIQUE INDEX `idx_project_version_key_zen` (`project_version_key` ASC) )
ENGINE = MyISAM
COMMENT = 'Database Version Tracking';


-- -----------------------------------------------------
-- Table `project_version_history`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `project_version_history` (
  `project_version_id` TINYINT(3) NOT NULL AUTO_INCREMENT ,
  `project_version_key` VARCHAR(40) NOT NULL DEFAULT '' ,
  `project_version_major` VARCHAR(20) NOT NULL DEFAULT '' ,
  `project_version_minor` VARCHAR(20) NOT NULL DEFAULT '' ,
  `project_version_patch` VARCHAR(20) NOT NULL DEFAULT '' ,
  `project_version_comment` VARCHAR(250) NOT NULL DEFAULT '' ,
  `project_version_date_applied` DATETIME NOT NULL DEFAULT '0001-01-01 01:01:01' ,
  PRIMARY KEY (`project_version_id`) )
ENGINE = MyISAM
COMMENT = 'Database Version Tracking History';


-- -----------------------------------------------------
-- Table `zones`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `zones` (
  `zone_id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT ,
  `zone_name` VARCHAR(255) NOT NULL DEFAULT '' ,
  `list_type` ENUM('exclude', 'normal') NOT NULL DEFAULT 'normal' ,
  `date_added` DATETIME NOT NULL ,
  `last_modified` DATETIME NOT NULL ,
  PRIMARY KEY (`zone_id`) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `tax_rules`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `tax_rules` (
  `tax_rule_id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT ,
  `type` ENUM('product', 'shipping') NOT NULL ,
  `tax_zone_id` INT(11) UNSIGNED NOT NULL ,
  `tax_priority` INT(5) NULL DEFAULT '1' ,
  `tax_rate` DECIMAL(7,4) NOT NULL DEFAULT '0.0000' ,
  `description_id` BIGINT UNSIGNED NOT NULL ,
  `last_modified` DATETIME NOT NULL ,
  `date_added` DATETIME NOT NULL ,
  PRIMARY KEY (`tax_rule_id`) ,
  INDEX `idx_tax_zone_id_zen` (`tax_zone_id` ASC) ,
  INDEX `fk_tax_rates_description1_idx` (`description_id` ASC) ,
  CONSTRAINT `fk_tax_rates_zones1`
    FOREIGN KEY (`tax_zone_id` )
    REFERENCES `zones` (`zone_id` )
    ON DELETE CASCADE
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_tax_rates_description1`
    FOREIGN KEY (`description_id` )
    REFERENCES `content` (`content_id` )
    ON DELETE RESTRICT
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `whos_online`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `whos_online` (
  `customer_id` INT(11) NULL DEFAULT NULL ,
  `full_name` VARCHAR(64) NOT NULL DEFAULT '' ,
  `session_id` VARCHAR(128) NOT NULL DEFAULT '' ,
  `ip_address` VARCHAR(20) NOT NULL DEFAULT '' ,
  `time_entry` VARCHAR(14) NOT NULL DEFAULT '' ,
  `time_last_click` VARCHAR(14) NOT NULL DEFAULT '' ,
  `last_page_url` VARCHAR(255) NOT NULL DEFAULT '' ,
  `host_address` TEXT NOT NULL ,
  `user_agent` VARCHAR(255) NOT NULL DEFAULT '' ,
  INDEX `idx_ip_address_zen` (`ip_address` ASC) ,
  INDEX `idx_session_id_zen` (`session_id` ASC) ,
  INDEX `idx_customer_id_zen` (`customer_id` ASC) ,
  INDEX `idx_time_entry_zen` (`time_entry` ASC) ,
  INDEX `idx_time_last_click_zen` (`time_last_click` ASC) ,
  INDEX `idx_last_page_url_zen` (`last_page_url` ASC) )
ENGINE = MyISAM;


-- -----------------------------------------------------
-- Table `coupon_gv_customer`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `coupon_gv_customer` (
  `customer_id` INT(5) NOT NULL DEFAULT '0' ,
  `amount` DECIMAL(15,4) NOT NULL DEFAULT '0.0000' ,
  PRIMARY KEY (`customer_id`) )
ENGINE = MyISAM;


-- -----------------------------------------------------
-- Table `coupon_gv_queue`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `coupon_gv_queue` (
  `unique_id` INT(5) NOT NULL AUTO_INCREMENT ,
  `customer_id` INT(5) NOT NULL DEFAULT '0' ,
  `order_id` INT(5) NOT NULL DEFAULT '0' ,
  `amount` DECIMAL(15,4) NOT NULL DEFAULT '0.0000' ,
  `date_created` DATETIME NOT NULL DEFAULT '0001-01-01 00:00:00' ,
  `ipaddr` VARCHAR(32) NOT NULL DEFAULT '' ,
  `release_flag` CHAR(1) NOT NULL DEFAULT 'N' ,
  PRIMARY KEY (`unique_id`) ,
  INDEX `idx_cust_id_order_id_zen` (`customer_id` ASC, `order_id` ASC) ,
  INDEX `idx_release_flag_zen` (`release_flag` ASC) )
ENGINE = MyISAM;


-- -----------------------------------------------------
-- Table `coupon_email_track`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `coupon_email_track` (
  `unique_id` INT(11) NOT NULL AUTO_INCREMENT ,
  `coupon_id` INT(11) NOT NULL DEFAULT '0' ,
  `customer_id_sent` INT(11) NOT NULL DEFAULT '0' ,
  `sent_firstname` VARCHAR(32) NULL DEFAULT NULL ,
  `sent_lastname` VARCHAR(32) NULL DEFAULT NULL ,
  `emailed_to` VARCHAR(32) NULL DEFAULT NULL ,
  `date_sent` DATETIME NOT NULL DEFAULT '0001-01-01 00:00:00' ,
  PRIMARY KEY (`unique_id`) ,
  INDEX `idx_coupon_id_zen` (`coupon_id` ASC) )
ENGINE = MyISAM;


-- -----------------------------------------------------
-- Table `coupon_restrict`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `coupon_restrict` (
  `restrict_id` INT(11) NOT NULL AUTO_INCREMENT ,
  `coupon_id` INT(11) NOT NULL DEFAULT '0' ,
  `product_id` INT(11) NOT NULL DEFAULT '0' ,
  `category_id` INT(11) NOT NULL DEFAULT '0' ,
  `coupon_restrict` CHAR(1) NOT NULL DEFAULT 'N' ,
  PRIMARY KEY (`restrict_id`) ,
  INDEX `idx_coup_id_prod_id_zen` (`coupon_id` ASC, `product_id` ASC) )
ENGINE = MyISAM;


-- -----------------------------------------------------
-- Table `order_detail`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `order_detail` (
  `order_detail_id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT ,
  `order_id` INT(11) UNSIGNED NOT NULL ,
  `product_id` INT(11) UNSIGNED NOT NULL ,
  `option_id` BIGINT UNSIGNED NOT NULL ,
  `quantity` FLOAT NOT NULL DEFAULT 1 ,
  `gift_wrap` TINYINT(1) NULL DEFAULT FALSE ,
  `wrapper_id` INT(11) NULL DEFAULT 0 ,
  `price` DECIMAL(15,4) NOT NULL DEFAULT 0 ,
  `original_price` DECIMAL(15,4) NOT NULL DEFAULT 0 ,
  `subtotal` DECIMAL(15,4) NOT NULL DEFAULT 0 ,
  `tax` DECIMAL(7,4) NOT NULL DEFAULT 0 ,
  `tax_json` TEXT NOT NULL ,
  `product_is_free` TINYINT(1) NOT NULL DEFAULT FALSE ,
  `one_time_charges` DECIMAL(15,4) NOT NULL DEFAULT 0 ,
  `item_name` VARCHAR(255) NOT NULL DEFAULT 'New Item' ,
  `item_description` VARCHAR(255) NOT NULL DEFAULT 'no description' ,
  `coupon_id` INT(11) UNSIGNED NOT NULL DEFAULT 0 ,
  `coupon_code` VARCHAR(45) NOT NULL DEFAULT '' ,
  `coupon_discount_amount` DECIMAL(15,4) NOT NULL DEFAULT 0 ,
  `sale_id` INT(11) UNSIGNED NOT NULL DEFAULT 0 ,
  `sale_description` VARCHAR(160) NOT NULL DEFAULT 'Sale' ,
  `sale_discount_amount` DECIMAL(15,4) NOT NULL DEFAULT 0 ,
  `ean` VARCHAR(13) NULL ,
  `upc` VARCHAR(12) NULL ,
  `sku` VARCHAR(32) NULL ,
  `isbn` VARCHAR(45) NULL ,
  `shipping_address_id` BIGINT UNSIGNED NULL DEFAULT NULL ,
  `billing_address_id` BIGINT UNSIGNED NULL DEFAULT NULL ,
  PRIMARY KEY (`order_detail_id`, `order_id`) ,
  INDEX `fk_shipping_address_contact_id_idx` (`shipping_address_id` ASC) ,
  INDEX `fk_billing_address_contact_id_idx` (`billing_address_id` ASC) ,
  CONSTRAINT `fk_order_detail_orders1`
    FOREIGN KEY (`order_id` )
    REFERENCES `orders` (`order_id` )
    ON DELETE CASCADE
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_shipping_address_contact_id`
    FOREIGN KEY (`shipping_address_id` )
    REFERENCES `address_book` (`contact_id` )
    ON DELETE SET NULL
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_billing_address_contact_id`
    FOREIGN KEY (`billing_address_id` )
    REFERENCES `address_book` (`contact_id` )
    ON DELETE SET NULL
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `shipment`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `shipment` (
  `shipment_id` BIGINT UNSIGNED NOT NULL AUTO_INCREMENT ,
  `order_id` INT(11) UNSIGNED NOT NULL ,
  `shipment_date` DATETIME NOT NULL ,
  `status` ENUM('pending', 'success', 'cancelled', 'error', 'failure') NOT NULL DEFAULT 'pending' ,
  `actual_delivery_date` DATETIME NOT NULL ,
  `tracking_number` VARCHAR(35) NULL DEFAULT '' ,
  `comments` TEXT NULL ,
  `date_created` DATETIME NOT NULL ,
  `date_modified` DATETIME NOT NULL ,
  PRIMARY KEY (`shipment_id`) ,
  CONSTRAINT `fk_shipment_orders1`
    FOREIGN KEY (`order_id` )
    REFERENCES `orders` (`order_id` )
    ON DELETE CASCADE
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `text`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `text` (
  `text_id` BIGINT UNSIGNED NOT NULL AUTO_INCREMENT ,
  `date_added` DATETIME NOT NULL ,
  PRIMARY KEY (`text_id`) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `prices`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `prices` (
  `price_id` BIGINT UNSIGNED NOT NULL AUTO_INCREMENT ,
  `type` VARCHAR(10) NOT NULL DEFAULT '(=)' ,
  PRIMARY KEY (`price_id`) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `product_stock`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `product_stock` (
  `stock_id` BIGINT UNSIGNED NOT NULL AUTO_INCREMENT ,
  PRIMARY KEY (`stock_id`) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `product_types`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `product_types` (
  `product_type_id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT ,
  `name` VARCHAR(255) NULL ,
  `name_plural` VARCHAR(255) NULL ,
  `code` VARCHAR(160) NULL ,
  `image_id` BIGINT UNSIGNED NULL DEFAULT NULL ,
  `has_ean` TINYINT(1) NOT NULL DEFAULT '0' ,
  `has_upc` TINYINT(1) NOT NULL DEFAULT '1' ,
  `has_sku` TINYINT(1) NOT NULL DEFAULT '1' ,
  `has_isbn` TINYINT(1) NOT NULL DEFAULT '0' ,
  PRIMARY KEY (`product_type_id`) ,
  INDEX `fk_image_id_idx` (`image_id` ASC) ,
  UNIQUE INDEX `code_UNIQUE` (`code` ASC) ,
  CONSTRAINT `fk_image_id`
    FOREIGN KEY (`image_id` )
    REFERENCES `images` (`image_id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `product_options`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `product_options` (
  `option_id` BIGINT UNSIGNED NOT NULL AUTO_INCREMENT ,
  `product_id` INT(11) UNSIGNED NOT NULL ,
  `product_type_id` INT(11) UNSIGNED NOT NULL ,
  `price_id` BIGINT UNSIGNED NOT NULL ,
  `stock_id` BIGINT UNSIGNED NOT NULL ,
  `image_id` BIGINT UNSIGNED NULL ,
  `ean` VARCHAR(160) NULL ,
  `upc` VARCHAR(160) NULL ,
  `sku` VARCHAR(160) NULL ,
  `isbn` VARCHAR(160) NULL ,
  `is_taxable` TINYINT(1) UNSIGNED NOT NULL DEFAULT 0 ,
  `grams` DECIMAL(15,8) NOT NULL DEFAULT 0 ,
  `weight` DECIMAL(15,8) NOT NULL DEFAULT 0 ,
  `weight_unit` VARCHAR(45) NOT NULL DEFAULT 'kg' ,
  PRIMARY KEY (`option_id`, `product_id`) ,
  INDEX `fk_product_options_products1_idx` (`product_id` ASC) ,
  INDEX `fk_product_options_prices1_idx` (`price_id` ASC) ,
  INDEX `fk_product_options_product_stock1_idx` (`stock_id` ASC) ,
  INDEX `fk_product_options_images1_idx` (`image_id` ASC) ,
  INDEX `fk_options_producttype_id_idx` (`product_type_id` ASC) ,
  CONSTRAINT `fk_product_options_products1`
    FOREIGN KEY (`product_id` )
    REFERENCES `products` (`product_id` )
    ON DELETE CASCADE
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_product_options_prices1`
    FOREIGN KEY (`price_id` )
    REFERENCES `prices` (`price_id` )
    ON DELETE RESTRICT
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_product_options_product_stock1`
    FOREIGN KEY (`stock_id` )
    REFERENCES `product_stock` (`stock_id` )
    ON DELETE RESTRICT
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_product_options_images1`
    FOREIGN KEY (`image_id` )
    REFERENCES `images` (`image_id` )
    ON DELETE SET NULL
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_options_producttype_id`
    FOREIGN KEY (`product_type_id` )
    REFERENCES `product_types` (`product_type_id` )
    ON DELETE RESTRICT
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `agent_roles`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `agent_roles` (
  `role_id` INT UNSIGNED NOT NULL AUTO_INCREMENT ,
  `handle` VARCHAR(160) NOT NULL ,
  `description_id` BIGINT UNSIGNED NOT NULL ,
  `date_created` DATETIME NOT NULL ,
  `last_modified` DATETIME NOT NULL ,
  PRIMARY KEY (`role_id`) ,
  INDEX `role_description_id_idx` (`description_id` ASC) ,
  CONSTRAINT `role_description_id`
    FOREIGN KEY (`description_id` )
    REFERENCES `content` (`content_id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `agents`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `agents` (
  `agent_id` INT UNSIGNED NOT NULL AUTO_INCREMENT ,
  `role_id` INT UNSIGNED NOT NULL ,
  `contact_id` BIGINT UNSIGNED NOT NULL ,
  `logo_image_id` BIGINT UNSIGNED NULL ,
  `code` VARCHAR(45) NOT NULL ,
  `is_active` TINYINT(1) UNSIGNED NOT NULL DEFAULT 0 ,
  `date_added` DATETIME NOT NULL ,
  `last_modified` DATETIME NOT NULL ,
  PRIMARY KEY (`agent_id`) ,
  UNIQUE INDEX `code_UNIQUE` (`code` ASC) ,
  INDEX `agent_role_id_idx` (`role_id` ASC) ,
  INDEX `fk_logo_image_id_idx` (`logo_image_id` ASC) ,
  CONSTRAINT `fk_agent_role_id`
    FOREIGN KEY (`role_id` )
    REFERENCES `agent_roles` (`role_id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_logo_image_id`
    FOREIGN KEY (`logo_image_id` )
    REFERENCES `images` (`image_id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `products`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `products` (
  `product_id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT ,
  `product_type_id` INT(11) UNSIGNED NOT NULL ,
  `description_id` BIGINT UNSIGNED NOT NULL ,
  `default_option_id` BIGINT UNSIGNED NULL ,
  `primary_image_id` BIGINT UNSIGNED NULL ,
  `url_rewrite_id` BIGINT UNSIGNED NOT NULL ,
  `ean` VARCHAR(13) NULL ,
  `upc` VARCHAR(12) NULL ,
  `reference` VARCHAR(32) NULL ,
  `date_added` DATETIME NOT NULL ,
  `last_modified` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP ,
  `date_available` DATETIME NULL ,
  `product_weight` FLOAT NOT NULL DEFAULT '0' ,
  `product_status` TINYINT(1) NOT NULL DEFAULT '0' ,
  `manufacturer_id` INT UNSIGNED NOT NULL ,
  `supplier_id` INT UNSIGNED NOT NULL ,
  `quantity_order_min` FLOAT NOT NULL DEFAULT '1' ,
  `is_free` TINYINT(1) NOT NULL DEFAULT '0' ,
  `is_call` TINYINT(1) NOT NULL DEFAULT '0' ,
  `is_always_free_shipping` TINYINT(1) NOT NULL DEFAULT '0' ,
  `quantity_order_max` FLOAT NOT NULL DEFAULT '0' ,
  `sort_order` INT(11) NOT NULL DEFAULT '0' ,
  `discount_type` TINYINT(1) NOT NULL DEFAULT '0' ,
  `hatched` TINYINT(1) NOT NULL DEFAULT '0' ,
  `featured` TINYINT(1) NOT NULL DEFAULT '0' ,
  `display_status` TINYINT(1) NOT NULL DEFAULT '1' ,
  `total_sold` FLOAT NOT NULL DEFAULT '0' ,
  `views` INT UNSIGNED NOT NULL DEFAULT '0' ,
  `wishlists` INT UNSIGNED NOT NULL DEFAULT '0' ,
  `meta_keywords_text_id` BIGINT UNSIGNED NOT NULL ,
  `meta_description_id` BIGINT UNSIGNED NOT NULL ,
  PRIMARY KEY (`product_id`) ,
  INDEX `idx_products_date_added_zen` (`date_added` ASC) ,
  INDEX `idx_products_status_zen` (`product_status` ASC) ,
  INDEX `idx_products_date_available_zen` (`date_available` ASC) ,
  INDEX `idx_products_ordered_zen` (`total_sold` ASC) ,
  INDEX `idx_products_sort_order_zen` (`sort_order` ASC) ,
  INDEX `idx_manufacturers_id_zen` (`manufacturer_id` ASC) ,
  INDEX `fk_products_product_options1_idx` (`default_option_id` ASC) ,
  INDEX `fk_product_type_id_idx` (`product_type_id` ASC) ,
  INDEX `fk_products_text1_idx` (`meta_keywords_text_id` ASC) ,
  INDEX `fk_product_primary_image_idx` (`primary_image_id` ASC) ,
  INDEX `fk_product_description_id_idx` (`description_id` ASC) ,
  INDEX `fk_products_text2_idx` (`meta_description_id` ASC) ,
  INDEX `fk_product_supplier_id_idx` (`supplier_id` ASC) ,
  CONSTRAINT `fk_products_text1`
    FOREIGN KEY (`meta_keywords_text_id` )
    REFERENCES `text` (`text_id` )
    ON DELETE RESTRICT
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_products_text2`
    FOREIGN KEY (`meta_description_id` )
    REFERENCES `content` (`content_id` )
    ON DELETE RESTRICT
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_products_product_options1`
    FOREIGN KEY (`default_option_id` )
    REFERENCES `product_options` (`option_id` )
    ON DELETE SET NULL
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_product_type_id`
    FOREIGN KEY (`product_type_id` )
    REFERENCES `product_types` (`product_type_id` )
    ON DELETE CASCADE
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_product_description_id`
    FOREIGN KEY (`description_id` )
    REFERENCES `content` (`content_id` )
    ON DELETE RESTRICT
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_product_primary_image`
    FOREIGN KEY (`primary_image_id` )
    REFERENCES `images` (`image_id` )
    ON DELETE SET NULL
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_product_manufacturer_id`
    FOREIGN KEY (`manufacturer_id` )
    REFERENCES `agents` (`agent_id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_product_supplier_id`
    FOREIGN KEY (`supplier_id` )
    REFERENCES `agents` (`agent_id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `collections`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `collections` (
  `collection_id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT ,
  `count` BIGINT UNSIGNED NOT NULL DEFAULT '0' ,
  PRIMARY KEY (`collection_id`) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `tags`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `tags` (
  `tag_id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT ,
  `store_id` SMALLINT UNSIGNED NOT NULL ,
  `collection_id` INT(11) UNSIGNED NULL DEFAULT NULL ,
  `is_root` TINYINT(1) NOT NULL DEFAULT 0 ,
  `description_id` BIGINT UNSIGNED NOT NULL ,
  `url_rewrite_id` BIGINT UNSIGNED NULL ,
  `image_id` BIGINT UNSIGNED NULL ,
  `sort_order` INT(3) NULL DEFAULT 0 ,
  `date_added` DATETIME NOT NULL ,
  `last_modified` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP ,
  `status` TINYINT(1) NOT NULL DEFAULT 0 ,
  `meta_keywords_text_id` BIGINT UNSIGNED NOT NULL ,
  `meta_description_text_id` BIGINT UNSIGNED NOT NULL ,
  `parent_id` INT(11) UNSIGNED NULL DEFAULT NULL ,
  PRIMARY KEY (`tag_id`) ,
  INDEX `idx_parent_id_tag_id` (`tag_id` ASC) ,
  INDEX `idx_sort_order` (`sort_order` ASC) ,
  INDEX `fk_tags_url_rewrite1_idx` (`url_rewrite_id` ASC) ,
  INDEX `fk_tags_text1_idx` (`meta_keywords_text_id` ASC) ,
  INDEX `fk_tags_text2_idx` (`meta_description_text_id` ASC) ,
  INDEX `fk_parent_id_idx` (`parent_id` ASC) ,
  INDEX `fk_banner_image_id_idx` (`image_id` ASC) ,
  INDEX `fk_tag_description_id_idx` (`description_id` ASC) ,
  INDEX `fk_tag_store_id_idx` (`store_id` ASC) ,
  INDEX `fk_collection_id_idx` (`collection_id` ASC) ,
  CONSTRAINT `fk_tags_url_rewrite1`
    FOREIGN KEY (`url_rewrite_id` )
    REFERENCES `url_rewrite` (`url_rewrite_id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_tags_text1`
    FOREIGN KEY (`meta_keywords_text_id` )
    REFERENCES `text` (`text_id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_tags_text2`
    FOREIGN KEY (`meta_description_text_id` )
    REFERENCES `text` (`text_id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_parent_id`
    FOREIGN KEY (`parent_id` )
    REFERENCES `tags` (`tag_id` )
    ON DELETE SET NULL
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_banner_image_id`
    FOREIGN KEY (`image_id` )
    REFERENCES `images` (`image_id` )
    ON DELETE SET NULL
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_tag_description_id`
    FOREIGN KEY (`description_id` )
    REFERENCES `content` (`content_id` )
    ON DELETE RESTRICT
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_tag_store_id`
    FOREIGN KEY (`store_id` )
    REFERENCES `stores` (`store_id` )
    ON DELETE CASCADE
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_tag_collection_id`
    FOREIGN KEY (`collection_id` )
    REFERENCES `collections` (`collection_id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `products_discount_quantity`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `products_discount_quantity` (
  `discount_id` INT(4) UNSIGNED NOT NULL DEFAULT '0' ,
  `products_id` INT(11) UNSIGNED NOT NULL DEFAULT '0' ,
  `discount_qty` FLOAT NOT NULL DEFAULT '0' ,
  `discount_price` DECIMAL(15,4) NOT NULL DEFAULT '0.0000' ,
  INDEX `idx_id_qty_zen` (`products_id` ASC, `discount_qty` ASC) )
ENGINE = MyISAM;


-- -----------------------------------------------------
-- Table `featured`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `featured` (
  `featured_id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT ,
  `product_id` INT(11) UNSIGNED NOT NULL DEFAULT '0' ,
  `featured_date_added` DATETIME NOT NULL ,
  `featured_last_modified` TIMESTAMP NOT NULL ,
  `expires_date` DATE NOT NULL DEFAULT '1000-01-01 00:00:00' ,
  `date_status_change` DATETIME NULL DEFAULT NULL ,
  `status` INT(1) NOT NULL DEFAULT '1' ,
  `featured_date_available` DATE NOT NULL DEFAULT '1000-01-01 00:00:00' ,
  PRIMARY KEY (`featured_id`, `product_id`) ,
  INDEX `idx_status_zen` (`status` ASC) ,
  INDEX `idx_products_id_zen` (`product_id` ASC) ,
  INDEX `idx_date_avail_zen` (`featured_date_available` ASC) ,
  INDEX `idx_expires_date_zen` (`expires_date` ASC) ,
  INDEX `fk_product_id_idx` (`product_id` ASC) ,
  CONSTRAINT `fk_product_id0`
    FOREIGN KEY (`product_id` )
    REFERENCES `products` (`product_id` )
    ON DELETE CASCADE
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `shipping_options`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `shipping_options` (
  `shipping_option_id` INT UNSIGNED NOT NULL AUTO_INCREMENT ,
  `store_id` SMALLINT UNSIGNED NOT NULL ,
  `zone_id` INT(11) UNSIGNED NOT NULL ,
  `carrier_id` INT UNSIGNED NOT NULL ,
  `price_id` BIGINT UNSIGNED NOT NULL ,
  `is_taxable` TINYINT(1) UNSIGNED NOT NULL DEFAULT 0 ,
  `rate_is_per` ENUM('weight', 'unit', 'volume', 'flat') NOT NULL DEFAULT 'unit' ,
  `description_id` BIGINT UNSIGNED NOT NULL ,
  `max_delivery_days` VARCHAR(45) NULL ,
  `active` TINYINT(1) UNSIGNED NOT NULL DEFAULT 0 ,
  `date_added` DATETIME NOT NULL ,
  `last_modified` DATETIME NOT NULL ,
  PRIMARY KEY (`shipping_option_id`, `store_id`) ,
  INDEX `fk_shipping_options_prices1_idx` (`price_id` ASC) ,
  INDEX `fk_shipping_options_zones1_idx` (`zone_id` ASC) ,
  INDEX `fk_shipping_options_stores1_idx` (`store_id` ASC) ,
  INDEX `fk_shipping_options_description1_idx` (`description_id` ASC) ,
  INDEX `fk_shipping_options_carriers1_idx` (`carrier_id` ASC) ,
  CONSTRAINT `fk_shipping_options_prices1`
    FOREIGN KEY (`price_id` )
    REFERENCES `prices` (`price_id` )
    ON DELETE RESTRICT
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_shipping_options_description1`
    FOREIGN KEY (`description_id` )
    REFERENCES `content` (`content_id` )
    ON DELETE RESTRICT
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_shipping_options_carriers1`
    FOREIGN KEY (`carrier_id` )
    REFERENCES `agents` (`agent_id` )
    ON DELETE CASCADE
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_shipping_options_zones1`
    FOREIGN KEY (`zone_id` )
    REFERENCES `zones` (`zone_id` )
    ON DELETE CASCADE
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_shipping_options_stores1`
    FOREIGN KEY (`store_id` )
    REFERENCES `stores` (`store_id` )
    ON DELETE CASCADE
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `cart`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `cart` (
  `cart_id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT ,
  `store_id` SMALLINT UNSIGNED NOT NULL ,
  `customer_id` INT(11) UNSIGNED NULL ,
  `created` DATETIME NOT NULL ,
  `modified` DATETIME NOT NULL ,
  `status` TINYINT(1) NOT NULL DEFAULT TRUE ,
  `secure_key` VARCHAR(255) NOT NULL ,
  `shipping_option_id` INT UNSIGNED NULL DEFAULT NULL ,
  `shipping_charges` DECIMAL(10,4) NULL DEFAULT NULL ,
  `shipping_charges_before_tax` DECIMAL(10,4) NULL DEFAULT NULL ,
  `product_tax_json` TEXT NULL ,
  `shipping_tax_json` TEXT NULL ,
  `currency_code` CHAR(3) NOT NULL ,
  `billing_address_id` BIGINT UNSIGNED NOT NULL ,
  `shipping_address_id` BIGINT UNSIGNED NOT NULL ,
  `payment_processor_id` VARCHAR(160) NULL ,
  PRIMARY KEY (`cart_id`) ,
  INDEX `fk_cart_products1_idx` (`customer_id` ASC) ,
  INDEX `fk_cart_stores1_idx` (`store_id` ASC) ,
  INDEX `fk_cart_shipping_option_id_idx` (`shipping_option_id` ASC) ,
  INDEX `fk_cart_currency_code_idx` (`currency_code` ASC) ,
  INDEX `fk_cart_billing_address_id_idx` (`billing_address_id` ASC) ,
  INDEX `fk_shipping_address_id_idx` (`shipping_address_id` ASC) ,
  CONSTRAINT `fk_cart_customers`
    FOREIGN KEY (`customer_id` )
    REFERENCES `customers` (`customer_id` )
    ON DELETE CASCADE
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_cart_stores1`
    FOREIGN KEY (`store_id` )
    REFERENCES `stores` (`store_id` )
    ON DELETE CASCADE
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_cart_shipping_option_id`
    FOREIGN KEY (`shipping_option_id` )
    REFERENCES `shipping_options` (`shipping_option_id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_cart_currency_code`
    FOREIGN KEY (`currency_code` )
    REFERENCES `currencies` (`currency_code` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_cart_billing_address_id`
    FOREIGN KEY (`billing_address_id` )
    REFERENCES `address_book` (`contact_id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_shipping_address_id`
    FOREIGN KEY (`shipping_address_id` )
    REFERENCES `address_book` (`contact_id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `coupons`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `coupons` (
  `coupon_id` INT UNSIGNED NOT NULL AUTO_INCREMENT ,
  `store_id` SMALLINT UNSIGNED NOT NULL ,
  `collection_id` INT(11) UNSIGNED NULL DEFAULT NULL ,
  `code` VARCHAR(45) NOT NULL ,
  `discount_type` VARCHAR(15) NOT NULL DEFAULT '(%)' ,
  `amount` DECIMAL(15,4) NOT NULL DEFAULT 0 ,
  `minimum_order` DECIMAL(15,4) NOT NULL DEFAULT 1 ,
  `start_date` DATETIME NOT NULL ,
  `expire_date` DATETIME NOT NULL ,
  `indefinite` TINYINT(1) UNSIGNED NOT NULL DEFAULT 0 ,
  `uses_per_coupon` INT NOT NULL DEFAULT 1 ,
  `uses_per_user` INT NOT NULL DEFAULT 1 ,
  `is_active` TINYINT(1) UNSIGNED NOT NULL DEFAULT 0 ,
  `restrict_to_zone` INT UNSIGNED NULL ,
  `date_created` DATETIME NOT NULL ,
  `last_modified` DATETIME NOT NULL ,
  PRIMARY KEY (`coupon_id`, `store_id`) ,
  UNIQUE INDEX `code_UNIQUE` (`code` ASC) ,
  INDEX `fk_coupon_collection_id_idx` (`collection_id` ASC) ,
  CONSTRAINT `fk_coupon_collection_id`
    FOREIGN KEY (`collection_id` )
    REFERENCES `collections` (`collection_id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `cart_items`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `cart_items` (
  `cart_item_id` BIGINT UNSIGNED NOT NULL AUTO_INCREMENT ,
  `cart_id` INT(11) UNSIGNED NOT NULL ,
  `store_id` SMALLINT UNSIGNED NOT NULL ,
  `product_id` INT(11) UNSIGNED NOT NULL ,
  `option_id` BIGINT UNSIGNED NOT NULL ,
  `coupon_id` INT UNSIGNED NULL ,
  `quantity` FLOAT NOT NULL DEFAULT 1 ,
  `date_added` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ,
  `shipping_address_id` BIGINT UNSIGNED NULL DEFAULT NULL ,
  `billing_address_id` BIGINT UNSIGNED NULL DEFAULT NULL ,
  PRIMARY KEY (`cart_item_id`, `cart_id`, `product_id`, `option_id`) ,
  INDEX `fk_cart_items_products1_idx` (`product_id` ASC) ,
  INDEX `fk_cart_items_options1_idx` (`option_id` ASC) ,
  INDEX `fk_cart_items_stores1_idx` (`store_id` ASC) ,
  INDEX `fk_cart_items_coupons1_idx` (`coupon_id` ASC) ,
  INDEX `fk_ci_shipping_address_id_idx` (`shipping_address_id` ASC) ,
  INDEX `fk_ci_billing_address_id_idx` (`billing_address_id` ASC) ,
  CONSTRAINT `fk_cart_items_cart10`
    FOREIGN KEY (`cart_id` )
    REFERENCES `cart` (`cart_id` )
    ON DELETE CASCADE
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_cart_items_products1`
    FOREIGN KEY (`product_id` )
    REFERENCES `products` (`product_id` )
    ON DELETE CASCADE
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_cart_items_options1`
    FOREIGN KEY (`option_id` )
    REFERENCES `product_options` (`option_id` )
    ON DELETE CASCADE
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_cart_items_stores1`
    FOREIGN KEY (`store_id` )
    REFERENCES `stores` (`store_id` )
    ON DELETE CASCADE
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_cart_items_coupons1`
    FOREIGN KEY (`coupon_id` )
    REFERENCES `coupons` (`coupon_id` )
    ON DELETE SET NULL
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_ci_shipping_address_id`
    FOREIGN KEY (`shipping_address_id` )
    REFERENCES `address_book` (`contact_id` )
    ON DELETE SET NULL
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_ci_billing_address_id`
    FOREIGN KEY (`billing_address_id` )
    REFERENCES `address_book` (`contact_id` )
    ON DELETE SET NULL
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `sessions`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `sessions` (
  `sesskey` VARCHAR(255) NOT NULL DEFAULT '' ,
  `expiry` DATETIME NOT NULL ,
  `value` MEDIUMBLOB NOT NULL ,
  PRIMARY KEY (`sesskey`) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `db_cache`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `db_cache` (
  `cache_entry_name` VARCHAR(64) NOT NULL DEFAULT '' ,
  `cache_data` MEDIUMBLOB NULL DEFAULT NULL ,
  `cache_entry_created` INT(15) NULL DEFAULT NULL ,
  PRIMARY KEY (`cache_entry_name`) )
ENGINE = MyISAM;


-- -----------------------------------------------------
-- Table `privileges`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `privileges` (
  `privilege_id` VARCHAR(255) NOT NULL ,
  `description_id` BIGINT UNSIGNED NOT NULL ,
  PRIMARY KEY (`privilege_id`) ,
  UNIQUE INDEX `privilege_id_UNIQUE` (`privilege_id` ASC) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `admin_activity_log`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `admin_activity_log` (
  `log_id` BIGINT(15) UNSIGNED NOT NULL AUTO_INCREMENT ,
  `admin_id` INT(11) UNSIGNED NOT NULL ,
  `access_date` DATETIME NOT NULL DEFAULT '0001-01-01 00:00:00' ,
  `page_accessed` VARCHAR(80) NOT NULL DEFAULT '' ,
  `page_parameters` TEXT NULL ,
  `ip_address` VARCHAR(20) NOT NULL DEFAULT '' ,
  `flagged` TINYINT NOT NULL DEFAULT '0' ,
  `attention` VARCHAR(255) NOT NULL DEFAULT '' ,
  `gzpost` MEDIUMBLOB NOT NULL ,
  PRIMARY KEY (`log_id`) ,
  INDEX `idx_page_accessed_zen` (`page_accessed` ASC) ,
  INDEX `idx_access_date_zen` (`access_date` ASC) ,
  INDEX `idx_flagged_zen` (`flagged` ASC) ,
  INDEX `idx_ip_zen` (`ip_address` ASC) ,
  INDEX `fk_admin_activity_log_admin1_idx` (`admin_id` ASC) ,
  CONSTRAINT `fk_admin_activity_log_admin1`
    FOREIGN KEY (`admin_id` )
    REFERENCES `admin` (`admin_id` )
    ON DELETE CASCADE
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `notification_profile`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `notification_profile` (
  `notification_id` SMALLINT(4) UNSIGNED NOT NULL AUTO_INCREMENT ,
  `keyword` VARCHAR(60) NOT NULL ,
  `scope` VARCHAR(255) NOT NULL ,
  `description_id` BIGINT UNSIGNED NOT NULL ,
  `icon` VARCHAR(255) NOT NULL DEFAULT 'no-icon.png' ,
  `entity_name` VARCHAR(60) NOT NULL ,
  `entity_name_plural` VARCHAR(60) NOT NULL ,
  `notification_text` TINYTEXT NOT NULL ,
  `send_after_period` INT NOT NULL DEFAULT 1 ,
  `send_after_interval` ENUM('second', 'minute', 'hour', 'day', 'week', 'month', 'year') NOT NULL DEFAULT 'day' ,
  `send_after_increments` SMALLINT UNSIGNED NOT NULL DEFAULT 5 ,
  `increments_since_last_sent` INT UNSIGNED NOT NULL DEFAULT 0 ,
  `last_sent_at` BIGINT UNSIGNED NOT NULL DEFAULT 0 ,
  `request_uri` VARCHAR(255) NOT NULL DEFAULT '/' ,
  `date_added` DATETIME NOT NULL ,
  `last_modified` DATETIME NOT NULL ,
  PRIMARY KEY (`notification_id`) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `alerts`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `alerts` (
  `alert_id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT ,
  `admin_id` INT(11) UNSIGNED NOT NULL ,
  `notification_id` SMALLINT(4) UNSIGNED NOT NULL ,
  `to_email` TINYINT(1) NOT NULL DEFAULT FALSE ,
  `to_dashboard` TINYINT(1) NOT NULL DEFAULT FALSE ,
  `is_displayed` TINYINT(1) NOT NULL DEFAULT FALSE ,
  `is_read` TINYINT(1) NOT NULL DEFAULT FALSE ,
  `tally` INT UNSIGNED NOT NULL DEFAULT 0 ,
  PRIMARY KEY (`alert_id`, `admin_id`, `notification_id`) ,
  INDEX `notification_recipient_admin_id_idx` (`admin_id` ASC) ,
  INDEX `notifications_notification_id_idx` (`notification_id` ASC) ,
  CONSTRAINT `notification_recipient_admin_id0`
    FOREIGN KEY (`admin_id` )
    REFERENCES `admin` (`admin_id` )
    ON DELETE CASCADE
    ON UPDATE NO ACTION,
  CONSTRAINT `notifications_notification_id0`
    FOREIGN KEY (`notification_id` )
    REFERENCES `notification_profile` (`notification_id` )
    ON DELETE CASCADE
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `customer_ua_trend`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `customer_ua_trend` (
  `pk` BIGINT(15) UNSIGNED NOT NULL AUTO_INCREMENT ,
  `customer_id` INT(11) UNSIGNED NOT NULL ,
  `fingerprint` TEXT NOT NULL ,
  `login_tally` INT(11) UNSIGNED NOT NULL DEFAULT 0 ,
  `last_login` DATETIME NOT NULL ,
  `date_added` DATETIME NOT NULL ,
  PRIMARY KEY (`pk`) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `admin_geoloc_trend`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `admin_geoloc_trend` (
  `pk` BIGINT(15) UNSIGNED NOT NULL AUTO_INCREMENT ,
  `admin_id` INT(11) NULL ,
  `country_iso2_code` CHAR(2) NOT NULL ,
  `city` VARCHAR(45) NULL ,
  `login_tally` INT(11) UNSIGNED NOT NULL DEFAULT 0 ,
  `last_login` DATETIME NOT NULL ,
  `date_added` DATETIME NOT NULL ,
  PRIMARY KEY (`pk`) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `config`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `config` (
  `config_id` VARCHAR(255) NOT NULL ,
  `param_id` INT UNSIGNED NOT NULL AUTO_INCREMENT ,
  `name` VARCHAR(255) NOT NULL ,
  `value` BLOB NOT NULL ,
  `last_modified` TIMESTAMP NULL ,
  PRIMARY KEY (`param_id`, `config_id`) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `logger`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `logger` (
  `logger_id` VARCHAR(60) NOT NULL ,
  `log_id` BIGINT UNSIGNED NOT NULL AUTO_INCREMENT ,
  `value` TEXT NOT NULL ,
  `logged_at` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ,
  PRIMARY KEY (`log_id`, `logger_id`) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `fulltext_catalog_index_isam`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `fulltext_catalog_index_isam` (
  `product_id` INT(11) UNSIGNED NOT NULL ,
  `keywords` TEXT NULL ,
  PRIMARY KEY (`product_id`) ,
  FULLTEXT INDEX `idx_data_index` (`keywords` ASC) )
ENGINE = MyISAM;


-- -----------------------------------------------------
-- Table `attribute_types`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `attribute_types` (
  `attribute_type_id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT ,
  `name` VARCHAR(255) NULL DEFAULT '' ,
  `name_plural` VARCHAR(255) NULL DEFAULT '' ,
  `code` VARCHAR(160) NULL ,
  `sort_order` INT NULL ,
  PRIMARY KEY (`attribute_type_id`) ,
  UNIQUE INDEX `code_UNIQUE` (`code` ASC) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `attributes`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `attributes` (
  `attribute_id` BIGINT UNSIGNED NOT NULL AUTO_INCREMENT ,
  `attribute_type_id` INT(11) UNSIGNED NOT NULL ,
  `value` VARCHAR(255) NULL DEFAULT ' ' ,
  `sort_order` INT UNSIGNED NOT NULL DEFAULT '0' ,
  `code` VARCHAR(160) NULL ,
  PRIMARY KEY (`attribute_id`) ,
  INDEX `fk_attribute_type_id_idx` (`attribute_type_id` ASC) ,
  UNIQUE INDEX `code_UNIQUE` (`code` ASC) ,
  CONSTRAINT `fk_attribute_type_id`
    FOREIGN KEY (`attribute_type_id` )
    REFERENCES `attribute_types` (`attribute_type_id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `product_images`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `product_images` (
  `product_id` INT(11) UNSIGNED NOT NULL ,
  `image_id` BIGINT UNSIGNED NOT NULL ,
  `is_primary` TINYINT(1) NOT NULL DEFAULT '0' ,
  PRIMARY KEY (`product_id`, `image_id`) ,
  INDEX `fk_product_images_products1_idx` (`product_id` ASC) ,
  INDEX `fk_product_images_images1_idx` (`image_id` ASC) ,
  CONSTRAINT `fk_product_images_products1`
    FOREIGN KEY (`product_id` )
    REFERENCES `products` (`product_id` )
    ON DELETE CASCADE
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_product_images_images1`
    FOREIGN KEY (`image_id` )
    REFERENCES `images` (`image_id` )
    ON DELETE CASCADE
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `price_values`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `price_values` (
  `price_id` BIGINT UNSIGNED NOT NULL ,
  `store_id` SMALLINT UNSIGNED NOT NULL ,
  `value` DECIMAL(15,4) NOT NULL DEFAULT '0.0000' ,
  PRIMARY KEY (`price_id`, `store_id`) ,
  INDEX `fk_price_values_prices1_idx` (`price_id` ASC) ,
  INDEX `fk_price_values_stores1_idx` (`store_id` ASC) ,
  CONSTRAINT `fk_price_values_prices1`
    FOREIGN KEY (`price_id` )
    REFERENCES `prices` (`price_id` )
    ON DELETE CASCADE
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_price_values_stores1`
    FOREIGN KEY (`store_id` )
    REFERENCES `stores` (`store_id` )
    ON DELETE CASCADE
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `product_stock_quantity`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `product_stock_quantity` (
  `stock_id` BIGINT UNSIGNED NOT NULL ,
  `store_id` SMALLINT UNSIGNED NOT NULL ,
  `quantity` FLOAT UNSIGNED NOT NULL DEFAULT 0 ,
  PRIMARY KEY (`stock_id`, `store_id`) ,
  INDEX `fk_product_stock_quantity_shops1_idx` (`store_id` ASC) ,
  CONSTRAINT `fk_product_stock_quantity_product_stock1`
    FOREIGN KEY (`stock_id` )
    REFERENCES `product_stock` (`stock_id` )
    ON DELETE CASCADE
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_product_stock_quantity_shops1`
    FOREIGN KEY (`store_id` )
    REFERENCES `stores` (`store_id` )
    ON DELETE CASCADE
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `store_locations`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `store_locations` (
  `store_id` SMALLINT UNSIGNED NOT NULL ,
  `contact_id` BIGINT UNSIGNED NOT NULL ,
  PRIMARY KEY (`store_id`, `contact_id`) ,
  INDEX `fk_shop_locations_locations1_idx` (`contact_id` ASC) ,
  CONSTRAINT `fk_shop_locations_shops1`
    FOREIGN KEY (`store_id` )
    REFERENCES `stores` (`store_id` )
    ON DELETE CASCADE
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_shop_locations_address_book1`
    FOREIGN KEY (`contact_id` )
    REFERENCES `address_book` (`contact_id` )
    ON DELETE CASCADE
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `permissions`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `permissions` (
  `role_id` SMALLINT UNSIGNED NOT NULL ,
  `privilege_id` VARCHAR(255) NOT NULL ,
  `permission` TINYINT(1) UNSIGNED NOT NULL ,
  `last_modified` DATETIME NOT NULL ,
  PRIMARY KEY (`role_id`, `privilege_id`) ,
  INDEX `fk_permissions_admin_roles1_idx` (`role_id` ASC) ,
  CONSTRAINT `fk_permissions_admin_roles1`
    FOREIGN KEY (`role_id` )
    REFERENCES `admin_roles` (`role_id` )
    ON DELETE CASCADE
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_permissions_privileges1`
    FOREIGN KEY (`privilege_id` )
    REFERENCES `privileges` (`privilege_id` )
    ON DELETE CASCADE
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `text_content`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `text_content` (
  `text_id` BIGINT UNSIGNED NOT NULL ,
  `language_code` CHAR(2) NOT NULL ,
  `text` TEXT NULL ,
  `last_modified` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP ,
  PRIMARY KEY (`language_code`, `text_id`) ,
  INDEX `fk_products_description_languages1_idx` (`language_code` ASC) ,
  INDEX `fk_text_content_text1_idx` (`text_id` ASC) ,
  CONSTRAINT `fk_products_description_languages100`
    FOREIGN KEY (`language_code` )
    REFERENCES `languages` (`language_code` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_text_content_text1`
    FOREIGN KEY (`text_id` )
    REFERENCES `text` (`text_id` )
    ON DELETE CASCADE
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `zones_to_countries`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `zones_to_countries` (
  `zone_id` INT(11) UNSIGNED NOT NULL ,
  `country_code` CHAR(2) NOT NULL ,
  PRIMARY KEY (`zone_id`, `country_code`) ,
  INDEX `fk_zones_to_countries_zones1_idx` (`zone_id` ASC) ,
  INDEX `fk_zones_to_countries_countries1_idx` (`country_code` ASC) ,
  CONSTRAINT `fk_zones_to_countries_zones1`
    FOREIGN KEY (`zone_id` )
    REFERENCES `zones` (`zone_id` )
    ON DELETE CASCADE
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_zones_to_countries_countries1`
    FOREIGN KEY (`country_code` )
    REFERENCES `countries` (`country_code` )
    ON DELETE CASCADE
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `zones_to_provinces`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `zones_to_provinces` (
  `zone_id` INT(11) UNSIGNED NOT NULL ,
  `province_id` INT(11) UNSIGNED NOT NULL ,
  PRIMARY KEY (`zone_id`, `province_id`) ,
  INDEX `fk_zones_to_states_zones1_idx` (`zone_id` ASC) ,
  INDEX `fk_zones_to_states_states1_idx` (`province_id` ASC) ,
  CONSTRAINT `fk_zones_to_states_zones1`
    FOREIGN KEY (`zone_id` )
    REFERENCES `zones` (`zone_id` )
    ON DELETE CASCADE
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_zones_to_states_states1`
    FOREIGN KEY (`province_id` )
    REFERENCES `provinces` (`province_id` )
    ON DELETE CASCADE
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `help_topics`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `help_topics` (
  `topic_id` VARCHAR(255) NOT NULL ,
  `language_code` CHAR(2) NOT NULL ,
  `topic` VARCHAR(255) NOT NULL ,
  `short_text` TEXT NOT NULL ,
  `long_text` MEDIUMTEXT NOT NULL ,
  `full_text` MEDIUMTEXT NOT NULL ,
  `date_added` DATETIME NOT NULL ,
  `last_modified` DATETIME NOT NULL ,
  PRIMARY KEY (`topic_id`) ,
  UNIQUE INDEX `topic_id_UNIQUE` (`topic_id` ASC) ,
  INDEX `fk_help_topics_languages1_idx` (`language_code` ASC) ,
  CONSTRAINT `fk_help_topics_languages1`
    FOREIGN KEY (`language_code` )
    REFERENCES `languages` (`language_code` )
    ON DELETE CASCADE
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `sale`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `sale` (
  `sale_id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT ,
  `store_id` SMALLINT UNSIGNED NOT NULL ,
  `collection_id` INT(11) UNSIGNED NULL DEFAULT NULL ,
  `description_id` BIGINT UNSIGNED NOT NULL ,
  `image_id` BIGINT UNSIGNED NULL ,
  `discount_type` VARCHAR(20) NOT NULL DEFAULT '(%)' ,
  `amount` DECIMAL(15,4) NOT NULL DEFAULT 0 ,
  `sale_begin` DATETIME NULL ,
  `sale_end` DATETIME NULL ,
  `indefinite` TINYINT(1) UNSIGNED NULL DEFAULT 0 ,
  `price_min` DECIMAL(5,4) NULL ,
  `price_max` DECIMAL(5,4) NULL ,
  `sort_order` INT UNSIGNED NOT NULL DEFAULT '0' ,
  `is_active` TINYINT(1) UNSIGNED NOT NULL ,
  `date_created` DATETIME NOT NULL ,
  `last_modified` DATETIME NOT NULL ,
  PRIMARY KEY (`sale_id`, `store_id`) ,
  INDEX `fk_sale_images1_idx` (`image_id` ASC) ,
  INDEX `fk_sale_stores1_idx` (`store_id` ASC) ,
  INDEX `fk_sale_description1_idx` (`description_id` ASC) ,
  INDEX `fk_sale_collection_id_idx` (`collection_id` ASC) ,
  CONSTRAINT `fk_sale_images1`
    FOREIGN KEY (`image_id` )
    REFERENCES `images` (`image_id` )
    ON DELETE SET NULL
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_sale_description1`
    FOREIGN KEY (`description_id` )
    REFERENCES `content` (`content_id` )
    ON DELETE RESTRICT
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_sale_stores1`
    FOREIGN KEY (`store_id` )
    REFERENCES `stores` (`store_id` )
    ON DELETE CASCADE
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_sale_collection_id`
    FOREIGN KEY (`collection_id` )
    REFERENCES `collections` (`collection_id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `store_keychain`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `store_keychain` (
  `admin_id` INT(11) UNSIGNED NOT NULL ,
  `key_hash` VARCHAR(255) NOT NULL ,
  `date_added` DATETIME NOT NULL ,
  PRIMARY KEY (`admin_id`) ,
  CONSTRAINT `fk_store_keychain_admin1`
    FOREIGN KEY (`admin_id` )
    REFERENCES `admin` (`admin_id` )
    ON DELETE CASCADE
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `application_preferences`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `application_preferences` (
  `pk` BIT NOT NULL DEFAULT 0 ,
  `app_name` ENUM('Cataleya') NOT NULL DEFAULT 'Cataleya' ,
  `app_version` VARCHAR(15) NOT NULL ,
  `created_by` ENUM('Fancy Paper Planes', 'FPP Labs', 'FPP Labs ©2015') NOT NULL DEFAULT 'FPP Labs ©2015' ,
  `license` TEXT CHARACTER SET 'utf8mb4' NOT NULL ,
  `developer` VARCHAR(160) NOT NULL DEFAULT 'unknown' ,
  `client` VARCHAR(160) NOT NULL DEFAULT 'unknown' ,
  `default_language` CHAR(2) NOT NULL DEFAULT 'EN' ,
  `dashboard_language_code` CHAR(2) NOT NULL DEFAULT 'EN' ,
  `shopfront_language_code` CHAR(2) NOT NULL DEFAULT 'EN' ,
  `auto_generate_skus` TINYINT(1) UNSIGNED NOT NULL DEFAULT 1 ,
  `allow_sku` TINYINT(1) UNSIGNED NOT NULL DEFAULT 1 ,
  `allow_ean` TINYINT(1) UNSIGNED NOT NULL DEFAULT 1 ,
  `allow_isbn` TINYINT(1) UNSIGNED NOT NULL DEFAULT 1 ,
  `allow_upc` TINYINT(1) UNSIGNED NOT NULL DEFAULT 1 ,
  `sku_autogen_format` VARCHAR(45) NOT NULL DEFAULT 'PT-SN-A' ,
  `sku_serial_num` BIGINT UNSIGNED NULL DEFAULT 0 ,
  `max_catalog_population` INT UNSIGNED NOT NULL DEFAULT 10000 ,
  `product_count` INT UNSIGNED NOT NULL DEFAULT '0' ,
  `max_admin_accounts` INT UNSIGNED NOT NULL DEFAULT 20 ,
  `admin_count` SMALLINT UNSIGNED NOT NULL DEFAULT '0' ,
  `max_customer_accounts` INT UNSIGNED NOT NULL DEFAULT 50000 ,
  `customer_count` INT NOT NULL DEFAULT '0' ,
  `last_modified` DATETIME NOT NULL ,
  `active` TINYINT(1) UNSIGNED NOT NULL ,
  `last_shutdown` DATETIME NOT NULL ,
  `last_activated` DATETIME NOT NULL ,
  `credits` TEXT NOT NULL ,
  `dashboard_theme_id` VARCHAR(255) NULL DEFAULT 'default' ,
  `shopfront_theme_id` VARCHAR(255) NULL DEFAULT 'default' ,
  PRIMARY KEY (`pk`) ,
  UNIQUE INDEX `pk_UNIQUE` (`pk` ASC) ,
  INDEX `fk_application_preferences_languages1_idx` (`default_language` ASC) ,
  INDEX `fk_dashboard_language_code_idx` (`dashboard_language_code` ASC) ,
  INDEX `fk_shopfront_language_code_idx` (`shopfront_language_code` ASC) ,
  CONSTRAINT `fk_application_preferences_languages1`
    FOREIGN KEY (`default_language` )
    REFERENCES `languages` (`language_code` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_dashboard_language_code`
    FOREIGN KEY (`dashboard_language_code` )
    REFERENCES `languages` (`language_code` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_shopfront_language_code`
    FOREIGN KEY (`shopfront_language_code` )
    REFERENCES `languages` (`language_code` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
MAX_ROWS = 1
MIN_ROWS = 0;


-- -----------------------------------------------------
-- Table `coupon_redeem_track`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `coupon_redeem_track` (
  `pk` BIGINT UNSIGNED NOT NULL AUTO_INCREMENT ,
  `coupon_id` INT UNSIGNED NOT NULL ,
  `customer_id` INT(11) UNSIGNED NOT NULL ,
  `order_id` INT(11) UNSIGNED NOT NULL ,
  `redeem_date` DATETIME NOT NULL ,
  PRIMARY KEY (`pk`, `coupon_id`, `customer_id`) ,
  INDEX `fk_coupon_redeem_track_customers1_idx` (`customer_id` ASC) ,
  INDEX `fk_coupon_redeem_track_coupons1_idx` (`coupon_id` ASC) ,
  INDEX `fk_coupon_redeem_track_orders1_idx` (`order_id` ASC) ,
  CONSTRAINT `fk_coupon_redeem_track_customers1`
    FOREIGN KEY (`customer_id` )
    REFERENCES `customers` (`customer_id` )
    ON DELETE CASCADE
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_coupon_redeem_track_coupons1`
    FOREIGN KEY (`coupon_id` )
    REFERENCES `coupons` (`coupon_id` )
    ON DELETE CASCADE
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_coupon_redeem_track_orders1`
    FOREIGN KEY (`order_id` )
    REFERENCES `orders` (`order_id` )
    ON DELETE CASCADE
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `cat_stats_daily`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `cat_stats_daily` (
  `pk` INT UNSIGNED NOT NULL AUTO_INCREMENT ,
  `product_id` INT(11) UNSIGNED NOT NULL ,
  `store_id` SMALLINT UNSIGNED NOT NULL ,
  `tally_views` INT UNSIGNED NOT NULL DEFAULT 0 ,
  `tally_wishlists` INT UNSIGNED NOT NULL DEFAULT 0 ,
  `tally_orders` INT UNSIGNED NOT NULL DEFAULT 0 ,
  `order_subtotal` DECIMAL(15,4) NOT NULL DEFAULT 0 ,
  `date_added` DATETIME NOT NULL ,
  PRIMARY KEY (`pk`) ,
  INDEX `fk_views_daily_products1_idx` (`product_id` ASC) ,
  INDEX `fk_cat_stats_daily_stores1_idx` (`store_id` ASC) ,
  CONSTRAINT `fk_views_daily_products1`
    FOREIGN KEY (`product_id` )
    REFERENCES `products` (`product_id` )
    ON DELETE CASCADE
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_cat_stats_daily_stores1`
    FOREIGN KEY (`store_id` )
    REFERENCES `stores` (`store_id` )
    ON DELETE CASCADE
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `cat_stats_monthly`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `cat_stats_monthly` (
  `pk` INT UNSIGNED NOT NULL AUTO_INCREMENT ,
  `product_id` INT(11) UNSIGNED NOT NULL ,
  `store_id` SMALLINT UNSIGNED NOT NULL ,
  `tally_views` INT UNSIGNED NOT NULL DEFAULT 0 ,
  `tally_wishlists` INT UNSIGNED NOT NULL DEFAULT 0 ,
  `tally_orders` INT UNSIGNED NOT NULL DEFAULT 0 ,
  `order_subtotal` DECIMAL(15,4) NOT NULL DEFAULT 0 ,
  `date_added` DATETIME NOT NULL ,
  PRIMARY KEY (`pk`) ,
  INDEX `fk_views_daily_products1_idx` (`product_id` ASC) ,
  INDEX `fk_cat_stats_monthly_stores1_idx` (`store_id` ASC) ,
  CONSTRAINT `fk_views_daily_products10`
    FOREIGN KEY (`product_id` )
    REFERENCES `products` (`product_id` )
    ON DELETE CASCADE
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_cat_stats_monthly_stores1`
    FOREIGN KEY (`store_id` )
    REFERENCES `stores` (`store_id` )
    ON DELETE CASCADE
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `cat_stats_x3month`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `cat_stats_x3month` (
  `pk` INT UNSIGNED NOT NULL AUTO_INCREMENT ,
  `product_id` INT(11) UNSIGNED NOT NULL ,
  `store_id` SMALLINT UNSIGNED NOT NULL ,
  `tally_views` INT UNSIGNED NOT NULL DEFAULT 0 ,
  `tally_wishlists` INT UNSIGNED NOT NULL DEFAULT 0 ,
  `tally_orders` INT UNSIGNED NOT NULL DEFAULT 0 ,
  `order_subtotal` DECIMAL(15,4) NOT NULL DEFAULT 0 ,
  `date_added` DATETIME NOT NULL ,
  PRIMARY KEY (`pk`) ,
  INDEX `fk_views_daily_products1_idx` (`product_id` ASC) ,
  INDEX `fk_cat_stats_x3month_stores1_idx` (`store_id` ASC) ,
  CONSTRAINT `fk_views_daily_products100`
    FOREIGN KEY (`product_id` )
    REFERENCES `products` (`product_id` )
    ON DELETE CASCADE
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_cat_stats_x3month_stores1`
    FOREIGN KEY (`store_id` )
    REFERENCES `stores` (`store_id` )
    ON DELETE CASCADE
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `failed_logins_hourly`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `failed_logins_hourly` (
  `pk` INT UNSIGNED NOT NULL AUTO_INCREMENT ,
  `tally_admin` INT UNSIGNED NOT NULL DEFAULT 0 ,
  `tally_customers` INT UNSIGNED NOT NULL DEFAULT 0 ,
  `date_added` DATETIME NOT NULL ,
  PRIMARY KEY (`pk`) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `failed_logins_monthly`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `failed_logins_monthly` (
  `pk` INT UNSIGNED NOT NULL AUTO_INCREMENT ,
  `tally_admin` INT UNSIGNED NOT NULL DEFAULT 0 ,
  `tally_customers` INT UNSIGNED NOT NULL DEFAULT 0 ,
  `date_added` DATETIME NOT NULL ,
  PRIMARY KEY (`pk`) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `failed_logins_x3month`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `failed_logins_x3month` (
  `pk` INT UNSIGNED NOT NULL AUTO_INCREMENT ,
  `tally_admin` INT UNSIGNED NOT NULL DEFAULT 0 ,
  `tally_customers` INT UNSIGNED NOT NULL DEFAULT 0 ,
  `date_added` DATETIME NOT NULL ,
  PRIMARY KEY (`pk`) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `admin_ua_trend`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `admin_ua_trend` (
  `pk` BIGINT(15) UNSIGNED NOT NULL AUTO_INCREMENT ,
  `admin_id` INT(11) UNSIGNED NOT NULL ,
  `fingerprint` TEXT NOT NULL ,
  `login_tally` INT(11) UNSIGNED NOT NULL DEFAULT 0 ,
  `last_login` DATETIME NOT NULL ,
  `date_added` DATETIME NOT NULL ,
  PRIMARY KEY (`pk`) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `customer_geoloc_trend`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `customer_geoloc_trend` (
  `pk` BIGINT(15) UNSIGNED NOT NULL AUTO_INCREMENT ,
  `customer_id` INT(11) NULL ,
  `country_iso2_code` CHAR(2) NOT NULL ,
  `city` VARCHAR(45) NULL ,
  `login_tally` INT(11) UNSIGNED NOT NULL DEFAULT 0 ,
  `last_login` DATETIME NOT NULL ,
  `date_added` DATETIME NOT NULL ,
  PRIMARY KEY (`pk`) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `staff_members`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `staff_members` (
  `admin_id` INT(11) UNSIGNED NOT NULL ,
  `store_id` SMALLINT UNSIGNED NOT NULL ,
  PRIMARY KEY (`admin_id`, `store_id`) ,
  INDEX `fk_staff_members_admin1_idx` (`admin_id` ASC) ,
  INDEX `fk_staff_members_stores1_idx` (`store_id` ASC) ,
  CONSTRAINT `fk_staff_members_admin1`
    FOREIGN KEY (`admin_id` )
    REFERENCES `admin` (`admin_id` )
    ON DELETE CASCADE
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_staff_members_stores1`
    FOREIGN KEY (`store_id` )
    REFERENCES `stores` (`store_id` )
    ON DELETE CASCADE
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `tiles`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `tiles` (
  `store_id` SMALLINT UNSIGNED NOT NULL ,
  `tile_id` INT UNSIGNED NOT NULL AUTO_INCREMENT ,
  `pixel_width` FLOAT UNSIGNED NOT NULL DEFAULT 1024 ,
  `pixel_height` FLOAT UNSIGNED NOT NULL DEFAULT 25 ,
  `sort_order` SMALLINT UNSIGNED NOT NULL DEFAULT 0 ,
  PRIMARY KEY (`tile_id`, `store_id`) ,
  INDEX `fk_tiles_stores1_idx` (`store_id` ASC) ,
  CONSTRAINT `fk_tiles_stores1`
    FOREIGN KEY (`store_id` )
    REFERENCES `stores` (`store_id` )
    ON DELETE CASCADE
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `slides`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `slides` (
  `slide_id` INT UNSIGNED NOT NULL AUTO_INCREMENT ,
  `tile_id` INT UNSIGNED NOT NULL ,
  `type` ENUM('image', 'text', 'html') NOT NULL DEFAULT 'image' ,
  `text_id` BIGINT UNSIGNED NULL ,
  `image_id` BIGINT UNSIGNED NULL ,
  `href` MEDIUMTEXT NOT NULL ,
  `target` ENUM('_blank', '_self',  '_parent', '_top') NOT NULL DEFAULT '_blank' ,
  `sort_order` SMALLINT UNSIGNED NOT NULL DEFAULT 0 ,
  PRIMARY KEY (`slide_id`, `tile_id`) ,
  INDEX `fk_slides_tiles1_idx` (`tile_id` ASC) ,
  CONSTRAINT `fk_slides_tiles1`
    FOREIGN KEY (`tile_id` )
    REFERENCES `tiles` (`tile_id` )
    ON DELETE CASCADE
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `wishlists`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `wishlists` (
  `wishlist_id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT ,
  `store_id` SMALLINT UNSIGNED NULL ,
  `customer_id` INT(11) UNSIGNED NOT NULL ,
  `created` DATETIME NOT NULL ,
  `modified` DATETIME NOT NULL ,
  `status` TINYINT(1) NOT NULL DEFAULT TRUE ,
  `secure_key` VARCHAR(255) NOT NULL ,
  PRIMARY KEY (`wishlist_id`) ,
  INDEX `fk_cart_products1_idx` (`customer_id` ASC) ,
  INDEX `fk_cart_stores1_idx` (`store_id` ASC) ,
  CONSTRAINT `fk_wishlist_customers`
    FOREIGN KEY (`customer_id` )
    REFERENCES `customers` (`customer_id` )
    ON DELETE CASCADE
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_cart_stores10`
    FOREIGN KEY (`store_id` )
    REFERENCES `stores` (`store_id` )
    ON DELETE CASCADE
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `wishlist_items`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `wishlist_items` (
  `wishlist_item_id` BIGINT UNSIGNED NOT NULL AUTO_INCREMENT ,
  `wishlist_id` INT(11) UNSIGNED NOT NULL ,
  `product_id` INT(11) UNSIGNED NOT NULL ,
  `option_id` BIGINT UNSIGNED NOT NULL ,
  `quantity` FLOAT NOT NULL DEFAULT 1 ,
  `date_added` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ,
  PRIMARY KEY (`wishlist_item_id`, `wishlist_id`, `product_id`, `option_id`) ,
  INDEX `fk_cart_items_products1_idx` (`product_id` ASC) ,
  INDEX `fk_cart_items_options1_idx` (`option_id` ASC) ,
  CONSTRAINT `fk_cart_items_cart100`
    FOREIGN KEY (`wishlist_id` )
    REFERENCES `wishlists` (`wishlist_id` )
    ON DELETE CASCADE
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_cart_items_products10`
    FOREIGN KEY (`product_id` )
    REFERENCES `products` (`product_id` )
    ON DELETE CASCADE
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_cart_items_options10`
    FOREIGN KEY (`option_id` )
    REFERENCES `product_options` (`option_id` )
    ON DELETE CASCADE
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `fulltext_order_index_isam`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `fulltext_order_index_isam` (
  `order_id` INT(11) UNSIGNED NOT NULL ,
  `keywords` TEXT NULL ,
  PRIMARY KEY (`order_id`) ,
  FULLTEXT INDEX `idx_data_index` (`keywords` ASC) )
ENGINE = MyISAM;


-- -----------------------------------------------------
-- Table `product_group`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `product_group` (
  `product_id` INT(11) UNSIGNED NOT NULL ,
  `member_product_id` INT(11) UNSIGNED NOT NULL ,
  PRIMARY KEY (`product_id`, `member_product_id`) ,
  INDEX `fk_product_group_products1_idx` (`product_id` ASC) ,
  INDEX `fk_product_group_products2_idx` (`member_product_id` ASC) ,
  CONSTRAINT `fk_product_group_products1`
    FOREIGN KEY (`product_id` )
    REFERENCES `products` (`product_id` )
    ON DELETE CASCADE
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_product_group_products2`
    FOREIGN KEY (`member_product_id` )
    REFERENCES `products` (`product_id` )
    ON DELETE CASCADE
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `options_to_attributes`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `options_to_attributes` (
  `option_id` BIGINT UNSIGNED NOT NULL ,
  `attribute_id` BIGINT UNSIGNED NOT NULL ,
  PRIMARY KEY (`option_id`, `attribute_id`) ,
  INDEX `fk_option_id_idx` (`option_id` ASC) ,
  INDEX `fk_attribute_id_idx` (`attribute_id` ASC) ,
  CONSTRAINT `fk_option_id`
    FOREIGN KEY (`option_id` )
    REFERENCES `product_options` (`option_id` )
    ON DELETE CASCADE
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_attribute_id`
    FOREIGN KEY (`attribute_id` )
    REFERENCES `attributes` (`attribute_id` )
    ON DELETE CASCADE
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `product_types_to_attribute_types`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `product_types_to_attribute_types` (
  `product_type_id` INT(11) UNSIGNED NOT NULL ,
  `attribute_type_id` INT(11) UNSIGNED NOT NULL ,
  `sort_order` INT UNSIGNED NOT NULL DEFAULT '0' COMMENT 'This is actually NOT a table wide sorting. It is local to product_types to attribute_types associations.' ,
  PRIMARY KEY (`product_type_id`, `attribute_type_id`) ,
  INDEX `fk_attribute_group_id_idx` (`attribute_type_id` ASC) ,
  CONSTRAINT `fk_product_type_id1223`
    FOREIGN KEY (`product_type_id` )
    REFERENCES `product_types` (`product_type_id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_attribute_type_id1233`
    FOREIGN KEY (`attribute_type_id` )
    REFERENCES `attribute_types` (`attribute_type_id` )
    ON DELETE CASCADE
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `products_to_stores`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `products_to_stores` (
  `product_id` INT(11) UNSIGNED NOT NULL ,
  `store_id` SMALLINT UNSIGNED NOT NULL ,
  PRIMARY KEY (`product_id`, `store_id`) ,
  INDEX `fk_product_id321_idx` (`product_id` ASC) ,
  INDEX `fk_store_id76243_idx` (`store_id` ASC) ,
  CONSTRAINT `fk_product_id321`
    FOREIGN KEY (`product_id` )
    REFERENCES `products` (`product_id` )
    ON DELETE CASCADE
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_store_id76243`
    FOREIGN KEY (`store_id` )
    REFERENCES `stores` (`store_id` )
    ON DELETE CASCADE
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `customer_contacts`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `customer_contacts` (
  `customer_id` INT(11) UNSIGNED NOT NULL ,
  `contact_id` BIGINT UNSIGNED NOT NULL ,
  PRIMARY KEY (`customer_id`, `contact_id`) ,
  INDEX `fk_customer_id_idx` (`customer_id` ASC) ,
  INDEX `contact_id_idx` (`contact_id` ASC) ,
  CONSTRAINT `fk_customer_id`
    FOREIGN KEY (`customer_id` )
    REFERENCES `customers` (`customer_id` )
    ON DELETE CASCADE
    ON UPDATE NO ACTION,
  CONSTRAINT `contact_id`
    FOREIGN KEY (`contact_id` )
    REFERENCES `address_book` (`contact_id` )
    ON DELETE CASCADE
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `store_contacts`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `store_contacts` (
  `store_id` SMALLINT UNSIGNED NOT NULL ,
  `contact_id` BIGINT UNSIGNED NOT NULL ,
  PRIMARY KEY (`store_id`, `contact_id`) ,
  INDEX `fk_store_id_idx` (`store_id` ASC) ,
  INDEX `fk_contact_id_idx` (`contact_id` ASC) ,
  CONSTRAINT `fk_store_id`
    FOREIGN KEY (`store_id` )
    REFERENCES `stores` (`store_id` )
    ON DELETE CASCADE
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_contact_id`
    FOREIGN KEY (`contact_id` )
    REFERENCES `address_book` (`contact_id` )
    ON DELETE CASCADE
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `checkout`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `checkout` (
  `cart_id` INT(11) UNSIGNED NOT NULL ,
  `store_id` SMALLINT UNSIGNED NULL ,
  `date_created` DATETIME NOT NULL ,
  `date_modified` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP ,
  `customer_id` INT(11) UNSIGNED NULL DEFAULT NULL ,
  `shipping_option_id` INT UNSIGNED NULL DEFAULT NULL ,
  `shipping_charges` DECIMAL(10,4) NULL DEFAULT NULL ,
  `shipping_before_tax` DECIMAL(10,4) NULL DEFAULT NULL ,
  `product_tax_json` TEXT NULL ,
  `shipping_tax_json` TEXT NULL ,
  `currency_code` CHAR(3) NOT NULL ,
  `billing_address_id` BIGINT UNSIGNED NOT NULL ,
  `shipping_address_id` BIGINT UNSIGNED NOT NULL ,
  `payment_type_id` SMALLINT UNSIGNED NULL DEFAULT NULL ,
  PRIMARY KEY (`cart_id`) ,
  INDEX `fk_orders_customers1_idx` (`customer_id` ASC) ,
  INDEX `fk_orders_currencies1_idx` (`currency_code` ASC) ,
  INDEX `fk_orders_stores1_idx` (`store_id` ASC) ,
  INDEX `fk_billing_address_id_idx` (`billing_address_id` ASC) ,
  INDEX `fk_checkout_shipping_option_id_idx` (`shipping_option_id` ASC) ,
  CONSTRAINT `fk_orders_customers10`
    FOREIGN KEY (`customer_id` )
    REFERENCES `customers` (`customer_id` )
    ON DELETE CASCADE
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_orders_currencies10`
    FOREIGN KEY (`currency_code` )
    REFERENCES `currencies` (`currency_code` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_orders_stores10`
    FOREIGN KEY (`store_id` )
    REFERENCES `stores` (`store_id` )
    ON DELETE CASCADE
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_billing_address_id0`
    FOREIGN KEY (`billing_address_id` )
    REFERENCES `address_book` (`contact_id` )
    ON DELETE RESTRICT
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_checkout_cart_id`
    FOREIGN KEY (`cart_id` )
    REFERENCES `cart` (`cart_id` )
    ON DELETE CASCADE
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_checkout_shipping_option_id`
    FOREIGN KEY (`shipping_option_id` )
    REFERENCES `shipping_options` (`shipping_option_id` )
    ON DELETE SET NULL
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `shipment_items`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `shipment_items` (
  `shipment_id` BIGINT UNSIGNED NOT NULL ,
  `order_id` INT(11) UNSIGNED NOT NULL ,
  `order_detail_id` INT(11) UNSIGNED NOT NULL ,
  `quantity` FLOAT NOT NULL ,
  PRIMARY KEY (`shipment_id`) ,
  CONSTRAINT `fk_si_shipment_id`
    FOREIGN KEY (`shipment_id` )
    REFERENCES `shipment` (`shipment_id` )
    ON DELETE CASCADE
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `content_languages`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `content_languages` (
  `content_id` BIGINT UNSIGNED NOT NULL ,
  `language_code` CHAR(2) NOT NULL ,
  `title` VARCHAR(255) NOT NULL DEFAULT 'Untitled Page' ,
  `description` TINYTEXT NOT NULL ,
  `text` MEDIUMTEXT NOT NULL ,
  `last_modified` DATETIME NOT NULL ,
  `date_added` DATETIME NOT NULL ,
  PRIMARY KEY (`content_id`, `language_code`) ,
  INDEX `content_description1_title_idx` (`title` ASC) ,
  INDEX `fk_content_page_id_idx` (`content_id` ASC) ,
  INDEX `fk_content_language_id_idx` (`language_code` ASC) ,
  CONSTRAINT `fk_content_page_id`
    FOREIGN KEY (`content_id` )
    REFERENCES `content` (`content_id` )
    ON DELETE CASCADE
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_content_language_id`
    FOREIGN KEY (`language_code` )
    REFERENCES `languages` (`language_code` )
    ON DELETE CASCADE
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `store_pages`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `store_pages` (
  `store_id` SMALLINT UNSIGNED NOT NULL ,
  `page_id` BIGINT UNSIGNED NOT NULL ,
  PRIMARY KEY (`store_id`, `page_id`) ,
  UNIQUE INDEX `page_id_UNIQUE` (`page_id` ASC) ,
  INDEX `fk_store_pages_store_id_idx` (`store_id` ASC) ,
  CONSTRAINT `fk_store_pages_store_id`
    FOREIGN KEY (`store_id` )
    REFERENCES `stores` (`store_id` )
    ON DELETE CASCADE
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_store_pages_page_id`
    FOREIGN KEY (`page_id` )
    REFERENCES `content` (`content_id` )
    ON DELETE CASCADE
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `products_to_collections`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `products_to_collections` (
  `product_id` INT(11) UNSIGNED NOT NULL ,
  `collection_id` INT(11) UNSIGNED NOT NULL ,
  `featured` TINYINT(1) NOT NULL DEFAULT '0' ,
  `sort_order` INT(12) NOT NULL DEFAULT '0' ,
  `date_added` DATETIME NOT NULL ,
  `last_modified` DATETIME NOT NULL ,
  PRIMARY KEY (`product_id`, `collection_id`) ,
  INDEX `fk_collections_id_idx` (`collection_id` ASC) ,
  INDEX `fk_collections_product_id_idx` (`product_id` ASC) ,
  CONSTRAINT `fk_collections_id`
    FOREIGN KEY (`collection_id` )
    REFERENCES `collections` (`collection_id` )
    ON DELETE CASCADE
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_collections_product_id`
    FOREIGN KEY (`product_id` )
    REFERENCES `products` (`product_id` )
    ON DELETE CASCADE
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `web_hooks`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `web_hooks` (
  `hook_id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT ,
  `description_id` BIGINT UNSIGNED NOT NULL ,
  `scope` VARCHAR(255) NOT NULL ,
  `headers` TEXT NULL ,
  `fields` TINYTEXT NULL ,
  `store_id` SMALLINT UNSIGNED NULL DEFAULT NULL ,
  `format` ENUM('json', 'xml', 'yml', 'query') NOT NULL DEFAULT 'json' ,
  `auth_key` VARCHAR(255) NOT NULL ,
  `failed_attempts` SMALLINT UNSIGNED NOT NULL DEFAULT '0' ,
  `url` VARCHAR(255) NOT NULL ,
  `date_added` DATETIME NOT NULL ,
  `last_modified` DATETIME NOT NULL ,
  `is_active` TINYINT(1) UNSIGNED NOT NULL DEFAULT '0' ,
  PRIMARY KEY (`hook_id`) ,
  INDEX `fk_webhook_store_id_idx` (`store_id` ASC) ,
  INDEX `fk_webhook_description_id_idx` (`description_id` ASC) ,
  CONSTRAINT `fk_webhook_store_id`
    FOREIGN KEY (`store_id` )
    REFERENCES `stores` (`store_id` )
    ON DELETE CASCADE
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_webhook_description_id`
    FOREIGN KEY (`description_id` )
    REFERENCES `content` (`content_id` )
    ON DELETE RESTRICT
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `plugin_registry`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `plugin_registry` (
  `handle` VARCHAR(160) NOT NULL ,
  `scope` VARCHAR(255) NOT NULL DEFAULT '*' ,
  `events` TINYTEXT NOT NULL ,
  `hooks` TINYTEXT NOT NULL ,
  `name` VARCHAR(255) NOT NULL ,
  `description` TEXT NOT NULL ,
  `vendor` VARCHAR(255) NULL ,
  `version` VARCHAR(45) NULL ,
  `type` ENUM('local', 'remote') NOT NULL ,
  `namespace` VARCHAR(160) NOT NULL ,
  `controller_class` VARCHAR(256) NOT NULL ,
  `status` TINYINT(1) UNSIGNED NOT NULL DEFAULT 0 ,
  `date_added` DATETIME NOT NULL ,
  `last_updated` DATETIME NOT NULL ,
  PRIMARY KEY (`handle`) ,
  UNIQUE INDEX `handle_UNIQUE` (`handle` ASC) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `event_registry`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `event_registry` (
  `event_id` INT UNSIGNED NOT NULL AUTO_INCREMENT ,
  `event` VARCHAR(250) NOT NULL ,
  `owner` VARCHAR(300) NOT NULL ,
  `date_added` VARCHAR(45) NOT NULL ,
  PRIMARY KEY (`event_id`) ,
  UNIQUE INDEX `event_UNIQUE` (`event` ASC) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `plugin_hooks`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `plugin_hooks` (
  `hook_id` INT UNSIGNED NOT NULL AUTO_INCREMENT ,
  `scope` VARCHAR(300) NOT NULL ,
  `listener` VARCHAR(300) NOT NULL ,
  `date_added` DATETIME NOT NULL ,
  PRIMARY KEY (`hook_id`) )
ENGINE = InnoDB;



SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
