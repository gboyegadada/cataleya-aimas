<?php


//// DB TABLES /////////
define('OUTPUT', 'JSON');



////////// CONFIG //////////
require_once './includes/app.config.php';	



//// CATALEYA TABLES /////////



$_stmnt = Cataleya\Helper\DBH::getInstance()->prepare('SELECT 1 FROM languages LIMIT 1');
if ($_stmnt->execute())
{

    // tables already installed
    $_json_reply = array (

    'status' => 'installed', 
    'message' => 'Database tables already installed.', 
    'time' => 0, 
    'token' => NEW_REQ_TOKEN
    );

    echo json_encode($_json_reply);
    exit();

}


else {

// create tables
Cataleya\Helper::startTimer('install');
Cataleya\Helper\DBH::runScript('cataleya.setup.sql');
$_how_long = Cataleya\Helper::stopTimer('install');



// tables installed
$_json_reply = array (

'status' => 'installed', 
'message' => ('Database tables installed in - ' . $_how_long . ' seconds'), 
'time' => $_how_long, 
'token' => NEW_REQ_TOKEN
);

echo json_encode($_json_reply);
exit();


}






