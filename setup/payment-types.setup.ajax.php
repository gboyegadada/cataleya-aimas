<?php


/////// PAYMENT TYPES ////////////////////
define('OUTPUT', 'JSON');



////////// APP TOP //////////
require_once './includes/app.config.php';	




$_results = Cataleya\Helper\DBH::getInstance()->query('SELECT 1 FROM payment_types LIMIT 1')->fetchAll(PDO::FETCH_ASSOC);

if (count($_results) > 0)
{

    // config already installed
    $_json_reply = array (

    'status' => 'installed', 
    'message' => 'Payment modules already installed.', 
    'time' => 0, 
    'token' =>  NEW_REQ_TOKEN
    );

    echo json_encode($_json_reply);
    exit();

}







Cataleya\Helper::startTimer('install');

$_dbh = Cataleya\Helper\DBH::getInstance();
$_dbh->beginTransaction();

// ------------------------- WEBPAY -------------------------------------------- //



// [1] WEBPAY: PAYMENT TYPE
$_payment_type_options = array (
    'name'  =>  'Interswitch WebPay', 
    'logo' => 'webpay.gif', 
    'display_name'  =>  'WebPay', 
    'config_id' =>  'webpay.config', 
    'payment_class' => 'Cataleya\Payment\PaymentType\WebPay', 
    'requires_redirect' =>  1
);

$payment_type_webpay = Cataleya\Payment\PaymentType::create($_payment_type_options);
$payment_type_webpay->setActive(TRUE);






// ------------------------- E-TRANZACT -------------------------------------------- //



// [2] E-TRANZACT: PAYMENT TYPE
$_payment_type_options = array (
    'name'  =>  'Interswitch E-Tranzact', 
    'logo' => 'etranzact.gif', 
    'display_name'  =>  'E-Tranzact', 
    'config_id' =>  'webpay.config', 
    'payment_class' => 'Cataleya\Payment\PaymentType\eTranzact', 
    'requires_redirect' =>  1
);

$payment_type_etranzact = Cataleya\Payment\PaymentType::create($_payment_type_options);
$payment_type_etranzact->setActive(TRUE);





// ------------------------- PAY-PAL: EXPRESS CHECKOUT + DIRECT PAYMENT -------------------------------------------- //



// [3.A] PAYMENT TYPE: EXPRESS CHECKOUT
$_payment_type_options = array (
    'name'  =>  'PayPal Express Checkout', 
    'logo' => 'paypal.gif', 
    'display_name'  =>  'PayPal', 
    'config_id' =>  'paypal.config', 
    'payment_class' => 'Cataleya\Payment\PaymentType\PayPalExpressCheckout', 
    'requires_redirect' =>  1
);

$payment_type_PayPalEC = Cataleya\Payment\PaymentType::create($_payment_type_options);
$payment_type_PayPalEC->setActive(TRUE);

// [3.B] PAYMENT TYPE: DIRECT PAYMENT
$_payment_type_options = array (
    'name'  =>  'PayPal Direct Payment', 
    'logo' => 'paypal-direct.gif',  
    'display_name'  =>  'Credit Card', 
    'config_id' =>  'paypal.config', 
    'payment_class' => 'Cataleya\Payment\PaymentType\PayPalDirectPayment', 
    'requires_redirect' =>  1, 
    'requires_billing_address' => 1
);

$payment_type_PayPalDP = Cataleya\Payment\PaymentType::create($_payment_type_options);
$payment_type_PayPalDP->setActive(FALSE);


// [3.B] PAYMENT TYPE: CASH ON DELIVERY
$_payment_type_options = array (
    'name'  =>  'Cash On Delivery', 
    'logo' => 'cash-on-delivery.gif',  
    'display_name'  =>  'Cash On Delivery', 
    'config_id' =>  'cash.on.delivery.config', 
    'payment_class' => 'Cataleya\Payment\PaymentType\CashOnDelivery', 
    'requires_redirect' =>  0, 
    'requires_billing_address' => 0
);

$payment_type_COD = Cataleya\Payment\PaymentType::create($_payment_type_options);
$payment_type_COD->setActive(FALSE);



// [3.B] PAYMENT TYPE: BANK TRANSFER
$_payment_type_options = array (
    'name'  =>  'Bank Transfer', 
    'logo' => 'bank-transfer.gif',  
    'display_name'  =>  'Bank Transfer', 
    'config_id' =>  'bank.transfer.config', 
    'payment_class' => 'Cataleya\Payment\PaymentType\BankTransfer', 
    'requires_redirect' =>  0, 
    'requires_billing_address' => 0
);

$payment_type_BT = Cataleya\Payment\PaymentType::create($_payment_type_options);
$payment_type_BT->setActive(FALSE);





$_dbh->commit();

$_how_long = Cataleya\Helper::stopTimer('install');



// tables installed
$_json_reply = array (

'status' => 'installed', 
'message' => ('Payment modules installed in - ' . $_how_long . ' seconds'), 
'time' => $_how_long, 
'token' => NEW_REQ_TOKEN
);

echo json_encode($_json_reply);
exit();

