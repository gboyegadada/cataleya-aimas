<?php


/////// PAYMENT TYPES ////////////////////
define('OUTPUT', 'JSON');



////////// APP TOP //////////
require_once './includes/app.config.php';	




$_results = Cataleya\Helper\DBH::getInstance()->query('SELECT 1 FROM tags LIMIT 1')->fetchAll(PDO::FETCH_ASSOC);

if (count($_results) > 0)
{

    // config already installed
    $_json_reply = array (

    'status' => 'installed', 
    'message' => 'Shop demo already installed.', 
    'time' => 0, 
    'token' =>  NEW_REQ_TOKEN
    );

    echo json_encode($_json_reply);
    exit();

}

$_project_info = file_exists(ROOT_PATH . 'project.info.json') 
        ? json_decode(file_get_contents(ROOT_PATH . 'project.info.json', FILE_IGNORE_NEW_LINES)) 
        : NULL;





Cataleya\Helper::startTimer('install');

$_dbh = Cataleya\Helper\DBH::getInstance();
// $_dbh->beginTransaction();

// ------------------------- DEMOS -------------------------------------------- //


// PROVINCES //

$_ng_provinces = array (
    'KW'    =>  'Kwara', 
    'KD'    =>  'Kaduna', 
    'AB'    =>  'Abia', 
    'PH'    =>  'Port Harcourt'
);


// do Lagos first (we'll be needing this later)
$_NG = Cataleya\Geo\Country::load('NG');
$_Lagos = $_NG->addProvince('LA', 'Lagos');

// do other provinces
foreach ($_ng_provinces as $k=>$v) 
{
    $_NG->addProvince($k, $v);
}


// ZONES //

$_zone_uk = Cataleya\Geo\Zone::create('UK Shipping')->setListType('normal')->addCountry(Cataleya\Geo\Country::load('GB'));
Cataleya\Geo\Zone::create('Outside UK Shipping')->setListType('exclude')->addCountry(Cataleya\Geo\Country::load('GB'));;

$_zone_lagos = Cataleya\Geo\Zone::create('Lagos Shipping')->setListType('normal')->addProvince($_Lagos);
Cataleya\Geo\Zone::create('Rest of Nigeria')->setListType('exclude')->addProvince($_Lagos);


// AGENT ROLES

$_Role_Manufacturer = Cataleya\Agent\Role::create('MANUFACTURER', 'Manufacturer', 'Product manufacturer.');
$_Role_Supplier = Cataleya\Agent\Role::create('SUPPLIER', 'Supplier', 'Supplier.'); 
$_Role_Carrier = Cataleya\Agent\Role::create('CARRIER', 'Shipping Carrier', 'Shipping Carrier.');




// PRODUCT TAXES
Cataleya\Tax\TaxRule::create($_zone_uk, Cataleya\Tax\TaxRules::TYPE_PRODUCT_TAX, 6, 'UK VAT');
Cataleya\Tax\TaxRule::create($_zone_lagos, Cataleya\Tax\TaxRules::TYPE_PRODUCT_TAX, 5, 'NG VAT');

// SHIPPNG TAXES
Cataleya\Tax\TaxRule::create($_zone_uk, Cataleya\Tax\TaxRules::TYPE_SHIPPING_TAX, 6, 'UK Shipping Tax');
Cataleya\Tax\TaxRule::create($_zone_lagos, Cataleya\Tax\TaxRules::TYPE_SHIPPING_TAX, 5, 'NG Shipping Tax');



// STORES

foreach ($_project_info->shops as $_s) 
{
    $_loc = null; $_bio = [];

    if (isset($_s->contact)) 
    {
        if (
            isset($_s->contact->{'province'}) && isset($_s->contact->{'province'}->{'iso2'}) && 
            isset($_s->contact->{'country'}) && isset($_s->contact->{'country'}->{'iso2'})
        ) 
        {

            $_loc = Cataleya\Geo\Province::loadByISO($_s->contact->province->iso2, $_s->contact->country->iso2);

        } else if (isset($_s->contact->{'country'}) && isset($_s->contact->{'country'}->{'iso2'})) {

            $_loc = Cataleya\Geo\Country::load($_s->contact->country->iso2);

        }


        $_bio = [

            'label'=>$_s->name, 
            'firstname'=>$_s->contact->firstname, 
            'lastname'=>$_s->contact->lastname, 
            'work_phone' => $_s->contact->phone, 
            'email' => $_s->contact->email, 
            'street' => $_s->contact->street, 
            'street2' => $_s->contact->street2, 
            'city' => $_s->contact->city

        ];
    }
    
    $_contact = Cataleya\Asset\Contact::create((!empty($_loc) ? $_loc : $_Lagos), $_bio);
    $_store = Cataleya\Store::create($_contact, $_s->name, $_s->description, 'EN', 'NGN');
    $_store->setHandle($_s->handle);


    // NG STORE PAGES
    Cataleya\Front\Shop\Page::create($_store, 'Contact', 'Contact...', $_store->getLanguageCode(), TRUE);
    Cataleya\Front\Shop\Page::create($_store, 'About', 'About Us...', $_store->getLanguageCode(), FALSE);
    Cataleya\Front\Shop\Page::create($_store, 'Terms', 'Term and Condidtions', $_store->getLanguageCode(), TRUE);
    Cataleya\Front\Shop\Page::create($_store, 'Return Policies', 'Return Policies', $_store->getLanguageCode(), TRUE);



    /* -------------------- AGENTS ----------------------------- */

    // 1. Default Manufacturer
    Cataleya\Agent::create($_Role_Manufacturer, $_s->name);

    // 2. Default Supplier
    Cataleya\Agent::create($_Role_Supplier, $_s->name);


    // 3. Default Shipping Agent
    $_shipping_agent = Cataleya\Agent::create($_Role_Carrier, $_s->name);




    /* --------------- SHIPPING OPTIONS ---------------------- */

    $_ng_options = array (
       array (
           'title'  =>  'Standard Delivery', 
           'text'   =>  'Delivery within 3-5 working days. Cost is N800.', 
           'max-delivery-days'   =>  5, 
           'rate'   =>  800, 
           'rate-type'  =>  'unit'
       ), 
        
       array (
           'title'  =>  'Next-Day Delivery', 
           'text'   =>  'Order must be placed by 3pm. A signature is required on delivery. Cost is N1200.', 
           'max-delivery-days'   =>  2, 
           'rate'   =>  1200, 
           'rate-type'  =>  'unit'
       ), 
        
       array (
           'title'  =>  'Same-Day Delivery', 
           'text'   =>  'Order must be placed by 12 noon. A signature is required on delivery. Cost is N1500.', 
           'max-delivery-days'   =>  1, 
           'rate'   =>  1500, 
           'rate-type'  =>  'unit'
       )
    );

    foreach ($_ng_options as $_option)
    {
        $_description = Cataleya\Asset\Description::create($_option['title'], $_option['text'], 'EN');
        $ShippingOption = Cataleya\Shipping\ShippingOption::create($_store, $_shipping_agent, $_zone_lagos, $_description);
        
       // Set Delivery Days
       $ShippingOption->setMaxDeliveryDays($_option['max-delivery-days']);   

        // Set Rate Type (rate per: weight, unit, volume, flat)
       $ShippingOption->setRateType($_option['rate-type']);
       
        // Set Rate 
       $ShippingOption->setRate($_option['rate']);

    }

}



// $_dbh->commit();

$_how_long = Cataleya\Helper::stopTimer('install');



// tables installed
$_json_reply = array (

'status' => 'installed', 
'message' => ('Shop demo installed in - ' . $_how_long . ' seconds'), 
'time' => $_how_long, 
'token' => NEW_REQ_TOKEN
);

echo json_encode($_json_reply);
exit();


